<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once 'CorePegadaian.php';

/**
 * @property Main_service main_service
 */
class Main extends CorePegadaian
{


    public function __construct()
    {
        parent::__construct();
        $this->load->model('MainModel', 'main_model');
        $this->load->service('Main_service', 'main_service');
        $this->load->service('Response_service', 'response_service');
        $this->load->service('Emas_service', 'emas_service');
        $this->load->library('form_validation');
    }

    function harga_emas_get()
    {
        $token = $this->getToken();
        if ($token) {
            $hargaEmas = $this->main_model->getLastHargaEmas();
            $this->set_response(array(
                'status' => 'success',
                'message' => '',
                'data' => $hargaEmas
            ), 200);
        } else {
            $this->errorUnAuthorized();
        }
    }

    function harga_stl_get()
    {
        $token = $this->getToken();

        if (!$token) {
            $this->errorUnAuthorized();
            return;
        }

        $response = $this->emas_service->getHargaStl();
        $this->send_response($response);
        return;
    }

    function fluktuasi_emas_get()
    {
        $token = $this->getToken();
        if ($token) {
            $harga = $this->main_model->getFluktuasiEmas();
            $this->set_response(array(
                'status' => 'success',
                'message' => '',
                'data' => $harga
            ), 200);
        } else {
            $this->errorUnAuthorized();
        }
    }

    function banner_get()
    {
        $adminURL = $this->config->item('admin_base_url');
        $banner = $this->main_model->getFrontBanner();
        $resBanner = array();

        foreach ($banner as $b) {
            $b->imgURL = $adminURL . 'upload/banner/' . $b->banner;
            $resBanner[] = $b;
        }

        $this->set_response(array(
            'status' => 'success',
            'message' => '',
            'data' => $resBanner
        ), 200);
    }

    function shortcut_banner_get()
    {
        $token = $this->getToken();
        if ($token) {
            $adminURL = $this->config->item('admin_base_url');
            $banner = $this->main_model->getShortCutBanner();
            $res = array();
            foreach ($banner as $b) {
                $b->gambar =  $adminURL . 'upload/images/' . $b->gambar;
                $res[] = $b;
            }

            $this->set_response(array(
                'status' => 'success',
                'message' => 'success',
                'data' => $res
            ), 200);
        } else {
            $this->errorUnAuthorized();
        }
    }

    function check_otp_post()
    {
        $token = $this->getToken();
        if ($token) {
            $this->form_validation->set_rules('no_telepon', 'no_telepon', 'required|numeric');
            $this->form_validation->set_rules('reff_id', 'reff_id', 'required');

            if ($this->form_validation->run() == false) {
                $this->set_response(array(
                    'status' => 'error',
                    'message' => 'invalid input',
                    'code' => 101,
                    'errors' => $this->form_validation->error_array()
                ), 401);
            } else {
                $this->load->helper('message');
                $phone = $this->post('no_telepon');
                $otp = $this->post('otp');
                if ($otp == null) {
                    $otp = "123456";
                }
                $reffId = $this->post('reff_id');
                $checkOtp = $this->checkOTP($otp, $phone, null, $token->channelId);

                if ($checkOtp->responseCode == '00') {
                    $this->response(array(
                        'status' => 'success',
                        'message' => 'OTP Valid',
                    ), 200);
                } else {
                    $response = array(
                        'status' => 'error',
                        'message' => 'OTP tidak ditemukan',
                        'code' => 102
                    );
                    $this->set_response($response, REST_Controller::HTTP_BAD_REQUEST);
                }
            }
        } else {
            $this->errorUnAuthorized();
        }
    }

    function info_product_get()
    {
        $productInfo = $this->main_model->getProductInfo();
        echo $productInfo;
    }

    function info_cached_data_post()
    {
        log_message('debug', 'Start : ' . __FUNCTION__ . ' ' . json_encode($this->post()));

        $token = $this->getToken();

        if (!$token) {
            return $this->errorUnAuthorized();
        }

        $results = $this->main_service->getInfoCachedData($token);

        log_message('debug', 'End : ' . __FUNCTION__ . ' ' . json_encode($results));

        return $this->send_response($results['status'], $results['message'], $results['data'], $results['code']);
    }
}
