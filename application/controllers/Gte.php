<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once 'CorePegadaian.php';

class Gte extends CorePegadaian
{
    public function __construct()
    {
        parent::__construct();
        $models = [
            'User',
            'Otp',
            'BankModel',
            'PaymentModel',
            'NotificationModel',
            'EmasModel',
            'ConfigModel',
            'GteModel',
            'ProductMasterModel',
            'MasterModel'
        ];
        
        $this->load->model($models);
        $this->load->library('form_validation');
        $this->load->helper('message');
        $this->load->service('Gte_service');
        $this->load->helper('Pegadaian');
        
        //Log request and post
        $CI =& get_instance();
        $uri = $CI->uri->uri_string();
        
        $postParam = $CI->input->post();
        $getParam = $CI->input->get();
        
//        log_message('debug', 'REQUEST INFO');
//        log_message('debug', 'REQUEST INFO URI: ' . $uri);
//        log_message('debug', 'REQUEST INFO POST PARAM:'. json_encode($postParam));
//        log_message('debug', 'REQUEST INFO GET PARAM:'. json_encode($getParam));
    }

    function check_promo_get()
    {
        $token = $this->getToken();
        if ($token) {
            $this->form_validation->set_data([
                'promo' => $this->query('promo')
            ]);
            
            $this->form_validation->set_rules('promo', 'promo', 'required');
            
            if ($this->form_validation->run() == false) {
                $this->set_response([
                    'status' => 'error',
                    'code' => 201,
                    'message' => 'Input error',
                    'errors' => $this->form_validation->error_array()
                ]);
            } else {
                $promo = $this->query('promo');
                $data = $this->GteModel->checkPromo($promo);
                if (count($data) == 0) {
                    $this->set_response([
                        'status' => 'error',
                        'code' => 102,
                        'message' => 'Promo tidak ditemukan'
                    ]);
                } else {
                    $this->set_response([
                        'status' => 'success',
                        'message' => '',
                        'data' => $data
                    ]);
                }
            }
        } else {
            $this->errorUnAuthorized();
        }
    }

    public function new_simulasi_gte_post()
    {
        $token = $this->getToken();
        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', 'response ' . __FUNCTION__ . 'UnAuthorized');
            return;
        }

        $this->form_validation->set_rules('amount', 'amount', 'callback__valGteAmount');
        $this->form_validation->set_rules('gram', 'gram', 'callback__valGteGram['.$this->post('norek', 'norek', 'required').']');
        $this->form_validation->set_rules('norek', 'norek', 'callback__valGteNorekEmas');
        $this->form_validation->set_rules('tenor', 'tenor', 'callback__valGteTenor');

        if (!$this->form_validation->run()) {
            $validation = Pegadaian::showErrorValidation($this->form_validation->error_array());
            log_message('debug', 'END ' . __FUNCTION__ . ' => ' . json_encode($validation));
            return $this->send_response('error', 'Validation Exception', $validation, '101');
        }

        $data = $this->post();

        //get User Cif berdasarkan Id
        $data['cif'] = $this->User->getUserCif($token->id);
        
        $response = $this->Gte_service->simulasiGte($data);
        log_message('debug', 'response ' . __FUNCTION__ . ' request with response: ' . json_encode($response));

        $dataResponse = $this->send_response($response['status'], $response['message'], $response['data'], $response['code']);

        log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($dataResponse));
        return;
    }

    function list_tenor_get()
    {

        log_message('Debug', 'Start Of ' . __FUNCTION__ . '=>' . json_encode($this->query()));

        $token = $this->getToken();

        if (!$token) {
            $this->errorUnAuthorized();
        }

        $item = array(
            array("label" => "30 hari", "value" => 30),
            array("label" => "60 hari", "value" => 60),
            array("label" => "90 hari", "value" => 90),
            array("label" => "120 hari", "value" => 120),
        );

        $dataResponse = $this->send_response('success', 'Data List Tenor', $item, '');

        log_message('Debug', 'End Of ' . __FUNCTION__ . 'success' . json_encode($dataResponse));
        return;
    }

    function _valGteNorekEmas($noRekEmas)
    {
        if (empty($noRekEmas)) {
            $this->form_validation->set_message('_valGteNorekEmas', 'Anda belum memilih nomor rekening.');
            return false;
        }

        if (strlen($noRekEmas) < 16) {
            $this->form_validation->set_message('_valGteNorekEmas', 'Nomor rekening harus terdapat minimal 16 digit');
            return false;
        }

        if (!is_numeric($noRekEmas)) {
            $this->form_validation->set_message('_valGteNorekEmas', 'Nomor rekening harus diisi dengan angka');
            return false;
        }
        return true;
    }

    function _valGteTenor($tenor)
    {
        if (empty($tenor)) {
            $this->form_validation->set_message('_valGteTenor', 'Anda belum memilih tenor.');
            return false;
        }

        if (!is_numeric($tenor)) {
            $this->form_validation->set_message('_valGteTenor', 'Tenor harus diisi dengan angka');
            return false;
        }

        return true;
    }

    public function simulasi_post()
    {
        $this->form_validation->set_rules('amount', 'amount', 'required');
        $this->form_validation->set_rules('gram', 'gram', 'required');
        $this->form_validation->set_rules('norek', 'norek', 'required');
        $this->form_validation->set_rules('tenor', 'tenor', 'required');

        if ($this->form_validation->run()) {
            $data = $this->post();

            //get productCode
            $productCode = $this->ProductMasterModel->get_productName('GADAI TABUNGAN EMAS');
            $data['productCode'] = $productCode->productCode;

            //getCif
            $user = $this->getToken();
            $data['cif'] = $this->User->getUserCif($user->id);

            $data['clientId'] =  $this->config->item('core_post_username');
            $data['channelId'] = $this->config->item('core_client_id');

            $data['jenisTransaksi'] = "OP";

            if ($data['gram'] != "0") {
                $data['tipe'] = "1";
            } elseif ($data['amount'] != "0") {
                $data['tipe'] = "2";
            }

            $path = '/simulasi/gte';
            $req = $this->coreRequest($path, $data, "Simulasi GTE");

            if ($req->responseCode == "00") {
                $this->set_response([
                    'response_code' => $req->responseCode,
                    'status' => 'success',
                    'message' => $req->responseDesc,
                    'data' => json_decode($req->data)
                ], 200);
            } else {
                $this->set_response([
                    'response_code' => $req->responseCode,
                    'status' => 'error',
                    'message' => $req->responseDesc,
                    'data' => json_decode($req->data)
                ], 201);
            }
        } else {
            $error = "";
            foreach ($this->form_validation->error_array() as $value) {
                $error = $value;
                break;
            }

            $this->set_response([
                'status' => 'error',
                'message' => $error,
                'code' => 101
            ]);
        }
    }

    function _valGteGram($gram, $norek)
    {
        if (empty($gram) && !is_numeric($gram)) {
            $this->form_validation->set_message('_valGteGram', 'Input Gram tidak boleh kosong.');
            return false;
        }

        //get data min dan max dari Master Model
        $min_gadai_gram = $this->MasterModel->getHargaGadaiEmas('Min_Gte_Gram');
        $max_gadai_gram = $this->MasterModel->getHargaGadaiEmas('Max_Gte_Gram');
        $gte_min_sisa_saldo = $this->MasterModel->getHargaGadaiEmas('gte_min_sisa_saldo');

        //get Rekening berdasarkan Cif
        $daftarRekening = $this->detailTabunganEmas($norek);

        if ($daftarRekening->responseCode == '12') {
            $this->form_validation->set_message('_valGteGram', 'Saldo emas tidak mencukupi.');
            return false;
        }

        //get Saldo efektif dari rekening
        $saldoEfektif = json_decode($daftarRekening->data)->saldoEfektif ?? '';

        // Validasi Ketika Inputan Lebih besar dari Saldo Rekening Emas
        if (empty($saldoEfektif)) {
            $this->form_validation->set_message('_valGteGram', 'Saldo emas tidak ditemukan.');
            return false;
        }

        //  Validasi Ketika Saldo Rekening Emas Kurang
        if ($saldoEfektif < $min_gadai_gram) {
            $this->form_validation->set_message('_valGteGram', 'Saldo anda tidak mencukupi.');
            return false;
        }

        //Validasi jika inputan gram lebih besar dari saldo emas
        if ($gram > $saldoEfektif) {
            $this->form_validation->set_message('_valGteGram', 'Saldo anda tidak mencukupi.');
            return false;
        }

        // Validasi Min gram
        if ($gram < $min_gadai_gram && $gram != '0') {
            $this->form_validation->set_message('_valGteGram', 'Minimal jumlah Gadai Tabungan Emas ' . $min_gadai_gram . ' gram');
            return false;
        }

        // Validasi Max gram
        if ($gram > $max_gadai_gram) {
            $this->form_validation->set_message('_valGteGram', 'Maksimal jumlah Gadai Tabungan Emas ' . $max_gadai_gram . ' gram');
            return false;
        }

        $hitung = $saldoEfektif - $gram;

        //  Validasi ketika Saldo Emas harus mengendap minimal 0.1
        if ($hitung < $gte_min_sisa_saldo) {
            $this->form_validation->set_message('_valGteGram', 'Saldo mengendap minimal ' . $gte_min_sisa_saldo . ' gram.');
            return false;
        }

    
        return true;
    }

    function _valGteAmount($amount)
    {
        if (empty($amount) && !is_numeric($amount)) {
            $this->form_validation->set_message('_valGteAmount', 'Input Rupiah tidak boleh kosong.');
            return false;
        }

        //get data min dan max dari Master Model
        $min_gadai_amount = $this->MasterModel->getHargaGadaiEmas('Min_Gte_Amount');
        $max_gadai_amount = $this->MasterModel->getHargaGadaiEmas('Max_Gte_Amount');

        // Validasi Min amount
        if ($amount < $min_gadai_amount && $amount != '0') {
            $this->form_validation->set_message('_valGteAmount', 'Minimal jumlah Gadai Tabungan Emas Rp.' . number_format($min_gadai_amount, 0, ',', '.') . '/Transaksi');
            return false;
        }

        // Validasi Max amount
        if ($amount > $max_gadai_amount) {
            $this->form_validation->set_message('_valGteAmount', 'Maksimal jumlah Gadai Tabungan Emas Rp.' . number_format($max_gadai_amount, 0, ',', '.') . '/Transaksi');
            return false;
        }

        //Jika Kelipatan 10000 true jika kelipatan tidak 10000 false
        if ($amount != ($amount % 10000 == 0 && $amount != '0')) {
            $this->form_validation->set_message('_valGteAmount', 'Gadai Tabungan Emas harus kelipatan Rp. 10.000');
            return false;
        }

        return true;
    }
}
