<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once 'CorePegadaian.php';

/**
 * @property Transaction_service transaction_service
 */
class Transaction extends CorePegadaian
{
    private $auth;

    public function __construct()
    {
        parent::__construct();
        // init load library
        $this->load->library('form_validation');
        $this->form_validation->set_message('required', '{field} harus diisi.');
        // init load service
        $this->load->service('Transaction_service', 'transaction_service');
        $this->auth = $this->getToken();
    }

    function inquiry_post()
    {
        if (empty($this->auth)) {
            return $this->errorUnAuthorized();
        }

        $request = $this->post();
        log_message('debug', 'START ' . __FUNCTION__ . ' => ' . json_encode($request));

        $product = $request['product'] ?? "";
        $this->form_validation->set_rules('product', 'Produk', 'callback__validation_product');

        if ($this->form_validation->run() == false) {
            $validation = Pegadaian::showErrorValidation($this->form_validation->error_array());
            log_message('debug', 'END ' . __FUNCTION__ . ' => ' . json_encode($validation));

            return $this->send_response('error', 'Validation Exception', $validation, '101');
        }
        
        $response = $this->{"_inquiry".ucfirst($product)}($request);
        return $this->send_response($response); 
    }

    function submit_post()
    {
        if (empty($this->auth)) {
            return $this->errorUnAuthorized();
        }

        $request = $this->post();

        log_message('debug', 'START ' . __FUNCTION__ . ' => ' . json_encode($request));

        $this->form_validation->set_rules('product', 'Produk', 'required');
        $this->form_validation->set_rules('trx_id', 'Transaction ID', 'required');
        $this->form_validation->set_rules('pin', 'PIN', 'required');

        if ($this->form_validation->run() == false) {
            $validation = Pegadaian::showErrorValidation($this->form_validation->error_array());
            log_message('debug', 'END ' . __FUNCTION__ . ' => ' . json_encode($validation));
            return $this->send_response('error', 'Validation Exception', $validation, '101');
        }

        $response = $this->transaction_service->submit($this->auth->id, $request);

        log_message('debug', 'END ' . __FUNCTION__ . ' => ' . json_encode($response));

        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    function inquiryMtonline_post()
    {
        if (empty($this->auth)) {
            return $this->errorUnAuthorized();
        }
        
        $request = $this->post();
        log_message('debug', 'START ' . __FUNCTION__ . ' => ' . json_encode($request));
        
        $this->form_validation->set_rules('product', 'Produk', 'callback__validation_product');
        $this->form_validation->set_rules('account_number', 'No Rekening Titipan Emas', 'required');
        $this->form_validation->set_rules('amount', 'Jumlah Pinjaman', 'required');
        $this->form_validation->set_rules('code_outlet', 'Kode Outlet', 'required');
        $this->form_validation->set_rules('total_taksiran', 'Total Taksiran', 'required');
        $this->form_validation->set_rules('product_code', 'Product Code', 'required');
        $this->form_validation->set_rules('bank_id', 'Bank ID', 'required');
        $this->form_validation->set_rules('payment_method', 'Metode Pembayaran', 'required');

        if ($this->form_validation->run() == false) {
            $validation = Pegadaian::showErrorValidation($this->form_validation->error_array());
            log_message('debug', 'END ' . __FUNCTION__ . ' => ' . json_encode($validation));
            return $this->send_response('error', 'Validation Exception', $validation, '101');
        }

        $response = $this->transaction_service->inquiry($this->auth->id, $request);

        log_message('debug', 'END ' . __FUNCTION__ . ' => ' . json_encode($response));

        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    // Validation Product
    function _validation_product($product)
    {
        $products = ['gtef', 'mtonline'];

        if (in_array($product, $products)) {
            return true;
        }

        $this->form_validation->set_message('_validation_product', 'Produk tidak ditemukan');

        return false;
    }

    private function _inquiryGtef($request)
    {
        $this->form_validation->set_rules('account_number', 'No Rekening Titipan Emas', 'required');
        $this->form_validation->set_rules('amount', 'Jumlah Pinjaman', 'required');
        $this->form_validation->set_rules('bank_id', 'Bank ID', 'required');
        $this->form_validation->set_rules('payment_method', 'Metode Pembayaran', 'required');
        $this->form_validation->set_rules('jenis_transaksi', 'Jenis Transaksi', 'required');

        $jenis_transaksi = $request['jenis_transaksi'] ?? null;
        if ($jenis_transaksi == "OP") {
            $this->form_validation->set_rules('tenor', 'Jangka Waktu', 'required');
        }

        if ($this->form_validation->run() == false) {
            $validation = Pegadaian::showErrorValidation($this->form_validation->error_array());
            $response['status'] = 'error';
            $response['message'] = 'Validation Exception';
            $response['data'] = $validation;
            $response['code'] = '101';
            
            return $response;
        }

        $response = $this->transaction_service->inquiry($this->auth->id, $request);
        return $response;
    }

    private function _inquiryMtonline($request)
    {
        $this->form_validation->set_rules('account_number', 'No Rekening Titipan Emas', 'required');
        $this->form_validation->set_rules('amount', 'Jumlah Pinjaman', 'required');
        $this->form_validation->set_rules('code_outlet', 'Kode Outlet', 'required');
        $this->form_validation->set_rules('total_taksiran', 'Total Taksiran', 'required');
        $this->form_validation->set_rules('product_code', 'Product Code', 'required');
        $this->form_validation->set_rules('bank_id', 'Bank ID', 'required');
        $this->form_validation->set_rules('payment_method', 'Metode Pembayaran', 'required');

        if ($this->form_validation->run() == false) {
            $validation = Pegadaian::showErrorValidation($this->form_validation->error_array());
            $response['status'] = 'error';
            $response['message'] = 'Validation Exception';
            $response['data'] = $validation;
            $response['code'] = '101';

            return $response;
        }

        $response = $this->transaction_service->inquiry($this->auth->id, $request);
        return $response;
    }
}
