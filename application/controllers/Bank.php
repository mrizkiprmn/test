<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once 'CorePegadaian.php';

class Bank extends CorePegadaian
{

    public function __construct()
    {
        parent::__construct();
        $models = [
            'User',
            'BankModel',
            'MasterModel',
            'NotificationModel',
            'ConfigModel'
        ];
        $this->load->model($models);
        $this->load->library('form_validation');
        $this->load->helper('message');
        $this->load->service('Payment_service');
    }

    /**
     * Digunakan untuk inquiry data rekening ke Bank
     */
    function inquiry_post()
    {
        // get token
        $token = $this->getToken();

        // token validation
        if (!$token) {
            $this->errorUnAuthorized();
            return;
        }

        $setData = array(
            'kode_bank'       => $this->post('kode_bank'),
            'no_rekening'     => $this->post('no_rekening'),
            'nama_rekening'   => $this->post('nama_rekening'),
        );

        $this->form_validation->set_data($setData);
        $this->form_validation->set_rules('kode_bank', 'kode_bank', 'required');
        $this->form_validation->set_rules('no_rekening', 'no_rekening', 'required');
        $this->form_validation->set_rules('nama_rekening', 'nama_rekening', 'required');

        // request validation
        if ($this->form_validation->run() == false) {
            $this->set_response(array(
                'status'  => 'error',
                'message' => 'Invalid Input',
                'code'    => 101,
                'errors'  => $this->form_validation->error_array(),
            ), 200);
            return;
        }

        $kode_bank       = $this->post('kode_bank');
        $no_rekening     = $this->post('no_rekening');
        $nama_rekening   = $this->post('nama_rekening');

        // rekening validation
        if ($this->BankModel->isBankExist($token->id, $kode_bank, $no_rekening)) {
            return $this->send_response('error', 'Nomor Rekening Sudah Terdaftar', 'Silahkan masukan nomor rekening lain', 101);
        }

        $req_bank = array(
            'kodeBank'           => $kode_bank,
            'noRekeningBank'     => $no_rekening,
            'namaRekeningBank'   => $nama_rekening,
        );

        log_message('debug', 'E-KYC bank/inquiry request Bank', $req_bank);
        // send data inquiry bank
        $res_bank = $this->checkRekBank($req_bank);
        log_message('debug', 'E-KYC bank/inquiry response Bank', $res_bank);

        switch ($res_bank->responseCode) {
            case '00':
                $data = json_decode($res_bank->data);
                $response                    = (array) $data;
                $response['user_AIID']       = $token->id;
                $response['kodeBank']        = $kode_bank;
                $response['noRekening']      = $data->noRekeningBank;
                $response['namaRekening']    = $data->namaRekeningBank;
                $response['ccy']             = $data->ccy;

                $this->send_response('success', 'Inquiry rekening berhasil', $response);
                break;
            case '68':
                $this->send_response('error', 'Koneksi ke Bank bermasalah', $res_bank);
                break;
            case '12':
                $this->send_response('error', 'Nomor Rekening Tidak Sesuai', $res_bank);
                break;
            case '15':
                $this->send_response('error', 'Nama Pemilik Rekening Tidak Sesuai', $res_bank);
                break;
            default:
                $this->send_response('error', 'Internal Server Error', $res_bank);
        }

        return;
    }

    /**
     * Digunakan untuk simpan data rekening Bank
     * Sebelumnya dilakukan validasi PIN
     */
    function save_post()
    {
        // get token
        $token = $this->getToken();

        // token validation
        if (!$token) {
            $this->errorUnAuthorized();
            return;
        }

        $setData = array(
            'pin'             => $this->post('pin'),
            'kode_bank'       => $this->post('kode_bank'),
            'no_rekening'     => $this->post('no_rekening'),
            'nama_rekening'   => $this->post('nama_rekening'),
        );

        $this->form_validation->set_data($setData);
        $this->form_validation->set_rules('pin', 'pin', 'required');
        $this->form_validation->set_rules('kode_bank', 'kode_bank', 'required');
        $this->form_validation->set_rules('no_rekening', 'no_rekening', 'required');
        $this->form_validation->set_rules('nama_rekening', 'nama_rekening', 'required');

        // request validation
        if ($this->form_validation->run() == false) {
            $this->send_response('error', 'Invalid Input', $this->form_validation->error_array());
            return;
        }

        $pin             = $this->post('pin');
        $kode_bank       = $this->post('kode_bank');
        $no_rekening     = $this->post('no_rekening');
        $nama_rekening   = $this->post('nama_rekening');

        // pin validation
        if ($this->User->isValidPIN($token->id, $pin) == false) {
            $this->send_response('error', 'Invalid Pin', '');
            return;
        }

        $data = array(
            'user_AAID'      => $token->id,
            'id_bank'        => 0,
            'nomor_rekening' => $no_rekening,
            'kode_bank'      => $kode_bank,
            'nama_pemilik'   => $nama_rekening
        );

        // save rekening bank
        $id = $this->BankModel->addRekeningBank($data);
        $response = $this->BankModel->getRekeningBank($id);
        // kirim notifikasi berahasil menyimpan rekening bank
        $this->_send_notification($data, $token);
        $this->send_response('success', 'Rekening berhasil disimpan', $response);
        return;
    }

    function _send_notification($data, $token)
    {
        // mapping data notif
        $nama_bank = $this->BankModel->getNamaBank($data['kode_bank']);
        $dataNotif = [
            'email' => $token->email,
            'user_id' => $data['user_AAID'],
            'namaNasabah' => $token->nama,
            'rekening' => [[
                'namaBank' => $nama_bank,
                'norekBank' => $data['nomor_rekening'],
                'namaNasabahBank' => $data['nama_pemilik']
            ]]
        ];

        // get template data for profile bank
        $template = Message::generateBankNotif($dataNotif);
        log_message('debug', 'start sending notification with => ', $dataNotif);

        // store to notification model
        $idNotif = $this->NotificationModel->add(
            $data['user_AAID'],
            NotificationModel::TYPE_PROFILE,
            NotificationModel::CONTENT_TYPE_HTML,
            "Profile Info",
            "Rekening Bank",
            $template['email'],
            $template['mobile'],
            "profile_bank"
        );

        // send notif to user apps
        Message::sendFCMNotif(
            $this->User->getFCMToken($data['user_AAID']),
            [
                "id" => $idNotif,
                "tipe" => "addRekeningBank",
                "title" => "Rekening Berhasil Ditambah",
                "tagline" => "Rekening Bank " . $nama_bank . " " . $data['nomor_rekening'] . " berhasil ditambah",
                "action_url" => "addRekeningBank"
            ]
        );

        // send email notif to user
        Message::sendEmail($token->email, "Penambahan Rekening Bank", $template['email']);
        log_message('debug', 'end sending notification');
    }

    function register_auto_debit_post()
    {
        $this->form_validation->set_message('required', '{field} harus diisi.');

        $token = $this->getToken();

        if (!$token) {
            return $this->errorUnAuthorized();
        }

        $this->form_validation->set_data($this->post());

        $this->form_validation->set_rules('jenis_transaksi', 'Jenis Transaksi', 'callback__validation_jenisTransaksi');
        $this->form_validation->set_rules('nama_pemilik_bank', 'Nama Pemilik Bank', 'required');
        $this->form_validation->set_rules('norek_bank', 'Nomor Rekening Bank', 'callback__validation_norekBank');
        $this->form_validation->set_rules('kode_bank', 'Kode Bank', 'required');
        $this->form_validation->set_rules('no_kredit', 'Nomor Kredit', 'required');
        $this->form_validation->set_rules('no_hp', 'Nomor Handphone', 'required');
        $this->form_validation->set_rules('jumlah_debit', 'Jumlah Pendebitan', 'callback__validation_openDebit');
        $this->form_validation->set_rules('jangka_waktu_debit', 'Jangka Waktu Pendebitan', 'required');
        $this->form_validation->set_rules('tanggal_debit', 'Tanggal Pendebitan', 'required');

        if ($this->form_validation->run() == false) {
            $validation = Pegadaian::showErrorValidation($this->form_validation->error_array());
            return $this->send_response('error', 'Data tidak lengkap', $validation, '101');
        }

        $response = $this->Payment_service->regAutoDebit($this->post(), $token);
        return $this->send_response($response);
    }

    function send_otp_auto_debit_post()
    {
        $this->form_validation->set_message('required', '{field} harus diisi.');

        $token = $this->getToken();
        
        if (!$token) {
            return $this->errorUnAuthorized();
        }

        $this->form_validation->set_data($this->post());

        $this->form_validation->set_rules('id_reg', 'Id Registrasi', 'required');
        $this->form_validation->set_rules('otp', 'Otp', 'required');

        if ($this->form_validation->run() == false) {
            $validation = Pegadaian::showErrorValidation($this->form_validation->error_array());
            return $this->send_response('error', 'Data tidak lengkap', $validation, '101');
        }

        $response = $this->Payment_service->sendOtpAutoDebit($this->post(), $token);
        return $this->send_response($response);
    }

    function list_active_debit_get()
    {
        $token = $this->getToken();
        if (!$token) {
            return $this->errorUnAuthorized();
        }

        $response = $this->Payment_service->listActiveAutoDebit($token);
        return $this->send_response($response);
    }

    function check_status_autodebit_get()
    {
        $token = $this->getToken();
        if (!$token) {
            return $this->errorUnAuthorized();
        }
    
        $response = $this->Payment_service->checkStatusAutoDebit($token);
        return $this->send_response($response);
    }

    function list_transaksi_autodebit_get()
    {
        $token = $this->getToken();
        if (!$token) {
            return $this->errorUnAuthorized();
        }

        $response = $this->Payment_service->listJenisTransaksi($token);
        return $this->send_response($response);
    }

    function _validation_jenisTransaksi($jenis_transaksi)
    {
        if (empty($jenis_transaksi)) {
            $this->form_validation->set_message('_validation_jenisTransaksi', 'Pilih salah satu {field}');
            return false;
        }

        return true;
    }

    function _validation_norekBank($norek_bank)
    {
        if (empty($norek_bank)) {
            $this->form_validation->set_message('_validation_norekBank', '{field} harus diisi.');
            return false;
        }

        if (strlen($norek_bank) < 13) {
            $this->form_validation->set_message('_validation_norekBank', '{field} harus terdapat minimal 13 karakter');
            return false;
        }

        if (!is_numeric($norek_bank)) {
            $this->form_validation->set_message('_validation_norekBank', '{field} harus diisi dengan angka');
            return false;
        }

        return true;
    }

    function _validation_openDebit($jumlah_debit)
    {
        $jenis_transaksi = $this->input->post('jenis_transaksi', true);
        $no_kredit = $this->input->post('no_kredit', true);

        //Jika jenis Transaksi Beli Tabungan Emas min pendebitan beli emas 50.000 dan max pendebitan beli emas 10.000.000
        $min_autodebit_open_payment = $this->ConfigModel->whereByVariable('min_autodebit_topup_emas');
        $max_autodebit_open_payment = $this->ConfigModel->whereByVariable('max_autodebit_topup_emas');
        $min_amount = Pegadaian::currencyIdr($min_autodebit_open_payment->value);
        $max_amount = Pegadaian::currencyIdr($max_autodebit_open_payment->value);
        $product_code = substr($no_kredit, 7, 2);
        $is_product_code_emas = ($product_code == '62' && $jenis_transaksi == 'SL');

        if (empty($jumlah_debit)) {
            $this->form_validation->set_message('_validation_openDebit', '{field} harus diisi.');
            return false;
        }

        if (!is_numeric($jumlah_debit)) {
            $this->form_validation->set_message('_validation_openDebit', '{field} harus diisi dengan angka');
            return false;
        }

        if ($is_product_code_emas && $jumlah_debit < $min_autodebit_open_payment->value) {
            $this->form_validation->set_message('_validation_openDebit', "Minimal {field} $min_amount");
            return false;
        }

        if ($is_product_code_emas && $jumlah_debit > $max_autodebit_open_payment->value) {
            $this->form_validation->set_message('_validation_openDebit', "Maksimal {field} $max_amount");
            return false;
        }

        return true;
    }
}
