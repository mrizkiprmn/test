<?php

defined('BASEPATH') or exit('No direct script access allowed');

require_once 'CorePegadaian.php';

class Version extends CorePegadaian
{
    function index_get()
    {
        echo json_encode($this->config->item('versions'));
    }
}
