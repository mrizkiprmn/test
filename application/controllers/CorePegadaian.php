<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once APPPATH . 'libraries/REST_Controller.php';
require_once APPPATH . 'libraries/ExpiredException.php';
require_once APPPATH . 'libraries/BeforeValidException.php';

use Restserver\Libraries\REST_Controller;
use \Curl\Curl;

/**
 * @property RestSwitching_service rest_switching_service
 * @property User User
 */
class CorePegadaian extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('User');
        $this->load->service('RestSwitching_service', 'rest_switching_service');
        //Log request and post
        $CI =& get_instance();
        $request_payload = $CI->input->get();

        if (empty($request_payload)) {
            $request_payload = $CI->input->post();
        }

        log_message('debug', 'Request Payload', $request_payload);
    }
    
    /**
     * Mendapatkan access token core pegadaian
     * Token digunakan untuk inquiry data ke pegadaian yang membutuhkan authorization
     * @return string
     */
    public function getCoreToken()
    {
        return $this->rest_switching_service->token();
    }
   
    public function coreRequest($path, $data, $operationId)
    {
        
        //Log message
        log_message('debug', $operationId.': '.json_encode($data));
        log_message('debug', 'CALL TO '.$path);
        $tokenChar = $this->getCoreToken();
        $curl = new Curl();
        $curl->setHeader('Content-Type', 'application/json');
        $curl->setHeader('Authorization', 'Bearer ' . $tokenChar);
        $curl->setConnectTimeout(100);
        $curl->setTimeout(100);
        $curl->post($this->config->item('core_API_URL').$path, $data);
        log_message('debug', 'FIRE : '.$this->config->item('core_API_URL').$path);
        log_message('debug', 'Response: '.$operationId.': '.json_encode($curl->response));
        if ($curl->error) {
            log_message('error', 'CORE CONNECT '.$operationId.': '.$curl->errorMessage);
            return (object) array(
                'responseCode' =>  $curl->errorCode,
                'responseDesc' => $curl->errorMessage
            );
        } else {
            //Cek jika responseCode == 00 dan tidak mempunyai data
            $resp = $curl->response;
            $code = $resp->responseCode ?? null;

            if ($code == '00' && !isset($resp->data)) {
                return (object) array(
                    'responseCode' =>  $curl->errorCode,
                    'responseDesc' => $curl->errorMessage
                );
            }
            log_message('debug', 'Response: '.$operationId.': '.json_encode($curl->response));
            return $curl->response;
        }
    }


    public function vaRequest($path, $data, $operationId)
    {
        log_message('debug', 'VA_REQUEST: '.$operationId.': '.json_encode($data));
        $tokenChar = $this->getCoreToken();
        if ($tokenChar == null) {
            return (object) array(
                //set response code selain 00 untuk menandakan error
                'responseCode' => '00000',
                'responseDesc' => 'Terjadi kesalahan mohon coba beberapa saat lagi.',
                'message' => 'Terjadi kesalahan mohon coba beberapa saat lagi.',
                
            );
        }
        
        $dataSend = json_encode($data);
        
        $process = curl_init($this->config->item('core_VA_API_URL').$path);
        curl_setopt($process, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Bearer ' . $tokenChar
        ));

        curl_setopt($process, CURLOPT_HEADER, 0);
        curl_setopt($process, CURLOPT_TIMEOUT, 30);
        curl_setopt($process, CURLOPT_POST, 1);
        curl_setopt($process, CURLOPT_PROXY, '');
        curl_setopt($process, CURLOPT_POSTFIELDS, $dataSend);
        curl_setopt($process, CURLOPT_HTTPPROXYTUNNEL, false);
        curl_setopt($process, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($process, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($process, CURLOPT_SSL_VERIFYPEER, 0);

        curl_setopt($process, CURLOPT_RETURNTRANSFER, 1);
        $return = curl_exec($process);
        
        
        if ($return == false) {
            //Jika false return object dengan property responseCode isi selain 00
            //code 00 dgunakan sebagai response sukses oleh core pegadaian
            log_message('error', 'Curl error: ' . curl_error($process));
            return (object) array(
                'responseCode' => '000000',
                'responseDesc' => 'Terjadi kesalahan mohon coba beberapa saat lagi.',
                'message' => 'Terjadi kesalahan mohon coba beberapa saat lagi.',
            );
        } else {
            // Jika sukses, data akan berupa string, lakukan proses decode
            $responseData = json_decode($return);
            log_message('debug', 'Response VA_REQUEST '.$operationId.': '.json_encode($responseData));
            if (!isset($responseData->responseCode)) {
                $responseData = (object) [
                    'responseCode' => '666',
                    'message' => 'Internal server error. Please try later.'
                ];
            }

            /**
             *  Handle jika dari core tidak menyediakan field vaNumber
             */
            if (isset($responseData->data)) {
                $_tempResponseData = json_decode($responseData->data, true);
                if (!isset($_tempResponseData['vaNumber'])) {
                    $_tempResponseData['vaNumber'] = $_tempResponseData['virtualAccount'];
                }
                $responseData->data = json_encode($_tempResponseData);
            }
            return $responseData;
        }
        curl_close($process);
    }

    function getToken()
    {
        $headers = $this->input->request_headers();

        if (!Authorization::tokenIsExist($headers)) {
            return $this->errorForbbiden();
        }

        $token = Authorization::validateToken($headers['Authorization']);

        if (isset($token) && isset($token->version) && $token->version == '3') {
            return $token;
        }

        return false;
    }

    function authCore()
    {
        $headers = $this->input->request_headers();
        if (Authorization::tokenIsExist($headers)) {
            $corePassword = $this->config->item('core_password');
            if ($headers['Authorization'] == $corePassword) {
                return true;
            }
            
            return false;
        }
        
        return false;
    }

    function authGoldcard()
    {
        $headers = $this->input->request_headers();
        if (!Authorization::tokenIsExist($headers)) {
            return false;
        }
        $goldcardPassword = getenv('GOLDCARD_PASSWORD');
        if ($headers['Authorization'] != $goldcardPassword) {
            return false;
        }
        return true;
    }

    /**
     * Check admin token
     * @param  $token string admin token
     * @return
     */
    public function checkAdminToken($token)
    {
        $this->load->model('AdminModel');
        $token = $this->AdminModel->getAdminToken($token);
       
        return $token;
    }

    function errorUnAuthorized()
    {
        $response = [
            'status' => 401,
            'message' => 'Unauthorized',
        ];

        $class = debug_backtrace()[0]['object']->router->method ?? '';

        log_message('debug', 'response authorize endpoint: ' . $class . ' data: ' . json_encode($response));

        return $this->set_response($response, 401);
    }

    function errorForbbiden()
    {
        $response = [
            'status' => 403,
            'message' => 'Forbidden',
        ];
        $this->set_response($response, 403);
        return;
    }

    function dbTransError($operationId)
    {
        log_message('error', 'DB Transaction ERROR '.$operationId);
        $this->set_response(array(
            'status' => 'error',
            'message' => 'System Maintenance, mohon coba beberapa saat lagi!',
            'code' => 104
        ), 200);
        return;
    }

    
    /**
     * Mendapatkan informasi harga emas dari Pegadaian Core
     */
    public function hargaEmas($channelId = '6017')
    {
        $url = '/param/hargaemas/';
        
        $data = array(
            "channelId" => $channelId,
            "clientId" => $this->config->item('core_post_username'),
            "flag" => "K"
        );
        return $this->coreRequest($url, $data, 'HargaEmas');
    }
    
    /**
     *
     * @param string $nama
     * @param string $ibuKandung
     * @param string $tanggalLahir
     * @param string $flag
     * @param string $channelId
     * @return void
     */
    function checkCustomer($nama, $ibuKandung, $tanggalLahir, $flag, $channelId = '6017')
    {
        $path = '/customer/check';
        $data = array(
            'ibuKandung' => $ibuKandung,
            'namaNasabah' => $nama,
            'tanggalLahir' => $tanggalLahir,
            'clientId' => $this->config->item('core_post_username'),
            'channelId' => $channelId,
            'flag' => $flag
        );
        log_message('debug', 'Data check customer '.json_encode($data));
        return $this->coreRequest($path, $data, 'CheckCustomer');
    }

    function aktifasiTransaksi($data)
    {
        $path = '/customer/activation';
        $data['clientId'] = $this->config->item('core_post_username');
        log_message('debug', 'Aktifasi Transaksi '.json_encode($data));
        return $this->coreRequest($path, $data, 'Aktifasi Transaksi');
    }
        
    
    /*
     * Cek info customer berdasarkan code CIF
     */
    public function check_cif($cif, $channelId = '6017')
    {
        $url = '/customer/inquiry';
        $data = array(
            "cif" => $cif,
            "channelId" => $channelId,
            "clientId" => $this->config->item('core_post_username')
        );
        return $this->coreRequest($url, $data, 'CheckCIF');
    }
    
    /**
     * Mengirim OTP melalui core API
     * @param string $phone
     * @param string $reffId
     * @return mixed
     */
    public function sendOTP($phone, $reffId, $requestType = null, $channelId = '6017')
    {
        $data = array(
            'noHp' => $phone,
            "channelId" => $channelId,
            "clientId" => $this->config->item('core_post_username'),
        );
        
        if ($requestType == null) {
            $data['requestType'] = 'register';
        } else {
            $data['requestType'] = $requestType;
        }
        
        $url = '/otp/send';
        
        return $this->coreRequest($url, $data, "SendOTP");
    }
    
    public function checkOTP($otp, $phone = null, $requestType = null, $channelId = '6017')
    {
        $data = array(
            "channelId" => $channelId,
            "clientId" => $this->config->item('core_post_username'),
            "token" => $otp
        );
        
        if ($requestType == null) {
            $data['requestType'] = "register";
            $data['noHp'] = $phone;
        } else {
            $data['requestType'] = $requestType;
            $data['noHp'] = $phone;
        }
        
        $url = '/otp/check';
        
        return $this->coreRequest($url, $data, "CheckOTP");
    }
        
    public function checkKYC($cif, $tanggal, $channelId = '6017')
    {
        $url = '/customer/kyc';
        $data = array(
           "channelId" => $channelId,
           "clientId" => $this->config->item('core_post_username'),
           "tanggalKYC" => $tanggal,
           "cif" => $cif
        );
        return $this->coreRequest($url, $data, "CheckKYC");
    }
    
    public function inquiryTabunganEmas($data)
    {
        $url = '/tabunganemas/inquiry';
        
        log_message('debug', "Inquiry Tabungan Emas Data: ".json_encode($data));
        return $this->coreRequest($url, $data, "InquiryTabunganEmas");
    }
    
    /**
     * Method untuk membuat billing virtual account BNI
     * @param string $amount
     * @param string $email
     * @param string $name
     * @param string $phone
     * @param string $flag
     * @param string $jenisTransaksi
     * @param string $keterangan
     * @param string $noRek
     * @param string $produkCode
     * @param string $trxId
     * @return void
     */
    public function createBillingVABNI($amount, $email, $name, $phone, $flag, $jenisTransaksi, $keterangan, $noRek, $produkCode, $trxId, $channelId = '6017', $idPromosi = '', $discountAmount = '')
    {
        $url = '/va/bni/createbilling';
        $data = array(
            'amount' => $amount,
            'channelId' => $channelId,
            'clientId' => $this->config->item('core_post_username'),
            'customerEmail' => $email,
            'customerName' => $name,
            'customerPhone' => $phone,
            'flag' => $flag,
            'jenisTransaksi' => $jenisTransaksi,
            'keterangan' => $keterangan,
            'productCode' => $produkCode,
            'norek' => $noRek,
            'trxId' => $trxId,
            'idPromosi' => $idPromosi,
            'discountAmount' => $discountAmount
        );
        log_message('debug', 'Create Billing BNI Data: '.json_encode($data));
        
        return $this->vaRequest($url, $data, 'CreateBillingVABNI');
    }
    
    public function mandiriClickPay(
        $amount,
        $bookingCode,
        $cardNumber,
        $jenisTransaksi,
        $keterangan,
        $noHp,
        $norek,
        $productCode,
        $tokenResponse,
        $trxId,
        $channelId = '6017',
        $idPromosi = null,
        $discountAmount = null
    ) {
        $url = '/clickpay/mandiri/payment';
        $data = array(
          "amount"=> $amount,
          "bookingCode" => $bookingCode,
          "cardNumber" => $cardNumber,
          "channelId" => $channelId,
          "clientId" => $this->config->item('core_post_username'),
          "jenisTransaksi" => $jenisTransaksi,
          "keterangan" => $keterangan,
          "noHp" => $noHp,
          "norek" => $norek ,
          "flag" => 'K',
          "productCode" => $productCode,
          "tokenResponse" => $tokenResponse,
          "trxId" => $trxId,
          "idPromosi" => $idPromosi,
          "discountAmount" => $discountAmount
        );
        
        log_message('debug', 'Mandiri Click Pay Data: '.json_encode($data));
        
        return $this->vaRequest($url, $data, 'MandiriClickPay');
    }


    public function checkBillingVABNI($trxId, $channelId = '6017')
    {
        $path = "/va/bni/inquirybilling";
        $data = array(
            'channelId' => $channelId,
            'clientId' => $this->config->item('core_post_username'),
            'trxId' => $trxId,
        );
        return $this->vaRequest($path, $data, 'InquiryBillingVABNI');
    }
    
    /**
     * Create billing VA MANDIRI dan BCA
     * @param array $data
     * @return void
     */
    public function createBillingPegadaian($data)
    {
        $data['clientId'] = $this->config->item('core_post_username');

        if ($data['kodeBank'] == '002') {
            /**
             * Transformasi beberapa field untuk briva
             */

            // Pastikan amount string
            $data['amount'] = (string) $data['amount'];

            if (isset($data['kodeProduk'])) {
                $data['productCode'] = $data['kodeProduk'];
                // Unset kode produk
                unset($data['kodeProduk']);
            }

            if (isset($data['reffSwitching'])) {
                $data['trxId'] = $data['reffSwitching'];
                // Unset reffSwitching
                unset($data['reffSwitching']);
            }

            // Finally unset kodeBank
            unset($data['kodeBank']);

            $url = '/va/bri/createbilling';
            return $this->vaRequest($url, $data, 'Create Billing Briva');
        } else {
            $url = '/va/create';
            return $this->coreRequest($url, $data, 'CreateBillingPegadaian');
        }
    }


    
    public function createBillingPermata($data)
    {
        $url = '/va/permata/createbilling';
        $data['clientId'] = $this->config->item('core_post_username');
        
        return $this->vaRequest($url, $data, 'CreateBillingPermata');
    }

    /**
     * Inquiry Rekening Koran Tabungan Emas
     * @param $noCIF
     * @param string $channelId
     * @return object
     */
    public function inquiryTabEmas($noCIF, $channelId = '6017', $flag = '')
    {
        $path = "/portofolio/tabemas";

        if (!empty($flag) && $flag == 'new') {
            $path = "/portofolio/pds/tabemas";
        }

        $data = [
            'channelId' => $channelId,
            'clientId' => $this->config->item('core_post_username'),
            'cif' => $noCIF
        ];

        $tabungan_emas = $this->coreRequest($path, $data, "InquiryTabunganEmas");

        // update data is_open_te
        $this->User->updateIsOpenTe($noCIF, (array) $tabungan_emas);

        return $tabungan_emas;
    }

    /**
     * Buka tabungan emas baru
     * @param string $amount
     * @param string $flag
     * @param string $ibuKandung
     * @param string $idKelurahan
     * @param string $jalan
     * @param string $jenisKelamin
     * @param string $kewarganegaraan
     * @param string $kodeCabang
     * @param string $namaNasabah
     * @param string $noHP
     * @param string $noIdentitas
     * @param string $reffSwitching
     * @param string $statusKawin
     * @param string $tanggalExpiredId
     * @param string $tanggalLahir
     * @param string $tempatLahir
     * @param string $tipeIdentitas
     * @return void
     */
    public function bukaTabunganEmas(
        $amount,
        $flag,
        $ibuKandung,
        $idKelurahan,
        $jalan,
        $jenisKelamin,
        $kewarganegaraan,
        $kodeCabang,
        $namaNasabah,
        $noHP,
        $noIdentitas,
        $reffSwitching,
        $statusKawin,
        $tanggalExpiredId,
        $tanggalLahir,
        $tempatLahir,
        $tipeIdentitas,
        $channelId = '6017',
        $cif = null,
        $kodeBankPembayar,
        $paymentMethod
    ) {
        $url = '/tabunganemas/open';
        
        $data = array(
            'channelId' => $channelId,
            'amount' => $amount,
            'clientId' => $this->config->item('core_post_username'),
            'cif' => $cif,
            'flag' => $flag,
            'ibuKandung' => $ibuKandung,
            'idKelurahan' => $idKelurahan,
            'jalan' => $jalan,
            'jenisKelamin' => $jenisKelamin,
            'kewarganegaraan' => $kewarganegaraan,
            'kodeCabang' => $kodeCabang,
            'namaNasabah' => $namaNasabah,
            'noHp' => $noHP,
            'noIdentitas' => $noIdentitas,
            'reffSwitching' => $reffSwitching,
            'statusKawin' => $statusKawin,
            'tanggalExpiredId' => $tanggalExpiredId,
            'tanggalLahir' => $tanggalLahir,
            'tempatLahir' => $tempatLahir,
            'tipeIdentitas' => $tipeIdentitas
        );
        
        log_message('debug', 'Data tabungan open emas '.json_encode($data));
        return $this->coreRequest($url, $data, "OpenTabunganEmas");
    }
    
    public function bukaTabunganEmas2($data)
    {
        $url = '/tabunganemas/open';
        if ($data['paymentMethod'] == 'GCASH') {
            $url = '/gcash/payment';
        }
        $data['clientId']=$this->config->item('core_post_username');
        log_message('debug', 'Data tabungan open emas2 '.json_encode($data));
        return $this->coreRequest($url, $data, "OpenTabunganEmas");
    }
    
    public function paymentTabungan($data)
    {
        $path = '/tabunganemas/payment';
        if ($data['paymentMethod'] == 'GCASH') {
            $path = '/gcash/payment';
        }

        $data['clientId'] = $this->config->item('core_post_username');
        log_message('debug', 'Data payment tabungan emas '.json_encode($data));
        return $this->coreRequest($path, $data, 'Payment tabungan emas');
    }
    
    function detailTabunganEmas($norek, $channelId = '6017')
    {
        $path = '/portofolio/dettabemas';
        $data = array(
            'channelId' => $channelId,
            'clientId' => $this->config->item('core_post_username'),
            'norek' => $norek
        );
        
        log_message('debug', 'Data detail tabungan emas '.json_encode($data));
        return $this->coreRequest($path, $data, 'DetailTabunganEmas');
    }
    
    function daftarTabunganEmas($cif, $channelId = '6017')
    {
        $path = '/portofolio/tabemas';
        $data = array(
            'channelId' => $channelId,
            'clientId' => $this->config->item('core_post_username'),
            'cif' => $cif
        );
        
        return $this->coreRequest($path, $data, "DaftarTabunganEmas");
    }
    
    /**
     * Endpoint untuk melakukan linking CIF dengan CORE
     * @param string $cif
     * @param string $ibuKandung
     * @param string $namaNasabah
     * @param string $tglLahir
     * @param string $username
     * @return void
     */
    function customerLink($cif, $ibuKandung, $namaNasabah, $tglLahir, $username, $createBy, $channelId = '6017')
    {
        $path = '/customer/link';
        $data = array(
            'channelId' => $channelId,
            'clientId' => $this->config->item('core_post_username'),
            'cif' => $cif,
            'ibuKandung' => $ibuKandung,
            'namaNasabah' => $namaNasabah,
            'tanggalLahir' => $tglLahir,
            'noHp' => $username,
            'username' => $username,
            'createBy'=>$createBy,
        );
        
        return $this->coreRequest($path, $data, "LINK CUSTOMER");
    }
    
    function inquiryGadai($amount, $jenisTransaksi, $norek, $channelId = '6017')
    {
        $path = '/gadai/inquiry';
        $data = array(
            'channelId' => $channelId,
            'clientId' => $this->config->item('core_post_username'),
            'amount' => $amount,
            'jenisTransaksi' => $jenisTransaksi,
            'norek' => $norek
        );
        
        return $this->coreRequest($path, $data, "INQUIRY GADAI");
    }
    
    /**
     * Endpoint untuk melakukan pembayaran payment gadai
     * @param array $data
     * @return void
     */
    function paymentGadai($data)
    {
        $path = '/gadai/payment';
        if ($data['paymentMethod'] == 'GCASH') {
            $path = '/gcash/payment';
        }
        $data['clientId'] = $this->config->item('core_post_username');
        return $this->coreRequest($path, $data, "PAYMENT GADAI");
    }
    
    /**
     * Endpooint API core untuk inquiry mikro
     * @param String $amount
     * @param String $jenisTransaksi
     * @param String $norek
     * @return void
     */
    function inquiryMikro($jenisTransaksi, $norek, $channelId = '6017')
    {
        $path = '/mikro/inquiry';
        $data = array(
            'channelId' => $channelId,
            'clientId' => $this->config->item('core_post_username'),
            'jenisTransaksi' => $jenisTransaksi,
            'norek' => $norek,
        );
        
        return $this->coreRequest($path, $data, "Inquiry Mikro");
    }
    
    /**
     * Endpoint untuk payment mikro
     * @param string $agenId
     * @param string $jenisTransaksi
     * @param string $norek
     * @param string $paymentMethod
     * @param string $walletId
     * @return void
     */
    function paymentMikro($data)
    {
        $path = '/mikro/payment';
        if ($data['paymentMethod'] == 'GCASH') {
            $path = '/gcash/payment';
        }
        $data['clientId'] = $this->config->item('core_post_username');
        return $this->coreRequest($path, $data, "Payment Mikro");
    }
    
    /**
     *
     * @param string $channelId
     * @return void
     */
    public function getHargaSTL($channelId = '6017')
    {
        $url = '/param/stl';
        $param = array(
            "channelId" => $channelId,
            "clientId" => $this->config->item('core_post_username'),
            "flag" => "K"
        );

        return $this->coreRequest($url, $param, 'GadaiSTL');
    }
    
    public function getPortofolioPinjaman($cif, $channelId = '6017')
    {
        $url = '/portofolio/pinjaman';
        $param = array(
            "channelId" => $channelId,
            "clientId" => $this->config->item('core_post_username'),
            "cif" => $cif
        );

        return $this->coreRequest($url, $param, 'PortofoloiPinjaman');
    }
    
    public function getPortofolioDetailPinjaman($norek, $channelId = '6017')
    {
        $url = '/portofolio/detpinjaman';
        $param = array(
            "channelId" => $channelId,
            "clientId" => $this->config->item('core_post_username'),
            "norek" => $norek
        );

        return $this->coreRequest($url, $param, 'DetailPortofolioPinjaman');
    }
    
    public function harga_mulia($channelId = '6017')
    {
        $url = '/param/hargamulia/';

        $data = array(
            "channelId" => $channelId,
            "clientId" => $this->config->item('core_post_username'),
            "flag" => "K"
        );
        return $this->coreRequest($url, $data, 'HargaEmas');
    }
    
    /**
     * Method untuk melakukan pembayaran
     * @param array $data
     * @return void
     */
    function inquiryMpoSeluler($data)
    {
        $url = '/mpo/inquiry';
        if (isset($data['paymentMethod'])) {
            if ($data['paymentMethod'] == 'GCASH') {
                $url = '/gcash/payment';
            }
        }
        $data['clientId'] = $this->config->item('core_post_username');
        
        return $this->coreRequest($url, $data, 'Inquiry MPO Seluler');
    }
    
    function paymentMpoSeluler($data)
    {
        $url = '/mpo/payment';
        if (isset($data['paymentMethod'])) {
            if ($data['paymentMethod'] == 'GCASH') {
                $url = '/gcash/payment';
            }
        }
        $data['clientId'] = $this->config->item('core_post_username');
        
        return $this->coreRequest($url, $data, 'Payment MPO');
    }
    
    /**
     * Method untuk melakukan aktifasi wallet pegadaian
     * @param array $data data yang dikirmkan
     * @return void
     */
    function aktivasiWallet($data)
    {
        $url = '/deposit/activation';
        $data['clientId'] = $this->config->item('core_post_username');
      
        return $this->coreRequest($url, $data, 'AktivasiWallet');
    }
    
    /**
     * Inquiry data wallet ke Core
     * @param array $data
     * @return void
     */
    function inquiryWallet($data)
    {
        $url = '/deposit/inquiry';
        $data['clientId'] = $this->config->item('core_post_username');
      
        return $this->coreRequest($url, $data, 'InquiryWallet');
    }
    
    function paymentWallet($data)
    {
        $url = '/deposit/payment';
        $data['clientId'] = $this->config->item('core_post_username');
      
        return $this->coreRequest($url, $data, 'PaymentWallet');
    }
    
    function checkRekBank($data)
    {
        $url = '/banking/check';
        $data['clientId'] = $this->config->item('core_post_username');
        $data['channelId'] = $this->config->item('core_client_id');
        return $this->coreRequest($url, $data, 'Check Rek Bank');
    }
    
    function inquiryTransferEmas($data)
    {
        $url = '/tabunganemas/transfer/inquiry';
        $data['clientId'] = $this->config->item('core_post_username');
        return $this->coreRequest($url, $data, 'Inquiry Transfer Emas');
    }
    
    function paymentTransferEmas($data)
    {
        $url = '/tabunganemas/transfer/payment';
        $data['clientId'] = $this->config->item('core_post_username');
        return $this->coreRequest($url, $data, 'Payment Transfer Emas');
    }
    
    function inquiryBuyBackEmas($data)
    {
        $url = '/tabunganemas/buyback/inquiry';
        $data['clientId'] = $this->config->item('core_post_username');
        return $this->coreRequest($url, $data, 'Inquiry Buyback Emas');
    }
    
    function paymentBuyBackEmas($data)
    {
        $url = '/tabunganemas/buyback/payment';
        $data['clientId'] = $this->config->item('core_post_username');
        return $this->coreRequest($url, $data, 'Payment Buyback Emas');
    }
    
    function saldoWallet($data)
    {
        $url = '/portofolio/wallet';
        $data['clientId'] = $this->config->item('core_post_username');
        return $this->coreRequest($url, $data, 'Saldo Wallet');
    }
    
    /**
     * Cek dan update saldo wallet user
     * @param string $channelId
     * @param string $userId
     * @param string $noHp
     * @return void
     */
    function updateWallet($channelId, $userId, $noHp)
    {
        $this->load->model('User');

        $checkData = array(
            'channelId' => $channelId,
            'flag' => 'K',
            'noHp' => $noHp,
            'tipe' => '1'
        );

        $checkSaldo = $this->saldoWallet($checkData);

        if ($checkSaldo->responseCode == '00') {
            if (!isset($checkSaldo->data)) {
                return array(
                    'saldo' => '0',
                    'norek' => ''
                );
            }

            $saldoData = json_decode($checkSaldo->data);

            //Update saldo wallet user
            $this->User->updateUser($userId, array(
                'norek' => $saldoData->norek,
                'saldo' => $saldoData->saldo
            ));

            return array(
                'saldo' => $saldoData->saldo,
                'norek' => $saldoData->norek
            );
        } else {
            return array(
                'saldo' => '0',
                'norek' => ''
            );
        }
    }
    
    function biayaCetak($channelId)
    {
        $url = '/param/biayacetak';
        $data = [];
        $data['clientId'] = $this->config->item('core_post_username');
        $data['channelId'] = $channelId;
        $data['flag'] = 'K';
        
        return $this->coreRequest($url, $data, 'Biaya Cetak');
    }
    
    function inquiryCetakEmas($data)
    {
        $url = '/tabunganemas/order/inquiry';
        $data['clientId'] = $this->config->item('core_post_username');
        return $this->coreRequest($url, $data, 'Inquiry Cetak Emas');
    }
    
    function paymentCetakEmas($data)
    {
        $url = '/tabunganemas/order/payment';
        if ($data['paymentMethod'] == 'GCASH') {
            $url = '/gcash/payment';
        }
        $data['clientId'] = $this->config->item('core_post_username');
        return $this->coreRequest($url, $data, 'Payment Cetak Emas');
    }
    
    function portofolioWallet($data)
    {
        $url = '/portofolio/rekkoranwallet';
        $data['clientId'] = $this->config->item('core_post_username');
        return $this->coreRequest($url, $data, 'Portofolio Wallet');
    }
    
    function systemParameter($data)
    {
        $url = '/param/system';
        $data['clientId'] = $this->config->item('core_post_username');
        return $this->coreRequest($url, $data, 'System Parameter');
    }

    function inquiryGTE($data)
    {
        $data['clientId'] = $this->config->item('core_post_username');
        $url = '/gte/inquiry';
        return $this->coreRequest($url, $data, 'Inquiry Gadai Tabungan Emas');
    }

    function openGTE($data)
    {
        $data['clientId'] = $this->config->item('core_post_username');
        $url = '/gte/open';
        return $this->coreRequest($url, $data, 'Open Gadai Tabungan Emas');
    }


    /**
     * Method untuk mendapatkan saldo terakhir berdasarkan norek
     * @param $norek String norek tabungan emas
     * @return array assosiatif array dengan key saldo, saldoAkhir, dan saldoBlokir
     */
    function getSaldo($norek)
    {
        
        $saldo = array(
            'saldo' => 0,
            'saldoAkhir' => 0,
            'saldoBlokir' => 0,
            'saldoEfektif' => 0
        );
        
        $detailTabungan = $this->detailTabunganEmas($norek, '6017');
        if ($detailTabungan->responseCode == '00') {
            if (isset($detailTabungan->data) && $detailTabungan->data != null) {
                $portof =  json_decode($detailTabungan->data);
                $saldo['saldo'] = $portof->saldo - $portof->saldoBlokir;
                $saldo['saldoAkhir'] = $portof->saldo;
                $saldo['saldoBlokir'] = $portof->saldoBlokir;
                $saldo['saldoEfektif'] = $portof->saldoEfektif;
            }
        }
        return $saldo;
    }

    function checkPromo($data)
    {
        $data['clientId'] = $this->config->item('core_post_username');
        $url = '/promo/check';
        return $this->coreRequest($url, $data, 'Check Promo');
    }
    
    function muliaSimulasi($data)
    {
        $data['clientId'] = $this->config->item('core_post_username');
        $url = '/mulia/simulasi';
        return $this->coreRequest($url, $data, 'Mulia Simulasi');
    }

    function muliaInquiry($data)
    {
        $data['clientId'] = $this->config->item('core_post_username');
        $url = '/mulia/inquiry';
        return $this->coreRequest($url, $data, 'Mulia Inquiry');
    }

    function muliaOpen($data)
    {
        $data['clientId'] = $this->config->item('core_post_username');
        $url = '/mulia/open';
        if ($data['paymentMethod'] == 'GCASH') {
            $url = '/gcash/payment';
        }
        return $this->coreRequest($url, $data, 'Mulia Open');
    }
    
    function getEfekToken()
    {
        $cek = $this->db->where('variable', 'gadaiefek_access_token')->get('config');
        if ($cek->num_rows() > 0) {
            return $cek->row()->value;
        }
        return null;
    }

    function getLosToken()
    {
        $cek = $this->db->where('variable', 'los_access_token')->get('config');
        if ($cek->num_rows() > 0) {
            return $cek->row()->value;
        }
        return null;
    }

    function getGPointToken()
    {
        $cek = $this->db->where('variable', 'gpoint_access_token')->get('config');
        if ($cek->num_rows() > 0) {
            return $cek->row()->value;
        }
        return null;
    }

    function getMsGcashToken()
    {
        $cek = $this->db->where('variable', 'msgcash_access_token')->get('config');
        if ($cek->num_rows() > 0) {
            return $cek->row()->value;
        }
        return null;
    }
    
    public function getEfekList($option)
    {
        $efekToken = $this->getEfekToken();
        
        $url = $this->config->item("gadaiefek_API_URL")."gemefeks/".$option;
            
        $curl = new Curl();
        $curl->setHeader('Content-Type', 'application/json');
        $curl->setHeader('Authorization', 'Bearer '.$efekToken);
        $curl->get($url);

        log_message('debug', 'Get GadaiEfekToken: '.json_encode($curl->response));

        if ($curl->error) {
            return $curl->response->status;
        } else {
            return $curl->response;
        }
    }
    
    public function efekSimulasi($data)
    {
        $efekToken = $this->getEfekToken();
        
        $url = $this->config->item("gadaiefek_API_URL")."pengajuan-gadai-efek-simulasi/";
            
        $curl = new Curl();
        $curl->setHeader('Content-Type', 'application/json');
        $curl->setHeader('Authorization', 'Bearer '.$efekToken);
        $curl->post($url, $data);

        log_message('debug', 'Simulasi Gadai Efek: '.json_encode($curl->response));

        if ($curl->error) {
            return $curl->response->status;
        } else {
            return $curl->response;
        }
    }

    public function efekOpen($data)
    {

        /** Fake response in case error pada API G-Cash **/
        
        // $fakeResponse = [
        //    'responseCode' => '00',
        //    'responseDesc' => 'Aproved',
        //    'data' =>(object) [
        //       'user_AIID' => '1310',
        //       'kode_booking' => '12345654321userpds',
        //       'bookingId' => '12345654321userpds',
        //       'noPengajuan' => 'los000001',
        //       'tanggalPengajuan' => '2019-02-27',
        //       'tglTransaksi' => '2019-02-27',
        //       'up' => '10000',
        //       'jaminanEfek' => '[{"kode":"HMSP","qty":"100","nama":"H.M. Sampoerna Tbk.","satuan":"100"}]',
        //       'namaNasabah' => 'SYAIFUL ANAM',
        //       'tenor' => '90',
        //       'minUp' => '37050000',
        //       'maxUp' => '37050000',
        //       'idBank' => '225',
        //     ]
        // ];

        // return (object) $fakeResponse;
                
        
        
        $losToken = $this->getLosToken();
        $openData = [
            'tc' => 'opengadai',
            'userid' => $this->config->item('los_username'),
            'data' => array($data)
        ];
        log_message('debug', 'Open Gadai Efek (LOS) Data: '.json_encode($openData));

        $url = $this->config->item("los_API_URL")."setdata/";

        $curl = new Curl();
        $curl->setHeader('Content-Type', 'application/json');
        $curl->setHeader('Authorization', 'Bearer '.$losToken);
        $curl->post($url, $openData);

        log_message('debug', 'Open Gadai Efek (LOS): '.json_encode($curl->response));

        return $curl->response;
    }

    public function gcashRekening($data)
    {
        $data['clientId'] = $this->config->item('core_post_username');
        $url = '/gcash/rekening';
        return $this->coreRequest($url, $data, 'Gcash Rekening');
    }

    public function gcashOpen($data)
    {
        //Fake response in case error pada API G-Cash
//         $fakeResponse = [
//            'responseCode' => '00',
//            'responseDesc' => 'Aproved',
//            'data' => json_encode([
//                'amount' => '0',
//                'trxId' => $data['trxId'],
//                'tglExpired' => '2020-01-01 10:10:10',
//                'virtualAccount' => '123456789'
//            ])
//         ];
//         return (object) $fakeResponse;
        

        $data['clientId'] = $this->config->item('core_post_username');
        $url = '/gcash/open';
        return $this->vaRequest($url, $data, 'Gcash Open');
    }

    public function gcashInquiry($data)
    {
        $data['clientId'] = $this->config->item('core_post_username');
        $url = '/gcash/balance';
        return $this->coreRequest($url, $data, 'Gcash Inquiry');
    }

    public function gcashInquiryTransfer($data)
    {
        $data['clientId'] = $this->config->item('core_post_username');
        $url = '/gcash/transfer/inquiry';
        return $this->coreRequest($url, $data, 'Gcash Inquiry');
    }

    public function gcashTransfer($data)
    {
        $data['clientId'] = $this->config->item('core_post_username');
        $url = '/gcash/transfer/payment';
        return $this->coreRequest($url, $data, 'Gcash Transfer');
    }

    public function gcashOtpBri($data)
    {
        $data['clientId'] = $this->config->item('core_post_username');
        $url = '/gcash/otp';
        return $this->coreRequest($url, $data, 'Gcash Transfer OTP BRI');
    }

    public function gcashHistory($data)
    {
        $data['clientId'] = $this->config->item('core_post_username');
        $url = '/gcash/mutasi/internal';
        return $this->coreRequest($url, $data, 'Gcash History');
    }

    public function gPointGetPoint($cif)
    {
        $gPointToken = $this->getGPointToken();
        
        $url = $this->config->item("gpoint_API_URL")."api/campaigns/point?userId=".$cif;
            
        $curl = new Curl();
        $curl->setHeader('Content-Type', 'application/json');
        $curl->setHeader('Authorization', 'Bearer '.$gPointToken);
        $curl->get($url);

        log_message('debug', 'Get User Point '.$cif.': '.json_encode($curl->response));

        if ($curl->error) {
            log_message('debug', $curl->errorCode . ': ' . $curl->errorMessage);
            return $curl->response;
        } else {
            return $curl->response;
        }
    }

    public function gPointVoucher($page, $limit)
    {
        $gPointToken = $this->getGPointToken();
        
        $url = $this->config->item("gpoint_API_URL")."api/vouchers?page=".$page."&limit=".$limit;
            
        $curl = new Curl();
        $curl->setHeader('Content-Type', 'application/json');
        $curl->setHeader('Authorization', 'Bearer '.$gPointToken);
        $curl->get($url);

        log_message('debug', 'Get GPoint Vouchers: '.json_encode($curl->response));

        if ($curl->error) {
            log_message('debug', $curl->errorCode . ': ' . $curl->errorMessage);
            return $curl->response;
        } else {
            return $curl->response;
        }
    }

    public function gPointUserVoucher($cif, $page, $limit)
    {
        $gPointToken = $this->getGPointToken();
        
        $url = $this->config->item("gpoint_API_URL")."api/vouchers/user?userId=".$cif."&status=1&page=".$page."&limit=".$limit;
            
        $curl = new Curl();
        $curl->setHeader('Content-Type', 'application/json');
        $curl->setHeader('Authorization', 'Bearer '.$gPointToken);
        $curl->get($url);

        log_message('debug', 'Get GPoint Vouchers: '.json_encode($curl->response));

        if ($curl->error) {
            log_message('debug', $curl->errorCode . ': ' . $curl->errorMessage);
            return $curl->response;
        } else {
            return $curl->response;
        }
    }

    function gPointValidateVoucher($data)
    {
        $gPointToken = $this->getGPointToken();
        
        $url = $this->config->item("gpoint_API_URL")."api/vouchers/validate";
            
        $curl = new Curl();
        $curl->setHeader('Content-Type', 'application/json');
        $curl->setHeader('Authorization', 'Bearer '.$gPointToken);
        $curl->post($url, $data);

        log_message('debug', 'Validate GPoint Vouchers Data: '.json_encode($data));
        log_message('debug', 'Validate GPoint Vouchers URL: '.$url);
        log_message('debug', 'Validate GPoint Vouchers Response: '.json_encode($curl->response));

        if ($curl->error) {
            log_message('debug', $curl->errorCode . ': ' . $curl->errorMessage);
            return $curl->response;
        } else {
            return $curl->response;
        }
    }

    public function gPointVoucherDetails($id)
    {
        $gPointToken = $this->getGPointToken();
        
        $url = $this->config->item("gpoint_API_URL")."api/vouchers/".$id;
            
        $curl = new Curl();
        $curl->setHeader('Content-Type', 'application/json');
        $curl->setHeader('Authorization', 'Bearer '.$gPointToken);
        $curl->get($url);

        log_message('debug', 'Get GPoint Vouchers: '.json_encode($curl->response));

        if ($curl->error) {
            log_message('debug', $curl->errorCode . ': ' . $curl->errorMessage);
            return $curl->response;
        } else {
            return $curl->response;
        }
    }

    public function gPointBuy($cif, $id)
    {
        $gPointToken = $this->getGPointToken();
        
        $url = $this->config->item("gpoint_API_URL")."api/vouchers/buy";
            
        $curl = new Curl();
        $curl->setHeader('Content-Type', 'application/json');
        $curl->setHeader('Authorization', 'Bearer '.$gPointToken);
        $data = [
            'voucherId' => $id,
            'userId' => $cif,
        ];
        $curl->post($url, $data);

        log_message('debug', 'Buy GPoint Vouchers: '.json_encode($curl->response));

        if ($curl->error) {
            log_message('debug', $curl->errorCode . ': ' . $curl->errorMessage);
            return $curl->response;
        } else {
            return $curl->response;
        }
    }

    public function gPointHistory($cif)
    {
        $gPointToken = $this->getGPointToken();
        
        $url = $this->config->item("gpoint_API_URL")."api/campaigns/point/history?userId=".$cif;
            
        $curl = new Curl();
        $curl->setHeader('Content-Type', 'application/json');
        $curl->setHeader('Authorization', 'Bearer '.$gPointToken);
        
        $curl->get($url);

        log_message('debug', 'Buy GPoint Vouchers: '.json_encode($curl->response));

        if ($curl->error) {
            log_message('debug', $curl->errorCode . ': ' . $curl->errorMessage);
            return $curl->response;
        } else {
            return $curl->response;
        }
    }

    public function gPointRedeem($data)
    {
        $gPointToken = $this->getGPointToken();
        
        $url = $this->config->item("gpoint_API_URL")."api/vouchers/redeem";
            
        $curl = new Curl();
        $curl->setHeader('Content-Type', 'application/json');
        $curl->setHeader('Authorization', 'Bearer '.$gPointToken);
        
        $curl->post($url, $data);

        log_message('debug', 'Buy GPoint Vouchers: '.json_encode($curl->response));

        if ($curl->error) {
            log_message('debug', $curl->errorCode . ': ' . $curl->errorMessage);
            return $curl->response;
        } else {
            return $curl->response;
        }
    }

    public function gPointValue($data)
    {
        $gPointToken = $this->getGPointToken();
        
        $url = $this->config->item("gpoint_API_URL")."api/campaigns/value";
            
        $curl = new Curl();
        $curl->setHeader('Content-Type', 'application/json');
        $curl->setHeader('Authorization', 'Bearer '.$gPointToken);

        log_message('debug', 'GPOINT REFRESH POINT: '.$url);
        
        $curl->post($url, $data);

        log_message('debug', 'GPOINT VALUE: '.json_encode($curl->response));

        if ($curl->error) {
            log_message('debug', $curl->errorCode . ': ' . $curl->errorMessage);
            return $curl->response;
        } else {
            return $curl->response;
        }
    }

    // Inquiry GPOINT
    public function gPointInquiry($data)
    {
        $url = '/gpoin/rewards/inquiry';
        $data['clientId'] = $this->config->item('core_post_username');
        // $data['clientId'] = '9997';
        // $data['channelId'] = '6017';
        $data['channelId'] = $this->config->item('core_client_id');
        return $this->coreRequest($url, $data, 'GPOIN REWARDS INQUIRY');
    }
    // Inquiry multi reward GPOINT
    public function gPointInquiryMultiReward($data)
    {
        $url = '/gpoin/rewardsmulti/inquiry';
        $data['clientId'] = $this->config->item('core_post_username');
        $data['channelId'] = $this->config->item('core_client_id');
        return $this->coreRequest($url, $data, 'GPOIN MUTLI REWARDS INQUIRY');
    }

    // Inquiry Cancel GPOINT
    public function gPointCancelInquiry($data)
    {
        $url = '/gpoin/rewards/cancel';
        $data['clientId'] = $this->config->item('core_post_username');
        $data['channelId'] = $this->config->item('core_client_id');
        return $this->coreRequest($url, $data, 'GPOIN REWARDS INQUIRY');
    }



    public function msGcashRequest($url, $type, $data = array(), $requestId)
    {
        $gPointToken = $this->getMsGcashToken();
        
        $url = $this->config->item("msgcash_API_URL").$url;
            
        $curl = new Curl();
        $curl->setHeader('Content-Type', 'application/json');
        $curl->setHeader('Authorization', 'Bearer '.$gPointToken);
        
        if ($type == 'POST') {
            $curl->post($url, $data);
        } elseif ($type == 'GET') {
            $curl->get($url);
        }
        

        log_message('debug', 'GCASH MICROSERVICE '.$requestId);
        log_message('debug', 'URL: '. $url) ;
        log_message('debug', 'DATA: '. json_encode($data)) ;

        if ($curl->error) {
            log_message('debug', 'ERROR: '.$curl->errorCode . ': ' . $curl->errorMessage);
            return $curl->response;
        } else {
            log_message('debug', 'RESPONSE: '. json_encode($curl->response)) ;
            
            $res = $curl->response;

            if ($res->responseCode == '00') {
                $res->data = json_encode($res->data);
            }
            return $res;
        }
    }

    public function msGcashOpen($data)
    {
        $data['clientId'] = $this->config->item('core_post_username');
        $url = 'open';
        return $this->msGcashRequest($url, 'POST', $data, 'OPEN VA');
    }

    public function msGcashRekening($data)
    {
        $data['clientId'] = $this->config->item('core_post_username');
        $url = 'rekening';
        return $this->msGcashRequest($url, 'POST', $data, 'GET REKENING');
    }

    public function msGcashBank()
    {
        //$data['clientId'] = $this->config->item('core_post_username');
        $url = 'banks';
        return $this->msGcashRequest($url, 'GET', [], 'GET BANK');
    }

    public function msGcashInquiry($data)
    {
        $data['clientId'] = $this->config->item('core_post_username');
        $url = 'balance';
        return $this->msGcashRequest($url, 'POST', $data, 'INQUIRY VA');
    }

    public function msGcashTransferInquiry($data)
    {
        $data['clientId'] = $this->config->item('core_post_username');
        $url = 'transfer/inquiry';
        return $this->msGcashRequest($url, 'POST', $data, 'INQUIRY TRANSFER');
    }

    public function msGcashTransferPayment($data)
    {
        $data['clientId'] = $this->config->item('core_post_username');
        $url = 'transfer/payment';
        return $this->msGcashRequest($url, 'POST', $data, 'TRANSFER');
    }

    public function validateToken()
    {
        $token = $this->getToken();
        if (!$token) {
            $this->errorUnAuthorized();
            return false;
        }
        return true;
    }

     /**
     * Method untuk melakukan cif management
     * @param array $data
     * @return void
     */
    function unlinkCif($data)
    {
       
        $url = '/customer/unlink';
        $data['clientId'] = $this->config->item('core_post_username');
        $data['channelId'] = $this->config->item('core_client_id');
        
        return $this->coreRequest($url, $data, 'Inquiry MPO Seluler');
    }

    function simulasiTE($data)
    {
        $url = '/simulasi/TopUpTab';
        $data['channelId'] = $this->config->item('core_client_id');
        $data['clientId'] = $this->config->item('core_post_username');
        return $this->coreRequest($url, $data, 'Simulasi TE');
    }

    function smsNotifPromo($data)
    {
        $url = '/otp/sms';
        $data['channelId'] = $this->config->item('core_client_id');
        $data['clientId'] = $this->config->item('core_post_username');
        return $this->coreRequest($url, $data, 'SMS Promo Notification');
    }

    //send status taksiran di tolak oleh admin cabang
    function notifAdminTolak($data)
    {
        $url = '/god/cancel';
        $data['channelId'] = $this->config->item('core_client_id');
        $data['clientId'] = $this->config->item('core_post_username');
        return $this->coreRequest($url, $data, 'Notif Admin Tolak');
    }

    // Verified dukcapil
    function verifiedDukcapil($data)
    {
        $url = '/customer/verifieddukcapil';
        $data['channelId'] = $this->config->item('core_client_id');
        $data['clientId'] = $this->config->item('core_post_username');
        return $this->coreRequest($url, $data, 'Verified Dukcapil');
    }

    // function to send a response
    function send_response($status, $message = null, $data = null, $code = null, $statusCode = 200, $err = null)
    {
        // if $status is an array
        // then it should be just set the response directly
        if (is_array($status)) {
            // from now $status become $response
            $this->set_response($status, $statusCode);

            return $status;
        }

        $response = [
            'status' => $status,
            'message' => $message,
            'data' => $data
        ];

        if (!empty($code)) {
            $response['code'] = $code;
        }

        if (!empty($err)) {
            $response['errors'] = $err;
        }

        $this->set_response($response, $statusCode);

        return $response;
    }

    //Fire to Dukcapil with NIK
    function inquiryDukcapil($data)
    {
        $url ='/customer/dukcapil';
        $data['channelId'] = $this->config->item('core_client_id');
        $data['clientId'] = $this->config->item('core_post_username');
        return $this->coreRequest($url, $data, 'Inquiry Dukcapil');
    }

    function ekycActivation($data)
    {
        $url = '/customer/ekyc/activation';
        $data['channelId'] = $this->config->item('core_client_id');
        $data['clientId'] = $this->config->item('core_post_username');
        return $this->coreRequest($url, $data, 'E-KYC Activation');
    }

    function frDukcapil($data)
    {
        $url = '/jswitch/kependudukan/image';
        $data['channelId'] = $this->config->item('core_client_id');
        $data['clientId'] = $this->config->item('core_post_username');
        return $this->coreRequest($url, $data, 'E-KYC FR Dukcapil');
    }

    // Tidak di pakai sementara
    // function generateIdpromosi($data){
    //     if ($data){
    //         // $tipe = $data->type;
    //         switch ($data->type) {
    //             case 'discount' :
    //                 $tipe = '0';
    //                 break;
                
    //             case 'goldback' :
    //                 $tipe =  '1';
    //                 break;

    //             case 'voucher' :
    //                 $tipe = '2';
    //                 break;
    //         }
    //         if ($data->promoCode){
    //             $code = $data->promoCode;
    //         } else {
    //             $code = $data->voucherCode;
    //         }
    //         // print_r($tipe); exit;
    //         $res = $tipe.';'.$code.';'.$data->journalAccount.';'.$data->refTrx.';'.$data->voucherName;
    //         return $res;
    //     } else {
    //         return '';
    //     }

    // }
}
