<?php

use GuzzleHttp\Psr7\Response;

defined('BASEPATH') or exit('No direct script access allowed');

require_once 'CorePegadaian.php';
require_once APPPATH . 'helpers/message_helper.php';

/**
 * @property MasterModel MasterModel
 * @property PegawaiModel pegawai_model
 * @property Gpoint_service Gpoint_service
 */
class Gpoint extends CorePegadaian
{
    private $page  = 1,
        $limit = 10;

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array(
            'User',
            'NotificationModel',
            'ConfigModel',
            'GpoinModel',
            'ProductMasterModel',
            'MuliaModel',
            'PaymentModel'
        ));
        $this->load->model('PegawaiModel', 'pegawai_model');
        $this->load->model('MasterModel');
        $this->load->library('form_validation');
        $this->load->service('Gpoint_service');
    }

    function vouchers_get($id = null)
    {
        $token = $this->getToken();
        if ($token) {
            $vouchers = null;
            if ($id == null) {
                $page = $this->query('page');
                $limit = $this->query('limit');
                $vouchers = $this->gPointVoucher($page, $limit);
            } else {
                $vouchers = $this->gPointVoucherDetails($id);
            }

            $this->set_response($vouchers);
        } else {
            $this->errorUnAuthorized();
        }
    }

    function myvoucher_get()
    {
        $token = $this->getToken();
        if ($token) {
            $user = $this->User->getUser($token->id);
            $page = $this->query('page');
            $limit = $this->query('limit');
            $vouchers = $this->gPointUserVoucher($user->cif, $page, $limit);
            $this->set_response($vouchers);
        } else {
            $this->errorUnAuthorized();
        }
    }

    function buy_post()
    {
        $token = $this->getToken();
        if ($token) {
            $this->form_validation->set_rules('id_voucher', 'id_voucher', 'required|integer');

            if ($this->form_validation->run() == false) {
                return $this->set_response([
                    'status' => 'error',
                    'message' => 'Invalid Input',
                    'code' => 101,
                    'errors' => $this->form_validation->error_array()
                ]);
            }

            $user = $this->User->getUser($token->id);
            $voucherId = $this->post('id_voucher');
            $response = $this->gPointBuy($user->cif, $voucherId);
            $this->set_response($response);
        } else {
            $this->errorUnAuthorized();
        }
    }

    function history_get()
    {
        $token = $this->getToken();
        if ($token) {
            $user = $this->User->getUser($token->id);
            $response = $this->gPointHistory($user->cif);
            $this->set_response($response);
        } else {
            $this->errorUnAuthorized();
        }
    }

    function validate_post()
    {
        $token = $this->getToken();
        if ($token) {
            $user = $this->User->getUser($token->id);

            $this->form_validation->set_rules('promo_code', 'promo_code', 'required');
            $this->form_validation->set_rules('jenis_transaksi', 'jenis_transaksi', 'required');
            $this->form_validation->set_rules('kode_produk', 'kode_produk', 'required');


            if ($this->form_validation->run() == false) {
                $this->set_response([
                    'status' => 'error',
                    'message' => 'Invalid input',
                    'code' => 101,
                    'errors' => $this->form_validation->error_array()
                ]);
            } else {
                $voucherId = $this->post('voucher_id');
                $promoCode = $this->post('promo_code');
                $jenisTransaksi = $this->post('jenis_transaksi');
                $kodeProduk = $this->post('kode_produk');
                $unit = $this->post('unit');
                $amount = $this->post('amount');

                $validators = [
                    'channel' => $this->config->item('core_post_username'),
                    'product' => $kodeProduk,
                    'transactionType' => $jenisTransaksi,

                ];

                if ($unit != null) {
                    $validators['unit'] = $unit;
                }

                $validateData = [
                    'promoCode' => $promoCode,
                    'voucherId' => $voucherId,
                    'userId' => $user->cif,
                    'validators' => $validators
                ];

                if ($amount != null) {
                    $validateData['transactionAmount'] = (int) $amount;
                }

                $validate = $this->gPointValidateVoucher($validateData);

                $this->set_response($validate);
            }
        } else {
            $this->errorUnAuthorized();
        }
    }


    function voucherimage_post($method = 'add')
    {
        if (!$this->authCore()) {
            $this->errorUnAuthorized();
        }

        if ($method == 'add') {
            $uploadDir = $this->config->item('upload_dir');

            $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
            $config['max_size'] = 0;
            $config['max_width'] = 0;
            $config['max_height'] = 0;
            $config['encrypt_name'] = true;
            $config['upload_path'] = $uploadDir . '/gpoint/vouchers';
            log_message('debug', 'GPOINT IMAGE PATH: ' . $config['upload_path']);
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('file')) {
                $this->response([
                    'code' => 101,
                    'status' => 'error',
                    'message' => '',
                    'errors' => $this->upload->display_errors()
                ], 200);
            } else {
                $data = $this->upload->data();

                $this->response([
                    'status' => 'success',
                    'message' => '',
                    'data' => array(
                        'filename' => 'gpoint/vouchers/'. $data['file_name']
                    )
                ], 200);
            }
        } elseif ($method == 'delete') {
            $this->form_validation->set_rules('filename', 'filename', 'required');
            if ($this->form_validation->run() == false) {
                return $this->set_response([
                    'status' => 'error',
                    'message' => 'Invalid input',
                    'code' => 101,
                    'errors' => $this->form_validation->error_array()
                ]);
            } else {
                $fileName = $this->post('filename');
                $uploadDir = $this->config->item('upload_dir');
                if (file_exists($uploadDir . '/gpoint/vouchers/' . $fileName)) {
                    unlink($uploadDir . '/gpoint/vouchers/' . $fileName);

                    $this->set_response([
                        'status' => 'success',
                        'message' => 'Voucher image deleted',
                        'data' => [
                            'filename' => $fileName
                        ]
                    ]);
                } else {
                    $this->set_response([
                        'status' => 'error',
                        'message' => 'File not found.',
                        'data' => null
                    ]);
                }
            }
        }
    }

    function gpoin_inquiry_post()
    {
        log_message('debug', 'gpoin Start ' . __FUNCTION__ . ' Request ' . json_encode($this->post()));

        $token = $this->getToken();

        if (!$token) {
            return $this->errorUnAuthorized();
        }

        $this->form_validation->set_rules('kodePromo', 'Promo Code', 'required');
        $this->form_validation->set_rules('transactionAmount', 'Transaction Amount', 'required');
        $this->form_validation->set_rules('reffSwitching', 'reffSwitching', 'required');
        $this->form_validation->set_rules('jenis', 'jenis', 'required');

        if ($this->form_validation->run() == false) {
            foreach ($this->form_validation->error_array() as $key => $value) {
                $error = $value;
                break;
            }

            return $this->send_response('error', $error ?? '', '', '101');
        }

        $typeValidate    = $this->Gpoint_service->transactionTypeValidate($this->post('jenis'));
        $transactionDate = $this->Gpoint_service->transactionDate();
        $Ref             = $this->User->CheckRef($this->post('kodePromo'));
        $user            = $this->User->getUser($token->id);

        // Block referral cgc
        $flag_cgc = $this->MasterModel->getParametersApps('cgc_feature_toggle');
        if ($flag_cgc != 1 && !empty($Ref)) {
            return $this->send_response('error', 'Kode promo tidak valid', null);
        }

        // Return error for product code micro
        if (!empty($typeValidate['productCode']) && $typeValidate['productCode'] == '80') {
            return $this->send_response('error', 'Kode promo tidak valid', null);
        }

        // Check Validate isEmployee
        $user_employee = $this->pegawai_model->getIsEmployeeByUser($user->cif, $user->no_hp);
        $ref_employee  = false;

        if (!empty($Ref->cif) && !empty($Ref->no_hp)) {
            $ref_employee = $this->pegawai_model->getIsEmployeeByUser($Ref->cif, $Ref->no_hp);
        }

        if ($user_employee || $ref_employee) {
            $message = $user_employee ? 'Kode referral ini tidak dapat digunakan oleh pegawai Pegadaian' : 'Kode referral tidak valid';

            return $this->send_response('error', $message, null);
        }

        // Check Validate Trx Gpoint
        if (!empty($user->cif)) {
            $response = $this->Gpoint_service->checkTransactionGpoint($user, $typeValidate, $Ref);
            if ($response['status'] == 'error') {
                return $this->send_response($response['status'], $response['message'], $response['data']);
            }
        }

        $data = [
            'cif'               => $this->post('cif') ? $this->post('cif') : '',
            'promoCode'         => strtoupper($this->post('kodePromo')),
            'transactionAmount' => $this->post('transactionAmount'),
            'productCode'       => $typeValidate['productCode'],
            'transactionType'   => $typeValidate['transactionType'],
            'phone'             => $this->post('phone'),
            'customerName'      => $this->post('customerName'),
            'transactionDate'   => $transactionDate,
            'reffSwitching'     => $this->post('reffSwitching'),
            'referrer'          => empty($Ref->cif) ? '' : $Ref->cif,
            'isReferral'        => empty($Ref->referral_code) ? 'false' : 'true'
        ];

        if ($data['isReferral'] == 'true') {
            $data['discountAmount'] = $this->post('transactionAmount');
            $data['campaignCode']   = 'referral';
            $this->gpoin_referral($data);
            return;
        }

        $inquiry = $this->gPointInquiry($data);

        if (empty($inquiry->responseCode)) {
            log_message('debug', 'gpoin error log ' . __FUNCTION__ . json_encode($inquiry) . ' Body:' . json_encode($data));

            return $this->send_response('error', 'Silahkan coba kembali', $inquiry->responseDesc, '102');
        }

        if ($inquiry->responseCode != '00') {
            log_message('debug', 'gpoin error log' . __FUNCTION__ . json_encode($inquiry) . ' Body:' . json_encode($data));

            return $this->send_response('error', 'Kode promo / referral tidak valid.', $inquiry->responseDesc, '102');
        }

        $dataInq = json_decode($inquiry->data);
        $dataArr = (array)$dataInq;
        $dataArr['rewards']->value = floatval($dataArr['rewards']->value);
        $dataArr['rewards']->discountAmount = floatval($dataArr['rewards']->discountAmount);

        if (empty($dataArr)) {
            log_message('debug', 'gpoin error log' . __FUNCTION__ . json_encode($inquiry) . ' Body:' . json_encode($data));

            return $this->send_response('error', 'Terjadi kesalahan pada sistem. Mohon coba beberapa saat lagi.', $inquiry->responseDesc, '102');
        }

        $dataGpoin = $this->GpoinModel->check($dataArr['reffSwitching']);

        if ($dataGpoin == '') {
            $this->GpoinModel->add($dataArr, $data);
        } else {
            $this->GpoinModel->update_trans_id($dataArr, $data['promoCode']);
        }

        $product = $this->ProductMasterModel->checkTable($data['productCode']);

        if ($product) {
            $queryPayment = $this->db->where(['reffSwitching' => $data['reffSwitching']])->get($product->table);
            $payment = ($queryPayment->num_rows() > 0) ? ($queryPayment->row()->totalKewajiban ?? 0) : 0;
            $payment = ($payment == 0) ? ($queryPayment->row()->total_kewajiban ?? 0) : $payment;
            $dataTotalKewajiban = [
                'productCode' => $data['productCode'],
                'type' => $dataArr['rewards']->type,
                'value' => floatval($dataArr['rewards']->value),
                'totalKewajiban' => floatval($payment)
            ];

            $dataArr['rewards']->totalKewajiban = $this->Gpoint_service->totalKewajiban($dataTotalKewajiban);
        }

        $dataMessageGpoin = [
            'productCode' => $data['productCode'],
            'value'       => $dataArr['rewards']->value,
            'typeRewards' => $dataArr['rewards']->type,
            'isReferral'  => $data['isReferral'],
            'promoCode'   => $data['promoCode']
        ];

        $dataArr['rewards']->message = $this->Gpoint_service->messageGpoint($dataMessageGpoin);

        log_message('debug', 'gpoin sukses log' . __FUNCTION__ . json_encode($inquiry));

        log_message('debug', 'gpoin End ' . __FUNCTION__ . ' Response ' . json_encode($inquiry));

        return $this->send_response('success', '', $dataArr['rewards']);
    }

    function gpoin_inquiry_cancel_post()
    {
        $token = $this->getToken();

        if (empty($token)) {
            return $this->errorUnAuthorized();
        }

        $this->form_validation->set_rules('reffSwitching', 'ID Transaksi', 'required');

        if (!$this->form_validation->run()) {
            return $this->send_response('error', $error[0] ?? null, '', '101');
        }

        $reffSwitching = $this->post('reffSwitching');
        $user = $this->user->profile($token->id);

        $response = $this->Gpoint_service->rewardReject($user, $reffSwitching);

        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    function list_voucher_get()
    {
        log_message('debug', 'START ' . __FUNCTION__ . ' => ' . json_encode($this->query()));

        $token = $this->getToken();

        if (empty($token)) {
            return $this->errorUnAuthorized();
        }

        $user = $this->User->getUser($token->id);
        $q_transaction_type = $this->query('transaction_type');
        $transaction_type = $this->Gpoint_service->transactionTypeValidate($q_transaction_type);

        if (!empty($q_transaction_type) && empty($transaction_type['transactionType'])) {
            return $this->send_response('error', 'Transaction Type tidak valid!', '', 101);
        }

        $page    = $this->query('page') ?? $this->page;
        $limit   = $this->query('limit') ?? $this->limit;
        $product = $transaction_type['productCode'] ?? '';
        $type    = $transaction_type['transactionType'] ?? '';

        $response = $this->Gpoint_service->getListVouchers($user, $page, $limit, $product, $type);

        log_message('debug', 'END ' . __FUNCTION__ . ' => ' . json_encode($response));

        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    function list_promotions_get()
    {
        log_message('debug', 'START ' . __FUNCTION__ . ' => ' . json_encode($this->query()));

        $token = $this->getToken();

        if (empty($token)) {
            return $this->errorUnAuthorized();
        }

        $q_transaction_type = $this->query('transaction_type');
        $transaction_type = $this->Gpoint_service->transactionTypeValidate($q_transaction_type);

        if (!empty($q_transaction_type) && empty($transaction_type['transactionType'])) {
            return $this->send_response('error', 'Transaction Type tidak valid!', '', 101);
        }

        $product = $transaction_type['productCode'] ?? '';
        $type    = $transaction_type['transactionType'] ?? '';

        $response = $this->Gpoint_service->getListPromotions($product, $type);

        log_message('debug', 'END ' . __FUNCTION__ . ' => ' . json_encode($response));

        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    function inquiry_voucher_post()
    {
        $token = $this->getToken();

        if (empty($token)) {
            return $this->errorUnAuthorized();
        }

        $this->form_validation->set_rules('cif', 'CIF', 'required');
        $this->form_validation->set_rules('promoCode', 'Promo Code', 'required');
        $this->form_validation->set_rules('referralCode', 'Referral Code', 'required');

        if ($this->form_validation->run() == false) {
            foreach ($this->form_validation->error_array() as $key => $value) {
                $error = $value;
                break;
            }

            return $this->send_response('error', $error ?? null, '', '101');
        }

        $response = $this->Gpoint_service->inquiryVoucher();

        return $this->send_response('success', 'Inquiry voucher', $response);
    }

    function show_milestone_get()
    {
        $token = $this->getToken();

        if (empty($token)) {
            $this->errorUnAuthorized();
            log_message('debug', 'Data Finansial = Unauthorized');
            return;
        }

        $user = $this->user->profile($token->id);
        $response = $this->Gpoint_service->showMileStone($user);

        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    function gpoin_referral($data)
    {
        $is_user_referral_code = true;

        if (!empty($data['promoCode']) && !empty($data['cif'])) {
            $user_referrer = $this->user->CheckRef($data['promoCode']);
            $user_referral = $this->user->getUser($data['cif']);
        }

        $user_referrer_cif = $user_referrer->cif ?? '';
        $user_referral_cif = $user_referral->cif ?? '';

        if (!empty($user_referral_cif)) {
            $is_user_referral_code = $user_referrer_cif != $user_referral_cif;
        }

        if (empty($is_user_referral_code)) {
            return $this->send_response('error', 'Kode referral milik sendiri tidak berlaku. Gunakan kode referral lainnya.', '', '102');
        }

        $inquiry = $this->gPointInquiryMultiReward($data);

        if (empty($inquiry->responseCode)) {
            log_message('debug', 'gpoin error log ' . __FUNCTION__ . json_encode($inquiry) . ' Body:' . json_encode($data));

            return $this->send_response('error', 'Silahkan coba kembali', $inquiry->responseDesc, '102');
        }

        if ($inquiry->responseCode != '00') {
            log_message('debug', 'gpoin error log ' . __FUNCTION__ . json_encode($inquiry) . ' Body : ' . json_encode($data));
            $default_message = 'Kamu belum mencapai minimum transaksi untuk menggunakan kode promo ini';
            $message = $inquiry->responseDesc ?? 'Terjadi kesalahan, mohon coba beberapa saat lagi';
            $message = $message == 'Maaf, tidak ada reward yang tersedia' ? $default_message : $message;

            return $this->send_response('error', $message, $inquiry->data ?? '', '102');
        }

        // Inquiry Gpoin Start
        $dataArr = $this->Gpoint_service->dataInquiryReferral($inquiry);
        ;
        //Inquiry Gpoin End

        if (empty($dataArr)) {
            log_message('debug', 'gpoin error log' . __FUNCTION__ . json_encode($inquiry) . ' Body:' . json_encode($data));

            return $this->send_response('error', $inquiry->responseDesc ?? 'Terjadi kesalahan, mohon coba beberapa saat lagi', '', '102');
        }

        // Insert Data Multi rewards to table gpoin
        $dataGpoin = $this->GpoinModel->check($dataArr['reffSwitching']);
        if ($dataGpoin == '') {
            $this->GpoinModel->add_multi_rewards($data, $dataArr);
        }
        // Insert Data Multi rewards to table gpoin End

        $product = $this->ProductMasterModel->checkTable($data['productCode']);
        if (empty($product)) {
            log_message('debug', 'gpoin error log' . __FUNCTION__ . json_encode($product));
            return  $this->send_response('error', 'Product Table Kosong', '', '102');
        }

        $data_referral     = json_decode(json_encode($dataArr['rewards']), true);
        $key_data_referral = array_search('referral', array_column($data_referral, 'reference'));

        if (!empty($data_referral[$key_data_referral])) {
            $dataArr['rewards'] = $data_referral[$key_data_referral];
        }

        // Hitung total kewajiban di ambil dari totalKewajiban(di table payment) - Discount Amount
        $queryPayment = $this->PaymentModel->checkInqPayment($product->table, $data['reffSwitching']);

        if (empty($queryPayment)) {
            return $this->send_response('error', 'Payment tidak ditemukan', '', '102');
        }

        $dataTotalKewajiban = [
            'productCode' => $data['productCode'],
            'type' => $dataArr['type'],
            'value' => floatval($dataArr['rewards']['discountAmount'] ?? ''),
            'totalKewajiban' => floatval($queryPayment->total_kewajiban ?? $queryPayment->totalKewajiban ?? '')
        ];

        $dataArr['rewards']['totalKewajiban'] = $this->Gpoint_service->totalKewajiban($dataTotalKewajiban);
        // Hitung total kewajiban End

        // Start Message Gpoin (isRefferal & noRefferal)
        $dataMessageGpoin = [
            'productCode' => $data['productCode'],
            'value' => $dataArr['discountAmount'],
            'typeRewards' => $dataArr['type'],
            'isRefferal' => $data['isReferral']
        ];
        $dataArr['rewards']['message'] = $this->Gpoint_service->messageGpoint($dataMessageGpoin);
        // End Message Gpoin

        $dataArr['rewards']['value'] = floatval($dataArr['rewards']['value']) ?? $dataArr['rewards']['value'];
        $dataArr['rewards']['discountAmount'] = floatval($dataArr['rewards']['value']) ?? $dataArr['rewards']['discountAmount'];

        $res = $this->send_response('success', '', $dataArr['rewards']);

        log_message('debug', 'gpoin sukses log' . __FUNCTION__ . json_encode($res));

        log_message('debug', 'gpoin End ' . __FUNCTION__ . ' Response ' . json_encode($res));
    }

    function referral_code_get()
    {
        $token = $this->getToken();

        if (empty($token)) {
            return $this->errorUnAuthorized();
        }

        $user  = $this->user->profile($token->id);
        $url   = http_build_query(['referral_name' => $user->nama, 'referral_code' =>  $user->referralCode]);
        $query = str_replace('+', '%20', $url);
        $shereableMessage = 'Selamat! Kamu mendapatkan kode referral "'
            . $user->referralCode . '" dari '
            . $user->nama . '. Dapatkan bonus emas/reward hingga Rp25.000 dengan cara gunakan kode referral'
            . ' pada saat bertransaksi di aplikasi Pegadaian Digital.'
            . ' https://sahabatpegadaian.com/program-ajak-sahabat/?'.$query;

        return $this->send_response('success', 'Get referral code', [
            'referralCode' => $user->referralCode,
            'shareableMessage' => $shereableMessage
        ]);
    }

    function ajaksahabat_image_get()
    {
        log_message('Debug', 'Start Of ' . __FUNCTION__ . '=>' . json_encode($this->query()));

        $token = $this->getToken();

        if (!$token) {
            $this->errorUnAuthorized();
        }

        $imagePath = $this->config->item('image_assets');

        $snk = [
            "carapakai" => [
                'kode_referal' => $imagePath . "ajak-sahabat/group-5.png",
                'download_aplikasi' => $imagePath . "ajak-sahabat/group-4.png",
                'gunakan_referal' => $imagePath . "ajak-sahabat/group-1.png",
                'dapatkan_reward' => $imagePath . 'ajak-sahabat/group-2.png'
            ],
            "snk" => [
                'reward_sahabat' => $imagePath . 'ajak-sahabat/bitmap.png',
                'ketentuan_reward' => $imagePath . 'ajak-sahabat/bitmap-2.png',
            ]
        ];

        return $this->send_response('success', '', $snk);
        log_message('Debug', 'End Of ' . __FUNCTION__ . 'success' . json_encode($snk));
    }

    function list_voucher_history_get()
    {
        log_message('debug', 'START ' . __FUNCTION__ . ' => ' . json_encode($this->query()));

        $token = $this->getToken();

        if (empty($token)) {
            return $this->errorUnAuthorized();
        }

        $user  = $this->User->getUser($token->id);
        $page  = $this->query('page') ?? $this->page;
        $limit = $this->query('limit') ?? $this->limit;

        $response = $this->Gpoint_service->getVoucherHistory($user, $page, $limit);

        log_message('debug', 'END ' . __FUNCTION__ . ' => ' . json_encode($response));

        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    function list_ranking_get()
    {
        log_message('debug', 'START ' . __FUNCTION__ . ' => ' . json_encode($this->query()));

        $token = $this->getToken();

        if (empty($token)) {
            return $this->errorUnAuthorized();
        }

        $response = $this->Gpoint_service->getListRanking($this->User->getUser($token->id), $this->query('periode'));

        log_message('debug', 'END ' . __FUNCTION__ . ' => ' . json_encode($response));

        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    function seed_pegawai_post()
    {
        set_time_limit(0);

        log_message('debug', 'START ' . __FUNCTION__ . ' => ' . json_encode($this->query()));

        $token = $this->getCoreToken();

        if (empty($token)) {
            return $this->errorUnAuthorized();
        }

        $response = $this->Gpoint_service->seedPegawai();

        log_message('debug', 'END ' . __FUNCTION__ . ' => ' . json_encode($response));

        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    function point_show_get()
    {
        $token = $this->getToken();

        if (empty($token)) {
            return $this->errorUnAuthorized();
        }

        $response = $this->Gpoint_service->getDataPoint($token->id);

        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    function point_get()
    {
        $token = $this->getToken();

        if (empty($token)) {
            return $this->errorUnAuthorized();
        }

        return $this->send_response($this->Gpoint_service->getCustomerTotalPoint($token->id));
    }
}
