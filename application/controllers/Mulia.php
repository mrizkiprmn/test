<?php

defined('BASEPATH') or exit('No direct script access allowed');
require_once 'CorePegadaian.php';

/**
 * @property Response_service response_service
 * @property Mulia_service mulia_service
 */
class Mulia extends CorePegadaian
{
    public function __construct()
    {
        parent::__construct();

        $this->load->helper('message');
        $this->load->helper('Pegadaian');

        $this->load->library('form_validation');

        $this->load->model('ConfigModel', 'ConfigModel');
        $this->load->model('GcashModel', 'GcashModel');
        $this->load->model('GpoinModel', 'GpoinModel');
        $this->load->model('MasterModel', 'MasterModel');
        $this->load->model('MuliaModel', 'MuliaModel');
        $this->load->model('NotificationModel', 'NotificationModel');
        $this->load->model('User', 'User');

        $this->load->service('Mulia_service', 'mulia_service');
        $this->load->service('Payment_service');
        $this->load->service('Response_service', 'response_service');
    }

    function index_get($id = null)
    {
        $token = $this->getToken();
        if ($token) {
            $imageURL = $this->config->item('mulia_image_path');

            if ($id != null) {
                $data = $this->MuliaModel->getProdukById($id);
                $data = $this->urlizeImage($data);
                $this->set_response([
                    'status' => 'success',
                    'message' => '',
                    'data' => $data
                ]);
            } else {
                $page = $this->query('page');
                $page == null ? $page = 0 : $page;

                $search = null;
                if ($this->query('search') != null) {
                    $search = $this->query("search");
                }

                $category = null;
                if ($this->query('category') != null) {
                    $category = $this->query('category');
                }

                $produksi = null;
                if ($this->query('produksi') != null) {
                    $produksi = $this->query('produksi');
                }

                $offset = 20 * $page;
                $getProduk = $this->MuliaModel->getProduk(
                    $offset,
                    $search,
                    $category,
                    $produksi
                );

                $this->load->library('pagination');

                $config['base_url'] = base_url() . 'mulia/';
                $config['total_rows'] = $getProduk['totalRow'];
                $config['per_page'] = 20;
                $config["use_page_numbers"] = true;
                $config["page_query_string"] = true;
                $config['query_string_segment'] = 'page';
                $this->pagination->initialize($config);

                $produks = $this->urlizeImage($getProduk['data']);

                $this->set_response([
                    'status' => 'success',
                    'message' => '',
                    'totalData' => $getProduk['totalRow'],
                    'page' => $page,
                    'data' => $produks
                ], 200);
            }
        } else {
            $this->errorUnAuthorized();
        }
    }

    /**
     * Endpoint untuk list produk overview di screen depan
     */
    function overview_get()
    {
        $token = $this->getToken();
        if ($token) {
            $data = $this->MuliaModel->getProdukOverview();
            $ubs = $this->urlizeImage($data['ubs']);
            $antam = $this->urlizeImage($data['antam']);

            $this->set_response([
                'status' => 'success',
                'message' => '',
                'data' => [
                    [
                        "vendor" => 'UBS',
                        "item" => $ubs
                    ],
                    [
                        "vendor" => 'ANTAM',
                        "item" => $antam
                    ],
                ]
            ]);
        } else {
            $this->errorUnAuthorized();
        }
    }

    /**
     * Endpoint untuk get list cart user
     */
    function cart_get()
    {
        $token = $this->getToken();
        if ($token) {
            $userCart = $this->MuliaModel->getCart($token->id);
            $this->set_response([
                'status' => 'success',
                'message' => '',
                'data' => $this->urlizeImage($userCart->result())
            ]);
        } else {
            $this->errorUnAuthorized();
        }
    }

    /**
     * Endpoint routing untuk cart method post
     * @param string $method
     */
    function cart_post($method = 'add')
    {
        $token = $this->getToken();
        if ($token) {
            if ($method == 'add') {
                $this->addCart($token);
            } elseif ($method == 'delete') {
                $this->deleteCart($token);
            } else {
                $this->errorForbbiden();
            }
        } else {
            $this->errorUnAuthorized();
        }
    }

    /**
     * Menambahkan/update item cart
     * @param type $token
     */
    function addCart($token)
    {

        $this->form_validation->set_rules('produkId', 'productId', 'required|integer');
        $this->form_validation->set_rules('qty', 'qty', 'required|integer|greater_than[0]');

        if ($this->form_validation->run() == false) {
            $this->set_response([
                'status' => 'error',
                'message' => 'Invalid input',
                'code' => 201,
                'error' => $this->form_validation->error_array()
            ]);
        } else {
            $produkId = $this->post('produkId');
            $qty = $this->post('qty');

            $this->load->helper('text');
            $cart = $this->MuliaModel->addCart($token->id, $produkId, $qty);

            $this->set_response([
                'status' => 'success',
                'message' => 'Cart added',
                'data' => $this->urlizeImage($cart)
            ]);
        }
    }

    /**
     * Hapus item dalam cart
     * @param type $token
     */
    function deleteCart($token)
    {
        $this->form_validation->set_rules('id', 'id', 'integer|required');

        if ($this->form_validation->run() == false) {
            $this->set_response([
                'status' => 'error',
                'message' => 'Invalid input',
                'code' => 201,
                'error' => $this->form_validation->error_array()
            ]);
        } else {
            $id = $this->post('id');
            $this->MuliaModel->deleteCart($id);
            $this->set_response([
                'status' => 'success',
                'message' => 'Cart removed',
                'data' => null
            ]);
        }
    }

    /**
     * Memberi tambahan URL untuk field thumbnail dan images
     * @param  $data array
     * @return $data array
     */
    function urlizeImage($data)
    {
        //Get config untuK URL gambar
        $imageURL = $this->config->item('mulia_image_path');

        if (is_array($data)) {
            foreach ($data as $d) {
                if (isset($d->images) && $d->images != null) {
                    $images = json_decode($d->images);
                    $returnImages = array();
                    foreach ($images as $i) {
                        $returnImages[] = $imageURL . $i;
                    }
                    $d->images = $returnImages;
                }

                $d->thumbnail = $imageURL . $d->thumbnail;
            }
        } else {
            if (isset($data->images) && $data->images != null) {
                $images = json_decode($data->images);
                $returnImages = array();
                foreach ($images as $i) {
                    $returnImages[] = $imageURL . $i;
                }
                $data->images = $returnImages;
            }

            $data->thumbnail = $imageURL . $data->thumbnail;
        }

        return $data;
    }

    function product_get($id = null)
    {
        $token = $this->getToken();
        if ($token) {
            if ($id != null) {
                $product = $this->MuliaModel->getProductById($id);
                if ($product) {
                    $this->set_response([
                        'status' => 'success',
                        'message' => '',
                        'data' => $product
                    ]);
                } else {
                    $this->set_response([
                        'status' => 'error',
                        'message' => 'Produk tidak ditemukan',
                        'code' => 102
                    ]);
                }
            } else {
                $vendor = $this->query('vendor');
                if ($vendor == null) {
                    $this->set_response([
                        'status' => 'error',
                        'message' => 'Vendor harus diisi',
                        'code' => 102
                    ]);
                    return;
                }

                // Get daftar mulia
                $mulia = $this->MuliaModel->getProduct($vendor);
                if ($mulia) {
                    $imagePath = $this->config->item('mulia_image_path');
                    $r = [];
                    foreach ($mulia as $m) {
                        if ($m->images != '' || $m->images != null) {
                            $imageList = json_decode($m->images);
                            $imageListPath = [];
                            foreach ($imageList as $il) {
                                $imageListPath[] = $imagePath . $il;
                            }
                            $m->images = $imageListPath;
                        }

                        if ($m->thumbnail != '' || $m->thumbnail != null) {
                            $m->thumbnail = $imagePath . $m->thumbnail;
                        }

                        $r[] = $m;
                    }

                    $this->set_response([
                        'status' => 'success',
                        'message' => '',
                        'data' => $r
                    ]);
                } else {
                    $this->set_response([
                        'status' => 'error',
                        'code' => '102',
                        'message' => 'Produk tidak ditemukan'
                    ]);
                }
            }
        } else {
            $this->errorUnAuthorized();
        }
    }

    function simulasi_post()
    {
        log_message('Debug', 'Start Of ' . __FUNCTION__ . '=>' . json_encode($this->post()));
        $this->form_validation->set_message('required', 'Input {field} tidak boleh kosong.');

        $token = $this->getToken();

        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', 'response ' . __FUNCTION__ . 'UnAuthorized');
            return;
        }

        $setData = [
            'tenor' => $this->post('tenor'),
            'vendor' => $this->post('vendor'),
            'item' => $this->post('item'),
            'uang_muka' => $this->post('uang_muka'),
            'total_harga' => $this->post('total_harga')
        ];

        $this->form_validation->set_data($setData);
        $this->form_validation->set_rules('tenor', 'tenor', 'callback__valEmptyNumeric');
        $this->form_validation->set_rules('vendor', 'vendor', 'callback__valEmptyNumeric');
        $this->form_validation->set_rules('item', 'item', 'required');
        $this->form_validation->set_rules('uang_muka', 'uang_muka', 'callback__valMuliaUangMuka['.$this->input->post('total_harga').']');
        $this->form_validation->set_rules('total_harga', 'total_harga', 'callback__valEmptyNumeric');

        if ($this->form_validation->run() == false) {
            $validation = Pegadaian::showErrorValidation($this->form_validation->error_array());
            log_message('debug', 'END ' . __FUNCTION__ . ' => ' . json_encode($validation));
            return $this->send_response('error', 'Validation Exception', $validation, '101');
        }

        $item = $this->post('item');

        $arrayOrder = json_decode($item, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            $this->send_response('error', 'Invalid input item. Not valid JSON: ' . json_last_error_msg(), 101);
            log_message('Debug', 'End Of' . __FUNCTION__ . 'error', json_last_error_msg());

            return;
        }

        $keping = '';
        $totalKeping = 0;
        $totalGram = 0;
        $count = 0;

        foreach ($arrayOrder as $key) {
            if (isset($key['qty']) && isset($key['gram'])) {
                $totalKeping = $key['gram'] * $key['qty'];
            }
            $totalGram += $totalKeping;
        }

        foreach ($arrayOrder as $o) {
            if (isset($o['qty']) && isset($o['gram'])) {
                $keping = $keping . $o['gram'] . ',' . $o['qty'];
                if ($count < count($arrayOrder)) {
                    $keping = $keping . ';';
                }
            }
            $count++;
        }

        if ($keping == '') {
            $this->send_response('error', 'Invalid input item', 101);
            log_message('Debug', 'End Of' . __FUNCTION__ . 'error', 'Invalid input item');

            return;
        }

        $vendor = $this->post('vendor');
        $uangMuka = $this->post('uang_muka');
        $tenor = $this->post('tenor');
        $total_harga = $this->post('total_harga');
        $uangMukaTunai = ($uangMuka / $total_harga) * 100;
        $uangMukaTunaiPercenRound = round($uangMukaTunai);
        $uangMukaHasilPercenRound = ($uangMukaTunaiPercenRound / 100) * $total_harga;
        $uangMukaTunaiDecimal = round($uangMukaHasilPercenRound);

        // Simulasi Mulia
        $simulasi = $this->muliaSimulasi([
            'uangMuka' => $uangMukaTunaiPercenRound,
            'tenor' => $tenor,
            'idVendor' => $vendor,
            'jenisTransaksi' => 'OP',
            'kepingMulia' => $keping,
            'channelId' => $token->channelId,
            'flag' => 'K',
            'productCode' => '37'
        ]);

        if ($simulasi->responseCode != '00') {
            $this->send_response('error', $simulasi->responseDesc, '', 103);
            log_message('Debug', 'End Of' . __FUNCTION__ . 'error', $simulasi->responseDesc);

            return;
        }

        $simulasiData = json_decode($simulasi->data);
        $simulasiData->totalGramLM = $totalGram;
        $simulasiData->uangMukaRound = $uangMukaTunaiDecimal;

        $this->send_response('success', '', $simulasiData);
        log_message('Debug', 'End Of' . __FUNCTION__ . 'success', $simulasiData);
    }

    function _simulasi_post()
    {
        if (!$this->getToken()) {
            return $this->errorUnAuthorized();
        }

        $this->form_validation->set_data([
            'tenor'       => $this->post('tenor'),
            'vendor'      => $this->post('vendor'),
            'item'        => $this->post('item'),
            'uang_muka'   => $this->post('uang_muka'),
            'total_harga' => $this->post('total_harga')
        ]);

        $this->form_validation->set_rules('tenor', 'tenor', 'callback__valEmptyNumeric');
        $this->form_validation->set_rules('vendor', 'vendor', 'callback__valEmptyNumeric');
        $this->form_validation->set_rules('item[]', 'item', 'required');
        $this->form_validation->set_rules('uang_muka', 'uang_muka', "callback__valMuliaUangMuka[{$this->post('total_harga')}]");
        $this->form_validation->set_rules('total_harga', 'total_harga', 'callback__valEmptyNumeric');

        if ($this->form_validation->run() == false) {
            $validation = Pegadaian::showErrorValidation($this->form_validation->error_array());

            return $this->send_response($this->response_service->fValidationErrResponse($validation));
        }

        $service = $this->mulia_service->simulation($this->post());

        return $this->send_response($service['status'], $service['message'], $service['data']);
    }

    function inquiry_post()
    {
        $token = $this->getToken();
        if ($token) {
            $setData = [
                'tenor' => $this->post('tenor'),
                'vendor' => $this->post('vendor'),
                'item' => $this->post('item'),
                'uang_muka' => $this->post('uang_muka'),
                'kode_outlet' => $this->post('kode_outlet'),
                'promo_code' => $this->post('promo_code')
            ];
            $this->form_validation->set_data($setData);

            $this->form_validation->set_rules('tenor', 'tenor', 'required|integer');
            $this->form_validation->set_rules('vendor', 'vendor', 'required|numeric');
            $this->form_validation->set_rules('item', 'item', 'required');
            $this->form_validation->set_rules('uang_muka', 'uang_muka', 'required|integer');
            $this->form_validation->set_rules('kode_outlet', 'kode_outlet', 'required|numeric');

            if ($this->form_validation->run() == false) {
                $this->set_response([
                    'status' => 'succes',
                    'message' => '',
                    'code' => '101',
                    'error' => $this->form_validation->error_array()
                ]);
            } else {
                // Pastikan user sudah mengisi profile sebelum inquiry
                $user = $this->User->getUser($token->id);

                if ($user->jenis_identitas == ''
                    || $user->nama_ibu == null
                    || $user->tempat_lahir == null
                    || $user->tgl_lahir == null
                    || $user->jenis_kelamin == ''
                    || $user->kewarganegaraan == ''
                    || $user->no_ktp == null
                    || $user->alamat == null
                    || $user->id_kelurahan == ''
                    || $user->status_kawin == ''
                ) {
                    return $this->set_response([
                        'status' => 'error',
                        'message' => 'Mohon lengkapi profile/Link CIF untuk melakukan pemesanan',
                        'code' => 111,
                    ]);
                }

                if ($user) {
                    $item = $this->post('item');
                }
                $promoCode = $this->post('promo_code');

                $arrayOrder = json_decode($item, true);

                if (json_last_error() !== JSON_ERROR_NONE) {
                    $this->set_response(array(
                        'status' => 'error',
                        'message' => 'Invalid input item. Not valid JSON: ' . json_last_error_msg(),
                        'code' => 101
                    ), 200);
                    return;
                }

                $keping = '';
                $count = 1;

                $cartDetails = [];

                foreach ($arrayOrder as $o) {
                    if (isset($o['qty']) && isset($o['gram'])) {
                        $keping = $keping . $o['gram'] . ',' . $o['qty'];

                        // Get denom details
                        $denomDetails = $this->MuliaModel->getDenomDetail($o['gram']);

                        if ($denomDetails) {
                            $cartDetails[] = [
                                'qty' => $o['qty'],
                                'denom' => $o['gram'],
                                'harga' => $denomDetails->harga
                            ];
                        }

                        if ($count < count($arrayOrder)) {
                            $keping = $keping . ';';
                        }
                    }
                    $count++;
                }

                if ($keping == '') {
                    $this->set_response(array(
                        'status' => 'error',
                        'message' => 'Invalid input item',
                        'code' => 101
                    ), 200);
                    return;
                }

                $vendor = $this->post('vendor');
                $uangMuka = $this->post('uang_muka');
                $tenor = $this->post('tenor');
                $kodeOutlet = $this->post('kode_outlet');

                $_inquiryData = [
                    'uangMuka' => $uangMuka,
                    'tenor' => $tenor,
                    'idVendor' => $vendor,
                    'jenisTransaksi' => 'OP',
                    'kepingMulia' => $keping,
                    'channelId' => $token->channelId,
                    'flag' => 'K',
                    'productCode' => '37',
                    'branchCode' => $kodeOutlet
                ];

                // Simulasi Mulia
                $inquiry = $this->muliaInquiry($_inquiryData);

                if ($inquiry->responseCode == '00') {
                    $inquiryData = json_decode($inquiry->data, true);

                    // Add new data
                    $inquiryData['user_AIID'] = $token->id;
                    $inquiryData['kepingMulia'] = $keping;
                    $inquiryData['kepingMuliaJson'] = json_encode($cartDetails);
                    $inquiryData['flag'] = 'K';
                    $inquiryData['idVendor'] = $vendor;
                    $inquiryData['kodeOutlet'] = $kodeOutlet;
                    $inquiryData['realTglTransaksi'] = date('Y-m-d H:i:s');

                    $this->MuliaModel->addPayment($inquiryData);

                    $inquiryData['idTransaksi'] = $inquiryData['reffSwitching'];

                    //Get biaya channel untuk masing-masing metode pembayaran
                    $biayaChannel = $this->Payment_service->getBiayaPayment($inquiryData['jenisTransaksi'], $_inquiryData['productCode']);
                    $inquiryData['biayaChannel'] = $biayaChannel;

                    $this->set_response([
                        'status' => 'success',
                        'message' => '',
                        'data' => $inquiryData
                    ]);
                } else {
                    $this->set_response([
                        'status' => 'error',
                        'code' => 103,
                        'message' => $inquiry->responseDesc
                    ]);
                }
            }
        } else {
            $this->errorUnAuthorized();
        }
    }

    function payment_post()
    {
        $token = $this->getToken();

        if (!$token) {
            $this->errorUnAuthorized();
            return;
        }

        $user = $this->User->getUser($token->id);

        $setData = [
            'id_transaksi' => $this->post('id_transaksi'),
            'payment' => $this->post('payment'),
            'booking_code' => $this->post('booking_code'),
            'card_number' => $this->post('card_number'),
            'token_response' => $this->post('token_response'),
            'pin' => $this->post('pin'),
            'va' => $this->post('va')
        ];

        $this->form_validation->set_data($setData);
        $this->form_validation->set_rules('id_transaksi', 'id_transaksi', 'required');
        $this->form_validation->set_rules('payment', 'payment', 'required');
        $payment = $this->post('payment');

        if ($payment == 'MANDIRI') {
            $this->form_validation->set_rules('booking_code', 'booking_code', 'required');
            $this->form_validation->set_rules('card_number', 'card_number', 'required');
            $this->form_validation->set_rules('token_response', 'token_response', 'required');
        }

        if ($payment == 'WALLET' || $payment == 'GCASH') {
            $this->form_validation->set_rules('pin', 'pin', 'required|exact_length[6]|numeric');
        }

        if ($payment == 'GCASH') {
            $this->form_validation->set_rules('va', 'va', 'required|numeric');
        }

        //Jika payment adalah wallet dan user belum akttifasi wallet
        if ($payment == 'WALLET' && $user->norek == '') {
            $this->set_response([
                'status' => 'error',
                'message' => 'Anda belum melakukan aktifasi wallet',
                'code' => 101
            ], 200);
            return;
        }

        if ($this->form_validation->run() == false) {
            $this->set_response(array(
                'status' => 'error',
                'message' => 'Invalid input',
                'code' => 101,
                'errors' => $this->form_validation->error_array()
            ), 200);
            return;
        }

        $idTransaksi = $this->post('id_transaksi');
        // Mendapatkan promo berdasarkan id transaksi
        $gpoin = $this->GpoinModel->get_promo($idTransaksi);
        //Mendapatkan payment berdasarkan id transaksi
        $checkPayment = $this->MuliaModel->getPaymentByTrxId($idTransaksi, $gpoin);
        log_message("debug", 'detail trx checkPayment => ' . json_encode($checkPayment));

        if (!$checkPayment) {
            log_message('debug', 'Payment not found' . $this->post('id_transaksi'));
            $this->set_response(array(
                'status' => 'success',
                'message' => 'Payment not found',
                'code' => 102
            ), 200);
            return;
        }

        $checkPayment->idTransaksi = $idTransaksi;
        $checkPayment->data_gpoin = $gpoin;
        $checkPayment->data_user = $user;
        $checkPayment->data_token = $token;

        if ($payment == 'FINPAY') {
            $checkPayment->kode_bank = '770';
            $response = $this->mulia_service->finpay_payment($payment, $checkPayment);
            return $this->send_response($response['status'], $response['message'], $response['data']);
        }

        if ($payment == 'VA_MAYBANK') {
            $response = $this->mulia_service->muliaPayment($payment, $checkPayment, $idTransaksi, $gpoin, $user, $token, '016');

            log_message('debug', 'End' . __FUNCTION__ . json_encode($response));

            return $this->send_response($response['status'], $response['message'], $response['data']);
        }

        if ($payment == 'BNI') {
            $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment('OP', 'BANK', '37', '009');

            $createBillingBNI = $this->createBillingVABNI(
                $checkPayment->hargaSetelahDiscount + $biayaTransaksi,
                $user->email,
                $token->nama,
                $token->no_hp,
                'K',
                'OP',
                "PDSOP " . $idTransaksi . " Mulia",
                "",
                '37',
                $idTransaksi,
                $token->channelId,
                $checkPayment->idPromosi,
                $checkPayment->discountAmount
            );

            if ($createBillingBNI->responseCode == '00') {
                if (!isset($createBillingBNI->data)) {
                    $this->set_response(array(
                        'status' => 'error',
                        'message' => 'Kesalahan jaringan mohon coba beberapa saat lagi',
                        'code' => 103,
                        'reason' => $createBillingBNI
                    ), 200);
                    return;
                }

                $billingData = json_decode($createBillingBNI->data);

                $virtualAccount = $billingData->virtualAccount;
                $tglExpired = $billingData->tglExpired;

                $updatePaymentData = array(
                    'tipe' => $payment,
                    'virtualAccount' => $virtualAccount,
                    'tglExpired' => $tglExpired,
                    'payment' => $payment,
                    'biayaTransaksi' => $biayaTransaksi,
                    'kodeBankPembayar' => '009',
                    'channelId' => $token->channelId
                );

                $this->MuliaModel->updatePayment($idTransaksi, $updatePaymentData);

                $dateExpired = new DateTime($tglExpired);

                // Get Cabang Detail
                $outlet = $this->MasterModel->getSingleCabang($checkPayment->kodeOutlet);

                $templateData = [
                    'nama'           => $token->nama,
                    'namaNasabah'    => $token->nama,
                    'payment'        => 'BNI',
                    'nama'           => $token->nama,
                    'va'             => $virtualAccount,
                    'totalKewajiban' => $checkPayment->hargaSetelahDiscount,
                    'tglExpired'     => $dateExpired->format('d/M/Y H:i:s'),
                    'tglExpiredUnformated' => $tglExpired,
                    'reffSwitching'  => $idTransaksi,
                    'trxId'          => $idTransaksi,
                    'biayaTransaksi' => $biayaTransaksi,
                    'namaCabang'     => $outlet->nama,
                    'alamatCabang'   => $outlet->alamat . ', ' . $outlet->kelurahan . ', ' . $outlet->kecamatan . ', ' . $outlet->kabupaten . ', ' . $outlet->provinsi,
                    'telpCabang'     => $outlet->telepon,
                    'item'           => json_decode($checkPayment->kepingMuliaJson),
                    'tglTransaksi'   => $checkPayment->tglTransaksi,
                    'totalHarga'     => $checkPayment->totalHarga,
                    'administrasi'   => $checkPayment->administrasi,
                    'uangMuka'       => $checkPayment->nominalUangMuka,
                    'biayaTransaksi' => $biayaTransaksi,
                    'jenisLogamMulia' => $checkPayment->jenisLogamMulia,
                    'realTglTransaksi' => $checkPayment->realTglTransaksi,
                    'pokokPembiayaan' => $checkPayment->pokokPembiayaan,
                    'tenor' => $checkPayment->tenor,
                    'angsuran' => $checkPayment->angsuran,
                    // Promo
                    'promoCode'       => $checkPayment->promoCode,
                    'discountAmount'  => $checkPayment->discountAmount
                ];

                $this->sendNotifVA($token, $templateData);
            } else {
                $this->set_response([
                    'status' => 'error',
                    'message' => $createBillingBNI->responseDesc,
                    'code' => 103
                ]);
            }
        } elseif ($payment == 'MANDIRI') {
            $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment('OP', 'BANK', '37', '008');
            $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment('OP', 'BANK', '37', '008');

            log_message('debug', 'Mandiri Click Pay');

            $bookingCode   = $this->post('booking_code');
            $cardNumber    = $this->post('card_number');
            $tokenResponse = $this->post('token_response');

            $newTotalKewajiban = $checkPayment->hargaSetelahDiscount + $biayaTransaksi;

            //Bayar dengan mandiri click pay
            $clickPay = $this->mandiriClickPay(
                $newTotalKewajiban,
                $bookingCode,
                $cardNumber,
                'OP',
                'Pembelian Mulia',
                $user->no_hp,
                "",
                '37',
                $tokenResponse,
                $checkPayment->reffSwitching,
                $token->channelId,
                $checkPayment->idPromosi,
                $checkPayment->discountAmount
            );

            if ($clickPay->responseCode == '00') {
                $clickpayData = json_decode($clickPay->data);
                $updateData['reffSwitching']    = $checkPayment->reffSwitching;
                $updateData['bookingCode']      = $bookingCode;
                $updateData['cardNumber']       = $cardNumber;
                $updateData['tokenResponse']    = $tokenResponse;
                $updateData['reffBiller']       = $clickpayData->reffBiller;
                $updateData['kodeBankPembayar'] = '008';
                $updateData['payment']          = $payment;
                $updateData['biayaTransaksi']   = $biayaTransaksiDisplay;
                $updateData['channelId']        = $token->channelId;

                //update data pembayaran
                $this->MuliaModel->updatePayment($updateData);

                //Set response
                $this->set_response([
                    'status'  => 'success',
                    'message' => 'Pembelian Mulia berhasil',
                    'data'    => array(
                        'reffBiller' => $clickpayData->reffBiller,
                        'redaksiPayment' => ''
                    ),
                ], 200);
            } else {
                $this->set_response(array(
                    'status'  => 'error',
                    'code'    => 103,
                    'message' => 'Terjadi kesalahan jaringan mohon coba beberapa saat lagi',
                    'reason'  => $clickPay,
                ));
            }
        } elseif ($payment == 'WALLET' || $payment == 'GCASH') {
            $pin = $this->post('pin');
            $va = $this->post('va');
            //Check user pin
            if (!$this->User->isValidPIN2($token->id, $pin)) {
                $this->set_response(array(
                    'status'  => 'error',
                    'message' => 'PIN tidak valid',
                    'code'    => 102,
                ), 200);

                return;
            }

            $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment('OP', 'WALLET', '37', '');
            $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment('OP', 'WALLET', '37', '');

            $openData = [
                'uangMuka' => $checkPayment->uangMuka,
                'tenor' => $checkPayment->tenor,
                'idVendor' => $checkPayment->idVendor,
                'jenisTransaksi' => 'OP',
                'kepingMulia' => $checkPayment->kepingMulia,
                'channelId' => $token->channelId,
                'flag' => 'K',
                'productCode' => '37',
                'namaNasabah' => $user->nama,
                'ibuKandung' => $user->nama_ibu,
                'tempatLahir' => $user->tempat_lahir,
                'tanggalLahir' => $user->tgl_lahir,
                'kodeCabang' => $checkPayment->kodeOutlet,
                'jenisKelamin' => $user->jenis_kelamin,
                'kewarganegaraan' => $user->kewarganegaraan,
                'statusKawin' => $user->status_kawin,
                'jalan' => $user->alamat,
                'idKelurahan' => $user->id_kelurahan,
                'tipeIdentitas' => $user->jenis_identitas,
                'noIdentitas' => $user->no_ktp,
                'noHp' => $user->no_hp,
                'tanggalExpiredId' => $user->tanggal_expired_identitas,
                'administrasi' => $checkPayment->administrasi,
                'reffSwitching' => $checkPayment->reffSwitching,
                'nominalUangMuka' => $checkPayment->nominalUangMuka,
                'totalKewajiban' => $checkPayment->hargaSetelahDiscount,
                'totalHarga' => $checkPayment->totalHarga,
                'biayaTransaksi' => $biayaTransaksi,
                'jenisLogamMulia' => $checkPayment->jenisLogamMulia,
                'realTglTransaksi' => $checkPayment->realTglTransaksi,
                'pokokPembiayaan' => $checkPayment->pokokPembiayaan,
                'tenor' => $checkPayment->tenor,
                'angsuran' => $checkPayment->angsuran,
                'discountAmount' => $checkPayment->discountAmount,
                'idPromosi' => $checkPayment->idPromosi
            ];

            if ($payment == 'WALLET') {
                $openData['paymentMethod'] = 'WALLET';
                $openData['norekWallet'] = $user->norek;
                $openData['walletId'] = $user->no_hp;
                $openData['surcharge'] = $checkPayment->surcharge;
            } elseif ($payment == 'GCASH') {
                $openData['gcashId'] = $va;
                $openData['paymentMethod'] = 'GCASH';
                $openData['amount'] = $checkPayment->hargaSetelahDiscount;
                $openData['surcharge'] = "0";
            }

            $walletPayment = $this->muliaOpen($openData);

            if ($walletPayment->responseCode == '00') {
                if (!isset($walletPayment->data)) {
                    $this->set_response(array(
                        'status'  => 'error',
                        'message' => 'Terjadi kesalahan, mohon coba beberapa saat lagi',
                        'code'    => 103,
                    ), 200);
                    return;
                }

                $walletData = json_decode($walletPayment->data, true);

                $walletData['isPaid']        = '1';
                $walletData['channelId']     = $token->channelId;
                $walletData['payment'] = $payment;
                $walletData['gcashId'] = $va;

                //Update wallet data
                $this->MuliaModel->updatePayment($checkPayment->reffSwitching, $walletData);


                // Get Cabang Detail
                $outlet = $this->MasterModel->getSingleCabang($checkPayment->kodeOutlet);

                //Send success notification
                $templateData = [
                    'nama'           => $token->nama,
                    'namaNasabah'    => $token->nama,
                    'payment'        => $payment,
                    'nama'           => $token->nama,
                    'va'             => '',
                    'totalKewajiban' => $checkPayment->hargaSetelahDiscount,
                    'tglExpired'     => '',
                    'tglExpiredUnformated' => '',
                    'reffSwitching'  => $idTransaksi,
                    'trxId'          => $idTransaksi,
                    'biayaTransaksi' => $biayaTransaksiDisplay,
                    'namaCabang'     => $outlet->nama,
                    'alamatCabang'   => $outlet->alamat . ', ' . $outlet->kelurahan . ', ' . $outlet->kecamatan . ', ' . $outlet->kabupaten . ', ' . $outlet->provinsi,
                    'telpCabang'     => $outlet->telepon,
                    'item'           => json_decode($checkPayment->kepingMuliaJson),
                    'tglTransaksi'   => $checkPayment->tglTransaksi,
                    'noKredit'       => isset($walletData->noKredit) ? $walletData->noKredit : '-',
                    'totalHarga'     => $checkPayment->totalHarga,
                    'administrasi'     => $checkPayment->administrasi,
                    'uangMuka'       => $checkPayment->uangMuka,
                    'biayaTransaksi' => $checkPayment->biayaTransaksi,
                    'jenisLogamMulia' => $checkPayment->jenisLogamMulia,
                    'realTglTransaksi' => $checkPayment->realTglTransaksi,
                    'pokokPembiayaan' => $checkPayment->pokokPembiayaan,
                    'tenor' => $checkPayment->tenor,
                    'angsuran' => $checkPayment->angsuran,
                    // Promo
                    'promoCode'       => $checkPayment->promoCode,
                    'discountAmount'  => $checkPayment->discountAmount
                ];

                $this->sendNotifVA($token, $templateData);
            } else {
                $this->set_response(array(
                    'status'  => 'error',
                    'message' => 'Nomor telepon terblokir',
                    'code'    => 103,
                    'reason'  => $walletPayment,
                ), 200);
            }
        } elseif ($payment == 'VA_BCA' || $payment == 'VA_MANDIRI' || $payment == 'VA_BRI') {
            $kodeBank = '';
            if ($payment == 'VA_BCA') {
                $kodeBank = '014';
            } elseif ($payment == 'VA_MANDIRI') {
                $kodeBank = '008';
            } elseif ($payment == 'VA_BRI') {
                $kodeBank = '002';
            }

            $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment('OP', 'BANK', '37', $kodeBank);
            $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment('OP', 'BANK', '37', $kodeBank);

            $createBillingData = [
                'channelId' => $token->channelId,
                'amount' => $checkPayment->hargaSetelahDiscount + $biayaTransaksi,
                'customerEmail' => $user->email,
                'customerName' => $user->nama,
                'customerPhone' => $user->no_hp,
                'norek' => $checkPayment->tenor,
                'flag' => 'K',
                'jenisTransaksi' => 'OP',
                'kodeProduk' => '37',
                'keterangan' => "PDSOP " . $idTransaksi . " Mulia",
                'reffSwitching' => $checkPayment->reffSwitching,
                'kodeBank' => $kodeBank,
                'discountAmount' => $checkPayment->discountAmount,
                'idPromosi' => $checkPayment->idPromosi
            ];

            $createBilling = $this->createBillingPegadaian($createBillingData);

            if ($createBilling->responseCode == '00') {
                if (!isset($createBilling->data)) {
                    $this->set_response(array(
                        'status' => 'error',
                        'message' => 'Kesalahan jaringan mohon coba beberapa saat lagi',
                        'code' => 103,
                        'reason' => $createBilling
                    ), 200);
                    return;
                }

                $billingData = json_decode($createBilling->data);

                $virtualAccount = $billingData->vaNumber;
                $tglExpired = $billingData->tglExpired;

                if ($payment == 'VA_BCA') {
                    $dataPrefix = $this->MasterModel->getPrefixBank('prefixBCA');
                    $virtualAccount = $dataPrefix . $billingData->vaNumber;
                    $tglExpired = $billingData->tglExpired;
                }

                $updatePaymentData = array(
                    'tipe' => $payment,
                    'virtualAccount' => $virtualAccount,
                    'tglExpired' => $tglExpired,
                    'payment' => $payment,
                    'biayaTransaksi' => $biayaTransaksi,
                    'kodeBankPembayar' => $kodeBank,
                    'channelId' => $token->channelId
                );

                $this->MuliaModel->updatePayment($idTransaksi, $updatePaymentData);

                $dateExpired = new DateTime($tglExpired);

                // Get Cabang Detail
                $outlet = $this->MasterModel->getSingleCabang($checkPayment->kodeOutlet);

                $templateData = [
                    'nama'           => $token->nama,
                    'namaNasabah'    => $token->nama,
                    'payment'        => $payment,
                    'nama'           => $token->nama,
                    'va'             => $virtualAccount,
                    'totalKewajiban' => $checkPayment->hargaSetelahDiscount,
                    'tglExpired'     => $dateExpired->format('d/M/Y H:i:s'),
                    'tglExpiredUnformated' => $tglExpired,
                    'reffSwitching'  => $idTransaksi,
                    'trxId'          => $idTransaksi,
                    'biayaTransaksi' => $biayaTransaksiDisplay,
                    'namaCabang'     => $outlet->nama,
                    'alamatCabang'   => $outlet->alamat . ', ' . $outlet->kelurahan . ', ' . $outlet->kecamatan . ', ' . $outlet->kabupaten . ', ' . $outlet->provinsi,
                    'telpCabang'     => $outlet->telepon,
                    'item'           => json_decode($checkPayment->kepingMuliaJson),
                    'tglTransaksi'   => $checkPayment->tglTransaksi,
                    'totalHarga'     => $checkPayment->totalHarga,
                    'administrasi'   => $checkPayment->administrasi,
                    'uangMuka'       => $checkPayment->nominalUangMuka,
                    'biayaTransaksi' => $biayaTransaksi,
                    'jenisLogamMulia' => $checkPayment->jenisLogamMulia,
                    'realTglTransaksi' => $checkPayment->realTglTransaksi,
                    'pokokPembiayaan' => $checkPayment->pokokPembiayaan,
                    'tenor' => $checkPayment->tenor,
                    'angsuran' => $checkPayment->angsuran,
                    // Promo
                    'promoCode'       => $checkPayment->promoCode,
                    'discountAmount'  => $checkPayment->discountAmount
                ];

                $this->sendNotifVA($token, $templateData);
            } else {
                $this->set_response([
                    'status' => 'error',
                    'message' => $createBilling->responseDesc,
                    'code' => 103
                ]);
            }
        } elseif ($payment == 'VA_PERMATA') {
            $kodeBank = '013';

            $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment('OP', 'BANK', '37', $kodeBank);
            $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment('OP', 'BANK', '37', $kodeBank);

            $createBillingData = [
                'channelId' => $token->channelId,
                'amount' => $checkPayment->hargaSetelahDiscount + $biayaTransaksi,
                'cif' => $user->cif,
                'customerEmail' => $user->email,
                'customerName' => $user->nama,
                'customerPhone' => $user->no_hp,
                'flag' => 'K',
                'jenisTransaksi' => 'OP',
                'productCode' => '37',
                'norek' => '',
                'keterangan' => "PDSOP " . $idTransaksi . " Mulia",
                'trxId' => $checkPayment->reffSwitching,
                'discountAmount' => $checkPayment->discountAmount,
                'idPromosi' => $checkPayment->idPromosi
            ];

            $createBilling = $this->createBillingPermata($createBillingData);

            if ($createBilling->responseCode == '00') {
                if (!isset($createBilling->data)) {
                    $this->set_response(array(
                        'status' => 'error',
                        'message' => 'Kesalahan jaringan mohon coba beberapa saat lagi',
                        'code' => 103,
                        'reason' => $createBilling
                    ), 200);
                    return;
                }

                $billingData = json_decode($createBilling->data);

                $virtualAccount = $billingData->virtualAccount;
                $tglExpired = $billingData->tglExpired;

                $updatePaymentData = array(
                    'tipe' => $payment,
                    'virtualAccount' => $virtualAccount,
                    'tglExpired' => $tglExpired,
                    'payment' => $payment,
                    'biayaTransaksi' => $biayaTransaksiDisplay,
                    'kodeBankPembayar' => $kodeBank,
                    'channelId' => $token->channelId
                );

                $this->MuliaModel->updatePayment($idTransaksi, $updatePaymentData);

                $dateExpired = new DateTime($tglExpired);

                // Get Cabang Detail
                $outlet = $this->MasterModel->getSingleCabang($checkPayment->kodeOutlet);

                $templateData = [
                    'nama'           => $token->nama,
                    'namaNasabah'    => $token->nama,
                    'payment'        => $payment,
                    'nama'           => $token->nama,
                    'va'             => $virtualAccount,
                    'totalKewajiban' => $checkPayment->hargaSetelahDiscount,
                    'tglExpired'     => $dateExpired->format('d/M/Y H:i:s'),
                    'tglExpiredUnformated' => $tglExpired,
                    'reffSwitching'  => $idTransaksi,
                    'trxId'          => $idTransaksi,
                    'biayaTransaksi' => $biayaTransaksiDisplay,
                    'namaCabang'     => $outlet->nama,
                    'alamatCabang'   => $outlet->alamat . ', ' . $outlet->kelurahan . ', ' . $outlet->kecamatan . ', ' . $outlet->kabupaten . ', ' . $outlet->provinsi,
                    'telpCabang'     => $outlet->telepon,
                    'item'           => json_decode($checkPayment->kepingMuliaJson),
                    'tglTransaksi'   => $checkPayment->tglTransaksi,
                    'totalHarga'     => $checkPayment->totalHarga,
                    'administrasi'   => $checkPayment->administrasi,
                    'uangMuka'       => $checkPayment->nominalUangMuka,
                    'biayaTransaksi' => $biayaTransaksi,
                    'jenisLogamMulia' => $checkPayment->jenisLogamMulia,
                    'realTglTransaksi' => $checkPayment->realTglTransaksi,
                    'pokokPembiayaan' => $checkPayment->pokokPembiayaan,
                    'tenor' => $checkPayment->tenor,
                    'angsuran' => $checkPayment->angsuran,
                    // Promo
                    'promoCode'       => $checkPayment->promoCode,
                    'discountAmount'  => $checkPayment->discountAmount
                ];

                $this->sendNotifVA($token, $templateData);
            } else {
                $errMsg = isset($createBilling->responseDesc) ? $createBilling->responseDesc
                    : 'Terjadi kesalahan. Mohon coba beberapa saat lagi';

                $this->set_response([
                    'status' => 'error',
                    'message' => $errMsg,
                    'code' => 103
                ]);
            }
        }
        return;
    }

    function validate_payment_mulia_post()
    {
        $token = $this->getToken();

        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', __FUNCTION__ . ' Validasi Pembayaran Mulia Error ' . json_decode($this->errorUnAuthorized()));
            return;
        }

        $this->form_validation->set_rules('id_transaksi', 'id_transaksi', 'required');
        $this->form_validation->set_rules('payment', 'payment', 'required');

        if ($this->form_validation->run() == false) {
            log_message('debug', __FUNCTION__ . ' Validasi Pembayaran Mulia Error', $this->form_validation->error_array());
            return $this->send_response('error', 'Invalid Input', $this->form_validation->error_array(), 101);
        }

        $idTransaksi = $this->post('id_transaksi');

        $response = $this->mulia_service->validatePaymentGcash($token, $idTransaksi);

        log_message('Debug', 'End Of' . __FUNCTION__ . 'success', $response);
        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    function history_get($id = null)
    {
        $token = $this->getToken();
        if ($token) {
            $history = $this->MuliaModel->getHistory($token->id, $id);
            if ($history) {
                $this->set_response([
                    'status' => 'success',
                    'message' => '',
                    'data' => $history
                ]);
            } else {
                $this->set_response([
                    'status' => 'error',
                    'message' => 'History tidak ditemukan',
                    'code' => '102'
                ]);
            }
        } else {
            $this->errorUnAuthorized();
        }
    }

    function vendor_get()
    {
        $token = $this->getToken();
        if ($token) {
            $vendor = $this->MuliaModel->getVendor();
            $this->set_response([
                'status' => 'success',
                'message' => '',
                'data' => $vendor
            ]);
        } else {
            $this->errorUnAuthorized();
        }
    }

    function kontrak_get()
    {
        $headers = $this->input->request_headers();
        if (Authorization::tokenIsExist($headers)) {
            $token = Authorization::validateMuliaToken($headers['Authorization']);

            if (isset($token)) {
                // Check apakah payment benar benar ada
                $payment = $this->MuliaModel->getPaymentByTrxId($token->reffSwitching);

                if ($payment) {
                    // Untuk security pastikan email token dan data user cocok
                    if ($payment->emailNasabah == $token->email) {
                        // Jumlah keping dan jumlah gram
                        $jumlahKeping = 0;
                        $jumlahGram = 0;

                        $itemData = json_decode($payment->kepingMuliaJson);

                        foreach ($itemData as $i) {
                            $jumlahGram = $jumlahGram + $i->denom;
                            $jumlahKeping = $jumlahKeping + $i->qty;
                        }

                        // Pokok pinjaman= marginBersih / tenor
                        $pokokPinjaman = $payment->marginBersih / $payment->tenor;

                        $tglKredit = $payment->tglKredit;

                        // Ekstrak terkait tanggal
                        $hariTerbilangKredit = $this->_toDay($tglKredit);
                        $tglAngsuran = substr($tglKredit, 8, 2);
                        $tglTerbilangKredit = $this->_toBilangan($tglAngsuran);
                        $bulanTerbilanKredit = $this->_toBulan(substr($tglKredit, 5, 2));
                        $tahunTerbilangKredit = $this->_toBilangan(substr($tglKredit, 0, 4));

                        // Get outlet pegadaian
                        $outlet = $this->MasterModel->getSingleCabang($payment->kodeOutlet);

                        $sewaModal = $payment->marginBersih *  $payment->tenor;

                        // Get user full address
                        $fullAddress = $this->User->getFullAddress($payment->user_AIID);

                        // Set response data
                        $kontrakData = [
                            'noKredit' => $payment->noKredit,
                            'tglKredit' => $payment->tglKredit,
                            'hariTerbilangKredit' => $hariTerbilangKredit,
                            'tglTerbilangKredit' => $tglTerbilangKredit,
                            'bulanTerbilanKredit' => $bulanTerbilanKredit,
                            'tahunTerbilangKredit' => $tahunTerbilangKredit,
                            'nama' => $payment->namaNasabah,
                            'alamat' => $fullAddress !== null ? strtoupper($fullAddress) : $payment->alamatNasabah,
                            'noTelp' => $payment->noHpNasabah,
                            'noKtp' => $payment->noKtpNasabah,
                            'namaCabang' => $outlet->nama,
                            'pokokPembiayaan' => $payment->pokokPembiayaan,
                            'pokokPembiayaanTerbilang' => $this->_toBilangan($payment->pokokPembiayaan),
                            'jumlahGram' => $jumlahGram,
                            'jumlahKeping' => $jumlahKeping,
                            'jenisLogamMulia' => $payment->jenisLogamMulia,
                            'totalHarga' => $payment->totalHarga,
                            'totalHargaTerbilang' => $this->_toBilangan($payment->totalHarga),
                            'tenor' => $payment->tenor,
                            'tenorTerbilang' => $this->_toBilangan($payment->tenor),
                            'marginBersih' => $payment->marginBersih,
                            'marginBersihTerbilang' => $this->_toBilangan($payment->marginBersih),
                            'administrasi' => $payment->administrasi,
                            'administrasiTerbilangan' => $this->_toBilangan($payment->administrasi),
                            'angsuran' => $payment->angsuran,
                            'angsuranTerbilang' => $this->_toBilangan($payment->angsuran),
                            'tglAngsuran' => $tglAngsuran,
                            'tglAngsuranTerbilang' => $tglTerbilangKredit,
                            'sewaModal' => $sewaModal,
                            'sewaModalTerbilang' =>  $this->_toBilangan($sewaModal)
                        ];

                        $this->set_response([
                            'status' => 'success',
                            'message' => '',
                            'data' => $kontrakData
                        ]);
                    } else {
                        $this->set_response([
                            'status' => 'error',
                            'code' => 102,
                            'message' => 'Data tidak ditemukan'
                        ]);
                    }
                } else {
                    $this->set_response([
                        'status' => 'error',
                        'code' => 102,
                        'message' => 'Data tidak ditemukan'
                    ]);
                }
            } else {
                $this->set_response([
                    'status' => 'error',
                    'code' => 102,
                    'message' => 'Data tidak ditemukan'
                ]);
            }
        } else {
            $this->errorForbbiden();
            return;
        }
    }

    function _toWord($x)
    {
        $x = abs($x);
        $angka = array(
            "", "satu", "dua", "tiga", "empat", "lima",
            "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas"
        );
        $temp = "";
        if ($x < 12) {
            $temp = " " . $angka[$x];
        } elseif ($x < 20) {
            $temp = $this->_toWord($x - 10) . " belas";
        } elseif ($x < 100) {
            $temp = $this->_toWord($x / 10) . " puluh" . $this->_toWord($x % 10);
        } elseif ($x < 200) {
            $temp = " seratus" . $this->_toWord($x - 100);
        } elseif ($x < 1000) {
            $temp = $this->_toWord($x / 100) . " ratus" . $this->_toWord($x % 100);
        } elseif ($x < 2000) {
            $temp = " seribu" . $this->_toWord($x - 1000);
        } elseif ($x < 1000000) {
            $temp = $this->_toWord($x / 1000) . " ribu" . $this->_toWord($x % 1000);
        } elseif ($x < 1000000000) {
            $temp = $this->_toWord($x / 1000000) . " juta" . $this->_toWord($x % 1000000);
        } elseif ($x < 1000000000000) {
            $temp = $this->_toWord($x / 1000000000) . " milyar" . $this->_toWord(fmod($x, 1000000000));
        } elseif ($x < 1000000000000000) {
            $temp = $this->_toWord($x / 1000000000000) . " trilyun" . $this->_toWord(fmod($x, 1000000000000));
        }
        return $temp;
    }


    function _toBilangan($x, $style = 2)
    {
        if ($x < 0) {
            $hasil = "minus " . trim($this->_toWord($x));
        } else {
            $hasil = trim($this->_toWord($x));
        }
        switch ($style) {
            case 1:
                $hasil = strtoupper($hasil);
                break;
            case 2:
                $hasil = strtolower($hasil);
                break;
            case 3:
                $hasil = ucwords($hasil);
                break;
            default:
                $hasil = ucfirst($hasil);
                break;
        }
        return $hasil;
    }

    function _toDay($strDate)
    {
        $day = date('D', strtotime($strDate));
        $dayList = array(
            'Sun' => 'Minggu',
            'Mon' => 'Senin',
            'Tue' => 'Selasa',
            'Wed' => 'Rabu',
            'Thu' => 'Kamis',
            'Fri' => 'Jumat',
            'Sat' => 'Sabtu'
        );
        return $dayList[$day];
    }

    function _toBulan($bulan)
    {
        switch ($bulan) {
            case 1:
                $bulan = "Januari";
                break;
            case 2:
                $bulan = "Februari";
                break;
            case 3:
                $bulan = "Maret";
                break;
            case 4:
                $bulan = "April";
                break;
            case 5:
                $bulan = "Mei";
                break;
            case 6:
                $bulan = "Juni";
                break;
            case 7:
                $bulan = "Juli";
                break;
            case 8:
                $bulan = "Agustus";
                break;
            case 9:
                $bulan = "September";
                break;
            case 10:
                $bulan = "Oktober";
                break;
            case 11:
                $bulan = "November";
                break;
            case 12:
                $bulan = "Desember";
                break;
        }
        return $bulan;
    }


    /**
     * Param templateData:
     * - nama
     * - totalKewajiban
     * - reffSwitching
     * - biayaTransaksi
     * - va
     * - tglExpired
     * - namaCabang
     * - alamatCabang
     * - telpCabang
     * - item (json)
     *
     * @param $token object user token
     * @param $templateData template data
     * @return void
     */
    function sendNotifVA($token, $templateData)
    {
        $template = $this->generateNotif($templateData);

        $emailTemplate   = $template['email'];
        $mobileTemplate  = $template['mobile'];
        $minimalTemplate = $template['minimal'];

        $totalBayar = $templateData['totalKewajiban'] + $templateData['biayaTransaksi'];
        $fTotalBayar = number_format($totalBayar, 0, ",", ".");

        $subtitle = "Segera Bayar Rp " . $fTotalBayar . " ke " . $templateData['va'];

        // Simpan notifikasi baru
        $idNotif = $this->NotificationModel->add(
            $token->id,
            NotificationModel::TYPE_MPO,
            NotificationModel::CONTENT_TYPE_HTML,
            $this->ConfigModel->getNamaProduk('OP', '37'),
            $subtitle,
            $mobileTemplate,
            $minimalTemplate,
            "ML"
        );

        //Kirim notifikasi pembayaran ke device user
        Message::sendFCMNotif(
            $this->User->getFCMToken($token->id),
            [
                "id"      => $idNotif,
                "tipe"    => "ML",
                "title"   => $this->ConfigModel->getNamaProduk('OP', '37'),
                "tagline" => $subtitle,
                "content" => "Bayar ke " . $templateData['va'] . " sebelum " . $templateData['tglExpired'],
                "token"   => $token->no_hp,
            ]
        );

        //Kirim Email Notifikasi
        $this->load->helper('message');
        Message::sendEmail($token->email, "Pembelian Mulia", $emailTemplate);

        //Set response
        $this->set_response([
            'status'  => 'success',
            'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
            'data'    => [
                'idTransaksi'    => $templateData['reffSwitching'],
                'virtualAccount' => $templateData['va'],
                'expired'        => $templateData['tglExpiredUnformated'],
                'now'            => date('Y-m-d H:i:s'),
            ],
        ], 200);
    }

    /**
     * Generate template notifikasi mobile, email dan versi minify untuk content webview
     * @param array $data
     * @return void
     */
    public function generateNotif($data)
    {
        $subject = "Pembelian Logam Mulia";

        $content = $this->load->view('mail/mulia/inquiry', $data, true);

        $message = $this->load->view('mail/email_template_top', array('title' => $subject), true);
        $message = $message . $content;
        $message = $message . $this->load->view('mail/email_template_bottom', array(), true);

        $mobile = $this->load->view('notification/top_template', array('title' => $subject), true);
        $mobile = $mobile . $content;
        $mobile = $mobile . $this->load->view('notification/bottom_template', array(), true);

        return array(
            'email'   => $message,
            'mobile'  => $mobile,
            'minimal' => $content,
        );
    }


    function _valMuliaUangMuka($uangMuka, $total_harga)
    {
        if (empty($uangMuka)) {
            $this->form_validation->set_message('_valMuliaUangMuka', 'Input {field} tidak boleh kosong.');
            return false;
        }

        if (!is_numeric($uangMuka)) {
            $this->form_validation->set_message('_valMuliaUangMuka', 'Input {field} harus diisi dengan angka');
            return false;
        }

        $uangMukaTunai = ($total_harga!=0) ? ($uangMuka / (int)$total_harga) * 100 : 0;
        $uangMukaTunaiPercenRound = round($uangMukaTunai);
        $uangMukaHasilPercenRound = ($uangMukaTunaiPercenRound / 100) * (int)$total_harga;
        $uangMukaTunaiDecimal = round($uangMukaHasilPercenRound);

        if ($uangMukaTunaiPercenRound < 15) {
            $this->form_validation->set_message('_valMuliaUangMuka', 'Minimal uang muka 15%.');
            return false;
        }

        if ($uangMukaTunaiPercenRound > 90) {
            $this->form_validation->set_message('_valMuliaUangMuka', 'Maksimal uang muka 90%.');
            return false;
        }

        return true;
    }

    function _valEmptyNumeric($post)
    {
        if (empty($post)) {
            $this->form_validation->set_message('_valEmptyNumeric', 'Input {field} tidak boleh kosong.');
            return false;
        }

        if (!is_numeric($post)) {
            $this->form_validation->set_message('_valEmptyNumeric', 'Input {field} harus diisi dengan angka');
            return false;
        }
    }
}
