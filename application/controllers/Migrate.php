<?php
class migrate extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->service('Master_service', 'master_service');
    }

    public function index()
    {
        if (!$this->master_service->authToken()) {
            echo 'Unauthorized Access';
            return;
        }
        $this->load->library('migration');

        if ($this->migration->current() === false) {
            show_error($this->migration->error_string());
            return;
        } else {
            echo 'Migrations ran successfully!';
            return;
        }
    }
}
