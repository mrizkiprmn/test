<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once 'CorePegadaian.php';

/**
 * @property Gtef_service gtef_service
 */
class Gtef extends CorePegadaian
{
    private $auth;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->service('Gtef_service', 'gtef_service');

        $this->auth = $this->getToken();
    }

    function onboarding_get()
    {
        if (empty($this->auth)) {
            return $this->errorUnAuthorized();
        }

        $response = $this->gtef_service->checkOnboarding($this->auth->id);
        return $this->send_response($response);
    }

    function list_rekening_get()
    {
        log_message('debug', 'START OF CONTROLLER ' . __FUNCTION__);

        if (empty($this->auth)) {
            return $this->errorUnAuthorized();
        }

        $response = $this->gtef_service->listRekening($this->auth->id);

        log_message('Debug', 'END OF CONTROLLER ' . __FUNCTION__);

        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    function riwayatTitipanEmas_get()
    {
        $token = $this->getToken();

        if (empty($token)) {
            log_message('debug', 'end of ' . __FUNCTION__ . ' =>  Empty Token');
            return $this->errorUnAuthorized();
        }

        log_message('debug', 'end of ' . __FUNCTION__ . ' =>  REQUEST' . $token->id);

        $response = $this->gtef_service->listRiwayatTE($token->id);

        log_message('debug', 'end of ' . __FUNCTION__ . ' => ' . json_encode($response));
        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    function detail_post()
    {
        $token = $this->getToken();
        log_message('debug', 'Start' . __FUNCTION__ . json_encode($this->getToken()));

        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', 'UnAuthorized ' . __FUNCTION__ . json_encode($this->errorUnAuthorized()));
            return;
        }

        $this->form_validation->set_rules('noKontrak', 'noKontrak', 'required');

        if (!$this->form_validation->run()) {
            $validation = Pegadaian::showErrorValidation($this->form_validation->error_array());
            
            return $this->send_response('error', 'Validation Exception', $validation, 101);
        }

        $noKontrak = $this->post('noKontrak');
        $response = $this->gtef_service->getDetailGadaiTitipanEmas($noKontrak);
        log_message('debug', 'End' . __FUNCTION__ . json_encode($response));

        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    public function cekstatus_post()
    {
        log_message('debug', __FUNCTION__ . 'data');
        $token = $this->getToken();

        if (empty($token)) {
            log_message('debug', 'response ' . __FUNCTION__ . 'UnAuthorized');
            return $this->errorUnAuthorized();
        }

        $this->form_validation->set_rules('nomor_rekening', 'Nomor Titipan', 'required|numeric');

        if (!$this->form_validation->run()) {
            $validation = Pegadaian::showErrorValidation($this->form_validation->error_array());
            log_message('debug', 'END ' . __FUNCTION__ . ' => ' . json_encode($validation));

            return $this->send_response('error', 'Validation Exception', $validation, '101');
        }

        $response = $this->gtef_service->validasiPopupNotifikasi($token->id, $this->post());
        log_message('debug', 'success' . __FUNCTION__ . json_encode($response));

        log_message('debug', __FUNCTION__ . 'End');
        return $this->send_response($response['status'], $response['message'], $response['data'], $response['code']);
    }

    function cekPengajuanPlafon_get()
    {
        $token = $this->getToken();

        if (empty($token)) {
            log_message('debug', 'end of ' . __FUNCTION__ . ' =>  Empty Token');
            return $this->errorUnAuthorized();
        }

        log_message('debug', 'end of ' . __FUNCTION__ . ' =>  REQUEST' . $token->id);

        $response = $this->gtef_service->cekPengajuanPlafon($token->id);

        log_message('debug', 'success' . __FUNCTION__ . json_encode($response));
        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    function cekPengajuanPinjaman_get()
    {
        $token = $this->getToken();
        log_message('debug', 'Start' . __FUNCTION__ . json_encode($this->getToken()));

        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', 'UnAuthorized' . __FUNCTION__ . json_encode($this->errorUnAuthorized()));
            return;
        }

        $response = $this->gtef_service->cekPengajuanTitipanEmas($token->id);
        log_message('debug', 'End' . __FUNCTION__ . json_encode($response));

        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    function cekPenyelesaianKredit_get()
    {
        $token = $this->getToken();
        log_message('debug', 'Start' . __FUNCTION__ . json_encode($this->getToken()));

        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', 'UnAuthorized' . __FUNCTION__ . json_encode($this->errorUnAuthorized()));

            return;
        }

        $setData = array(
            'tipe_Transaksi' => $this->query('tipe_Transaksi'),
            'no_Kredit' => $this->query('no_Kredit'),
        );

        $this->form_validation->set_data($setData);
        $this->form_validation->set_rules('tipe_Transaksi', 'tipe_Transaksi', 'required');
        $this->form_validation->set_rules('no_Kredit', 'no_Kredit', 'required');

        if (!$this->form_validation->run()) {
            $error = $this->send_response('error', $this->form_validation->error_array(), 101);
            log_message('debug', 'Error' . __FUNCTION__ . ' Message : ' . json_encode($error));
            return $error;
        }

        $data = $this->query();

        $response = $this->gtef_service->cekDetailPenyelesaianKredit($data);
        log_message('debug', 'End' . __FUNCTION__ . json_encode($response));

        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    function titipan_portofolio_get()
    {
        log_message('debug', __FUNCTION__ . ' Titipan Portofolio Emas ' . 'Start');
        $token = $this->getToken();

        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', __FUNCTION__ . ' Titipan Portofolio Emas Error ' . json_encode($this->errorUnAuthorized()));

            return;
        }

        $this->form_validation->set_data(['cif' => $this->query('cif')]);
        $this->form_validation->set_rules('cif', 'CIF', 'numeric|required');

        if (!$this->form_validation->run()) {
            $this->send_response('error', $this->form_validation->error_array(), 101);

            log_message('debug', __FUNCTION__  . ' Titipan Portofolio Emas Error ' . json_encode($this->form_validation->error_array()));
            return;
        }

        $response = $this->gtef_service->getTitipanPortofolio($this->query('cif'));

        log_message('debug', __FUNCTION__ . ' Titipan Portofolio Emas ' . 'End');
        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    function no_titipan_favorite_post()
    {
        $token = $this->getToken();
        // Token Validation
        if (!$token) {
            $error = $this->errorUnAuthorized();
            log_message('debug', 'error ' . __FUNCTION__ . ' => ' . $error);
            return;
        }

        $this->form_validation->set_data(['no_rekening' => $this->post('no_rekening'), 'tipe' => $this->post('tipe'), 'jenis_transaksi' => $this->post('jenis_transaksi'), 'nama_nasabah' => $this->post('nama')]);
        // set Validation
        $this->form_validation->set_rules('no_rekening', 'no_rekening', 'required|numeric');
        $this->form_validation->set_rules('tipe', 'tipe', 'required|callback_checkTipeFavorite');
        $this->form_validation->set_rules('jenis_transaksi', 'jenis_transaksi', 'required');
        $this->form_validation->set_rules('nama_nasabah', 'nama', 'required');

        if ($this->form_validation->run() == false) {
            log_message('debug', 'error ' . __FUNCTION__ . ' => ' . json_encode($this->form_validation->error_array()));
            return $this->send_response('error', $this->form_validation->error_array() ?? null, '', 101);
        }

        $dataResponse = $this->gtef_service->addNoTitipanFavorite($this->post(), $token);
        log_message('debug', 'end of ' . __FUNCTION__ . ' => ' . json_encode($dataResponse));

        return $this->send_response('success', '', $dataResponse);
    }

    function no_titipan_favorite_get()
    {
        $token = $this->getToken();

        if (!$token) {
            $error = $this->errorUnAuthorized();
            log_message('debug', 'error ' . __FUNCTION__ . ' => ' . $error);
            return;
        }

        $this->form_validation->set_data(['tipe' => $this->query('tipe')]);
        $this->form_validation->set_rules('tipe', 'tipe', 'required|callback_checkTipeFavorite');

        if ($this->form_validation->run() == false) {
            log_message('debug', 'error ' . __FUNCTION__ . ' => ' . json_encode($this->form_validation->error_array()));
            return $this->send_response('error', $this->form_validation->error_array() ?? null, '', 101);
        }

        $favorite = $this->User->getFavorite($token->id, $this->query('tipe'));
        
        $res = $this->send_response('success', '', $favorite);
        log_message('debug', 'end of ' . __FUNCTION__ . ' => ' . json_encode($res));
    }

    function checkTipeFavorite($str)
    {
        if (empty($str)) {
            $this->form_validation->set_message('checkTipeFavorite', 'tipe favorite tidak valid');
            return false;
        }
        
        $allowedStr = array('emas', 'payment', 'gadai', 'mikro', 'gcash', 'mpo', 'titipan');
        if (!in_array($str, $allowedStr)) {
            $this->form_validation->set_message('checkTipeFavorite', 'tipe favorite tidak valid');
            return false;
        }
        
        return true;
    }

    /**
   * Endpoint untuk melakukan inquiry payment titipan emas
   */
    function inquiry_titipan_emas_post()
    {
        if (empty($this->auth)) {
            log_message('debug', 'UnAuthorized' . __FUNCTION__ . json_encode($this->errorUnAuthorized()));
            return $this->errorUnAuthorized();
        }

        $request = $this->post();

        log_message('debug', 'START ' . __FUNCTION__ . ' => ' . json_encode($request));

        $this->form_validation->set_rules('product', 'Produk', 'required');
        $this->form_validation->set_rules('noKontrak', 'No Rekening Titipan Emas', 'required');

        if ($this->form_validation->run() == false) {
            $validation = Pegadaian::showErrorValidation($this->form_validation->error_array());
            log_message('debug', 'END ' . __FUNCTION__ . ' => ' . json_encode($validation));

            return $this->send_response('error', 'Validation Exception', $validation, '101');
        }

        $response = $this->gtef_service->inquiryTitipanEmas($this->auth->id, $request);

        log_message('debug', 'END ' . __FUNCTION__ . ' => ' . json_encode($response));

        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    function cekNorekInquiryPayment_get()
    {
        log_message('debug', __FUNCTION__ . ' Inquiry Payment GTEF ' . 'Start');
        if (empty($this->auth)) {
            log_message('debug', 'UnAuthorized' . __FUNCTION__ . json_encode($this->errorUnAuthorized()));
            return $this->errorUnAuthorized();
        }

        $this->form_validation->set_data(['noKontrak' => $this->query('noKontrak')]);
        $this->form_validation->set_rules('noKontrak', 'No Rekening Titipan Emas', 'required');

        if (!$this->form_validation->run()) {
            $error = $this->send_response('error', $this->form_validation->error_array(), 101);
            log_message('debug', 'Error' . __FUNCTION__ . ' Message : ' . json_encode($error));
            return $error;
        }

        $response = $this->gtef_service->getNorekInquiryPayment($this->query('noKontrak'));
        log_message('debug', 'End' . __FUNCTION__ . json_encode($response));

        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    function list_rekening_group_gadai_get()
    {
        if (empty($this->auth)) {
            return $this->errorUnAuthorized();
        }

        $response = $this->gtef_service->list_kredit_aktif_group_gadai($this->auth->id);

        return $this->send_response($response);
    }
}
