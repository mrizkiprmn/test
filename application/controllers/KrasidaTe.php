<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once 'CorePegadaian.php';

class KrasidaTe extends CorePegadaian
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->service('KrasidaTe_service', 'KrasidaTe_service');

        $this->auth = $this->getToken();
    }

    public function calculate_qr_data_post()
    {
        $token = $this->getToken();
        if (empty($token)) {
            return $this->errorUnAuthorized();
        }

        $this->form_validation->set_data($this->post());
        $this->form_validation->set_error_delimiters('', '');
        $this->form_validation->set_rules('url', 'url', 'required');
        
        if (!$this->form_validation->run()) {
            $this->send_response('error', $this->form_validation->error_string(), '', 101);
        }
        
        $response = $this->KrasidaTe_service->calculateDataFromQr($this->post());

        $this->send_response($response);
    }

    public function validate_saldo_blokir_post()
    {
        $token = $this->getToken();

        if (empty($token)) {
            return $this->errorUnAuthorized();
        }

        $this->form_validation->set_data($this->post());
        $this->form_validation->set_rules('saldoBlokir', 'saldoBlokir', 'required');
        $this->form_validation->set_rules('noRekening', 'noRekening', 'required');

        if (!$this->form_validation->run()) {
            $this->send_response('error', $this->form_validation->error_array(), '', 101);
        }

        $response = $this->KrasidaTe_service->validateSaldoBlokir($this->post(), $token);

        $this->send_response($response);
    }

    public function list_tenor_get()
    {
        $token = $this->getToken();

        if (empty($token)) {
            return $this->errorUnAuthorized();
        }

        $this->form_validation->set_data($this->query());
        $this->form_validation->set_rules('totalHarga', 'totalHarga', 'required');

        if (!$this->form_validation->run()) {
            $this->send_response('error', $this->form_validation->error_array(), '', 101);
        }

        $response = $this->KrasidaTe_service->getListTenor($this->query());

        $this->send_response($response);
    }

    public function inquiry_krasida_te_post()
    {
        $token = $this->getToken();

        if (empty($token)) {
            return $this->errorUnAuthorized();
        }

        $this->form_validation->set_data($this->post());
        $this->form_validation->set_rules('tenor', 'tenor', 'required');
        $this->form_validation->set_rules('totalHarga', 'totalHarga', 'required');
        $this->form_validation->set_rules('noRekening', 'noRekening', 'required');
        $this->form_validation->set_rules('saldoBlokir', 'saldoBlokir', 'required');

        if (!$this->form_validation->run()) {
            $this->send_response('error', $this->form_validation->error_array(), '', 101);
        }
        
        $response = $this->KrasidaTe_service->inquiryCicilEmas($this->post(), $token);

        $this->send_response($response);
    }

    public function submit_krasida_te_post()
    {
        $token = $this->getToken();

        if (empty($token)) {
            return $this->errorUnAuthorized();
        }

        $this->form_validation->set_data($this->post());
        $this->form_validation->set_rules('pin', 'pin', 'required');
        $this->form_validation->set_rules('norek', 'norek', 'required');
        $this->form_validation->set_rules('up', 'up', 'required');
        $this->form_validation->set_rules('tenor', 'tenor', 'required');
        $this->form_validation->set_rules('reffSwitching', 'reffSwitching', 'required');
        $this->form_validation->set_rules('surcharge', 'surcharge', 'required');
        $this->form_validation->set_rules('totalKewajiban', 'totalKewajiban', 'required');
        $this->form_validation->set_rules('lockGram', 'lockGram', 'required');
        $this->form_validation->set_rules('administrasi', 'administrasi', 'required');
        $this->form_validation->set_rules('tglTransaksi', 'tglTransaksi', 'required');
        $this->form_validation->set_rules('angsuran', 'angsuran', 'required');

        if (!$this->form_validation->run()) {
            return $this->send_response('error', $this->form_validation->error_string(), '', 101);
        }
        
        $response = $this->KrasidaTe_service->submitCicilEmas($token, $this->post());

        $this->send_response($response['status'], $response['message'], $response['data'], $response['code']);
    }

    public function riwayat_krasida_te_get()
    {
        $token = $this->getToken();

        if (empty($token)) {
            return $this->errorUnAuthorized();
        }

        $response = $this->KrasidaTe_service->RiwayatKrasidaTe($token);

        $this->send_response($response['status'], $response['message'], $response['data'], $response['code']);
    }

    public function detail_riwayat_krasida_te_get()
    {
        $token = $this->getToken();

        if (empty($token)) {
            return $this->errorUnAuthorized();
        }

        $this->form_validation->set_data($this->query());
        $this->form_validation->set_rules('noKontrak', 'noKontrak', 'required');

        if (!$this->form_validation->run()) {
            return $this->send_response('error', $this->form_validation->error_array(), '', 101);
        }

        $response = $this->KrasidaTe_service->detailRiwayatKrasidaTe($this->query());

        $this->send_response($response['status'], $response['message'], $response['data'], $response['code']);
    }

    public function is_already_cicil_emas_get()
    {
        $token = $this->getToken();

        if (empty($token)) {
            return $this->errorUnAuthorized();
        }

        $response = $this->KrasidaTe_service->isAlreadyCicilEmas($token);

        return $this->send_response($response);
    }
}
