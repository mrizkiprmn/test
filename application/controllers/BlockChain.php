<?php

defined('BASEPATH') or exit('No direct script access allowed');

require_once 'CorePegadaian.php';

/**
 * @property BlockChain_service block_chain_service
 * @property Response_service response_service
 */
class BlockChain extends CorePegadaian
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');

        $this->load->service('BlockChain_service', 'block_chain_service');
        $this->load->service('Response_service', 'response_service');

        $this->form_validation->set_message('required', '{field} harus diisi.');
    }

    function gold_post()
    {
        if (empty($this->authCore())) {
            return $this->errorUnAuthorized();
        }

        $reff_switching = $this->post('reff_switching');

        /* Start Form Validation */
        $this->form_validation->set_data(['reff_switching' => $reff_switching]);
        $this->form_validation->set_rules('reff_switching', 'Reff Switching ID', 'required');

        if ($this->form_validation->run() == false) {
            $validation = $this->form_validation->error_array();
            $response   = $this->response_service->fValidationErrResponse($validation);

            return $this->send_response($response);
        }
        /* End Form Validation */

        return $this->send_response($this->block_chain_service->storeGold($reff_switching));
    }
}
