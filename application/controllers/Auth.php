<?php

defined('BASEPATH') or exit('No direct script access allowed');

require_once 'CorePegadaian.php';

/**
 * @property ResetPasswordModel reset_password_model
 * @property User User
 * @property Auth_service auth_service
 * @property Response_service response_service
 * @property AuditLogModel audit_log_model
 */
class Auth extends CorePegadaian
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('User');
        $this->load->model('ResetPasswordModel', 'reset_password_model');
        $this->load->library('form_validation');
        $this->load->helper('message');

        $this->load->library('form_validation');
        $this->form_validation->set_message('required', '{field} harus diisi.');

        $this->load->model('AuditLogModel', 'audit_log_model');
        $this->load->model('ConfigModel');
        $this->load->model('EmasModel');
        $this->load->model('MasterModel');
        $this->load->model('NotificationModel');
        $this->load->model('ResetPasswordModel');
        $this->load->model('ResetPasswordModel', 'reset_password_model');

        $this->load->service('Auth_service', 'auth_service');
        $this->load->service('Response_service', 'response_service');
    }

    public function login_post()
    {
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('agen', 'agen', 'required');

        if (empty($this->form_validation->run())) {
            return $this->send_response($this->response_service->fValidationErrResponse($this->form_validation->error_array()));
        }

        $service = $this->auth_service->login($this->post());

        if ($service['status'] !== 'success') {
            return $this->send_response($service);
        }

        $response = [
            'status' => $service['status'],
            'user' => $service['data']['user'],
            'token' => $service['data']['token']
        ];

        return $this->set_response($response);
    }

    public function register_post()
    {
        $this->form_validation->set_rules('nama', 'nama', 'required');
        $this->form_validation->set_rules('no_telepon', 'no_telepon', 'required|numeric');
        $this->form_validation->set_rules('agen', 'agen', 'required');

        if (!$this->form_validation->run()) {
            $response = array(
                'code' => 101,
                'status' => 'error',
                'message' => 'Invalid input',
                'errors' => $this->form_validation->error_array()
            );
            $this->set_response($response, 200);
        } else {
            $no_telepon = $this->input->post('no_telepon');
            $nama = $this->input->post('nama');
            $agen = $this->input->post('agen');
            $version = $this->input->post('version');

            //Check if user phone exist
            if ($this->User->isPhoneExist($no_telepon) && $this->User->isActive($no_telepon)) {
                $this->response(array(
                    'code' => 102,
                    'status' => 'error',
                    'message' => 'No telepon sudah digunakan',
                ), 200);
            } else {
                //Generate random 6 number for OTP
                $this->load->helper('message');

                $r = $this->User->register($nama, $no_telepon);

                $sendOtp = $this->sendOTP($no_telepon, $r['reffId']);
                
                if ($sendOtp->responseCode == '00') {
                    $access_token = bin2hex(random_bytes(78));
                    
                    $channelId = $agen == "web" ? "6014" : "6017";
                    
                    $tokenData = array(
                        'id' => $r['userId'],
                        'email' => '',
                        'nama' => $nama,
                        'no_hp' => $no_telepon,
                        'access_token' => $access_token,
                        'agen' => $agen,
                        'channelId' => $channelId,
                        'version' => $version
                    );
                    
                    $updateData = array();
                    
                    if ($agen=='android') {
                        $updateData['token'] = $access_token;
                    } elseif ($agen=='web') {
                        $updateData['token_web'] = $access_token;
                    }
                    
                    $this->User->updateUser($r['userId'], $updateData);
                    

                    $token = Authorization::generateToken($tokenData);

                    $response = array(
                        'status' => 'success',
                        'token' => $token,
                        'reffId' => $r['reffId'],
                        'message' => 'Kami mengirimkan kode OTP ke no telepon anda',
                        'data' => $sendOtp->data
                    );
                    $this->set_response($response, 200);
                } else {
                    $this->set_response(array(
                        'code' => 102,
                        'status'=>'error',
                        'message'=> 'otp tidak berhasil dikirim',
                        'reason' => $sendOtp
                    ));
                }
            }
        }
    }

    public function verify_otp_post()
    {
        $this->form_validation->set_rules('otp', 'otp', 'required|numeric|exact_length[6]');
        $this->form_validation->set_rules('reff_id', 'reff id', 'required');

        if (!$this->form_validation->run()) {
            $this->response(array(
                'status' => 'error',
                'message' => 'Invalid input',
                'code' => 101,
                'errors' => $this->form_validation->error_array()
            ), 200);
        } else {
            $otp = $this->input->post('otp');
            $reffId = $this->input->post('reff_id');
            
            //Get no hp user berdasarkan reff id
            $user = $this->db->where('otp_AIID', $reffId)
                            ->join('user', 'otp.user_AIID=user.user_AIID')
                            ->get('otp')
                            ->row();
            
            $checkOtp = $this->checkOTP($otp, $user->no_hp);
            
            if ($checkOtp->responseCode == '00') {
                $this->response(array(
                    'status'=>'success',
                    'message'=>'',
                ), 200);
            } else {
                $response = array(
                    'status' => 'error',
                    'message' => 'OTP tidak ditemukan',
                    'code' => 102
                );
                $this->set_response($response, 200);
            }
        }
    }

    public function register_new_post($method = '')
    {
        $this->form_validation->set_message('required', '{field} harus diisi.');

        switch ($method) {
            case 'check':
                $this->form_validation->set_rules('nama', 'Nama', 'required');
                $this->form_validation->set_rules('email', 'Email', 'callback__validation_email_check');
                $this->form_validation->set_rules('no_hp', 'No Handphone', 'callback__validation_no_hp_check');
                $func = 'registerCheck';
                break;
            case 'otp':
                $this->form_validation->set_rules('no_hp', 'No Handphone', 'callback__validation_no_hp_check');
                $this->form_validation->set_rules('otp', 'Kode OTP', 'required');
                $func = 'registerOtp';
                break;
            case 'resend_otp':
                $this->form_validation->set_rules('no_hp', 'No Handphone', 'callback__validation_no_hp_check');
                $func = 'resendOtp';
                break;
            default:
                $this->form_validation->set_rules('register_id', 'Register ID', 'required');
                $this->form_validation->set_rules('fcm_token', 'FCM Token', 'required');
                $this->form_validation->set_rules('nama', 'Nama', 'required');
                $this->form_validation->set_rules('email', 'Email', 'callback__validation_email_check');
                $this->form_validation->set_rules('no_hp', 'No Handphone', 'callback__validation_no_hp_check');
                $this->form_validation->set_rules('password', 'Password', 'callback__validation_password');
                $func = 'register';
                break;
        }

        if ($this->form_validation->run() == false) {
            $validation = Pegadaian::showErrorValidation($this->form_validation->error_array());
            return $this->send_response('error', 'Validation Exception', $validation, '101');
        }

        $response = $this->auth_service->{$func}($this->post());

        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    public function login_new_post()
    {
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('agen', 'Agen', 'callback__validation_agen');

        if ($this->form_validation->run() == false) {
            return $this->send_response('error', 'Invalid Input', '', '101', 200, $this->form_validation->error_array());
        }

        $response = $this->auth_service->login($this->post());

        if ($response['status'] !== 'success') {
            return $this->send_response($response['status'], $response['message'], $response['data']);
        }

        return $this->set_response([
            'status' => $response['status'],
            'user'   => $response['data']['user'],
            'token'  => $response['data']['token']
        ]);
    }

    public function request_password_reset_post()
    {
        $this->form_validation->set_rules('email', 'email', 'required');

        if ($this->form_validation->run() == false) {
            $validation = Pegadaian::showErrorValidation($this->form_validation->error_array());
            return $this->send_response('error', 'Validation Exception', $validation, '101');
        }

        $service = $this->auth_service->requestResetPassword($this->post());

        return $this->send_response($service['status'], $service['message'], $service['data']);
    }

    public function reset_password_get()
    {
        $token = $this->query('t');

        if ($this->reset_password_model->isValid($token)) {
            return $this->load->view('password/reset_password', ['token' => $token]);
        }

        return $this->send_response('error', 'Token sudah tidak valid', null);
    }

    public function reset_password_success_get()
    {
        $token = $this->query('t');

        if (!$this->reset_password_model->isValid($token)) {
            redirect('/');
        }

        $this->reset_password_model->off($token);

        $this->load->view('password/reset_password_success');
    }

    public function reset_password_post()
    {
        $this->form_validation->set_message('required', '{field} harus diisi.');
        $this->form_validation->set_message('min_length', '{field} harus terdapat minimal 8 karakter');
        $this->form_validation->set_rules('token', 'token', 'required');
        $this->form_validation->set_rules('password', 'Password', 'callback__validation_password|min_length[8]');

        if (!$this->reset_password_model->isValid($this->post('token'))) {
            return $this->send_response('error', 'Token tidak valid!', null);
        }

        if ($this->form_validation->run() == false) {
            return $this->load->view('password/reset_password', ['token' => $this->post('token')]);
        }

        $id       = $this->reset_password_model->getUserId($this->post('token'));
        $user     = $this->User->getUser($id);
        $password = md5($this->post('password'));

        $this->User->updateUser($id, ['password' => $password]);
        $this->audit_log_model->add([
            'user_id' => $user->user_AIID,
            'cif' => $user->cif,
            'nama_nasabah' => $user->nama,
            'no_hp' => $user->no_hp,
            'default_property' => $user->password,
            'change_property' => $password,
            'action' => $this->audit_log_model::ACTION_CHANGE,
            'modul' => $this->audit_log_model::MODUL_RESET_PASSWORD,
            'platform' => $this->audit_log_model::PLATFORM_PDS,
        ]);

        redirect('/auth/reset_password_success?t=' . $this->post('token'));
    }

    public function reset_password_otp_post($method = 'update')
    {
        $request = $this->post();

        switch ($method) {
            case 'check':
                $this->form_validation->set_rules('email', 'Email / No HP', 'required');
                $this->form_validation->set_rules('otp', 'Kode OTP', 'required');
                $func = 'checkOtpResetPassword';
                break;
            case 'resend_otp':
                $this->form_validation->set_rules('email', 'No Handphone', 'required');
                $request['no_hp'] = $this->post('email');
                $request['request_type'] = 'resetpassword';
                $func = 'resendOtp';
                break;
            default:
                $this->form_validation->set_rules('email', 'Email / No HP', 'required');
                $this->form_validation->set_message('min_length', '{field} harus terdapat minimal 8 karakter');
                $this->form_validation->set_rules('password', 'Password', 'callback__validation_password|min_length[8]');
                $this->form_validation->set_rules('otp', 'Kode', 'required|numeric');
                $func = 'resetPasswordByOtp';
        }

        if ($this->form_validation->run() == false) {
            $validation = Pegadaian::showErrorValidation($this->form_validation->error_array());
            return $this->send_response('error', 'Validation Exception', $validation, '101');
        }

        if (empty($func)) {
            return $this->send_response('success', 'Password valid', 'null');
        }

        $service = $this->auth_service->{$func}($request);

        return $this->send_response($service['status'], $service['message'], $service['data']);
    }

    function register_fcm_token_put()
    {
        
        $token = $this->getToken();
        if ($token) {
            $user = $this->User->getUser($token->id);
            
             $set_data = array(
                'fcm_token' => $this->put('fcm_token')
             );
             
             $this->form_validation->set_data($set_data);
             $this->form_validation->set_rules('fcm_token', 'fcm_token', 'required');
             if (!$this->form_validation->run()) {
                 $this->set_response(array(
                    'status'=>'error',
                    'message'=>'Invalid input',
                    'errors'=>$this->form_validation->error_array()
                 ), 200);
             } else {
                 $prevFCMToken = $user->fcm_token;
                
                 $fcm_token = $this->put('fcm_token');
                
                 //Kirimkan notifikasi FCM dengan tipe logout ke device sebelumnya
                 if ($prevFCMToken != $fcm_token) {
                     $this->load->helper('message');
                     Message::sendFCMNotif($prevFCMToken, array(
                        'id' => '00000',
                        'tipe' => 'LOGOUT'
                     ));
                 }
                
                 $update = $this->User->updateUser($token->id, array('fcm_token'=>$fcm_token));
                 $this->set_response(array(
                    'status'=>'success',
                    'message'=>'',
                 ), 200);
             }
        } else {
            $this->errorUnAuthorized();
        }
    }
    
    function activate_post()
    {
        $headers = $this->input->request_headers();
        $token = $this->getToken();

        if ($token) {
            $this->form_validation->set_rules('email', 'email', 'valid_email');
            $this->form_validation->set_rules('password', 'password', 'required|min_length[6]|max_length[12]');

            if ($token->agen == 'android') {
                $this->form_validation->set_rules('fcm_token', 'FCM Token', 'required');
            }

            if (!$this->form_validation->run()) {
                $error = "";
                foreach ($this->form_validation->error_array() as $key => $value) {
                    $error = $value;
                    break;
                }
                $response = array(
                    'code' => 101,
                    'status' => 'error',
                    'message' => $error,
                );
                $this->set_response($response, 200);
            } else {
                $email = $this->input->post('email');
                $password = $this->input->post('password');
                $fcm_token = $this->input->post('fcm_token');
                $headers = $this->input->request_headers();
                
                if ($this->User->isEmailExist2($email)) {
                    $this->set_response(array(
                        'status' => 'error',
                        'code' => 102,
                        'message' => 'Email sudah digunakan'
                    ), 200);
                    return;
                }
                
                //Generate email verification token
                $emailVerifyToken = bin2hex(random_bytes(78));
                               
                //Update user
                $update = array(
                    'email' => $email,
                    'password' => md5($password),
                    'email_verification_token' => $emailVerifyToken,
                    'status'=> 1,
                    'fcm_token' => $fcm_token
                );
                
                if ($token->agen=='android') {
                    $update['token'] = $token->access_token;
                } elseif ($token->agen=='web') {
                    $update['token'] = $token->access_token;
                }

                $this->User->updateUser($token->id, $update);
                
                //Buat notifikasi baru dengan tipe profile
                $templateData = array(
                    'nama' => $token->nama,
                    'email' => $email
                );
                
                $template = $this->load->view('notification/email_notification', $templateData, true);
                //Simpan notifikasi baru
                
                $notifType = NotificationModel::TYPE_ACCOUNT;
                $notifTitle = "Selamat Datang di Pegadaian Digital Service";
                $notifTagline = "Hai ".$token->nama.', lakukan verifikasi email untuk melanjutkan registrasi';

                $newNotif = $this->NotificationModel->add($token->id, $notifType, 'html', $notifTitle, $notifTagline, $template);
                $this->load->helper('message');
                //Kirim notifikasi FCM
                Message::sendNotifWelcome($fcm_token, $newNotif, $token->nama);
                
                $this->load->helper('message');
                Message::sendEmailVerificationEmail($email, $token->nama, $emailVerifyToken);
                
                log_message("info", "Email Verification register 3 ", $emailVerifyToken);
                $response = array(
                    'status' => 'success',
                    'message' => 'Thank You',
                    'data' => $this->User->profile($token->id)
                );
                $this->set_response($response, 200);
            }
        } else {
            $this->errorUnAuthorized();
        }
    }

    function request_password_reset_web_post()
    {
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('motherName', 'motherName', 'required');

        //Check form validation
        if (!$this->form_validation->run()) {
            $this->response([
                'code'=> 101,
                'status' => 'error',
                'message' => 'Invalid Input',
                'errors' => $this->form_validation->error_array()
            ], 200);
        } else {
            $email = $this->input->post('email');
            $motherName = $this->input->post('motherName');
            $telfonCare = $this->ConfigModel->getTelfonCustomerCare();

            if ($this->User->isResetPasswordAvailable($email)) {
                return $this->set_response([
                    'status' => 'error',
                    'message' => 'Kami mendeteksi Anda terlalu sering melakukan permintaan reset password. Mohon tunggu 30 menit lagi.',
                    'code' => 101
                ]);
            }

            $date = $nextReset = new DateTime('now');
            $date->add(new DateInterval('P1D'));
            $nextReset->add(new DateInterval('PT30M'));
                             
            //Cek no telepon atau email ada
            if ($this->User->isEmailExist2($email)) {
                $user = $this->User->profile($email);

                if ($user->cif) {
                    if (!$this->User->checkMothersName($user->user_AIID, $motherName)) {
                        return $this->set_response(array(
                            'status'=>'error',
                            'message'=>'Email atau handphone atau ibu kandung tidak ditemukan di sistem kami. Apakah anda sudah memasukkan informasi yang benar? Jika masalah terus berlanjut, silakan hubungi hotline kami '.$telfonCare,
                            'code'=>101
                        ));
                    }
                }

                $token = bin2hex(random_bytes(78));
                
                $newRequestData = array(
                    'user_AIID' => $user->id,
                    'token' => $token,
                    'valid_until' => $date->format('Y-m-d H:i:s'),
                );

                //simpan request reset baru
                $this->ResetPasswordModel->add($newRequestData);
                
                $this->User->updateUserByEmail($email, [
                    'next_password_reset' => $nextReset->format('Y-m-d H:i:s'),
                ]);

                //Kirim email berserta dengan link reset password
                Message::sendResetPasswordEmail($user->email, $user->nama, $token);

                $response = array(
                    'status' => 'success',
                    'message' => 'Petunjuk reset password telah dikirimkan ke email anda',
                    'method' => 'email'
                );
                $this->set_response($response, 200);
            } elseif ($user = $this->User->isPhoneExist($email)) {
                if ($user->cif) {
                    if (!$this->User->checkMothersName($user->user_AIID, $motherName)) {
                        return $this->set_response(array(
                            'status'=>'error',
                            'message'=>'Email atau handphone atau ibu kandung tidak ditemukan di sistem kami. Apakah anda sudah memasukkan informasi yang benar? Jika masalah terus berlanjut, silakan hubungi hotline kami '.$telfonCare,
                            'code'=>101
                        ));
                    }
                }

                $newRequestData = array(
                    'user_AIID' => $user->user_AIID,
                    'no_hp' => $email, //nama inputnya email
                    'valid_until' => $date->format('Y-m-d H:i:s'),
                    'next_password_reset' => $nextReset->format('Y-m-d H:i:s')
                );

                //simpan request reset baru
                $idReset = $this->ResetPasswordModel->add($newRequestData);

                $this->User->updateUserByEmail($email, [
                    'next_password_reset' => $nextReset->format('Y-m-d H:i:s'),
                ]);
                
                //Request OTP untuk reset password
                $this->sendOTP($email, $idReset, "resetpassword");
                
                $this->set_response(array(
                    'status' => 'success',
                    'message' => 'Kode reset password telah dikirimkan ke nomor '.$email.'. Masukan kode untuk melakukan reset password pada aplikasi Pegadaian Digital Service',
                    'method' => 'phone'
                ), 200);
            } else {
                $this->response([
                    'code' => 102,
                    'status' => 'error',
                    'message' => 'Profile tidak ditemukan atau email belum terverifikasi'
                ], 200);
            }
        }
    }
    
    function verify_email_get()
    {
        
        $token = $this->query("t");
        
        if ($this->User->isValidEmailVerificationToken($token)) {
            $user = $this->User->verifyEmail($token);
            
            //send fcm message to user
            Message::sendFCMNotif($user->fcm_token, array(
                'tipe' => 'EMAIL_VERIFIED',
                'emailStatus' => '1'
            ));
            
            $this->load->view('mail/email_verification_success');
        } else {
            $this->load->view('mail/already_verified');
        }
    }
    
    function check_token_get()
    {
        $token = $this->getToken();
        if ($token) {
            $this->set_response(array(
                'status' => 'success',
                'message' => 'Valid Token',
                'data' => array(
                    'accessToken' => $token->access_token,
                    'agen' => $token->agen,
                    'valid' => true
                )
            ), 200);
        } else {
            $this->set_response(array(
                'status' => 'success',
                'message' => 'Invalid Token',
                'data' => array(
                    'accessToken' => null,
                    'agen' => null,
                    'valid' => false
                )
            ), 200);
        }
    }

    function core_token_post()
    {
        if (!$this->validateToken()) {
            return;
        }
        
        $this->set_response(array(
            'status' => 'success',
            'message' => '',
            'data' => array(
                'accessToken' => $this->getCoreToken(),
            )
        ), 200);
    }

    function send_otp_get()
    {
        // get token
        $token = $this->getToken();
        // token validation
        if (!$token) {
            $this->errorUnAuthorized();
            return;
        }

        $reffId       = mt_rand(1000, 9999);
        $request_type = $this->query('request_type');
        $request_type = !empty($request_type) ? $request_type : 'aktivasi';
        $sendOTP      = $this->sendOTP($token->no_hp, $reffId, $request_type);

        log_message('debug', 'send otp core response', $sendOTP);

        $result = $sendOTP;
        if ($sendOTP->responseCode != '00') {
            $this->send_response('error', 'Terjadi kesalahan mohon coba lagi', '');
            return;
        }

        $this->send_response('success', '', $result);
    }

    function check_otp_post()
    {
        // get token and validate it
        $token = $this->getToken();
        if (!$token) {
            $this->errorUnAuthorized();
            return;
        }

        // set rules for the request body
        $this->form_validation->set_data($this->post());
        $this->form_validation->set_rules('otp', 'otp', 'numeric|required|exact_length[6]');

        // validate request body
        if ($this->form_validation->run() == false) {
            $response = $this->send_response('error', 'Invalid Input', [], '', 101, $this->form_validation->error_array());
            return;
        }

        // send check otp to core
        $request_type = $this->post('request_type');
        $request_type = !empty($request_type) ? $request_type : 'aktivasi';
        $checkOtp = $this->checkOTP($this->post('otp'), $token->no_hp, $request_type);

        // validate check otp response
        if ($checkOtp->responseCode != '00') {
            $response = $this->send_response('error', 'OTP tidak ditemukan/expired', $checkOtp);
            return;
        }

        // send response
        $otpData = json_decode($checkOtp->data);
        $response = $this->send_response('success', 'OTP valid', $otpData);
    }

    function _validation_email_check($email)
    {
        if (empty($email)) {
            $this->form_validation->set_message('_validation_email_check', '{field} harus diisi.');
            return false;
        }

        if (!empty($this->User->find(['email' => $email]))) {
            $this->form_validation->set_message('_validation_email_check', '{field} sudah terdaftar.');
            return false;
        }

        if (empty(Message::emailCheck($email))) {
            $this->form_validation->set_message('_validation_email_check', '{field} tidak valid/ tidak aktif');
            return false;
        }

        return true;
    }

    function _validation_no_hp_check($no_hp)
    {
        if (empty($no_hp)) {
            $this->form_validation->set_message('_validation_no_hp_check', '{field} harus diisi.');
            return false;
        }

        $this->form_validation->set_message('_validation_no_hp_check', '{field} sudah terdaftar.');
        $user = $this->User->find(['no_hp' => $no_hp]);

        return empty($user);
    }

    function _validation_password($password)
    {
        $validation_password = Pegadaian::validationPassword($password);

        $this->form_validation->set_message('_validation_password', $validation_password['message']);

        return $validation_password['is_valid'];
    }

    function _validation_agen($agen)
    {
        $this->form_validation->set_message('_validation_agen', '{field} yang dikirim tidak valid');

        return in_array($agen, ['mobile', 'web']);
    }

    function cekPasswordSaatIni_post()
    {
        $this->form_validation->set_message('required', '{field} harus diisi.');

        $token = $this->getToken();
        if (!$token) {
            $this->errorUnAuthorized();
            return;
        }

        $this->form_validation->set_rules('password', 'Password Lama', 'required|matches[password]');

        if (!$this->form_validation->run()) {
            $error = $this->send_response('error', 'Invalid Input', $this->form_validation->error_array(), 101);
            return $error;
        }

        $response = $this->auth_service->cekPasswordSaatIni($this->post(), $token);

        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    public function ubahPasswordBaru_post()
    {
        $this->form_validation->set_message('required', '{field} harus diisi.');
        $this->form_validation->set_message('min_length', '{field} harus terdapat minimal {param} karakter');

        $token = $this->getToken();
        if (!$token) {
            $this->errorUnAuthorized();
            return;
        }

        if (empty($this->post('tipe'))) {
            $this->form_validation->set_rules('passwordLama', 'Password Lama', 'required|min_length[8]|matches[passwordLama]');
        }
        $this->form_validation->set_rules('passwordBaru', 'Password Baru', 'callback__validation_password');

        if (!$this->form_validation->run()) {
            $validation = Pegadaian::showErrorValidation($this->form_validation->error_array());
            return $this->send_response('error', 'Validation Exception', $validation, '101');
        }

        $response = $this->auth_service->sendPasswordBaru($this->post(), $token);

        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

}
