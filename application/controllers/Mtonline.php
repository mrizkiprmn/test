<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once 'CorePegadaian.php';

/**
 * @property Mtonline_service mtonline_service
 */
class Mtonline extends CorePegadaian
{
    private $auth;

    public function __construct()
    {
        parent::__construct();
        // library
        $this->load->library('form_validation');
        // service
        $this->load->service('Mtonline_service', 'mtonline_service');
        // model
        $this->load->model('LoggerModel', 'logger_model');

        $this->auth = $this->getToken();
    }

    public function sisaLimit_get()
    {
        log_message('debug', 'START OF CONTROLLER ' . __FUNCTION__);

        if (empty($this->auth)) {
            $this->errorUnAuthorized();
            log_message('debug', __FUNCTION__  . json_encode($this->errorUnAuthorized()));

            return;
        }

        $response = $this->mtonline_service->limitPinjaman($this->auth->id);
        log_message('debug', 'success' . __FUNCTION__ . json_encode($response));

        log_message('Debug', 'END OF CONTROLLER ' . __FUNCTION__);
        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    public function portofolio_get()
    {
        log_message('debug', 'START OF CONTROLLER ' . __FUNCTION__);

        if (empty($this->auth)) {
            $this->errorUnAuthorized();
            log_message('debug', __FUNCTION__  . json_encode($this->errorUnAuthorized()));

            return;
        }

        $response = $this->mtonline_service->portofolioPinjaman($this->auth->id);
        log_message('debug', 'success' . __FUNCTION__ . json_encode($response));

        log_message('Debug', 'END OF CONTROLLER ' . __FUNCTION__);
        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    public function portofolio_detail_post()
    {
        log_message('debug', 'START OF CONTROLLER ' . __FUNCTION__);

        if (empty($this->auth)) {
            $this->errorUnAuthorized();
            log_message('debug', __FUNCTION__  . json_encode($this->errorUnAuthorized()));

            return;
        }

        $this->form_validation->set_rules('nomor_rekening', 'Nomor Kontrak', 'required|numeric');
        
        if (!$this->form_validation->run()) {
            $validation = Pegadaian::showErrorValidation($this->form_validation->error_array());
            log_message('debug', 'END ' . __FUNCTION__ . ' => ' . json_encode($validation));
            return $this->send_response('error', 'Validation Exception', $validation, '101');
        }

        $response = $this->mtonline_service->portofolioPinjamanDetail($this->auth->id, $this->post());
        
        log_message('debug', 'success' . __FUNCTION__ . json_encode($response));

        log_message('Debug', 'END OF CONTROLLER ' . __FUNCTION__);
        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    public function cekPengajuan_post()
    {
        log_message('debug', 'START OF CONTROLLER ' . __FUNCTION__);

        if (empty($this->auth)) {
            $this->errorUnAuthorized();
            log_message('debug', __FUNCTION__  . json_encode($this->errorUnAuthorized()));

            return;
        }

        $this->form_validation->set_rules('nomor_rekening', 'Nomor Rekening', 'required|numeric');
        $this->form_validation->set_rules('kewajiban', 'Kewajiban', 'required|numeric');
        $this->form_validation->set_rules('sisa_limit', 'Sisa Limit', 'required|numeric');

        if (!$this->form_validation->run()) {
            $validation = Pegadaian::showErrorValidation($this->form_validation->error_array());
            log_message('debug', 'END ' . __FUNCTION__ . ' => ' . json_encode($validation));

            return $this->send_response('error', 'Validation Exception', $validation, '101');
        }

        $response = $this->mtonline_service->cekPengajuan($this->auth->id, $this->post());
        log_message('debug', 'success' . __FUNCTION__ . json_encode($response));

        log_message('Debug', 'END OF CONTROLLER ' . __FUNCTION__);
        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    public function otp_send_post()
    {
        if (!$this->validateToken()) {
            $this->set_response($this->response_invalid_token(), 200);
            return;
        }

        $request_body = $this->post();
        $request_body['clientId'] = $this->config->item('core_post_username');
        $request_body['channelId'] = $this->config->item('core_client_id');
        $request_body['requestType'] = 'mtonline';

        //save request
        $this->logger_model->save(__FUNCTION__, 'REQUEST', $request_body);

        $reffId = mt_rand(1000, 9999);
        $sendOTP = $this->sendOTP($this->post('noHp'), $reffId, $request_body['requestType'], $request_body['channelId']);
        $result = json_encode($sendOTP);

        // get response
        $response = $this->response_set($result, __FUNCTION__);
        $this->set_response($response, 200);
    }

    public function otp_check_post()
    {
        if (!$this->validateToken()) {
            $this->set_response($this->response_invalid_token(), 200);
            return;
        }

        $request_body = $this->post();
        $request_body['clientId'] = $this->config->item('core_post_username');
        $request_body['channelId'] = $this->config->item('core_client_id');
        $request_body['requestType'] = 'mtonline';

      //save request
        $this->logger_model->save(__FUNCTION__, 'REQUEST', $request_body);

        $checkOTP = $this->checkOTP($this->post('token'), $this->post('noHp'), $request_body['requestType'], $request_body['channelId']);
        $result = json_encode($checkOTP);
      // get response
        $response = $this->response_set($result, __FUNCTION__);
        $this->set_response($response, 200);
    }

    private function response_invalid_token()
    {
        return array(
        'status' => 'error',
        'code' => '99',
        'message' => 'Invalid Token',
        'data' => ''
        );
    }

    private function response_set($response = [], $srcFunc = '')
    {
        if (gettype($response) == 'string') {
            $temp = $response;
            $response = [];
            $response['body'] = $temp;
        }

        if (empty($response['body'])) {
            $response['body'] = $response;
        }

        $res = json_decode($response['body'], true);

        if (!empty($res['data']) && gettype($res['data']) == 'string') {
            $res['data'] = json_decode($res['data'], true);
        }

        log_message('debug', 'response /mtonline/ method ' . $srcFunc . ' data: ' . json_encode($res));
      //save response
        $this->logger_model->save($srcFunc, 'RESPONSE', $res);

      // response error
        if ($res['responseCode'] != '00') {
            return array(
            'status' => 'error',
            'code' => $res['responseCode'],
            'message' => $res['responseDesc'],
            'data' => ''
            );
        }

        return array(
        'status' => 'success',
        'code' => '00',
        'message' => '',
        'data' => $res
        );
    }
}
