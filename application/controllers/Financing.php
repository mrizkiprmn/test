<?php

defined('BASEPATH') or exit('No direct script access allowed');

require_once 'CorePegadaian.php';

class Financing extends CorePegadaian
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model('User', 'user_model');
        $this->load->model('CollateralModel', 'collateral_model');
        $this->load->service('Financing_service', 'financing_service');
        $this->form_validation->set_message('required', '{field} harus diisi.');
    }

    function index_get()
    {
        $token = $this->getToken();

        if (empty($token)) {
            return $this->errorUnAuthorized();
        }

        /* Start Form Validation */
        $this->form_validation->set_data(['page' => $this->query('page'), 'count_collateral' => $this->query('count_collateral')]);
        $this->form_validation->set_rules('page', 'page', 'numeric|required');
        $this->form_validation->set_rules('count_collateral', 'Jumlah Agunan', 'numeric');

        if ($this->form_validation->run() == false) {
            $errors = $this->form_validation->error_array();
            foreach ($errors as $key => $error) {
                $err_validations[] = [
                    'field' => $key,
                    'message' => $error
                ];
            }

            return $this->send_response('error', 'Validation Exception', $err_validations ?? [], '101');
        }
        /* End Form Validation */

        $user = $this->user_model->profile($token->id);
        $response = $this->financing_service->getDataForm($this->query(), $user);

        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    function store_post()
    {
        $token = $this->getToken();

        if (empty($token)) {
            return $this->errorUnAuthorized();
        }

        $page = $this->post('page') ?? 0;

        /* Start Form Validation Form One */
        switch ($page) {
            case 1:
                $validation = [
                    'is_have_business' => $this->post('is_have_business'),
                    'loan_amount' => $this->post('loan_amount'),
                    'collaterals' => $this->post('collaterals'),
                ];
                $this->form_validation->set_data($validation);
                $this->form_validation->set_rules('is_have_business', 'Status Kepemilikan Usaha', 'callback__is_have_business');
                $this->form_validation->set_rules('loan_amount', 'Pengajuan Pinjaman', 'required|callback__validation_loan_amount');
                $this->form_validation->set_rules('collaterals', 'Jenis Agunan', 'callback__validation_form_one');
                break;
            case 2:
                $this->form_validation->set_data([
                    'tenor' => $this->post('tenor'),
                    'collaterals' => $this->post('collaterals'),
                ]);
                $this->form_validation->set_rules('tenor', 'Tenor', 'required');
                $this->form_validation->set_rules('collaterals', 'Jenis Agunan', 'callback__validation_form_two');
                break;
            case 3:
                $this->form_validation->set_data(['code_outlet' => $this->post('code_outlet')]);
                $this->form_validation->set_rules('code_outlet', 'Kode Cabang', 'required');
                break;
        }

        if (!empty($page) && $this->form_validation->run() == false) {
            $errors = $this->form_validation->error_array();
            $err_validations = $this->financing_service->setValidationResponse($errors);

            return $this->send_response('error', 'Validation Exception', $err_validations ?? [], '101');
        }
        /* End Form Validation */

        $response = $this->financing_service->postDataForm($this->post(), $this->user_model->profile($token->id));

        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    function notification_post()
    {
        log_message('debug', 'START ' . __FUNCTION__ . ' => ' . json_encode($this->post()));

        $listValidation = array();
        if (!$this->authCore()) {
            return $this->errorUnAuthorized();
        }

        $this->form_validation->set_data([
            'code_booking' => $this->post('code_booking'),
            'statusNotif' => $this->post('statusNotif'),
            'code_status' => $this->post('status')['code_status'],
            'desc_status' => $this->post('status')['desc_status'],
            'pic' => $this->post('status')['pic'],
        ]);

        $this->form_validation->set_rules('code_booking', 'code_booking', 'required');
        $this->form_validation->set_rules('statusNotif', 'statusNotif', 'required');
        if (!empty($this->post('status'))) {
            $this->form_validation->set_rules('code_status', 'code_status', 'required');
            $this->form_validation->set_rules('desc_status', 'desc_status', 'required');
            $this->form_validation->set_rules('pic', 'pic', 'required');
        }

        /*
         * Status Flow Disbursal From LOS
         */
        if ($this->post('statusNotif') == '5') {
            $this->form_validation->set_data([
                'code_booking' => $this->post('code_booking'),
                'status' => $this->post('status'),
                'statusNotif' => $this->post('statusNotif'),
                'code_status' => $this->post('status')['code_status'],
                'desc_status' => $this->post('status')['desc_status'],
                'pic' => $this->post('status')['pic'],
                'status_history' => $this->post('status_history'),
                'code_outlet' => $this->post('code_outlet'),
                'tenor' => $this->post('tenor'),
                'up' => $this->post('up'),
                'up_approval' => $this->post('up_approval'),
                'angsuran' => $this->post('angsuran'),
                'nama_usaha' => $this->post('nama_usaha'),
                'deskripsi_usaha' => $this->post('deskripsi_usaha'),
                'nama_cabang' => $this->post('nama_cabang'),
                'sewa_modal' => $this->post('sewa_modal'),
                'admin' => $this->post('admin'),
                'alamat_cabang' => $this->post('alamat_cabang'),
                'admin' => $this->post('admin'),
                'up_approval' => $this->post('up_approval')
            ]);

            $this->form_validation->set_rules('code_booking', 'code_booking', 'required');
            $this->form_validation->set_rules('up_approval', 'up_approval', 'required');
            $this->form_validation->set_rules('statusNotif', 'statusNotif', 'required');
            $this->form_validation->set_rules('code_status', 'code_status', 'required');

            $this->form_validation->set_rules('code_outlet', 'code_outlet', 'required');
            $this->form_validation->set_rules('tenor', 'tenor', 'required');
            $this->form_validation->set_rules('up', 'up', 'required');
            $this->form_validation->set_rules('angsuran', 'angsuran', 'required');
            $this->form_validation->set_rules('nama_usaha', 'nama_usaha', 'required');
            $this->form_validation->set_rules('deskripsi_usaha', 'deskripsi_usaha', 'required');
            $this->form_validation->set_rules('nama_cabang', 'nama_cabang', 'required');
            $this->form_validation->set_rules('sewa_modal', 'sewa_modal', 'required');
            $this->form_validation->set_rules('admin', 'admin', 'required');
            $this->form_validation->set_rules('alamat_cabang', 'alamat_cabang', 'required');

            if (!empty($this->post('status_history'))) {
                $this->form_validation->set_rules('status_history', 'status_history', 'trim|xss_clean|numeric');
            }
        }

        if ($this->form_validation->run() == false) {
            $errors = $this->form_validation->error_array();
            foreach ($errors as $key => $error) {
                $err_validations[] = [
                    'field' => $key,
                    'message' => $error
                ];
            }

            return $this->send_response('error', 'Validation Exception', $err_validations ?? [], '101');
        }

        $listStatus = [0, 1, 2, 3, 4, 5, 6];
        if (!in_array($this->post('statusNotif'), $listStatus)) {
            return $this->send_response('error', 'Prefix status tidak ditemukan!', null);
        }

        $response = $this->financing_service->notification($this->post());

        log_message('debug', 'END ' . __FUNCTION__ . ' => ' . json_encode($this->post()));


        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    function simulation_post()
    {
        $token = $this->getToken();

        if (empty($token)) {
            return $this->errorUnAuthorized();
        }

        /* Start Form Validation */
        $this->form_validation->set_data(['loan_amount' => $this->post('loan_amount')]);
        $this->form_validation->set_rules('loan_amount', 'Pengajuan Pinjaman', 'required|callback__validation_loan_amount');

        if ($this->form_validation->run() == false) {
            $err = $this->form_validation->error_array();
            $err_validations = $this->financing_service->setValidationResponse($err);

            return $this->send_response('error', 'Validation Exception', $err_validations ?? [], '101');
        }
        /* End Form Validation */

        $response = $this->financing_service->simulation($this->post());

        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    function list_riwayat_get()
    {
        log_message('debug', 'START ' . __FUNCTION__ . ' => ' . json_encode($this->query()));

        $token = $this->getToken();

        if (empty($token)) {
            return $this->errorUnAuthorized();
        }

        $response = $this->financing_service->listRiwayat($this->user_model->profile($token->id));

        log_message('debug', 'END ' . __FUNCTION__ . ' => ' . json_encode($this->query()));
        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    function detail_get()
    {
        log_message('debug', 'START ' . __FUNCTION__ . ' => ' . json_encode($this->query()));

        $token = $this->getToken();

        if (empty($token)) {
            return $this->errorUnAuthorized();
        }

        $response = $this->financing_service->detail($this->user_model->profile($token->id), $this->query('code_booking'));

        log_message('debug', 'END ' . __FUNCTION__ . ' => ' . json_encode($this->query()));

        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    function tracking_get()
    {
        log_message('debug', 'START ' . __FUNCTION__ . ' => ' . json_encode($this->query()));

        $token = $this->getToken();

        if (empty($token)) {
            return $this->errorUnAuthorized();
        }

        $response = $this->financing_service->tracking($this->query('code_booking'));

        log_message('debug', 'END ' . __FUNCTION__ . ' => ' . json_encode($this->query()));

        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    function _validation_loan_amount($loan_amount)
    {
        $loan_amount = (int) $loan_amount;
        $data = json_decode(file_get_contents("php://input"));
        $is_have_business = $data->is_have_business ?? null;
        $collaterals = $data->collaterals ?? [];

        $min = 1000000;
        $max = 2000000;

        switch (true) {
            case !empty($is_have_business) && $collaterals[0]->collateral_type != 'true':
                $max = 400000000;
                break;
            case empty($is_have_business) && $collaterals[0]->collateral_type != 'true':
                $max = 100000000;
                break;
        }

        $min_value = Pegadaian::currencyIdr($min);
        $max_value = Pegadaian::currencyIdr($max);

        if ($loan_amount < $min) {
            $this->form_validation->set_message('_validation_loan_amount', "{field} minimal {$min_value}");
            return false;
        }

        if ($loan_amount > $max) {
            $this->form_validation->set_message('_validation_loan_amount', "{field} maksimal {$max_value}");
            return false;
        }

        return true;
    }

    function _validation_form_one()
    {
        $validation = true;
        $data = json_decode(file_get_contents("php://input"), true);
        $collaterals = $data['collaterals'] ?? null;

        $this->form_validation->set_message('_validation_form_one', 'isian {field} tidak sesuai.');

        // validation is array
        if (!is_array($collaterals)) {
            $this->form_validation->set_message('_validation_form_one', '{field} harus berisi sebuah array');
            return false;
        }

        // validation empty array
        if (empty($collaterals)) {
            $this->form_validation->set_message('_validation_form_one', 'Pilih jenis agunan.');
            return false;
        }

        if (empty($data['is_have_business']) && (!empty($collaterals[0]['collateral_type']) && $collaterals[0]['collateral_type'] === 'true')) {
            $this->form_validation->set_rules('collateral_type.0', 'Jenis Agunan', 'required');
            $this->form_validation->set_message('required', 'Kamu harus memiliki agunan untuk kondisi ini.');
            return false;
        }

        foreach ($collaterals as $key => $collateral) {
            $collateral_type = $collateral['collateral_type'] ?? '';
            $market_price    = $collateral['market_price'] ?? '';
            $type            = $collateral['type'] ?? '';

            if (empty($collateral_type)) {
                $this->form_validation->set_rules('collateral_type.' . $key, 'Jenis Agunan', 'required');
                $this->form_validation->set_message('required', 'Pilih jenis agunan.');
                return false;
            }

            if (!empty($collateral_type) && $collateral['collateral_type'] == 'KN' && empty($type)) {
                $this->form_validation->set_rules('type.' . $key, 'Jenis Kendaraan', 'required');
                $this->form_validation->set_message('required', 'Pilih jenis kendaraan.');
                return false;
            }

            if ($collateral_type !== 'true' && (empty($market_price))) {
                $this->form_validation->set_rules('market_price.' . $key, 'Harga Pasar Agunan', 'required');
                $validation = false;
            }

            $validation_market_price = 1000000;
            if ($collateral_type !== 'true' && (int) $market_price < $validation_market_price) {
                $this->form_validation->set_rules('market_price.' . $key, 'Harga Pasar Agunan', 'required');
                $this->form_validation->set_message('required', 'Jumlah minimal harga pasar agunan '. Pegadaian::currencyIdr($validation_market_price));
                return false;
            }
        }

        return $validation;
    }

    function _validation_form_two()
    {
        $validation = true;
        $data = json_decode(file_get_contents("php://input"), true);
        $collaterals = $data['collaterals'] ?? [];
        $this->form_validation->set_message('_validation_form_two', 'isian {field} tidak sesuai.');

        // validation is array
        if (!is_array($collaterals)) {
            $this->form_validation->set_message('_validation_form_two', '{field} harus berisi sebuah array');
            return false;
        }

        foreach ($collaterals as $key => $collateral) {
            $id = $collateral['id'] ?? '';
            $vehicle_number = $collateral['vehicle_number'] ?? '';
            $production_year = $collateral['production_year'] ?? '';
            $validation_vehicle_number = true;

            if (empty($id)) {
                $this->form_validation->set_rules('id.' . $key, 'Jenis Agunan ID', 'required');
                $validation = false;
            }

            if (!empty($id)) {
                $data_collateral = $this->collateral_model->find(['id' => $id]);
            }

            if ((!empty($data_collateral) && $data_collateral->detail->collateral_type != 'KNMB')) {
                $validation_vehicle_number = false;
            }

            if ($validation_vehicle_number == false && empty($vehicle_number)) {
                $this->form_validation->set_rules('vehicle_number.' . $key, 'Nomor Polisi', 'required');
                return false;
            }

            if (!empty($data_vehicle_number) && in_array($vehicle_number, $data_vehicle_number)) {
                $this->form_validation->set_rules('vehicle_number.' . $key, 'Nomor Polisi', 'required');
                $this->form_validation->set_message('required', '{field} tidak boleh ada yang sama.');
                return false;
            }

            $pattern = '/^([A-Za-z]{1,4})(\s|-)*([0-9]{1,5})(\s|-)*([A-Za-z]{0,3})$/i';
            if (!$validation_vehicle_number && !preg_match($pattern, $vehicle_number)) {
                $this->form_validation->set_rules('vehicle_number.' . $key, 'Nomor Polisi', 'required');
                $this->form_validation->set_message('required', '{field} tidak sesuai.');
                return false;
            }

            if (!empty($collateral['vehicle_number'])) {
                $data_vehicle_number[] = $collateral['vehicle_number'];
            }

            if (empty($production_year)) {
                $this->form_validation->set_rules('production_year.' . $key, 'Tahun Pembuatan', 'required');

                $validation = false;
            }

            $date = new DateTime();
            if (!empty($production_year) && ((int) $production_year > (int) $date->format('Y'))) {
                $this->form_validation->set_rules('production_year.' . $key, 'Tahun Pembuatan', 'required');
                $this->form_validation->set_message('required', '{field} tidak sesuai.');

                return false;
            }
        }

        return $validation;
    }

    function _is_have_business($str)
    {
        $data = json_decode(file_get_contents("php://input"), true);

        if (empty($data['is_have_business']) && $data['is_have_business'] != '0') {
            $this->form_validation->set_message('_is_have_business', 'Pilih status kepemilikan usaha');
            return false;
        }

        return true;
    }
}
