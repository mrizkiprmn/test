<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once 'CorePegadaian.php';

class Goldcard extends CorePegadaian
{
    public function __construct()
    {
        parent::__construct();

        $models = [
            'NotificationModel',
            'MainModel'
        ];

        $this->load->model($models);
        $this->load->helper(['message', 'pegadaian']);
        $this->load->library('form_validation');
        $this->load->service('goldcard_service');
        $this->load->service('notification_service');
        $this->load->service('response_service');

        // Log request and post
        $CI = &get_instance();
        $uri = $CI->uri->uri_string();
        $postParam = $CI->input->post();
        $getParam = $CI->input->get();

        log_message('debug', 'REQUEST INFO');
        log_message('debug', 'REQUEST INFO URI: ' . $uri);

        if ($postParam) {
            log_message('debug', 'REQUEST INFO POST PARAM:' . json_encode($postParam));
        }

        if ($postParam) {
            log_message('debug', 'REQUEST INFO GET PARAM:' . json_encode($getParam));
        }
    }

    function inquiry_post()
    {
        $token = $this->getToken();

        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', __FUNCTION__ . ' = Unauthorized');
            return;
        }

        $response = $this->goldcard_service->gcInquiry($token);
        log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
        $this->send_response($response);
        return;
    }

    function ajukan_post()
    {
        $token = $this->getToken();

        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', __FUNCTION__ . ' = Unauthorized');
            return;
        }

        $user = $this->User->profile($token->id);

        // get requirement from Goldcard API
        $requirements = $this->goldcard_service->getRequirements();

        // logic umur >21 atau umur>17 sudah menikah
        if (!$this->goldcard_service->requirementUmur($user, $requirements['umur'], $requirements['umur_menikah'])) {
            $errorMessage = $this->goldcard_service->messageGoldcard('ajukan.umur');
            $response = $this->send_response('error', $errorMessage['message_content'], $errorMessage);
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        // Logic cek KYC
        if (!$this->goldcard_service->isKYC($user, $requirements['kyc'])) {
            $errorMessage = $this->goldcard_service->messageGoldcard('ajukan.kyc');
            $response = $this->send_response('error', $errorMessage['message_content'], $errorMessage);
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        // Logic cek aktivasi finansial
        if (!$this->goldcard_service->isAktifasiFinansial($user, $requirements['aktivasi_finansial'])) {
            $errorMessage = $this->goldcard_service->messageGoldcard('ajukan.aktivasifinansial');
            $response = $this->send_response('error', $errorMessage['message_content'], $errorMessage);
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        // Logic cek is open tabungan emas
        if (!$this->goldcard_service->isOpenTE($user, $requirements['open_te'])) {
            $errorMessage = $this->goldcard_service->messageGoldcard('ajukan.opente');
            $response = $this->send_response('error', $errorMessage['message_content'], $errorMessage);
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        // Logic cek is register gte
        if (!$this->goldcard_service->isRegisterGTE($user, $requirements['registrasi_gte'])) {
            $errorMessage = $this->goldcard_service->messageGoldcard('ajukan.registrasigte');
            $response = $this->send_response('error', $errorMessage['message_content'], $errorMessage);
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        // postRegistrations untuk get application number
        $res = $this->goldcard_service->postRegistrations($user);

        if ($res['code'] != 00) {
            $errorMessage = $this->goldcard_service->messageGoldcard('get_application_number.error');
            $response = $this->send_response('error', $errorMessage['message_content'], $errorMessage);
            log_message('debug', 'end of ' . __FUNCTION__ . ' GET request with response: ' . json_encode($response));
            return;
        }

        // mendapatkan progres status pengajuan yang telah dilakukan user
        $statusPengajuan = $this->goldcard_service->getProgresStatusPengajuan($res['data']);

        $message = $this->goldcard_service->messageGoldcard($statusPengajuan);

        $response = $this->send_response('success', $message['message_content'], $message);
        log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
        return;
    }

    function alamat_post($method = '')
    {
        $token = $this->getToken();

        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', __FUNCTION__ . ' = Unauthorized');
            return;
        }

        $user = $this->User->profile($token->id);
        $response = [];

        switch ($method) {
            case 'simpan':
                $this->form_validation->set_data($this->post());
                // cardDeliver must contain 1 home address or 2 office address
                $this->form_validation->set_rules('cardDeliver', 'Card Deliver', 'required|numeric|in_list[1,2]');

                if ($this->form_validation->run() == false) {
                    $response = $this->send_response('error', 'Data tidak lengkap', $this->form_validation->error_array());
                    return;
                }

                $setAlamat = $this->goldcard_service->setAlamatPengiriman($user->GoldCardApplicationNumber, $this->post());
                if ($setAlamat['code'] != "00") {
                    $response = $this->send_response('error', 'Set alamat gagal', $setAlamat['message']);
                    break;
                }

                $response = $this->send_response('success', 'Set alamat pengiriman sukses', $setAlamat['data']);
                break;
            case '':
                $getAlamat = $this->goldcard_service->getAlamatPengiriman($user->GoldCardApplicationNumber);
                if ($getAlamat['code'] != "00") {
                    $response = $this->send_response('error', 'Get alamat gagal', $getAlamat['message']);
                    break;
                }

                $response = $this->send_response('success', 'Get alamat sukses', $getAlamat['data']);
                break;
            default:
                $response = $this->send_response('error', 'Method tidak ada', ["status" => false]);
                break;
        }
        log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
        return;
    }

    public function ajukan_limit_post()
    {
        $token = $this->getToken();

        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', __FUNCTION__ . ' = Unauthorized');
            return;
        }

        $this->form_validation->set_data($this->post());

        $this->form_validation->set_rules('nominal_limit', 'Nominal Limit', 'required');
        $this->form_validation->set_rules('norek', 'Nomor Rekening', 'required');
        if ($this->form_validation->run() == false) {
            $response = $this->send_response('error', 'Data tidak lengkap', $this->form_validation->error_array());
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        $user = $this->User->profile($token->id);

        // get harga emas terbaru
        $harga_emas = $this->MainModel->getLastHargaEmas();

        // get requirement from Goldcard API
        $requirements = $this->goldcard_service->getRequirements();

        // get list tabungan emas
        $inquiryTabEmas = $this->inquiryTabEmas($user->cif, $channelId = '6017', $flag = 'new');

        if ($inquiryTabEmas->responseCode != '00') {
            $errorMessage = $this->goldcard_service->messageGoldcard('list_te.ErrorServer');
            $response = $this->send_response('error', $errorMessage['message_content'], $errorMessage);
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        // tabungan emas yang dipilih user
        $tab_emas = $this->goldcard_service->selectTE(json_decode($inquiryTabEmas->data)->listTabungan, $this->post('norek'));

        // logic nomor rekening tabungan emas tidak ada
        if (!$tab_emas) {
            $errorMessage = $this->goldcard_service->messageGoldcard('ajukan_limit.TENotFound');
            $response = $this->send_response('error', $errorMessage['message_content'], $errorMessage);
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        // logic dapat dibagi 10.000
        if (!$this->goldcard_service->DividedBy10Thousand($this->post('nominal_limit'))) {
            $errorMessage = $this->goldcard_service->messageGoldcard('ajukan_limit.DividedBy10Thousand');
            $response = $this->send_response('error', $errorMessage['message_content'], $errorMessage);
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        // logic max limit 999.900.000
        if (!$this->goldcard_service->MaxLimitGoldcard($this->post('nominal_limit'), $requirements['limit_pengajuan_max'])) {
            $errorMessage = $this->goldcard_service->messageGoldcard('ajukan_limit.MaxLimitGoldcard');
            $response = $this->send_response('error', $errorMessage['message_content'], $errorMessage);
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        // logic min limit 3.000.000
        if (!$this->goldcard_service->MinLimitGoldcard($this->post('nominal_limit'), $requirements['limit_pengajuan_min'])) {
            $errorMessage = $this->goldcard_service->messageGoldcard('ajukan_limit.MinLimitGoldcard');
            $response = $this->send_response('error', $errorMessage['message_content'], $errorMessage);
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        // logic saldo tidak mencukupi dan saldo efektif minimal 0,1 gram
        $cekSaldoEffektif = $this->goldcard_service->cekSaldoEfektif($this->post('nominal_limit'), $harga_emas['hargaBeli'], $tab_emas->saldoEfektif);
        if ($cekSaldoEffektif == "errorSaldoTidakMencukupi") {
            $errorMessage = $this->goldcard_service->messageGoldcard('ajukan_limit.InsufficientBalance');
            $response = $this->send_response('error', $errorMessage['message_content'], $errorMessage);
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        if ($cekSaldoEffektif == "errorSaldoMinimum") {
            $errorMessage = $this->goldcard_service->messageGoldcard('ajukan_limit.MinSaldoEfektif');
            $response = $this->send_response('error', $errorMessage['message_content'], $errorMessage);
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        $setLimitPengajuan = $this->goldcard_service->setLimitPengajuan([
            'appNumber' => $user->GoldCardApplicationNumber,
            'cardLimit' => $this->post('nominal_limit'),
            'harga_emas' => $harga_emas['hargaBeli']
        ]);

        if ($setLimitPengajuan['code'] != "00") {
            $errorMessage = $this->goldcard_service->messageGoldcard('ajukan_limit.goldcardError');
            $response = $this->send_response('error', $errorMessage['message_header'], $errorMessage, "11");
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        $message = $this->goldcard_service->messageGoldcard('ajukan_limit.sukses');
        $response = $this->send_response('success', $message['message_content'], $setLimitPengajuan['data']);
        log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
        return;
    }

    function data_diri_post()
    {
        $token = $this->getToken();

        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', __FUNCTION__ . ' Unauthorized');
            return;
        }

        $user = $this->User->profile($token->id);

        $this->form_validation->set_data($this->post());

        // Validasi form harus terisi semua
        $this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'required');
        $this->form_validation->set_rules('nama_ibu', 'Nama Ibu', 'required');
        $this->form_validation->set_rules('foto_ktp_base_64', 'Foto KTP Base 64', 'required');
        $this->form_validation->set_rules('foto_diri_base_64', 'Foto Diri Base 64', 'required');
        $this->form_validation->set_rules('no_hp_kerabat', 'No Handphone Kerabat', 'required|numeric');
        $this->form_validation->set_rules('addressLine1', 'Address Line', 'required');
        $this->form_validation->set_rules('province', 'Province', 'required');
        $this->form_validation->set_rules('addressCity', 'City', 'required');
        $this->form_validation->set_rules('subdistrict', 'Subdistrict', 'required');
        $this->form_validation->set_rules('village', 'Village', 'required');

        if ($this->form_validation->run() == false) {
            // Send Response
            $response = $this->send_response('error', 'Data tidak lengkap', $this->form_validation->error_array());
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        // Validasi salah, mengembalikkan response dan mengarahkan kembali ke screen Pengajuan Kartu Tabungan Emas
        if ($user->tglLahir != $this->post('tgl_lahir') || $user->namaIbu != $this->post("nama_ibu")) {
            $errorMessage = $this->goldcard_service->messageGoldcard('gold_card_submission.mismatchData');
            $response = $this->send_response('error', $errorMessage['message_content'], $errorMessage);
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        // Mengambil data full nasabah ke core via switching
        $coreCustomerData = $this->goldcard_service->getCustomerData($user);
        if (empty($coreCustomerData['responseCode']) || $coreCustomerData['responseCode'] != "00") {
            $errorMessage = $this->goldcard_service->messageGoldcard('gold_card_switching_getCustomerData.gagal');
            $response = $this->send_response('error', $errorMessage['message_content'], '');
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        // Send data diri data to goldcard API
        $postDataDiri = $this->goldcard_service->postDataDiriToGoldcard([
            $user, json_decode($coreCustomerData['data']), $this->post()
        ]);
        if ($postDataDiri['code'] != "00") {
            $response = $this->send_response('error', $postDataDiri['message'], $postDataDiri['data']);
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }
        $message = $this->goldcard_service->messageGoldcard('gold_card_submission.sukses');
        $response = $this->send_response('success', $message['message_content'], $message);
        log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ');
        return;
    }

    public function list_te_get()
    {
        $token = $this->getToken();

        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', __FUNCTION__ . ' = Unauthorized');
            return;
        }

        $user = $this->User->profile($token->id);

        // tabungan emas user
        $tab_emas = $this->inquiryTabEmas($user->cif, $channelId = '6017', $flag = 'new');

        // logic tidak dapat mendapatkan tabungan emas dari sisi server
        if ($tab_emas->responseCode != '00') {
            $errorMessage = $this->goldcard_service->messageGoldcard('list_te.ErrorServer');
            $response = $this->send_response('error', $errorMessage['message_content'], $errorMessage);
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        // logic tabungan emas tidak ada
        if (!$tab_emas->data) {
            $errorMessage = $this->goldcard_service->messageGoldcard('list_te.TENotFound');
            $response = $this->send_response('error', $errorMessage['message_content'], $errorMessage);
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        // Filter tabungan emas rekening induk
        $filter_tab_emas = $this->goldcard_service->filterRekening($tab_emas, $user->cif);
        if (!$filter_tab_emas) {
            $errorMessage = $this->goldcard_service->messageGoldcard('list_te.TENotFound');
            $response = $this->send_response('error', $errorMessage['message_content'], $errorMessage);
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        $message = $this->goldcard_service->messageGoldcard('list_te.sukses');

        $response = $this->send_response('success', $message['message_content'], $filter_tab_emas);
        log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
        return;
    }

    public function pilih_te_post()
    {
        $token = $this->getToken();

        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', __FUNCTION__ . ' = Unauthorized');
            return;
        }

        $this->form_validation->set_data($this->post());
        $this->form_validation->set_rules('norek', 'Nomor Rekening', 'required');
        if ($this->form_validation->run() == false) {
            $response = $this->send_response('error', 'Data tidak lengkap', $this->form_validation->error_array());
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        $user = $this->User->profile($token->id);

        // get list tabungan emas
        $inquiryTabEmas = $this->inquiryTabEmas($user->cif, $channelId = '6017', $flag = 'new');

        if ($inquiryTabEmas->responseCode != '00') {
            $errorMessage = $this->goldcard_service->messageGoldcard('list_te.ErrorServer');
            $response = $this->send_response('error', $errorMessage['message_content'], $errorMessage);
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        // get harga emas terbaru
        $harga_emas = $this->MainModel->getLastHargaEmas();

        // get requirement from Goldcard API
        $requirements = $this->goldcard_service->getRequirements();

        // tabungan emas yang dipilih user
        $tab_emas = $this->goldcard_service->selectTE(json_decode($inquiryTabEmas->data)->listTabungan, $this->post('norek'));

        // logic nomor rekening tabungan emas tidak ada
        if (!$tab_emas) {
            $errorMessage = $this->goldcard_service->messageGoldcard('pilih_te.TENotFound');
            $response = $this->send_response('error', $errorMessage['message_content'], $errorMessage);
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        // logic min saldo setara 3.000.000
        if (!$this->goldcard_service->MinSaldoTE($requirements['limit_pengajuan_min'], $harga_emas['hargaBeli'], $tab_emas->saldoEfektif)) {
            $errorMessage = $this->goldcard_service->messageGoldcard('pilih_te.MinSaldoTE');
            $response = $this->send_response('error', $errorMessage['message_content'], $errorMessage);
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        // set norek untuk dikirim ke goldcard API
        $setNorek = $this->goldcard_service->setNorek($user->GoldCardApplicationNumber, $this->post('norek'));
        log_message('debug', __FUNCTION__ . ' set norek goldcard ' . json_encode($this->post()));
        if ($setNorek['code'] != "00") {
            $response = $this->send_response('error', 'Set norek goldcard gagal', $setNorek['message']);
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }
        $response = $this->send_response('success', 'Set norek goldcard sukses', $setNorek['data']);
        log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
        return;
    }

    public function cek_saldo_te_get()
    {
        $token = $this->getToken();
        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', __FUNCTION__ . ' = Unauthorized');
            return;
        }

        $user = $this->User->profile($token->id);

        // get list tabungan emas
        $inquiryTabEmas = $this->inquiryTabEmas($user->cif, $channelId = '6017', $flag = 'new');
        if ($inquiryTabEmas->responseCode != '00') {
            $errorMessage = $this->goldcard_service->messageGoldcard('cek_saldo_te.TENotFound');
            $response = $this->send_response('error', $errorMessage['message_content'], $errorMessage);
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        // get query param
        $norek = $this->query('nomer_rekening');
        $norek_validation = empty($norek) || strlen($norek) != 16 || !(is_numeric($norek)) ? false : true;

        if ($norek_validation == false) {
            $response = $this->send_response('error', 'Data tabungan salah', $this->form_validation->error_array());
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        $saldo = $this->goldcard_service->getSaldoGoldcard($inquiryTabEmas, $norek, $user->cif);
        if (!$saldo) {
            $errorMessage = $this->goldcard_service->messageGoldcard('cek_saldo_te.TENotFound');
            $response = $this->send_response('error', $errorMessage['message_content'], $errorMessage);
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }
        $response = $this->send_response('success', '', $saldo);
        log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
        return;
    }

    public function final_post()
    {
        $token = $this->getToken();

        if (!$token) {
            $this->errorUnAuthorized();
            return;
        }

        $user = $this->User->profile($token->id);

        // set finalisasi data ke Goldcard API
        $this->send_response($this->goldcard_service->setFinalisasi($user));
        return;
    }

    public function status_pengajuan_post()
    {
        $token = $this->getToken();

        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', __FUNCTION__ . ' = Unauthorized');
            return;
        }

        $user = $this->User->profile($token->id);

        // get status pengajuan dari goldcard API
        $getStatusPengajuan = $this->goldcard_service->getStatusPengajuan($user->GoldCardApplicationNumber);
        log_message('debug', __FUNCTION__ . ' get status pengajuan goldcard ' . json_encode($this->post()));
        if ($getStatusPengajuan['code'] != "00") {
            $response = $this->send_response('error', 'get status pengajuan goldcard gagal', $getStatusPengajuan['message']);
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        $response = $this->send_response('success', 'get status pengajuan goldcard sukses', $getStatusPengajuan['data']);
        log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
        return;
    }

    function send_otp_get()
    {
        $token = $this->getToken();
        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', __FUNCTION__ . ' = Unauthorized');
            return;
        }

        // Get query param
        $request_type = $this->query('request_type');

        // Send otp via core
        $reffId = mt_rand(1000, 9999);
        $sendOTP = $this->sendOTP($token->no_hp, $reffId, $request_type);
        log_message('debug', 'send otp core response' . json_encode($sendOTP));
        if ($sendOTP->responseCode != '00') {
            $this->send_response('error', 'Terjadi kesalahan mohon coba lagi', '');
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($sendOTP));
            return;
        }
        $this->send_response('success', 'Berhasil mengirim otp', $sendOTP);
        log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($sendOTP));
        return;
    }

    function check_otp_post()
    {
        $token = $this->getToken();
        if (!$token) {
            $this->errorUnAuthorized();
            return;
        }

        // Form validation
        $this->form_validation->set_data($this->post());
        $this->form_validation->set_rules('otp', 'otp', 'numeric|required|exact_length[6]');
        if ($this->form_validation->run() == false) {
            $response = $this->send_response('error', 'Invalid Input', $this->form_validation->error_array());
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        // Set post variable
        $request_type = $this->post('request_type') ?: 'aktivasi';
        $otp = $this->post('otp');

        // Send check otp to core
        log_message('debug', 'start sending check_otp request to core');
        $checkOtp = $this->checkOTP($otp, $token->no_hp, $request_type);
        log_message('debug', 'end sending check_otp request to core with response : ' . json_encode($checkOtp));

        // Validate check otp response
        if ($checkOtp->responseCode != '00') {
            $response = $this->send_response('error', 'OTP tidak ditemukan/expired', $checkOtp);
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        // send response
        $otpData = json_decode($checkOtp->data);
        $response = $this->send_response('success', 'OTP valid', $otpData);
        log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
        return;
    }

    public function cek_aktivasi_post()
    {
        $token = $this->getToken();

        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', __FUNCTION__ . ' Unauthorized');
            return;
        }

        $response = $this->goldcard_service->cekAktivasi($token);
        log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
        $this->send_response($response);
        return;
    }

    public function aktivasi_kartu_post()
    {
        $token = $this->getToken();
        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', __FUNCTION__ . ' Unauthorized');
            return;
        }

        $this->form_validation->set_data($this->post());
        $this->form_validation->set_rules('nomor_kartu_pertama', 'Nomor Kartu Pertama', 'numeric|required|exact_length[6]');
        $this->form_validation->set_rules('nomor_kartu_terakhir', 'Nomor Kartu Terakhir', 'numeric|required|exact_length[4]');
        $this->form_validation->set_rules('tanggal_berlaku', 'Tanggal Berlaku Kartu', 'required|exact_length[5]');
        $this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required');
        if ($this->form_validation->run() == false) {
            $response = $this->send_response('error', 'Data tidak lengkap', $this->goldcard_service->messageGoldcard('aktivasi_kartu.dataSalah'));
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        $user = $this->User->profile($token->id);

        // Validasi aktifasi goldcard
        $cekAktivasiKartu = $this->goldcard_service->cekAktivasiKartu($user->GoldCardApplicationNumber, $this->post());
        if ($cekAktivasiKartu['code'] != "00") {
            $response = $this->send_response('error', $cekAktivasiKartu['message_content'], $cekAktivasiKartu, $cekAktivasiKartu['code']);
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        // Update goldcard_account_number pada tabel user
        $updateAccountNumber = $this->goldcard_service->updateAccountNumber($token->id, $cekAktivasiKartu['data']['accountNumber']);
        if ($updateAccountNumber !== true) {
            $response = $this->send_response('error', $updateAccountNumber['message'], '');
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        $response = $this->send_response('success', $cekAktivasiKartu['message'], $cekAktivasiKartu['data'], $cekAktivasiKartu['code']);
        log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
        return;
    }

    public function data_pekerjaan_post()
    {
        $token = $this->getToken();
        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', __FUNCTION__ . ' Unauthorized');
            return;
        }

        // Mendapatkan data user dan data pekerjaan
        $user = $this->User->profile($token->id);

        $this->form_validation->set_data($this->post());
        // Validasi form harus terisi semua
        $this->form_validation->set_rules('pekerjaan', 'Pekerjaan', 'required');
        $this->form_validation->set_rules('status_pekerjaan', 'Status Pekerjaan', 'required');
        $this->form_validation->set_rules('jumlah_karyawan', 'Jumlah Karyawan', 'required');
        $this->form_validation->set_rules('nama_perusahaan', 'Nama Perusahaan', 'required');
        $this->form_validation->set_rules('alamat_kantor', 'Alamat Kantor', 'required');
        $this->form_validation->set_rules('nomor_telepon_kantor', 'Telepon Kantor', 'numeric|required');
        $this->form_validation->set_rules('jumlah_pendapatan', 'Jumlah Pendapatan', 'numeric|required');
        $this->form_validation->set_rules('lama_bekerja', 'Lama Bekerja', 'numeric|required');
        // provinsi_kantor contains street name with number with RT RW
        $this->form_validation->set_rules('provinsi_kantor', 'Provinsi Kantor', 'required');
        $this->form_validation->set_rules('kota_kantor', 'Kota Kantor', 'required');
        $this->form_validation->set_rules('kecamatan_kantor', 'Kecamatan Kantor', 'required');
        $this->form_validation->set_rules('kelurahan_kantor', 'Kelurahan Kantor', 'required');

        if ($this->form_validation->run() == false) {
            $response = $this->send_response('error', 'Data tidak lengkap', $this->form_validation->error_array());
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        // Mengirim data pekerjaan ke goldcard
        $postDataPekerjaan = $this->goldcard_service->postDataPekerjaan([$user, $this->post()]);
        if ($postDataPekerjaan['code'] != "00") {
            $response = $this->send_response('error', $postDataPekerjaan['message'], $postDataPekerjaan['data']);

            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        $message = $this->goldcard_service->messageGoldcard('data_pekerjaan.sukses');
        $response = $this->send_response('success', $message['message_content'], $message);
        log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
        return;
    }

    public function riwayat_transaksi_get()
    {
        $token = $this->getToken();
        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', __FUNCTION__ . ' Unauthorized');
            return;
        }

        // Get data history transaksi dari goldcard
        $getRiwayat = $this->goldcard_service->getRiwayatTransaksi($token, $this->query());
        if ($getRiwayat == false) {
            $message = $this->goldcard_service->messageGoldcard('gc_hist_transaksi.gagal');
            $this->send_response('error', $message['message_content'], '');
            return;
        }

        $message = $this->goldcard_service->messageGoldcard('gc_hist_transaksi.sukses');
        $this->send_response('success', $message['message_content'], $getRiwayat['data']);
        log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($getRiwayat));

        return;
    }

    public function saldo_get()
    {
        $token = $this->getToken();
        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', __FUNCTION__ . ' = Unauthorized');
            return;
        }

        // Get saldo kartu emas
        $saldo = $this->goldcard_service->getSaldo($token);
        if (!$saldo || $saldo['code'] != "00") {
            $errorMessage = $this->goldcard_service->messageGoldcard('cek_saldo.notFound');
            $response = $this->send_response('error', $errorMessage['message_content'], '');
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        $response = $this->send_response('success', $saldo['message'], $saldo['data']);
        log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
        return;
    }

    public function cetak_tagihan_get()
    {
        $token = $this->getToken();
        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', __FUNCTION__ . ' = Unauthorized');
            return;
        }

        // Get cetak tagihan kartu emas
        $cetakTagihan = $this->goldcard_service->getCetakTagihan($token);
        if (!$cetakTagihan || $cetakTagihan['code'] != "00") {
            $errorMessage = $this->goldcard_service->messageGoldcard('cetak_tagihan.notFound');
            $response = $this->send_response('error', $errorMessage['message_content'], '');
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        $response = $this->send_response('success', 'Berhasil mengambil data cetak tagihan', $cetakTagihan['data']);
        log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
        return;
    }

    // request payload:
    // account_number, payment_amount
    public function hidden_inquiry_pembayaran_post()
    {
        $token = $this->getToken();
        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', __FUNCTION__ . ' Unauthorized');
            return;
        }

        $this->form_validation->set_data($this->post());
        $this->form_validation->set_rules('payment_amount', 'Payment Amount', 'numeric|required');

        if ($this->form_validation->run() == false) {
            $response = $this->response_service->fValidationErrResponse($this->form_validation->error_array());
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            $this->send_response($response);
            return;
        }

        // Validasi payment inquiry ke Goldcard API
        $response = $this->goldcard_service->validatePaymentInquiry($this->post(), $token);
        log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
        $this->send_response($response);
        return;
    }

    // request payload:
    // amount, payment, booking_code, card_number, token_response, pin, va
    function hidden_create_payment_post()
    {
        $token = $this->getToken();
        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', __FUNCTION__ . ' Unauthorized');
            return;
        }

        $this->form_validation->set_data($this->post());
        $this->form_validation->set_rules('amount', 'amount', 'required');
        $this->form_validation->set_rules('payment', 'payment', 'required');
        $this->form_validation->set_rules('pin', 'pin', 'required|exact_length[6]|numeric');

        if ($this->post('payment') == 'MANDIRI') {
            $this->form_validation->set_rules('booking_code', 'booking_code', 'required');
            $this->form_validation->set_rules('card_number', 'card_number', 'required');
            $this->form_validation->set_rules('token_response', 'token_response', 'required');
        }

        if ($this->post('payment') == 'GCASH') {
            $this->form_validation->set_rules('va', 'va', 'required|numeric');
        }

        if ($this->form_validation->run() == false) {
            $response = $this->response_service->fValidationErrResponse($this->form_validation->error_array());
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            $this->send_response($response);
            return;
        }

        $response = $this->goldcard_service->gcCreatePayment($this->post(), $token);
        log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
        $this->send_response($response);
        return;
    }

    public function check_tagihan_gte_get()
    {
        $token = $this->getToken();
        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', __FUNCTION__ . ' Unauthorized');
            return;
        }

        $checkTagihan = $this->goldcard_service->checkTagihanGTE($token);

        $response = $this->send_response($checkTagihan['status'], $checkTagihan['message'], "");
        log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
        return;
    }

    function portofolio_get()
    {
        $token = $this->getToken();

        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', __FUNCTION__ . ' = Unauthorized');
            return;
        }

        $response = $this->goldcard_service->gcGetListGte($token);
        log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
        $this->send_response($response);
        return;
    }

    // request payload:
    // alasan, kode_alasan
    function blokir_kartu_post()
    {
        $token = $this->getToken();
        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', __FUNCTION__ . ' Unauthorized');
            return;
        }

        $this->form_validation->set_data($this->post());
        $this->form_validation->set_rules('alasan', 'Alasan', 'required');
        $this->form_validation->set_rules('kode_alasan', 'Kode Alasan', 'required');

        if ($this->form_validation->run() == false) {
            $response = $this->response_service->fValidationErrResponse($this->form_validation->error_array());
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            $this->send_response($response);
            return;
        }

        $response = $this->goldcard_service->gcBlockCard($this->post(), $token);
        log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
        $this->send_response($response);
        return;
    }

    public function pengajuan_kenaikan_limit_post($method = 'submit')
    {
        $token = $this->getToken();

        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', __FUNCTION__ . ' = Unauthorized');
            return;
        }

        // validasi method
        $listMethod = ['inquiry', 'submit'];
        if (!in_array($method, $listMethod)) {
            $response = $this->send_response('error', 'Method tidak ada', ["status" => false]);
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return;
        }

        $this->form_validation->set_data($this->post());

        $user = $this->User->profile($token->id);

        // do inquiry increase limit to goldcard api if it is method inquiry
        // doing the NPWP, effective gold balance checking, minimum effective gold balance remain, and minimum increase limit validation
        if ($method == 'inquiry') {
            $this->form_validation->set_rules('nominal_limit', 'Nominal Limit', 'required|numeric');

            if ($this->form_validation->run() == false) {
                $response = $this->response_service->fValidationErrResponse($this->form_validation->error_array());
                log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
                $this->send_response($response);
                return;
            }

            $inquiryLimit = $this->goldcard_service->inquiryKenaikanLimit([
                'accNumber' => $user->goldCardAccountNumber,
                'nominalLimit' => $this->post('nominal_limit')
            ]);
        }

        // do submit increase limit to goldcard api if it is method submit
        if ($method == 'submit') {
            $this->form_validation->set_rules('ref_id', 'Ref ID', 'required');

            if ($this->form_validation->run() == false) {
                $response = $this->response_service->fValidationErrResponse($this->form_validation->error_array());
                log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
                $this->send_response($response);
                return;
            }

            $inquiryLimit = $this->goldcard_service->submitKenaikanLimit([
                'npwp' => $this->post('npwp'),
                'refID' => $this->post('ref_id')
            ]);
        }

        $response = $this->send_response($inquiryLimit['status'], $inquiryLimit['message'], $inquiryLimit['data'], $inquiryLimit['code']);
        log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
        return;
    }

    // request payload: none
    function reset_post()
    {
        $token = $this->getToken();
        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', __FUNCTION__ . ' Unauthorized');
            return;
        }

        $response = $this->goldcard_service->gcResetRegistration($token);
        log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
        $this->send_response($response);
        return;
    }

    function norek_pilihan_get()
    {
        $token = $this->getToken();

        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', __FUNCTION__ . ' = Unauthorized');
            return;
        }

        $response = $this->goldcard_service->getNorekPilihan($token);
        log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
        $this->send_response($response);
        return;
    }
}
