<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once 'CorePegadaian.php';

/**
 * @property Notification_service notification_service
 * @property Emas_service emas_service
 */
class Emas extends CorePegadaian
{
    public function __construct()
    {
        parent::__construct();
        $models = [
            'User',
            'Otp',
            'BankModel',
            'PaymentModel',
            'NotificationModel',
            'EmasModel',
            'ConfigModel',
            'GcashModel',
            'GpoinModel',
            'ProductMasterModel',
            'MasterModel'
        ];

        $this->load->model($models);
        $this->load->library('form_validation');
        $this->load->helper('message');
        $this->load->service('Emas_service');
        $this->load->service('Payment_service');
        $this->load->service('Notification_service', 'notification_service');
    }

    function open_post($method = 'inquiry')
    {
        $token = $this->getToken();
        if ($token) {
            $user = $this->User->getUser($token->id);

            if ($method == 'inquiry') {
                $this->_openInquiry($token, $user);
            } elseif ($method == 'payment') {
                $this->_openPayment($token, $user);
            } else {
                $this->set_response([
                    'status' => 'error',
                    'message' => 'Path harus inquiry atau payment',
                    'code' => 101
                ]);
            }
        } else {
            $this->errorUnAuthorized();
        }
    }

    function _openInquiry($token, $user)
    {
        $setData = array(
            'amount' => $this->post('amount'),
            'domisili' => $this->post('domisili'),
            'nama' => $this->post('nama'),
            'jenis_identitas' => $this->post('jenis_identitas'),
            'no_identitas' => $this->post('no_identitas'),
            'tanggal_expired_identitas' => $this->post('tanggal_expired_identitas'),
            'tempat_lahir' => $this->post('tempat_lahir'),
            'tanggal_lahir' => $this->post('tanggal_lahir'),
            'no_hp' => $this->post('no_hp'),
            'jenis_kelamin' => $this->post('jenis_kelamin'),
            'status_kawin' => $this->post("status_kawin"),
            'kode_kelurahan' => $this->post("kode_kelurahan"),
            'jalan' => $this->post("jalan"),
            'ibu_kandung' => $this->post("ibu_kandung"),
            'kewarganegaraan' => $this->post("kewarganegaraan"),
            'kode_cabang' => $this->post("kode_cabang"),
            'promo_code' => $this->post('promo_code'),
            'voucher_id' => $this->post('voucher_id')
        );

        $this->form_validation->set_data($setData);

        $this->form_validation->set_rules('amount', 'Amount', 'required');
        $this->form_validation->set_rules('domisili', 'Domisili', 'required');
        $this->form_validation->set_rules('nama', 'Nama Nasabah', 'required');
        $this->form_validation->set_rules('jenis_identitas', 'Jenis Identitas', 'required');
        $this->form_validation->set_rules('no_identitas', 'No Identitas', 'required|exact_length[16]');
        
        if ($this->post('jenis_identitas')=='12') {
            $this->form_validation->set_rules('tanggal_expired_identitas', 'tanggal_expired_identitas', 'required');
        }

        $this->form_validation->set_rules('tempat_lahir', 'Tempat Lahir', 'required');
        $this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required');
        $this->form_validation->set_rules('no_hp', 'No HP', 'required');
        $this->form_validation->set_rules('jenis_kelamin', 'Jenis Kelamin', 'required');
        $this->form_validation->set_rules('status_kawin', 'Status Kawin', 'required');

        if ($this->post('domisili') == '1') {
            $this->form_validation->set_rules('kode_kelurahan', 'Kode kelurahan', 'required');
        } elseif ($this->post('domisili') == '2') {
            $this->form_validation->set_rules('jalan', 'Jalan', 'required');
        }

        $this->form_validation->set_rules('ibu_kandung', 'Ibu Kandung', 'required');
        $this->form_validation->set_rules('kewarganegaraan', 'Jenis Kelamin', 'required');
        $this->form_validation->set_rules('kode_cabang', 'Kode Cabang', 'required');
        
        if ($this->form_validation->run() == false) {
            $this->set_response(array(
                'status' => 'error',
                'code' => 201,
                'message' => '',
                'errros' => $this->form_validation->error_array()
            ), 200);
        } else {
            $amount = $this->post('amount');
            $domisili = $this->post('domisili');
            $nama = $this->post('nama');
            $jenis_identitas = $this->post('jenis_identitas');
            $no_identitas = $this->post('no_identitas');
            $tanggal_expired_identitas = $this->post('tanggal_expired_identitas');
            $tempat_lahir = $this->post('tempat_lahir');
            $tanggal_lahir = $this->post('tanggal_lahir');
            $no_hp = $this->post('no_hp');
            $jenis_kelamin = $this->post('jenis_kelamin');
            $status_kawin = $this->post("status_kawin");

            $kode_kelurahan = $this->post("kode_kelurahan");
            $jalan = $this->post("jalan");

            $ibu_kandung = $this->post("ibu_kandung");
            $kewarganegaraan = $this->post("kewarganegaraan");
            $kode_cabang = $this->post("kode_cabang");
            $flag = 'K';
            
            $promoCode = $this->post('promo_code');
            $voucherId = $this->post('voucher_id');

            $uploadDir = $this->config->item('upload_dir');

            $namaFoto = '';
            if (!isset($_FILES['userfile']['name'])) {
                // get foto name from UserModel
                $namaFoto = $this->User->profile($token->id)->fotoKTP;
                if (!$namaFoto) {
                    $this->response([
                        'code' => 101,
                        'status' => 'error',
                        'message' => 'Foto KTP tidak boleh kosong',
                        'errors' => 'Foto KTP Tidak boleh kosong'
                    ], 200);
                    return;
                }
            } else {
                $config['upload_path'] = $uploadDir . '/user/ktp';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
                $config['max_size'] = 0;
                $config['max_width'] = 0;
                $config['max_height'] = 0;
                $config['encrypt_name'] = true;

                $this->load->library('upload', $config);

                $uploadData = null;
                if (!$this->upload->do_upload('userfile')) {
                    $this->response([
                        'code' => 101,
                        'status' => 'error',
                        'message' => '',
                        'errors' => $this->upload->display_errors()
                    ], 200);
                    return;
                } else {
                    //Delete previous user file
                    $prevFile = $this->User->profile($token->id)->fotoKTP;

                    if ($prevFile != null && file_exists($uploadDir . '/user/ktp/' . $prevFile)) {
                        unlink($uploadDir . '/user/ktp/' . $prevFile);
                    }

                    $uploadData = $this->upload->data();
                    $namaFoto = $uploadData['file_name'];
                }
            }

            /*
             * Lakukan inquiry tabungan.
             * Response data:
             * administrasi
             */
            
            $inquiryTabunganEmasData = [
                'channelId' => $token->channelId,
                'clientId' => $this->config->item('core_post_username'),
                'jenisTransaksi' => 'OP',
                'flag' => $flag,
                'amount' => $amount,
                'norek' => null,
                'kodeCabang' => $kode_cabang,
                'cif' => $user->cif,
            ];

            $jurnalAccount = null;

            // Lakukan validasi promo/voucher gpoint jika ada
            if ($promoCode != null) {
                if ($user->cif == null) {
                    return $this->set_response([
                        'status' => 'error',
                        'message' => 'Anda harus memiliki CIF untuk menggunakan fitur voucher',
                        'code' => 102
                    ]);
                }

                $validateData = [
                    'promoCode' => $promoCode,
                    'voucherId' => $voucherId,
                    'userId' => $user->cif,
                    'transactionAmount' => (int) $amount,
                    'validators' => [
                        'channel' => $this->config->item('core_post_username'),
                        'product' => '62',
                        'transactionType' => 'OP',
                        'unit' => 'rupiah',
                    ]
                ];

                $validateVoucher = $this->gPointValidateVoucher($validateData);

                if ($validateVoucher->status == 'Error') {
                    return $this->set_response([
                        'status' => 'error',
                        'message' => $validateVoucher->message,
                        'code' => 103,
                    ]);
                } elseif ($validateVoucher->status == 'Success') {
                    $inquiryTabunganEmasData['discountAmount'] = $validateVoucher->data->discount;
                    $inquiryTabunganEmasData['idPromosi'] = $validateVoucher->data->journalAccount.';'.$promoCode;
                }
            }

            $inquiry = $this->inquiryTabunganEmas($inquiryTabunganEmasData);
            if ($inquiry->responseCode == '00') {
                $dataInquiry = json_decode($inquiry->data);

                //Dapatkan id transaksi sesuai dengan reffSwitching
                $administrasi = $dataInquiry->administrasi;
                $satuan = $dataInquiry->satuan;
                $harga = $dataInquiry->harga;
                $totalKewajiban = $dataInquiry->totalKewajiban;
                $hargaEmas = $dataInquiry->harga;
                $nilaiTransaksi = $dataInquiry->nilaiTransaksi;
                $hakNasabah = $dataInquiry->hakNasabah;
                $surcharge = $dataInquiry->surcharge;
                $reffCore = $dataInquiry->reffCore;
                $saldoEmas = $dataInquiry->saldoEmas;
                $saldoNominal = $dataInquiry->saldoNominal;
                $biayaTitip = $dataInquiry->biayaTitip;
                $namaProduk = $dataInquiry->namaProduk;
                $tglTransaksi = $dataInquiry->tglTransaksi;
                $trxId =$dataInquiry->reffSwitching;
                $gram = $dataInquiry->gram;

                $paymentData = array(
                    'id_transaksi' => $trxId,
                    'jenis_transaksi' => 'OP',
                    'amount' => $amount,
                    'gram' => $gram,
                    'administrasi' => $administrasi,
                    'kode_produk' => '62',
                    'harga' => $harga,
                    'satuan' => $satuan,
                    'saldoEmas' => $saldoEmas,
                    'total_kewajiban' => $totalKewajiban,
                    'reffCore' =>$reffCore,
                    'user_AIID' => $token->id,
                    'keterangan' => 'Pembelian Emas',
                    'surcharge' => $surcharge,
                    'reffCore' => $reffCore,
                    'nilaiTransaksi' => $nilaiTransaksi,
                    'hakNasabah' => $hakNasabah,
                    'saldoNominal' => $saldoNominal,
                    'biayaTitip' => $biayaTitip,
                    'namaProduk' => $namaProduk,
                    'tglTransaksi' => $tglTransaksi,
                    'reffSwitching' => $trxId
                );

                if (isset($inquiryTabunganEmasData['discountAmount'], $inquiryTabunganEmasData['idPromosi'])) {
                    $paymentData['discountAmount'] = $inquiryTabunganEmasData['discountAmount'];
                    $paymentData['idPromosi'] = $inquiryTabunganEmasData['idPromosi'];
                }

                $this->PaymentModel->add($paymentData);

                $this->User->updateUser($token->id, [
                    'jenis_identitas' => $jenis_identitas,
                    'no_ktp' => $no_identitas,
                    'tanggal_expired_identitas' => $tanggal_expired_identitas,
                    'tempat_lahir' => $tempat_lahir,
                    'tgl_lahir' => $tanggal_lahir,
                    'jenis_kelamin' => $jenis_kelamin,
                    'status_kawin' => $status_kawin,
                    'id_kelurahan' => $kode_kelurahan,
                    'alamat' => $jalan,
                    'nama_ibu' => $ibu_kandung,
                    'kewarganegaraan' => $kewarganegaraan,
                    'kode_cabang' => $kode_cabang,
                    'foto_ktp_url' => $namaFoto,
                    'foto_url' => $namaFoto,
                    'domisili' => $domisili
                ]);

                $this->set_response([
                    'status' => 'success',
                    'message' => '',
                    'data' => $paymentData
                ]);
            } else {
                $this->set_response([
                    'status' => 'error',
                    'message' => $inquiry->responseCode.':'.$inquiry->responseDesc,
                    'code' => 103,
                    'reason' => $inquiry
                ]);
            }
        }
    }

    function _openPayment($token, $user)
    {
        $setData = array(
            'id_transaksi' => $this->post('id_transaksi'),
            'payment' => $this->post("payment"),
            'booking_code' => $this->post('booking_code'),
            'card_number' => $this->post('card_number'),
            'token_response' => $this->post('token_response'),
            'va' => $this->post('va'),
            'pin' => $this->post('pin'),
        );

        $this->form_validation->set_rules('id_transaksi', 'id_transaksi', 'required');
        $this->form_validation->set_rules('payment', 'Payment', 'required');
            
        $payment = $this->post('payment');
        
        if ($payment == 'MANDIRI') {
            $this->form_validation->set_rules('booking_code', 'booking_code', 'required');
            $this->form_validation->set_rules('card_number', 'card_number', 'required');
            $this->form_validation->set_rules('token_response', 'token_response', 'required');
        }
        
        if ($payment == 'WALLET' && $user->norek == '') {
            $this->set_response(array(
                'status' => 'error',
                'code' => 201,
                'message' => 'Anda belum melakukan aktifasi wallet',
            ), 200);
            return;
        }

        if ($payment == 'WALLET' || $payment == 'GCASH') {
            $this->form_validation->set_rules('pin', 'pin', 'required|exact_length[6]|numeric');
        }

        $this->form_validation->set_data($setData);

        if ($this->form_validation->run() == false) {
            $this->set_response(array(
                'status' => 'error',
                'code' => 201,
                'message' => '',
                'errros' => $this->form_validation->error_array()
            ), 200);
        } else {
            $trxId = $this->post('id_transaksi');
            $pin = $this->post('pin');
            $va = $this->post('va');

            $checkPayment = $this->PaymentModel->getPaymentByTrxId2($trxId);

            // Mendapatkan promo berdasarkan id transaksi
            $gpoin = $this->GpoinModel->get_promo($trxId);
            $idPromosi = '';
            $promoCode = '';
            $discountAmount = 0;
            $promoAmount = 0;
            if ($gpoin != '') {
                if ($gpoin->type == 'discount') {
                    $discountAmount = $gpoin->value;
                }
                $promoAmount = $gpoin->value;
                $idPromosi = $gpoin->idPromosi;
                $promoCode = $gpoin->promoCode;
            };
            
            if (!$checkPayment) {
                return $this->set_response([
                    'status' => 'error',
                    'message' => 'Transaksi tidak ditemukan',
                    'code' => 102
                ]);
            }
            $amount = $checkPayment->amount;
            $administrasi = $checkPayment->administrasi;
            $satuan = $checkPayment->satuan;
            $harga = $checkPayment->harga;
            $totalKewajiban = $checkPayment->totalKewajiban;
            $hargaEmas = $checkPayment->harga;
            $nilaiTransaksi = $checkPayment->nilaiTransaksi;
            $hakNasabah = $checkPayment->hakNasabah;
            $surcharge = $checkPayment->surcharge;
            $reffCore = $checkPayment->reffCore;
            $saldoEmas = $checkPayment->saldoEmas;
            $saldoNominal = $checkPayment->saldoNominal;
            $biayaTitip = $checkPayment->biayaTitip;
            $namaProduk = $checkPayment->namaProduk;
            $tglTransaksi = $checkPayment->tglTransaksi;
            $trxId =$checkPayment->reffSwitching;
            $gram = $checkPayment->gram;
            // $discountAmount = $checkPayment->discountAmount;
            // $idPromosi = $checkPayment->idPromosi;

            $jenisTransaksi = 'OP';
            $kodeProduct = '62';
            $keterangan = "PDSOP ".$trxId;

            $checkPayment->$discountAmount = $discountAmount;
            $checkPayment->data_user = $user;
            $checkPayment->data_token = $token;
            
            if ($payment == 'VA_MAYBANK') {
                $response = $this->Emas_service->paymentOpenTabemas($payment, $checkPayment, $discountAmount, $user, $token, '016');
                return $this->send_response($response);
            }

            if ($payment == 'FINPAY') {
                $checkPayment->kode_bank = '770';
                $response = $this->Emas_service->finpay_payment_open_tabemas($payment, $checkPayment);
                return $this->send_response($response);
            }

            //Buat billing melalui VA, Bisa BNI atau Mandiri
            if ($payment == 'BNI') {
                $biayaTransaksi =  $this->ConfigModel->getRealBiayaPayment('OP', 'BANK', '62', '009');
                $biayaTransaksiDisplay =  $this->ConfigModel->getBiayaPayment('OP', 'BANK', '62', '009');
                
                $billingBNI = $this->createBillingVABNI(
                    (string) ($amount + $biayaTransaksi - $discountAmount),
                    $user->email,
                    $user->nama,
                    $user->no_hp,
                    'K',
                    'OP',
                    $keterangan, //keterangan pembayaran emas
                    '',
                    $kodeProduct,
                    $trxId,
                    $token->channelId,
                    $idPromosi,
                    $promoAmount
                );

                if ($billingBNI->responseCode == '00') {
                    if (!isset($billingBNI->data)) {
                        $this->set_response(array(
                            'status' =>'error',
                            'message' => 'Terjadi kesalahan jaringan, mohon coba beberapa saat lagi',
                            'code' => 103
                        ), 200);
                        return;
                    }
                    
                    $billingData = json_decode($billingBNI->data);
                    
                    
                    $virtualAccount = $billingData->virtualAccount;
                    $tglExpired = $billingData->tglExpired;

                    $this->db->trans_start();

                    //Save tabungan emas dan update data user
                    $this->User->saveTabunganEmas(
                        $token->id,
                        $user->domisili,
                        $user->nama,
                        $user->jenis_identitas,
                        $user->no_ktp,
                        $user->tanggal_expired_identitas,
                        $user->tempat_lahir,
                        $user->tgl_lahir,
                        $user->no_hp,
                        $user->jenis_kelamin,
                        $user->status_kawin,
                        $user->id_kelurahan,
                        $user->alamat,
                        $user->nama_ibu,
                        $user->kewarganegaraan,
                        $user->kode_cabang,
                        'K',
                        $user->foto_url,
                        $checkPayment->amount,
                        $trxId
                    );
                    
                    $paymentData = array(
                        'tipe' => $payment,
                        'payment' => $payment,
                        'kodeBankPembayar' => '009',
                        'virtual_account' => $virtualAccount
                    );

                    //Simpan payment pada tahap ini, virtual account dan tgl expired masih kosong
                    $this->PaymentModel->update($trxId, $paymentData);

                    $this->db->trans_complete();
                    if ($this->db->trans_status() === false) {
                        $this->dbTransError('TRANSACTION ERROR OPEN TABUNGAN EMAS BNI ');
                    }

                    $template = $this->generateOpenTabemasNotif($user->nama, $payment, $amount + $biayaTransaksiDisplay - $discountAmount, $virtualAccount, $tglExpired, $trxId, $promoCode, $promoAmount);
                    
                    $emailTemplate= $template['email'];
                    $mobileTemplate = $template['mobile'];
                    $minimalTemplate = $template['minimal'];
                    
                    //Simpan notifikasi baru
                    $idNotif = $this->NotificationModel->add(
                        $token->id,
                        NotificationModel::TYPE_EMAS,
                        NotificationModel::CONTENT_TYPE_HTML,
                        $this->ConfigModel->getNamaProduk('OP', '62'),
                        "Segera Bayar Rp". number_format($amount + $biayaTransaksiDisplay - $discountAmount, 0, ",", ".")." ke ".$virtualAccount,
                        $mobileTemplate,
                        $minimalTemplate,
                        "OP"
                    );

                    //Kirim notifikasi pembayaran ke device user
                    Message::sendFCMNotif(
                        $this->User->getFCMToken($token->id),
                        [
                        "id" => $idNotif,
                        "tipe" => "OP",
                        "title" => $this->ConfigModel->getNamaProduk('OP', '62'),
                        "tagline" => "Segera Bayar Rp". number_format($amount + $biayaTransaksiDisplay - $discountAmount, 0, ",", ".")." ke ".$virtualAccount,
                        "content" => "Bayar ke " . $virtualAccount . " sebelum " . $tglExpired,
                        "paymentType" => $payment,
                        "token" => $token->no_hp
                        ]
                    );

                    //Kirim Email Notifikasi
                    $this->load->helper('message');
                    Message::sendEmailOpenTabunganPayment($token->email, $emailTemplate);


                    //Set response
                    $this->set_response([
                        'status' => 'success',
                        'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
                        'data' => array(
                            'idTransaksi' => $trxId,
                            'virtualAccount' => $virtualAccount,
                            'expired' => $tglExpired,
                            'now' => date('Y-m-d H:i:s')
                        )
                    ], 200);
                } else {
                    $this->set_response([
                        'status' => 'error',
                        'message' => 'Something went wrong. Please try again later.',
                        'code' => 103,
                        'reason' => $billingBNI
                    ], 200);
                }
            } elseif ($payment == 'MANDIRI') {
                $biayaTransaksi =  $this->ConfigModel->getRealBiayaPayment('OP', 'BANK', '62', '008');
                
                $bookingCode = $this->post('booking_code');
                $cardNumber = $this->post('card_number');
                $tokenResponse = $this->post('token_response');
                
                //Bayar dengan mandiri click pay
                $clickPay = $this->mandiriClickPay(
                    (string) ($amount + $biayaTransaksi - $discountAmount),
                    $bookingCode,
                    $cardNumber,
                    'OP',
                    "PDSOP " . $trxId . " ",
                    $user->no_hp,
                    '',
                    '62',
                    $tokenResponse,
                    $trxId,
                    $token->channelId
                );

                if ($clickPay->responseCode == '00') {
                    $clickpayData = json_decode($clickPay->data);
                    
                    $this->db->trans_start();

                    $paymentData = array(
                        'payment' => $payment,
                        'kodeBankPembayar' => '008',
                        'payment' => 'BANK',
                        'bookingCode' => $bookingCode,
                        'cardNumber' => $cardNumber,
                        'tokenResponse' => $tokenResponse,
                        'reffBiller' => $clickpayData->reffBiller
                    );
                    
                    //Save tabungan emas dan update data user
                    $this->User->saveTabunganEmas(
                        $token->id,
                        $user->domisili,
                        $user->nama,
                        $user->jenis_identitas,
                        $user->no_ktp,
                        $user->tanggal_expired_identitas,
                        $user->tempat_lahir,
                        $user->tgl_lahir,
                        $user->no_hp,
                        $user->jenis_kelamin,
                        $user->status_kawin,
                        $user->id_kelurahan,
                        $user->alamat,
                        $user->nama_ibu,
                        $user->kewarganegaraan,
                        $user->kode_cabang,
                        'K',
                        $user->foto_url,
                        $checkPayment->amount,
                        $trxId
                    );

                    //update data pembayaran
                    $this->PaymentModel->update($trxId, $paymentData);

                    $this->db->trans_complete();
                    if ($this->db->trans_status() === false) {
                        $this->dbTransError('TRANSACTION ERROR OPEN TABUNGAN EMAS MANDIRI');
                    }

                    //Set response
                    $this->set_response([
                        'status' => 'success',
                        'message' => 'Pembayaran berhasil',
                        'data' => array(
                            'reffBiller' => $clickpayData->reffBiller
                        )
                    ], 200);
                } else {
                    $this->set_response(array(
                        'status' => 'error',
                        'code' => 103,
                        'message' => 'Terjadi kesalahan jaringan mohon coba beberapa saat lagi',
                        'reason' => $clickPay
                    ));
                }
            } elseif ($payment == 'WALLET') {
                //Check user pin
                if (!$this->User->isValidPIN2($token->id, $pin)) {
                    $this->set_response(array(
                        'status' => 'error',
                        'message' => 'PIN tidak valid',
                        'code' => 102
                    ), 200);
                    return;
                }
                    
                log_message('debug', 'PAYMENT EMAS WALLET');
                
                $biayaTransaksi =  $this->ConfigModel->getRealBiayaPayment('OP', 'WALLET', '62', '');

                $openData = array(
                    'amount' => (string) ($amount + $biayaTransaksi - $discountAmount),
                    'cif' => $user->cif,
                    'flag' => 'K',
                    'ibuKandung' => $user->nama_ibu,
                    'idKelurahan' => $user->id_kelurahan,
                    'jalan' => $user->alamat,
                    'jenisKelamin' => $user->jenis_kelamin,
                    'kewarganegaraan' => $user->kewarganegaraan,
                    'kodeCabang' => $user->kode_cabang,
                    'namaNasabah' => $user->nama,
                    'noHp' => $user->no_hp,
                    'noIdentitas' => $user->no_ktp,
                    'reffSwitching' => $trxId,
                    'statusKawin' => $user->status_kawin,
                    'tanggalExpiredId' => $user->tanggal_expired_identitas,
                    'tanggalLahir' => $user->tgl_lahir,
                    'tempatLahir' => $user->tempat_lahir,
                    'tipeIdentitas' => $user->jenis_identitas,
                    'channelId' => $token->channelId,
                );

                if ($payment == 'WALLET') {
                    $openData['paymentMethod'] = 'WALLET';
                    $openData['norekWallet'] = $user->norek;
                    $openData['walletId'] = $user->noHP;
                } else {
                    $openData['gcashId'] = $va;
                    $openData['paymentMethod'] = 'GCASH';
                    $openData['productCode'] = '62';
                    $openData['jenisTransaksi'] = 'OP';
                }
                
                $openTabungan = $this->bukaTabunganEmas2($openData);

                if ($openTabungan->responseCode == '00') {
                    $resData = json_decode($openTabungan->data);

                    $noRekening = isset($resData->norek) ? $resData->norek : '';
                    $saldoEmas = isset($resData->saldoEmas) ? $resData->saldoEmas : '';
                    $saldoNominal = isset($resData->saldoNominal) ? $resData->saldoNominal : '';
                    $tglBuka = isset($resData->tglBuka) ? $resData->tglBuka : '';
                    $administrasi = isset($resData->administrasi) ? $resData->administrasi : '';
                    $gram = isset($resData->gram) ? $resData->gram : '';
                    $harga = isset($resData->harga) ? $resData->harga : '';
                    $nilaiTransaksi = isset($resData->nilaiTransaksi) ? $resData->nilaiTransaksi : '';
                    $surcharge = isset($resData->surcharge) ? $resData->surcharge : '';
                    $totalKewajiban = isset($resData->totalKewajiban) ? $resData->totalKewajiban : '';
                    $namaNasabah = isset($resData->namaNasabah) ? $resData->namaNasabah : '';

                    //Lakukan pengecekan CIF customer dan update data rekening tabungan
                    if ($payment == 'WALLET') {
                        $checkCustomer = $this->detailTabunganEmas($noRekening);
                    }
                    
                    
                    if ($checkCustomer->responseCode == '00') {
                        $checkCustomerData = json_decode($checkCustomer->data);

                        $tanggalBuka = $checkCustomerData->tglBuka;
                        $namaNasabah = $checkCustomerData->namaNasabah;
                        $noRek = $checkCustomerData->norek;
                        $saldo = $checkCustomerData->saldo;
                        $cif = $checkCustomerData->cif;
                        
                        $this->db->trans_start();

                        //Simpan tabungan emas
                        $this->User->saveTabunganEmas(
                            $token->id,
                            $user->domisili,
                            $user->nama,
                            $user->jenis_identitas,
                            $user->no_ktp,
                            $user->tanggal_expired_identitas,
                            $user->tempat_lahir,
                            $user->tgl_lahir,
                            $user->no_hp,
                            $user->jenis_kelamin,
                            $user->status_kawin,
                            $user->id_kelurahan,
                            $user->alamat,
                            $user->nama_ibu,
                            $user->kewarganegaraan,
                            $user->kode_cabang,
                            'K',
                            $user->foto_url,
                            $checkPayment->amount,
                            $trxId
                        );
                        
                        
                        $paymentData = array(
                            'payment' => $payment,
                            'is_paid' => '1',
                            'tipe' => $payment,
                            'namaNasabah' => $user->nama,
                            'norek' => $noRekening,
                            'totalKewajiban' => $totalKewajiban
                        );

                        //Simpan payment pada tahap ini, virtual account dan tgl expired masih kosong
                        $this->PaymentModel->update($trxId, $paymentData);


                        $this->User->activeTabunganEmas($trxId, $noRekening, $saldoEmas, $saldoNominal, $tglBuka, $checkCustomerData->cif);

                        //Update CIF user
                        $this->User->updateUser($token->id, array('cif' => $cif));

                        //Masukan saldo emas ke dalam transaksi (saldo dan saldo akhir jumlahnya sama)
                        $this->EmasModel->addHistoryIn($noRek, $saldo, $saldo, '1', 'OP');
                        
                        $dataTabunganEmas = $this->User->getTabunganEmas($trxId);

                        $namaOutlet = $dataTabunganEmas->namaOutlet;
                        $alamatOutlet = $dataTabunganEmas->alamatOutlet;
                        $teleponOutlet = $dataTabunganEmas->teleponOutlet;

                        $body = $this->generateOpenTabemasNotifSuccess(
                            $namaNasabah,
                            $noRek,
                            $cif,
                            $tanggalBuka,
                            $saldo,
                            $namaOutlet,
                            $alamatOutlet,
                            $teleponOutlet,
                            $trxId,
                            $totalKewajiban
                        );

                        log_message('debug', "BODY HTML", $body['mobile']);

                        log_message('debug', 'Generate Body Message OP: '.$user->email.' SUCCESS');


                        //Tambahkan ke notifikasi
                        $idNotif = $this->NotificationModel->add(
                            $token->id,
                            NotificationModel::TYPE_EMAS,
                            NotificationModel::CONTENT_TYPE_HTML,
                            "Buka Tabungan Emas",
                            "Tabungan Emas Anda Sudah Aktif",
                            $body['mobile'],
                            "OP"
                        );

                        log_message('debug', 'Save Notif Message OP: '.$user->email.' SUCCESS');
                        
                        $user2 = $this->User->getUser($token->id);

                        //Kirim notifikasi bahwa rekening tabungan sudah berhasil dibuka ke device user
                        Message::sendFCMNotif(
                            $user2->fcm_token,
                            [
                            "id" => $idNotif,
                            "tipe" => "OP_SUCCESS",
                            "namaNasabah" => $namaNasabah,
                            "noRekening" => $noRek,
                            "idTransaksi" => $trxId,
                            "saldoEmas" => $saldo,
                            "tanggal_buka" => $tanggalBuka,
                            "cif" => $cif,
                            "paymentType" => $payment,
                            "token" => $token->no_hp
                                ]
                        );

                        log_message('debug', 'SEND FCM Message OP: '.$user->email.' SUCCESS');

                        //Send email ke user memberitahukan bahwa pembukaan rekening tabungan telah berhasil
                        $this->load->helper('message');
                        Message::sendEmailOpenTabungaEmasSuccess($user->email, $body['email']);

                        log_message('debug', 'Send Email Message OP: '.$user->email.' SUCCESS');
                        
                        //Update Saldo Wallet
                        $saldoWallet = $this->updateWallet($token->channelId, $token->id, $user->no_hp);
                        $resData['wallet'] = $saldoWallet;

                        $this->db->trans_complete();
                        if ($this->db->trans_status() === false) {
                            $this->dbTransError('TRANSACTION ERROR OPEN TABUNGAN EMAS WALLET');
                        }

                        $this->set_response([
                            'status' => 'success',
                            'message' => 'Buka Tabungan Emas Sukses',
                            'data' => $resData
                                ], 200);
                    } else {
                        $this->set_response(array(
                            'status' => 'error',
                            'code' => 103,
                            'message' => 'Check Customer Error',
                            'reason' => $checkCustomer
                                ), 200);
                    }
                } else {
                    $this->set_response(array(
                        'status' => 'error',
                        'code' => 103,
                        'message' => 'Open Tabungan Error',
                        'reason' => $openTabungan
                            ), 200);
                }
            } elseif ($payment == 'GCASH') {
                //Check user pin
                if (!$this->User->isValidPIN2($token->id, $pin)) {
                    $this->set_response(array(
                        'status' => 'error',
                        'message' => 'PIN tidak valid',
                        'code' => 102
                    ), 200);
                    return;
                }
                
                log_message('debug', 'PAYMENT EMAS WALLET');
                
                $biayaTransaksiDisplay =  $this->ConfigModel->getBiayaPayment('OP', 'WALLET', '62', '');
                $biayaTransaksi =  $this->ConfigModel->getRealBiayaPayment('OP', 'WALLET', '62', '');
                
                $openData = array(
                    'amount' => (string) ($amount + $biayaTransaksi - $discountAmount),
                    'cif' => $user->cif,
                    'flag' => 'K',
                    'ibuKandung' => $user->nama_ibu,
                    'idKelurahan' => $user->id_kelurahan,
                    'jalan' => $user->alamat,
                    'jenisKelamin' => $user->jenis_kelamin,
                    'kewarganegaraan' => $user->kewarganegaraan,
                    'kodeCabang' => $user->kode_cabang,
                    'namaNasabah' => $user->nama,
                    'noHp' => $user->no_hp,
                    'noIdentitas' => $user->no_ktp,
                    'reffSwitching' => $trxId,
                    'statusKawin' => $user->status_kawin,
                    'tanggalExpiredId' => $user->tanggal_expired_identitas,
                    'tanggalLahir' => $user->tgl_lahir,
                    'tempatLahir' => $user->tempat_lahir,
                    'tipeIdentitas' => $user->jenis_identitas,
                    'channelId' => $token->channelId,
                );

                $openData['gcashId'] = $va;
                $openData['paymentMethod'] = 'GCASH';
                $openData['productCode'] = '62';
                $openData['jenisTransaksi'] = 'OP';
                
                
                $openTabungan = $this->bukaTabunganEmas2($openData);
                
                if ($openTabungan->responseCode == '00') {
                    $resData = json_decode($openTabungan->data);

                    $noRekening = isset($resData->norek) ? $resData->norek : '';
                    $saldoEmas = isset($resData->saldoEmas) ? $resData->saldoEmas : '';
                    $saldoNominal = isset($resData->saldoNominal) ? $resData->saldoNominal : '';
                    $tglBuka = isset($resData->tglBuka) ? $resData->tglBuka : '';
                    $administrasi = isset($resData->administrasi) ? $resData->administrasi : '';
                    $gram = isset($resData->gram) ? $resData->gram : '';
                    $harga = isset($resData->harga) ? $resData->harga : '';
                    $nilaiTransaksi = isset($resData->nilaiTransaksi) ? $resData->nilaiTransaksi : '';
                    $surcharge = isset($resData->surcharge) ? $resData->surcharge : '';
                    $totalKewajiban = isset($resData->totalKewajiban) ? $resData->totalKewajiban : '';
                    $namaNasabah = isset($resData->namaNasabah) ? $resData->namaNasabah : '';

                    $this->db->trans_start();

                    //Simpan tabungan emas
                    $this->User->saveTabunganEmas(
                        $token->id,
                        $user->domisili,
                        $user->nama,
                        $user->jenis_identitas,
                        $user->no_ktp,
                        $user->tanggal_expired_identitas,
                        $user->tempat_lahir,
                        $user->tgl_lahir,
                        $user->no_hp,
                        $user->jenis_kelamin,
                        $user->status_kawin,
                        $user->id_kelurahan,
                        $user->alamat,
                        $user->nama_ibu,
                        $user->kewarganegaraan,
                        $user->kode_cabang,
                        'K',
                        $user->foto_url,
                        $checkPayment->amount,
                        $trxId
                    );
  
                    
                    
                    $paymentData = array(
                        'payment' => $payment,
                        'gcashIdTujuan' => $va,
                        'gcashId' => $va
                    );

                    // Get GCASH Detail
                    $vaDetails = $this->GcashModel->getVaDetailsByVa($va);

                    //Simpan payment pada tahap ini, virtual account dan tgl expired masih kosong
                    $this->PaymentModel->update($trxId, $paymentData);
                    
                    $template = $this->generateOpenTabemasNotif($user->nama, $payment, $amount + $biayaTransaksiDisplay - $discountAmount, null, null, $trxId, $vaDetails->namaBank, $promoCode, $promoAmount);
                    
                    $emailTemplate= $template['email'];
                    $mobileTemplate = $template['mobile'];
                    $minimalTemplate = $template['minimal'];
                    
                    //Simpan notifikasi baru
                    $idNotif = $this->NotificationModel->add(
                        $token->id,
                        NotificationModel::TYPE_EMAS,
                        NotificationModel::CONTENT_TYPE_HTML,
                        $this->ConfigModel->getNamaProduk('OP', '62'),
                        "Terima kasih telah melakukan pengajuan Pembukaan Rekening Tabungan Emas Pegadaian",
                        $mobileTemplate,
                        $minimalTemplate,
                        "OP"
                    );

                    //Kirim notifikasi pembayaran ke device user
                    Message::sendFCMNotif(
                        $this->User->getFCMToken($token->id),
                        [
                        "id" => $idNotif,
                        "tipe" => "OP",
                        "title" => $this->ConfigModel->getNamaProduk('OP', '62'),
                        "tagline" => "Terima kasih telah melakukan pengajuan Pembukaan Rekening Tabungan Emas Pegadaian",
                        "content" => "Terima kasih telah melakukan pengajuan Pembukaan Rekening Tabungan Emas Pegadaian",
                        "paymentType" => $payment,
                        "token" => $token->no_hp
                        ]
                    );

                    // Kirim Email Notifikasi
                    $this->load->helper('message');
                    Message::sendEmailOpenTabunganPayment($token->email, $emailTemplate);


                    $this->db->trans_complete();
                    if ($this->db->trans_status() === false) {
                        $this->dbTransError('TRANSACTION ERROR OPEN TABUNGAN EMAS WALLET');
                    }

                    $this->set_response([
                        'status' => 'success',
                        'message' => 'Buka Tabungan Emas Sukses',
                        'data' => $resData
                            ], 200);
                } else {
                    $this->set_response(array(
                        'status' => 'error',
                        'code' => 103,
                        'message' => 'Open Tabungan Error',
                        'reason' => $openTabungan
                            ), 200);
                }
            } elseif ($payment == 'VA_BCA' || $payment == 'VA_MANDIRI' || $payment == 'VA_BRI') {
                $kodeBank = '';
                if ($payment == 'VA_BCA') {
                    $kodeBank = '014';
                } elseif ($payment=='VA_MANDIRI') {
                    $kodeBank = '008';
                } elseif ($payment=='VA_BRI') {
                    $kodeBank = '002';
                }
                
                $biayaTransaksi =  $this->ConfigModel->getRealBiayaPayment('OP', 'BANK', '62', $kodeBank);
                $biayaTransaksiDisplay =  $this->ConfigModel->getBiayaPayment('OP', 'BANK', '62', $kodeBank);

                $createBillingData = [
                    'channelId' => $token->channelId,
                    'amount' => (string) ($amount + $biayaTransaksi - $discountAmount),
                    'customerEmail' => $user->email,
                    'customerName' => $user->nama,
                    'customerPhone' => $user->no_hp,
                    'flag' => 'K',
                    'jenisTransaksi' => 'OP',
                    'kodeProduk' => '62',
                    'keterangan' => $keterangan,
                    'reffSwitching' => $trxId,
                    'kodeBank' => $kodeBank
                ];
                $billing = $this->createBillingPegadaian($createBillingData);

                if ($billing->responseCode == '00') {
                    if (!isset($billing->data)) {
                        $this->set_response(array(
                            'status' =>'error',
                            'message' => 'Terjadi kesalahan jaringan, mohon coba beberapa saat lagi',
                            'code' => 103
                        ), 200);
                        return;
                    }
                    
                    $billingData = json_decode($billing->data);

                    $virtualAccount = $billingData->vaNumber;
                    $tglExpired = $billingData->tglExpired;

                    if ($payment == 'VA_BCA') {
                        $dataPrefix = $this->MasterModel->getPrefixBank('prefixBCA');
                        $virtualAccount = $dataPrefix . $billingData->vaNumber;
                        $tglExpired = $billingData->tglExpired;
                    }

                    $this->db->trans_start();

                    //Save tabungan emas dan update data user
                    $this->User->saveTabunganEmas(
                        $token->id,
                        $user->domisili,
                        $user->nama,
                        $user->jenis_identitas,
                        $user->no_ktp,
                        $user->tanggal_expired_identitas,
                        $user->tempat_lahir,
                        $user->tgl_lahir,
                        $user->no_hp,
                        $user->jenis_kelamin,
                        $user->status_kawin,
                        $user->id_kelurahan,
                        $user->alamat,
                        $user->nama_ibu,
                        $user->kewarganegaraan,
                        $user->kode_cabang,
                        'K',
                        $user->foto_url,
                        $checkPayment->amount,
                        $trxId
                    );
                    
                    $paymentData = array(
                        'payment' => $payment,
                        'kodeBankPembayar' => $kodeBank,
                        'virtual_account' =>  $virtualAccount
                    );

                    //Simpan payment pada tahap ini, virtual account dan tgl expired masih kosong
                    $this->PaymentModel->update($trxId, $paymentData);

                    $this->db->trans_complete();
                    if ($this->db->trans_status() === false) {
                        $this->dbTransError('TRANSACTION ERROR OPEN TABUNGAN EMAS BNI ');
                    }

                        
                    $template = $this->generateOpenTabemasNotif($user->nama, $payment, $amount + $biayaTransaksiDisplay - $discountAmount, $virtualAccount, $tglExpired, $trxId, $promoCode, $promoAmount);
                    
                    $emailTemplate= $template['email'];
                    $mobileTemplate = $template['mobile'];
                    $minimalTemplate = $template['minimal'];
                    
                    //Simpan notifikasi baru
                    $idNotif = $this->NotificationModel->add(
                        $token->id,
                        NotificationModel::TYPE_EMAS,
                        NotificationModel::CONTENT_TYPE_HTML,
                        $this->ConfigModel->getNamaProduk('OP', '62'),
                        "Segera Bayar Rp". number_format($amount + $biayaTransaksiDisplay, 0, ",", ".")." ke ".$virtualAccount,
                        $mobileTemplate,
                        $minimalTemplate,
                        "OP"
                    );

                    //Kirim notifikasi pembayaran ke device user
                    Message::sendFCMNotif(
                        $this->User->getFCMToken($token->id),
                        [
                        "id" => $idNotif,
                        "tipe" => "OP",
                        "title" => $this->ConfigModel->getNamaProduk('OP', '62'),
                        "tagline" => "Segera Bayar Rp". number_format($amount + $biayaTransaksiDisplay, 0, ",", ".")." ke ".$virtualAccount,
                        "content" => "Bayar ke " . $virtualAccount . " sebelum " . $tglExpired,
                        "paymentType" => $payment,
                        "token" => $token->no_hp
                        ]
                    );

                    //Kirim Email Notifikasi
                    $this->load->helper('message');
                    Message::sendEmailOpenTabunganPayment($token->email, $emailTemplate);


                    //Set response
                    $this->set_response([
                        'status' => 'success',
                        'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
                        'data' => array(
                            'idTransaksi' => $trxId,
                            'virtualAccount' => $virtualAccount,
                            'expired' => $tglExpired,
                            'now' => date('Y-m-d H:i:s')
                        )
                    ], 200);
                } else {
                    $this->set_response([
                        'status' => 'error',
                        'message' => 'Something went wrong. Please try again later.',
                        'code' => 103,
                        'reason' => $billing
                    ], 200);
                }
            } elseif ($payment == 'VA_PERMATA') {
                $kodeBank = '013';
                $biayaTransaksi =  $this->ConfigModel->getRealBiayaPayment('OP', 'BANK', '62', $kodeBank);
                $biayaTransaksiDisplay =  $this->ConfigModel->getBiayaPayment('OP', 'BANK', '62', $kodeBank);

                $createBillingData = [
                    'channelId' => $token->channelId,
                    'amount' => (string) ($amount + $biayaTransaksi - $discountAmount),
                    'customerEmail' => $user->email,
                    'customerName' => $user->nama,
                    'customerPhone' => $user->no_hp,
                    'flag' => 'K',
                    'jenisTransaksi' => 'OP',
                    'productCode' => '62',
                    'norek' => '',
                    'keterangan' => $keterangan,
                    'trxId' => $trxId,
                ];
                $billing = $this->createBillingPermata($createBillingData);

                if ($billing->responseCode == '00') {
                    if (!isset($billing->data)) {
                        $this->set_response(array(
                            'status' =>'error',
                            'message' => 'Terjadi kesalahan jaringan, mohon coba beberapa saat lagi',
                            'code' => 103
                        ), 200);
                        return;
                    }
                    
                    $billingData = json_decode($billing->data);
                    
                    
                    $virtualAccount = $billingData->virtualAccount;
                    $tglExpired = $billingData->tglExpired;

                    $this->db->trans_start();

                    //Save tabungan emas dan update data user
                    $this->User->saveTabunganEmas(
                        $token->id,
                        $user->domisili,
                        $user->nama,
                        $user->jenis_identitas,
                        $user->no_ktp,
                        $user->tanggal_expired_identitas,
                        $user->tempat_lahir,
                        $user->tgl_lahir,
                        $user->no_hp,
                        $user->jenis_kelamin,
                        $user->status_kawin,
                        $user->id_kelurahan,
                        $user->alamat,
                        $user->nama_ibu,
                        $user->kewarganegaraan,
                        $user->kode_cabang,
                        'K',
                        $user->foto_url,
                        $checkPayment->amount,
                        $trxId
                    );
                    
                    $paymentData = array(
                        'payment' => $payment,
                        'kodeBankPembayar' => $kodeBank,
                        'payment' => 'BANK',
                        'virtual_account' => $virtualAccount
                    );

                    //Simpan payment pada tahap ini, virtual account dan tgl expired masih kosong
                    $this->PaymentModel->update($trxId, $paymentData);

                    $this->db->trans_complete();
                    if ($this->db->trans_status() === false) {
                        $this->dbTransError('TRANSACTION ERROR OPEN TABUNGAN EMAS BNI ');
                    }

                        
                    $template = $this->generateOpenTabemasNotif($user->nama, $payment, $amount + $biayaTransaksiDisplay - $discountAmount, $virtualAccount, $tglExpired, $trxId, $promoCode, $promoAmount);
                    
                    $emailTemplate= $template['email'];
                    $mobileTemplate = $template['mobile'];
                    $minimalTemplate = $template['minimal'];
                    
                    //Simpan notifikasi baru
                    $idNotif = $this->NotificationModel->add(
                        $token->id,
                        NotificationModel::TYPE_EMAS,
                        NotificationModel::CONTENT_TYPE_HTML,
                        $this->ConfigModel->getNamaProduk('OP', '62'),
                        "Segera Bayar Rp". number_format($amount + $biayaTransaksiDisplay - $discountAmount, 0, ",", ".")." ke ".$virtualAccount,
                        $mobileTemplate,
                        $minimalTemplate,
                        "OP"
                    );

                    //Kirim notifikasi pembayaran ke device user
                    Message::sendFCMNotif(
                        $this->User->getFCMToken($token->id),
                        [
                        "id" => $idNotif,
                        "tipe" => "OP",
                        "title" => $this->ConfigModel->getNamaProduk('OP', '62'),
                        "tagline" => "Segera Bayar Rp". number_format($amount + $biayaTransaksiDisplay - $discountAmount, 0, ",", ".")." ke ".$virtualAccount,
                        "content" => "Bayar ke " . $virtualAccount . " sebelum " . $tglExpired,
                        "paymentType" => $payment,
                        "token" => $token->no_hp
                        ]
                    );

                    //Kirim Email Notifikasi
                    $this->load->helper('message');
                    Message::sendEmailOpenTabunganPayment($token->email, $emailTemplate);


                    //Set response
                    $this->set_response([
                        'status' => 'success',
                        'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
                        'data' => array(
                            'idTransaksi' => $trxId,
                            'virtualAccount' => $virtualAccount,
                            'expired' => $tglExpired,
                            'now' => date('Y-m-d H:i:s')
                        )
                    ], 200);
                } else {
                    $this->set_response([
                        'status' => 'error',
                        'message' => 'Something went wrong. Please try again later.',
                        'code' => 103,
                        'reason' => $billing
                    ], 200);
                }
            }
        }
    }


    /**
     * Method untuk menggenerate HTML Konten notifikasi sukses buka tabungan emas
     * @param type $nama
     * @param type $noRekening
     * @param type $cif
     * @param type $tanggalBuka
     * @param type $saldo
     * @param type $namaOutlet
     * @param type $alamatOutlet
     * @param type $teleponOutlet
     * @return String $message HTML Content yang sudah tergenerate
     */
    function generateOpenTabemasNotifSuccess($nama, $noRekening, $cif, $tanggalBuka, $saldo, $namaOutlet, $alamatOutlet, $teleponOutlet, $trxId, $totalKewajiban)
    {
        $subject = "Selamat Rekening Tabungan Emas Anda Sudah Aktif";
        
        //Batas waktu KYC = tanggalBuka + 6 bulan
        $batasWaktu = new DateTime($tanggalBuka);
        $interval = new DateInterval('P6M');
        $batasWaktu->add($interval);
        
        //Format tanggal buka
        $fTanggalBuka = new DateTime($tanggalBuka);
        
        
        $viewData = array(
            'nama' => $nama,
            'noRekening' => $noRekening,
            'cif' => $cif,
            'tanggalBuka' => $fTanggalBuka->format('d/m/Y'),
            'saldo' => $saldo,
            'namaOutlet' =>$namaOutlet,
            'alamatOutlet' => $alamatOutlet,
            'teleponOutlet' => $teleponOutlet,
            'batasWaktu' => $batasWaktu->format('d/m/Y'),
            'trxId' => $trxId
        );

        $bottom = $this->notification_service->getCoupon('OP', $totalKewajiban, $cif ?? null);

        $message = $this->load->view('mail/email_template_top', array('title'=>$subject), true);
        $message = $message.$this->load->view('mail/notif_opentabemas_success', $viewData, true);
        $message = $message.$this->load->view('mail/email_template_bottom', $bottom, true);
        
        $mobile = $this->load->view('notification/top_template', array('title'=>$subject), true);
        $mobile = $mobile.$this->load->view('mail/notif_opentabemas_success', $viewData, true);
        $mobile = $mobile.$this->load->view('notification/bottom_template', $bottom, true);
        
        return array(
            'email' => $message,
            'mobile' => $mobile
        );
    }
    
    /**
     * Method untuk menggenerate content HTML notif buka tabungan emas
     * @param String $payment
     * @param String $jumlah
     * @param String $nama
     * @param String $rekeningEmas
     * @param String $gram
     * @param String $noRekening
     * @param String $tanggalExpired
     * @return String generated content
     */
    function generateOpenTabemasNotif($nama, $payment, $amount, $va, $tglExpired, $trxId, $promoCode = '', $promoAmount = 0, $bankName = null)
    {
        $subject = "Pengajuan Pembukaan Rekening Tabungan Emas Pegadaian";
                
        //Convert tanggal expired ke dd-mm-yyy H:i:s
        $date = new DateTime($tglExpired);
        
        $viewData = array(
            
            'nama' => $nama,
            'payment' => $payment,
            'va' => $va,
            'amount' => $amount,
            'tglExpired' => $date->format('d/m/Y H:i:s'),
            'trxId' => $trxId,
            'bankName' => $bankName,
            'promoCode' => $promoCode,
            'promoAmount' => $promoAmount
        );
        
        $content = $this->load->view('mail/notif_opentabemas', $viewData, true);
        
        $message = $this->load->view('mail/email_template_top', array('title'=>$subject), true);
        $message = $message.$content;
        $message = $message.$this->load->view('mail/email_template_bottom', array(), true);

        $mobile = $this->load->view('notification/top_template', array('title'=>$subject), true);
        $mobile = $mobile.$content;
        $mobile = $mobile.$this->load->view('notification/bottom_template', array(), true);
        
        return array(
            'email' => $message,
            'mobile' => $mobile,
            'minimal' => $content
        );
    }
    
    
    /**
     * Method untuk menggenerate content html notif beli emas
     * @param String $payment
     * @param String $jumlah
     * @param String $nama
     * @param String $rekeningEmas
     * @param String $gram
     * @param String $noRekening
     * @param String $tanggalExpired
     * @param String $trxId
     * @return String generated html content
     */
    function generateBeliEmasNotif($trxId, $cif)
    {
        $checkPayment = $this->PaymentModel->getPaymentByTrxId2($trxId, true);
        
        // Get promo (if Any)
        $gpoin = $this->GpoinModel->get_promo($trxId);
        $promoCode = '';
        $idPromosi = '';
        $discountAmount = 0;
        $promoAmount = 0;
        if ($gpoin != '') {
            if ($gpoin->type == 'discount') {
                $discountAmount = $gpoin->value;
            }
            $promoCode = $gpoin->promoCode;
            $promoAmount = $gpoin->value;
            $idPromosi = $gpoin->idPromosi;
        };
        $checkPayment['promoCode'] = $promoCode;
        $checkPayment['promoAmount'] = $promoAmount;
        $checkPayment['discountAmount'] = $discountAmount;

        $user = $this->User->getUser($checkPayment['user_AIID']);
        $checkPayment['namaNasabahUser'] = $user->nama;
    
        log_message('debug', 'GENERATENOTIFBELIEMAS1: ', $checkPayment);
        
        $checkPayment['cif'] = $cif;
        $subject = "";
        
        if ($checkPayment['payment'] == 'BNI' ||
                $checkPayment['payment'] == 'VA_MANDIRI' ||
                $checkPayment['payment'] == 'VA_BCA' ||
                $checkPayment['payment'] == 'VA_BRI' ||
                $checkPayment['payment'] == 'VA_PERMATA'
            ) {
            $subject = "Konfirmasi Pembelian Tabungan Emas Pegadaian ".$checkPayment['norek'];
        } elseif ($checkPayment['payment'] == 'WALLET' || $checkPayment['payment'] == 'GCASH') {
            $subject = "Selamat Top Up Tabungan Emas Berhasil";
        }
        
        log_message('debug', 'GENERATENOTIFBELIEMAS: ', $checkPayment);
                
        $content = $this->load->view('mail/notif_beliemas', $checkPayment, true);
        
        $email = $this->load->view('mail/email_template_top', array('title'=>$subject), true);
        $email = $email.$content;
        $email = $email.$this->load->view('mail/email_template_bottom', array(), true);
        
        $mobile = $this->load->view('notification/top_template', array('title'=>$subject), true);
        $mobile = $mobile.$content;
        $mobile = $mobile.$this->load->view('notification/bottom_template', array(), true);
        
        
        return array(
            'email' => $email,
            'mobile' => $mobile,
            'minimal' => $content
        );
    }
  
    /**
     * Endpoint alternatif untuk beli emas.
     * Proses inquiry tabungan dan VA dipisah
     */
    function beli_v2_post()
    {
        $token = $this->getToken();
        if ($token) {
            $user = $this->User->getUser($token->id);

            $setData = array(
                'no_rekening' => $this->post('no_rekening'),
                'jumlah' => $this->post('jumlah'),
                'promo_code' => $this->post('promo_code'),

            );

            $this->form_validation->set_data($setData);

            $this->form_validation->set_rules('no_rekening', 'No Rekening', 'required|exact_length[16]');
            $this->form_validation->set_rules('jumlah', 'Jumlah', 'required|integer');

            if ($this->form_validation->run() == false) {
                $this->set_response(array(
                    'status' => 'error',
                    'message' => 'Invalid Input',
                    'code' => 201,
                    'errors' => $this->form_validation->error_array()
                ), 200);
            } else {
                $no_rekening = $this->post('no_rekening');
                $jumlah = $this->post('jumlah');
                $promoCode = $this->post('promo_code');

                //Dapatkan detail user dan rekening berdasarkan nomo rekening
                $rekeningTabungan = $this->User->getUserByNoRek($no_rekening);
                //Lakukan inquiry tabungan emas
                
                $inquiryTabunganEmasData = [
                    'channelId' => $this->config->item('core_client_id'),
                    'clientId' => $this->config->item('core_post_username'),
                    'jenisTransaksi' => 'SL',
                    'flag' => 'K',
                    'amount' => $jumlah,
                    'norek' => $no_rekening,
                    'kodeCabang' => $user->kode_cabang,
                    'cif' => $user->cif,
                ];

                // Lakukan validasi promo/voucher gpoint jika ada
                if ($promoCode != null) {
                    if ($user->cif == null) {
                        return $this->set_response([
                            'status' => 'error',
                            'message' => 'Anda harus memiliki CIF untuk menggunakan fitur voucher',
                            'code' => 102
                        ]);
                    }

                    $validateData = [
                        'promoCode' => $promoCode,
                        'voucherId' => null,
                        'userId' => $user->cif,
                        'transactionAmount' => (int) $jumlah,
                        'validators' => [
                            'channel' => $this->config->item('core_post_username'),
                            'product' => '62',
                            'transactionType' => 'SL',
                            'unit' => 'rupiah',
                        ]
                    ];

                    $validateVoucher = $this->gPointValidateVoucher($validateData);

                    if ($validateVoucher->status == 'Error') {
                        return $this->set_response([
                            'status' => 'error',
                            'message' => $validateVoucher->message,
                            'code' => 103,
                        ]);
                    } elseif ($validateVoucher->status == 'Success') {
                        $inquiryTabunganEmasData['discountAmount'] = $validateVoucher->data->discount;
                        $inquiryTabunganEmasData['idPromosi'] = $validateVoucher->data->journalAccount.';'.$promoCode;
                    }
                }

                $inquiryTabungan = $this->inquiryTabunganEmas($inquiryTabunganEmasData);
                if ($inquiryTabungan->responseCode == '00') {
                    //Fallback jika tidak bisa connect ke core
                    if (!isset($inquiryTabungan->data)) {
                        $this->set_response(array(
                            'status' => 'error',
                            'message'=> 'Kesalahan jaringan mohon coba beberapa saat lagi',
                            'code' => 103,
                            'reason' => $inquiryTabungan
                        ), 200);
                        return;
                    }

                    $inquiryData = json_decode($inquiryTabungan->data);

                    $trxId = $inquiryData->reffSwitching;
                    $gram = $inquiryData->gram;
                    $administrasi = $inquiryData->administrasi;
                    $satuan = $inquiryData->satuan;
                    $harga = $inquiryData->harga;
                    $totalKewajiban = $inquiryData->totalKewajiban;
                    $biaya = $inquiryData->surcharge;
                    $namaNasabah = $inquiryData->namaNasabah;
                    $hargaEmas = $inquiryData->harga;
                    $nilaiTransaksi = $inquiryData->nilaiTransaksi;
                    $hakNasabah = $inquiryData->hakNasabah;
                    $surcharge = $inquiryData->surcharge;
                    $reffCore = $inquiryData->reffCore;
                    $saldoEmas = $inquiryData->saldoEmas;
                    $norek = $inquiryData->norek;
                    $saldoNominal = $inquiryData->saldoNominal;
                    $biayaTitip = $inquiryData->biayaTitip;
                    $namaProduk = $inquiryData->namaProduk;
                    $tglTransaksi = $inquiryData->tglTransaksi;
                    $paymentData = array(
                        'id_transaksi' => $trxId,
                        'jenis_transaksi' => 'SL',
                        'amount' => $jumlah,
                        'gram' => $gram,
                        'administrasi' => $administrasi,
                        'kode_produk' => '62',
                        'harga' => $harga,
                        'satuan' => $satuan,
                        'saldoEmas' => $saldoEmas,
                        'total_kewajiban' => $totalKewajiban,
                        'reffCore' => $inquiryData->reffCore,
                        'user_AIID' => $token->id,
                        'keterangan' => 'Pembelian Emas',
                        'no_rekening' => $norek,
                        'surcharge' => $surcharge,
                        'reffCore' => $reffCore,
                        'nilaiTransaksi' => $nilaiTransaksi,
                        'hakNasabah' => $hakNasabah,
                        'saldoNominal' => $saldoNominal,
                        'biayaTitip' => $biayaTitip,
                        'namaProduk' => $namaProduk,
                        'tglTransaksi' => $tglTransaksi,
                        'totalKewajiban' => $totalKewajiban,
                        'namaNasabah' => $namaNasabah,
                        'jenisTransaksi' => 'SL',
                        'norek' => $no_rekening,
                        'reffSwitching' => $trxId
                        
                    );

                    if (isset($inquiryTabunganEmasData['discountAmount'], $inquiryTabunganEmasData['idPromosi'])) {
                        $paymentData['discountAmount'] = $inquiryTabunganEmasData['discountAmount'];
                        $paymentData['idPromosi'] = $inquiryTabunganEmasData['idPromosi'];
                    }
                    
                    //Simpan payment pada tahap ini, virtual account dan tgl expired masih kosong
                    $this->PaymentModel->add($paymentData);
                    
                    //Get biaya channel untuk masing-masing metode pembayaran
                    $biayaChannel = $this->Payment_service->getBiayaPayment($paymentData['jenisTransaksi'], $paymentData['kode_produk']);
                    
                    $this->set_response(array(
                        'status' => 'success',
                        'message' => 'Inquiry success',
                        'data' => array(
                            'idTransaksi' => $trxId,
                            'amount' => $jumlah,
                            'gram' => $gram,
                            'administrasi' => $administrasi,
                            'biaya' => $biaya,
                            'satuan' => $satuan,
                            'totalKewajiban' => $totalKewajiban,
                            'noRekening' => $no_rekening,
                            'namaNasabah' => $namaNasabah,
                            'hargaEmas' => $hargaEmas,
                            'biayaChannel' => $biayaChannel
                        )
                    ), 200);
                } else {
                    $this->set_response(array(
                        'status' => 'error',
                        'message' => 'Inquiry Tabungan Error',
                        'code' => 103,
                        'reason' => $inquiryTabungan
                    ), 200);
                }
            }
        } else {
            $this->errorUnAuthorized();
        }
    }
    
    function payment_post()
    {
        $token = $this->getToken();
        if ($token) {
            $user = $this->User->getUser($token->id);
           
            $setData = array(
               'id_transaksi' => $this->post('id_transaksi'),
               'payment' => $this->post('payment'),
               'booking_code' => $this->post('booking_code'),
               'card_number' => $this->post('card_number'),
               'token_response' => $this->post('token_response'),
               'pin' => $this->post('pin'),
               'va' => $this->post('va')
            );
           
            $this->form_validation->set_data($setData);
           
            $this->form_validation->set_rules('id_transaksi', 'id_transaksi', 'required');
            $this->form_validation->set_rules('payment', 'payment', 'required');
           
            $payment = $this->post('payment');
           
            if ($payment == 'MANDIRI') {
                $this->form_validation->set_rules('booking_code', 'booking_code', 'required');
                $this->form_validation->set_rules('card_number', 'card_number', 'required');
                $this->form_validation->set_rules('token_response', 'token_response', 'required');
            }
            
            if ($payment == 'WALLET' || $payment == 'GCASH') {
                $this->form_validation->set_rules('pin', 'pin', 'required|exact_length[6]|numeric');
            }
            
            //Jika payment adalah wallet dan user belum akttifasi wallet
            if ($payment == 'WALLET' && $user->norek == '') {
                $this->set_response(array(
                   'status' => 'error',
                   'message' => 'Anda belum melakukan aktifasi wallet',
                   'code' => 101
                ), 200);
                return;
            }

            if ($payment == 'GCASH') {
                $this->form_validation->set_rules('va', 'va', 'required|numeric');
            }
           
            if ($this->form_validation->run() == false) {
                $this->set_response(array(
                   'status' => 'error',
                   'message' => 'Invalid input',
                   'code' => 101,
                   'errors' => $this->form_validation->error_array()
                ), 200);
            } else {
                $idTransaksi = $this->post('id_transaksi');
               
               
                //Mendapatkan payment berdasarkan id transaksi
                $checkPayment = $this->PaymentModel->getPaymentByTrxId($idTransaksi);
               
                if ($checkPayment) {
                    $checkCustomer = $this->detailTabunganEmas($checkPayment->no_rekening, $token->channelId);
                    
                    if ($checkCustomer->responseCode == '00') {
                        if (!isset($checkCustomer->data)) {
                            $this->set_response(array(
                                'status' => 'error',
                                'message' => 'Kesalahan jaringan mohon coba beberapa saat lagi',
                                'code' => 103,
                                'reason' => $checkCustomer
                                    ), 200);
                            return;
                        }
                                
                        $customerData = json_decode($checkCustomer->data);

                        // Mendapatkan promo berdasarkan id transaksi
                        $gpoin = $this->GpoinModel->get_promo($idTransaksi);
                        $idPromosi = '';
                        $discountAmount = 0;
                        $promoAmount = 0;
                        if ($gpoin != '') {
                            if ($gpoin->type == 'discount') {
                                $discountAmount = $gpoin->value;
                            }
                            $promoAmount = $gpoin->value;
                            $idPromosi = $gpoin->idPromosi;
                        };
                        // $discountAmount = $checkPayment->discountAmount;
                        // $idPromosi = $checkPayment->idPromosi;

                        $checkPayment->data_customer = $customerData;
                        $checkPayment->idTransaksi = $idTransaksi;
                        $checkPayment->discountAmount = $discountAmount;
                        $checkPayment->data_user = $user;
                        $checkPayment->data_token = $token;
                        
                        if ($payment == 'VA_MAYBANK') {
                            $response = $this->Emas_service->paymentTopup($payment, $checkPayment, $customerData, $idTransaksi, $discountAmount, $user, $token, '016');
                            return $this->send_response($response);
                        }

                        if ($payment == 'FINPAY') {
                            $checkPayment->kode_bank = '770';
                            $response = $this->Emas_service->payment_finpay_topup($payment, $checkPayment);
                            return $this->send_response($response);
                        }
                        
                        if ($payment == 'BNI') {
                            $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment('SL', 'BANK', '62', '009');
                            $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment('SL', 'BANK', '62', '009');
                            $createBillingBNI = $this->createBillingVABNI(
                                $checkPayment->total_kewajiban + $biayaTransaksi - $discountAmount,
                                $user->email,
                                $token->nama,
                                $token->no_hp,
                                'K',
                                'SL',
                                "PDSSL " . $idTransaksi . " " . $checkPayment->no_rekening,
                                $checkPayment->no_rekening,
                                '62',
                                $idTransaksi,
                                $token->channelId,
                                $idPromosi,
                                $promoAmount
                            );

                            if ($createBillingBNI->responseCode == '00') {
                                log_message('debug', 'RESPONSE CREATE BILLING BNI', $createBillingBNI);
                                if (!isset($createBillingBNI->data)) {
                                    $this->set_response(array(
                                        'status' => 'error',
                                        'message' => 'Kesalahan jaringan mohon coba beberapa saat lagi',
                                        'code' => 103,
                                        'reason' => $createBillingBNI
                                            ), 200);
                                    return;
                                }
                                $billingData = json_decode($createBillingBNI->data);


                                $virtualAccount = $billingData->virtualAccount;
                                $tglExpired = $billingData->tglExpired;

                                $updatePaymentData = array(
                                    'tipe' => $payment,
                                    'virtual_account' => $virtualAccount,
                                    'tanggal_expired' => $tglExpired,
                                    'payment' => $payment,
                                    'biayaTransaksi' => $biayaTransaksiDisplay
                                );

                                //Buat payment baru
                                //Simpan payment
                                $this->PaymentModel->update($idTransaksi, $updatePaymentData);

                                $lblNotifNama = isset($checkPayment->namaNasabah) ? $checkPayment->namaNasabah : $token->nama;
                                $cif = $this->User->getUserCif($token->id);

                                $template = $this->generateBeliEmasNotif($idTransaksi, $customerData->cif);

                                $mobileTemplate = $template['mobile'];
                                $emailTemplate = $template['email'];
                                $minimalTemplate = $template['minimal'];

                                //Simpan notifikasi baru
                                $notifId = $this->NotificationModel->add(
                                    $token->id,
                                    NotificationModel::TYPE_EMAS,
                                    NotificationModel::CONTENT_TYPE_HTML,
                                    $this->ConfigModel->getNamaProduk('SL', '62'),
                                    "Segera bayar Rp. " . number_format($checkPayment->total_kewajiban + $biayaTransaksiDisplay - $discountAmount, 0, ",", ".") . ' ke ' . $virtualAccount,
                                    $mobileTemplate,
                                    $minimalTemplate,
                                    "SL"
                                );

                                //Kirim notifikasi pembayaran ke device user
                                Message::sendFCMNotif(
                                    $this->User->getFCMToken($token->id),
                                    [
                                    "id" => $notifId,
                                    "tipe" => "SL",
                                    "title" => $this->ConfigModel->getNamaProduk('SL', '62'),
                                    "tagline" => "Segera bayar Rp. " . number_format($checkPayment->total_kewajiban + $biayaTransaksiDisplay - $discountAmount, 0, ",", ".") . ' ke ' . $virtualAccount,
                                    "content" => "Bayar ke " . $virtualAccount . " sebelum " . $tglExpired,
                                    "token" => $token->no_hp
                                        ]
                                );

                                //Kirim Email Notifikasi
                                $this->load->helper('message');
                                Message::sendEmailBeliEmas(
                                    $token->email,
                                    $checkPayment->no_rekening,
                                    $emailTemplate
                                );

                                //Mendapatkan detail tabungan emas user
                                $saldo = $this->getSaldo($checkPayment->no_rekening);

                                //Set response
                                $this->set_response([
                                    'status' => 'success',
                                    'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
                                    'data' => array(
                                        'idTransaksi' => $idTransaksi,
                                        'virtualAccount' => $virtualAccount,
                                        'expired' => $tglExpired,
                                        'now' => date('Y-m-d H:i:s'),
                                        'saldo' => $saldo['saldo'],
                                        'saldoAkhir' => $saldo['saldoAkhir'],
                                        'saldoBlokir' => $saldo['saldoBlokir'],
                                        'saldoEfektif' => $saldo['saldoEfektif']
                                    )
                                        ], 200);
                            } else {
                                log_message('debug', 'Create VA error. Reason:', $createBillingBNI);
                                $this->set_response(array(
                                    'status' => 'error',
                                    'message' => 'Mohon coba kembali beberapa saat lagi',
                                    'code' => 103,
                                    'reason' => $createBillingBNI
                                        ), 200);
                            }
                        } elseif ($payment == 'MANDIRI') {
                            $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment('SL', 'BANK', '62', '008');
                            $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment('SL', 'BANK', '62', '008');

                            $bookingCode = $this->post('booking_code');
                            $cardNumber = $this->post('card_number');
                            $tokenResponse = $this->post('token_response');
                            
                            //Bayar dengan mandiri click pay
                            $clickPay = $this->mandiriClickPay(
                                $checkPayment->total_kewajiban + $biayaTransaksi - $discountAmount,
                                $bookingCode,
                                $cardNumber,
                                'SL',
                                "PDSSL " . $idTransaksi . " " . $checkPayment->no_rekening,
                                $user->no_hp,
                                $checkPayment->no_rekening,
                                '62',
                                $tokenResponse,
                                $idTransaksi,
                                $token->channelId,
                                $idPromosi,
                                $promoAmount
                            );
                             
                            if ($clickPay->responseCode == '00') {
                                log_message('debug', 'RESPONSE CREATE BILLING MANDIRI', $clickPay);
                                $clickpayData = json_decode($clickPay->data);

                                $updateData['bookingCode'] = $bookingCode;
                                $updateData['cardNumber'] = $cardNumber;
                                $updateData['tokenResponse'] = $tokenResponse;
                                $updateData['reffBiller'] = $clickpayData->reffBiller;
                                $updateData['kodeBankPembayar'] ='008';
                                $updateData['payment'] = $payment;
                                $updateData['biayaTransaksi'] = $biayaTransaksiDisplay;

                                //update data pembayaran
                                $this->PaymentModel->update($idTransaksi, $updateData);

                                //Mendapatkan saldo rekening
                                $saldo = $this->getSaldo($checkPayment->no_rekening);

                                //Set response
                                $this->set_response([
                                    'status' => 'success',
                                    'message' => 'Pembayaran berhasil',
                                    'data' => array(
                                        'reffBiller' => $clickpayData->reffBiller,
                                        'saldo' => $saldo['saldo'],
                                        'saldoAkhir' => $saldo['saldoAkhir'],
                                        'saldoBlokir' => $saldo['saldoBlokir'],
                                        'saldoEfektif' => $saldo['saldoEfektif']
                                    )
                                ], 200);
                            } else {
                                $this->set_response(array(
                                    'status' => 'error',
                                    'code' => 103,
                                    'message' => 'Terjadi kesalahan jaringan mohon coba beberapa saat lagi',
                                    'reason' => $clickPay
                                ));
                            }
                        } elseif ($payment == 'WALLET' || $payment == 'GCASH') {
                            $pin = $this->post('pin');
                            $va = $this->post('va');
                            //Check user pin
                            if (!$this->User->isValidPIN2($token->id, $pin)) {
                                $this->set_response(array(
                                    'status' => 'error',
                                    'message' => 'PIN tidak valid',
                                    'code' => 102
                                ), 200);
                                return;
                            }
                            
                            $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment('SL', 'WALLET', '62', '');
                            $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment('SL', 'WALLET', '62', '');

                            log_message('debug', 'PAYMENT EMAS WALLET');
                            $paymentData = array(
                                'administrasi' => $checkPayment->administrasi,
                                'gram' => $checkPayment->gram,
                                'hakNasabah' => $checkPayment->hakNasabah,
                                'harga' => $checkPayment->harga,
                                'jenisTransaksi' => $checkPayment->jenis_transaksi,
                                'nilaiTransaksi' => $checkPayment->nilaiTransaksi,
                                'norek' => $checkPayment->no_rekening,
                                'reffSwitching' => $checkPayment->id_transaksi,
                                'surcharge' => $checkPayment->surcharge,
                                'totalKewajiban' => $checkPayment->total_kewajiban - $discountAmount,
                                'productCode' => '62',
                                'flag' => 'K',
                                'channelId' => $token->channelId,
                                'idPromosi' => $idPromosi,
                                'discountAmount' => $promoAmount
                            );

                            if ($payment == 'WALLET') {
                                $paymentData['paymentMethod'] = 'WALLET';
                                $paymentData['norekWallet'] = $user->norek;
                                $paymentData['walletId'] = $user->no_hp;
                            } elseif ($payment == 'GCASH') {
                                $paymentData['amount'] = $checkPayment->totalKewajiban;
                                $paymentData['paymentMethod'] = 'GCASH';
                                $paymentData['gcashId'] = $va;
                            }

                        
                            $walletPayment = $this->paymentTabungan($paymentData);

                            if ($walletPayment->responseCode == '00') {
                                log_message('debug', 'RESPONSE CREATE BILLING GCASH AND WALLET', $walletPayment);
                                $walletData = json_decode($walletPayment->data, true);

                                $walletData['payment'] = $payment;
                                $walletData['gcashId'] = $va;
                                $walletData['is_paid'] = '1';
                                $walletData['biayaTransaksi'] = $biayaTransaksiDisplay;

                                //Update wallet data
                                $this->PaymentModel->update($checkPayment->id_transaksi, $walletData);

                                $lblNotifNama = isset($checkPayment->namaNasabah) ? $checkPayment->namaNasabah : $token->nama;
                                $cif = $this->User->getUserCif($token->id);

                                $template = $this->generateBeliEmasNotif($idTransaksi, $cif);

                                $mobileTemplate = $template['mobile'];
                                $emailTemplate = $template['email'];
                                $minimalTemplate = $template['minimal'];

                                //Simpan notifikasi baru
                                $notifId = $this->NotificationModel->add(
                                    $token->id,
                                    NotificationModel::TYPE_EMAS,
                                    NotificationModel::CONTENT_TYPE_HTML,
                                    $this->ConfigModel->getNamaProduk('SL', '62'),
                                    "Top Up Emas Berhasil",
                                    $mobileTemplate,
                                    $minimalTemplate,
                                    "SL"
                                );

                                //Kirim notifikasi pembayaran ke device user
                                Message::sendFCMNotif(
                                    $this->User->getFCMToken($token->id),
                                    [
                                    "id" => $notifId,
                                    "tipe" => "SL",
                                    "title" => $this->ConfigModel->getNamaProduk('SL', '62'),
                                    "tagline" => "Top Up Emas Berhasil",
                                    "content" => "Top Up Emas Berhasil",
                                    "paymentType" => $payment,
                                    "token" => $token->no_hp
                                        ]
                                );

                                //Kirim Email Notifikasi
                                $this->load->helper('message');
                                Message::sendEmailBeliEmas(
                                    $token->email,
                                    $checkPayment->no_rekening,
                                    $emailTemplate
                                );

                                //Update Saldo Wallet
                                $saldoWallet = $this->updateWallet($token->channelId, $token->id, $user->no_hp);
                                $walletData['wallet'] = $saldoWallet;

                                $saldo = $this->getSaldo($checkPayment->no_rekening);
                                $walletData['saldo'] = $saldo['saldo'];
                                $walletData['saldoAkhir'] = $saldo['saldoAkhir'];
                                $walletData['saldoBlokir'] = $saldo['saldoBlokir'];
                                $walletData['saldoEfektif'] = $saldo['saldoEfektif'];

                                //Set response
                                $this->set_response([
                                    'status' => 'success',
                                    'message' => 'Pembelian Pembelian Emas Berhasil',
                                    'data' => $walletData
                                ], 200);
                            } else {
                                $this->set_response(array(
                                    'status' => 'error',
                                    'message' => '',
                                    'code' => 103,
                                    'reason' => $walletPayment
                                ), 200);
                            }
                        } elseif ($payment == 'VA_BCA' || $payment == 'VA_MANDIRI' || $payment == 'VA_BRI') {
                            $kodeBank = '';
                            if ($payment == 'VA_BCA') {
                                $kodeBank = '014';
                            } elseif ($payment == 'VA_MANDIRI') {
                                $kodeBank = '008';
                            } elseif ($payment == 'VA_BRI') {
                                $kodeBank = '002';
                            }
                            
                            $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment('SL', 'BANK', '62', $kodeBank);
                            $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment('SL', 'BANK', '62', $kodeBank);

                            $createBillingData = [
                                'channelId' => $token->channelId,
                                'amount' => $checkPayment->total_kewajiban + $biayaTransaksi - $discountAmount,
                                'customerEmail' => $user->email,
                                'customerName' => $token->nama,
                                'customerPhone' => $token->no_hp,
                                'flag' => 'K',
                                'jenisTransaksi' => 'SL',
                                'kodeProduk' => '62',
                                'norek' => $checkPayment->no_rekening,
                                'keterangan' => "PDSSL " . $idTransaksi . " " . $checkPayment->no_rekening,
                                'reffSwitching' => $idTransaksi,
                                'kodeBank' => $kodeBank,
                                'idPromosi' => $idPromosi,
                                'discountAmount' => $promoAmount
                            ];
                            $createBilling = $this->createBillingPegadaian($createBillingData);

                            if ($createBilling->responseCode == '00') {
                                log_message('debug', 'RESPONSE CREATE BILLING VA', $createBilling);
                                if (!isset($createBilling->data)) {
                                    $this->set_response(array(
                                        'status' => 'error',
                                        'message' => 'Kesalahan jaringan mohon coba beberapa saat lagi',
                                        'code' => 103,
                                        'reason' => $createBilling
                                            ), 200);
                                    return;
                                }

                                $billingData = json_decode($createBilling->data);


                                $virtualAccount = $billingData->vaNumber;
                                $tglExpired = $billingData->tglExpired;

                                if ($payment == 'VA_BCA') {
                                    $dataPrefix = $this->MasterModel->getPrefixBank('prefixBCA');
                                    $virtualAccount = $dataPrefix . $billingData->vaNumber;
                                    $tglExpired = $billingData->tglExpired;
                                }

                                $updatePaymentData = array(
                                    'tipe' => $payment,
                                    'virtual_account' => $virtualAccount,
                                    'tanggal_expired' => $tglExpired,
                                    'payment' => $payment,
                                    'biayaTransaksi' => $biayaTransaksiDisplay
                                );

                                //Buat payment baru
                                //Simpan payment
                                $this->PaymentModel->update($idTransaksi, $updatePaymentData);

                                $lblNotifNama = isset($checkPayment->namaNasabah) ? $checkPayment->namaNasabah : $token->nama;
                                $cif = $this->User->getUserCif($token->id);

                                $template = $this->generateBeliEmasNotif($idTransaksi, $customerData->cif);

                                $mobileTemplate = $template['mobile'];
                                $emailTemplate = $template['email'];
                                $minimalTemplate = $template['minimal'];

                                //Simpan notifikasi baru
                                $notifId = $this->NotificationModel->add(
                                    $token->id,
                                    NotificationModel::TYPE_EMAS,
                                    NotificationModel::CONTENT_TYPE_HTML,
                                    $this->ConfigModel->getNamaProduk('SL', '62'),
                                    "Segera bayar Rp. " . number_format($checkPayment->total_kewajiban + $biayaTransaksiDisplay - $discountAmount, 0, ",", ".") . ' ke ' . $virtualAccount,
                                    $mobileTemplate,
                                    $minimalTemplate,
                                    "SL"
                                );

                                //Kirim notifikasi pembayaran ke device user
                                Message::sendFCMNotif(
                                    $this->User->getFCMToken($token->id),
                                    [
                                    "id" => $notifId,
                                    "tipe" => "SL",
                                    "title" => $this->ConfigModel->getNamaProduk('SL', '62'),
                                    "tagline" => "Segera bayar Rp. " . number_format($checkPayment->total_kewajiban + $biayaTransaksiDisplay - $discountAmount, 0, ",", ".") . ' ke ' . $virtualAccount,
                                    "content" => "Bayar ke " . $virtualAccount . " sebelum " . $tglExpired,
                                    "token" => $token->no_hp
                                        ]
                                );

                                //Kirim Email Notifikasi
                                $this->load->helper('message');
                                Message::sendEmailBeliEmas(
                                    $token->email,
                                    $checkPayment->no_rekening,
                                    $emailTemplate
                                );

                                //Mendapatkan detail tabungan emas user
                                $saldo = $this->getSaldo($checkPayment->no_rekening);

                                //Set response
                                $this->set_response([
                                    'status' => 'success',
                                    'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
                                    'data' => array(
                                        'idTransaksi' => $idTransaksi,
                                        'virtualAccount' => $virtualAccount,
                                        'expired' => $tglExpired,
                                        'now' => date('Y-m-d H:i:s'),
                                        'saldo' => $saldo['saldo'],
                                        'saldoAkhir' => $saldo['saldoAkhir'],
                                        'saldoBlokir' => $saldo['saldoBlokir'],
                                        'saldoEfektif' => $saldo['saldoEfektif'],
                                    )
                                        ], 200);
                            } else {
                                log_message('debug', 'Create VA error. Reason:', $createBilling);
                                $this->set_response(array(
                                    'status' => 'error',
                                    'message' => 'Mohon coba kembali beberapa saat lagi',
                                    'code' => 103,
                                    'reason' => $createBilling
                                        ), 200);
                            }
                        } elseif ($payment == 'VA_PERMATA') {
                            $kodeBank = '013';
                            $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment('SL', 'BANK', '62', $kodeBank);
                            $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment('SL', 'BANK', '62', $kodeBank);

                            $createBillingData = [
                                'channelId' => $token->channelId,
                                'amount' => $checkPayment->total_kewajiban + $biayaTransaksi - $discountAmount,
                                'customerEmail' => $user->email,
                                'customerName' => $token->nama,
                                'customerPhone' => $token->no_hp,
                                'flag' => 'K',
                                'jenisTransaksi' => 'SL',
                                'productCode' => '62',
                                'norek' => $checkPayment->no_rekening,
                                'keterangan' => "PDSSL " . $idTransaksi . " " . $checkPayment->no_rekening,
                                'trxId' => $idTransaksi,
                                'idPromosi' => $idPromosi,
                                'discountAmount' => $promoAmount
                            ];
                            $createBilling = $this->createBillingPermata($createBillingData);

                            if ($createBilling->responseCode == '00') {
                                log_message('debug', 'RESPONSE CREATE BILLING VA_PERMATA', $createBilling);
                                if (!isset($createBilling->data)) {
                                    $this->set_response(array(
                                        'status' => 'error',
                                        'message' => 'Kesalahan jaringan mohon coba beberapa saat lagi',
                                        'code' => 103,
                                        'reason' => $createBilling
                                            ), 200);
                                    return;
                                }

                                $billingData = json_decode($createBilling->data);


                                $virtualAccount = $billingData->virtualAccount;
                                $tglExpired = $billingData->tglExpired;

                                $updatePaymentData = array(
                                    'tipe' => $payment,
                                    'virtual_account' => $virtualAccount,
                                    'tanggal_expired' => $tglExpired,
                                    'payment' => $payment,
                                    'biayaTransaksi' => $biayaTransaksiDisplay
                                );

                                //Buat payment baru
                                //Simpan payment
                                $this->PaymentModel->update($idTransaksi, $updatePaymentData);

                                $lblNotifNama = isset($checkPayment->namaNasabah) ? $checkPayment->namaNasabah : $token->nama;
                                $cif = $this->User->getUserCif($token->id);

                                $template = $this->generateBeliEmasNotif($idTransaksi, $customerData->cif);

                                $mobileTemplate = $template['mobile'];
                                $emailTemplate = $template['email'];
                                $minimalTemplate = $template['minimal'];

                                //Simpan notifikasi baru
                                $notifId = $this->NotificationModel->add(
                                    $token->id,
                                    NotificationModel::TYPE_EMAS,
                                    NotificationModel::CONTENT_TYPE_HTML,
                                    $this->ConfigModel->getNamaProduk('SL', '62'),
                                    "Segera bayar Rp. " . number_format($checkPayment->total_kewajiban + $biayaTransaksiDisplay - $discountAmount, 0, ",", ".") . ' ke ' . $virtualAccount,
                                    $mobileTemplate,
                                    $minimalTemplate,
                                    "SL"
                                );

                                //Kirim notifikasi pembayaran ke device user
                                Message::sendFCMNotif(
                                    $this->User->getFCMToken($token->id),
                                    [
                                    "id" => $notifId,
                                    "tipe" => "SL",
                                    "title" => $this->ConfigModel->getNamaProduk('SL', '62'),
                                    "tagline" => "Segera bayar Rp. " . number_format($checkPayment->total_kewajiban + $biayaTransaksiDisplay - $discountAmount, 0, ",", ".") . ' ke ' . $virtualAccount,
                                    "content" => "Bayar ke " . $virtualAccount . " sebelum " . $tglExpired,
                                    "token" => $token->no_hp
                                        ]
                                );

                                //Kirim Email Notifikasi
                                $this->load->helper('message');
                                Message::sendEmailBeliEmas(
                                    $token->email,
                                    $checkPayment->no_rekening,
                                    $emailTemplate
                                );

                                //Mendapatkan detail tabungan emas user
                                $saldo = $this->getSaldo($checkPayment->no_rekening);

                                //Set response
                                $this->set_response([
                                    'status' => 'success',
                                    'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
                                    'data' => array(
                                        'idTransaksi' => $idTransaksi,
                                        'virtualAccount' => $virtualAccount,
                                        'expired' => $tglExpired,
                                        'now' => date('Y-m-d H:i:s'),
                                        'saldo' => $saldo['saldo'],
                                        'saldoAkhir' => $saldo['saldoAkhir'],
                                        'saldoBlokir' => $saldo['saldoBlokir'],
                                        'saldoEfektif' => $saldo['saldoEfektif'],
                                    )
                                        ], 200);
                            } else {
                                log_message('debug', 'Create VA error. Reason:', $createBilling);
                                $this->set_response(array(
                                    'status' => 'error',
                                    'message' => 'Mohon coba kembali beberapa saat lagi',
                                    'code' => 103,
                                    'reason' => $createBilling
                                        ), 200);
                            }
                        }
                    } else {
                        $this->set_response(array(
                          'status' => 'error',
                          'message' => 'Terjadi kesalahan',
                          'code' => 103,
                          'reason' => $checkCustomer
                        ), 200);
                    }
                } else {
                    log_message('debug', 'Payment not found '.$this->post('id_transaksi'));
                    $this->set_response(array(
                       'status' => 'success',
                       'message' => 'Payment not found',
                       'code' => 102
                    ), 200);
                }
            }
        } else {
            $this->errorUnAuthorized();
        }
    }
    
    function history_get()
    {
        $token = $this->getToken();
        if ($token) {
            $setData = array(
                'no_rekening' => $this->query('no_rekening')
            );
            $this->form_validation->set_data($setData);

            $this->form_validation->set_rules('no_rekening', 'No Rekening', 'exact_length[16]|numeric|required');

            //Mendapatkan daftar history tabungan bulan ini
            if ($this->form_validation->run() == false) {
                $this->set_response(array(
                    'status' => 'error',
                    'message' => 'Invalid Input',
                    'code' => 102,
                    'errors' => $this->form_validation->error_array()
                ));
            } else {
                $noRekening = $this->query('no_rekening');
                $history = $this->EmasModel->getHistory($noRekening);
                $this->set_response(array(
                    'status' => 'success',
                    'message' => '',
                    'data' => $history
                ));
            }
        } else {
            $this->errorUnAuthorized();
        }
    }
    
    
    /**
     * Endpoint untuk melakukan pengecekan apakah sebelumnya user sudah  menyelesaikan payment atau belum
     */
    function check_unpaid_payment_get()
    {
        $token = $this->getToken();
        if ($token) {
            //Check apakah payment dengan status belum bayar dan belum expired
            $unpaid = $this->EmasModel->getUnpaidPayment($token->id);
            
            //Check ke core apakah status sudah terbayarkan atau belum. Status 1 = billing aktif
            if ($unpaid) {
                $checkBilling = $this->checkBillingVABNI($unpaid->id_transaksi, $token->channelId);
                if ($checkBilling->responseCode == '00') {
                    $billingData = json_decode($checkBilling->data);
                    if ($billingData->status == '1') {
                        $billingData->unpaid = true;
                        $this->set_response(array(
                            'status' => 'success',
                            'message' => 'Anda memiliki tagihan yang belum terbayarkan',
                            'data' => $billingData
                        ), 200);
                    } else {
                        $this->set_response(array(
                            'status' => 'success',
                            'message' => 'Tidak ada payment aktif',
                            'data' => array(
                                'unpaid' => false,
                            )
                        ), 200);
                    }
                } else {
                    $this->set_response(array(
                        'status' => 'success',
                        'message' => 'Tidak ada payment aktif',
                        'data' => array(
                            'unpaid' => false,
                        )
                    ), 200);
                }
            } else {
                $this->set_response(array(
                    'status' => 'success',
                    'message' => 'Tidak ada payment aktif',
                    'data' => array(
                        'unpaid' => false
                    )
                ), 200);
            }
        } else {
            $this->errorUnAuthorized();
        }
    }

    public function portofolio_post()
    {
        $token = $this->getToken();
        $this->form_validation->set_rules('cif', 'CIF', 'numeric|required');

        if (!$token) {
            return $this->errorUnAuthorized();
        }

        if (!$this->form_validation->run()) {
            return $this->send_response('error', 'invalid input', $this->form_validation->error_array(), 101);
        }

        $tab_emas = $this->Emas_service->listAccountNumber($this->post('cif'));

        log_message('debug', 'list tabungan emas', $tab_emas);

        return $this->send_response('success', '', $tab_emas);
    }

    public function detail_portofolio_get()
    {
        log_message('debug', 'Start : ' . __FUNCTION__ . ' ' . json_encode($this->query()));
        $token = $this->getToken();

        if (empty($token)) {
            return $this->errorUnAuthorized();
        }

        $this->form_validation->set_data(['no_rekening' => $this->query('no_rekening')]);
        $this->form_validation->set_rules('no_rekening', 'Nomor Rekening', 'numeric|required');

        if ($this->form_validation->run() == false) {
            foreach ($this->form_validation->error_array() as $key => $value) {
                $error = $value;
                break;
            }

            return $this->send_response('error', $error ?? null, '', 101);
        }

        $no_rekening      = $this->query('no_rekening');
        $response = $this->Emas_service->listMutations($no_rekening ?? null);

        return $this->send_response(
            $response['status'],
            $response['message'],
            $response['data'],
            $response['code']
        );
    }

    public function detail_portofolio_v2_get()
    {
        $token = $this->getToken();

        if (empty($token)) {
            return $this->errorUnAuthorized();
        }

        $this->form_validation->set_data($this->query());

        $this->form_validation->set_rules('no_rekening', 'Nomor Rekening', 'numeric|required');
        $this->form_validation->set_rules('limit', 'Limit', 'numeric|required');

        if ($this->form_validation->run() == false) {
            $error = $this->form_validation->error_array();
            return $this->send_response('error', $error ?? null, '', 101);
        }

        $response = $this->Emas_service->listMutation($this->query());
        
        return $this->send_response(
            $response['status'],
            $response['message'],
            $response['data'],
            $response['code']
        );
    }

    function transfer_post($method = 'inquiry')
    {
        $token = $this->getToken();
        
        if (!$token) {
            return $this->errorUnAuthorized();
        }

        $user = $this->User->getUser($token->id);

        if ($user->aktifasiTransFinansial == '0' || $user->aktifasiTransFinansial == '2') {
            return $this->send_response('error', $this->getAktifasiFinansialMsg($user->aktifasiTransFinansial), '', 201);
        }

        if ($method == 'payment') {
            $setData = array(
                'payment' => $this->post('payment'),
                'id_transaksi' => $this->post('id_transaksi'),
                'booking_code' => $this->post('booking_code'),
                'card_number' => $this->post('card_number'),
                'token_response' => $this->post('token_response'),
                'pin' => $this->post('pin'),
                'va' => $this->post('va')
            );
                
            $payment = $this->post('payment');
            $this->form_validation->set_data($setData);
            
            if ($payment == 'MANDIRI') {
                $this->form_validation->set_rules('booking_code', 'jumlah', 'required');
                $this->form_validation->set_rules('card_number', 'card_number', 'required|numeric');
                $this->form_validation->set_rules('token_response', 'jumlah', 'required');
            }

            if ($payment == 'WALLET' || $payment == 'GCASH') {
                $this->form_validation->set_rules('pin', 'pin', 'required|exact_length[6]|numeric');
            }

            if ($payment == 'GCASH') {
                $this->form_validation->set_rules('va', 'va', 'required|numeric');
            }
     
            $this->form_validation->set_rules('payment', 'payment', 'required');
            $this->form_validation->set_rules('id_transaksi', 'id_transaksi', 'required');
            $this->form_validation->set_rules('pin', 'pin', 'required');
            
            if (!$this->form_validation->run()) {
                return $this->send_response('error', 'Invalid input', $this->form_validation->error_array(), 101);
            }
                    
            $trxId = $this->post('id_transaksi');
            $pin = $this->post('pin');
            
            //Check if valid user PIN
            if (!$this->User->isValidPIN2($token->id, $pin)) {
                return $this->send_response('error', 'PIN tidak valid', '', 102);
            }
                 
            $checkPayment = $this->PaymentModel->getPaymentByTrxId2($trxId);
                    
            if (!$checkPayment) {
                return $this->send_response('error', 'Transaksi tidak ditemukan', '', 102);
            }

            if ($payment != 'WALLET') {
                log_message('debug', 'response ' . __FUNCTION__ . 'Pembayaran tidak valid');
                return $this->send_response('error', 'Pembayaran tidak valid', '', 102);
            }
                                
            $paymentData = array(
                "channelId" => $token->channelId,
                "gramTransaksi" =>  $checkPayment->gramTransaksi,
                "jenisTransaksi" => $checkPayment->jenisTransaksi,
                "namaNasabahTujuan" => $checkPayment->namaNasabahTujuan,
                "norek" => $checkPayment->norek,
                "norekTujuan" => $checkPayment->norekTujuan,
                "norekWallet" => $user->norek,
                "paymentMethod" => "WALLET",
                "reffBiller" => $checkPayment->id,
                "reffSwitching" => $checkPayment->reffSwitching,
                "tipeTransaksi" => "2",
                "totalKewajiban" => $checkPayment->totalKewajiban,
                "walletId" => $user->no_hp
            );
            
            $reqPayment = $this->paymentTransferEmas($paymentData);
                                
            if ($reqPayment->responseCode != '00') {
                log_message('debug', 'response ' . __FUNCTION__ . 'Terjadi kesalahan mohon coba beberapa saat lagi');
                return $this->send_response('error', 'Terjadi kesalahan mohon coba beberapa saat lagi', $reqPayment, 103);
            }
             
            if (!isset($reqPayment->data)) {
                return $this->send_response('error', 'Terjadi kesalahan mohon coba beberapa saat lagi', $reqPayment, 103);
            }
                
            $reqPaymentData = json_decode($reqPayment->data);

            $updateData = (array) $reqPaymentData;
            $updateData['is_paid'] = '1';
            $updateData['payment'] = $payment;
                
            $this->PaymentModel->update($trxId, $updateData);
                
            //Send notifikasi pembayaran telah berhasil
            $template = $this->generateTransferEmasNotif($trxId);

            $emailTemplate= $template['email'];
            $mobileTemplate = $template['mobile'];
            $minimalTemplate = $template['minimal'];

            //Simpan notifikasi baru
            $idNotif = $this->NotificationModel->add(
                $token->id,
                NotificationModel::TYPE_EMAS,
                NotificationModel::CONTENT_TYPE_HTML,
                $this->ConfigModel->getNamaProduk('TR', '62'),
                "Transfer Keluar Tabungan Emas ".$checkPayment->norekTujuan." berhasil",
                $mobileTemplate,
                $minimalTemplate,
                "TR"
            );

            //Kirim notifikasi pembayaran ke device user
            Message::sendFCMNotif(
                $this->User->getFCMToken($token->id),
                [
                    "id" => $idNotif,
                    "tipe" => "TR",
                    "title" => $this->ConfigModel->getNamaProduk('TR', '62'),
                    "tagline" => "Transfer Keluar Tabungan Emas " . $checkPayment->norekTujuan . " berhasil",
                    "content" => "",
                    "token" => $token->no_hp,
                    "noRekening" => $user->norek,
                    "namaNasabah" => $user->nama,
                    "cif" => $user->cif,
                    "code" => "TR_OUT"
                ]
            );
 
            //Kirim Email Notifikasi
            
            Message::sendEmail($token->email, "Transfer Keluar Tabungan Emas ".$checkPayment->norekTujuan, $emailTemplate);
                                                
            //Kirim juga notifikasi ke user rekening tujuan
            $userTujuan = $this->User->getUserByNoRek($checkPayment->norekTujuan);

            if ($userTujuan) {
                log_message('debug', 'USER_TUJUAN', $userTujuan);
                                
                //Send notifikasi pembayaran telah berhasil
                $template = $this->generateTransferEmasNotif($trxId, true, $userTujuan->nama);

                $emailTemplate = $template['email'];
                $mobileTemplate = $template['mobile'];
                $minimalTemplate = $template['minimal'];

                //Simpan notifikasi baru
                $idNotif2 = $this->NotificationModel->add(
                    $userTujuan->user_AIID,
                    NotificationModel::TYPE_EMAS,
                    NotificationModel::CONTENT_TYPE_HTML,
                    $this->ConfigModel->getNamaProduk('TR', '62'),
                    "Transfer Masuk Tabungan Emas dari ".$user->nama." Sejumlah ".$checkPayment->gramTransaksi.' gram',
                    $mobileTemplate,
                    $minimalTemplate,
                    "TR"
                );

                //Kirim notifikasi pembayaran ke device user
                Message::sendFCMNotif(
                    $userTujuan->fcm_token,
                    [
                    "id" => $idNotif2,
                    "tipe" => "TR",
                    "title" => $this->ConfigModel->getNamaProduk('TR', '62'),
                    "tagline" => "Transfer Masuk Tabungan Emas dari ".$user->nama." Sejumlah ".$checkPayment->gramTransaksi.' gram',
                    "content" => "",
                    "token" => $userTujuan->no_hp
                    ]
                );


                //Kirim Email Notifikasi
                $this->load->helper('message');
                Message::sendEmail($userTujuan->email, "Transfer Masuk Tabungan Emas ".$checkPayment->norekTujuan, $emailTemplate);
            }
                                    
                //Update Saldo Wallet
                $saldoWallet = $this->updateWallet($token->channelId, $token->id, $user->no_hp);
                $reqPaymentData->wallet = $saldoWallet;

                $saldo = $this->getSaldo($checkPayment->norek);
                $reqPaymentData->saldo = $saldo['saldo'];
                $reqPaymentData->saldoAkhir = $saldo['saldoAkhir'];
                $reqPaymentData->saldoBlokir = $saldo['saldoBlokir'];
                $reqPaymentData->saldoEfektif = $saldo['saldoEfektif'];

                return $this->send_response('success', 'Transfer emas berhasil', $reqPaymentData, '');
        }
        if ($method == 'inquiry') {
            $setData = array(
                'jumlah' => $this->post('jumlah'),
                'norek' => $this->post('norek'),
                'norek_tujuan' => $this->post('norek_tujuan')
            );

            $this->form_validation->set_data($setData);
            $this->form_validation->set_rules('norek', 'norek', 'callback__validation_noRekeningEmas');
            $this->form_validation->set_rules('norek_tujuan', 'norek_tujuan', 'callback__validation_noRekEmasTujuan['.$this->input->post('norek').']');
            $this->form_validation->set_rules('jumlah', 'jumlah', 'callback__validation_transferGram['.$this->input->post('norek').']');

            if (!$this->form_validation->run()) {
                $validation = Pegadaian::showErrorValidation($this->form_validation->error_array());
                return $this->send_response('error', 'Validation Exception', $validation, '101');
            }

            $jumlah = $this->post('jumlah');
            $norek = $this->post('norek');
            $norekTujuan = $this->post('norek_tujuan');
            
            $inquiryData = array(
                'channelId' => $token->channelId,
                'jenisTransaksi' => 'TR',
                'norek' => $norek,
                'norekTujuan' => $norekTujuan,
                'gramTransaksi' => $jumlah
            );

            $inquiry = $this->inquiryTransferEmas($inquiryData);
            
            if ($inquiry->responseCode == '12') {
                return $this->send_response('error', 'Limit Transfer Tabungan Emas Kamu sudah mencapai limit maksimal Transfer Tabungan Emas per Hari', '', 103);
            }

            if ($inquiry->responseCode != '00') {
                log_message('debug', 'response ' . __FUNCTION__ . $inquiry->responseDesc);
                return $this->send_response('error', $inquiry->responseDesc, '', 103);
            }

            if (!isset($inquiry->data)) {
                return $this->send_response('error', 'Terjadi kesalahan, mohon coba beberapa saat lagi', $inquiry, 103);
            }

            $resData = json_decode($inquiry->data);
            $resData->id_transaksi = $resData->reffSwitching;
                        
            $saveData = (array) $resData;

            $saveData['jenis_transaksi'] = 'TR';
            $saveData['user_AIID'] = $token->id;
            $saveData['id_transaksi'] = $resData->reffSwitching;
            $saveData['kode_produk'] = '62';
            $saveData['jenis_transaksi'] = 'TR';

            $this->PaymentModel->add($saveData);

            return $this->send_response('success', '', $resData, '');
        }
    }

    function buyback_post($method = 'inquiry')
    {
        $token = $this->getToken();
        if ($token) {
            $user = $this->User->getUser($token->id);
            if ($user->aktifasiTransFinansial == '0' || $user->aktifasiTransFinansial == '2') {
                $this->set_response(array(
                    'status' => 'error',
                    'message' => $this->getAktifasiFinansialMsg($user->aktifasiTransFinansial),
                    'code' => 201
                ), 200);
                return;
            }

            if ($method == 'payment') {
                $setData = array(
                    'id_transaksi' => $this->post('id_transaksi'),
                    'pin' => $this->post('pin')
                );
                
                $payment = $this->post('payment');
                $this->form_validation->set_data($setData);
                
                $this->form_validation->set_rules('id_transaksi', 'id_transaksi', 'required');
                $this->form_validation->set_rules('pin', 'pin', 'required|exact_length[6]|numeric');
                
                if ($this->form_validation->run() == false) {
                    $this->set_response(array(
                        'status' => 'error',
                        'code' => 101,
                        'message' => 'Invalid input',
                        'errors' => $this->form_validation->error_array()
                    ));
                } else {
                    $trxId = $this->post('id_transaksi');
                    $pin = $this->post('pin');
                    
                    //Check if valid user PIN
                    if ($this->User->isValidPIN2($token->id, $pin)) {
                        $checkPayment = $this->PaymentModel->getPaymentByTrxId2($trxId);
                    
                        if ($checkPayment) {
                            $payment = $checkPayment->payment;
                            
                            $paymentData = array(
                                'channelId' => $token->channelId,
                                'administrasi' => $checkPayment->administrasi,
                                'gramTransaksi' => $checkPayment->gramTransaksi,
                                'jenisTransaksi' => $checkPayment->jenisTransaksi,
                                'nilaiTransaksi' => $checkPayment->nilaiTransaksi,
                                'reffSwitching' => $checkPayment->reffSwitching,
                                'surcharge' => $checkPayment->surcharge,
                                'totalKewajiban' => $checkPayment->totalKewajiban,
                                'norek' => $checkPayment->norek,
                            );

                            if ($payment == 'WALLET' || $payment == 'GCASH') {
                                $paymentData['norekWallet'] = $user->norek;
                                $paymentData['paymentMethod'] = 'WALLET';
                                $paymentData['walletId'] = $user->no_hp;

                                if ($payment=='WALLET') {
                                    $paymentData['tipeTransaksi'] = '1';
                                    $paymentData['norekWalletTujuan'] = $checkPayment->norekWalletTujuan;
                                } elseif ($payment == 'GCASH') {
                                    $paymentData['gcashIdTujuan'] = $checkPayment->gcashIdTujuan;
                                    $paymentData['tipeTransaksi'] = '3';
                                }
                            } elseif ($payment == 'BANK') {
                                $paymentData['kodeBankTujuan'] = $checkPayment->kodeBankTujuan;
                                $paymentData['norekBankTujuan'] = $checkPayment->norekBankTujuan;
                                $paymentData['norekWallet'] = $user->norek;
                                $paymentData['walletId'] = $user->no_hp;
                                $paymentData['namaBankTujuan'] = $checkPayment->namaBankTujuan;
                                $paymentData['paymentMethod'] = 'WALLET';
                                $paymentData['tipeTransaksi'] = '2';
                            }
                            
                            $reqPayment = $this->paymentBuyBackEmas($paymentData);
                                
                            if ($reqPayment->responseCode == '00') {
                                if (!isset($reqPayment->data)) {
                                    $this->set_response(array(
                                        'status' => 'error',
                                        'message' => 'Terjadi kesalahan mohon coba beberapa saat lagi',
                                        'code' => 103,
                                        'reason' => $reqPayment
                                    ));
                                    return;
                                }
                                    
                                $reqPaymentData = json_decode($reqPayment->data);
                                    
                                $updateData = (array) $reqPaymentData;
                                $updateData['is_paid'] = '1';
                                $updateData['payment'] = $payment;
                                    
                                $this->PaymentModel->update($trxId, $updateData);
                                    
                                //Send notifikasi pembayaran telah berhasil
                                $template = $this->generateBuyBackNotif($trxId);

                                $emailTemplate= $template['email'];
                                $mobileTemplate = $template['mobile'];
                                $minimalTemplate = $template['minimal'];

                                //Simpan notifikasi baru
                                $idNotif = $this->NotificationModel->add(
                                    $token->id,
                                    NotificationModel::TYPE_EMAS,
                                    NotificationModel::CONTENT_TYPE_HTML,
                                    $this->ConfigModel->getNamaProduk('BB', '62'),
                                    "Jual Tabungan Emas ".$checkPayment->norek." berhasil",
                                    $mobileTemplate,
                                    $minimalTemplate,
                                    "BB"
                                );

                                //Kirim notifikasi pembayaran ke device user
                                Message::sendFCMNotif(
                                    $this->User->getFCMToken($token->id),
                                    [
                                    "id" => $idNotif,
                                    "tipe" => "BB",
                                    "title" => $this->ConfigModel->getNamaProduk('BB', '62'),
                                    "tagline" => "Jual Tabungan Emas ".$checkPayment->norek." berhasil",
                                    "content" => "",
                                    "token" => $token->no_hp,
                                    "noRekening" => $user->norek,
                                    "namaNasabah" => $user->nama,
                                    "cif" => $user->cif
                                    ]
                                );

                                //Kirim Email Notifikasi
                                $this->load->helper('message');
                                Message::sendEmail($token->email, "Jual Tabungan Emas ".$checkPayment->norek, $emailTemplate);
                                    
                                //Update Saldo Wallet
                                $saldoWallet = $this->updateWallet($token->channelId, $token->id, $user->no_hp);
                                $reqPaymentData->wallet = $saldoWallet;

                                $saldo = $this->getSaldo($checkPayment->norek);
                                $reqPaymentData->saldo = $saldo['saldo'];
                                $reqPaymentData->saldoAkhir = $saldo['saldoAkhir'];
                                $reqPaymentData->saldoBlokir = $saldo['saldoBlokir'];
                                $reqPaymentData->saldoEfektif = $saldo['saldoEfektif'];

                                    
                                $this->set_response(array(
                                    'status' => 'success',
                                    'message' => 'Jual tabungan emas berhasil',
                                    'data' => $reqPaymentData
                                ), 200);
                            } else {
                                $this->set_response(array(
                                    'status' => 'error',
                                    'message' => 'Terjadi kesalahan mohon coba beberapa saat lagi',
                                    'code' => 103,
                                    'reason' => $reqPayment
                                ));
                            }
                        } else {
                            $this->set_response(array(
                                'status' => 'error',
                                'message' => 'Transaksi tidak ditemukan',
                                'code' => 102
                            ), 200);
                        }
                    } else {
                        $this->set_response(array(
                            'status' => 'error',
                            'message' => 'PIN tidak valid',
                            'code' => 102
                        ), 200);
                    }
                }
            } elseif ($method == 'inquiry') {
                $setData = array(
                    'jumlah' => $this->post('jumlah'),
                    'norek' => $this->post('norek'),
                    'payment' => $this->post('payment'),
                    'kode_bank_tujuan' => $this->post('kode_bank_tujuan'),
                    'nama_nasabah_bank' => $this->post('nama_nasabah_bank'),
                    'norek_bank' => $this->post('norek_bank'),
                    'va' => $this->post('va')
                );
                
                $payment = $this->post('payment');
                
                $this->form_validation->set_data($setData);
                $this->form_validation->set_rules('norek', 'norek', 'required|exact_length[16]|numeric');
                $this->form_validation->set_rules('jumlah', 'jumlah', 'required|numeric');
                $this->form_validation->set_rules('payment', 'payment', 'required');

                if ($payment == 'BANK') {
                    $this->form_validation->set_rules('kode_bank_tujuan', 'kode_bank_tujuan', 'required|numeric');
                    $this->form_validation->set_rules('nama_nasabah_bank', 'nama_nasabah_bank', 'required');
                    $this->form_validation->set_rules('norek_bank', 'norek_bank', 'required|numeric');
                }

                if ($payment == 'GCASH') {
                    $this->form_validation->set_rules('va', 'va', 'required|numeric');
                }
                
                if ($this->form_validation->run() == false) {
                    $this->set_response(array(
                        'status' => 'error',
                        'code' => 101,
                        'message' => 'Invalid input',
                        'errors' => $this->form_validation->error_array()
                    ));
                } else {
                    $jumlah = $this->post('jumlah');
                    $norek = $this->post('norek');
                    $kodeBankTujuan = $this->post('kode_bank_tujuan');
                    $namaNasabahBank = $this->post('nama_nasabah_bank');
                    $norekBank = $this->post('norek_bank');
                    $va = $this->post('va');

                    $gold_price = $this->emas_service->getHargaEmas();
                    if (empty($gold_price['hargaBeli'])) {
                        return $this->set_response([
                            'status' => "error",
                            'message' => "Tidak dapat mengambil data",
                            'code' => 203,
                            'reason' => $gold_price
                        ], 401);
                    }

                    $dataInquiry = array(
                        "channelId" => "6017",
                        "gramTransaksi" => $jumlah,
                        "jenisTransaksi" => "BB",
                        "nilaiTransaksi" => $jumlah * $gold_price['hargaBeli'] * 100,
                        "norek" => $norek,
                    );
                    
                    if ($payment == 'WALLET' || $payment == 'GCASH') {
                        //Pastikan sudah punya wallet
                        if ($user->norek == '') {
                            $this->set_response(array(
                                'status' => 'error',
                                'messsage' => 'Anda harus memiliki wallet untuk melanjutkan transaksi',
                                'code' => 103
                            ), 200);
                            return;
                        }

                        $dataInquiry["norekWallet"] = $user->norek;
                        $dataInquiry["paymentMethod"] = "WALLET";
                        $dataInquiry["walletId"] = $user->no_hp;

                        if ($payment == 'WALLET') {
                            $dataInquiry["norekWalletTujuan"] = $user->norek;
                            $dataInquiry["tipeTransaksi"] = "1";
                        } elseif ($payment == 'GCASH') {
                            $dataInquiry["tipeTransaksi"] = "3";
                            $dataInquiry["gcashIdTujuan"] = $va;
                        }
                    } elseif ($payment == 'BANK') {
                        $dataInquiry["norekWalletTujuan"] = $user->norek;
                        $dataInquiry["kodeBankTujuan"] = $kodeBankTujuan;
                        $dataInquiry["namaBankTujuan"] = $namaNasabahBank;
                        $dataInquiry["norekBankTujuan"] = $norekBank;
                        $dataInquiry["paymentMethod"] = "WALLET";
                        $dataInquiry["tipeTransaksi"] = "2";
                    }
                    
                    $inquiry = $this->inquiryBuyBackEmas($dataInquiry);
                    if ($inquiry->responseCode == '00') {
                        if (!isset($inquiry->data)) {
                            $this->set_response(array(
                                'status' => 'error',
                                'message' => 'Terjadi kesalahan, mohon coba beberapa saat lagi',
                                'reason' => $inquiry,
                                'code' => 103
                                    ), 200);
                            return;
                        }

                        $resData = json_decode($inquiry->data);
                        $resData->id_transaksi = $resData->reffSwitching;

                        $saveData = (array) $resData;

                        $saveData['jenis_transaksi'] = 'BB';
                        $saveData['user_AIID'] = $token->id;
                        $saveData['id_transaksi'] = $resData->reffSwitching;
                        $saveData['kode_produk'] = '62';
                        $saveData['jenis_transaksi'] = 'BB';
                        $saveData["payment"] = $payment;
                        $saveData["tipe"] = $payment;
                        $saveData['gcashIdTujuan'] = $va;

                        $this->PaymentModel->add($saveData);

                        $this->set_response(array(
                            'status' => 'success',
                            'message' => '',
                            'data' => $resData
                                ), 200);
                    } else {
                        $this->set_response(array(
                            'status' => 'error',
                            'message' => 'Terjadi kesalahan, mohon coba beberapa saat lagi',
                            'reason' => $inquiry,
                            'code' => 103
                                ), 200);
                    }
                }
            }
        } else {
            $this->errorUnAuthorized();
        }
    }
    
   
    function cetak_post($method = 'inquiry')
    {
        $token = $this->getToken();
        if ($token) {
            $user = $this->User->getUser($token->id);
            if ($user->aktifasiTransFinansial == '0' || $user->aktifasiTransFinansial == '2') {
                $this->set_response(array(
                    'status' => 'error',
                    'message' => $this->getAktifasiFinansialMsg($user->aktifasiTransFinansial),
                    'code' => 201
                ), 200);
                return;
            }

            if ($method == 'payment') {
                $setData = array(
                    'id_transaksi' => $this->post('id_transaksi'),
                    'payment' => $this->post('payment'),
                    'booking_code' => $this->post('booking_code'),
                    'card_number' => $this->post('card_number'),
                    'token_response' => $this->post('token_response'),
                    'pin' => $this->post('pin'),
                    'va' => $this->post('va'),
                );
           
                $this->form_validation->set_data($setData);

                $this->form_validation->set_rules('id_transaksi', 'id_transaksi', 'required');
                $this->form_validation->set_rules('payment', 'payment', 'required');
                $this->form_validation->set_rules('pin', 'pin', 'required|exact_length[6]|numeric');

                $payment = $this->post('payment');

                if ($payment == 'MANDIRI') {
                     $this->form_validation->set_rules('booking_code', 'booking_code', 'required');
                     $this->form_validation->set_rules('card_number', 'card_number', 'required');
                     $this->form_validation->set_rules('token_response', 'token_response', 'required');
                }

                //Jika payment adalah wallet dan user belum akttifasi wallet
                if ($payment == 'WALLET' && $user->norek == '') {
                    $this->set_response(array(
                       'status' => 'error',
                       'message' => 'Anda belum melakukan aktifasi wallet',
                       'code' => 101
                    ), 200);
                    return;
                }

                if ($payment == 'GCASH') {
                    $this->form_validation->set_rules('va', 'va', 'required|numeric');
                }

                if ($this->form_validation->run() == false) {
                    $this->set_response(array(
                        'status' => 'error',
                        'message' => 'Invalid input',
                        'code' => 101,
                        'errors' => $this->form_validation->error_array()
                    ), 200);
                } else {
                    $idTransaksi = $this->post('id_transaksi');
                    $pin = $this->post('pin');
                    $promo_code = $this->post('promo_code');
                                 
                    //Check user pin
                    if (!$this->User->isValidPIN2($token->id, $pin)) {
                        $this->set_response(array(
                            'status' => 'error',
                            'message' => 'PIN tidak valid',
                            'code' => 102
                        ), 200);
                        return;
                    }
               
                    //Mendapatkan payment berdasarkan id transaksi
                    $checkPayment = $this->PaymentModel->getPaymentByTrxId($idTransaksi);
                    
                    $idPromosi = $checkPayment->idPromosi;
                    $discountAmount = $checkPayment->discountAmount;

                    $checkPayment->idTransaksi = $idTransaksi;
                    $checkPayment->discountAmount = $discountAmount;
                    $checkPayment->data_user = $user;
                    $checkPayment->data_token = $token;

                    if ($payment == 'VA_MAYBANK') {
                        $response = $this->Emas_service->paymentCetakEmas($payment, $checkPayment, $idTransaksi, $discountAmount, $user, $token, '016');
                        return $this->send_response($response);
                    }

                    if ($payment == 'FINPAY') {
                        $checkPayment->kode_bank = '770';
                        $response = $this->Emas_service->payment_finpay_cetak_emas($payment, $checkPayment);
                        return $this->send_response($response);
                    }

                    if ($checkPayment) {
                        if ($payment == 'BNI') {
                            $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment('OD', 'BANK', '62', '009');
                            $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment('OD', 'BANK', '62', '009');
                            $createBillingBNI = $this->createBillingVABNI(
                                $checkPayment->totalKewajiban + $biayaTransaksi - $discountAmount,
                                $user->email,
                                $token->nama,
                                $token->no_hp,
                                'K',
                                $checkPayment->jenisTransaksi,
                                "PDSOD " . $idTransaksi . " " . $checkPayment->norek,
                                $checkPayment->norek,
                                '62',
                                $idTransaksi,
                                $token->channelId,
                                $idPromosi,
                                $discountAmount
                            );

                            if ($createBillingBNI->responseCode == '00') {
                                if (!isset($createBillingBNI->data)) {
                                    $this->set_response(array(
                                        'status' => 'error',
                                        'message' => 'Kesalahan jaringan mohon coba beberapa saat lagi',
                                        'code' => 103,
                                        'reason' => $createBillingBNI
                                            ), 200);
                                    return;
                                }

                                $billingData = json_decode($createBillingBNI->data);

                                $virtualAccount = $billingData->virtualAccount;
                                $tglExpired = $billingData->tglExpired;

                                $updatePaymentData = array(
                                    'tipe' => $payment,
                                    'virtual_account' => $virtualAccount,
                                    'tanggal_expired' => $tglExpired,
                                    'payment' => $payment,
                                    'biayaTransaksi' => $biayaTransaksiDisplay,
                                    'kodeBankPembayar' => '009'
                                );

                                //Update payment
                                $this->PaymentModel->update($idTransaksi, $updatePaymentData);

                                $template = $this->generateCetakEmasNotif($idTransaksi);

                                $mobileTemplate = $template['mobile'];
                                $emailTemplate = $template['email'];
                                $minimalTemplate = $template['minimal'];

                                //Simpan notifikasi baru
                                $notifId = $this->NotificationModel->add(
                                    $token->id,
                                    NotificationModel::TYPE_EMAS,
                                    NotificationModel::CONTENT_TYPE_HTML,
                                    $this->ConfigModel->getNamaProduk('OD', '62'),
                                    "Segera bayar Rp. " . number_format($checkPayment->totalKewajiban + $biayaTransaksiDisplay - $discountAmount, 0, ",", ".") . ' ke ' . $virtualAccount,
                                    $mobileTemplate,
                                    $minimalTemplate,
                                    "OD"
                                );

                                //Kirim notifikasi pembayaran ke device user
                                Message::sendFCMNotif(
                                    $this->User->getFCMToken($token->id),
                                    [
                                    "id" => $notifId,
                                    "tipe" => "OD",
                                    "title" => $this->ConfigModel->getNamaProduk('OD', '62'),
                                    "tagline" => "Segera bayar Rp. " . number_format($checkPayment->totalKewajiban + $biayaTransaksiDisplay - $discountAmount, 0, ",", ".") . ' ke ' . $virtualAccount,
                                    "content" => "Bayar ke " . $virtualAccount . " sebelum " . $tglExpired,
                                    "token" => $token->no_hp
                                        ]
                                );

                                //Kirim Email Notifikasi
                                $this->load->helper('message');
                                Message::sendEmail($user->email, 'Order Cetak Tabungan Emas', $emailTemplate);

                                //get saldo terakhir
                                $saldo = $this->getSaldo($checkPayment->norek);

                                //Set response
                                $this->set_response([
                                    'status' => 'success',
                                    'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
                                    'data' => array(
                                        'idTransaksi' => $idTransaksi,
                                        'virtualAccount' => $virtualAccount,
                                        'expired' => $tglExpired,
                                        'now' => date('Y-m-d H:i:s'),
                                        'saldo' => $saldo['saldo'],
                                        'saldoAkhir' => $saldo['saldoAkhir'],
                                        'saldoBlokir' => $saldo['saldoBlokir'],
                                        'saldoEfektif' => $saldo['saldoEfektif']
                                    )
                                        ], 200);
                            } else {
                                log_message('debug', 'Create VA error. Reason:' . json_encode($createBillingBNI));
                                $this->set_response(array(
                                    'status' => 'error',
                                    'message' => 'Mohon coba kembali beberapa saat lagi',
                                    'code' => 103,
                                    'reason' => $createBillingBNI
                                        ), 200);
                            }
                        } elseif ($payment == 'MANDIRI') {
                                 $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment('OD', 'BANK', '62', '008');
                                 $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment('OD', 'BANK', '62', '008');

                                 $bookingCode = $this->post('booking_code');
                                 $cardNumber = $this->post('card_number');
                                 $tokenResponse = $this->post('token_response');

                                 //Bayar dengan mandiri click pay
                                 $clickPay = $this->mandiriClickPay(
                                     $checkPayment->totalKewajiban + $biayaTransaksi - $discountAmount,
                                     $bookingCode,
                                     $cardNumber,
                                     $checkPayment->jenisTransaksi,
                                     "PDSOD " . $idTransaksi . " " . $checkPayment->norek,
                                     $user->no_hp,
                                     $checkPayment->norek,
                                     '62',
                                     $tokenResponse,
                                     $idTransaksi,
                                     $token->channelId,
                                     $idPromosi,
                                     $discountAmount
                                 );

                            if ($clickPay->responseCode == '00') {
                                $clickpayData = json_decode($clickPay->data);

                                $updateData['bookingCode'] = $bookingCode;
                                $updateData['cardNumber'] = $cardNumber;
                                $updateData['tokenResponse'] = $tokenResponse;
                                $updateData['reffBiller'] = $clickpayData->reffBiller;
                                $updateData['kodeBankPembayar'] ='008';
                                $updateData['payment'] = $payment;
                                $updateData['biayaTransaksi'] = $biayaTransaksiDisplay;
                                $updateData['is_paid'] = '1';

                                //update data pembayaran
                                $this->PaymentModel->update($idTransaksi, $updateData);

                                //mendapatkan saldo terakhir
                                $saldo = $this->getSaldo($checkPayment->norek);

                                //Set response
                                $this->set_response([
                                    'status' => 'success',
                                    'message' => 'Pembayaran Order Cetak Emas Berhasil',
                                    'data' => array(
                                       'reffBiller' => $clickpayData->reffBiller,
                                       'saldo' => $saldo['saldo'],
                                       'saldoAkhir' => $saldo['saldoAkhir'],
                                       'saldoBlokir' => $saldo['saldoBlokir'],
                                       'saldoEfektif' => $saldo['saldoEfektif'],

                                    )
                                ], 200);
                            } else {
                                $this->set_response(array(
                                    'status' => 'error',
                                    'code' => 103,
                                    'message' => 'Terjadi kesalahan jaringan mohon coba beberapa saat lagi',
                                    'reason' => $clickPay
                                ));
                            }
                        } elseif ($payment == 'WALLET' || $payment == 'GCASH') {
                                 $va = $this->post('va');

                                 $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment('OD', 'WALLET', '62', '');
                                 $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment('OD', 'WALLET', '62', '');

                                 log_message('debug', 'PAYMENT EMAS WALLET');
                                 
                                 $arrayOrder = json_decode($checkPayment->order, true);

                                 $paymentData = array(
                                     'administrasi' => $checkPayment->administrasi,
                                     'jenisTransaksi' => $checkPayment->jenisTransaksi,
                                     'kodeCabang' => $checkPayment->kodeCabang,
                                     'noBuku' => $checkPayment->noBuku,
                                     'norek' => $checkPayment->norek,
                                     'order' => $arrayOrder,
                                     'reffSwitching' => $checkPayment->id_transaksi,
                                     'surcharge' => $checkPayment->surcharge,
                                     'totalKewajiban' => $checkPayment->totalKewajiban - $discountAmount,
                                     'vendor' => $checkPayment->vendor,
                                     'channelId' => $token->channelId,
                                     'idPromosi' => $idPromosi,
                                     'discountAmount' => $discountAmount
                                 );

                                 if ($payment == 'WALLET') {
                                     $paymentData['paymentMethod'] = 'WALLET';
                                     $paymentData['norekWallet'] = $user->norek;
                                     $paymentData['walletId'] = $user->no_hp;
                                 } elseif ($payment == 'GCASH') {
                                     $paymentData['paymentMethod'] = 'GCASH';
                                     $paymentData['gcashId'] = $va;
                                 }

                                 $walletPayment = $this->paymentCetakEmas($paymentData);

                                 if ($walletPayment->responseCode == '00') {
                                     $walletData = json_decode($walletPayment->data, true);

                                     $walletData['payment'] = $payment;
                                     $walletData['is_paid'] = '1';
                                     $walletData['biayaTransaksi'] = $biayaTransaksiDisplay;
                                     $walletData['order'] = json_encode($arrayOrder);

                                     //Update wallet data
                                     $this->PaymentModel->update($checkPayment->id_transaksi, $walletData);

                                     $template = $this->generateCetakEmasNotif($idTransaksi);

                                     $mobileTemplate = $template['mobile'];
                                     $emailTemplate = $template['email'];
                                     $minimalTemplate = $template['minimal'];

                                     //Simpan notifikasi baru
                                     $notifId = $this->NotificationModel->add(
                                         $token->id,
                                         NotificationModel::TYPE_EMAS,
                                         NotificationModel::CONTENT_TYPE_HTML,
                                         $this->ConfigModel->getNamaProduk('OD', '62'),
                                         "Order Cetak Emas Barhasil",
                                         $mobileTemplate,
                                         $minimalTemplate,
                                         "OD"
                                     );

                                     //Kirim notifikasi pembayaran ke device user
                                     Message::sendFCMNotif(
                                         $this->User->getFCMToken($token->id),
                                         [
                                         "id" => $notifId,
                                         "tipe" => "OD",
                                         "title" => $this->ConfigModel->getNamaProduk('OD', '62'),
                                         "tagline" => "Order Cetak Emas Berhasil",
                                         "content" => "Order Cetak Emas Berhasil",
                                         "paymentType" => $payment,
                                         "token" => $token->no_hp
                                             ]
                                     );

                                     //Kirim Email Notifikasi
                                     $this->load->helper('message');
                                     Message::sendEmail(
                                         $token->email,
                                         'Order Cetak Tabungan Emas',
                                         $emailTemplate
                                     );

                                    
                                     //Update Saldo Wallet
                                     $saldoWallet = $this->updateWallet($token->channelId, $token->id, $user->no_hp);
                                     $walletData['wallet'] = $saldoWallet;

                                     //get saldo
                                     $saldo = $this->getSaldo($checkPayment->getSaldo($checkPayment->norek));

                                     $walletData['saldo'] = $saldo['saldo'];
                                     $walletData['saldoAkhir'] = $saldo['saldoAkhir'];
                                     $walletData['saldoBlokir'] = $saldo['saldoBlokir'];
                                     $walletData['saldoEfektif'] = $saldo['saldoEfektif'];

                                    
                                     //Set response
                                     $this->set_response([
                                         'status' => 'success',
                                         'message' => 'Order Cetak Emas Berhasil',
                                         'data' => $walletData
                                     ], 200);
                                 } else {
                                     $this->set_response(array(
                                         'status' => 'error',
                                         'message' => '',
                                         'code' => 103,
                                         'reason' => $walletPayment
                                     ), 200);
                                 }
                        } elseif ($payment == 'VA_BCA' || $payment == 'VA_MANDIRI' || $payment == 'VA_BRI') {
                                $kodeBank = '';
                            if ($payment == 'VA_BCA') {
                                $kodeBank = '014';
                            } elseif ($payment == 'VA_MANDIRI') {
                                $kodeBank = '008';
                            } elseif ($payment == 'VA_BRI') {
                                $kodeBank = '002';
                            }
                                $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment('OD', 'BANK', '62', $kodeBank);
                                $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment('OD', 'BANK', '62', $kodeBank);
                                $createBillingData = [
                                    'channelId' => $token->channelId,
                                    'amount' => $checkPayment->totalKewajiban + $biayaTransaksi - $discountAmount,
                                    'customerEmail' => $user->email,
                                    'customerName' => $token->nama,
                                    'customerPhone' => $token->no_hp,
                                    'flag' => 'K',
                                    'jenisTransaksi' => $checkPayment->jenisTransaksi,
                                    'kodeProduk' => '62',
                                    'norek' => $checkPayment->no_rekening,
                                    'keterangan' => "PDSOD " . $idTransaksi . " " . $checkPayment->norek,
                                    'reffSwitching' => $idTransaksi,
                                    'kodeBank' => $kodeBank,
                                    'idPromosi' => $idPromosi,
                                    'discountAmount' => $discountAmount
                                ];

                                $createBilling = $this->createBillingPegadaian($createBillingData);

                                if ($createBilling->responseCode == '00') {
                                    if (!isset($createBilling->data)) {
                                        $this->set_response(array(
                                            'status' => 'error',
                                            'message' => 'Kesalahan jaringan mohon coba beberapa saat lagi',
                                            'code' => 103,
                                            'reason' => $createBilling
                                                ), 200);
                                        return;
                                    }

                                    $billingData = json_decode($createBilling->data);

                                    $virtualAccount = $billingData->vaNumber;
                                    $tglExpired = $billingData->tglExpired;

                                    if ($payment == 'VA_BCA') {
                                        $dataPrefix = $this->MasterModel->getPrefixBank('prefixBCA');
                                        $virtualAccount = $dataPrefix . $billingData->vaNumber;
                                        $tglExpired = $billingData->tglExpired;
                                    }

                                    $updatePaymentData = array(
                                        'tipe' => $payment,
                                        'virtual_account' => $virtualAccount,
                                        'tanggal_expired' => $tglExpired,
                                        'payment' => $payment,
                                        'biayaTransaksi' => $biayaTransaksiDisplay,
                                        'kodeBankPembayar' => $kodeBank
                                    );

                                    //Update payment
                                    $this->PaymentModel->update($idTransaksi, $updatePaymentData);

                                    $template = $this->generateCetakEmasNotif($idTransaksi);

                                    $mobileTemplate = $template['mobile'];
                                    $emailTemplate = $template['email'];
                                    $minimalTemplate = $template['minimal'];

                                    //Simpan notifikasi baru
                                    $notifId = $this->NotificationModel->add(
                                        $token->id,
                                        NotificationModel::TYPE_EMAS,
                                        NotificationModel::CONTENT_TYPE_HTML,
                                        $this->ConfigModel->getNamaProduk('OD', '62'),
                                        "Segera bayar Rp. " . number_format($checkPayment->totalKewajiban + $biayaTransaksiDisplay, 0, ",", ".") . ' ke ' . $virtualAccount,
                                        $mobileTemplate,
                                        $minimalTemplate,
                                        "OD"
                                    );

                                    //Kirim notifikasi pembayaran ke device user
                                    Message::sendFCMNotif(
                                        $this->User->getFCMToken($token->id),
                                        [
                                        "id" => $notifId,
                                        "tipe" => "OD",
                                        "title" => $this->ConfigModel->getNamaProduk('OD', '62'),
                                        "tagline" => "Segera bayar Rp. " . number_format($checkPayment->totalKewajiban + $biayaTransaksiDisplay, 0, ",", ".") . ' ke ' . $virtualAccount,
                                        "content" => "Bayar ke " . $virtualAccount . " sebelum " . $tglExpired,
                                        "token" => $token->no_hp
                                            ]
                                    );

                                    //Kirim Email Notifikasi
                                    $this->load->helper('message');
                                    Message::sendEmail($user->email, 'Order Cetak Tabungan Emas', $emailTemplate);

                                    //get saldo terakhir
                                    $saldo = $this->getSaldo($checkPayment->norek);

                                    //Set response
                                    $this->set_response([
                                        'status' => 'success',
                                        'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
                                        'data' => array(
                                            'idTransaksi' => $idTransaksi,
                                            'virtualAccount' => $virtualAccount,
                                            'expired' => $tglExpired,
                                            'now' => date('Y-m-d H:i:s'),
                                            'saldo' => $saldo['saldo'],
                                            'saldoAkhir' => $saldo['saldoAkhir'],
                                            'saldoBlokir' => $saldo['saldoBlokir'],
                                            'saldoEfektif' => $saldo['saldoEfektif'],
                                        )
                                            ], 200);
                                } else {
                                    log_message('debug', 'Create VA error. Reason:' . json_encode($createBilling));
                                    $this->set_response(array(
                                        'status' => 'error',
                                        'message' => 'Mohon coba kembali beberapa saat lagi',
                                        'code' => 103,
                                        'reason' => $createBilling
                                            ), 200);
                                }
                        } elseif ($payment == 'VA_PERMATA') {
                            $kodeBank = '013';
                                
                            $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment('OD', 'BANK', '62', $kodeBank);
                            $biayaTransaksiDisplay = $this->ConfigModel->getBiayaPayment('OD', 'BANK', '62', $kodeBank);
                            $createBillingData = [
                                'channelId' => $token->channelId,
                                'amount' => $checkPayment->totalKewajiban + $biayaTransaksi - $discountAmount,
                                'customerEmail' => $user->email,
                                'customerName' => $token->nama,
                                'customerPhone' => $token->no_hp,
                                'flag' => 'K',
                                'jenisTransaksi' => $checkPayment->jenisTransaksi,
                                'productCode' => '62',
                                'norek' => $checkPayment->no_rekening,
                                'keterangan' => "PDSOD " . $idTransaksi . " " . $checkPayment->norek,
                                'trxId' => $idTransaksi,
                                'idPromosi' => $idPromosi,
                                'discountAmount' => $discountAmount
                            ];
                            $createBilling = $this->createBillingPermata($createBillingData);

                            if ($createBilling->responseCode == '00') {
                                if (!isset($createBilling->data)) {
                                    $this->set_response(array(
                                        'status' => 'error',
                                        'message' => 'Kesalahan jaringan mohon coba beberapa saat lagi',
                                        'code' => 103,
                                        'reason' => $createBilling
                                            ), 200);
                                    return;
                                }

                                $billingData = json_decode($createBilling->data);

                                $virtualAccount = $billingData->virtualAccount;
                                $tglExpired = $billingData->tglExpired;

                                $updatePaymentData = array(
                                    'tipe' => $payment,
                                    'virtual_account' => $virtualAccount,
                                    'tanggal_expired' => $tglExpired,
                                    'payment' => $payment,
                                    'biayaTransaksi' => $biayaTransaksiDisplay,
                                    'kodeBankPembayar' => $kodeBank
                                );

                                //Update payment
                                $this->PaymentModel->update($idTransaksi, $updatePaymentData);

                                $template = $this->generateCetakEmasNotif($idTransaksi);

                                $mobileTemplate = $template['mobile'];
                                $emailTemplate = $template['email'];
                                $minimalTemplate = $template['minimal'];

                                //Simpan notifikasi baru
                                $notifId = $this->NotificationModel->add(
                                    $token->id,
                                    NotificationModel::TYPE_EMAS,
                                    NotificationModel::CONTENT_TYPE_HTML,
                                    $this->ConfigModel->getNamaProduk('OD', '62'),
                                    "Segera bayar Rp. " . number_format($checkPayment->totalKewajiban + $biayaTransaksiDisplay, 0, ",", ".") . ' ke ' . $virtualAccount,
                                    $mobileTemplate,
                                    $minimalTemplate,
                                    "OD"
                                );

                                //Kirim notifikasi pembayaran ke device user
                                Message::sendFCMNotif(
                                    $this->User->getFCMToken($token->id),
                                    [
                                    "id" => $notifId,
                                    "tipe" => "OD",
                                    "title" => $this->ConfigModel->getNamaProduk('OD', '62'),
                                    "tagline" => "Segera bayar Rp. " . number_format($checkPayment->totalKewajiban + $biayaTransaksiDisplay, 0, ",", ".") . ' ke ' . $virtualAccount,
                                    "content" => "Bayar ke " . $virtualAccount . " sebelum " . $tglExpired,
                                    "token" => $token->no_hp
                                        ]
                                );

                                //Kirim Email Notifikasi
                                $this->load->helper('message');
                                Message::sendEmail($user->email, 'Order Cetak Tabungan Emas', $emailTemplate);

                                //get saldo terakhir
                                $saldo = $this->getSaldo($checkPayment->norek);

                                //Set response
                                $this->set_response([
                                    'status' => 'success',
                                    'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
                                    'data' => array(
                                        'idTransaksi' => $idTransaksi,
                                        'virtualAccount' => $virtualAccount,
                                        'expired' => $tglExpired,
                                        'now' => date('Y-m-d H:i:s'),
                                        'saldo' => $saldo['saldo'],
                                        'saldoAkhir' => $saldo['saldoAkhir'],
                                        'saldoBlokir' => $saldo['saldoBlokir'],
                                        'saldoEfektif' => $saldo['saldoEfektif']
                                    )
                                        ], 200);
                            } else {
                                log_message('debug', 'Create VA error. Reason:' . json_encode($createBilling));
                                $this->set_response(array(
                                    'status' => 'error',
                                    'message' => 'Mohon coba kembali beberapa saat lagi',
                                    'code' => 103,
                                    'reason' => $createBilling
                                        ), 200);
                            }
                        }
                    } else {
                        log_message('debug', 'Payment not found'.$this->post('id_transaksi'));
                        $this->set_response(array(
                            'status' => 'success',
                            'message' => 'Payment not found',
                            'code' => 102
                        ), 200);
                    }
                }
            } elseif ($method == 'inquiry') {
                $setData = array(
                    'norek' => $this->post('norek'),
                    'vendor' => $this->post('vendor'),
                    'order' => $this->post('order'),
                    'kode_cabang' => $this->post('kode_cabang'),
                    'promo_code' => $this->post('promo_code')
                );
                
                $this->form_validation->set_data($setData);
                $this->form_validation->set_rules('norek', 'norek', 'required|exact_length[16]|numeric');
                $this->form_validation->set_rules('vendor', 'vendor', 'required');
                $this->form_validation->set_rules('order', 'order', 'required');
                $this->form_validation->set_rules('kode_cabang', 'kode_cabang', 'required');

                
                if ($this->form_validation->run() == false) {
                    $this->set_response(array(
                        'status' => 'error',
                        'code' => 101,
                        'message' => 'Invalid input',
                        'errors' => $this->form_validation->error_array()
                    ));
                } else {
                    $norek = $this->post('norek');
                    $vendor = $this->post('vendor');
                    $order = $this->post('order');
                    $kodeCabang = $this->post('kode_cabang');
                    $promoCode = $this->post('promo_code');
                    
                    $arrayOrder = json_decode($order, true);
                    $totalBerat = 0;
                    
                    foreach ($arrayOrder as $order) {
                        $totalBerat += $order['berat'] * $order['keping'];
                    }
                    
                    if (json_last_error() !== JSON_ERROR_NONE) {
                        $this->set_response(array(
                            'status' => 'error',
                            'message' => 'Invalid input order',
                            'code' => 101,
                        ), 200);
                        return;
                    }
                    
                    $max_cetak_gram = $this->MasterModel->getHargaCetakEmas('Max_Cetak_Gram');
                    $cetak_sisa_saldo_gram = $this->MasterModel->getHargaCetakEmas('Cetak_min_sisa_saldo');

                    //Pertama cek apakah no rekening punya buku tabungan
                    $detailTabunganEmas = $this->detailTabunganEmas($norek, $token->channelId);
                    $saldoEfektif = json_decode($detailTabunganEmas->data)->saldoEfektif ?? '';

                    //val tidak mempunyai saldo efektif
                    if (empty($saldoEfektif)) {
                        $this->send_response('error', 'Saldo emas tidak ditemukan', '');
                        log_message('debug', 'response ' . __FUNCTION__ . ' Message : Saldo emas tidak ditemukan');
                        return;
                    }

                    //val ketika jumlah keping emas yang dibeli lebih besar dari saldo
                    if ($totalBerat > $saldoEfektif) {
                        $this->send_response('error', 'Saldo emas tidak mencukupi', '');
                        log_message('debug', 'response ' . __FUNCTION__ . ' Message : Saldo emas tidak mencukupi');
                        return;
                    }

                    $hitung = $saldoEfektif - $totalBerat;

                    // Val jumlah keping emas tidak mengendapkan Saldo Emas minimal 0.1
                    if ($hitung < $cetak_sisa_saldo_gram) {
                        $this->send_response('error', 'Saldo mengendap minimal ' . $cetak_sisa_saldo_gram . ' gram.', '');
                        log_message('debug', 'response ' . __FUNCTION__ . ' Message : Saldo mengendap minimal ' . $transfer_min_sisa_saldo . ' gram.' . $max_cetak_gram . ' Gram');
                        return;
                    }
                    
                    // val ketika jumlah keping emas besar dari max cetak 20gr
                    if ($totalBerat > $max_cetak_gram) {
                        $this->send_response('error', 'Maksimal jumlah Cetak Emas ' . $max_cetak_gram . ' Gram', '');
                        log_message('debug', 'response ' . __FUNCTION__ . ' Message : Maksimal jumlah Cetak Emas ' . $max_cetak_gram . ' Gram');
                        return;
                    }

                    // Find grand total biaya
                    $grandTotal = 0;
                    foreach ($arrayOrder as $ao) {
                        $grandTotal = $grandTotal + $ao['totalBiaya'];
                    }
                    
                    if ($detailTabunganEmas->responseCode == '00') {
                        if (!isset($detailTabunganEmas->data)) {
                            $this->set_response(array(
                                'status' => 'error',
                                'message' => 'Terjadi kesalahan. Mohon coba beberapa saat lagi',
                                'code' => 103,
                                'reason' => $detailTabunganEmas
                            ), 200);
                            return;
                        }
                        
                        $dataDettabemas = json_decode($detailTabunganEmas->data);
                        
                        if ($dataDettabemas->status == "1") {
                            //Transaksi bisa dilanjutkan
                            $dataInquiry = array(
                                'channelId' => $token->channelId,
                                'jenisTransaksi' => 'OD',
                                'kodeCabang' => $kodeCabang,
                                'noBuku' => $dataDettabemas->noBuku,
                                'norek' => $norek,
                                'order' => $arrayOrder,
                                'vendor' => $vendor
                            );

                            // Lakukan validasi promo/voucher gpoint jika ada
                            if ($promoCode != null) {
                                if ($user->cif == null) {
                                    return $this->set_response([
                                        'status' => 'error',
                                        'message' => 'Anda harus memiliki CIF untuk menggunakan fitur voucher',
                                        'code' => 102
                                    ]);
                                }

                                $validateData = [
                                    'promoCode' => $promoCode,
                                    'voucherId' => null,
                                    'userId' => $user->cif,
                                    'transactionAmount' => (int) $grandTotal,
                                    'validators' => [
                                        'channel' => $this->config->item('core_post_username'),
                                        'product' => '62',
                                        'transactionType' => 'OD',
                                        'unit' => 'rupiah',
                                    ]
                                ];

                                $validateVoucher = $this->gPointValidateVoucher($validateData);

                                if ($validateVoucher->status == 'Error') {
                                    return $this->set_response([
                                        'status' => 'error',
                                        'message' => $validateVoucher->message,
                                        'code' => 103,
                                    ]);
                                } elseif ($validateVoucher->status == 'Success') {
                                    $dataInquiry['discountAmount'] = $validateVoucher->data->discount;
                                    $dataInquiry['idPromosi'] = $validateVoucher->data->journalAccount.';'.$promoCode;
                                }
                            }
                            
                            $inquiryCetakEmas = $this->inquiryCetakEmas($dataInquiry);
                                
                            if ($inquiryCetakEmas->responseCode == '00') {
                                if (!isset($inquiryCetakEmas->data)) {
                                    $this->set_response(array(
                                        'status' => 'error',
                                        'message' => 'Terjadi kesalahan. Mohon coba beberapa saat lagi',
                                        'code' => 103,
                                        'reason' => $inquiryCetakEmas
                                    ), 200);
                                    return;
                                }
                                
                                
                                $inquiryData = json_decode($inquiryCetakEmas->data);
                                
                                $saveData = (array) $inquiryData;
                                $saveData['user_AIID'] = $token->id;
                                $saveData['id_transaksi'] = $inquiryData->reffSwitching;
                                $saveData['order'] = json_encode($inquiryData->order);
                                $saveData['kodeCabang'] = $kodeCabang;
                                $saveData['vendor'] = $vendor;
                                $saveData['jenis_transaksi'] = 'OD';
                                $saveData['kode_produk'] = '62';
                                $saveData['no_rekening'] = $setData['norek'];

                                if (isset($dataInquiry['discountAmount'], $dataInquiry['idPromosi'])) {
                                    $saveData['discountAmount'] = $dataInquiry['discountAmount'];
                                    $saveData['idPromosi'] = $dataInquiry['idPromosi'];
                                }
                                                                
                                $this->PaymentModel->add($saveData);
                                
                                //Get biaya channel untuk masing-masing metode pembayaran
                                $biayaChannel = $this->Payment_service->getBiayaPayment($saveData['jenis_transaksi'], $saveData['kode_produk']);
                                
                                $saveData['biayaChannel'] = $biayaChannel;
                        
                                //Set response
                                $this->set_response(array(
                                    'status' => 'success',
                                    'message' => '',
                                    'data' => $saveData
                                ), 200);
                            } else {
                                //val dasi rc 14 balikaan core
                                if ($inquiryCetakEmas->responseCode == '14') {
                                    $this->send_response('error', 'Mohon melakukan proses cetak buku tabungan terlebih dahulu.', '', 102);
                                    log_message('debug', 'response ' . __FUNCTION__ . ' Message : Mohon melakukan proses cetak buku tabungan terlebih dahulu');
                                    return;
                                }

                                $this->set_response(array(
                                    'status' => 'error',
                                    'message' => 'Terjadi kesalahan. Mohon coba beberapa saat lagi',
                                    'code' => 103,
                                    'reason' => $inquiryCetakEmas
                                ), 200);
                            }
                        } else {
                            $this->set_response(array(
                                'status' => 'error',
                                'message' => 'Transaksi tidak bisa dilanjutkan. Anda harus memiliki buku tabungan. Hubungi cabang tempat Anda buka rekening tabungan emas.',
                                'code' => 102,
                                'reason' => $detailTabunganEmas
                            ), 200);
                        }
                    } else {
                        $this->set_response(array(
                            'status' => 'error',
                            'message' => 'Terjadi kesalahan. Mohon coba beberapa saat lagi',
                            'code' => 103,
                            'reason' => $detailTabunganEmas
                        ), 200);
                    }
                }
            }
        } else {
            $this->errorUnAuthorized();
        }
    }

    function gadai_post($method = 'inquiry')
    {
        $token = $this->getToken();
        if ($token) {
            $user = $this->User->getUser($token->id);
            if ($user->aktifasiTransFinansial == '0' || $user->aktifasiTransFinansial == '2') {
                $this->set_response([
                    'status' => 'error',
                    'message' => $this->getAktifasiFinansialMsg($user->aktifasiTransFinansial),
                    'code' => 201
                ], 200);
                return;
            }

            if ($method == 'payment') {
                $setData = [
                    'id_transaksi' => $this->post('id_transaksi'),
                    'pin' => $this->post('pin')
                ];
           
                $this->form_validation->set_data($setData);

                $this->form_validation->set_rules('id_transaksi', 'id_transaksi', 'required');
                $this->form_validation->set_rules('pin', 'pin', 'required|numeric');

                //Jika payment adalah wallet dan user belum akttifasi wallet
                if ($user->norek == '') {
                    $this->set_response(array(
                       'status' => 'error',
                       'message' => 'Anda belum melakukan aktifasi transaksi finansial',
                       'code' => 101
                    ), 200);
                    return;
                }

                if ($this->form_validation->run() == false) {
                    $this->set_response([
                        'status' => 'error',
                        'message' => 'Invalid input',
                        'code' => 101,
                        'errors' => $this->form_validation->error_array()
                    ], 200);
                } else {
                    $idTransaksi = $this->post('id_transaksi');
               
                    //Mendapatkan payment berdasarkan id transaksi
                    $checkPayment = $this->EmasModel->getGTE($idTransaksi);

                    if ($checkPayment) {
                        $pin = $this->post('pin');
                         
                        //Check user pin
                        if (!$this->User->isValidPIN2($token->id, $pin)) {
                             $this->set_response([
                                 'status' => 'error',
                                 'message' => 'PIN tidak valid',
                                 'code' => 102
                             ], 200);
                             return;
                        }
                         
                        $biayaTransaksi = $this->ConfigModel->getRealBiayaPayment('OP', 'WALLET', '32', '');
                        
                        $paymentData = [
                            "channelId" => $token->channelId,
                            "cif" => $user->cif,
                            "gram" => $checkPayment->gram,
                            "jenisTransaksi" => "OP",
                            "noKredit" => $checkPayment->noKredit,
                            "norekBankTujuan" => $checkPayment->norekBankTujuan,
                            "tenor" => $checkPayment->tenor,
                            "tipeTransaksi" =>  $checkPayment->tipeTransaksi,
                            "up" => $checkPayment->up,
                            "administrasi" => $checkPayment->administrasi,
                            "surcharge" => $checkPayment->surcharge,
                            "totalKewajiban" => $checkPayment->totalKewajiban,
                            "paymentMethod" => 'WALLET',
                            "walletId" => $user->no_hp,
                            "norekWallet" => $user->norek,
                            "reffSwitching" => $checkPayment->reffSwitching,
                            "reffBiller" => $checkPayment->id,
                            "requestType" => $checkPayment->requestType,
                            "productCode" => $checkPayment->productCode,
                            'jenisPromo' => $checkPayment->jenisPromo
                        ];
                        
                        // Get promo (if Any)
                        $gpoin = $this->GpoinModel->get_promo($checkPayment->reffSwitching);
                        $idPromosi = '';
                        $discountAmount = 0;
                        $promoAmount = 0;
                        if ($gpoin != '') {
                            if ($gpoin->type == 'discount') {
                                $discountAmount = $gpoin->value;
                            }
                            $promoAmount = $gpoin->value;
                            $idPromosi = $gpoin->idPromosi;
                        };
                        $paymentData['idPromosi'] = $idPromosi;
                        $paymentData['discountAmount'] = $promoAmount;

                        if ($checkPayment->gcashIdTujuan != null) {
                            // Pembayaran dengan GCASH
                            $paymentData['gcashIdTujuan'] = $checkPayment->gcashIdTujuan;
                        } else {
                            // Pembayaran dengan WALLET
                            $paymentData["kodeBankTujuan"] = $checkPayment->kodeBankTujuan;
                            $paymentData["namaBankTujuan"] = $checkPayment->namaBankTujuan;
                        }

                         $walletPayment = $this->openGTE($paymentData);

                        if ($walletPayment->responseCode == '00') {
                            $walletData = json_decode($walletPayment->data, true);

                            $walletData['isPaid'] = '1';
                            $walletData['tenor'] = filter_var($walletData['tenor'], FILTER_SANITIZE_NUMBER_INT);

                            $this->EmasModel->updateGTE($idTransaksi, $walletData);

                            $walletData['nama'] = $user->nama;
                            $walletData['gram'] = $checkPayment->gram;

                            $saldo = $this->getSaldo($checkPayment->noKredit);
                            $walletData['saldo'] = $saldo['saldo'];
                            $walletData['noKredit'] = $checkPayment->noKredit;
                            $walletData['gcashIdTujuan'] = $checkPayment->gcashIdTujuan;


                            $template = $this->generateGTENotif($walletData);

                            $mobileTemplate = $template['mobile'];
                            $emailTemplate = $template['email'];
                            $minimalTemplate = $template['minimal'];

                            //Simpan notifikasi baru
                            $notifId = $this->NotificationModel->add(
                                $token->id,
                                NotificationModel::TYPE_GADAI,
                                NotificationModel::CONTENT_TYPE_HTML,
                                $this->ConfigModel->getNamaProduk('OP', '32'),
                                "Gadai Tabungan Emas",
                                $mobileTemplate,
                                $minimalTemplate,
                                "OP"
                            );

                            //Kirim notifikasi pembayaran ke device user
                            Message::sendFCMNotif(
                                $this->User->getFCMToken($token->id),
                                [
                                "id" => $notifId,
                                "tipe" => "GT",
                                "title" => $this->ConfigModel->getNamaProduk('OP', '32'),
                                "tagline" => "Gadai Tabungan Emas Berhasil",
                                "content" => "Gadai Tabungan Emas Berhasil",
                                "paymentType" => 'WALLET',
                                "token" => $token->no_hp
                                    ]
                            );

                            //Kirim Email Notifikasi
                            $this->load->helper('message');
                            Message::sendEmail(
                                $token->email,
                                'Gadai Tabungan Emas',
                                $emailTemplate
                            );

                            //Update Saldo Wallet
                            $saldoWallet = $this->updateWallet($token->channelId, $token->id, $user->no_hp);
                            $walletData['wallet'] = $saldoWallet;

                            $walletData['saldoBlokir'] = $saldo['saldoBlokir'];
                            $walletData['saldoAkhir'] = $saldo['saldoAkhir'];
                            $walletData['saldoEfektif'] = $saldo['saldoEfektif'];


                            //Set response
                            $this->set_response([
                                'status' => 'success',
                                'message' => 'Gadai  Tabungan Emas Berhasil',
                                'data' => $walletData
                            ], 200);
                        } else {
                            $this->set_response(array(
                                'status' => 'error',
                                'message' => '',
                                'code' => 103,
                                'reason' => $walletPayment
                            ), 200);
                        }
                    } else {
                        log_message('debug', 'Payment not found'.$this->post('id_transaksi'));
                        $this->set_response(array(
                            'status' => 'success',
                            'message' => 'Payment not found',
                            'code' => 102
                        ), 200);
                    }
                }
            } elseif ($method == 'inquiry') {
                $setData = array(
                    'payment' => $this->post('payment'),
                    'norek' => $this->post('norek'),
                    'gram' => $this->post('gram'),
                    'id_bank' => $this->post('id_bank'),
                    'tenor' => $this->post('tenor'),
                    'up' => $this->post('up'),
                    'tipe' => $this->post('tipe'),
                    'va' => $this->post('va')
                );
                
                $this->form_validation->set_data($setData);
                $this->form_validation->set_rules('norek', 'norek', 'required|exact_length[16]|numeric');
                $this->form_validation->set_rules('gram', 'vendor', 'required');
                $this->form_validation->set_rules('tenor', 'tenor', 'required|integer');
                $this->form_validation->set_rules('up', 'up', 'required|integer');
                $this->form_validation->set_rules('tipe', 'tipe', 'required|integer');
                
                if ($this->post('payment') != 'GCASH') {
                    $this->form_validation->set_rules('id_bank', 'id_bank', 'required|integer');
                }

                if ($this->post('payment') == 'GCASH') {
                    $this->form_validation->set_rules('va', 'va', 'required|numeric|required');
                }
                
                if ($this->form_validation->run() == false) {
                    $this->set_response([
                        'status' => 'error',
                        'code' => 101,
                        'message' => 'Invalid input',
                        'errors' => $this->form_validation->error_array()
                    ]);
                } else {
                    $norek = $this->post('norek');
                    $gram = $this->post('gram');
                    $id_bank = $this->post('id_bank');
                    $tenor = $this->post('tenor');
                    $up = $this->post('up');
                    $tipe = $this->post('tipe');
                    $productCode = $this->post('product_code') == null ? '32' : $this->post('product_code');
                    $jenisPromo = $this->post('jenis_promo');
                    $va = $this->post('va');
                    $payment = $this->post('payment');

                    $dataInquiry = [
                        'channelId' => $token->channelId,
                        'cif' => $user->cif,
                        'gram' => $gram,
                        'jenisTransaksi' => 'OP',
                        'noKredit' => $norek,
                        'tenor' => $tenor,
                        'up' => $up,
                        'requestType' => $tipe,
                        'productCode' => $productCode,
                    ];

                    if ($payment == null || $payment == 'BANK') {
                        //mendapatkan detail bank sesuai id bank
                        $userBank = $this->BankModel->getById($id_bank);
                        
                        if (!$userBank) {
                            $this->set_response([
                                'status' => 'error',
                                'message' => 'Rekening bank tidak valid',
                                'code' => 101
                            ], 200);
                            return;
                        }

                        $dataInquiry['tipeTransaksi'] = '2';
                        $dataInquiry['kodeBankTujuan'] = $userBank->kode_bank;
                        $dataInquiry['namaBankTujuan'] = $userBank->nama_pemilik;
                        $dataInquiry['norekBankTujuan'] = $userBank->nomor_rekening;
                    } elseif ($payment == 'GCASH') {
                        // CEK USER WALLET
                        $dataInquiry['tipeTransaksi'] = '3';
                        $dataInquiry['gcashIdTujuan'] = $va;
                    }
                    
                    
                    if ($jenisPromo != null) {
                        $dataInquiry['jenisPromo'] = strtoupper($jenisPromo);
                    }

                    $inquiryGTE = $this->inquiryGTE($dataInquiry);

                    if ($inquiryGTE->responseCode == '00') {
                        if (!isset($inquiryGTE->data)) {
                            $this->set_response([
                                'status' => 'error',
                                'message' => 'Terjadi kesalahan. Mohon coba beberapa saat lagi',
                                'code' => 103,
                                'reason' => $inquiryGTE
                            ], 200);
                            return;
                        }

                        $saveData = json_decode($inquiryGTE->data, true);
                        $saveData['user_AIID'] = $token->id;
                        $saveData['requestType'] = $tipe;
                        $saveData['productCode'] = $productCode;
                        $saveData['jenisPromo'] = $jenisPromo;

                        $this->EmasModel->saveGTE($saveData);

                        $this->set_response([
                            'status' => 'success',
                            'message' => '',
                            'data' => $saveData
                        ], 200);
                    } else {
                        if ($inquiryGTE->responseCode == '15') {
                            $this->send_response('error', 'Kamu belum melakukan registrasi Gadai Tabungan Emas, silahkan melakukan registrasi di cabang Pegadaian terdekat.', '', 103);
                            return;
                        }

                        $this->set_response([
                            'status' => 'error',
                            'message' => 'Terjadi kesalahan. Mohon coba beberapa saat lagi',
                            'code' => 103,
                            'reason' => $inquiryGTE
                        ], 200);
                    }
                }
            }
        } else {
            $this->errorUnAuthorized();
        }
    }

    function validate_payment_emas_post()
    {
        $token = $this->getToken();

        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', __FUNCTION__ . ' Validasi Pembayaran Emas Error ' . json_decode($this->errorUnAuthorized()));
            return;
        }

        $this->form_validation->set_rules('id_transaksi', 'id_transaksi', 'required');
        $this->form_validation->set_rules('payment', 'payment', 'required');
    
        if ($this->form_validation->run() == false) {
            return $this->send_response('error', 'Invalid Input', $this->form_validation->error_array(), 101);
        }

        $id_transaksi = $this->post('id_transaksi');

        $response = $this->Emas_service->validatePaymentGcash($token, $id_transaksi);
        
        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    /**
     * Endpoint untuk mendapatkan biaya
     */
    function minimal_transaksi_get($param = 'saldo')
    {
        
        $token = $this->getToken();
        
        if ($token) {
            $minBeli = 0.001;
            $minSaldo = 0.01;
            $minJual = 1;
            $minCetak = 1;
            $minTransfer = 1;
            
            $data['channelId'] = $token->channelId;
            $data['flag'] = "K";
            if ($param == 'saldo') {
                $data['key'] = 'EMAS.MINIMAL.SALDO';
            } elseif ($param == 'beli') {
                $data['key'] = '62.1.MIN.SALE';
            } elseif ($param == 'jual') {
                $data['key'] = '62.1.MIN.BUY';
            } elseif ($param == 'transfer') {
                $data['key'] = '62.1.MIN.TRANSFER';
            } elseif ($param == 'cetak') {
                $data['key'] = '62.1.MIN.ORDER';
            } else {
                 $this->set_response(array(
                    'status' => 'error',
                    'message' => 'Parameter tidak cocok',
                    'code' => 102
                        ), 200);
                return;
            }
            
            $inquiry = $this->systemParameter($data);
            if ($inquiry->responseCode == '00') {
                if (!isset($inquiry->data)) {
                    $this->set_response(array(
                        'status' => 'error',
                        'message' => 'Terjadi kesalahan mohon coba beberapa saat lagi',
                        'code' => 103,
                        'reason' => $inquiry
                            ), 200);
                    return;
                }
                
                $inquiryData = json_decode($inquiry->data);
                $this->set_response(array(
                    'status' => 'success',
                    'message' => '',
                    'data' => number_format($inquiryData->value, 4, ".", ",")
                ), 200);
            } else {
                $this->set_response(array(
                    'status' => 'error',
                    'message' => 'Terjadi kesalahan mohon coba beberapa saat lagi',
                    'code' => 103,
                    'reason' => $inquiry
                ), 200);
            }
        } else {
            $this->errorUnAuthorized();
        }
    }

    function simulasi_emas_post()
    {
        log_message('Debug', 'Start Of ' . __FUNCTION__ . '=>' . json_encode($this->post()));
        $token = $this->getToken();
        
        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', 'response ' . __FUNCTION__ . 'UnAuthorized');
            return;
        }

        $this->form_validation->set_rules('amount', 'amount', 'callback__validation_beliEmasAmount');
        $this->form_validation->set_rules('gram', 'gram', 'callback__validation_beliEmasGram');
        $this->form_validation->set_rules('norek', 'norek', 'callback__validation_noRekeningEmas');

        if (!$this->form_validation->run()) {
            $validation = Pegadaian::showErrorValidation($this->form_validation->error_array());
            log_message('debug', 'END ' . __FUNCTION__ . ' => ' . json_encode($validation));
            return $this->send_response('error', 'Validation Exception', $validation, '101');
        }

        //array inputan
        $data = $this->post();

        //get productCode
        $productCode = $this->ProductMasterModel->get_productName('TABUNGAN EMAS');
        $data['productCode'] = $productCode->productCode;

        //getCif
        $data['cif'] = $this->User->getUserCif($token->id);
        $data['jenisTransaksi'] = "OP";

        // Jika gram == 0 maka false errornya "Format Data field tipe tidak benar ",,
        if ($data['gram'] != "0") {
            $data['tipe'] = "1";
        }
        if ($data['amount'] != "0") {
            $data['tipe'] = "2";
        }

        $req = $this->simulasiTE($data);

        // success Jika responeCode Sama dengan 00
        if ($req->responseCode == "00") {
            return $this->send_response('success', $req->responseDesc, json_decode($req->data), $req->responseCode);

            log_message('debug', 'response ' . __FUNCTION__ . ' Message : ' .  json_decode($req->data));
        }

        //error jika responseCode Tidak sama dengan 00
        log_message('debug', 'response ' . __FUNCTION__ . ' Message : ' . !empty($req->data) ? json_decode($req->data) : '');
        return $this->send_response('error', $req->responseDesc, !empty($req->data) ? json_decode($req->data) : '', $req->responseCode);
    }

    function simulasi_jualemas_post()
    {
        log_message('Debug', 'Start Of ' . __FUNCTION__ . '=>' . json_encode($this->post()));
        $token = $this->getToken();
        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', 'response ' . __FUNCTION__ . 'UnAuthorized');
            return;
        }

        $this->form_validation->set_rules('amount', 'amount', 'callback__validation_jualEmas['.$this->post('norek').']');
        $this->form_validation->set_rules('norek', 'norek', 'callback__validation_noRekeningEmas');

        if (!$this->form_validation->run()) {
            $validation = Pegadaian::showErrorValidation($this->form_validation->error_array());
            log_message('debug', 'END ' . __FUNCTION__ . ' => ' . json_encode($validation));
            return $this->send_response('error', 'Validation Exception', $validation, '101');
        }

        //Menapung data untuk ditampung ke service
        $response = $this->Emas_service->simulasiTabemas($this->post());

        return $this->send_response($response['status'], $response['message'], $response['data'], $response['code']);
    }


    function generateBuybackNotif($trxId)
    {
        $checkPayment = $this->PaymentModel->getPaymentByTrxId2($trxId, true);
        
        if ($checkPayment['payment'] == 'BANK') {
            $this->load->model('BankModel');
            $checkPayment['namaBank'] = $this->BankModel->getNamaBank($checkPayment['kodeBankTujuan']);
        }
        
        $subject = "Jual Tabungan Emas ".$checkPayment['norek']." Berhasil";
                
        $content = $this->load->view('mail/notif_buybackemas', $checkPayment, true);
        
        $email = $this->load->view('mail/email_template_top', array('title'=>$subject), true);
        $email = $email.$content;
        $email = $email.$this->load->view('mail/email_template_bottom', array(), true);
        
        $mobile = $this->load->view('notification/top_template', array('title'=>$subject), true);
        $mobile = $mobile.$content;
        $mobile = $mobile.$this->load->view('notification/bottom_template', array(), true);
        
        
        return array(
            'email' => $email,
            'mobile' => $mobile,
            'minimal' => $content
        );
    }

    function generateGTENotif($checkPayment)
    {
        
        $subject = "Gadai Tabungan Emas ".$checkPayment['norek']." Berhasil";
        
        if ($checkPayment['gcashIdTujuan'] == null) {
            $this->load->model('BankModel');
            $namaBank = $this->BankModel->getNamaBank($checkPayment['kodeBankTujuan']);
            $checkPayment['namaBank'] = $namaBank;
        }
                
        $content = $this->load->view('mail/notif_gte', $checkPayment, true);
        
        $email = $this->load->view('mail/email_template_top', array('title'=>$subject), true);
        $email = $email.$content;
        $email = $email.$this->load->view('mail/email_template_bottom', array(), true);
        
        $mobile = $this->load->view('notification/top_template', array('title'=>$subject), true);
        $mobile = $mobile.$content;
        $mobile = $mobile.$this->load->view('notification/bottom_template', array(), true);
        
        
        return array(
            'email' => $email,
            'mobile' => $mobile,
            'minimal' => $content
        );
    }
    
    function generateCetakEmasNotif($trxId)
    {
        $checkPayment = $this->PaymentModel->getPaymentByTrxId2($trxId, true);
        
        $subject = "";
        
        if ($checkPayment['payment'] == 'BNI' ||
                $checkPayment['payment'] == 'VA_BCA' ||
                $checkPayment['payment'] == 'VA_MANDIRI' ||
                $checkPayment['payment'] == 'VA_BRI'
        ) {
            $subject = "Konfirmasi Order Cetak Tabungan Emas Pegadaian ".$checkPayment['norek'];
        } elseif ($checkPayment['payment'] == 'WALLET' || $checkPayment['payment'] == 'GCASH') {
            $subject = "Selamat Order Cetak Tabungan Emas Berhasil";
            $this->load->model('MasterModel');
            $cabang = $this->MasterModel->getSingleCabang($checkPayment['kodeCabang']);
            $checkPayment['namaCabang'] = $cabang->nama;
            $checkPayment['alamatCabang'] = $cabang->alamat.', '.$cabang->kelurahan.', '.$cabang->kecamatan.', '.$cabang->kabupaten.', '.$cabang->provinsi;
            $checkPayment['noTelpCabang'] = $cabang->telepon;
        }
        
        $content = $this->load->view('mail/notif_cetakemas', $checkPayment, true);
        
        $email = $this->load->view('mail/email_template_top', array('title'=>$subject), true);
        $email = $email.$content;
        $email = $email.$this->load->view('mail/email_template_bottom', array(), true);
        
        $mobile = $this->load->view('notification/top_template', array('title'=>$subject), true);
        $mobile = $mobile.$content;
        $mobile = $mobile.$this->load->view('notification/bottom_template', array(), true);
        
        
        return array(
            'email' => $email,
            'mobile' => $mobile,
            'minimal' => $content
        );
    }
    
    
    
    function generateTransferEmasNotif($trxId, $isRekTujuan = false, $nasabahTujuan = null)
    {
        $checkPayment = $this->PaymentModel->getPaymentByTrxId2($trxId, true);
        $subject = "";
        
        if ($checkPayment['payment'] == 'BNI') {
            $subject = "Konfirmasi Transfer Tabungan Emas Pegadaian ".$checkPayment['norek'];
        } elseif ($checkPayment['payment'] == 'WALLET') {
            $subject = "Selamat Transfer Keluar Tabungan Emas Berhasil";
        }
        
        $content = "";
        if ($isRekTujuan) {
            $subject = "Selamat Transfer Masuk Tabungan Emas Berhasil";
            $checkPayment['nasabahTujuan'] = $nasabahTujuan;
            $content = $this->load->view('mail/notif_transferemas_tujuan', $checkPayment, true);
        } else {
            $content = $this->load->view('mail/notif_transferemas', $checkPayment, true);
        }
        
        
        $email = $this->load->view('mail/email_template_top', array('title'=>$subject), true);
        $email = $email.$content;
        $email = $email.$this->load->view('mail/email_template_bottom', array(), true);
        
        $mobile = $this->load->view('notification/top_template', array('title'=>$subject), true);
        $mobile = $mobile.$content;
        $mobile = $mobile.$this->load->view('notification/bottom_template', array(), true);
        
        
        return array(
            'email' => $email,
            'mobile' => $mobile,
            'minimal' => $content
        );
    }

    function getAktifasiFinansialMsg($status)
    {
        if ($status == '0') {
            return 'Mohon maaf, Anda tidak dapat melanjutkan transaksi. Status Transaksi Finansial: Tidak Aktif. Informasi lebih lanjut, silakan datang ke Kantor Pegadaian terdekat atau hubungi call center';
        } elseif ($status=='2') {
            return 'Mohon maaf, Anda tidak dapat melanjutkan transaksi. Status Transaksi Finansial: Blokir. Informasi lebih lanjut, silakan datang ke Kantor Pegadaian terdekat atau hubungi call center';
        }
    }


    function _validation_noRekeningEmas($noRekEmas)
    {

        if (empty($noRekEmas)) {
            $this->form_validation->set_message('_validation_noRekeningEmas', 'Anda belum memilih nomor rekening.');
            return false;
        }

        if (strlen($noRekEmas) < 16) {
            $this->form_validation->set_message('_validation_noRekeningEmas', 'Nomor rekening harus terdapat minimal 16 digit');
            return false;
        }

        if (!is_numeric($noRekEmas)) {
            $this->form_validation->set_message('_validation_noRekeningEmas', 'Nomor rekening harus diisi dengan angka');
            return false;
        }

        return true;
    }

    function _validation_noRekEmasTujuan($noRekEmasTujuan, $noRekEmas)
    {

        if (empty($noRekEmasTujuan)) {
            $this->form_validation->set_message('_validation_noRekEmasTujuan', 'Pilih salah satu nomor rekening tujuan.');
            return false;
        }

        if (strlen($noRekEmasTujuan) < 16) {
            $this->form_validation->set_message('_validation_noRekEmasTujuan', 'Nomor rekening tujuan harus terdapat minimal 16 digit.');
            return false;
        }

        if (!is_numeric($noRekEmasTujuan)) {
            $this->form_validation->set_message('_validation_noRekEmasTujuan', 'Nomor rekening tujuan harus diisi dengan angka.');
            return false;
        }

        //cek apakah norek == noreTujuan
        if ($noRekEmasTujuan == $noRekEmas) {
            $this->form_validation->set_message('_validation_noRekEmasTujuan', 'Tidak dapat melakukan transfer ke no rekening yang sama');
            return false;
        }

        return true;
    }

    function _validation_jualEmas($jualEmas, $norek)
    {
        //get data min dan max dari Master Model
        $min_jual_gram = $this->MasterModel->getHargaJualEmas('Min_Jual_Emas_Gram');
        $max_jual_gram = $this->MasterModel->getHargaJualEmas('Max_Jual_Emas_Gram');
        $jual_min_sisa_saldo = $this->MasterModel->getHargaJualEmas('jual_min_sisa_saldo');
        
        if (empty($jualEmas) || $jualEmas == "0") {
            $this->form_validation->set_message('_validation_jualEmas', 'Input gram tidak boleh kosong.');
            return false;
        }

        if (!is_numeric($jualEmas)) {
            $this->form_validation->set_message('_validation_jualEmas', 'Input gram harus diisi dengan angka');
            return false;
        }

        // Validasi Min gram
        if ($jualEmas < $min_jual_gram && $jualEmas != '0') {
            $this->form_validation->set_message('_validation_jualEmas', 'Minimal jumlah jual Tabungan Emas ' . $min_jual_gram . ' gram/Transaksi');
            return false;
        }

        // Validasi Max gram
        if ($jualEmas > $max_jual_gram) {
            $this->form_validation->set_message('_validation_jualEmas', 'Maksimal jumlah jual Tabungan Emas ' . $max_jual_gram . ' gram/Transaksi');
            return false;
        }

        //get Rekening berdasarkan Cif
        $daftarRekening = $this->detailTabunganEmas($norek);

        if ($daftarRekening->responseCode == '12') {
            $this->form_validation->set_message('_validation_jualEmas', 'Saldo emas tidak mencukupi.');
            return false;
        }
        
        //get Saldo efektif dari rekening
        $saldoEfektif = json_decode($daftarRekening->data)->saldoEfektif ?? '';

        // Validasi Ketika Inputan Lebih besar dari Saldo Rekening Emas
        if ($jualEmas > $saldoEfektif) {
            $this->form_validation->set_message('_validation_jualEmas', 'Saldo emas anda tidak cukup');
            return false;
        }

        $hitung = $saldoEfektif - $jualEmas;
        
        //  Validasi Ketika Saldo Rekening Emas Kurang
        if ($hitung < $jual_min_sisa_saldo) {
            $this->form_validation->set_message('_validation_jualEmas', 'Saldo mengendap minimal ' . $jual_min_sisa_saldo . ' gram');
            return false;
        }

        return true;
    }
    
    function _validation_beliEmasGram($beliEmasGram)
    {
        $min_topup_gram = $this->MasterModel->getHargaBeliEmas('Min_Beli_Emas_Gram');
        $max_topup_gram = $this->MasterModel->getHargaBeliEmas('Max_Beli_Emas_Gram');

        if (empty($beliEmasGram) && !is_numeric($beliEmasGram)) {
            $this->form_validation->set_message('_validation_beliEmasGram', 'Input gram tidak boleh kosong.');
            return false;
        }

        // Validasi min gram
        if ($beliEmasGram < $min_topup_gram && $beliEmasGram != "0") {
            $this->form_validation->set_message('_validation_beliEmasGram', 'Minimal jumlah Top Up Tabungan Emas ' . $min_topup_gram . ' Gram');
            return false;
        }

        // Validasi Max gram
        if ($beliEmasGram > $max_topup_gram) {
            $this->form_validation->set_message('_validation_beliEmasGram', 'Maksimal jumlah Top Up Tabungan Emas ' . $max_topup_gram . ' Gram');
            return false;
        }

        return true;
    }

    function _validation_beliEmasAmount($beliEmasAmount)
    {
        $min_topup_amount = $this->MasterModel->getHargaBeliEmas('Min_Beli_Emas');
        $max_topup_amount = $this->MasterModel->getHargaBeliEmas('Max_Beli_Emas');

        if (empty($beliEmasAmount) && !is_numeric($beliEmasAmount)) {
            $this->form_validation->set_message('_validation_beliEmasAmount', 'Input rupiah tidak boleh kosong.');
            return false;
        }

        //Validasi Min Amount
        if ($beliEmasAmount < $min_topup_amount && $beliEmasAmount != '0') {
            $this->form_validation->set_message('_validation_beliEmasAmount', 'Minimal jumlah Top Up Tabungan Emas Rp.' . number_format($min_topup_amount, 0, ',', '.'));
            return false;
        }

        //Validasi Max Amount
        if ($beliEmasAmount > $max_topup_amount) {
            $this->form_validation->set_message('_validation_beliEmasAmount', 'Maksimal jumlah Top Up Tabungan Emas Rp.' . number_format($max_topup_amount, 0, ',', '.'));
            return false;
        }

        return true;
    }

    function _validation_transferGram($tfGram, $norek)
    {
        $min_transfer_gram = $this->MasterModel->getHargaTransferEmas('Min_Transfer_Emas');
        $max_transfer_gram = $this->MasterModel->getHargaTransferEmas('Max_Transfer_Emas');
        $transfer_min_sisa_saldo = $this->MasterModel->getHargaTransferEmas('Transfer_min_sisa_saldo');

        if (empty($tfGram) || $tfGram == '0') {
            $this->form_validation->set_message('_validation_transferGram', 'Input gram tidak boleh kosong.');
            return false;
        }

        // Validasi Min gram
        if ($tfGram < $min_transfer_gram) {
            $this->form_validation->set_message('_validation_transferGram', 'Minimal jumlah Transfer Emas ' . $min_transfer_gram . ' gram/transaksi');
            return false;
        }

        // Validasi Max gram
        if ($tfGram > $max_transfer_gram) {
            $this->form_validation->set_message('_validation_transferGram', 'Maksimal jumlah Transfer Emas ' . $max_transfer_gram . ' gram/transaksi');
            return false;
        }

        //get Rekening berdasarkan Cif
        $daftarRekening = $this->detailTabunganEmas($norek);

        if ($daftarRekening->responseCode == '12') {
            $this->form_validation->set_message('_validation_transferGram', 'Saldo emas tidak mencukupi.');
            return false;
        }

        //get Saldo efektif dari rekening
        $saldoEfektif = json_decode($daftarRekening->data)->saldoEfektif ?? '';

        // Validasi Ketika Inputan Lebih besar dari Saldo Rekening Emas
        if (empty($saldoEfektif)) {
            $this->form_validation->set_message('_validation_transferGram', 'Saldo emas tidak ditemukan.');
            return false;
        }
        
        // Validasi Ketika Inputan Lebih besar dari Saldo Rekening Emas
        if ($tfGram > $saldoEfektif) {
            $this->form_validation->set_message('_validation_transferGram', 'Saldo emas tidak mencukupi.');
            return false;
        }

        $hitung = $saldoEfektif - $tfGram;

        //  Validasi ketika Saldo Emas harus mengendap minimal 0.1
        if ($hitung < $transfer_min_sisa_saldo) {
            $this->form_validation->set_message('_validation_transferGram', 'Saldo mengendap minimal ' . $transfer_min_sisa_saldo . ' gram.');
            return false;
        }

        return true;
    }
}
