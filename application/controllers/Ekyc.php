<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once 'CorePegadaian.php';
require_once APPPATH . 'helpers/message_helper.php';

use \Curl\Curl;

/**
 * @property Ekyc_service ekyc_service
 */
class Ekyc extends CorePegadaian
{
    // ************ NOTES ************ //
    // status E-KYC:
    // - Belum E-KYC = null
    // - Sedang diproses = 1
    // - Disetujui (sudah valid) = 2
    // - Ditolak = 3
    // - Banned = 4
    // ******************************* //

    // ************ NOTES ************ //
    // account_type E-KYC:
    // - Registered
    // - Standard
    // - Premium
    // ******************************* //
    public function __construct()
    {
        parent::__construct();
        // init load model
        $this->load->model('User');
        $this->load->model('EkycModel');
        $this->load->model('EkycSelfieModel');
        $this->load->model('NotificationModel');
        $this->load->model('MasterModel');
        // init load library
        $this->load->library('form_validation');
        // init load service
        $this->load->service('Ekyc_service', 'ekyc_service');
        $this->load->service('RestSwitchingPortofolio_service');
        $this->load->service('RestSwitchingCustomer_service');
    }

    function ekyc_ktp_post()
    {
        $token = $this->getToken();

        if (!$token) {
            return $this->errorUnAuthorized();
        }

        $uploadDir = $this->config->item('upload_dir');
        $coeUrl = $this->config->item('coe_api_url');

        $dirKtp = $uploadDir . '/ekyc/ktp';
        if (is_dir($dirKtp) === false) {
            mkdir($dirKtp);
        }

        $dirKtpUser = $dirKtp . '/' . $token->id;
        if (is_dir($dirKtpUser) === false) {
            mkdir($dirKtpUser);
        }

        $files = glob($uploadDir . '/ekyc/ktp/' . $token->id . '/*'); // get all file names
        foreach ($files as $file) {
            if (is_file($file)) {
                unlink($file);
            }
        }

        $config['upload_path']          = $dirKtpUser;
        $config['allowed_types']        = 'gif|jpg|png|jpeg|bmp';
        $config['max_size']             = 0;
        $config['max_width']            = 0;
        $config['max_height']           = 0;
        $config['encrypt_name']         = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('ktpfile')) {
            log_message('debug', 'E-KYC Photo KTP Error' . json_encode($this->upload->display_errors()));
            ;
            return $this->send_response('error', $this->upload->display_errors(), '');
        }

        // get image desc
        $data = $this->upload->data();
        // ************ Send File to COE KTP ************ //
        // Tembak ke endpoint OCR KTP CoE /ocr/upload, Request Body:
        $body = array(
            'image_path' => '@' . $data['full_path'],
            'image_type' => '1',
            'phone_number' => '081136123999',
            'client_key' => $this->config->item('CLIENT_KEY_OCR')
        );

        $is_coe_by_pass = $this->config->item('is_coe_bypass');

        if (empty($is_coe_by_pass)) {
            $curl = new Curl();
            $curl->setHeader('Content-Type', 'multipart/form-data');
            $curl->setConnectTimeout(100);
            $curl->setTimeout(100);
            $curl->post($coeUrl . '/upload', $body);
            $ocrResponse = $curl->response;

            if ($curl->error) {
                log_message('debug', 'Error OCR KTP' . json_encode($curl->errorMessage));
                return $this->send_response('error', $curl->errorMessage, '');
            }

            if ($ocrResponse->status == 'ERROR') {
                log_message('debug', 'Error OCR KTP' . json_encode($ocrResponse));
                return $this->send_response('error', $ocrResponse->error->message, '');
            }

            log_message('debug', 'OCR KTP' . json_encode($ocrResponse));

            if (!$ocrResponse->data) {
                log_message('debug', 'Error OCR KTP' . json_encode($ocrResponse));
                return $this->send_response('error', 'Response OCR Data tidak ditemukan', '');
            }
        }

        $ekycField = [
            'image_ktp' => $data['file_name'],
            'account_type' => 'standard',
            'nik' => $ocrResponse->data->nik ?? ''
        ];

        $birth_date = $ocrResponse->data->tanggal_lahir ?? null;
        if (!empty($birth_date)) {
            $birth_date = new DateTime($birth_date);
            $birth_date = $birth_date->format('Y-m-d');
            $ekycField['tanggal_lahir'] = !empty($birth_date) ? $birth_date : null;
        }

        $this->EkycModel->update($token->id, $ekycField);

        return $this->response([
            'status' => 'success',
            'message' => '',
            'data' => $data
        ], 200);
    }

    function ekyc_upsert_screen_post($method = 'update')
    {
        //get Token
        $token = $this->getToken();

        //Token Validation
        if (!$token) {
            $this->errorUnAuthorized();
            return;
        }
        // Set roles parameters
        $this->form_validation->set_rules('screen', 'Screen', 'required');

        // Validation parameters
        if ($this->form_validation->run() == false) {
            $this->set_response(array(
                'status' => 'error',
                'code' => 101,
                'message' => 'Invalid input',
                'errors' => $this->form_validation->error_array()
            ));
            return;
        }

        if ($method == 'insert') {
            // Declare data untuk insert
            $data = [
                'user_id' => $token->id,
                'screen' => $this->post('screen'),
                'created_date' => date('Y-m-d H:i:s')
            ];
            log_message('debug', 'E-KYC Insert screen Start' . json_encode($data));
            // Insert data
            if ($this->EkycModel->getbyUser($token->id) == '') {
                if ($this->EkycModel->insert($data) == 'sukses') {
                    $this->response([
                        'status' => 'success',
                        'message' => 'Insert Data Sukses',
                        'data' => $data
                    ], 200);
                } else {
                    $this->response([
                        'status' => 'error',
                        'message' => 'Insert Data Gagal',
                        'data' => $data
                    ], 200);
                }
            }
        } else {
            // Declare data untuk update
            $data = [
                'screen' => $this->post('screen'),
                'update_date' => date('Y-m-d H:i:s')
            ];
            log_message('debug', 'E-KYC Update screen Start' . json_encode($data));
            // Update data
            if ($this->EkycModel->update($token->id, $data) == 'sukses') {
                $this->response([
                    'status' => 'success',
                    'message' => 'Update Data Sukses',
                    'data' => $data
                ], 200);
            } else {
                $this->response([
                    'status' => 'error',
                    'message' => 'Update Data Gagal',
                    'data' => $data
                ], 200);
            }
        }
    }

    function ekyc_get_result_ktp_get()
    {
        //get Token
        $token = $this->getToken();

        //Token Validation
        if (!$token) {
            $this->errorUnAuthorized();
            return;
        }

        // get dir ekyc & get data ekyc
        $ekycDir = $this->config->item('ekyc_image_path');
        $data = $this->EkycModel->getbyUser($token->id);

        // Data Validation
        if ($data == '') {
            $this->response([
                'status' => 'error',
                'message' => 'Image Tidak Ditemukan'
            ], 200);
            return;
        }
        // generate response jika sukses.
        $response = [
            'KTPImage' => $ekycDir . 'ktp/' . $token->id . '/' . $data->image_ktp,
            'KTPNo' => $data->nik,
            'KTPTanggalLahir' => !empty($data->tanggal_lahir) ? date('d-m-Y', strtotime($data->tanggal_lahir)) : ''
        ];
        $this->response([
            'status' => 'success',
            'message' => '',
            'data' => $response
        ], 200);
    }

    function ekyc_ktp_check_post($method = 'update')
    {
        //get Token
        $token = $this->getToken();

        //Token Validation
        if ($token) {
            // Set roles parameters
            $this->form_validation->set_rules('nama_ibu', 'Nama Ibu', 'required');
            $this->form_validation->set_rules('nik', 'NIK', 'required');
            $this->form_validation->set_rules('tgl_lahir', 'Tanggal Lahir', 'required');

            if ($this->form_validation->run() == false) {
                foreach ($this->form_validation->error_array() as $key => $value) {
                    $error = $value;
                    break;
                }

                $this->set_response([
                    'status' => 'error',
                    'message' => $error,
                    'data' => ''
                ]);
            } else {
                log_message('debug', 'E-KYC check KTP Body' . json_encode($this->post()));
                // Check NIK & Nama Ibu
                // Harusnya melakukan pengecekan ke Dukcapil tp karena belum bisa tembak ke dukcapil jadi masih ambil sample dari database dulu
                $nik = $this->post('nik');
                $namaIbu = $this->post('nama_ibu');
                $tglLahir = $this->post('tgl_lahir');

                $user = $this->User->getUser($token->id);
                $ekyc = $this->EkycModel->getbyUser($token->id);
                $isSukses = false;

                // Check data user dan ekyc harus ada
                if ((isset($user)) && (isset($ekyc))) {
                    // Count Error
                    $errorCount = $ekyc->error_count + 1;
                    if ($errorCount > 2) { // kalau error ke 3 kali langsung di banned
                        $field = [
                            'status' =>  4, //Banned
                            'error_count' => $errorCount
                        ];
                    } else {
                        $field = [
                            'error_count' => $errorCount,
                        ];
                    }

                    // Set response
                    $response = [
                        'nik' => $user->no_ktp,
                        'errorCount' => $ekyc->error_count,
                        'status' => $ekyc->status
                    ];

                    // Check NIK
                    if ($nik != $user->no_ktp) {
                        // update status dan error_count
                        $this->EkycModel->update($token->id, $field);
                        $this->set_response([
                            'status' => 'error',
                            'message' => 'NIK Tidak ditemukan, Mohon di cek kembali',
                            'data' => $response
                        ]);
                        return;
                    } else {
                        $isSukses = true;
                    };

                    // Check Tanggal Lahir
                    if ($tglLahir != $user->tgl_lahir) {
                        // update status dan error_count
                        $this->EkycModel->update($token->id, $field);
                        $this->set_response([
                            'status' => 'error',
                            'message' => 'Tanggal lahir Tidak Sesuai, Mohon di cek kembali',
                            'data' => $response
                        ]);
                        return;
                    } else {
                        $isSukses = true;
                    };

                    // Check Nama Ibu
                    if (strtolower($namaIbu) != strtolower($user->nama_ibu)) {
                        // update status dan error_count
                        $this->EkycModel->update($token->id, $field);
                        $this->set_response([
                            'status' => 'error',
                            'message' => 'Nama Ibu Tidak Sesuai, Mohon di cek kembali',
                            'data' => $response
                        ]);
                        return;
                    } else {
                        $isSukses = true;
                    };

                    if ($isSukses == true) {
                        // Reset error count jadi '' jika sukses.
                        $fieldEkyc = [
                            'error_count' => '',
                            'account_type' => 'Standard',
                            'nik' => $nik
                        ];
                        $this->EkycModel->update($token->id, $fieldEkyc);

                        $fieldUser = [
                            'no_ktp' => $nik,
                            'nama_ibu' => strtoupper($namaIbu),
                            'tgl_lahir' => $tglLahir
                        ];
                        $this->user->updateUser($token->id, $fieldUser);

                        $this->set_response([
                            'status' => 'success',
                            'message' => 'NIK dan Nama Ibu Sesuai, Terimakasih',
                            'data' => $response
                        ]);
                        return;
                    }
                } else {
                    $this->set_response([
                        'status' => 'error',
                        'message' => 'Data tidak ditemukan',
                        'data' => ''
                    ]);
                }
            }
        } else {
            $this->errorUnAuthorized();
        }
    }

    function ekyc_selfie_dukcapil_post()
    {
        $token = $this->getToken();

        if (!$token) {
            return $this->errorUnAuthorized();
        }

        log_message('debug', 'start of ' . __FUNCTION__ . ' => ' . json_encode($this->post()));

        $this->form_validation->set_rules('faceBase64', 'Selfie', 'required');

        if (!$this->form_validation->run()) {
            $error = $this->form_validation->error_array();
            $this->send_response('error', $error[0], '');
        }

        $response = $this->ekyc_service->submitEkycSelfie($this->post(), $token->id);

        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

    // ************ NOTES ************ //
    // account_type E-KYC:
    // - Registered
    // - Standard
    // - Premium
    // ******************************* //
    function ekyc_check_account_get()
    {
        // get Token
        $token = $this->getToken();

        //Token Validation
        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', 'Account Type = Unauthorized');
            return;
        }

        // get User
        $dataUser = $this->User->getUser($token->id);

        // User Validation
        if ($dataUser == false) {
            $this->set_response([
                'status' => 'error',
                'message' => 'Data User tidak ditemukan',
            ]);
            log_message('debug', 'Account Type = Data User tidak ditemukan');
            return;
        }

        // 1. untuk validasi data standard
        // 2. untuk validasi data registered
        // 3. untuk validasi data premium
        // *****Standard***** //
        if (($dataUser->no_ktp != '') && ($dataUser->aktifasiTransFinansial == 0)) {
            $this->send_response('success', '', 'Standard');
            // *****Registered***** //
        } elseif (($dataUser->no_ktp == '') && ($dataUser->aktifasiTransFinansial == 0)) {
            $this->send_response('success', '', 'Registered');
            // *****Premium***** //
        } elseif (($dataUser->no_ktp != '') && ($dataUser->aktifasiTransFinansial == 1)) {
            $this->send_response('success', '', 'Premium');
        } else {
            $this->send_response('error', 'Account type tidak dikenali mohon cek kembali data pada account ini', '');
            log_message('debug', 'Account Type =  Nomor KTP: ' . $dataUser->no_ktp . ' aktifasiTransFinansial : ' . $dataUser->aktifasiTransFinansial);
        }
    }

    // ************ NOTES ************ //
    // status E-KYC:
    // - Belum E-KYC = null
    // - Sedang diproses = 1
    // - Disetujui (sudah valid) = 2
    // - Ditolak = 3
    // - Banned = 4
    // ******************************* //

    function ekyc_check_status_get()
    {
        // get Token
        $token = $this->getToken();

        //Token Validation
        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', 'Check Status = Unauthorized');
            return;
        }

        // get User
        $dataUser = $this->User->getUser($token->id);
        // get Ekyc
        $dataEkyc = $this->EkycModel->getbyUser($token->id);

        // Check ke table user, jika ada dan aktifasi trans finansialnya sudah 1, maka dia di anggap data ekyc sudah di setujui.
        // jika ada di table user tetapi aktifasi trans finansialnya belum 1, maka akan di cek ke table EKYC, jika tidak ada data,
        // maka akan di anggap belum melakukan ekyc, jika ada akan di cek statusnya satu per satu.
        if ($dataUser->aktifasiTransFinansial == 1) {
            $this->send_response('success', 'Disetujui', 2);
        } else {
            if ($dataEkyc == '') {
                $this->send_response('success', 'Belum E-KYC', '');
            } else {
                switch ($dataEkyc->status) {
                    case 1:
                        $this->send_response('success', 'Sedang diproses', $dataEkyc->status);
                        break;
                    case 2:
                        $this->send_response('success', 'Disetujui', $dataEkyc->status);
                        break;
                    case 3:
                        $this->send_response('success', 'Ditolak', $dataEkyc->status);
                        break;
                    case 4:
                        $this->send_response('success', 'Banned', $dataEkyc->status);
                        break;
                    default:
                        $this->send_response('success', 'Belum E-KYC', '');
                        break;
                }
            }
        }
    }

    /**
     * Untuk mendapatkan tipe akun, status dan screen.
     */
    function account_get()
    {
        // get token
        $token = $this->getToken();

        // token validation
        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', 'Check Status = Unauthorized');
            return;
        }

        // get data user
        $dataUser = $this->User->getUser($token->id);
        // get data ekyc
        $dataEkyc = $this->EkycModel->getbyUser($token->id);
        // set default ekyc screen
        $ekycScreen = '';
        $account_type = '';

        // user validation
        if ($dataUser == false) {
            $this->set_response([
                'status' => 'error',
                'message' => 'Data User tidak ditemukan',
            ]);
            log_message('debug', 'Account Type = Data User tidak ditemukan');
            return;
        }

        // 1. untuk validasi data standard
        // 2. untuk validasi data registered
        // 3. untuk validasi data premium
        // *****Standard***** //
        $account_type = 'Registered';
        if (($dataUser->no_ktp != '') && ($dataUser->aktifasiTransFinansial == 0)) {
            $account_type = 'Standard';
        } elseif (($dataUser->no_ktp != '') && ($dataUser->aktifasiTransFinansial == 1)) {
            $account_type = 'Premium';
        } else {
            $this->send_response('error', 'Account type tidak dikenali mohon cek kembali data pada account ini', '');
            log_message('debug', 'Account Type =  Nomor KTP: ' . $dataUser->no_ktp . ' aktifasiTransFinansial : ' . $dataUser->aktifasiTransFinansial);
        }

        // Check ke table user, jika ada dan aktifasi trans finansialnya sudah 1, maka dia di anggap data ekyc sudah di setujui.
        // jika ada di table user tetapi aktifasi trans finansialnya belum 1, maka akan di cek ke table EKYC, jika tidak ada data,
        // maka akan di anggap belum melakukan ekyc, jika ada akan di cek statusnya satu per satu.
        if ($dataUser->aktifasiTransFinansial == 1) {
            $status = 'Disetujui';
        } else {
            if ($dataEkyc == '') {
                $status = 'Belum E-KYC';
            } else {
                $ekycScreen = $dataEkyc->screen;
                switch ($dataEkyc->status) {
                    case 1:
                        $status = 'Sedang diproses';
                        break;
                    case 2:
                        $status = 'Disetujui';
                        break;
                    case 3:
                        $status = 'Ditolak';
                        break;
                    case 4:
                        $status = 'Banned';
                        break;
                    default:
                        $status = 'Belum E-KYC';
                        break;
                }
            }
        }

        $response = array(
            'account_type' => $account_type,
            'status'       => $status,
            'screen'       => $ekycScreen
        );

        $this->send_response('success', 'Data ditemukan', $response);
    }

    function ekyc_data_finansial_post()
    {
        // get token
        $token = $this->getToken();

        // token validation
        if (!$token) {
            $this->errorUnAuthorized();
            log_message('debug', 'Data Finansial = Unauthorized');
            return;
        }

        // set request data and rules
        $reqData = array(
            'pendidikan_terakhir' => $this->post('pendidikan_terakhir'),
            'sumber_dana' => $this->post('sumber_dana'),
            'penghasilan' => $this->post('penghasilan'),
            'maksud_dan_tujuan' => $this->post('maksud_dan_tujuan'),
        );

        $this->form_validation->set_data($reqData);

        foreach ($reqData as $key => $value) {
            $this->form_validation->set_rules($key, $key, 'required');
        }

        // check validity request data
        if (!$this->form_validation->run()) {
            $response = [
                'code' => 101,
                'status' => 'error',
                'message' => 'Invalid Input',
                'errors' => $this->form_validation->error_array()
            ];
            $this->response($response, 200);
            log_message('debug', 'response ' . __FUNCTION__ . ' data: ' . json_encode($response));
            return;
        }

        // update data finansial
        if ($this->EkycModel->update($token->id, $reqData) == 'gagal') {
            $response = [
                'code' => 99,
                'status' => 'error',
                'message' => 'Gagal mengupdate',
                'errors' => $this->form_validation->error_array()
            ];
            $this->response($response, 200);
            log_message('debug', 'response ' . __FUNCTION__ . ' data: ' . json_encode($response));
            return;
        }

        // set response
        $response = [
            'status' => 'success',
            'message' => '',
            'data' => ''
        ];
        $this->set_response($response, 200);
        log_message('debug', 'response ' . __FUNCTION__ . ' data: ' . json_encode($response));
    }

    // ******************************************** Admin ******************************************** //
    function verified_dukcapil_post()
    {
        $admin = $this->checkAdminToken($this->post('token'));

        if (!$admin) {
            return $this->errorUnAuthorized();
        }

        $this->form_validation->set_rules('user_AIID', 'user_AIID', 'required');

        if ($this->form_validation->run() == false) {
            foreach ($this->form_validation->error_array() as $key => $value) {
                $error = $value;
                break;
            }

            $this->set_response([
                'status' => 'error',
                'message' => $error,
                'code' => 101,
            ]);
        }
        // get User
        $dataUser = $this->User->getUser($this->post('user_AIID'));
        // get Ekyc
        $dataEkyc = $this->EkycModel->getbyUser($this->post('user_AIID'));

        $body = [
            'cif' => $dataUser->cif,
            'noHp' => $dataUser->no_hp,
            'pendidikan' => $dataEkyc->pendidikan_terakhir ? $dataEkyc->pendidikan_terakhir : '',
            'penghasilan' => $dataEkyc->penghasilan ? $dataEkyc->penghasilan : '',
            'sumberDana' => $dataEkyc->sumber_dana ? $dataEkyc->sumber_dana : '',
            'tujuanPengajuanDana' => $dataEkyc->maksud_dan_tujuan ? $dataEkyc->maksud_dan_tujuan : ''
        ];
        $verifiedDukcapil = $this->verifiedDukcapil($body);
        if ($verifiedDukcapil->responseCode != '00') {
            log_message('debug', 'Verified E-KYC Dukcapil Error' . json_encode($verifiedDukcapil));
            $this->send_response('error', $verifiedDukcapil, '');
            return;
        }

        $data = json_decode($verifiedDukcapil->data);
        if ($data) {
            $userField = [
                'is_dukcapil_verified' => $data->isVerifiedDukcapil
            ];
            $this->User->updateUser($this->post('user_AIID'), $userField);
            log_message('debug', 'Verified E-KYC Dukcapil Success' . json_encode($verifiedDukcapil));
            $this->send_response('success', $verifiedDukcapil->responseDesc, $data);
        }
    }

    // ************ NOTES ************ //
    // status E-KYC:
    // - Belum E-KYC = null
    // - Sedang diproses = 1
    // - Disetujui (sudah valid) = 2
    // - Ditolak = 3
    // - Banned = 4
    // ******************************* //
    function approval_ekyc_post()
    {
        $admin = $this->checkAdminToken($this->post('token'));

        if (!$admin) {
            return $this->errorUnAuthorized();
        }

        log_message('debug', 'Start Endpoint Approval E-KYC' . json_encode($this->post()));

        // set Validation
        $this->form_validation->set_rules('user_AIID', 'user_AIID', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');

        if ($this->form_validation->run() == false) {
            foreach ($this->form_validation->error_array() as $key => $value) {
                $error = $value;
                break;
            }

            return $this->send_response('error', $error ?? null, '', '', 101);
        }

        $user_id     = $this->post('user_AIID');
        $dataUser    = $this->User->getUser($user_id);
        $name        = $dataUser->nama;
        $birth_date  = $dataUser->tgl_lahir;
        $birth_place = $dataUser->tempat_lahir;
        $mother_name = $dataUser->nama_ibu;
        $ekyc        = $this->EkycModel->getbyUser($user_id);
       
        switch ($this->post('status')) {
            case 1:
                $status = 'Sedang diproses';
                break;
            case 2:
                $status = 'Disetujui';
                $userField = [
                    'nama' => $name,
                    'tgl_lahir' => $birth_date,
                    'tempat_lahir' => $birth_place,
                    'nama_ibu' => $mother_name,
                    'aktifasiTransFinansial' => '1',
                    'tanggal_aktifasi_finansial' => date('Y-m-d H:i:s')
                ];

                $ekycField = [
                    'status' => $this->post('status'),
                    'account_type' => 'Premium'
                ];

                $kycMessage = " Status akun kamu telah berhasil di upgrade.";
                $cabang = "Untuk info lebih lanjut, mohon hubungi cabang pegadaian terdekat. Terima kasih.";
                $judul = "Upgrade Akun Berhasil";
                $messagekyc = "Premium";
                break;
            case 3:
                $status    = 'Belum berhasil';
                $ekycField = ['status' => $this->post('status'), 'reason' => $this->post('reason')];
                $userField = ['aktifasiTransFinansial' => '0', 'tanggal_aktifasi_finansial' => ''];

                $kycMessage = " Status akun kamu belum berhasil di upgrade";
                $cabang = "Mohon datang ke cabang Pegadaian terdekat dengan membawa KTP untuk melakukan upgrade akun.";
                $judul = "Upgrade Akun Belum Berhasil";
                $messagekyc = "Standar";
                break;
            case 4:
                $status = 'Banned';
                break;
            default:
                $status = 'Belum E-KYC';
                break;
        }

        $notifData = array(
            'user_id' => $user_id,
            'namaNasabah' => $dataUser->nama,
            'cif' => $dataUser->cif,
            'status' => $status,
            'akun_type' => $messagekyc,
            'pesan' => $kycMessage,
            'pesanCabang' => $cabang,
            'judul' => $judul,
            'tanggal' => date('Y-m-d H:i:s'),
        );

        $template = $this->generateEkycNotif($notifData);
        $mobileTemplate = $template['mobile'];
        $emailTemplate = $template['email'];
        $minimalTemplate = $template['minimal'];

        $idNotif = $this->NotificationModel->add(
            $this->post('user_AIID'),
            NotificationModel::TYPE_EKYC,
            NotificationModel::CONTENT_TYPE_TEXT,
            "Status Akun",
            $judul,
            $mobileTemplate,
            $minimalTemplate,
            "KYC"
        );

        if ($status == 'Disetujui' && !empty($userField)) {
            $aktifasiData = [
                'channelId'  => $this->config->item('core_client_id'),
                'cif'        => $dataUser->cif,
                'token'      => '000000',
                'username'   => $dataUser->no_hp,
                'isEkyc'     => '1',
                'name'       => $name,
                'birthDate'  => $birth_date,
                'birthPlace' => $birth_place,
                'motherName' => $mother_name,
            ];

            log_message('debug', 'Data Approval E-KYC' . json_encode($aktifasiData));

            // Activation Financial
            if (!$ekyc->is_success_activation_customer) {
                $aktifasiData = $this->aktifasiTransaksi($aktifasiData);
                if ($aktifasiData->responseCode != '00') {
                    log_message('debug', 'Approval E-KYC Gagal' . json_encode($aktifasiData));
                    return $this->send_response('error', $aktifasiData->responseDesc, '');
                }
                $this->EkycModel->update($user_id, ['is_success_activation_customer' => true]);
            }

            // Register GTE
            if (!$ekyc->is_success_register_gte) {
                $registerGte = $this->RestSwitchingCustomer_service->customerReggte(['cif' => $dataUser->cif, 'username' => $dataUser->no_hp]);
                if ($registerGte['responseCode'] !== '00') {
                    log_message('debug', 'Approval E-KYC Gagal' . json_encode($registerGte));
                    return $this->send_response('error', $registerGte, '');
                }
                $this->EkycModel->update($user_id, ['is_success_register_gte' => true]);
            }

            Message::sendFCMNotif(
                $this->User->getFCMToken($this->post('user_AIID')),
                [
                    "id" => $idNotif,
                    "tipe" => "EKYCSuccess",
                    "title" => "Pegadaian Digital Service Status Akun",
                    "tagline" => "Status Akun Anda " . $status,
                    "action_url" => "EKYCSuccess",
                    "status" => $status,
                    "message_ekyc" => "Status Terakhir Akun Anda"
                ]
            );

            log_message('debug', 'Approval E-KYC Sukses' . json_encode($aktifasiData));
            log_message('debug', 'Notifikasi Approval E-KYC' . $status);
        }

        if ($status == "Ditolak") {
            Message::sendFCMNotif(
                $this->User->getFCMToken($this->post('user_AIID')),
                [
                    "id" => $idNotif,
                    "tipe" => "EKYCVisitOutlet",
                    "title" => "Pegadaian Digital Service Status Akun",
                    "tagline" => "Status Akun Anda " . $status,
                    "action_url" => "EKYCVisitOutlet",
                    "status" => $status,
                    "message_ekyc" => "Status Terakhir Akun Anda"
                ]
            );

            log_message('debug', 'Notifikasi Approval E-KYC' . $status);
        }

        $this->load->helper('message');
        Message::sendEmailApprovalKYC($dataUser->email, $dataUser->nama, $status, $judul, $kycMessage, $cabang, $emailTemplate);

        // Update User
        if (!empty($userField)) {
            $this->User->updateUser($this->post('user_AIID'), $userField);
        }

        // Update E-KYC
        if (!empty($ekycField)) {
            $this->EkycModel->update($this->post('user_AIID'), $ekycField);
        }

        log_message('debug', 'Approval E-KYC' . $status);

        $this->send_response('success', $status, '');
    }

    function generateEkycNotif($data)
    {
        $notifData = $data["judul"];

        $subject = $notifData;

        $content = $this->load->view('mail/notif_ekyc_success', $data, true);

        $email = $this->load->view('mail/email_template_top', array('title' => $subject), true);
        $email = $email . $content;
        $email = $email . $this->load->view('mail/email_template_bottom', array(), true);

        $mobile = $this->load->view('notification/top_template', array('title' => $subject), true);
        $mobile = $mobile . $content;
        $mobile = $mobile . $this->load->view('notification/bottom_template', array(), true);

        return array(
            'email' => $email,
            'mobile' => $mobile,
            'minimal' => $content,
        );
    }


    function inquiry_dukcapil_post()
    {
        $admin = $this->checkAdminToken($this->post('token'));

        if (!$admin) {
            return $this->errorUnAuthorized();
        }

        log_message('info', 'Start : inquiry_dukcapil ' . $this->post('nik'));

        $this->form_validation->set_rules('nik', 'NIK not blank', 'required');
        $this->form_validation->set_rules('token', 'Token not blank', 'required');
        if ($this->form_validation->run() == false) {
            foreach ($this->form_validation->error_array() as $key => $value) {
                $error = $value;
                break;
            }

            $this->set_response([
                'status' => 'error',
                'message' => $error,
                'code' => 101,
            ]);
        }

        $reqBody = array(
            'nik' => $this->post('nik'),
            'requestType' => 'INQUIRY'
        );

        $response = $this->inquiryDukcapil($reqBody);

        if ($response->responseCode != '00') {
            log_message('debug', 'Fire Dukcapil Gagal!' . json_encode($response));

            return $this->send_response('error', $response->responseDesc, '', '', 502);
        }

        $data = json_encode($response->data);
        $nik  = $this->post('nik');
        $dataUser = $this->User->getUser($nik);
        if (!empty($dataUser)) {
            $update_data_user = [
                'nama'         => $data->nama ?? $dataUser->nama,
                'tgl_lahir'    => $data->tglLahir ?? $dataUser->tgl_lahir,
                'tempat_lahir' => $data->tempatLahir ?? $dataUser->tempat_lahir,
                'nama_ibu'     => $data->ibuKandung ?? $dataUser->nama_ibu,
                'agama'        => $data->agama ?? $dataUser->agama,
            ];

            $this->User->updateUser($dataUser->user_AIID, $update_data_user);
        }

        log_message('debug', 'Fire Dukcapil Success' . json_encode($response));
        return $this->send_response('success', '', $response->data);
    }

    function dob_check($str)
    {
        if (!DateTime::createFromFormat('Y-m-d', $str)) { //yes it's YYYY-MM-DD
            $this->form_validation->set_message('dob_check', 'The {field} has not a valid date format');
            return false;
        } else {
            return true;
        }
    }

    function cek_open_tab_emas_get()
    {
        log_message('debug', 'start of ' . __FUNCTION__ . ' => ' . json_encode($this->get()));

        $token = $this->getToken();

        if (empty($token)) {
            return $this->errorUnAuthorized();
        }

        $response = $this->ekyc_service->checkOpenTabEmas($token->id);

        log_message('debug', 'end of ' . __FUNCTION__ . ' => ' . json_encode($response));

        return $this->send_response($response['status'], $response['message'], $response['data']);
    }
}
