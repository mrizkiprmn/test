<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once 'CorePegadaian.php';
require_once APPPATH . 'helpers/message_helper.php';

/**
 *  POST /courier/gosend/booking
 *  GET  /courier
 *  POST /courier/gosend/cancel
 *  POST /otp/send
 *  POST /otp/check
 *  POST /customer/dukcapil
 *  POST /customer/dukcapilchannel
 *  POST /customer/checkosl
 *  POST /banking/check
 *  POST /disbursal
 *  POST /paymid
 *  POST /fetch_harga_gosend_post
 *  POST /cancel
 */
class God extends CorePegadaian
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('LoggerModel', 'GadaiModel'));
        $this->load->service('God_service');
        $this->load->service('restGosend_service');
        $this->load->library('form_validation');
    }

    function courier_gosend_booking_post()
    {
        if (!$this->validateToken()) {
            $this->set_response($this->response_invalid_token(), 200);
            return;
        }

        $request_body = $this->post();
        $request_body['clientId'] = $this->config->item('core_post_username');
        $request_body['channelId'] = $this->config->item('core_client_id');
        $request_body['paymentType'] = '3';
        $request_body['reffIdClient'] = 'string';

      //save request
        $this->LoggerModel->save(__FUNCTION__, 'REQUEST', $request_body);

        $ch = curl_init();
        $curl_options = array(
        CURLOPT_URL => $this->config->item('core_API_URL') . '/courier/gosend/booking',
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_POST => 1,
        CURLOPT_HEADER => 0,
        CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json',
        'Accept: application/json',
        'Authorization: Bearer' . $this->getCoreToken()
        ),
        CURLOPT_POSTFIELDS => json_encode($request_body)
        );

        curl_setopt_array($ch, $curl_options);
        $result = array(
        'body' => curl_exec($ch),
        'http_code' => curl_getinfo($ch, CURLINFO_HTTP_CODE),
        );

      // get response
        $response = $this->response_set($result, __FUNCTION__);
        $this->set_response($response, 200);
    }

    function courier_get()
    {
        if (!$this->validateToken()) {
            $this->set_response($this->response_invalid_token(), 200);
            return;
        }

        $request_body = $this->get();

      //save request
        $this->LoggerModel->save(__FUNCTION__, 'REQUEST', $request_body);

        $ch = curl_init();
        $curl_options = array(
        CURLOPT_URL => $this->config->item('core_API_URL') . '/courier',
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_POST => 1,
        CURLOPT_HEADER => 0,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json',
        'Accept: application/json',
        'Authorization: Bearer' . $this->getCoreToken()
        ),

        );

        curl_setopt_array($ch, $curl_options);
        $result = array(
        'body' => curl_exec($ch),
        'http_code' => curl_getinfo($ch, CURLINFO_HTTP_CODE),
        );

      // get response
        $response = $this->response_set($result, __FUNCTION__);
        $this->set_response($response, 200);
    }

    function courier_gosend_cancel_post()
    {
        if (!$this->validateToken()) {
            $this->set_response($this->response_invalid_token(), 200);
            return;
        }

        $request_body = $this->post();

      //save request
        $this->LoggerModel->save(__FUNCTION__, 'REQUEST', $request_body);

        $ch = curl_init();
        $curl_options = array(
        CURLOPT_URL => $this->config->item('core_API_URL') . '/courier/gosend/cancel',
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_POST => 1,
        CURLOPT_HEADER => 0,
        CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json',
        'Accept: application/json',
        'Authorization: Bearer' . $this->getCoreToken()
        ),
        CURLOPT_POSTFIELDS => json_encode($request_body)
        );

        curl_setopt_array($ch, $curl_options);
        $result = array(
        'body' => curl_exec($ch),
        'http_code' => curl_getinfo($ch, CURLINFO_HTTP_CODE),
        );

      // get response
        $response = $this->response_set($result, __FUNCTION__);
        $this->set_response($response, 200);
    }

    function otp_send_post()
    {
        if (!$this->validateToken()) {
            $this->set_response($this->response_invalid_token(), 200);
            return;
        }

        $request_body = $this->post();
        $request_body['clientId'] = $this->config->item('core_post_username');
        $request_body['channelId'] = $this->config->item('core_client_id');
        $request_body['requestType'] = 'god';

      //save request
        $this->LoggerModel->save(__FUNCTION__, 'REQUEST', $request_body);

        $reffId = mt_rand(1000, 9999);
        $sendOTP = $this->sendOTP($this->post('noHp'), $reffId, $request_body['requestType'], $request_body['channelId']);
        $result = json_encode($sendOTP);

      // get response
        $response = $this->response_set($result, __FUNCTION__);
        $this->set_response($response, 200);
    }

    function otp_check_post()
    {
        if (!$this->validateToken()) {
            $this->set_response($this->response_invalid_token(), 200);
            return;
        }

        $request_body = $this->post();
        $request_body['clientId'] = $this->config->item('core_post_username');
        $request_body['channelId'] = $this->config->item('core_client_id');
        $request_body['requestType'] = 'god';

      //save request
        $this->LoggerModel->save(__FUNCTION__, 'REQUEST', $request_body);

        $checkOTP = $this->checkOTP($this->post('token'), $this->post('noHp'), $request_body['requestType'], $request_body['channelId']);
        $result = json_encode($checkOTP);
      // get response
        $response = $this->response_set($result, __FUNCTION__);

        if ($response['code'] != '00') {
            $response['message'] = 'OTP tidak sesuai';
        }

        $this->set_response($response, 200);
    }

    function customer_dukcapil_post()
    {
        if (!$this->validateToken()) {
            $this->set_response($this->response_invalid_token(), 200);
            return;
        }

        $request_body = $this->post();

      //save request
        $this->LoggerModel->save(__FUNCTION__, 'REQUEST', $request_body);

        $ch = curl_init();
        $curl_options = array(
        CURLOPT_URL => $this->config->item('core_API_URL') . '/customer/dukcapil',
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_POST => 1,
        CURLOPT_HEADER => 0,
        CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json',
        'Accept: application/json',
        'Authorization: Bearer' . $this->getCoreToken()
        ),
        CURLOPT_POSTFIELDS => json_encode($request_body)
        );

        curl_setopt_array($ch, $curl_options);
        $result = array(
        'body' => curl_exec($ch),
        'http_code' => curl_getinfo($ch, CURLINFO_HTTP_CODE),
        );

      // get response
        $response = $this->response_set($result, __FUNCTION__);
        $this->set_response($response, 200);
    }

    function customer_dukcapilchannel_post()
    {
        if (!$this->validateToken()) {
            $this->set_response($this->response_invalid_token(), 200);
            return;
        }

        $request_body = $this->post();
        $request_body['clientId'] = $this->config->item('core_post_username');
        $request_body['channelId'] = $this->config->item('core_client_id');

      //save request
        $this->LoggerModel->save(__FUNCTION__, 'REQUEST', $request_body);

        $ch = curl_init();
        $curl_options = array(
        CURLOPT_URL => $this->config->item('core_API_URL') . '/customer/dukcapilchannel',
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_POST => 1,
        CURLOPT_HEADER => 0,
        CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json',
        'Accept: application/json',
        'Authorization: Bearer' . $this->getCoreToken()
        ),
        CURLOPT_POSTFIELDS => json_encode($request_body)
        );

        curl_setopt_array($ch, $curl_options);
        $result = array(
        'body' => curl_exec($ch),
        'http_code' => curl_getinfo($ch, CURLINFO_HTTP_CODE),
        );

      // get response
        $response = $this->response_set($result, __FUNCTION__);
        $this->set_response($response, 200);
    }

    function customer_checkosl_post()
    {
        if (!$this->validateToken()) {
            $this->set_response($this->response_invalid_token(), 200);
            return;
        }

        $request_body = $this->post();
        $request_body['clientId'] = $this->config->item('core_post_username');
        $request_body['channelId'] = $this->config->item('core_client_id');
        $request_body['flag'] = 'K';
        $request_body['productType'] = '01';

      //save request
        $this->LoggerModel->save(__FUNCTION__, 'REQUEST', $request_body);

        $ch = curl_init();
        $curl_options = array(
        CURLOPT_URL => $this->config->item('core_API_URL') . '/customer/checkosl',
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_POST => 1,
        CURLOPT_HEADER => 0,
        CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json',
        'Accept: application/json',
        'Authorization: Bearer' . $this->getCoreToken()
        ),
        CURLOPT_POSTFIELDS => json_encode($request_body)
        );

        curl_setopt_array($ch, $curl_options);
        $result = array(
        'body' => curl_exec($ch),
        'http_code' => curl_getinfo($ch, CURLINFO_HTTP_CODE),
        );

      // get response
        $response = $this->response_set($result, __FUNCTION__);
        $this->set_response($response, 200);
    }

    function banking_check_post()
    {
        if (!$this->validateToken()) {
            $this->set_response($this->response_invalid_token(), 200);
            return;
        }

        $request_body = $this->post();
        $request_body['clientId'] = $this->config->item('core_post_username');
        $request_body['channelId'] = $this->config->item('core_client_id');
      //save request
        $this->LoggerModel->save(__FUNCTION__, 'REQUEST', $request_body);

        $ch = curl_init();
        $curl_options = array(
        CURLOPT_URL => $this->config->item('core_API_URL') . '/banking/check',
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_POST => 1,
        CURLOPT_HEADER => 0,
        CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json',
        'Accept: application/json',
        'Authorization: Bearer' . $this->getCoreToken()
        ),
        CURLOPT_POSTFIELDS => json_encode($request_body)
        );

        curl_setopt_array($ch, $curl_options);

        $result = array(
        'body' => curl_exec($ch),
        'http_code' => curl_getinfo($ch, CURLINFO_HTTP_CODE),
        );

      // get response
        $response = $this->response_set($result, __FUNCTION__);
        $this->set_response($response, 200);
    }

    function disbursal_post()
    {
        if (!$this->validateToken()) {
            $this->set_response($this->response_invalid_token(), 200);
            return;
        }

        $request_body = $this->post();

      //save request
        $this->LoggerModel->save(__FUNCTION__, 'REQUEST', $request_body);

        $data = '';
        $data = $this->GadaiModel->check($request_body['kodeBooking'], $request_body['tipe']);
        $request_body['branchCode'] = '';

        if ($data != '') {
            $request_body['branchCode'] = $data->kode_outlet;
        }

        $request_body['clientId'] = $this->config->item('core_post_username');
        $request_body['channelId'] = $this->config->item('core_client_id');
        $request_body['jenisTransaksi'] = 'OP';
        $request_body['flag'] = 'K';

        $ch = curl_init();
        $curl_options = array(
        CURLOPT_URL => $this->config->item('core_API_URL') . '/god/disbursal',
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_POST => 1,
        CURLOPT_HEADER => 0,
        CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json',
        'Accept: application/json',
        'Authorization: Bearer' . $this->getCoreToken()
        ),
        CURLOPT_POSTFIELDS => json_encode($request_body)
        );

        curl_setopt_array($ch, $curl_options);
        $result = array(
        'body' => curl_exec($ch),
        'http_code' => curl_getinfo($ch, CURLINFO_HTTP_CODE),
        );

      // get response
        $response = $this->response_set($result, __FUNCTION__);
        $this->set_response($response, 200);
    }

    function response_set($response = [], $srcFunc = '')
    {
        if (gettype($response) == 'string') {
            $temp = $response;
            $response = [];
            $response['body'] = $temp;
        }

        if (empty($response['body'])) {
            $response['body'] = $response;
        }

        $res = json_decode($response['body'], true);

        if (!empty($res['data']) && gettype($res['data']) == 'string') {
            $res['data'] = json_decode($res['data'], true);
        }

        log_message('debug', 'response /god/ method ' . $srcFunc . ' data: ' . json_encode($res));
      //save response
        $this->LoggerModel->save($srcFunc, 'RESPONSE', $res);

      // response error
        if ($res['responseCode'] != '00') {
            return array(
            'status' => 'error',
            'code' => $res['responseCode'],
            'message' => $res['responseDesc'],
            'data' => ''
            );
        }

        return array(
        'status' => 'success',
        'code' => '00',
        'message' => '',
        'data' => $res
        );
    }

    function response_invalid_token()
    {
        return array(
        'status' => 'error',
        'code' => '99',
        'message' => 'Invalid Token',
        'data' => ''
        );
    }

    function pay_midtrans_post()
    {
        $token = $this->getToken();

        if (empty($token)) {
            return $this->errorUnAuthorized();
        }

        log_message('debug', 'start of ' . __FUNCTION__ . ' => ' . json_encode($this->post()));

        if (empty($this->post())) {
            log_message('debug', 'end of ' . __FUNCTION__ . ' =>  Empty Data');
            return $this->send_response('error', 'Request data tidak ada', null);
        }

        $response = $this->God_service->payMidtransGod($this->post());

        if (empty($response)) {
            log_message('debug', 'end of ' . __FUNCTION__ . ' =>  Empty Response');
            return $this->send_response('error', 'Pengambilan data gagal', null);
        }

        log_message('debug', 'end of ' . __FUNCTION__ . ' => ' . json_encode($response));

        return $this->send_response($response['status'], $response['message'], $response['data']);
    }

  //cek harga gosend
    function fetch_harga_gosend_post()
    {
        $token = $this->getToken();

        if (empty($token)) {
            return $this->errorUnAuthorized();
        }

        log_message('debug', 'start of ' . __FUNCTION__ . ' => ' . json_encode($this->post()));

        $hargagosend   = $this->restGosend_service->getPrice($this->post());
        if (empty($hargagosend)) {
            log_message('debug', 'end of ' . __FUNCTION__ . ' => Pengambilan data gagal');
            return $this->send_response('error', 'Pengambilan data gagal', null);
        }

        log_message('debug', 'end of ' . __FUNCTION__ . ' => ' . json_encode($hargagosend));

        return $this->send_response('success', 'Pengambilan harga gosend sukses', json_decode($hargagosend['data']));
    }

    function cancel_post()
    {
        $token = $this->getToken();

        if (empty($token)) {
            return $this->errorUnAuthorized();
        }

        log_message('debug', 'start of ' . __FUNCTION__ . ' => ' . json_encode($this->post()));

        $this->form_validation->set_data($this->post());
        $this->form_validation->set_rules('kodeBooking', 'kodeBooking', 'required');
        $this->form_validation->set_rules('jenisTransaksi', 'jenisTransaksi', 'required');
        $this->form_validation->set_rules('isCoreRegister', 'isCoreRegister', 'required');

        if (!$this->form_validation->run()) {
            $response = $this->send_response('error', 'Invalid Input', $this->form_validation->error_array());
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . $response);
            return $response;
        }

        $updateCancel = $this->GadaiModel->updateStatusGodCancel($this->post());
        log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . $updateCancel);
        $response = $this->God_service->godCancel($this->post());

        if (empty($response)) {
            log_message('debug', 'end of ' . __FUNCTION__ . ' =>  Empty Response');
            return $this->send_response('error', 'Pengambilan data gagal', null);
        }

        log_message('debug', 'end of ' . __FUNCTION__ . ' => ' . json_encode($response));

        return $this->send_response($response['status'], $response['message'], $response['data']);
    }
}
