<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Column_In_Table_Transaction_Gtef extends CI_Migration
{
    public function up()
    {
        $column = [
            'user_id' => [
                'type' => 'INT',
                'unsigned' => true,
                'null' => false,
                'after' => 'id'
            ],
            'gcash_id' => [
                'type' => 'INT',
                'unsigned' => true,
                'null' => true,
                'after' => 'bank_id'
            ],
            'payment_method' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
                'after' => 'gcash_id'
            ],
            'late_charge_rental_cost' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
                'after' => 'rental_cost_fee'
            ],
            'deposit_extension_fee' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
                'after' => 'late_charge_rental_cost'
            ],
            'lease_payment_date' => [
                'type' => 'DATETIME',
                'null' => true,
                'after' => 'credit_number'
            ],
            'is_paid' => [
                'type' => 'BOOLEAN',
                'default' => false,
                'after' => 'transfer_surcharge'
            ],
            'payment_date' => [
                'type' => 'DATETIME',
                'null' => true,
                'after' => 'is_paid'
            ],
            'trx_type' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => false,
                'after' => 'payment_date'
            ],
            'trx_code' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => false,
                'after' => 'trx_type'
            ],
            'reff_switching' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
                'after' => 'trx_code'
            ],
            'reff_core' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
                'after' => 'reff_switching'
            ],
            'reff_biller' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
                'after' => 'reff_core'
            ],
            'is_submit' => [
                'type' => 'BOOLEAN',
                'default' => false,
                'after' => 'reff_biller'
            ],
        ];

        $this->dbforge->add_column('transaction_gtef', $column);

        $field = [
            'bank_id' => [
                'null' => true
            ],
        ];

        $this->dbforge->modify_column('transaction_gtef', $field);
    }

    public function down()
    {
        //
    }
}