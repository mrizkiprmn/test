<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * @property mixed dbforge
 */
class Migration_Add_Table_Rekening_Bank_God extends CI_Migration
{
    public function up()
    {
        $this->dbforge->add_field([
            'id' => [
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => true,
                'auto_increment' => true
            ],
            'user_id' => [
                'type' => 'INT',
                'constraint' => '11',
                'null' => false,
            ],
            'nomor_rekening' => [
                'type' => 'VARCHAR',
                'constraint' => '30',
                'null' => false
            ],
            'kode_bank' => [
                'type' => 'VARCHAR',
                'constraint' => '5',
                'null' => false
            ],
            'nama_pemilik' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => false
            ],
            'created_at' => [
                'type' => 'DATETIME',
                'null' => false,
            ]
        ]);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('rekening_bank_gadai');
    }

    public function down()
    {
        //
    }
}
