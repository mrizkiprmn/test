<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Column_cif_In_Table_Audit_Log extends CI_Migration
{
    public function up()
    {
        $column = [
            'cif' => [
                'type' => 'varchar',
                'constraint' => '100',
                'after' => 'modul'
            ],
            'nama_nasabah' => [
                'type' => 'varchar',
                'constraint' => '100',
                'after' => 'cif'
            ],
            'no_hp' => [
                'type' => 'varchar',
                'constraint' => '100',
                'after' => 'nama_nasabah'
            ],
            'platform' => [
                'type' => 'ENUM',
                'constraint' => ['','pds', 'admin'],
                'null' => false,
                'after' => 'change_date'
            ]
        ];

        $this->dbforge->add_column('audit_log', $column);
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->dbforge->drop_table('audit_log', true);
    }
}
