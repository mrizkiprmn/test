<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Table_Cache_Tabemas_Detail_History extends CI_Migration
{
    public function up()
    {

        $this->dbforge->add_field([
            'id' => [
                'type' => 'VARCHAR',
                'constraint' => '250',
                'null' => false
            ],
            'reff_core' => [
                'type' => 'VARCHAR',
                'constraint' => '255'
            ],
            'norek' => [
                'type' => 'VARCHAR',
                'constraint' => '20'
            ],
            'harga' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
                'default' => '0'
            ],
            'berat' => [
                'type' => 'DOUBLE',
                'null' => false,
                'default' => 0
            ],
            'jenis_transaksi' => [
                'type' => 'VARCHAR',
                'constraint' => '20',
                'before' => 'norek'
            ],
            'tgl_transaksi' => [
                'type' => 'DATE',
                'null' => false
            ],
            'created_at' => [
                'type' => 'DATETIME',
                'null' => false,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'null' => true,
            ],
        ]);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('cache_tabemas_detail_history');
    }

    public function down()
    {
        //
    }
}
