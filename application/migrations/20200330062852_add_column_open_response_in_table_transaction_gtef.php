<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * @property mixed dbforge
 */
class Migration_Add_Column_OPEN_RESPONSE_In_Table_Transaction_Gtef extends CI_Migration
{
    public function up()
    {
        $column = [
            'code_outlet' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
                'after' => 'tef_no'
            ],
            'estimated_value' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
                'after' => 'code_outlet'
            ],
            'amount_approved' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
                'after' => 'amount'
            ],
            'customer_name' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
                'after' => 'amount_approved'
            ],
            'tenor' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
                'after' => 'customer_name'
            ],
            'cost' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
                'after' => 'tenor'
            ],
            'deposit_cost' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
                'after' => 'cost'
            ],
            'rental_cost' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
                'after' => 'deposit_cost'
            ],
            'rental_cost_fee' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
                'after' => 'rental_cost'
            ],
            'credit_number' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
                'after' => 'rental_cost_fee'
            ],
            'credit_date' => [
                'type' => 'DATETIME',
                'null' => true,
                'after' => 'credit_number'
            ],
            'auction_date' => [
                'type' => 'DATETIME',
                'null' => true,
                'after' => 'credit_date'
            ],
            'due_date' => [
                'type' => 'DATETIME',
                'null' => true,
                'after' => 'auction_date'
            ],
            'auction_surcharge' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
                'after' => 'due_date'
            ],
            'insurance_surcharge' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
                'after' => 'auction_surcharge'
            ],
            'admin_surcharge' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
                'after' => 'insurance_surcharge'
            ],
            'transfer_surcharge' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
                'after' => 'admin_surcharge'
            ],
        ];

        $this->dbforge->add_column('transaction_gtef', $column);
    }

    public function down()
    {
        //
    }
}
