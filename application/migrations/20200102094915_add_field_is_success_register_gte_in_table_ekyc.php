<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Field_Is_Success_Register_Gte_In_Table_Ekyc extends CI_Migration
{
    public function up()
    {
        // this up() migration is auto-generated, please modify it to your needs
        // Drop table 'table_name' if it exists
        $column = array(
            'is_success_register_gte' => [
                'type' => 'BOOLEAN',
                'default' => false
            ],
        );

        $this->dbforge->add_column('ekyc', $column);
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
