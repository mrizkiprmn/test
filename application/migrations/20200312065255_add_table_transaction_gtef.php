<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * @property mixed dbforge
 */
class Migration_Add_Table_Transaction_Gtef extends CI_Migration
{
    public function up()
    {
        // Table structure for table 'table_name'
        $this->dbforge->add_field([
            'id' => [
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => true,
                'unique' => true
            ],
            'bank_id' => [
                'type' => 'INT',
                'unsigned' => true,
                'null' => false,
            ],
            'tef_no' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => false,
            ],
            'amount' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => false,
            ],
            'created_at' => [
                'type' => 'DATETIME',
                'null' => false,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'null' => false,
            ]
        ]);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('transaction_gtef');
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->dbforge->drop_table('transaction_gtef', true);
    }
}
