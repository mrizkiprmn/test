<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Table_Master_Tenor extends CI_Migration
{
    public function up()
    {
        // this up() migration is auto-generated, please modify it to your needs
        // Drop table 'table_name' if it exists
        $this->dbforge->drop_table('master_tenor', true);

        // Table structure for table 'table_name'
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => true,
                'auto_increment' => true
            ),
            'tenor' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
            ),
            'IJK' => array(
                'type' => 'decimal',
                'constraint' => '10,2',
            )
        ));
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('master_tenor');
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->dbforge->drop_table('master_tenor', true);
    }
}
