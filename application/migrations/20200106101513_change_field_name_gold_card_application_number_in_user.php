<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Change_Field_Name_Gold_Card_Application_Number_In_User extends CI_Migration
{
    public function up()
    {
        $fields = array(
            'gold_card_application_number' => array(
                'name' => 'goldcard_application_number',
                'type' => 'VARCHAR',
                'constraint' => '250',
                'null' => true
            ),
        );
        $this->dbforge->modify_column('user', $fields);
    }

    public function down()
    {
    }
}
