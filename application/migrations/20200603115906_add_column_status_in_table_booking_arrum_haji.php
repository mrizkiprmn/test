<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * @property mixed dbforge
 */
class Migration_Add_Column_Status_In_Table_Booking_Arrum_Haji extends CI_Migration
{
    public function up()
    {
        $column = [
            'applied_at' => [
                'type' => 'DATETIME',
                'null' => true,
                'after' => 'code_outlet'
            ],
            'status' => [
                'type' => 'ENUM',
                'constraint' => ['active', 'expire', 'approve', 'decline'],
                'null' => false,
                'after' => 'applied_at'
            ],
            'decline_reason' => [
                'type' => 'varchar',
                'constraint' => '100',
                'null' => true,
                'after' => 'status'
            ],
        ];

        $this->dbforge->add_column('booking_arrum_haji', $column);
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->dbforge->drop_table('table_name', true);
    }
}
