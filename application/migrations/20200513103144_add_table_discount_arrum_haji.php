<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Table_Discount_Arrum_Haji extends CI_Migration
{
    public function up()
    {
        // this up() migration is auto-generated, please modify it to your needs
        // Drop table 'table_name' if it exists
        $this->dbforge->drop_table('discount_arrum_haji', true);

        // Table structure for table 'table_name'
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => true,
                'auto_increment' => true
            ),
            'batas_bawah' => array(
                'type' => 'decimal',
                'constraint' => '10,2'
            ),
            'batas_atas' => array(
                'type' => 'decimal',
                'constraint' => '10,2'
            ),
            'discount_rate' => array(
                'type' => 'decimal',
                'constraint' => '10,2'
            )
        ));
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('discount_arrum_haji');
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->dbforge->drop_table('discount_arrum_haji', true);
    }
}
