<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Enum_On_Field_Group_In_Table_Mpo extends CI_Migration
{
    public function up()
    {
         # Change the ENUM contstraint for EmployeeRole to include a contractor role
         $field = array(
            'group' => array(
                'type' => 'ENUM',
                'constraint' => array('seluler','listrik','air','asuransi','voucher')
            )
         );
        
        # Modify the field
         $this->dbforge->modify_column('mpo', $field);
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
        // $this->dbforge->drop_table('table_name', true);
    }
}
