<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * @property mixed dbforge
 */
class Migration_Add_Table_History_Pin extends CI_Migration
{
    public function up()
    {
        $this->dbforge->add_field([
            'user_id' => [
                'type' => 'INT',
                'constraint' => '11',
                'null' => false,
                'unique' => true,
            ],
            'cif' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'last_access_time' => [
                'type' => 'DATETIME',
                'null' => false,
            ],
            'counter' => [
                'type' => 'SMALLINT',
                'null' => false,
                'default' => 0,
            ],
            'is_blocked' => [
                'type' => 'BOOLEAN',
                'null' => false,
                'default' => false,
            ],
            'blocked_date' => [
                'type' => 'DATETIME',
                'null' => true,
            ],
            'created_at' => [
                'type' => 'DATETIME',
                'null' => false,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'null' => true,
            ],
        ]);
        $this->dbforge->add_key('user_id', true);
        $this->dbforge->create_table('user_pin');
    }

    public function down()
    {
        //
    }
}
