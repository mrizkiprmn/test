<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * @property mixed dbforge
 */
class Migration_Add_Column_Date_Try_Login_Date_And_Wrong_Login_Count_In_Table_User extends CI_Migration
{
    public function up()
    {
        $column = [
            'try_login_date' => [
                'type' => 'DATETIME',
                'null' => true,
            ],

            'wrong_password_count' => [
                'type' => 'SMALLINT',
                'null' => false,
                'default' => 0,
            ],
        ];

        $this->dbforge->add_column('user', $column);
    }

    public function down()
    {
        //
    }
}
