<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Column_Pelunasan_Krasida_Mikro extends CI_Migration
{
    public function up()
    {
        $column = [
            'diskonPelunasan' => [
                'type' => 'INT',
                'constraint' => '11',
                'null' => true,
            ],
            'tarifSM' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'tunggakanPokok' => [
                'type' => 'INT',
                'constraint' => '11',
                'null' => true,
            ],
            'tunggakanSM' => [
                'type' => 'INT',
                'constraint' => '11',
                'null' => true,
            ],
            'sisaPokok' => [
                'type' => 'INT',
                'constraint' => '11',
                'null' => true,
            ],
        ];

        $this->dbforge->add_column('payment_mikro', $column);
    }

    public function down()
    {
        //
    }
}
