<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Table_Financing extends CI_Migration
{
    public function up()
    {
        // this up() migration is auto-generated, please modify it to your needs
        // Drop table 'table_name' if it exists
        $this->dbforge->drop_table('financing', true);

        // Table structure for table 'table_name'
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'VARCHAR',
                'constraint' => '250',
                'null' => false
            ),
            'user_id' => [
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => true,
                'null' => false,
            ],
            'code_product' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'code_booking' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'code_outlet' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'code_promo' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'date' => [
                'type' => 'DATETIME',
                'null' => true,
            ],
            'loan_amount' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'tenor' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'installment' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'status' => [
                'type' => 'SMALLINT',
                'constraint' => '2',
                'null' => false,
                'default' => 1
            ],
            'status_history' => [
                'type' => 'TEXT',
                'null' => true
            ],
            'response_los' => [
                'type' => 'TEXT',
                'null' => true
            ],
            'request_los' => [
                'type' => 'TEXT',
                'null' => true
            ],
            'is_have_business' => [
                'type' => 'BOOLEAN',
                'null' => true
            ],
            'is_sent' => [
                'type' => 'BOOLEAN',
                'null' => false,
                'default' => false
            ],
            'created_at' => [
                'type' => 'DATETIME',
                'null' => false,
            ],
            'update_at' => [
                'type' => 'DATETIME',
                'null' => true,
            ],
            'status_desc' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'pic_current_status' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => true,
            ]
        ));

        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('financing');
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->dbforge->drop_table('financing', true);
    }
}
