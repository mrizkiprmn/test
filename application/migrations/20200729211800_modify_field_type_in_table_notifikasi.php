<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Modify_Field_Type_In_Table_Notifikasi extends CI_Migration
{
    public function up()
    {
        // define colomn for modify table
        $column = [
            'type' => [
                'type' => 'ENUM',
                'constraint' => ['promo','account','emas','mpo','gadai','micro','admin','jatuh_tempo','gcash','gtef','arrum_haji','refund_god']
            ]
        ];

        $this->dbforge->modify_column('notifikasi', $column);
    }

    public function down()
    {
        //
    }
}
