<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Column_In_Table_Audit_Log extends CI_Migration
{
    /**
     * @var mixed
     */
    private $dbforge;

    public function up()
    {
        // define column for modify table
        $column = [
            'other_property' => [
                'type' => 'TEXT',
                'null' => true
            ],
        ];

        // add column
        $this->dbforge->add_column('audit_log', $column);
    }

    public function down()
    {
        //
    }
}
