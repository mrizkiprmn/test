<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Enum_On_Field_Type_In_Table_Notifikasi extends CI_Migration
{
    public function up()
    {
        # Change the ENUM contstraint for type to include krasida_te
        $field = array(
            'type' => array(
                'type' => 'ENUM',
                'constraint' => array('promo', 'account', 'emas', 'mpo', 'gadai', 'micro', 'admin', 'jatuh_tempo', 'gcash', 'gtef', 'arrum_haji', 'refund_god', 'krasida_te')
            )
        );
        
        # Modify the field
        $this->dbforge->modify_column('notifikasi', $field);
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
        // $this->dbforge->drop_table('table_name', true);
    }
}
