<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Field_Screen_Name_In_Table_Ref_Banner_Shortcut extends CI_Migration
{
    public function up()
    {
        // this up() migration is auto-generated, please modify it to your needs
        // Drop table 'table_name' if it exists
        $column = array(
            'screen_name' => [
                 'type' => 'VARCHAR',
                 'constraint' => '100',
                 'null' => true,
            ],
            'updateby' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => true,
            ],
            'createby' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => true,
            ],
            'updateat' => [
                'type' => 'DATETIME',
                'null' => false,
            ],
            'createat' => [
                'type' => 'DATETIME',
                'null' => false,
            ],
        );
        
        $this->dbforge->add_column('ref_banner_shortcut', $column);
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->dbforge->drop_table('ref_banner_shortcut', true);
    }
}
