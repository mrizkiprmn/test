<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Field_Is_Open_Te_Table_User extends CI_Migration
{
    public function up()
    {
        // this up() migration is auto-generated, please modify it to your needs
        // Drop table 'table_name' if it exists
        $column = array(
            'is_open_te' => [
                'type' => 'BOOLEAN',
                'default' => false
            ],
        );

        $this->dbforge->add_column('user', $column);
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
