<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * @property mixed dbforge
 */
class Migration_Add_Column_Is_Submit_In_Table_Transactions extends CI_Migration
{
    public function up()
    {
        $column = [
            'is_submit' => [
                'type' => 'BOOLEAN',
                'default' => false,
            ]
        ];

        $this->dbforge->add_column('transactions', $column);
    }

    public function down()
    {
        //
    }
}
