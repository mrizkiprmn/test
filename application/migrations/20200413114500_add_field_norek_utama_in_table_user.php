<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Field_Norek_Utama_In_Table_User extends CI_Migration
{
    public function up()
    {
        // define colomn for modify table
        $column = array(
            'norek_utama' => [
                'type' => 'VARCHAR',
                'constraint' => '20',
                'before' => 'norek'
            ]
        );
        // add column
        $this->dbforge->add_column('user', $column);
    }

    public function down()
    {
        //
    }
}
