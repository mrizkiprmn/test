<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Column_Tanggal_Lahir_In_Table_Ekyc extends CI_Migration
{
    public function up()
    {
        $this->dbforge->add_field([
            'tanggal_lahir' => [
                'type' => 'DATE',
                'null' => true,
                'after' => 'nik',
            ]
        ]);
    }

    public function down()
    {
        //
    }
}
