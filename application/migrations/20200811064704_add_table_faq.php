<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Table_Faq extends CI_Migration
{
    public function up()
    {
        $this->dbforge->drop_table('tbl_faq', true);

        $this->dbforge->add_field([
            'id' => [
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => true,
                'auto_increment' => true
            ],
            'category' => [
                'type' => 'varchar',
                'constraint' => '100',
            ],
            'sub_category' => [
                'type' => 'varchar',
                'constraint' => '100',
            ],
            'question' => [
                'type' => 'text',
                'null' => true,
            ],
            'answer' => [
                'type' => 'text',
                'null' => true,
            ],
            'counter' => [
                'type' => 'varchar',
                'constraint' => '100',
            ],
        ]);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('tbl_faq');
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->dbforge->drop_table('tbl_faq', true);
    }
}
