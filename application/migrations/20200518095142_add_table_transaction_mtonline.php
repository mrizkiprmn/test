<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Table_Transaction_Mtonline extends CI_Migration
{
    public function up()
    {
        // Table structure for table 'table_name'
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => true,
                'unique' => true,
                'auto_increment' => true
            ),
            'bank_id' => [
                'type' => 'INT',
                'unsigned' => true,
                'null' => true,
            ],
            'gcash_id' => [
                'type' => 'INT',
                'unsigned' => true,
                'null' => true,
            ],
            'contract_number' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => false,
            ],
            'code_outlet' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'estimated_value' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'amount' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => false,
            ],
            'amount_approved' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'total_amount' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'customer_name' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'type_transaction' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'product_name' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'golongan' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'jumlah_hari_real' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'jumlah_hari_tarif' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'tenor' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'sewa_modal' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'tagihan_sewa_modal' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'metode_pencairan' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'denda' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'admin_surcharge' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'insurance_surcharge' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'surcharge' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'total_surcharge' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'status' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'credit_number' => [
                'type' => 'DATETIME',
                'null' => true,
            ],
            'due_date' => [
                'type' => 'DATETIME',
                'null' => true,
            ],
            'credit_date' => [
                'type' => 'DATETIME',
                'null' => true,
            ],
            'auction_date' => [
                'type' => 'DATETIME',
                'null' => true,
            ],
            'transaction_date' => [
                'type' => 'DATETIME',
                'null' => true,
            ],
            'is_disbursed' => [
                'type' => 'BOOLEAN',
                'default' => false,
            ],
            'jenisPromo' => [
                'type' => 'VARCHAR',
                'constraint' => '30',
                'null' => true,
            ],
            'created_at' => array(
                'type' => 'DATETIME',
                'null' => false,
            ),
            'updated_at' => [
                'type' => 'DATETIME',
                'null' => false,
            ]
        ));
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('apply_mtonline');
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->dbforge->drop_table('apply_mtonline', true);
    }
}
