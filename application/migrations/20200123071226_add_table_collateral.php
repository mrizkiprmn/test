<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Table_Collateral extends CI_Migration
{
    public function up()
    {
        // this up() migration is auto-generated, please modify it to your needs
        // Drop table 'table_name' if it exists
        $this->dbforge->drop_table('collateral', true);

        // Table structure for table 'table_name'
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => true,
                'auto_increment' => true
            ),
            'financing_id' => array(
                'type' => 'VARCHAR',
                'constraint' => '250',
                'null' => false
            ),
            'type' => array(
                'type' => 'VARCHAR',
                'constraint' => '250',
                'null' => true
            ),
            'detail' => array(
                'type' => 'TEXT',
                'null' => true
            ),
            'created_at' => array(
                'type' => 'DATETIME',
                'null' => false,
            )
        ));
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('collateral');
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->dbforge->drop_table('collateral', true);
    }
}
