<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Field_Id_Super_Admin_In_Table_Reset_Password_User_Mikro extends CI_Migration
{
    public function up()
    {
        // define colomn for modify table
        $column = array(
            'id_super_admin' => array(
            'type' => 'MEDIUMINT',
            'constraint' => '8',
            'after' => 'id_admin_cabang'
            )
        );
        // add column
        $this->dbforge->add_column('reset_password_user', $column);
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->dbforge->drop_table('reset_password_user', true);
    }
}
