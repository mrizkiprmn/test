<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Table_Reset_Password_User_Mikro extends CI_Migration
{
    public function up()
    {
        // this up() migration is auto-generated, please modify it to your needs
        // Drop table 'table_name' if it exists
        // Table structure for table 'table_name'
        $this->dbforge->add_field([
            'reset_id' => [
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => true,
                'auto_increment' => true
            ],
            'username' => [
                'type' => 'VARCHAR',
                'constraint' => '150',
                'null' => false
            ],
            'password' => [
                'type' => 'VARCHAR',
                'constraint' => '150',
                'null' => false
            ],
            'date_created' => [
                'type' => 'DATETIME',
                'null' => false
            ]
        ]);
        
        $this->dbforge->add_key('reset_id', true);
        $this->dbforge->create_table('reset_password_user');
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
