<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Field_Response_Frdukcapil extends CI_Migration
{
    public function up()
    {
        // define colomn for modify table
        $column = array(
            'response_fr_dukcapil' => array(
                'type' => 'TEXT',
                'after' => 'error_count'
            )
        );
        // add column
        $this->dbforge->add_column('ekyc', $column);
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
        // $this->dbforge->drop_table('table_name', true);
    }
}
