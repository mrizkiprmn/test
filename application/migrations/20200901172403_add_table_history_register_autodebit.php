<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Table_History_Register_Autodebit extends CI_Migration
{
    public function up()
    {
        // this up() migration is auto-generated, please modify it to your needs
        // Drop table 'table_name' if it exists
        $this->dbforge->drop_table('history_register_autodebit', true);

        // Table structure for table 'table_name'
        $this->dbforge->add_field(array(
            'id' => [
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => true,
                'auto_increment' => true
            ],
            'user_id' => [
                'type' => 'MEDIUMINT',
                'constraint' => '11',
                'null' => false,
            ],
            'user_name' => [
                'type' => 'varchar',
                'constraint' => '255',
                'null' => false,
            ],
            'no_hp' => [
                'type' => 'varchar',
                'constraint' => '50',
                'null' => false,
            ],
            'type_transaksi' => [
                'type' => 'ENUM',
                'constraint' => ['SL','CC'],
                'null' => false
            ],
            'transaksi_name' => [
                'type' => 'varchar',
                'constraint' => '200',
                'null' => false,
            ],
            'no_kredit' => [
                'type' => 'varchar',
                'constraint' => '200',
                'null' => false,
            ],
            'bank_name' => [
                'type' => 'varchar',
                'constraint' => '255',
                'null' => false,
            ],
            'norek_bank' => [
                'type' => 'varchar',
                'constraint' => '255',
                'null' => false,
            ],
            'total_debit' => [
                'type' => 'varchar',
                'constraint' => '255',
                'null' => false
            ],
            'time_interval_debit' => [
                'type' => 'varchar',
                'constraint' => '50',
                'null' => false
            ],
            'date_debit' => [
                'type' => 'varchar',
                'constraint' => '50',
                'null' => false
            ],
            'product_code' => [
                'type' => 'varchar',
                'constraint' => '2',
                'null' => false,
            ],
            'product_name' => [
                'type' => 'varchar',
                'constraint' => '100',
                'null' => false,
            ],
            'id_reg' => [
                'type' => 'varchar',
                'constraint' => '200',
                'null' => false,
            ],
            'status_regis' => [
                'type' => 'SMALLINT',
                'constraint' => '2',
                'null' => false,
                'default' => 0
            ],
            'start_regis' => [
                'type' => 'DATETIME',
                'null' => false
            ],
            'exp_regis' => [
                'type' => 'DATETIME',
                'null' => false
            ],
            'exp_date_debit' => [
                'type' => 'DATETIME',
                'null' => false
            ],
            'counter_otp' => [
                'type' => 'SMALLINT',
                'constraint' => '2',
                'null' => false,
                'default' => 0
            ],
            'flag_otp' => [
                'type' => 'ENUM',
                'constraint' => ['pending_otp','pending_register','active'],
                'default' => 'pending_otp',
                'null' => false
            ],
            'status_debit' => [
                'type' => 'ENUM',
                'constraint' => ['not_active','active'],
                'default' => 'not_active',
                'null' => false
            ],
            'date_active_debit' => [
                'type' => 'DATETIME',
                'null' => true
            ]
        ));
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('history_register_autodebit');
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
        // $this->dbforge->drop_table('table_name', true);
    }
}
