<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Field_Wrong_Password_Counter_In_Table_Admin extends CI_Migration
{
    public function up()
    {
        // this up() migration is auto-generated, please modify it to your needs
        // Drop table 'table_name' if it exists
        $column = array(
            'wrong_password_count' => [
                'type' => 'INT',
                'constraint' => '8',
                'after' => 'role_id',
                'default' => 0
            ],
        );

        $this->dbforge->add_column('admin', $column);
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->dbforge->drop_table('admin', true);
    }
}
