<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Table_Pegawai extends CI_Migration
{
    public function up()
    {
        // this up() migration is auto-generated, please modify it to your needs
        // Drop table 'table_name' if it exists
        $this->dbforge->drop_table('pegawai', true);

        // Table structure for table 'table_name'
        $this->dbforge->add_field([
            'id' => [
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => true,
                'auto_increment' => true
            ],
            'nip' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => false,
            ],
            'ktp' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'nama' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'hp_1' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'hp_2' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'cif' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'ket' => [
                'type' => 'text',
                'null' => true,
            ]
        ]);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('pegawai');
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->dbforge->drop_table('pegawai', true);
    }
}
