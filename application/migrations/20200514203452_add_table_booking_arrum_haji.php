<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Table_Booking_Arrum_Haji extends CI_Migration
{
    public function up()
    {
        // this up() migration is auto-generated, please modify it to your needs
        // Drop table 'table_name' if it exists
        $this->dbforge->drop_table('booking_arrum_haji', true);

        // Table structure for table 'table_name'
        $this->dbforge->add_field(array(
            'booking_id' => array(
                'type' => 'BIGINT',
                'constraint' => '25'
            ),
            'user_id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8'
            ),
            'user_name' => array(
                'type' => 'varchar',
                'constraint' => '255'
            ),
            'taksiran' => array(
                'type' => 'varchar',
                'constraint' => '50'
            ),
            'pengajuan_pinjaman' => array(
                'type' => 'varchar',
                'constraint' => '50'
            ),
            'tenor' => array(
                'type' => 'varchar',
                'constraint' => '255'
            ),
            'diskon_tabel_id' => array(
                'type' => 'mediumint',
                'constraint' => '8'
            ),
            'diskon_tabel' => array(
                'type' => 'varchar',
                'constraint' => '50'
            ),
            'mu_nah' => array(
                'type' => 'varchar',
                'constraint' => '50'
            ),
            'diskon' => array(
                'type' => 'varchar',
                'constraint' => '50'
            ),
            'mu_nah_nett' => array(
                'type' => 'varchar',
                'constraint' => '50'
            ),
            'angsuran' => array(
                'type' => 'varchar',
                'constraint' => '50'
            ),
            'mu_nah_akad' => array(
                'type' => 'varchar',
                'constraint' => '50'
            ),
            'ijk' => array(
                'type' => 'varchar',
                'constraint' => '50'
            ),
            'biaya_buka_rekening' => array(
                'type' => 'varchar',
                'constraint' => '50'
            ),
            'outlet_pengajuan' => array(
                'type' => 'varchar',
                'constraint' => '255'
            ),
            'code_outlet' => array(
                'type' => 'varchar',
                'constraint' => '255'
            ),
            'created_at' => array(
                'type' => 'DATETIME',
                'null' => false
            ),
            'updated_at' => array(
                'type' => 'DATETIME',
                'null' => false
            )
        ));
        $this->dbforge->add_key('booking_id', true);
        $this->dbforge->create_table('booking_arrum_haji');
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->dbforge->drop_table('booking_arrum_haji', true);
    }
}
