<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * @property mixed dbforge
 */
class Migration_Add_Table_Transactions extends CI_Migration
{
    public function up()
    {
        $this->dbforge->add_field([
            'id' => [
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => true,
                'auto_increment' => true
            ],
            'user_id' => [
                'type' => 'INT',
                'unsigned' => true,
                'null' => false,
            ],
            'relation_id' => [
                'type' => 'INT',
                'unsigned' => true,
                'null' => false,
            ],
            'relation_table' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => false,
            ],
            'type' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => false,
            ],
            'trx_id' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'trx_type' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => false,
            ],
            'trx_code' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => false,
            ],
            'reff_switching' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'reff_core' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'reff_biller' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'date' => [
                'type' => 'DATETIME',
                'null' => false,
            ],
            'created_at' => [
                'type' => 'DATETIME',
                'null' => false,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'null' => true,
            ]
        ]);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('transactions');
    }

    public function down()
    {
        $this->dbforge->drop_table('transactions', true);
    }
}
