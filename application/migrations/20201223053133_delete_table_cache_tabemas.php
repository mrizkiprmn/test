<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Delete_Table_Cache_Tabemas extends CI_Migration
{
    public function up()
    {
        $this->dbforge->drop_table('cache_tabemas', true);
        $this->dbforge->drop_table('cache_tabemas_detail_history', true);
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
        // $this->dbforge->drop_table('table_name', true);
    }
}