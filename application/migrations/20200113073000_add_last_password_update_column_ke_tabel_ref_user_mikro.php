<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Last_Password_Update_Column_Ke_Tabel_Ref_User_Mikro extends CI_Migration
{
    public function up()
    {
        // this up() migration is auto-generated, please modify it to your needs
        // Drop table 'table_name' if it exists
        // Table structure for table 'table_name'
        $column = array(
            'last_password_update' => [
                'type' => 'DATETIME',
                'null' => true
            ],
        );
        $this->dbforge->add_column('ref_user_mikro', $column);
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
