<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Modify_Field_Id_And_Bankid_In_Table_Transaction_Gtef extends CI_Migration
{
    public function up()
    {
        $fields = array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'unsigned' => true,
                'auto_increment' => true,
                'null' => false
            ),
            'bank_id' => array(
                'type' => 'INT',
                'unsigned' => true,
                'null' => true,
            ),
            'trx_type' => array(
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ),
            'trx_code' => array(
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ),
        );

        $this->dbforge->modify_column('transaction_gtef', $fields);
    }

    public function down()
    {
    }
}
