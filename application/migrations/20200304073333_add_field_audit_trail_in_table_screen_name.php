<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Field_Audit_Trail_In_Table_Screen_Name extends CI_Migration
{
    public function up()
    {
        // this up() migration is auto-generated, please modify it to your needs
        // Drop table 'table_name' if it exists
        $column = array(
            'updated_by' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => true,
            ),
            'created_by' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => true,
            ),
            'updated_at' => array(
                'type' => 'DATETIME',
                'null' => false,
            )
        );
        
        $this->dbforge->add_column('screen_name', $column);
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->dbforge->drop_table('screen_name', true);
    }
}
