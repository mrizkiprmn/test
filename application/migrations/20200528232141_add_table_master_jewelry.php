<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * @property mixed dbforge
 */
class Migration_Add_Table_Master_Jewelry extends CI_Migration
{
    public function up()
    {
        // this up() migration is auto-generated, please modify it to your needs
        // Drop table 'table_name' if it exists
        $this->dbforge->drop_table('master_jewelry', true);

        // Table structure for table 'table_name'
        $this->dbforge->add_field([
            'id' => [
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => true,
                'auto_increment' => true
            ],
            'code' => [
                'type' => 'char',
                'constraint' => '4',
                'null' => false
            ],
            'name' => [
                'type' => 'varchar',
                'constraint' => '100',
            ],
            'created_at' => [
                'type' => 'DATETIME',
                'null' => false,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'null' => true
            ]
        ]);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('master_jewelry');
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->dbforge->drop_table('master_jewelry', true);
    }
}
