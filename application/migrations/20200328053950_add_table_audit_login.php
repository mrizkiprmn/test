<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * @property mixed dbforge
 */
class Migration_Add_Table_Audit_Login extends CI_Migration
{
    public function up()
    {
        $this->dbforge->add_field([
            'id' => [
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => true,
                'auto_increment' => true
            ],
            'user_id' => [
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => true,
                'null' => true,
            ],
            'channel_id' => [
                'type' => 'VARCHAR',
                'constraint' => '10',
                'null' => true,
            ],
            'device_id' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'ip' => [
                'type' => 'VARCHAR',
                'constraint' => '30',
                'null' => true,
            ],
            'latitude' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'longitude' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'timezone' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'timestamp' => [
                'type' => 'VARCHAR',
                'constraint' => '200',
                'null' => true,
            ],
            'created_at' => [
                'type' => 'DATETIME',
                'null' => false,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'null' => true,
            ],
        ]);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('audit_login');
    }

    public function down()
    {
        //
    }
}
