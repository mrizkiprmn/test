<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Field_Reason_In_Table_Ekyc extends CI_Migration
{
    public function up()
    {
        // this up() migration is auto-generated, please modify it to your needs
        // Drop table 'table_name' if it exists
        $column = array(
            'reason' => [
                'type' => 'VARCHAR',
                'constraint' => '250',
                'null' => true
            ],
        );

        $this->dbforge->add_column('ekyc', $column);
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
