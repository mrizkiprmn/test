<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Field_Gold_Card_Application_Number extends CI_Migration
{
    public function up()
    {
        // this up() migration is auto-generated, please modify it to your needs
        // Drop table 'table_name' if it exists
        $column = array(
            'gold_card_application_number' => [
                'type' => 'VARCHAR',
                'constraint' => '250',
                'null' => true
            ],
        );

        $this->dbforge->add_column('user', $column);
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
