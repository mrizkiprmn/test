<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Field_Status_In_Table_Reset_Password_User_Mikro extends CI_Migration
{
    public function up()
    {
        // define colomn for modify table
        $column = array(
            'is_used' => array(
                'type' => 'BOOLEAN',
                'default' => false,
                'after' => 'id_super_admin'
            )
        );
        // add column
        $this->dbforge->add_column('reset_password_user', $column);
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->dbforge->drop_table('reset_password_user', true);
    }
}
