<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Field_Midtrans_Response_In_Table_Gadai_Logam_Mulia_And_Perhiasan extends CI_Migration
{
    public function up()
    {
        // define colomn for modify table
        $column = array(
            'midtrans_response' => array(
                'type' => 'TEXT',
                'null' => true
            ),
        );

        // add column
        $this->dbforge->add_column('gadai_perhiasan', $column);

        // add column
        $this->dbforge->add_column('gadai_logam_mulia', $column);
    }

    public function down()
    {
        //
    }
}
