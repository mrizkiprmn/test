<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Table_Booking_Arrum_Haji_Agunan extends CI_Migration
{
    public function up()
    {
        // this up() migration is auto-generated, please modify it to your needs
        // Drop table 'table_name' if it exists
        $this->dbforge->drop_table('booking_arrum_haji_agunan', true);

        // Table structure for table 'table_name'
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'MEDIUMINT',
                'constraint' => '8',
                'unsigned' => true,
                'auto_increment' => true
            ),
            'booking_id' => array(
                'type' => 'BIGINT',
                'constraint' => '25'
            ),
            'label' => array(
                'type' => 'varchar',
                'constraint' => '255'
            ),
            'jenis_agunan' => array(
                'type' => 'varchar',
                'constraint' => '255'
            ),
            'tipe_jaminan' => array(
                'type' => 'varchar',
                'constraint' => '50'
            ),
            'karat' => array(
                'type' => 'int',
                'constraint' => '50'
            ),
            'gram' => array(
                'type' => 'varchar',
                'constraint' => '50'
            ),
            'jenis_perhiasan' => array(
                'type' => 'varchar',
                'constraint' => '50',
                'null' => true
            ),
            'kode_perhiasan' => array(
                'type' => 'varchar',
                'constraint' => '255',
                'null' => true
            ),
            'nilai_agunan' => array(
                'type' => 'varchar',
                'constraint' => '255'
            ),
            'quantity' => array(
                'type' => 'varchar',
                'constraint' => '255'
            )
        ));
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('booking_arrum_haji_agunan');
    }

    public function down()
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->dbforge->drop_table('booking_arrum_haji_agunan', true);
    }
}
