<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Column_Start_Date_Debit_In_Table_History_Register_Auto_Debit extends CI_Migration
{
    public function up()
    {
        $column = [
            'start_date_debit' => [
                'type' => 'DATETIME',
                'null' => false,
                'after' => 'exp_regis'
            ],
            'date_unreg_debit' => [
                'type' => 'DATETIME',
                'null' => true,
                'after' => 'date_active_debit'
            ],
            'date_next_debit' => [
                'type' => 'DATETIME',
                'null' => true,
                'after' => 'status_debit'
            ],
            'kode_bank' => [
                'type' => 'varchar',
                'constraint' => '5',
                'null' => false,
                'after' => 'no_kredit'
            ],
        ];
        $this->dbforge->add_column('history_register_autodebit', $column);
    }

    public function down()
    {
    }
}
