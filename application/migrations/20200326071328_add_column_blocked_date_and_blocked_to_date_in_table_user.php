<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * @property mixed dbforge
 */
class Migration_Add_Column_Blocked_Date_And_Blocked_To_Date_In_Table_User extends CI_Migration
{
    public function up()
    {
        $column = [
            'blocked_date' => [
                'type' => 'DATETIME',
                'null' => true,
            ],
            'blocked_to_date' => [
                'type' => 'DATETIME',
                'null' => true,
            ],
        ];

        $this->dbforge->add_column('user', $column);
    }

    public function down()
    {
        //
    }
}
