<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_Column_Title_And_Image_In_Tabel_Ref_Bank extends CI_Migration
{
    public function up()
    {
        $column = [
            'title' => [
                'type' => 'varchar',
                'constraint' => '30',
                'after' => 'showOnGcash'
            ],
            'image' => [
                'type' => 'varchar',
                'constraint' => '30',
                'after' => 'title'
            ]
        ];

        $this->dbforge->add_column('ref_bank', $column);
    }

    public function down()
    {
    }
}
