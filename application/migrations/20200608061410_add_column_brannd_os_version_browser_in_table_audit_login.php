<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * @property mixed dbforge
 */
class Migration_Add_Column_Brannd_Os_Version_Browser_In_Table_Audit_Login extends CI_Migration
{
    public function up()
    {
        $this->dbforge->add_column('audit_login', [
            'browser' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => true,
                'after' => 'timezone',
            ],
            'os_version' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => true,
                'after' => 'timezone',
            ],
            'brand' => [
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => true,
                'after' => 'timezone',
            ]
        ]);
    }

    public function down()
    {
        //
    }
}
