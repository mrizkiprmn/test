<?php

/**
 * @property ConfigModel config_model
 * @property UserPinModel user_pin_model
 * @property EkycModel ekyc_model
 * @property User user_model
 * @property Emas_service emas_service
 * @property Notification_service notification_service
 * @property RestSwitchingCustomer_service rest_switching_customer_service
 * @property RestSwitchingOtp_service rest_switching_otp_service
 * @property Response_service response_service
 */
class Profile_service extends MY_Service
{
    protected $response = [
        'code'    => 101,
        'status' => 'error',
        'message' => 'Terjadi kesalahan mohon coba beberapa saat lagi.',
        'data' => null
    ];

    /**
     * Profile_service constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('ConfigModel', 'config_model');
        $this->load->model('UserPinModel', 'user_pin_model');
        $this->load->model('User', 'user_model');

        $this->load->model('AuditLogModel', 'audit_log_model');
        $this->load->model('EkycModel', 'ekyc_model');
        $this->load->service('Gpoint_service', 'gpoint_service');
        $this->load->service('RestSwitchingPortofolio_service', 'rest_switching_portofolio_service');
        $this->load->service('AuditLog_service', 'audit_log_service');
        $this->load->service('Emas_service', 'emas_service');
        $this->load->service('Notification_service', 'notification_service');
        $this->load->service('RestSwitchingCustomer_service', 'rest_switching_customer_service');
        $this->load->service('RestSwitchingOtp_service', 'rest_switching_otp_service');
        $this->load->service('Response_service', 'response_service');
    }

    public function checkUser($user_id)
    {
        try {
            $user_profile = $this->user_model->profile($user_id);
            $body['cif'] = $user_profile->cif;
            $rest_customer = $this->rest_switching_customer_service->customerInquiry($body);
            $tab_emas = $this->emas_service->listAccountNumber($body['cif']);

            if ($rest_customer['responseCode'] == '00') {
                $customer = json_decode($rest_customer['data']);

                $user_profile->noKTP             = $customer->noIdentitas;
                $user_profile->noIdentitas       = $customer->noIdentitas;
                $user_profile->jenisKelamin      = $customer->jenisKelamin;
                $user_profile->tempatLahir       = $customer->tempatLahir;
                $user_profile->tglLahir          = $customer->tglLahir;
                $user_profile->nama              = $customer->namaNasabah;
                $user_profile->namaIbu           = !empty($customer->ibuKandung) ? $customer->ibuKandung : '';
                $user_profile->jenisIdentitas    = !empty($customer->tipeIdentitas) ? $customer->tipeIdentitas : '';
                $user_profile->agama             = !empty($customer->agama) ? $customer->agama : '';
            }

            $user_profile->totalSaldoSeluruh = $tab_emas['totalSaldoSeluruh'] ?? '';
            $user_profile->ekyc_match_score = -1;
            $ekyc = $this->ekyc_model->getbyUser($user_id);

            if (!empty($ekyc->response_fr_dukcapil)) {
                $response_fr_dukcapil = json_decode($ekyc->response_fr_dukcapil);
                $response_fr_dukcapil->data = json_decode($response_fr_dukcapil->data);
                $user_profile->ekyc_match_score = $response_fr_dukcapil->data->matchScore ?? -1;
            }

            $this->response['code']    = 200;
            $this->response['status']  = 'success';
            $this->response['message'] = 'Check Data User';
            $this->response['data']    = $user_profile;
        } catch (Exception $e) {
            log_message('debug', 'Error Service ' . __CLASS__ . ' ' . __FUNCTION__ . ' message => ' . $e->getMessage());
        }

        return $this->response;
    }

    public function resetPin($user, $pin, $otp, $request_type)
    {
        $payload_otp = [
            'cif'         => $user->cif,
            'flag'        => 'K',
            'noHp'        => $user->no_hp,
            'norek'       => $user->no_hp,
            'requestType' => $request_type,
            'token'       => $otp,
        ];

        $rest_otp  = $this->rest_switching_otp_service->otpValidate($payload_otp);
        $rest_code = $rest_otp['responseCode'] ?? null;

        if ($rest_code != 00) {
            $this->response['message'] = 'Reset Pin Gagal';
            $this->response['status']  = 'error';
            $this->response['data']    = $rest_otp;

            return $this->response;
        }

        $un_blocked_pin = $this->unBlockedPinFromCore($user->cif, $user->no_hp);

        if ($un_blocked_pin['status'] != 'success') {
            $this->response['message'] = 'Reset Pin Gagal';
            $this->response['status']  = 'error';

            return $this->response;
        }

        $this->user_model->updateUser($user->user_AIID, array(
            'pin' => md5($pin),
            'last_update_pin' => date('Y-m-d H:i:s'),
        ));

        $this->response['code']    = 200;
        $this->response['status']  = 'success';
        $this->response['message'] = 'Sukses Perbarui PIN';
        $this->response['data']    = null;

        return $this->response;
    }

    public function getProfile($user)
    {
        $list_tab_emas = $this->emas_service->listAccountNumber($user->cif);

        // get update data user
        $user = $this->user_model->profile($user->id);
        $user->tabunganEmas = $list_tab_emas;
        //$user->point = $this->gpoint_service->getCustomerTotalPoint($user->id);

        $this->response['message'] = 'Get data profile successfully';
        $this->response['status']  = 'success';
        $this->response['data']    = $user;

        return $this->response;
    }

    public function syncDataUserFromSwitching($user_id)
    {
        $user = $this->user_model->getUser($user_id);

        if (empty($user) || empty($user->cif)) {
            return false;
        }

        $rest_customer = $this->rest_switching_customer_service->customerInquiry(['cif' => $user->cif]);

        if ($rest_customer['responseCode'] !== '00') {
            return false;
        }

        $customer = json_decode($rest_customer['data']);

        $this->user_model->updateUser($user->user_AIID, [
            'no_ktp'               => $customer->noIdentitas,
            'jenis_identitas'      => $customer->tipeIdentitas,
            'jenis_kelamin'        => $customer->jenisKelamin,
            'nama'                 => $customer->namaNasabah,
            'nama_ibu'             => $customer->ibuKandung,
            'tempat_lahir'         => $customer->tempatLahir,
            'tgl_lahir'            => $customer->tglLahir,
            'is_dukcapil_verified' => $customer->isDukcapilVerified ?? 0
        ]);

        return true;
    }

    public function unBlockedPinFromCore($cif, $no_hp)
    {
        $user = $this->user_model->find(['cif' => $cif, 'no_hp' => $no_hp]);

        if (empty($user)) {
            $this->response['message'] = 'User not found!';
            return $this->response;
        }

        $user_pin = $this->user_pin_model->unBlockedPinByUserId($user->user_AIID);

        if (empty($user_pin)) {
            $this->response['message'] = 'Unblock user pin failed!';
            return $this->response;
        }

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Unblock user pin successfully!';

        return $this->response;
    }

    public function checkPinByCif($cif, $pin)
    {
        $user = $this->user_model->find(['cif' => $cif, 'aktifasiTransFinansial' => '1']);

        if (empty($user)) {
            $this->response['message'] = 'User not found!';
            return $this->response;
        }

        $is_valid_pin = $this->user_model->isValidPIN2($user->user_AIID, $pin);

        if (!$is_valid_pin) {
            $this->response['message'] = 'PIN tidak valid';
            return $this->response;
        }

        $this->response['status'] = 'success';
        $this->response['message'] = 'PIN Tersedia dan sudah di aktifasi finansial';

        return $this->response;
    }

    public function sendNotificationBlockedLoginOneHour($user)
    {
        $count = $this->user_model::MIN_WRONG_PASSWORD;
        $time_min_wrong_password = $this->config_model->whereByVariable('min_wrong_password');
        $time = $time_min_wrong_password->value_3 ?? '1 Jam';

        $message = "Akun Pegadaian Digital kamu diblokir selama {$time} karena gagal melakukan login sebanyak {$count} kali.";

        $this->rest_switching_otp_service->otpSend([
            'noHp' => $user->no_hp,
            'requestType' => 'block-login-hour'
        ]);

        $this->sendNotificationBlockedLogin($user, $message);
    }

    public function sendNotificationBlockedLoginOneDay($user)
    {
        $time_max_wrong_password = $this->config_model->whereByVariable('max_wrong_password');
        $time = $time_max_wrong_password->value_3 ?? '24 Jam';

        $count = $this->user_model::MAX_WRONG_PASSWORD;
        $message = "Akun Pegadaian Digital kamu diblokir selama {$time} karena gagal melakukan login sebanyak {$count} kali. ";

        $this->rest_switching_otp_service->otpSend([
            'noHp' => $user->no_hp,
            'requestType' => 'block-login-day'
        ]);

        $this->sendNotificationBlockedLogin($user, $message);
    }

    public function sendNotificationBlockedPin($user)
    {
        $message = "PIN Pegadaian Digital kamu diblokir karena gagal melakukan input PIN sebanyak 3 kali. 
        Untuk sementara semua kegiatan transaksi yang membutuhkan PIN, tidak dapat kamu lakukan hingga PIN kembali aktif.";

        $data = new stdClass();
        $data->user = $user;
        $data->title = 'Notifikasi Keamanan Pemblokiran PIN Pegadaian Digital';
        $data->text = 'Notifikasi Keamanan Pemblokiran PIN Pegadaian Digital';
        $data->message = $message;
        $data->last_access_time = date('d M Y H:i');
        $data->email_notification = 'mail/info/email_blocked_pin';

        $this->rest_switching_otp_service->otpSend([
            'noHp' => $user->no_hp,
            'requestType' => 'block-pin'
        ]);

        $this->notification_service->sendNotificationEmail($data);
    }

    public function setPrimaryRekening($token_id, $norek)
    {
        // Get user data
        $user = $this->user_model->getUser($token_id);

        // Set Rekening utama
        $fields = [
            'norek_utama' => $norek
        ];
        $this->user_model->updateUser($token_id, $fields);

        return $this->emas_service->listAccountNumber($user->cif);
    }

    public function checkPinUser($user_id, $request)
    {
        $user = $this->user_model->getUser($user_id);

        if (empty($user)) {
            $this->response['message'] = 'User not found!';
            return $this->response;
        }

        if (empty($user->pin)) {
            $this->response['message'] = "Akun anda belum aktivasi PIN.";
            return $this->response;
        }

        $is_valid_pin = $this->user_model->isValidPIN2($user_id, $request['pin'] ?? null);

        if (!$is_valid_pin) {
            $user_pin = $this->user_pin_model->find(['user_id' => $user_id]);
            $counter  = $this->user_model::LIMIT_MIN_WRONG_PIN - $user_pin->counter;
            $this->response['message'] = "PIN salah. Kamu memiliki kesempatan {$counter} kali lagi.";
            return $this->response;
        }

        $this->response['status'] = 'success';
        $this->response['message'] = 'PIN Tersedia dan sudah di aktifasi finansial';

        return $this->response;
    }

    public function updatePinUser($user_id, $request, $use_check_pin = true)
    {
        $check_pin = $this->checkPinUser($user_id, $request);
        if ($use_check_pin && $check_pin['status'] != 'success') {
            return $check_pin;
        }

        $validation_pin = $this->validatePin($user_id, $request);
        if ($validation_pin['status'] != 'success') {
            return $validation_pin;
        }

        $user = $this->user_model->getUser($user_id);
        $new_pin = md5($request['new_pin']);
        $this->user_model->updateUser($user_id, [
            'pin' => $new_pin,
            'last_update_pin' => date('Y-m-d H:i:s'),
        ]);

        $this->audit_log_service->logResetPinPds(
            $user_id,
            $user->cif,
            $user->nama,
            $user->no_hp,
            $user->pin,
            $new_pin
        );

        $this->response['status']  = 'success';
        $this->response['message'] = 'PIN Berhasil Diubah';
        $this->response['data']    = ['text' => 'Selamat! kamu berhasil mengubah pin akun Pegadaian Digital.'];

        return $this->response;
    }

    public function checkOtpPinForget($user_id, $request)
    {
        $user = $this->user_model->getUser($user_id);
        $rest_otp = $this->rest_switching_otp_service->otpValidate([
            'cif'         => $user->cif,
            'flag'        => 'K',
            'noHp'        => $user->no_hp,
            'norek'       => $user->no_hp,
            'requestType' => 'pin-reset-pds',
            'token'       => $request['otp'],
        ]);

        if (($rest_otp['responseCode'] ?? null) != 00) {
            $this->response['message'] = 'OTP yang dimasukan tidak valid';
            $this->response['status']  = 'error';
            $this->response['data']    = $rest_otp;

            return $this->response;
        }

        $this->response['status']  = 'success';
        $this->response['message'] = 'OTP yang dimasukan valid!';

        return $this->response;
    }

    public function forgetPinUser($user_id, $request)
    {
        if ($request['new_pin'] != $request['new_pin_confirmation']) {
             $this->response['message'] = 'Masukan PIN yang sama dengan sebelumnya';
            return $this->response;
        }

        $user = $this->user_model->getUser($user_id);
        $this->unBlockedPinFromCore($user->cif, $user->no_hp);

        return $this->updatePinUser($user_id, $request, false);
    }

    public function validatePin($user_id, $request)
    {
        $this->response['status'] = 'error';
        $is_valid_increasing_pin = $this->validateIncreasingPin($request['new_pin']);
        if (!$is_valid_increasing_pin) {
            $this->response['message'] = 'PIN terlalu lemah. Hindari menggunakan angka berurut seperti 123456.';
            return $this->response;
        }

        $is_valid_repetition_pin = $this->validateRepetitionPin($request['new_pin']);
        if (!$is_valid_repetition_pin) {
            $this->response['message'] = 'PIN terlalu lemah. Hindari menggunakan angka sama seperti 111111.';
            return $this->response;
        }

        $this->response['status']  = 'success';
        $this->response['message'] = 'PIN sudah valid';

        return $this->response;
    }

    public function checkOtpPinCreate($user_id, $request)
    {
        $user = $this->user_model->getUser($user_id);
        $rest_otp = $this->rest_switching_customer_service->customerActivation([
            'cif'         => $user->cif,
            'token'       => $request['otp'],
            'username'    => $user->no_hp,
        ]);

        if (($rest_otp['responseCode'] ?? null) != 00) {
            $this->response['message'] = 'OTP yang dimasukan tidak valid';
            $this->response['status']  = 'error';
            $this->response['data']    = $rest_otp;

            return $this->response;
        }

        $this->response['status']  = 'success';
        $this->response['message'] = 'OTP yang dimasukan valid!';

        return $this->response;
    }

    public function createPinUser($user_id, $request)
    {
        if ($request['new_pin'] != $request['new_pin_confirmation']) {
            $this->response['message'] = 'Masukan PIN yang sama dengan sebelumnya';
            return $this->response;
        }

        $user = $this->user_model->getUser($user_id);
        $this->unBlockedPinFromCore($user->cif, $user->no_hp);

        $update_pin = $this->updatePinUser($user_id, $request, false);

        if ($update_pin['status'] != 'success') {
            return $update_pin;
        }

        // update activation finansial = 1
        $this->user_model->updateUser($user_id, ['aktifasiTransFinansial' => '1']);
        
        $this->audit_log_service->logAktifasiFinansialPds(
            $user->user_AIID,
            $user->cif,
            $user->nama,
            $user->no_hp,
            $user->aktifasiTransFinansial,
            '1'
        );

        $this->response['status']  = 'success';
        $this->response['message'] = 'PIN sudah valid';

        return $this->response;
    }

    public function isExistUser($no_hp, $cif = null) : array
    {
        $this->response = $this->response_service->getResponse('general.succeeded');
        $this->response['message'] = 'Check Exist User';
        $this->response['data'] = [
            'is_exist_user' => false,
            'user' => new stdClass()
        ];

        $query = ['no_hp' => $no_hp];

        if (!empty($cif)) {
            $query['cif'] = $cif;
        }

        if (empty($user = $this->user_model->find($query))) {
            return $this->response;
        }

        $this->response['data']['is_exist_user'] = (boolean) $user->is_open_te;
        $this->response['data']['user'] = [
            'phoneNumber' => $user->no_hp,
            'cif' => $user->cif
        ];

        return $this->response;
    }

    public function checkUserStatusLinkCif($no_hp)
    {
        try {
            $this->response['data'] = false;
            $check_profile = $this->user_model->find(['no_hp' => $no_hp]);

            if (empty($check_profile)) {
                $this->response['message'] = 'User dengan No Hp ' . $no_hp . ' tidak ditemukan!';
                return $this->response;
            }

            if (!empty($check_profile->cif) && !empty($check_profile->aktifasiTransFinansial)) {
                $this->response['message'] = 'User dengan No Hp ' . $no_hp . ' sudah link cif!';
                return $this->response;
            }

            $this->response['code'] = 200;
            $this->response['status'] = 'success';
            $this->response['message'] = 'Check Data Register User';
            $this->response['data'] = true;
        } catch (Exception $e) {
            Pegadaian::showError($e . getMessage());
        }
        return $this->response;
    }

    private function sendNotificationBlockedLogin($user, $message)
    {
        $data = new stdClass();
        $data->user = $user;
        $data->title = 'Notifikasi Keamanan Pegadaian Digital';
        $data->text = 'Notifikasi Keamanan Pegadaian Digital';
        $data->message = $message;
        $data->last_try_login = date('d M Y H:i');
        $data->email_notification = 'mail/info/email_blocked_login';

        $this->notification_service->sendNotificationEmail($data);
    }

    private function validateIncreasingPin($pin)
    {
        $regex_increasing = '/^(?=\d{6}$)1?2?3?4?5?6?7?8?9?0?$/';

        $validate = preg_match($regex_increasing, $pin);

        return empty($validate);
    }

    private function validateRepetitionPin($pin)
    {
        $regex_repetition = '/^(\d)\1+$/';

        $validate = preg_match($regex_repetition, $pin);

        return empty($validate);
    }
}
