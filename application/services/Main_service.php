<?php

/**
 * @property User user
 * @property Response_service response_service
 */
class Main_service extends MY_Service
{
    protected $pegadaian_helper;
    protected $response = [
        'code' => 101,
        'status' => 'error',
        'message' => 'Internal Server Error',
        'data' => null
    ];

    public function __construct()
    {
        parent::__construct();

        $this->load->model('User', 'user');
        $this->load->model('MasterModel', 'master_model');
        $this->load->helper('Pegadaian');
        $this->load->service('RestSwitchingPortofolio_service', 'rest_switching_portofolio_service');
        $this->load->service('RestSwitchingGcash_service', 'rest_switching_gcash_service');
        $this->load->service('Emas_service', 'emas_service');
        $this->load->service('Gcash_service', 'gcash_service');

        $this->pegadaian_helper = new Pegadaian();
    }

    /**
     * Method ini digunakan untuk mendapatkan message saat core mati baik karena eod maupun sebab lainnya.
     * Ketika method ini dipanggil, maka response code dan statusnya selalu mengembalikan nilai success (200)
     * @param $token : merupakan object yang berisi id dan channelId, etc.
     * @return array
     */
    public function getInfoCachedData($token)
    {
        /*------------------------------- Start | Get EOD parameters -------------------------------*/
        $current_time_parameter = "H:i:s";
        $start_time_parameter = $this->master_model->getParameters('eod_start_time');
        $end_time_parameter = $this->master_model->getParameters('eod_end_time');
        $last_update_format_parameter = "d F Y | H:i";

        $borderTime = $this->getBorderTime($current_time_parameter, $start_time_parameter, $end_time_parameter);

        $eod_start_time = strtotime($borderTime['start_result']);
        $eod_end_time = strtotime($borderTime['end_result']);
        $current_time = strtotime(date($current_time_parameter));

        log_message('debug', 'start time: ' . $borderTime['start_result'] . ' -> ' . $eod_start_time);
        log_message('debug', 'end time: ' . $borderTime['start_result'] . ' -> ' . $eod_end_time);
        log_message('debug', 'current time: ' . date("Y-m-d " . date($current_time_parameter)) . ' -> ' . $current_time);
        /*------------------------------- End | Get EOD parameters -------------------------------*/

        $beranda_message_inside_eod = "Transaksi digital dapat dilakukan pukul " . $end_time_parameter . " - " . $start_time_parameter . ".";
        $beranda_message_outside_eod = "Maaf, sedang dilakukan perbaikan sistem. Silahkan coba beberapa saat lagi.";
        $riwayat_message_inside_eod = "Update riwayat dapat dilihat pukul " . $end_time_parameter . " - " . $start_time_parameter . ".";

        $user = $this->user->getUser($token->id);

        $rest_params = [
            'channelId' => $token->channelId,
            'noHp' => $user->no_hp,
            'flag' => 'K'
        ];

        $rest_gcash_rekening = $this->rest_switching_gcash_service->gcashRekening($rest_params);
        $rest_tab_emas = $this->rest_switching_portofolio_service->pdsTabEmas(['cif' => $user->cif]);

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Data successfully loaded';

        log_message('debug', 'gcash' . json_encode($rest_gcash_rekening));
        log_message('debug', 'tabemas' . json_encode($rest_tab_emas));

        // Kosongkan message ketika core dalam kondisi baik
        if (($rest_gcash_rekening['responseCode'] == '00' || $rest_gcash_rekening['responseCode'] == '14') && ($rest_tab_emas['responseCode'] == '00' || $rest_tab_emas['responseCode'] == '14' || $rest_tab_emas['responseCode'] == '30')) {
            $this->response['data']['beranda'] = null;
            $this->response['data']['riwayat'] = null;

            return $this->response;
        }

        $tab_emas = $this->emas_service->listAccountNumber($user->cif);
        $va_gcash = $this->gcash_service->getVirtualAccount($token, $user->no_hp);

        $is_eod = (($current_time >= $eod_start_time) && ($current_time <= $eod_end_time));
        $beranda_message = ($is_eod) ? $beranda_message_inside_eod : $beranda_message_outside_eod;
        $riwayat_message = ($is_eod) ? $riwayat_message_inside_eod : $beranda_message_outside_eod;
        log_message('debug', 'beranda: ' . $beranda_message);
        log_message('debug', 'riwayat: ' . $riwayat_message);

        /**
         * Ketika core dalam kondisi mati, cek apakah nasabah sudah memiliki rekening TE dan GCash.
         * Apabila nasabah belum memiliki keduanya, maka kembalikan default message
         * */
        if (empty($tab_emas->listTabungan) && empty($va_gcash->listRekening)) {
            $this->response['data']['beranda'] = $beranda_message;
            $this->response['data']['riwayat'] = $riwayat_message;

            return $this->response;
        }

        $tab_emas_length = empty($tab_emas->listTabungan) ? 1 : count($tab_emas->listTabungan);
        $tab_emas_last_update = strtotime($tab_emas->listTabungan[$tab_emas_length - 1]->lastUpdate ?? date("Y-m-d H:i:s"));

        $va_gcash_length = empty($va_gcash->listRekening) ? 1 : count($va_gcash->listRekening);
        $va_gcash_last_update = strtotime($va_gcash->listRekening[$va_gcash_length - 1]->lastUpdate ?? date("Y-m-d H:i:s"));

        log_message('debug', 'tab_emas_last_update' . $tab_emas_last_update);
        log_message('debug', 'va_gcash_last_update' . $va_gcash_last_update);

        $last_update = date($last_update_format_parameter, (($tab_emas_last_update < $va_gcash_last_update) ? $tab_emas_last_update : $va_gcash_last_update));
        $formated_last_update = $this->pegadaian_helper->getLocalization($last_update);

        log_message('debug', 'result_last_update' . $last_update);
        $beranda_message = sprintf(($beranda_message . ' Update saldo terakhir %s'), $formated_last_update);

        $this->response['data']['beranda'] = $beranda_message;
        $this->response['data']['riwayat'] = $riwayat_message;

        return $this->response;
    }

    public function getBorderTime($current, $start, $end)
    {
        $current = date($current);
        $start = date($start);
        $end = date($end);

        $data_interval = new DateInterval('P1D');
        $start_result = new DateTime('now');
        $end_result = new DateTime('now');

        if (strtotime($start) < strtotime($end)) {
            return [
                "current" => $current,
                "start" => $start,
                "start_result" => $start_result->format("Y-m-d") . ' ' . $start,
                "end" => $end,
                "end_result" => $end_result->format("Y-m-d") . ' ' . $end
            ];
        }


        if ((strtotime($current) < strtotime($start)) && (strtotime($current) <= strtotime($end))) {
            $start_result->sub($data_interval);
        }


        if ((strtotime($current) > strtotime($end))) {
            $end_result->add($data_interval);
        }

        return [
            "current" => $current,
            "start" => $start,
            "start_result" => $start_result->format("Y-m-d") . ' ' . $start,
            "end" => $end,
            "end_result" => $end_result->format("Y-m-d") . ' ' . $end
        ];
    }

    public function getForceUpdatePassword($password): bool
    {
        $validation = Pegadaian::validationPassword($password);
        $is_force_update = true;

        if ($validation['is_valid']) {
            $is_force_update = false;
        }

        return $is_force_update;
    }
}
