<?php

class God_service extends MY_Service
{
    protected $response;

    public function __construct()
    {
        parent::__construct();

        // init load model
        $this->load->model('GadaiModel', 'gadai');
        $this->load->service('RestSwitchingGod_service', 'rest_switching_god_service');
        $this->response = [
            'code'    => 101,
            'status'  => 'error',
            'message' => 'Internal Server Error',
            'data'    => null
        ];
    }

    /**
     * Validasi JSON key
     * @param string $keys list key yang akan di cek
     * @param string $arr JSON yang di cek
     * @return bool|array return TRUE apabila seluruh key ada, dan return error bila terdapat key yang tidak ada
     */
    public static function validateJSON(array $keys, array $arr)
    {
        $array = array_diff_key(array_flip($keys), $arr);
        if (!empty($array)) {
            $error = [];
            foreach ($array as $key => $value) {
                $error[$key] = "" . $key . " field is required";
            }
            return $error;
        }
        return [];
    }

    public function payMidtransGod($god)
    {
        // init response
        $succeeded_response = [
            'status'  => 'success',
            'message' => 'Payment Midtrans'
        ];

        // checking midtrans data by id order midtrans
        $midtrans_data = $this->checkIdMidtrans($god);

        // if id order midtrans exist
        if ($midtrans_data) {
            $succeeded_response['data'] = $midtrans_data;

            return $succeeded_response;
        }

        // if id order midtrans not exist
        // create new midtrans order id
        $god['order_id'] = $god['id'] . Pegadaian::getMillSecID();
        $request = [
            'payment_type' => $god['payment_type'],
            'transaction_details' => array('order_id' => $god['order_id'], 'gross_amount' => $god['gross_amount']),
            'gopay' => array('enable_callback' => $god['enable_callback'], 'callback_url' => $god['callback_url']),
            'customer_details' => array('first_name' => $god['first_name'], 'last_name' => $god['last_name'], 'email' => $god['email'], 'phone' => $god['phone']),
            'item_details' => array('id' => $god['id'], 'price' => $god['price'], 'quantity' => $god['quantity'], 'name' => $god['name'])
        ];

        $rest_god_service = new RestSwitchingGod_service();
        $rest_god = $rest_god_service->payMidtransGod($request);
        $rest_god_data = json_decode($rest_god['data']);

        if ($rest_god['responseCode'] != 00) {
            return [
                'status'  => 'error',
                'message' => $rest_god['responseDesc'],
                'data'    => null
            ];
        }

        // save successfully created id order midtrans
        $this->gadai->updateOrderMidtrans(
            $god['id'],
            $this->getTableName($god['callback_url']),
            ['id_order_midtrans' => $god['order_id'], 'midtrans_response' => json_encode($rest_god)]
        );

        $succeeded_response['data'] = $rest_god_data;

        return $succeeded_response;
    }

    // godCancel fungsi untuk cancel order gojek
    public function godCancel($data)
    {
        if ($data['isCoreRegister'] == 'false') {
            $result = [
                'status'  => 'success',
                'message' => 'Transaksi anda telah dibatalkan',
                'data'    => []
            ];

            return $result;
        }

        $branchCode = $data['branchCode'] ?? '';

        if (!$branchCode) {
            $cekOutlet = $this->GadaiModel->check($data['kodeBooking'], $data['jenisTransaksi']);
            $branchCode = $cekOutlet->kode_outlet ?? '';
        }

        $request = [
            'kodeBooking' => $data['kodeBooking'],
            'flag' =>  'K',
            'branchCode' =>  $branchCode
        ];

        $rest_god_service = new RestSwitchingGod_service();
        $rest_cancel_god = $rest_god_service->godCancel($request);
        $rest_cancel_god_data = json_decode($rest_cancel_god['data']);

        $result = [
            'status'  => 'success',
            'message' => 'Cancel God',
            'data'    => $rest_cancel_god_data
        ];

        if ($rest_cancel_god['responseCode'] != 00) {
            $result =  [
                'status'  => 'error',
                'message' => $rest_cancel_god['responseDesc'],
                'data'    => null
            ];
        }
        return $result;
    }

    public function getData($kode_booking)
    {
        if (empty($kode_booking)) {
            return null;
        }

        $data = $this->gadai->getByKodeBookingPerhiasan($kode_booking);

        if (!empty($data)) {
            return $data;
        }

        $data = $this->gadai->getByKodeBookingLogamMulia($kode_booking);
        
        if (!empty($data)) {
            return $data;
        }
        
        return null;
    }

    private function checkIdMidtrans(array $god)
    {
        // get midtrans data
        $gadai = $this->gadai->getDataMidtrans($god['id'], $this->getTableName($god['callback_url']));

        if (empty($gadai->id_order_midtrans)) {
            return false;
        }

        // check id order midtrans status
        $rest_god_service = new RestSwitchingGod_service();
        $rest_god = $rest_god_service->statusMidtrans(['idOrderMidtrans' => $gadai->id_order_midtrans]);
        $rest_god_data = json_decode($rest_god['data']);

        if ($rest_god['responseCode'] != '00') {
            return false;
        }

        if ($rest_god_data->status == 'settlement') {
            return [ 'transaction_status' => $rest_god_data->status ];
        }

        if ($rest_god_data->status != 'pending') {
            return false;
        }

        // return last response midtrans data
        $midtransData = json_decode($gadai->midtrans_response);
        return json_decode($midtransData->data);
    }

    private function getTableName(string $callback_url)
    {
        if (strpos(strtolower($callback_url), 'perhiasan') !== false) {
            return 'gadai_perhiasan';
        }

        return 'gadai_logam_mulia';
    }
}
