<?php

class Master_service extends MY_Service
{
    const RESPONSE_GENERAL_SUCCESS = 'general.succeeded';

    protected $response = [
        'status' => 'error',
        'message' => 'Terjadi kesalahan mohon coba beberapa saat lagi.',
        'data' => null
    ];

    /**
     * AuditLog_service constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('MasterModel', 'master_model');
    }

    public function getByCategory($data)
    {
        $result = $this->master_model->getSubCategory($data['category']);

        if (!$result) {
            return $this->response;
        }

        $this->response = $this->response_service->getResponse($this::RESPONSE_GENERAL_SUCCESS);
        $this->response['message'] = 'Get FAQ By Category Berhasil';
        $this->response['data'] = $result;

        return $this->response;
    }

    public function getBySubCategory($data)
    {
        $result = $this->master_model->getBySubCategoryFaq($data['sub_category']);

        if (!$result) {
            return $this->response;
        }

        $this->response = $this->response_service->getResponse($this::RESPONSE_GENERAL_SUCCESS);
        $this->response['message'] = 'Get FAQ By Sub Category Berhasil';
        $this->response['data'] = $result;

        return $this->response;
    }

    public function searchFaq($data)
    {
        $result = $this->master_model->searchFaq($data['search_index']);

        if (!$result) {
            return $this->response;
        }

        $this->response = $this->response_service->getResponse($this::RESPONSE_GENERAL_SUCCESS);
        $this->response['message'] = 'Search FAQ Berhasil';
        $this->response['data'] = $result;

        return $this->response;
    }

    public function getAll($data)
    {
        $result = $this->master_model->getFaqWithLimit($data['limit']);

        if (!$result) {
            return $this->response;
        }

        $this->response = $this->response_service->getResponse($this::RESPONSE_GENERAL_SUCCESS);
        $this->response['message'] = 'Get All FAQ Berhasil';
        $this->response['data'] = $result;

        return $this->response;
    }

    public function authToken()
    {
        $headers = $this->input->request_headers();

        if (!Authorization::tokenIsExist($headers)) {
            return false;
        }

        if ($headers['Authorization'] != $this->config->item('core_password')) {
            return false;
        }

        return true;
    }
}
