<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\RequestOptions;

/**
 * @property ConfigModel config_model
 * @property Cache_service cache_service
 */
class RestSwitching_service extends MY_Service
{
    /**
     * @var
     */
    private $client_id;
    private $channel_id;
    private $default_response = [
        'responseCode' => 99,
        'responseMessage' => 'Terjadi Kesalahan Client RestSwitching',
        'responseData' => null,
    ];

    /**
     * RestSwitching_service constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('ConfigModel', 'config_model');
        $this->load->service('Cache_service', 'cache_service');
        $this->load->helper('Pegadaian');
    }

    // Function PostData to restSwitching
    public function postData($url, $data, $counter = 0, $channel_id = '', $client_id = '')
    {
        try {
            $client = $this->client($counter);

            if (empty($channel_id)) {
                $data['channelId'] = $this->channel_id;
            }

            if (empty($client_id)) {
                $data['clientId'] = $this->client_id;
            }

            $rest_response = $client->request('POST', $url, [RequestOptions::JSON => $data,]);
        } catch (ClientException $e) {
            $rest_response = $e->getResponse();
        } catch (ConnectException $e) {
            $rest_response = $e->getResponse();
        } catch (Exception $e) {
            $message = $e->getMessage();
            $this->default_response['responseData'] = $message;
        }

        $response = !empty($rest_response) ? json_decode($rest_response->getBody(), true) : null;
        $check_response_invalid = $response['error'] ?? null;

        log_message('debug', 'start of CALL RestSwitching URL : ' . $url, $data);
        log_message('debug', 'end of CALL RestSwitching URL : ' . $url, $response ?? $this->default_response['responseData']);

        if (empty($response['responseCode'])) {
            $response = $this->default_response;
        }

        $counter++;
        $new_request = $this->refreshToken($check_response_invalid, $url, $data, $counter);

        if ($new_request) {
            return $new_request;
        }

        return empty($response) ? $this->default_response : $response;
    }

    /**
     * Function get core token
     *
     * @return string|null
     */
    public function token()
    {
        // get from cache data first
        $key = implode(':', [ $this->cache_service::PREFIX_KEY, $this->cache_service::TOKEN_SWITCHING ]);
        $token = $this->redis->get($key);

        if (!empty($token)) {
            return $token;
        }

        return $this->getToken();
    }

    /**
     * Function get token call rest switching oauth/token
     *
     * @return mixed
     */
    public function getToken()
    {
        // cek session availability
        $data = [
            'username'   => $this->config->item('core_post_username'),
            'password'   => $this->config->item('core_post_password'),
            'grant_type' => 'password',
        ];

        $client = new Client([
            'base_uri' => $this->config->item('core_API_URL'),
            'headers' => [
                'Authorization' => $this->config->item('core_auth_header'),
                'Content-Type' => 'application/x-www-form-urlencoded',
            ]
        ]);

        $response = $client->post('oauth/token', [
            RequestOptions::FORM_PARAMS => $data
        ]);

        $rest_response = json_decode($response->getBody()->getContents());
        $token = $rest_response->access_token ?? null;
        $expires = $rest_response->expires_in ?? null;

        if (!empty($token) && !empty($expires)) {
            $token = $this->cache_service->setThenGet($this->cache_service::TOKEN_SWITCHING, $token, $expires);
        }

        return $token;
    }

    private function client($counter = 0)
    {
        $token = $this->token();

        if ($counter == 2) {
            Pegadaian::showError('Token Invalid after retry !');
        }

        if (empty($token)) {
            show_error('Token Rest Switching not found !', 400);
        }

        $this->client_id = $this->config->item('core_post_username');
        $this->channel_id = '6017';

        return new Client([
            'base_uri' => $this->config->item('core_API_URL'),
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $token,
                'Content-Type' => 'application/json'
            ],
        ]);
    }

    private function refreshToken($response_error, $url, $data, $counter)
    {
        if ($response_error == 'invalid_token') {
            $this->getToken();
            return $this->postData($url, $data, $counter);
        }

        return false;
    }
}
