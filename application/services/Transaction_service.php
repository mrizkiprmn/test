<?php

/**
 * @property BankModel bank_model
 * @property RestSwitchingGtef_service rest_switching_gtef_service
 * @property TransactionModel transaction_model
 * @property TransactionGtefModel transaction_gtef_model
 * @property User user_model
 * @property Gtef_service gtef_service
 */
class Transaction_service extends MY_Service
{
    /* Default Response */
    protected $response = [
        'code' => 101,
        'status' => 'error',
        'message' => 'Terjadi kesalahan, mohon coba beberapa saat lagi.',
        'data' => null
    ];

    /**
     * Transaction_service constructor.
     */
    public function __construct()
    {
        parent::__construct();
        // Load Helper
        $this->load->helper('Pegadaian');
        // Load Model
        $this->load->model('BankModel', 'bank_model');
        $this->load->model('GcashModel', 'gcash_model');
        $this->load->model('TransactionModel', 'transaction_model');
        $this->load->model('TransactionGtefModel', 'transaction_gtef_model');
        $this->load->model('ApplyMtonlineModel', 'apply_mtonline_model');
        $this->load->model('User', 'user_model');
        $this->load->model('MasterModel', 'master_model');
        // Load Service
        $this->load->service('RestSwitchingGtef_service', 'rest_switching_gtef_service');
        $this->load->service('Gtef_service', 'gtef_service');
        $this->load->service('RestSwitchingMtonline_service', 'rest_switching_mtonline_service');
        $this->load->service('Notification_service', 'notification_service');
        $this->load->service('Response_service', 'response_service');
    }

    /**
     * Func dynamic inquiry by param product
     *
     * @param $user_id
     * @param $request
     * @return array
     */
    public function inquiry($user_id, $request)
    {
        try {
            $user = $this->user_model->profile($user_id);
            if (empty($user)) {
                $this->response['message'] = 'Data user not found!';
                return $this->response;
            }

            $trx_type_func = $this->getClassFuncByTrxType("inquiry", $request['product']);
            $this->$trx_type_func($user, $request);

            return $this->response;
        } catch (Exception $e) {
            log_message('debug', 'ERROR MESSAGE' . __FUNCTION__ . ' => ' . $e->getMessage());
            return $this->response;
        }
    }

    // Func inquiry trx gtef
    public function inquiryGtef($user, $request)
    {
        if ($request['payment_method'] === "BANK") {
            $is_user_bank = $this->bank_model->isUserBank($user->id, $request['bank_id']);
            $pembayaran_bank =  $request['bank_id'];
        }

        if ($request['payment_method'] === "GCASH") {
            /* ini nanti di pake [start]
                $is_user_bank = $this->gcash_model->isUserGcash($user->id, $request['bank_id']);
                $pembayaran_gcash =  $request['bank_id'];
                ini nanti di pake [end] */
                
            $this->response['message'] = 'Belum Mendukung Gcash';
            return $this->response;
            
        }

        if (empty($is_user_bank)) {
            $this->response['message'] = 'Data bank atau gcash tidak ditemukan';
            return $this->response;
        }

        $rest_gtef = $this->rest_switching_gtef_service->getRestDetailGtef(['noKontrak' => $request['account_number']]);
        
        if ($rest_gtef['responseCode'] != "00") {
            $this->response['message'] = $rest_gtef['responseDesc'] ?? $rest_gtef['responseMessage'] . " from RestSwitching";
            return $this->response;
        }

        $gtef = json_decode($rest_gtef['data']);

        $limit_pinjaman_perhari = $this->master_model->getValTaksiranGtef('max_pinjaman_perhari_gtef');
        if ($limit_pinjaman_perhari < ($gtef->pinjamanHariIni + $request['amount'])) {
            $this->response = $this->response_service->getResponse('gtefInquiry.pinjamanMelebihiLimitPinjamanPerhari');
            return $this->response;
        }

        if ($request['amount'] > $gtef->sisaLimit) {
            $this->response = $this->response_service->getResponse('gtefInquiry.pinjamanMelebihiSisaLimit');
            return $this->response;
        }

        // ini untuk fase selanjutnya (MT)
        // $min_gadai_titipan_emas = $this->master_model->getMinMaxAmountGtef('min_gadai_titipan_emas');
        // if ($request['jenis_transaksi'] == "MT" && $request['amount'] < ($gtef->sewaModalBerjalan + $gtef->tagihanDenda + $gtef->tagihanPerpanjang + $min_gadai_titipan_emas)) {
        //     $this->response = $this->response_service->getResponse('gtefInquiry.kewajibanMelebihiPinjaman');
        //     return $this->response;
        // }

        $trx  = $this->createOrUpdateDataInquiryGTEF(
            $user->id,
            [
                'user_id' => $user->id ?? null,
                'bank_id' => $pembayaran_bank ?? null,
                'gcash_id' => $pembayaran_gcash ?? null,
                'payment_method' => $request['payment_method'],
                'tef_no' => $request['account_number'],
                'amount' => $request['amount'],
                'code_outlet' => $gtef->kodeCabang,
                'estimated_value' => floatval($gtef->nilaiTaksiran),
                'tenor' => $request['tenor'] ?? $gtef->tenor,
                'trx_type' => $request['jenis_transaksi'],
                'trx_code' => $this->transaction_gtef_model::TRX_CODE,
            ]
        );

        if (empty($trx)) {
            return $this->response;
        }

        $payload = $this->setDataInquiryGtef($user, $trx, false);
        $rest_inquiry_open = $this->rest_switching_gtef_service->inquiryOpen($payload);

        if ($rest_inquiry_open['responseCode'] != "00") {
            $this->response['message'] = $rest_inquiry_open['responseDesc'] ?? $rest_inquiry_open['responseMessage'];
            return $this->response;
        }

        $inquiry_open = json_decode($rest_inquiry_open['data']);

        $inquiry_open_admin = $inquiry_open->administrasi ?? 0;
        $inquiry_open_up_approved = $inquiry_open->hakNasabah ?? 0;
        $minimal_up_approved = $this->master_model->getMinimalUpApprovedGTEF() ?? 0;
        
        if ($request['amount'] <= $inquiry_open_admin) {
            $this->response['message'] = "Pinjaman yang kamu ajukan lebih kecil dari Biaya Administrasi";
            return $this->response;
        }

        if ($inquiry_open_up_approved < $minimal_up_approved) {
            $this->response['message'] = "Transaksi tidak bisa dilanjutkan, karena pencairan kamu dibawah ".Pegadaian::currencyIdr($minimal_up_approved);
            return $this->response;
        }

        $inquiry_open_data = $this->updateDataInquiryGtef($trx, $inquiry_open, false);
        
        $response_data = [
            'trx_id' => $inquiry_open_data->reff_switching ?? null,
            'tanggal_bayar_sewa_modal' => ($inquiry_open_data->lease_payment_date != null) ? Pegadaian::formatDateDmy($inquiry_open_data->lease_payment_date) : null,
            'tef_no' => $inquiry_open_data->tef_no ?? null,
            'amount' => ($inquiry_open_data->amount != null) ? Pegadaian::currencyIdr($inquiry_open_data->amount) : Pegadaian::currencyIdr(0),
            'amount_approved' => ($inquiry_open_data->amount_approved != null) ? Pegadaian::currencyIdr($inquiry_open_data->amount_approved) : Pegadaian::currencyIdr(0),
            'rental_cost' => ($inquiry_open_data->rental_cost != null) ? Pegadaian::currencyIdr($inquiry_open_data->rental_cost) : Pegadaian::currencyIdr(0),
            'insurance_surcharge' => ($inquiry_open_data->insurance_surcharge != null) ? Pegadaian::currencyIdr($inquiry_open_data->insurance_surcharge) : Pegadaian::currencyIdr(0),
            'bank_name' => $inquiry_open_data->bank->bank_name,
            'bank_account_number' => $inquiry_open_data->bank->account_number,
            'bank_customer_name' => $inquiry_open_data->bank->customer_name,
            'bank_thumbnail' => $inquiry_open_data->bank->thumbnail,
        ];

        if (isset($request['tenor'])) {
            $response_data['admin_surcharge'] = ($inquiry_open_data->admin_surcharge != null) ? Pegadaian::currencyIdr($inquiry_open_data->admin_surcharge) : Pegadaian::currencyIdr(0);
            $response_data['jangka_waktu'] = $inquiry_open_data->tenor;
        }

        if (!empty($inquiry_open_data->rental_cost_fee)) {
            $response_data['tagihan_sewa_modal'] = ($inquiry_open_data->rental_cost_fee != null) ? Pegadaian::currencyIdr($inquiry_open_data->rental_cost_fee) : Pegadaian::currencyIdr(0);
        }

        if (!empty($inquiry_open_data->deposit_extension_fee)) {
            $response_data['tagihan_perpanjang_titipan'] = ($inquiry_open_data->deposit_extension_fee != null) ? Pegadaian::currencyIdr($inquiry_open_data->deposit_extension_fee) : Pegadaian::currencyIdr(0);
        }

        if (!empty($inquiry_open_data->late_charge_rental_cost)) {
            $response_data['tagihan_denda_keterlambatan_SM'] = ($inquiry_open_data->late_charge_rental_cost != null) ? Pegadaian::currencyIdr($inquiry_open_data->late_charge_rental_cost) : Pegadaian::currencyIdr(0);
        }

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Submit Transaction Successfully';
        $this->response['data'] = $response_data;

        return $this->response;
    }

    // Func inquiry trx mtonline
    public function inquiryMtonline($user, $request)
    {
        if ($request['payment_method'] === "BANK") {
            $is_user_bank = $this->bank_model->isUserBank($user->id, $request['bank_id']);
            $pembayaran_bank =  $request['bank_id'];
        }

        if ($request['payment_method'] === "GCASH") {
            /* ini nanti di pake [start]
                $is_user_bank = $this->gcash_model->isUserGcash($user->id, $request['bank_id']);
                $pembayaran_gcash =  $request['bank_id'];
                ini nanti di pake [end] */
                
            $this->response['message'] = 'Belum Mendukung Gcash';
            return $this->response;
        }

        if (empty($is_user_bank)) {
            $this->response['message'] = 'Data bank atau gcash tidak ditemukan';
            return $this->response;
        }
        
        $trx_code = ($request['product_code'] === "01") ?  $this->apply_mtonline_model::TRX_CODE_KCA : $this->apply_mtonline_model::TRX_CODE_TITIPAN_EMAS;
        $type =  ($request['product_code'] === "01") ?  $this->apply_mtonline_model::TYPE_KCA : $this->apply_mtonline_model::TYPE_TITIPAN_EMAS;
        
        $trx  = $this->createOrUpdateDataInquiry(
            [
                'user_id' => $user->id,
                'type' => $type,
                'trx_code' => $trx_code,
                'trx_id' => strtotime(date('Y-m-d H:i:s')) . random_int(1000000, 9999999)
            ],
            [
                'bank_id' => $pembayaran_bank ?? null,
                'gcash_id' => $pembayaran_gcash ?? null,
                'metode_pencairan' => $request['payment_method'],
                'contract_number' => $request['account_number'],
                'amount' => $request['amount'],
                'code_outlet' => $request['code_outlet'],
                'estimated_value' => floatval($request['total_taksiran'])
            ],
            'apply_mtonline',
            $this->apply_mtonline_model
        );

        if (empty($trx)) {
            return $this->response;
        }

        $trx_mtonline = $this->apply_mtonline_model->find_inquiry(['id' => $trx->relation_id]);
        $payload = $this->setDataMtonline($user, $trx_mtonline, $trx, false);
        $rest_inquiry_open = $this->rest_switching_mtonline_service->inquiryOpen($payload);

        if ($rest_inquiry_open['responseCode'] === "13") {
            $this->response['code'] = 200;
            $this->response['status'] = 'success';
            $this->response['message'] = 'Transaksi Melebihi Limit';
            $this->response['data'] = [
                'title' => 'Transaksi Melebihi Limit',
                'desc' => $rest_inquiry_open['responseDesc'] ?? "Transaksi kamu melebihi limit tambahan pinjaman 500 juta per hari",
                'button' => [
                    [
                        'title' => 'Oke',
                        'callback' => null,
                        'color' => 'green',
                    ]
                ],
            ];

            return $this->response;
        }

        if ($rest_inquiry_open['responseCode'] != "00") {
            $this->response['message'] = $rest_inquiry_open['responseDesc'] ?? "Gagal Inquiry Data";
            return $this->response;
        }

        $inquiry_open = json_decode($rest_inquiry_open['data']);
        $inquiry_open_data = $this->updateDataInquiryMtonline($trx, $trx_mtonline, $inquiry_open);

        $response_data = [
            'trx_id' => $inquiry_open_data->trx->trx_id ?? null,
            'date' => isset($inquiry_open_data->trx->date) ? Pegadaian::formatDateLos($inquiry_open_data->trx->date) : null,
            'contract_number' => $inquiry_open_data->trx_mtonline->contract_number ?? null,
            'bank_name' => $inquiry_open_data->trx_mtonline->bank->bank_name ?? $inquiry_open_data->trx_mtonline->bank->namaBank,
            'bank_account_number' => $inquiry_open_data->trx_mtonline->bank->account_number ?? $inquiry_open_data->trx_mtonline->bank->virtualAccount,
            'bank_customer_name' => $inquiry_open_data->trx_mtonline->bank->customer_name ?? $trx_mtonline->customer_name,
            'bank_thumbnail' => $inquiry_open_data->trx_mtonline->bank->thumbnail ?? null,
            'due_date' => $inquiry_open_data->trx_mtonline->due_date ?? null,
            'hari_real' => $inquiry_open_data->trx_mtonline->jumlah_hari_real ?? null,
            'hari_tarif' => $inquiry_open_data->trx_mtonline->jumlah_hari_tarif ?? null,
            'amount' => isset($inquiry_open_data->trx_mtonline->amount) ? Pegadaian::currencyIdr($inquiry_open_data->trx_mtonline->amount) : Pegadaian::currencyIdr('0'),
            'total_uang_pinjaman' => isset($inquiry_open->up) ? Pegadaian::currencyIdr($inquiry_open->up) : Pegadaian::currencyIdr('0'),
            'sewa_modal' => isset($inquiry_open_data->trx_mtonline->tagihan_sewa_modal) ? Pegadaian::currencyIdr($inquiry_open_data->trx_mtonline->tagihan_sewa_modal) : Pegadaian::currencyIdr('0'),
            'administrasi' => isset($inquiry_open_data->trx_mtonline->admin_surcharge) ? Pegadaian::currencyIdr($inquiry_open_data->trx_mtonline->admin_surcharge) : Pegadaian::currencyIdr('0'),
            'asuransi' => isset($inquiry_open_data->trx_mtonline->insurance_surcharge) ? Pegadaian::currencyIdr($inquiry_open_data->trx_mtonline->insurance_surcharge) : Pegadaian::currencyIdr('0'),
            'surcharge' => isset($inquiry_open_data->trx_mtonline->surcharge) ? Pegadaian::currencyIdr($inquiry_open_data->trx_mtonline->surcharge) : Pegadaian::currencyIdr('0'),
            'total_biaya' => isset($inquiry_open_data->trx_mtonline->total_surcharge) ? Pegadaian::currencyIdr($inquiry_open_data->trx_mtonline->total_surcharge) : Pegadaian::currencyIdr('0'),
            'amount_approved' => isset($inquiry_open_data->trx_mtonline->amount_approved) ? Pegadaian::currencyIdr($inquiry_open_data->trx_mtonline->amount_approved) : Pegadaian::currencyIdr('0'),
            'metode_pencairan' => $inquiry_open_data->trx_mtonline->metode_pencairan ?? null,
        ];

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Submit Transaction Successfully';
        $this->response['data'] = $response_data;

        return $this->response;
    }

    /**
     * Func dynamic submit by param product
     *
     * @param $user_id
     * @param $request
     * @return array
     */
    public function submit($user_id, $request)
    {
        try {
            $user = $this->user_model->profile($user_id);
            if (empty($user)) {
                $this->response['message'] = 'Data user tidak ditemukan';
                return $this->response;
            }

            $trx_type_func = $this->getClassFuncByTrxType("submit", $request['product']);
            $this->$trx_type_func($user, $request);

            return $this->response;
        } catch (Exception $e) {
            log_message('debug', 'ERROR MESSAGE' . __FUNCTION__ . ' => ' . $e->getMessage());
            return $this->response;
        }
    }

    // Func submit trx gtef
    public function submitGtef($user, $request)
    {
        $is_valid_pin = $this->user_model->isValidPIN2($user->id, $request['pin']);

        if (empty($is_valid_pin)) {
            $this->response['message'] = 'PIN is wrong!';
            return $this->response;
        }

        $trx = $this->transaction_gtef_model->find(['reff_switching' => $request['trx_id'], 'is_submit' => false]);

        if (empty($trx)) {
            $this->response['message'] = 'Data transaction not found!';
            return $this->response;
        }
        
        $payload = $this->setDataInquiryGtef($user, $trx, true);
        $submit_trx = $this->rest_switching_gtef_service->open($payload);
        
        if ($submit_trx['responseCode'] != "00") {
            $this->response['message'] = $submit_trx['responseDesc'] ?? $submit_trx['responseMessage'] . " from RestSwitching";
            return $this->response;
        }

        $gtef = json_decode($submit_trx['data']);
        $inquiry_open_data = $this->updateDataInquiryGtef($trx, $gtef, true);
        $this->notification_service->notifikasiGTEF($inquiry_open_data, "submit");

        $response_data = [
            'amount' => Pegadaian::currencyIdr($inquiry_open_data->amount),
            'bank_name' => $inquiry_open_data->bank->bank_name,
            'bank_account_number' => $inquiry_open_data->bank->account_number,
            'bank_customer_name' => $inquiry_open_data->bank->customer_name,
            'bank_thumbnail' => $inquiry_open_data->bank->thumbnail,
        ];

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Gadai Titipan Emas Fisik Berhasil';
        $this->response['data'] = $response_data;

        return $this->response;
    }

    public function submitMtonline($user, $request)
    {
        $is_valid_pin = $this->user_model->isValidPIN2($user->id, $request['pin']);

        if (empty($is_valid_pin)) {
            $this->response['message'] = 'PIN is wrong!';
            return $this->response;
        }

        $trx = $this->transaction_model->find(['trx_id' => $request['trx_id'], 'is_submit' => false]);

        if (empty($trx)) {
            $this->response['message'] = 'Data transaction not found!';
            return $this->response;
        }

        $trx_mtonline = $this->apply_mtonline_model->find(['id' => $trx->relation_id]);
        $payload = $this->setDataMtonline($user, $trx_mtonline, $trx, true);
        $submit_trx = $this->rest_switching_mtonline_service->open($payload);
        
        $text_match = 'Error : Saldo rekening';
        if ($submit_trx['responseCode'] == "500" && (preg_match("/{$text_match}/i", $submit_trx['responseDesc']))) {
            $this->response['message'] = "Transfer menggunakan bank ini sedang mengalami gangguan.";
            return $this->response;
        }

        if ($submit_trx['responseCode'] != "00") {
            $this->response['message'] = $submit_trx['responseDesc'] . " from RestSwitching";
            return $this->response;
        }

        $mtonline = json_decode($submit_trx['data']);
        $inquiry_open_data = $this->updateDataInquiryMtonline($trx, $trx_mtonline, $mtonline);
        $this->transaction_model->updateById($trx->id, ['is_submit' => true]);
        $this->notification_service->simpanNotifikasiMtonline($user, $inquiry_open_data);

        $response_data = [
            'amount' => Pegadaian::currencyIdr($inquiry_open_data->trx_mtonline->amount_approved),
            'bank_name' => $inquiry_open_data->trx_mtonline->bank->bank_name ?? $inquiry_open_data->trx_mtonline->bank->namaBank,
            'bank_account_number' => $inquiry_open_data->trx_mtonline->bank->account_number ?? $inquiry_open_data->trx_mtonline->bank->virtualAccount,
            'bank_customer_name' => $inquiry_open_data->trx_mtonline->bank->customer_name ?? $trx_mtonline->customer_name,
            'bank_thumbnail' => $inquiry_open_data->trx_mtonline->bank->thumbnail ?? null,
        ];

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Minta Tambah Online Berhasil';
        $this->response['data'] = $response_data;

        return $this->response;
    }

    /**
     * Func to get dynamic name class inquiry transaction
     *
     * @param $type "inquiry" || "submit"
     * @param $trx_type
     * @return string
     */
    private function getClassFuncByTrxType($type, $trx_type)
    {
        return $type . ucfirst($trx_type);
    }

    /**
     * Func to create or update data old transaction and transaction inquiry product
     *
     * @param $trx_data
     * @param $trx_product_data
     * @param $relation_table
     * @param $model_trans
     * @return object
     */
    private function createOrUpdateDataInquiry($trx_data, $trx_product_data, $relation_table, $model_trans)
    {
        $is_exist_trx = $this->transaction_model->find(['user_id' => $trx_data['user_id'], 'is_submit' => false, 'relation_table' => $relation_table]);
        
        if (!empty($is_exist_trx)) {
            $trx_product = $model_trans->updateById($is_exist_trx->relation_id, $trx_product_data);
            return $this->setTrxData($relation_table, $trx_product->id, $trx_data, $is_exist_trx->id);
        }

        $trx_product = $model_trans->save($trx_product_data);
        
        if (empty($trx_product)) {
            return null;
        }

        $trx = $this->setTrxData($relation_table, $trx_product->id, $trx_data);
        if (empty($trx)) {
            return null;
        }

        return $trx;
    }

    private function createOrUpdateDataInquiryGTEF($user_id, $trx_product_data)
    {
        $is_exist_trx = $this->transaction_gtef_model->find(['user_id' => $user_id, 'is_submit' => false]);
       
        if (!empty($is_exist_trx)) {
            $trx_product = $this->transaction_gtef_model->updateById($is_exist_trx->id, $trx_product_data);
            return $trx_product;
        }

        $trx_product = $this->transaction_gtef_model->save($trx_product_data);
        
        if (empty($trx_product)) {
            return null;
        }

        return $trx_product;
    }

    /**
     * Func to set and store data trx to table transaction
     *
     * @param $relation_table
     * @param $relation_id
     * @param $trx_data
     * @param null $trx_id
     * @return array|bool|mixed
     */
    private function setTrxData($relation_table, $relation_id, $trx_data, $trx_id = null)
    {
        $trx_product = ['relation_table' => $relation_table, 'relation_id' => $relation_id];

        return $this->storeTrx($trx_product, $trx_data, $trx_id);
    }

    /**
     * Func for store data to table transaction
     *
     * @param array $trx_product
     * @param array $trx
     * @param null $trx_id
     * @return array|bool|mixed
     */
    private function storeTrx(array $trx_product, array $trx, $trx_id = null)
    {
        $trx['relation_id'] = $trx_product['relation_id'];
        $trx['relation_table'] = $trx_product['relation_table'];

        if (empty($trx['relation_id']) || empty($trx['relation_table'])) {
            return null;
        }

        if (!empty($trx_id)) {
            unset($trx['trx_id']);
            return $this->transaction_model->updateById($trx_id, $trx);
        }

        return $this->transaction_model->save($trx);
    }

    /**
     * Func set data inquiry | open gtef
     *
     * @param $user
     * @param $trx_gtef
     * @return array
     */
    private function setDataInquiryGtef($user, $trx, $is_submit = false)
    {
        if ($trx->payment_method === "BANK") {
            $bank = $this->bank_model->findById($trx->bank_id);

            $return = [
                'cif' => $user->cif,
                'flag' => 'K',
                'ibuKandung' => $user->namaIbu,
                'jenisKelamin' => $user->jenisKelamin,
                'jenisTransaksi' => $trx->trx_type,
                'norekBank' => $bank->account_number,
                'paymentMethod' => $trx->payment_method,
                'kodeBank' => $bank->bank_code,
                'kodeCabang' => $trx->code_outlet,
                'kodeProduk' => $trx->trx_code,
                'noIdentitas' => $user->noKTP,
                'namaNasabah' => $user->nama,
                'namaNasabahBank' => $bank->customer_name,
                'rubrik' => 'KT',
                'tanggalLahir' => $user->tglLahir,
                'tanggalPengajuan' => date('Y-m-d'),
                'totalTaksiran' => $trx->estimated_value,
                'up' => $trx->amount,
                'noTitipan' => $trx->tef_no,
                'tenor' => $trx->tenor,
                "kodeBankPembayar" => $bank->bank_code,
                "tagihanSewaModal" => $trx->rental_cost_fee ?? "0",
                "tagihanPerpanjangan" => $trx->deposit_extension_fee ?? "0",
                "tagihanDenda" => $trx->late_charge_rental_cost ?? "0",
            ];
        }

        if ($trx->payment_method === "GCASH") {
            $gcash = $this->gcash_model->getVADetails($trx->gcash_id);

            $return = [
                'cif' => $user->cif,
                'flag' => 'K',
                'ibuKandung' => $user->namaIbu,
                'jenisKelamin' => $user->jenisKelamin,
                'jenisTransaksi' => $trx->trx_type,
                'paymentMethod' => $trx->payment_method,
                'kodeCabang' => $trx->code_outlet,
                'kodeProduk' => $trx->trx_code,
                'noIdentitas' => $user->noKTP,
                'namaNasabah' => $user->nama,
                'rubrik' => 'KT',
                'tanggalLahir' => $user->tglLahir,
                'tanggalPengajuan' => date('Y-m-d'),
                'totalTaksiran' => $trx->estimated_value,
                'up' => $trx->amount,
                "gcashId" => $gcash->virtualAccount,
                'noTitipan' => $trx->tef_no,
                'tenor' => $trx->tenor,
                "kodeBankPembayar" => "",
                "tagihanSewaModal" => $trx->rental_cost_fee ?? "0",
                "tagihanPerpanjangan" => $trx->deposit_extension_fee ?? "0",
                "tagihanDenda" => $trx->late_charge_rental_cost ?? "0",
            ];
        }

        if ($is_submit) {
            $return["reffIdSwitching"] = $trx->reff_switching ?? null;
            $return["jenisTransaksi"] = "OP";
        }

        return $return;
    }

    private function setDataMtonline($user, $trx_mtonline, $trx, $is_submit = false)
    {
        if ($trx_mtonline->metode_pencairan === "BANK") {
            $bank = $this->bank_model->findById($trx_mtonline->bank_id);

            $return = [
                "noKredit" => $trx_mtonline->contract_number,
                "up" => $trx_mtonline->amount,
                "totalTaksiran" => $trx_mtonline->estimated_value,
                "rubrik" => "KT",
                "tenor" => "120",
                "kodeBank" => $bank->bank_code,
                "norekBank" => $bank->account_number,
                "namaNasabahBank" => $bank->customer_name,
                "kodeCabang" => $trx_mtonline->code_outlet,
                "cif" => $user->cif,
                "paymentMethod" => $trx_mtonline->metode_pencairan,
                "kodeBankPembayar" => $bank->bank_code,
                "jenisTransaksi" => "MT"
            ];
        }

        if ($trx_mtonline->metode_pencairan === "GCASH") {
            $gcash = $this->gcash_model->getVADetails($trx_mtonline->gcash_id);

            $return = [
                "noKredit" => $trx_mtonline->contract_number,
                "up" => $trx_mtonline->amount,
                "totalTaksiran" => $trx_mtonline->estimated_value,
                "rubrik" => "KT",
                "tenor" => "120",
                "kodeBank" => "",
                "gcashId" => $gcash->virtualAccount,
                "kodeCabang" => $trx_mtonline->code_outlet,
                "cif" => $user->cif,
                "paymentMethod" => $trx_mtonline->metode_pencairan,
                "kodeBankPembayar" => "",
                "jenisTransaksi" => "MT"
            ];
        }

        if ($is_submit) {
            $return["reffIdSwitching"] = $trx->reff_switching ?? null;
        }

        return $return;
    }

    /**
     * Func update data inquiry | open gtef to table trx gtef
     *
     * @param $trx
     * @param $trx_gtef
     * @param $gtef
     * @return mixed|null
     */
    private function updateDataInquiryGtef($trx, $gtef, $is_submit = false)
    {
        // update data trx gtef
        $arr_update = [
            'customer_name' => $gtef->namaNasabah ?? null,
            'rental_cost' => $gtef->sewaModal ?? null,
            'rental_cost_fee' => $gtef->tagihanSewaModal ?? null,
            'late_charge_rental_cost' => $gtef->tagihanDenda ?? null,
            'cost' => $gtef->tarif ?? null,
            'lease_payment_date' => $gtef->tglBayarSM ?? null,
            'credit_date' => $gtef->tglKredit ?? null,
            'auction_date' => $gtef->tglLelang ?? null,
            'due_date' => $gtef->tglJatuhTempo ?? null,
            'auction_surcharge' => $gtef->biayaLelang ?? null,
            'insurance_surcharge' => $gtef->asuransi ?? null,
            'admin_surcharge' => $gtef->administrasi ?? null,
            'transfer_surcharge' => $gtef->surcharge ?? null,
            'deposit_cost' => $gtef->biayaTitip ?? null,
            'deposit_extension_fee' => $gtef->tagihanPerpanjangan ?? null,
            'amount_approved' => $gtef->hakNasabah ?? null,
            'credit_number' => $gtef->norek ?? null,
            'reff_switching' => $gtef->reffSwitching ?? null,
            'reff_core' => $gtef->reffCore ?? null,
            'reff_biller' => $gtef->reffBiller ?? null,
        ];

        if ($is_submit) {
            $arr_update['is_submit'] = true;
        }
        
        $trx_data = $this->transaction_gtef_model->updateById($trx->id, $arr_update);

        $data = new stdClass();
        $data = $trx_data;

        return $data;
    }

    private function updateDataInquiryMtonline($trx, $trx_mtonline, $mtonline)
    {
        // update data trx mtonline
        $trx_mtonline_data = $this->apply_mtonline_model->updateById($trx_mtonline->id, [
            'amount_approved' => $mtonline->hakNasabah ?? $trx_mtonline->amount_approved,
            'total_amount' => $mtonline->up ?? $trx_mtonline->total_amount,
            'customer_name' => $mtonline->namaNasabah  ?? $trx_mtonline->customer_name,
            'type_transaction' => $mtonline->jenisTransaksi  ?? $trx_mtonline->type_transaction,
            'product_name' => $mtonline->namaProduk ?? $trx_mtonline->product_name,
            'golongan' => $mtonline->golongan ?? $trx_mtonline->golongan,
            'jumlah_hari_real' => $mtonline->jumlahHariReal ?? $trx_mtonline->jumlah_hari_real,
            'jumlah_hari_tarif' => $mtonline->jumlahHariTarif ?? $trx_mtonline->jumlah_hari_tarif,
            'tenor' => $mtonline->tenor ?? $trx_mtonline->tenor,
            'sewa_modal' => $mtonline->sewaModal ?? $trx_mtonline->sewa_modal,
            'tagihan_sewa_modal' => $mtonline->tagihanSewaModal ?? $trx_mtonline->tagihan_sewa_modal,
            'metode_pencairan' => $mtonline->metodePencairan ?? $trx_mtonline->metode_pencairan,
            'denda' => $mtonline->denda ?? $trx_mtonline->denda,
            'admin_surcharge' => $mtonline->administrasi ?? $trx_mtonline->admin_surcharge,
            'insurance_surcharge' => $mtonline->asuransi ?? $trx_mtonline->insurance_surcharge,
            'surcharge' => $mtonline->surcharge ?? $trx_mtonline->surcharge,
            'total_surcharge' => $mtonline->totalKewajiban ?? $trx_mtonline->total_surcharge,
            'status' => $mtonline->status ?? $trx_mtonline->status,
            'credit_number' => $mtonline->norek ?? $trx_mtonline->credit_number,
            'due_date' => $mtonline->tglJatuhTempo ?? $trx_mtonline->due_date,
            'credit_date' => $mtonline->tglKredit ?? $trx_mtonline->credit_date,
            'auction_date' => $mtonline->tglLelang ?? $trx_mtonline->auction_date,
            'transaction_date' => $mtonline->tglTransaksi ?? $trx_mtonline->transaction_date,
        ]);

        // update data trx
        $trx_data = $this->transaction_model->updateById($trx->id, [
            'trx_id' => $mtonline->reffSwitching ?? $trx->trx_id,
            'trx_type' => $mtonline->jenisTransaksi ?? $this->apply_mtonline_model::TRX_TYPE_MT,
            'reff_switching' => $mtonline->reffSwitching ?? $trx->reff_switching,
            'reff_core' => $mtonline->reffCore ?? $trx->reff_core,
            'date' => date('Y-m-d H:i:s'),
        ]);

        $data = new stdClass();
        $data->trx = $trx_data;
        $data->trx_mtonline = $trx_mtonline_data;
        $data->total_uang_pinjaman = $mtonline->up ?? null;

        return $data;
    }
}
