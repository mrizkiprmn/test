<?php


/**
 * @property RestSwitching_service rest_switching_service
 */
class RestSwitchingParam_service extends MY_Service
{
    /**
     * RestSwitchingParam_service constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->service('RestSwitching_service', 'rest_switching_service');
    }

    // Endpoint POST /param/stl
    public function getHargaSTL($data)
    {
        $url = "/param/stl";

        return $this->rest_switching_service->postData($url, $data);
    }

    // Endpoint POST /param/hargaemas
    public function getHargaEmas($data)
    {
        $url = "/param/hargaemas";

        return $this->rest_switching_service->postData($url, $data);
    }
}
