<?php

/**
 * @property BankModel bank_model
 * @property ConfigModel config_model
 * @property User user_model
 * @property Pegadaian Pegadaian
 * @property RestVaPayment_service rest_va_payment_service
 * @property restSwitchingPayment_service rest_switching_payment_service
 */
class Payment_service extends MY_Service
{
    protected $pegadaian_helper;
    // Default Response
    protected $response = [
        'status' => 'error',
        'message' => 'Terjadi kesalahan, mohon coba beberapa saat lagi.',
        'data' => null,
        'code' => 101
    ];

    public function __construct()
    {
        parent::__construct();
        // init load model
        $this->load->model('BankModel', 'bank_model');
        $this->load->model('User', 'user_model');
        $this->load->model('ConfigModel', 'config_model');
        $this->load->model('ProductMasterModel', 'product_master_model');
        $this->load->model('MasterModel', 'master_model');
        $this->load->model('GteModel', 'gte_model');
        $this->load->model('MuliaModel', 'mulia_model');
        $this->load->model('PaymentModel', 'payment_model');
        
        $this->load->model('AutodebitModel', 'autodebit_model');
        // init load helper
        $this->load->helper('Pegadaian');
        $this->pegadaian_helper = new Pegadaian();
        // init load service
        $this->load->service('RestVaPayment_service', 'rest_va_payment_service');
        $this->load->service('restSwitchingPayment_service', 'rest_switching_payment_service');
        $this->load->service('Response_service', 'response_service');
    }

    public function getBiayaPayment($jenis_Transaksi, $productCode)
    {
        return [
            'BNI' => $this->config_model->getBiayaPayment($jenis_Transaksi, 'BANK', $productCode, '009'),
            'MANDIRI' => $this->config_model->getBiayaPayment($jenis_Transaksi, 'BANK', $productCode, '008'),
            'WALLET' => $this->config_model->getBiayaPayment($jenis_Transaksi, 'WALLET', $productCode, ''),
            'VA_BCA' => $this->config_model->getBiayaPayment($jenis_Transaksi, 'BANK', $productCode, '014'),
            'VA_MANDIRI' => $this->config_model->getBiayaPayment($jenis_Transaksi, 'BANK', $productCode, '008'),
            'VA_PERMATA' => $this->config_model->getBiayaPayment($jenis_Transaksi, 'BANK', $productCode, '013'),
            'VA_BRI' => $this->config_model->getBiayaPayment($jenis_Transaksi, 'BANK', $productCode, '002'),
            'VA_MAYBANK' => $this->config_model->getBiayaPayment($jenis_Transaksi, 'BANK', $productCode, '016'),
            'FINPAY' => $this->config_model->getBiayaPayment($jenis_Transaksi, 'BANK', $productCode, '770'),
            'VA_MAYBANK' => $this->config_model->getBiayaPayment($jenis_Transaksi, 'BANK', $productCode, '016')
        ];
    }

    public function getPaymentComponent($jenis_Transaksi, $productCode)
    {
        $arrayOthersName = ["Mandiri Clickpay", "Wallet"];
        $bank_data = $this->BankModel->getBankList();

        foreach ($bank_data as $bank) {
            $tipe = 'VA';
            $statusParam = "va_{$bank->title}_payment";
            $paymentMethodName = "Virtual Account {$bank->title}";

            if (in_array($bank->nama, $arrayOthersName)) {
                $tipe = 'OTHERS';
                $statusParam = "{$bank->nama} payment";
                $paymentMethodName = $bank->nama;
            }

            $array_data = [
                "type" => $tipe,
                "paymentMethodName" => $paymentMethodName,
                "paymentMethodID" => $bank->title,
                "image" => $bank->image,
                "biayaChannel" => $this->ConfigModel->getBiayaPayment($jenis_Transaksi, 'BANK', $productCode, $bank->kode),
                "status" => $this->MasterModel->getParameters($statusParam),
                "isGcash" => (bool) $bank->showOnGcash
            ];

            $array_va[] = $array_data;
        }

        return $array_va ?? array();
    }

    public function createPayment($params, $user, $funcAfter = null)
    {
        // user pin authentification
        if (!$this->userPinAuth($user->user_AIID, $params)) {
            return [
                'status' => 'error',
                'message' => 'PIN tidak valid',
                'code' => 102
            ];
        }

        $data = $this->mappingPaymentData($params, $user);

        switch ($params['payment']) {
            case 'BNI':
                $response = $this->createPaymentBNI($data);
                break;
            case 'MANDIRI':
                $response = $this->createPaymentMandiri($params, $data);
                break;
            case 'VA_BRI':
                $response = $this->createPaymentBRI($data);
                break;
            case 'VA_PERMATA':
                $response = $this->createPaymentPermata($data);
                break;
            case 'GCASH':
            case 'WALLET':
                $response = $this->createPaymentGcashWallet($data, $user);
                break;
            default:
                $response = $this->createPaymentPgdn($data);
                break;
        }

        log_message('debug', __FUNCTION__ . ' response => '. json_encode($response));
        return $response;
    }

    private function createPaymentBNI($data)
    {
        $createBillingBNI = $this->rest_va_payment_service->createBillingVaBNI($data);

        if ($createBillingBNI['responseCode'] != '00' || !isset($createBillingBNI['data'])) {
            return $this->response = [
                'status' => 'error',
                'message' => $createBillingBNI['responseDesc'],
                'code' => 103,
            ];
        }

        $billingData = json_decode($createBillingBNI['data']);

        //Set response
        return $this->response = [
            'code' => '00',
            'status' => 'success',
            'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
            'data' => [
                'idTransaksi' => $data['reffSwitching'],
                'virtualAccount' => $billingData->virtualAccount,
                'expired' => $billingData->tglExpired,
                'now' => date('Y-m-d H:i:s')
            ]
        ];
    }

    private function createPaymentMandiri($params, $data)
    {
        $mandiri = $this->rest_va_payment_service->createBillingVaMandiri($data);

        if ($mandiri->responseCode != '00') {
            return $this->response = [
                'status' => 'error',
                'code' => 103,
                'message' => 'Terjadi kesalahan jaringan mohon coba beberapa saat lagi'
            ];
        }

        $mandiriData = json_decode($mandiri);
        $respData = [
            'bookingCode' => $params['kode_booling'],
            'cardNumber' => $params['card_number'],
            'tokenResponse' => $params['tokenResponse'],
            'reffBiller' => $mandiriData->reffBiller,
            'kodeBankPembayar' => $params['kode_bank'],
            'payment' => $params['payment'],
            'biayaTransaksi' => $params['biayaTransaksi'],
        ];

        return $this->response = [
            'code' => '00',
            'status' => 'success',
            'message' => 'Pembayaran Berhasil',
            'data' => $respData
        ];
    }

    private function createPaymentBRI($data)
    {
        $bri = $this->rest_va_payment_service->createBillingVaBRI($data);

        if ($bri['responseCode'] != '00') {
            return $this->response = [
                'status' => 'error',
                'message' => $bri['responseDesc'],
                'code' => 103,
                'reason' => $bri
            ];
        }

        $briData = json_decode($bri['data']);

        return $this->response = [
            'code' => '00',
            'status' => 'success',
            'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
            'data' => [
                'idTransaksi' => $data['trxId'],
                'virtualAccount' => $briData->virtualAccount,
                'expired' => $briData->tglExpired,
                'now' => date('Y-m-d H:i:s')
            ]
        ];
    }

    private function createPaymentPermata($data)
    {
        $permata = $this->rest_va_payment_service->createBillingVaPermata($data);

        if ($permata->responseCode != '00' || !isset($permata->data)) {
            return $this->response = [
                'status' => 'error',
                'message' => $permata->responseDesc ?? 'Kesalahan jaringan mohon coba beberapa saat lagi',
                'code' => 103,
            ];
        }

        $permataData = json_decode($permata->data);

        //Set response
        return $this->response = [
            'code' => '00',
            'status' => 'success',
            'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
            'data' => [
                'idTransaksi' => $data['reffSwitching'],
                'virtualAccount' =>  $permataData->virtualAccount,
                'expired' => $permataData->tglExpired,
                'now' => date('Y-m-d H:i:s')
            ]
        ];
    }

    private function createPaymentGcashWallet($data, $user)
    {
        $ePayment = $this->createEpayment($data);

        if ($ePayment->responseCode == '00' || !isset($ePayment->data)) {
            return $this->response = [
                'status' => 'error',
                'message' => 'Terjadi kesalahan mohon coba beberapa saat lagi',
                'code' => 103,
                'reason' => $ePayment
            ];
        }

        $ePaymentData = json_decode($ePayment->data, true);
        $ePaymentData['payment'] = $data['paymentMethod'];
        $ePaymentData['reffSwitching'] = $data['reffSwitching'];
        $ePaymentData['paid'] = '1';

        if ($data['paymentMethod'] == "WALLET") {
            // Update Saldo Wallet
            $saldoWallet = $this->updateWallet($user->user_AIID, $user->no_hp);
            $ePaymentData['wallet'] = $saldoWallet;
        }

        // Set response
        return $this->response = [
            'code' => '00',
            'status' => 'success',
            'message' => "Pembayaran " . $data['norek'] . ' Sukses',
            'data' => $ePaymentData
        ];
    }

    private function createPaymentPgdn($data)
    {
        $pgdn = $this->rest_switching_payment_service->vaPgdnPayment($data);

        if ($pgdn->responseCode != '00' || !isset($pgdn->data)) {
            return $this->response = [
                'status' => 'error',
                'message' => 'Kesalahan jaringan mohon coba beberapa saat lagi',
                'code' => 103,
            ];
        }

        $pgdnData = json_decode($pgdn->data);
        //Set response
        return $this->response = [
            'code' => '00',
            'status' => 'success',
            'message' => 'Mohon lakukan pembayaran ke no rekening yang tertera',
            'data' => [
                'idTransaksi' => $data['reffSwitching'],
                'virtualAccount' => $pgdnData->vaNumber,
                'expired' => $pgdnData->tglExpired,
                'now' => date('Y-m-d H:i:s')
            ]
        ];
    }

    private function createEpayment($data, $payMethod = 'WALLET')
    {
        if ($payMethod == 'WALLET') {
            return $this->rest_switching_payment_service->gadaiPayment($data);
        }

        return $this->rest_switching_payment_service->gcashPayment($data);
    }

    private function updateWallet($userId, $noHp)
    {
        $data = ['saldo' => '0', 'norek' => ''];
        $checkSaldo = $this->rest_switching_payment_service->getSaldoWallet(['noHp' => $noHp, 'tipe' => '1']);

        if ($checkSaldo->responseCode != '00' || !isset($checkSaldo->data)) {
            return $data;
        }

        $saldoData = json_decode($checkSaldo->data);
        // Update saldo wallet user
        $data = ['norek' => $saldoData->norek, 'saldo' => $saldoData->saldo];
        $this->user_model->updateUser($userId, $data);

        return $data;
    }

    private function userPinAuth($userId, $params)
    {
        if ($params['payment'] != 'GCASH') {
            return true;
        }

        return $this->user_model->isValidPIN2($userId, $params['pin']);
    }

    private function mappingPaymentData($params, $user)
    {
        $paymentAmount = $params['totalKewajiban'] + $params['biayaTransaksi'];

        $data = [
            'amount' => $paymentAmount,
            'jenisTransaksi' => $params['jenisTransaksi'],
            'norek' => $params['norek'],
            'idPromosi' => '',
            'discountAmount' => ''
        ];

        if (in_array($params['payment'], ['VA_BNI', 'VA_BRI', 'VA_PERMATA', 'BNI'])) {
            $data['productCode'] = $params['kode_produk'];
            $data['customerEmail'] = $user->email;
            $data['customerName'] = $user->nama;
            $data['customerPhone'] = $user->no_hp;
            $data['keterangan'] = $params['keterangan'];
            $data['trxId'] = $params['reffSwitching'];
        }

        if (in_array($params['payment'], ['WALLET', 'GCASH'])) {
            $data['minimalUpCicil'] = $params['minimalUpCicil'];
            $data['reffSwitching'] = $params['reffSwitching'];
            $data['paymentMethod'] = $params['payment'];
        }

        if (in_array($params['payment'], ['VA_BCA', 'VA_MANDIRI'])) {
            $data['kodeProduk'] = $params['kode_produk'];
            $data['customerEmail'] = $user->email;
            $data['customerName'] = $user->nama;
            $data['customerPhone'] = $user->no_hp;
            $data['keterangan'] = $params['keterangan'];
            $data['reffSwitching'] = $params['reffSwitching'];
            $data['kodeBank'] = $params['kode_bank'];
        }

        if ($params['payment'] == 'MANDIRI') {
            $data['productCode'] = $params['kode_produk'];
            $data['bookingCode'] = $params['booking_code'];
            $data['cardNumber'] = $params['card_number'];
            $data['keterangan'] = $params['keterangan'];
            $data['noHp'] = $user->no_hp;
            $data['tokenResponse'] = $params['token_response'];
            $data['trxId'] = $params['reffSwitching'];
        }

        if ($params['payment'] == 'GCASH') {
            $data['gcashId'] = $params['va'];
        }

        if ($params['payment'] == 'WALLET') {
            $data['norekWallet'] = $user->norek;
            $data['walletId'] = $user->no_hp;
        }

        return $data;
    }

    public function regAutoDebit($data, $token)
    {
        $time_waiting = $this->config_model->whereByVariable('time_waiting_otp');
        $data_user = $this->user_model->getUser($token->id);

        if (empty($data_user)) {
            $this->response['message'] = 'Data user tidak ditemukan';
            return $this->response;
        }

        $data['user_id'] = $data_user->user_AIID;
        $data['user_email'] = $data_user->email;

        $cek_transaksi = $this->autodebit_model->cekNoKreditByUserId($data['user_id'], $data['no_kredit']);

        if (!empty($cek_transaksi)) {
            foreach ($cek_transaksi as $item) {
                if ($item->status_regis == '1') {
                    $this->response['message'] = 'Data transaksi sudah terdaftar';
                    $this->response['data'] =
                    [
                        "title" => "Transaksi Sudah Terdaftar",
                        "description" => "Rekening yang kamu masukkan telah terdaftar",
                        "button" =>
                        [
                            'title' => 'Kembali',
                            'callback' => 'this._hideModal',
                            'color' => 'greenButton',
                        ],
                    ];
                    return $this->response;
                }
            }
        }

        $req_bank = [
            'kodeBank' => $data['kode_bank'],
            'noRekeningBank' => $data['norek_bank'],
            'namaRekeningBank' => $data['nama_pemilik_bank'],
        ];

        $cek_data_bank = $this->rest_switching_payment_service->check_rek_bank($req_bank);

        if ($cek_data_bank['responseCode'] == '15') {
            $this->response['message'] = $cek_data_bank['responseDesc'] ?? '';
            $this->response['data'] =
            [
                "title" => "Informasi Bank Tidak Ditemukan",
                "description" => "Periksa kembali informasi bank kamu",
                "button" =>
                [
                    'title' => 'Kembali',
                    'callback' => 'this._hideModal',
                    'color' => 'greenButton',
                ],
            ];
            return $this->response;
        }
        
        if ($cek_data_bank['responseCode'] != '00') {
            $this->response['message'] = $cek_data_bank['responseDesc'] ?? '';
            return $this->response;
        }

        $data_bank = json_decode($cek_data_bank['data']);

        if ($data_bank->namaRekeningBank != $data_user->nama) {
            $this->response['message'] = 'Nama pemilik bank tidak sesuai dengan nama pemilik aplikasi';
            $this->response['data'] =
            [
                "title" => "Informasi Bank Tidak Ditemukan",
                "description" => "Periksa kembali informasi bank kamu",
                "button" =>
                [
                    'title' => 'Kembali',
                    'callback' => 'this._hideModal',
                    'color' => 'greenButton',
                ],
            ];
            return $this->response;
        }

        $data['user_name'] = $data_bank->namaRekeningBank;
        $cek_kode_bank = $this->bank_model->getBankList($data['kode_bank']);

        if (empty($cek_kode_bank)) {
            $this->response['message'] = 'Kode bank tidak ditemukan';
            return $this->response;
        }

        $data['bank_name'] = $cek_kode_bank->nama;
        $data['transaksi_name'] = 'Bayar Angsuran';
        if ($data['jenis_transaksi'] == 'SL') {
            $data['transaksi_name'] = 'Beli Tabungan Emas';
        }

        $data['product_code'] = substr($data['no_kredit'], 7, 2);
        $data_Product = $this->product_master_model->checkTable($data['product_code']);

        if (empty($data_Product)) {
            $this->response['message'] = 'Data produk tidak ditemukan';
            return $this->response;
        }

        $nama_product = strtolower($data_Product->productName);
        $data['product_name'] = ucwords($nama_product);

        $request_data = [
            "no_rekbnk" => $data['norek_bank'],
            "bill_no"   => $data['no_kredit'],
            "phon_numb" => $data['no_hp'],
            "exp_date"  => $data['jangka_waktu_debit'],
            "int_date"  => $data['tanggal_debit'],
            "amt_idr"   => $data['jumlah_debit'],
            "usr_email" => $data['user_email'],
            "sec_msg"   => "",
        ];

        $debit_request = $this->rest_switching_payment_service->request_auto_debit($request_data);

        if ($debit_request['responseCode'] != '00') {
            $this->response['message'] = $debit_request['responseDesc'] ?? '';
            return $this->response;
        }

        $data_debit = json_decode($debit_request['data']);
        $data['id_reg'] = $data_debit->id_reg;
        $data['start_regis'] = date('Y-m-d H:i:s', strtotime($data_debit->res_tgl));
        $data['tgl_mulai'] = Pegadaian::getLocalization(date('d F Y', strtotime($data_debit->int_date)));
        $data['start_date_debit'] = date('Y-m-d H:i:s', strtotime($data_debit->int_date));
        $data['tgl_exp'] = Pegadaian::getLocalization(date('d F Y', strtotime($data_debit->exp_date)));
        $data['exp_date_debit'] = date('Y-m-d H:i:s', strtotime($data_debit->exp_date));
        $data['exp_regis'] = date('Y-m-d H:i:s', strtotime($time_waiting->value, strtotime($data['start_regis'])));

        $this->autodebit_model->add($data);

        $data_response = $this->data_response($data);
        $data_response['jangka_waktu_debit'] = "{$data_response['jangka_waktu_debit']} Bulan";
        $data_response['jumlah_debit'] = Pegadaian::currencyIdr($data['jumlah_debit']);
        $data_response['id_reg'] = $data['id_reg'];

        $response = $this->response_service->getResponse('general.succeeded');
        $response['message'] = 'Registrasi auto debit berhasil, Otp akan dikirim';
        $response['data'] = $data_response ?? null;

        return $response;
    }

    private function data_response($data)
    {
        return [
            "jenis_transaksi" => $data['transaksi_name'],
            "nama_product" => $data['product_name'],
            "no_kredit" => $data['no_kredit'],
            "nama_nasabah" => $data['user_name'],
            "jangka_waktu_debit" => $data['jangka_waktu_debit'] ?? $data['time_interval_debit'],
            "tanggal_pendebitan" => $data['tanggal_debit'] ?? $data['date_debit'],
            "tanggal_mulai" => $data['tgl_mulai'] ?? $data['start_date_debit'],
            "tanggal_selesai" => $data['tgl_exp'] ?? $data['exp_date_debit'],
            "nama_bank" => $data['bank_name'],
            "norek_bank" => $data['norek_bank'],
            "no_hp" => $data['no_hp']
        ];
    }

    public function sendOtpAutoDebit($data, $token)
    {
        $data_user = $this->user_model->getUser($token->id);

        if (empty($data_user)) {
            $this->response['message'] = 'Data user tidak ditemukan';
            return $this->response;
        }

        $cek_id_reg = $this->autodebit_model->cekIdReg($data['id_reg']);

        if (empty($cek_id_reg)) {
            $this->response['message'] = 'Id registrasi belum terdaftar';
            return $this->response;
        }

        if ($cek_id_reg->user_id != $data_user->user_AIID) {
            $this->response['message'] = 'Data pemilik transaksi tidak sesuai';
            return $this->response;
        }

        $data_send_otp = [
            "id_reg" => $data['id_reg'],
            "mdr_otp" => $data['otp'],
            "sec_msg" => "",
        ];

        $data_debit_otp = $this->rest_switching_payment_service->send_otp_auto_debit($data_send_otp);

        if ($data_debit_otp['responseCode'] != '00') {
            $this->response['message'] = $data_debit_otp['responseDesc'] ?? '';
            return $this->response;
        }

        $data_respons_otp = json_decode($data_debit_otp['data']);
        $data_respons_otp->date_next_debit = $cek_id_reg->start_date_debit;

        $this->autodebit_model->updateByIdReg($data_respons_otp);

        $cek_update = $this->autodebit_model->cekIdReg($data['id_reg']);
        $data_format = json_decode(json_encode($cek_update), true);
        $data_template = $this->data_response($data_format);

        $data_template['jangka_waktu_debit'] = "{$data_template['jangka_waktu_debit']} Bulan";
        $data_template['tanggal_mulai'] = Pegadaian::getLocalization(date('d F Y', strtotime($data_template['tanggal_mulai'])));
        $data_template['tanggal_selesai'] = Pegadaian::getLocalization(date('d F Y', strtotime($data_template['tanggal_selesai'])));
        $data_template['jam_registrasi'] = Pegadaian::getLocalization(date('d F Y H:i:s', strtotime($data_format['start_regis'])));
        $data_template['jumlah_debit'] = Pegadaian::currencyIdr($data_format['total_debit']);
        $data_template['status_otp'] = $data_format['flag_otp'];
        $data_template['status_debit'] = $data_format['status_debit'];
        $data_template['tgl_aktif_debit'] = $data_format['date_active_debit'];
        $data_template['tipe_transaksi'] = $data_format['type_transaksi'];

        $this->notification_service->notification_reg_auto_debit($data_user, $data_template);

        $response = $this->response_service->getResponse('general.succeeded');
        $response['message'] = 'Aktifasi autodebit berhasil';
        $response['data'] = $data_template ?? null;

        return $response;
    }

    public function listActiveAutoDebit($token)
    {
        $interval_next_debit = $this->config_model->whereByVariable('interval_next_debit'); // isi nya +1 month
        $date_now = date('Y-m-d h:i:s');
        $data_user = $this->user_model->getUser($token->id);
        
        if (empty($data_user)) {
            $this->response['message'] = 'Data user tidak ditemukan';
            return $this->response;
        }

        $data_transaksi = $this->autodebit_model->find(['user_id' => $data_user->user_AIID]);

        if (empty($data_transaksi)) {
            $this->response['message'] = 'Data transaksi tidak ditemukan';
            return $this->response;
        }

        foreach ($data_transaksi as $item) {
            //jika tgl sekarang lebih besar dari tgl debit selanjutnya maka akan +1 bulan
            if ($item->status_debit == 'active' && $date_now > $item->date_next_debit) {
                $item->update_next_debit = date('Y-m-d H:i:s', strtotime($interval_next_debit->value, strtotime($item->date_next_debit)));
                $this->autodebit_model->updateNextDebit($item);
            }

            //jika tgl sekarang lebih besar dari tgl exp debit maka update data selesai debit
            if ($item->status_debit == 'active' && $date_now > $item->exp_date_debit) {
                $this->autodebit_model->updateStatusExpDebit($item);
            }

            $icon = "beliEmas";
            if ($item->type_transaksi == 'CC') {
                $item->transaksi_name = "$item->transaksi_name $item->product_name";
                $icon = "paymentMikro";
            }
            
            if ($item->product_code == '17') {
                $icon = "pembiayaanHaji";
            }

            //list data debit yg ditampilkan berdasarkan debit yang active saja
            if ($item->status_debit == 'active' && $item->date_active_debit != null) {
                $data[] = [
                    'id' => $item->id,
                    'user_id' => $item->user_id,
                    'title' => $item->transaksi_name,
                    'nomor' => $item->no_kredit,
                    'icon' => $icon,
                    'date_next_debit' => Pegadaian::getLocalization(date('d F Y', strtotime($item->date_next_debit))),
                    'status_otp' => $item->flag_otp,
                    'status_debit' => $item->status_debit,
                    'date_active_debit' => $item->date_active_debit
                ];
            }
        }
        
        $response = $this->response_service->getResponse('general.succeeded');
        $response['message'] = 'List autodebit aktif berhasil';
        $response['data'] = $data ?? [];

        return $response;
    }

    public function checkStatusAutoDebit($token)
    {
        $next_day = $this->config_model->whereByVariable('next_day_debit');
        $date_now  = date('Y-m-d H:i:s');
        $data_user = $this->user_model->getUser($token->id);
        
        if (empty($data_user)) {
            $this->response['message'] = 'Data user tidak ditemukan';
            return $this->response;
        }

        $data_transaksi = $this->autodebit_model->find(['user_id' => $data_user->user_AIID]);

        if (empty($data_transaksi)) {
            $this->response['message'] = 'Data transaksi tidak ditemukan';
            return $this->response;
        }

        foreach ($data_transaksi as $item) {
            // 24 jam setelah melakukan register
            $date_one_day = date('Y-m-d H:i:s', strtotime($next_day->value, strtotime($item->start_regis)));

            // jika status otp = pending_otp dan besar dari 15 menit update otp = pending_register
            if ($item->flag_otp == 'pending_otp' && $date_now > $item->exp_regis) {
                $this->autodebit_model->updateStatusOtp($item);
            }

            // jika status otp = pending_register dan besar dari 24 jam
            if ($item->flag_otp == 'pending_register' && $date_now > $date_one_day) {
                $this->autodebit_model->deleteAutoDebit($item);
            }
        }

        $response = $this->response_service->getResponse('general.succeeded');
        $response['message'] = 'Cek status transaksi autodebit berhasil';

        $data_pending_otp = $this->autodebit_model->cekFlagOtp($item->user_id, 'pending_otp');

        if (!empty($data_pending_otp)) {
            $response_data = $data_pending_otp;
        }

        if (empty($response_data)) {
            $data_pending_register = $this->autodebit_model->cekFlagOtp($item->user_id, 'pending_register');
        }

        if (!empty($data_pending_register)) {
            $response_data = $data_pending_register;
        }

        $response['data'] = $response_data ?? null;

        return $response;
    }

    public function listJenisTransaksi($token)
    {
        $data_user = $this->user_model->getUser($token->id);
        
        if (empty($data_user)) {
            $this->response['message'] = 'Data user tidak ditemukan';
            return $this->response;
        }

        $list_item = $this->config_model->whereByVariable('jenis_transaksi_autodebit');
        $data_list = json_decode($list_item->value);

        $response = $this->response_service->getResponse('general.succeeded');
        $response['message'] = 'Data list jenis transaksi berhasil';
        $response['data'] = $data_list;

        return $response;
    }

    public function getPaymentByReffSwitching($reff_switching)
    {
        // Tbl Payment
        $payment = $this->payment_model->getPaymentByReffSwitching($reff_switching);
        $model   = $this->payment_model;

        // Tbl Payment Mulia
        if (empty($payment)) {
            $payment = $this->mulia_model->getPaymentByReffSwitching($reff_switching);
            $model   = $this->mulia_model;
        }

        // Tbl GTE
        if (empty($payment)) {
            $payment = $this->gte_model->getPaymentByReffSwitching($reff_switching);
            $model   = $this->gte_model;
        }

        if (empty($payment)) {
            return null;
        }

        $payment->product_code = $model::PRODUCT_CODE;

        return $payment;
    }
}
