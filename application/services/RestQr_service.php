<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use Psr\Http\Message\ResponseInterface;

class RestQr_service extends MY_Service
{
    /**
     * RestQr_service constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->url      = $this->config->item('qr_api_url');
        $this->username = $this->config->item('qr_api_username');
        $this->password = $this->config->item('qr_api_password');
        $this->prefix   = $this->config->item('qr_api_prefix');
        $this->client_key =  $this->config->item('qr_client_key');
        $this->basic_token = $this->config->item('qr_basic_token');
    }

    protected function initHttpClient($url, $token)
    {
        $this->client = new Client([
            'base_uri' => $url,
            'headers' => [
                'Authorization' => $token,
                'Content-Type' => 'application/json',
            ]
        ]);
    }

    /**
     * @param $url
     * @param array $query
     * @return ResponseInterface
     */
    public function getData($url, $query = [])
    {
        try {
            $qr_token = $this->token();

            if (empty($qr_token)) {
                Pegadaian::showError('Token not found !', '', 400);
            }

            $token = "Bearer $qr_token";
            $this->initHttpClient($this->url, $token);

            $rest_response = $this->client->get($url, ['query' => $query]);
            log_message('debug', 'RestQr_service url => ' . $url);
        } catch (BadResponseException $e) {
            log_message('debug', 'RestQr_service  => ' . $e->getMessage());
            $rest_response = $e->getResponse();
        } catch (ClientException $e) {
            log_message('debug', 'RestQr_service  => ' . $e->getMessage());
            $rest_response = $e->getResponse();
        } catch (ConnectException $e) {
            log_message('debug', 'RestQr_service  => ' . $e->getMessage());
            $rest_response = $e->getResponse();
        }

        $response = !empty($rest_response) ? $rest_response->getBody()->getContents() : null;
        $response_data = json_decode($response, true);

        log_message('debug', 'start of RestQrService : ' . $url, $query);
        log_message('debug', 'end of RestQrService : ' . $url, $response);

        if (empty($response_data['code'])) {
            return null;
        }

        return $response_data;
    }

    /**
     * @param $url
     * @return mixed|null
     */
    public function getScanBarcode($url)
    {
        if ($this->config->item('qr_is_dummy') == 1) {
            return $this->dummyScanBarcode();
        }

        $response = $this->getData($url, ['clientKey' => $this->client_key]);

        return $response;
    }

    /**
     * @param $url
     * @return mixed|null
     */
    public function getSuccessQr($transaction_code)
    {
        $url = "{$this->url}{$this->prefix}{$transaction_code}/success";

        $response = $this->getData($url, ['clientKey' => $this->client_key]);

        return $response;
    }

    protected function token()
    {
        $data = [
            'username' => $this->username,
            'password' => $this->password
        ];

        $token_data = $this->getToken($data);

        return $token_data;
    }

    protected function getToken($query)
    {
        if ($this->config->item('qr_is_dummy') == 1) {
            return $this->dummyQrToken();
        }

        $token = "Basic $this->basic_token";
        $this->initHttpClient($this->url, $token);

        $url = "tokens/refresh";
        try {
            $rest_response = $this->client->get($url, ['query' => $query]);
        } catch (BadResponseException $e) {
            $rest_response = $e->getResponse();
        }

        $response = !empty($rest_response) ? $rest_response->getBody()->getContents() : null;
        $response_data = json_decode($response);
        $token = $response_data->data->tokens ?? null;

        return $token;
    }

    private function dummyQrToken()
    {
        return [
            "code" => "00",
            "status" => "Success",
            "message" => "Data Berhasil Dikirim",
            "data" => [
                "username" => "merchantsatu",
                "tokens" => "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDQ4NDAwMTYsImp0aSI6Im1lcmNoYW50c2F0dSJ9.YiC8RE55HxbSpgH9DdDATU54z2aJ_EG47LMLbheI5HbEIr-BjOtZGI2I9oXgbyTMZ8mASFjHUXw0IQJHHbTjwA",
                "expireAt" => "2020-11-08T19:53:36.976724Z",
                "created_at" => "2020-10-09T19:53:36.976724Z",
                "updated_at" => "0001-01-01T00:00:00Z"
            ]
        ];
    }

    private function dummyScanBarcode()
    {
        return [
            "code" => "00",
            "status" => "Success",
            "message" => "Data Berhasil Dikirim",
            "data" => [
                "merchantId" => 1,
                "data" => [
                    [
                        "namaPerhiasan" => "cincin emas saphire",
                        "jenisPerhiasan" => "CC",
                        "berat" => "6",
                        "karat" => "24",
                        "harga" => "6000000",
                        "jumlah" => "1",
                        "subTotalHarga" => "6000000",
                        "description" => 'lorem ipsum dulur amet',
                    ],
                    [
                        "namaPerhiasan" => "kalung emas meltion",
                        "jenisPerhiasan" => "KL",
                        "berat" => "3",
                        "karat" => "24",
                        "harga" => "3000000",
                        "jumlah" => "1",
                        "subTotalHarga" => "3000000",
                        "description" => 'lorem ipsum dulur amet',
                    ]
                ],
                "count" => 0,
                "status" => "active",
                "phoneNumber" => "089681062277",
                "transactionCode" => "XM6L3RUILE",
                "transactionAmount" => "9000000",
                "imageQr" => "iVBORw0KGgoAAAANSUhEUgAAAQAAAAEAAQMAAABmvDolAAAABlBMVEX///8AAABVwtN+AAACAElEQVR42uyYPY7+LAzEB1FQcgSOws2eJDfjKByBkgJlXtnk+Xy3+ldxFEsrbTa/xstkGBt33XXXv9VCkh1gS2sgB/BAlL/xWgAA15G9cLH4nrYRy/HCDuDJrSeOyBWRvsONWMhqESgj1oUN0q1hQM8sis7cJQF9kwikjRRNrvhDtGcH1B/0NB+h5fll/d9ATg7MSvSsSwcQuP5l6ScHFv/yas7Dmm3WiwFseAAYsSK0PAJ3xOI+viwLAAawiD/E6tiyZ3UD2MOrTRsAW94D1dcOTXp5ekvTArCQxXX9VVLCCHozUczuUgAidyC/b97Vtzzt3Q4gmnQSGpCUm0/u3aYJYPFNzLxIlgsNhxi/bNACoLKrWduMhT2RLf+EnAsAI5YHUhExBpbJ0RkD4FveWLOwXUKcinH/iBEmALIgsPimk+g7V8MU4JtKDi+X26gut14KkOAt/idZriMjVKilfI0P5wc8iwaHZwqS/MAPTRoBGqS3mYKkTSeB4cftTw88x7mWNt3kzOMLX/so+8Ax6xXKv0PHh20G78UScCwPs0ytc3yQS+pTkyaAuQMpKk350TXoz2meH3iu3SBzqUwHbm5FrgnozaTbg923zP676bUBUO8hHR+GZjmYAo5JFLoDEU93OokbAw6B6fQTxSoWmQ6+Q4594K677vqu/wIAAP//TjSfODMIRXIAAAAASUVORK5CYII=",
                "ExpireAt" => "2020-10-10T01:28:10.902123Z"
            ]
        ];
    }
}
