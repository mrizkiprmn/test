<?php

use GuzzleHttp\Exception\GuzzleException;

class RestSwitchingPayment_service extends MY_Service
{
    const REQUEST_REG_DEBIT = 'REG_AUT';
    const SEND_OTP_DEBIT = 'OTP_CON';

    /**
     * RestSwitchingPayment_service constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->service('RestSwitching_service', 'rest_switching_service');
        $this->load->service('RestVa_service', 'rest_switching_va_service');
    }

    // Endpoint POST /gadai/inquiry
    public function gadaiInquiry(array $data)
    {
        $url = '/gadai/inquiry';

        return $this->rest_switching_service->postData($url, $data);
    }

    // Endpoint POST /gadai/payment
    // payload data
    // amount
    // jenisTransaksi
    // norek
    // productCode
    // minimalUpCicil
    // reffSwitching
    // paymentMethod
    // norekWallet
    // walletId
    public function gadaiPayment(array $data)
    {
        $url = '/gadai/payment';
        $data['flag'] = 'K';

        return $this->rest_switching_service->postData($url, $data);
    }

    // Endpoint POST /gcash/payment
    // payload data
    // amount
    // jenisTransaksi
    // norek
    // productCode
    // minimalUpCicil
    // reffSwitching
    // paymentMethod
    // gcashId
    public function gcashPayment(array $data)
    {
        $url = '/gcash/payment';
        $data['flag'] = 'K';

        return $this->rest_switching_service->postData($url, $data);
    }

    // Endpoint POST /va/create
    // payload data
    // amount
    // jenisTransaksi
    // norek
    // kodeProduk
    // customerEmail
    // customerName
    // customerPhone
    // keterangan
    // reffSwitching
    // kodeBank
    public function vaPgdnPayment(array $data)
    {
        $url = '/va/create';
        $data['flag'] = 'K';

        return $this->rest_switching_service->postData($url, $data);
    }

    function getSaldoWallet($data)
    {
        $url = '/portofolio/wallet';
        $data['flag'] = 'K';

        return $this->rest_switching_service->postData($url, $data);
    }

    public function createBillingFinPay($data)
    {
        $url = '/va/finpay/createbilling';
        
        return $this->rest_switching_va_service->postData($url, $data);
    }

    public function request_auto_debit($data)
    {
        $url = '/jatelindo/register';
        $data['api_tp'] = self::REQUEST_REG_DEBIT;

        return $this->rest_switching_service->postData($url, $data);
    }

    public function send_otp_auto_debit($data)
    {
        $url = '/jatelindo/otpConfirmation';
        $data['api_tp'] = self::SEND_OTP_DEBIT;

        return $this->rest_switching_service->postData($url, $data);
    }

    public function check_rek_bank($data)
    {
        $url = '/banking/check';
        return $this->rest_switching_service->postData($url, $data);
    }
}
