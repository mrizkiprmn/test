<?php

class RestSwitchingMpo_service extends MY_Service
{
    /**
     * @var
     */
    private $rest_switching_service;

    /**
     * RestSwitchingMpo_service constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->service('RestSwitching_service');

        $this->rest_switching_service = new RestSwitching_service();
    }

    // Endpoint POST /otp/validate
    public function checkHarga(array $data)
    {
        $url = '/mpo/checkharga';

        return $this->rest_switching_service->postData($url, $data);
    }
}
