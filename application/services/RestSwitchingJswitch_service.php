<?php

class RestSwitchingJswitch_service extends MY_Service
{
    private $base_path = 'jswitch';

    /**
     * RestSwitchingGpoint_service constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->service('RestSwitching_service', 'rest_switching_service');
    }

    // Endpoint POST jswitch/kependudukan/image
    public function kependudukanImage(array $data, $is_dummy = false)
    {
        $url = "{$this->base_path}/kependudukan/image";

        if ($is_dummy) {
            return $this->dummyKependudukanImage();
        }

        return $this->rest_switching_service->postData($url, $data);
    }

    private function dummyKependudukanImage()
    {
        return [
            'responseCode' => '00',
            'responseDesc' => 'Approved',
            'data' => "{\"httpResponseCode\":200,\"matchScore\":\"-1.0\",\"transactionId\":\"pegadaian_poc#pegadaian\",\"uid\":\"3275090705960007\",\"verificationResult\":false}"
        ];
    }
}
