<?php

/**
 * @property User user_model
 * @property Financing_service financing_service
 * @property ArrumHajiModel arrum_haji_model
 * @property RestSwitchingParam_service rest_switching_param_service
 * @property RestSwitchingMikroSyariah_service rest_switching_mikro_syariah_service
 * @property Notification_service notification_service
 * @property NotificationModel notification_model
 * @property RestCoreMikro_service rest_core_mikro_service
 */
class Mikro_service extends MY_Service
{
    /**
     * @property User user_model
     * @property Financing_service financing_service
     * @property ArrumHajiModel arrum_haji_model
     * @property RestSwitchingParam_service rest_switching_param_service
     * @property RestSwitchingMikroSyariah_service rest_switching_mikro_syariah_service
     * @property Notification_service notification_service
     * @property NotificationModel notification_model
     */
    protected $marhun_bih = 25000000;
    protected $mu_nah_rate = 0.95 / 100;
    protected $min_rpc = 1.5;
    protected $mu_nah_akad = 270000;
    protected $biaya_buka_rekening = 500000;
    protected $limit = 1900000;
    protected $response = [
        'code' => 101,
        'status' => 'error',
        'message' => 'Internal Server Error',
        'data' => null
    ];

    //Khusus Pembayaran Maybank
    public function mikroPayment($payment, $checkPayment, $productCode, $idTransaksi, $user, $token, $kodeBank)
    {

        $biayaTransaksi = $this->config_model->getRealBiayaPayment($checkPayment->jenisTransaksi, 'BANK', $productCode, $kodeBank);
        $biayaTransaksiDisplay = $this->config_model->getBiayaPayment($checkPayment->jenisTransaksi, 'BANK', $productCode, $kodeBank);

        $amount = $checkPayment->totalKewajiban + $biayaTransaksi;
        if ($checkPayment->jenisTransaksi == "TB") {
            $amount = ($checkPayment->totalKewajiban + $biayaTransaksi) - $checkPayment->saldoRekeningPendamping;
        }

        $data = array(
            'amount' => $amount,
            'channelId' => $token->channelId,
            'clientId' => $this->config->item('core_post_username'),
            'customerEmail' => $user->email,
            'customerName' => $user->nama,
            'customerPhone' => $user->no_hp,
            'flag' => 'K',
            'jenisTransaksi' => $checkPayment->jenisTransaksi,
            'keterangan' => 'PDS' . $checkPayment->jenisTransaksi . ' ' . $idTransaksi . ' ' . $checkPayment->norek,
            'kodeProduk' => $productCode,
            'kodeBank' => $kodeBank,
            'reffSwitching' => $idTransaksi,
        );

        $billingMayBank = $this->rest_switching_payment_emas_service->createBillingVAMayBank($data, $checkPayment->norek);

        if ($billingMayBank['responseCode'] != '00') {
            $this->response['message'] = $billingMayBank['responseDesc'];
            return $this->response;
        }

        $dataPrefix = $this->master_model->getPrefixBank('prefixMaybank');
        $billingData = json_decode($billingMayBank['data']);
        $virtualAccount = $billingData->vaNumber;
        $virtualAccountFormat = $dataPrefix . substr($virtualAccount, 0, 11);
        $tglExpired = $billingData->tglExpired;

        $updatePaymentData = array(
            'tipe' => $payment,
            'virtual_account' => $virtualAccount,
            'tanggal_expired' => $tglExpired,
            'kodeBankPembayar' => $kodeBank,
            'payment' => $payment,
            'biayaTransaksi' => $biayaTransaksiDisplay
        );

        $this->mikro_model->updatePayment($idTransaksi, $updatePaymentData);

        $this->notification_service->notificationMikro(
            $checkPayment,
            $virtualAccountFormat,
            $tglExpired,
            $idTransaksi,
            $token,
            $productCode
        );

        $dataPayment = array(
            'idTransaksi' => $idTransaksi,
            'virtualAccount' => $virtualAccountFormat,
            'expired' => $tglExpired,
            'now' => date('Y-m-d H:i:s'),
        );

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Mohon lakukan pembayaran ke no rekening yang tertera';
        $this->response['data'] = $dataPayment ?? null;

        return $this->response;
    }

    /**
     * Mikro_service constructor.
     */
    public function __construct()
    {
        parent::__construct();
        // init load model
        $this->load->model('ArrumHajiModel', 'arrum_haji_model');
        $this->load->model('MikroModel', 'mikro_model');
        $this->load->model('MasterModel', 'master_model');
        $this->load->model('masterJewelryModel', 'master_jewelry_model');
        $this->load->model('User', 'user_model');
        $this->load->model('NotificationModel', 'notification_model');
        $this->load->model('ConfigModel', 'config_model');
        $this->load->model('ArrumHajiModel', 'arrum_haji_model');
        $this->load->model('ArrumHajiAgunanModel', 'arrum_haji_agunan_model');
        // init load service
        $this->load->service('Financing_service', 'financing_service');
        $this->load->service('RestSwitchingParam_service', 'rest_switching_param_service');
        $this->load->service('RestSwitchingMikroSyariah_service', 'rest_switching_mikro_syariah_service');
        $this->load->service('Notification_service', 'notification_service');
        $this->load->service('RestCoreMikro_service', 'rest_core_mikro_service');
        $this->load->service('RestSwitchingPaymentEmas_service', 'rest_switching_payment_emas_service');
        $this->load->service('RestSwitchingPayment_service', 'restswitching_payment_service');
        $this->load->service('Response_service', 'response_service');
    }

    public function simulasiAgunan($data)
    {
        if (empty($data)) {
            return $this->response;
        }

        $data = $data['agunan'];

        $stlData = [
            'clientId' => $this->config->item('core_syariah_post_username'),
            'flag' => 'S'
        ];

        $inquiryStl = $this->rest_switching_param_service->getHargaSTL($stlData);

        if ($inquiryStl['responseCode'] !== '00') {
            return $this->response;
        }

        $dataStl = json_decode($inquiryStl['data'], true);
        $hargaSTL = $dataStl['hargaEmas'];

        $result = $this->mappingSimulasiAgunan($data, $hargaSTL);

        $this->response['code'] = 200;
        $this->response['status'] = "success";
        $this->response['message'] = "Simulasi Agunan Arrum Haji Berhasil";
        $this->response['data']['totalTaksiran'] = $result->totalTaksiran;
        $this->response['data']['detail'] = $result->detail;

        return $this->response;
    }

    function mappingSimulasiAgunan($data, $hargaSTL)
    {

        $totalTaksiran = 0;
        $detail = [];
        $agunan = [];

        foreach ($data as $dat) {
            $bUpdate = false;
            $bUpdatePerhiasan = false;

            if (empty($dat['jenisEmas']['formatted_value'])) {
                $this->response['message'] = "jenis emas tidak boleh kosong!";
                return $this->response;
            }

            if ($dat['jenisEmas']['value'] == 'perhiasan') {
                $result = $dat['kadarPerhiasan']['value']/24 * $dat['beratPerhiasan']['value'] * $hargaSTL;
                $result = round(ceil($result), -2);
                $totalTaksiran += $result;
                $detailAgunan['label'] = $dat['jenisPerhiasan']['formatted_value'] . " - " . $dat['kadarPerhiasan']['formatted_value'] ." - " . $dat['beratPerhiasan']['formatted_value'];

                foreach ($detail as &$det) {
                    if ($det['label'] == $detailAgunan['label']) {
                        $det['value'] += $result;
                        $det['quantity'] += 1;
                        $bUpdatePerhiasan = true;
                        break;
                    }
                }

                foreach ($agunan as &$ag) {
                    if ($ag['label'] == $detailAgunan['label']) {
                        $ag['nilai_agunan'] += $result;
                        $ag['quantity'] += 1;
                        break;
                    }
                }

                if (!$bUpdatePerhiasan) {
                    $detailAgunan['value'] = $result;
                    $detailAgunan['quantity'] = 1;
                    array_push($detail, $detailAgunan);

                    $agunan_data = array(
                        'label' => $detailAgunan['label'],
                        'jenis_agunan' => $this->arrum_haji_model::PERHIASAN,
                        'karat' => $dat['kadarPerhiasan']['value'],
                        'gram' => $dat['beratPerhiasan']['value'],
                        'jenis_perhiasan' => $dat['jenisPerhiasan']['value'],
                        'kode_perhiasan' => $this->master_model->getJewelryCode($dat['jenisPerhiasan']['value']),
                        'tipe_jaminan' => 'KTEP',
                        'nilai_agunan' => $result,
                        'quantity' => 1
                    );
                    array_push($agunan, $agunan_data);
                }

                continue;
            }

            $result = $dat['denominasi']['value'] * $hargaSTL * $dat['jumlahKeping']['value'];
            $result = round(ceil($result), -2);
            $totalTaksiran += $result;
            $detailAgunan['label'] = $dat['jenisEmas']['formatted_value'] . " - " . $dat['denominasi']['formatted_value'];

            foreach ($detail as &$det) {
                if ($det['label'] == $detailAgunan['label']) {
                    $det['value'] += $result;
                    $det['quantity'] += $dat['jumlahKeping']['value'];
                    $bUpdate = true;
                    break;
                }
            }

            foreach ($agunan as &$ag) {
                if ($ag['label'] == $detailAgunan['label']) {
                    $ag['nilai_agunan'] += $result;
                    $ag['quantity'] += $dat['jumlahKeping']['value'];
                    $ag['gram'] = $dat['denominasi']['value'] * $ag['quantity'];
                    break;
                }
            }

            if (!$bUpdate) {
                $detailAgunan['value'] = $result;
                $detailAgunan['quantity'] = intval($dat['jumlahKeping']['formatted_value']);
                array_push($detail, $detailAgunan);

                $agunan_data = array(
                    'label' => $detailAgunan['label'],
                    'jenis_agunan' => $this->arrum_haji_model::LOGAM_MULIA,
                    'karat' => $this->arrum_haji_model::LOGAM_MULIA_KARAT,
                    'gram' => $dat['denominasi']['value'] * $dat['jumlahKeping']['value'],
                    'jenis_perhiasan' => null,
                    'kode_perhiasan' => null,
                    'tipe_jaminan' => 'KTML',
                    'nilai_agunan' => $result,
                    'quantity' => $dat['jumlahKeping']['value']
                );

                array_push($agunan, $agunan_data);
            }
        }

        $result_mapping = new stdClass();
        $result_mapping->totalTaksiran = $totalTaksiran;
        $result_mapping->detail = $detail;
        $result_mapping->agunan = $agunan;

        return $result_mapping;
    }

    public function getTenor($data)
    {
        $result = [];
        $tenors = $this->master_model->getTenor();

        $total_taksiran = $data['totalTaksiran'] + $this->marhun_bih;
        $perbandingan = round(($this->marhun_bih / $total_taksiran) * 100, 2);
        $koef_diskon = $this->mikro_model->getDiscountRate($perbandingan);

        foreach ($tenors as $tenor) {
            $result_tenor = $this->hitungTenor(intval($tenor->tenor), $total_taksiran, $koef_diskon);

            $hasil['title'] = $tenor->tenor . " Bulan";
            $hasil['description'] = Pegadaian::currencyIdr(round($result_tenor['angsuran'], -2)) . "/bulan";

            if ($data['rpc'] > $result_tenor['angsuran']) {
                array_push($result, $hasil);
            }
        }

        if (count($result) == 0) {
            $this->response['message'] = "RPC tidak mencukupi untuk memilih angsuran";
            return $this->response;
        }

        $this->response['status'] = "success";
        $this->response['message'] = "Get Tenor Arrum Haji Berhasil";
        $this->response['data'] = $result;

        return $this->response;
    }

    public function getSummary($data)
    {
        $total_taksiran = $data['totalTaksiran'] + $this->marhun_bih;
        $perbandingan = round(($this->marhun_bih / $total_taksiran) * 100, 2);
        $koef_diskon = $this->mikro_model->getDiscountRate($perbandingan);

        $result_tenor = $this->hitungTenor($data['tenor'], $total_taksiran, $koef_diskon);

        $ijk = $this->master_model->getIJKByTenor($data['tenor']);

        $biaya_administrasi = $this->mu_nah_akad + ($this->marhun_bih * $ijk/100);
        $total_biaya_pengajuan = $this->biaya_buka_rekening + $biaya_administrasi;
        $nama_outlet = $this->master_model->getSingleCabang($data['idOutletPengajuan'])->nama;

        $result = array(
            'totalTaksiran' => array('label' => 'Total Taksiran Agunan', 'value' => round($data['totalTaksiran'], -2),'isHidden' => false),
            'pengajuanPinjaman' => array('label' => 'Pengajuan Pinjaman', 'value' => $this->marhun_bih,'isHidden' => false),
            'tenor' => array('label' => 'Tenor', 'value' => $data['tenor'],'isHidden' => false),
            'angsuran' => array('label' => 'Angsuran', 'value' => round($result_tenor['angsuran'], -2),'isHidden' => false),
            'biayaAdministrasi' => array('label' => 'Biaya Administrasi', 'value' => round($biaya_administrasi, -2),'isHidden' => false),
            'biayaBukaRekening' => array('label' => 'Biaya Buka Rekening', 'value' => $this->biaya_buka_rekening,'isHidden' => false),
            'totalBiayaPengajuan' => array('label' => 'Total Biaya Pengajuan', 'value' => round($total_biaya_pengajuan, -2),'isHidden' => false),
            'outletPengajuan' => array('label' => 'Outlet Pengajuan', 'value' => $nama_outlet,'isHidden' => false),
            'muNah' => array('label' => 'Mu Nah', 'value' => round($result_tenor['mu_nah'], -2),'isHidden' => true),
            'muNahNett' => array('label' => 'Mu Nah Nett', 'value' => round($result_tenor['mu_nah_nett'], -2),'isHidden' => true),
            'diskonTabel' => array('label' => 'Diskon Tabel', 'value' => number_format((float)$koef_diskon, 2, '.', ''),'isHidden' => true),
            'diskon' => array('label' => 'Diskon', 'value' => round($result_tenor['diskon'], -2),'isHidden' => true)
        );

        $this->response['status'] = "success";
        $this->response['message'] = "Get Summary Arrum Haji Berhasil";
        $this->response['data'] = $result;

        return $this->response;
    }

    function hitungTenor($tenor, $total_taksiran, $koef_diskon)
    {
        $up_tenor = $this->marhun_bih / $tenor;
        $mu_nah = $this->mu_nah_rate * $total_taksiran;
        $diskon = $koef_diskon / 100 * $mu_nah;
        $mu_nah_nett = $mu_nah - $diskon;

        $angsuran = $mu_nah_nett + $up_tenor;
        $result = array('up_tenor' => $up_tenor, 'mu_nah' => $mu_nah, 'diskon' => $diskon, 'mu_nah_nett' => $mu_nah_nett, 'angsuran' => $angsuran);

        return $result;
    }

    public function getSimulasiParam()
    {
        $data['limit'] = $this->limit;
        $fields['jenisEmas']['type'] = 'dropdown';

        $jenis_emas_options =
            array(
                [
                    "label" => "Pilih Jenis Emas",
                    "value" => ""
                ],
                [
                    "label" => "Perhiasan",
                    "value" => "perhiasan"
                ],
                [
                    "label" => "Logam Mulia",
                    "value" => "logam mulia"
                ]
            );

        $fields['jenisEmas']['options'] = $jenis_emas_options;
        //perhiasan

        $fields['jenisPerhiasan']['dependsOn'] = 'perhiasan';
        $fields['jenisPerhiasan']['type'] = 'dropdown';

        $jenis_perhiasan_option = array(
            [
                "label" => "Pilih Jenis perhiasan",
                "value" => ""
            ],
        );

        $perhiasans = $this->master_jewelry_model->get();
        foreach ($perhiasans as $key => $perhiasan) {
            $array_data = [
                "label" => $perhiasan->name,
                "value" => $perhiasan->name
            ];

            array_push($jenis_perhiasan_option, $array_data);
        }

        $fields['jenisPerhiasan']['options'] = $jenis_perhiasan_option;

        //kadarPerhiasan
        $fields['kadarPerhiasan']['dependsOn'] = 'perhiasan';
        $fields['kadarPerhiasan']['type'] = 'dropdown';

        $master_karat_data = $this->config_model->whereByVariable('master_karat')->value;
        $array_karat = explode(',', $master_karat_data);

        $kadar_perhiasan_option = array(
            [
                "label" => "Pilih kadar perhiasan",
                "value" => ""
            ],
        );

        foreach ($array_karat as $key => $karat) {
            $array_data = [
                "label" => $karat ." Karat",
                "value" => $karat
            ];
            array_push($kadar_perhiasan_option, $array_data);
        }

        $fields['kadarPerhiasan']['options'] = $kadar_perhiasan_option;

        //beratPerhiasan
        $fields['beratPerhiasan']['dependsOn'] = 'perhiasan';
        $fields['beratPerhiasan']['type'] = 'text';
        $fields['beratPerhiasan']['placeHolder'] = 'Masukan Berat Perhiasan';

        //denominasi
        $fields['denominasi']['dependsOn'] = 'logam mulia';
        $fields['denominasi']['type'] = 'dropdown';

        $master_denominasi_data = $this->config_model->whereByVariable('master_denominasi')->value;
        $array_denominasi = explode(',', $master_denominasi_data);

        $denominasi_option = array(
            [
                "label" => "Pilih Denominasi",
                "value" => ""
            ],
        );

        foreach ($array_denominasi as $key => $denom) {
            $array_data = [
                "label" => $denom ." gram",
                "value" => $denom
            ];
            array_push($denominasi_option, $array_data);
        }

        $fields['denominasi']['options'] = $denominasi_option;
        //jumlahKeping
        $fields['jumlahKeping']['dependsOn'] = 'logam mulia';
        $fields['jumlahKeping']['type'] = 'spinner';
        $fields['jumlahKeping']['defaultValue'] = '1';
        $fields['jumlahKeping']['isQuantity'] = true;

        $data['fields'] = $fields;

        $this->response['code'] = 200;
        $this->response['status'] = "success";
        $this->response['message'] = "Get Simulasi Param Berhasil";
        $this->response['data'] = $data;
        return $this->response;
    }

    public function simulasiRpc($data)
    {
        if ($data['pengeluaran'] > $data['penghasilan']) {
            $this->response['message'] = "Validation Exception";

            $result[] = array('field' => 'pengeluaran', 'message' => 'Pengeluaran tidak boleh lebih besar dari penghasilan');
            $this->response['data'] = $result;
            return $this->response;
        }

        $nominal_sisa = ($data['penghasilan'] - $data['pengeluaran']);
        $rpc = $nominal_sisa/$this->min_rpc;
        $minimal_nominal_tenor = $this->config_model->whereByVariable('minimal_nominal_tenor')->value;

        //membandingkan nilai RPC dari perhitungan pemasukan dan pengeluaran dengan RPC minimal
        if ($rpc < $minimal_nominal_tenor) {
            $this->response['status'] = "error";
            $this->response['message'] = "Validation Exception";

            $result['title'] = 'Belum Ada Tenor Yang Cocok';
            $result['description'] = 'Maaf, pengajuan kamu tidak dapat dilanjutkan karena belum ada tenor yang sesuai dengan profil finansial kamu';
            $this->response['data'] = $result;

            return $this->response;
        }

        $this->response['code'] = 200;
        $this->response['status'] = "success";
        $this->response['message'] = "Data Successfully Proceed";

        $result['rpc'] = $rpc;
        $this->response['data'] = $result;

        return $this->response;
    }

    public function pengajuanArrumHaji($data, $token)
    {
        $ijk = $this->master_model->getIJKByTenor($data['tenor']);

        $data['booking_id'] = $this->arrum_haji_model->getNewId($data['id_outlet_pengajuan']);
        $data['ijk'] = $ijk / 100 * $this->marhun_bih;
        $data['mu_nah_akad'] = $this->mu_nah_akad;
        $data['biaya_buka_rekening'] = $this->biaya_buka_rekening;
        $data['outlet_pengajuan'] = $this->master_model->getSingleCabang($data['id_outlet_pengajuan'])->nama;
        $data['diskon_tabel_id'] = $this->mikro_model->getDiscountId($data['diskon_tabel']);

        $stlData = [
            'clientId' => $this->config->item('core_syariah_post_username'),
            'flag' => 'S'
        ];

        $inquiryStl = $this->rest_switching_param_service->getHargaSTL($stlData);

        if ($inquiryStl['responseCode'] !== '00') {
            return $this->response;
        }

        $dataStl = json_decode($inquiryStl['data'], true);
        $hargaSTL = $dataStl['hargaEmas'];
        $result = $this->mappingSimulasiAgunan($data['agunan'], $hargaSTL);

        $data['agunan'] = $result->agunan;
        foreach ($data['agunan'] as &$dat) {
            $dat['booking_id'] = $data['booking_id'];
        }

        $result = $this->arrum_haji_model->add($data, $token);

        if (empty($result)) {
            $this->response['message'] = "Terjadi kesalahan simpan data, silahkan coba Lagi!";
            return $this->response;
        }

        $submit = $this->submitFormArrumHaji($result);

        if ($submit['status'] != 'success') {
            return $submit;
        }

        $this->sendNotificationArrumHaji(["booking_id" => $data['booking_id'], "status" => ""]);

        $this->response['code']    = "200";
        $this->response['status']  = "success";
        $this->response['message'] = "Submit Arrum Haji Berhasil";

        return $this->response;
    }

    public function listRiwayat($user_id)
    {
        $items = $this->arrum_haji_model->getByUserId($user_id);

        if (empty($items)) {
            $this->response['message'] = 'Data tidak ditemukan!';
            return $this->response;
        }

        $this->response['status'] = 'success';
        $this->response['message'] = 'success get data';
        $this->response['data'] = $items;

        return $this->response;
    }

    public function getRiwayat($user_id)
    {
        $list_financing = $this->riwayatFinancing($user_id);
        $list_hajj_trip = $this->riwayatArrumHaji($user_id);
        $list_riwayat   = array_merge($list_financing, $list_hajj_trip);

        function sortFunction($a, $b)
        {
            return strtotime($b->created_at) - strtotime($a->created_at);
        }

        usort($list_riwayat, "sortFunction");

        $this->response['status'] = 'success';
        $this->response['message'] = 'Get Riwayat Mikro';
        $this->response['data'] = $list_riwayat;

        return $this->response;
    }

    public function detailRiwayatPengajuan($token, $booking_id, $tipe)
    {
        if ($tipe == 'financing') {
            $financing_response = $this->financing_service->detail($token, $booking_id);

            $this->response['status'] = 'success';
            $this->response['message'] = 'Get Detail Riwayat Mikro';
            $this->response['data'] = $financing_response['data'];

            return $this->response;
        }

        $data = $this->arrum_haji_model->find(['booking_id' => $booking_id]);

        $this->response['status'] = 'success';
        $this->response['message'] = 'Get Detail Riwayat Arrum Haji';
        $this->response['data'] = $data;

        return $this->response;
    }


    public function riwayatFinancing($user_id)
    {
        $data = $this->financing_service->listRiwayat($this->user_model->profile($user_id));

        return $data['status'] == 'success' ? $data['data'] : [];
    }

    public function riwayatArrumHaji($user_id)
    {
        $data = $this->listRiwayat($user_id);

        return $data['status'] == 'success' ? $data['data'] : [];
    }

    public function sendNotificationArrumHaji($request)
    {
        $arrum_haji = $this->arrum_haji_model->getByCodeBooking($request['booking_id']);

        if (empty($arrum_haji)) {
            $this->response['message'] = 'Data Arrum Haji tidak ditemukan!';
            return $this->response;
        }

        $user = $this->user_model->getUser($arrum_haji->user_id);
        if (empty($user)) {
            $this->response['message'] = 'Data User tidak ditemukan!';
            return $this->response;
        }

        $data_notification = $this->getDataNotificationStatusArrumHaji($request['status'] ?? null);

        if (!empty($request['status'])) {
            $arrum_haji = $this->arrum_haji_model->updateStatusByCodeBooking(
                $arrum_haji->booking_id,
                $request['status'],
                $request['reason'] ?? null
            );
        }

        $data = new stdClass();
        $data->user = $user;
        $data->arrum_haji = $arrum_haji;
        $data->title = $data_notification['text'];
        $data->tagline = $data_notification['text'];
        $data->html_notification = $data_notification['html_notification'];
        $data->html_content = $data_notification['html_content'];
        $data->email_notification = $data_notification['email_notification'];
        $data->type = $this->notification_model::TYPE_ARRUM_HAJI;
        $data->transaction_type = $this->arrum_haji_model::TRX_TYPE_OPEN;

        $this->notification_service->sendNotificationMobile($data);
        $this->notification_service->sendNotificationEmail($data);

        $this->response['status'] = 'success';
        $this->response['message'] = 'Send Notification Arrum Haji';

        return $this->response;
    }

    public function submitFormArrumHaji($data)
    {
        if (empty($data)) {
            return $this->response;
        }

        $payload = [
            'jenisTransaksi' => $this->arrum_haji_model::TRX_TYPE_OPEN,
            'kodeProduk' => $this->arrum_haji_model::PRODUCT_CODE,
            'kodeBooking' => $data->booking_id,
            'kodeCabang' => $data->code_outlet,
            'tipeChanneling' => '1',
            'dataHaji' => $this->setDataHajiForSubmit($data),
            'dataJaminanKantong' => $this->setDataJaminanKantongForSubmit($data),
            'dataPinjaman' => $this->setDataPinjamanForSubmit($data),
            'dataNasabah' => $this->setDataNasabahForSubmit($data),
            'flag' => 'S',
            'clientId' => $this->config->item('core_post_username')
        ];

        $rest_mikro = $this->rest_switching_mikro_syariah_service->mikroSyariahPdsHaji($payload);

        if ($rest_mikro['responseCode'] !== '00') {
            $this->arrum_haji_model->deleteByCodeBooking($data->booking_id);
            $this->arrum_haji_agunan_model->deleteByCodeBooking($data->booking_id);

            $this->response['message'] = $rest_mikro['responseDesc'];
            return $this->response;
        }

        $this->response['code']    = "200";
        $this->response['status']  = 'success';
        $this->response['message'] = 'Submit Form Arrum Haji Successfully';

        return $this->response;
    }

    public function getPortofolio($user_id)
    {
        $user = $this->user_model->getUser($user_id);

        if (empty($user->cif)) {
            $this->response['message'] = 'Data CIF tidak ditemukan';
            return [];
        }

        $rest_data = $this->rest_core_mikro_service->byCif(['cif' => $user->cif]);

        return $rest_data['responseData'] ?? [];
    }

    protected function setDataHajiForSubmit($arrum_haji)
    {
        $arrum_haji_user = $arrum_haji->user;
        return [
            'kodeBankHaji' => '',
            'cabangBankHaji' => '',
            'alamatBankHaji' => '',
            'namaLengkapHaji' => $arrum_haji_user->nama,
            'nikHaji' => $arrum_haji_user->no_ktp,
            'tglLahirHaji' => $arrum_haji_user->tgl_lahir,
            'nomorRekeningHaji' => '',
            'jenisHaji' => '1',
        ];
    }

    protected function setDataPinjamanForSubmit($arrum_haji)
    {
        return [
            'angsuran' => $arrum_haji->angsuran,
            'bunga' => '0',
            'collectionOfTipeJaminan' => '',
            'documentChecklist' => '',
            'metodeAngsuran' => 1,
            'periodeBunga' => '01M',
            'rubrik' => 'KT',
            'sektorBI' => '',
            'sewaModal' => '0',
            'sewaModalMaksimal' => '0',
            'tenor' => $arrum_haji->tenor,
            'tglFpk' => date('Y-m-d'),
            'tipeNasabah' => '2',
            'totalTaksiran' => $arrum_haji->taksiran,
            'tujuanKredit' => '6',
            'up' => $arrum_haji->pengajuan_pinjaman,
            'upMax' => $arrum_haji->pengajuan_pinjaman,
            'upPengajuan' => $arrum_haji->pengajuan_pinjaman,
        ];
    }

    protected function setDataNasabahForSubmit($arrum_haji)
    {
        $arrum_haji_user = $arrum_haji->user;

        return [
            'cif' => '',
            'ibuKandung' => $arrum_haji_user->nama_ibu,
            'idKelurahan' => $arrum_haji_user->id_kelurahan,
            'jalan' => $arrum_haji_user->alamat,
            'jenisKelamin' => $arrum_haji_user->jenis_kelamin,
            'kewarganegaraan' => $arrum_haji_user->kewarganegaraan,
            'namaNasabah' => $arrum_haji_user->nama,
            'noHp' => $arrum_haji_user->no_hp,
            'noIdentitas' => $arrum_haji_user->no_ktp,
            'statusKawin' => $arrum_haji_user->status_kawin,
            'tanggalLahir' => $arrum_haji_user->tgl_lahir,
            'tempatLahir' => $arrum_haji_user->tempat_lahir,
            'tipeIdentitas' => $arrum_haji_user->jenis_identitas,
        ];
    }

    protected function setDataJaminanKantongForSubmit($arrum_haji)
    {
        $arrum_haji_agunan = $arrum_haji->agunan;

        foreach ($arrum_haji_agunan as $agunan) {
            $data[] = [
                'tipeJaminan' => $agunan->tipe_jaminan,
                'taksiran' => $agunan->nilai_agunan,
                'karat' => $agunan->karat,
                'berat' => $agunan->gram,
                'jumlah' => $agunan->quantity,
                'beratKotor' => $agunan->gram,
                'jenisPerhiasan' => $agunan->kode_perhiasan,
                'keterangan' => $agunan->label,
            ];
        }

        return $data ?? [];
    }

    private function getDataNotificationStatusArrumHaji($status)
    {
        switch ($status) {
            case 'approve':
                return [
                    'text' => 'Selamat! Pengajuan Arrum Haji Diterima',
                    'html_notification' => 'mail/arrum_haji/approve/mobile_notification',
                    'html_content' => 'mail/arrum_haji/approve/_content_mobile',
                    'email_notification' => 'mail/arrum_haji/approve/email_notification'
                ];
                break;
            case 'decline':
                return [
                    'text' => 'Mohon Maaf Pengajuan Arrum Haji Kamu Ditolak',
                    'html_notification' => 'mail/arrum_haji/decline/mobile_notification',
                    'html_content' => 'mail/arrum_haji/decline/_content_mobile',
                    'email_notification' => 'mail/arrum_haji/decline/email_notification'
                ];
            case 'expire':
                return [
                    'text' => 'Mohon Maaf Pengajuan Arrum Haji Sudah Tidak Berlaku',
                    'html_notification' => 'mail/arrum_haji/expire/mobile_notification',
                    'html_content' => 'mail/arrum_haji/expire/_content_mobile',
                    'email_notification' => 'mail/arrum_haji/expire/email_notification'
                ];
                break;
            default:
                return [
                    'text' => 'Selamat! Pengajuan Arrum Haji Berhasil',
                    'html_notification' => 'mail/arrum_haji/apply/mobile_notification',
                    'html_content' => 'mail/arrum_haji/apply/_content_mobile',
                    'email_notification' => 'mail/arrum_haji/apply/email_notification'
                ];
        }
    }

    public function payment_finpay_mikro($payment, $check_payment)
    {
        $biaya_transaksi = $this->config_model->getRealBiayaPayment($check_payment->jenisTransaksi, 'BANK', $check_payment->productCode, $check_payment->kode_bank);
        $amount = $check_payment->totalKewajiban;

        // max_transaksi 10.000.000, max_transaksi_admin 10.001.500
        $max_transaksi = $this->config_model->whereByVariable('max_transaksi_finpay');
        $max_transaksi_msg = Pegadaian::currencyIdr($max_transaksi->value);
        $max_transaksi_admin = $max_transaksi->value + $biaya_transaksi;

        if ($amount > $max_transaksi->value && $amount < $max_transaksi_admin || $amount > $max_transaksi_admin) {
            $this->response['message'] = "Transaksi tidak dapat diproses, Maksimal transaksi $max_transaksi_msg";
            return $this->response;
        }

        $data = [
            'amount' => strval($amount + $biaya_transaksi),
            'channelId' => $check_payment->data_token->channelId,
            'clientId' => $this->config->item('core_post_username'),
            'customerEmail' => $check_payment->data_user->email,
            'customerName' => $check_payment->data_user->nama,
            'customerPhone' => $check_payment->data_user->no_hp,
            'flag' => 'K',
            'jenisTransaksi' => $check_payment->jenisTransaksi,
            'keterangan' => 'PDS' . $check_payment->jenisTransaksi . ' ' . $check_payment->idTransaksi . ' ' . $check_payment->norek,
            'norek' => $check_payment->norek,
            'productCode' => $check_payment->productCode,
            'trxId' => $check_payment->idTransaksi,
        ];

        $billing_virtual = $this->restswitching_payment_service->createBillingFinPay($data);

        if ($billing_virtual['responseCode'] != '00') {
            $this->response['message'] = $billing_virtual['responseDesc'];
            return $this->response;
        }

        $data_billing = json_decode($billing_virtual['data']);
        $tgl_expired = date('Y-m-d H:i:s', time() + (60 * 180));

        $update_payment_data = [
            'tipe' => $payment,
            'virtual_account' => $data_billing->payment_code,
            'tanggal_expired' => $tgl_expired,
            'payment' => $payment,
            'biayaTransaksi' => $biaya_transaksi,
            'kodeBankPembayar' => $check_payment->kode_bank,
        ];

        $this->mikro_model->updatePayment($check_payment->idTransaksi, $update_payment_data);

        $this->notification_service->notificationMikro(
            $check_payment,
            $data_billing->payment_code,
            $tgl_expired,
            $check_payment->idTransaksi,
            $check_payment->data_token,
            $check_payment->productCode
        );

        $data_payment = [
            'idTransaksi' => $check_payment->idTransaksi,
            'virtualAccount' => $data_billing->payment_code,
            'expired' => $tgl_expired,
            'now' => date('Y-m-d H:i:s'),
        ];

        $response = $this->response_service->getResponse('general.succeeded');
        $response['message'] = 'Mohon lakukan pembayaran ke no rekening yang tertera';
        $response['data'] = $data_payment ?? null;

        return $response;
    }
}
