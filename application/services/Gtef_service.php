<?php

/**
 * @property BankModel bank_model
 * @property User user_model
 * @property Pegadaian Pegadaian
 * @property Payment_service payment_service
 * @property RestSwitchingGtef_service rest_switching_gtef_service
 * @property RestSwitchingPortofolio_service rest_switching_portofolio_service
 * @property Transaction_service transaction_service
 */
class Gtef_service extends MY_Service
{
    // Default Response
    protected $response = [
        'status' => 'error',
        'message' => 'Terjadi kesalahan, mohon coba beberapa saat lagi.',
        'data' => null,
        'code' => 101
    ];

    public $jenisTransaksi = 'Titipan Emas Fisik';
    public $jenisTransaksiTef = 'TE';

    public function __construct()
    {
        parent::__construct();
        // init load model
        $this->load->model('BankModel', 'bank_model');
        $this->load->model('User', 'user_model');
        $this->load->model('GadaiModel', 'gadai_model');
        $this->load->model('MasterModel', 'master_model');
        // init load helper
        $this->load->helper('Pegadaian');
        $this->load->service('RestSwitchingGtef_service', 'rest_switching_gtef_service');
        $this->load->service('Payment_service', 'payment_service');
        $this->load->service('RestSwitchingPortofolio_service', 'rest_switching_portofolio_service');
        $this->load->service('Transaction_service', 'transaction_service');
        $this->load->service('Notification_service', 'notification_service');
        $this->load->service('Response_service', 'response_service');
    }

    public function listRekening($user_id)
    {
        log_message('Debug', 'START OF SERVICE ' . __FUNCTION__ . ' => ' . json_encode($user_id));

        $user = $this->user_model->getUser($user_id);
        if (empty($user)) {
            $this->response['message'] = 'Data user not found';
            log_message('Debug', 'END OF SERVICE ' . __FUNCTION__ . ' => ' . json_encode($this->response));
            return $this->response;
        }

        if (empty($user->cif)) {
            $this->response['message'] = 'Data cif not found';
            log_message('Debug', 'END OF SERVICE ' . __FUNCTION__ . ' => ' . json_encode($this->response));
            return $this->response;
        }

        $items = $this->getDataListRekening(['cif' => $user->cif]);
        if (empty($items)) {
            $this->response['message'] = 'Data list rekening tidak ditemukan';
            log_message('Debug', 'END OF SERVICE ' . __FUNCTION__ . ' => ' . json_encode($this->response));
            return $this->response;
        }

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Get list rekening successfully';
        $this->response['data'] = $items;

        log_message('Debug', 'END OF SERVICE ' . __FUNCTION__ . ' => ' . json_encode($this->response));

        return $this->response;
    }

    public function listRiwayatTE($user_id)
    {
        $user = $this->user_model->getUser($user_id);

        if (empty($user)) {
            $this->response['message'] = 'Data cif tidak ada';
            return $this->response;
        }

        $request = array(
            'cif' => $user->cif
        );

        $rest_portofolio_tipemas = $this->rest_switching_portofolio_service->portofolioTitipanEmas($request);
        $rest_portofolio_tipemas_data = json_decode($rest_portofolio_tipemas['data']);

        $result = [
            'status' => 'success',
            'message' => 'Sukses Get Portofolio Titipan Emas ',
            'data' => $rest_portofolio_tipemas_data
        ];

        if ($rest_portofolio_tipemas['responseCode'] != 00) {
            $result = [
                'status' => 'error',
                'message' => $rest_portofolio_tipemas['responseDesc'],
                'data' => null
            ];
        }

        return $result;
    }

    public function getDetailGadaiTitipanEmas($dataKontrak)
    {
        $rest_portofolioDetail = $this->rest_switching_gtef_service->getRestDetailGtef(['noKontrak' => $dataKontrak]);
        
        if ($rest_portofolioDetail['responseCode'] != "00") {
            $this->response['message'] = $rest_portofolioDetail['responseDesc'] ?? $rest_portofolioDetail['responseMessage'];

            return $this->response;
        }

        $data = json_decode($rest_portofolioDetail['data']);
        $sisa_limit = $data->sisaLimit ?? 0;
        $pinjaman_aktif = $data->plafonDiambil ?? 0;
        $nilai_taksiran = $data->nilaiTaksiran ?? 0;
        $max_pinjaman = $sisa_limit + $pinjaman_aktif;
        $jumlah_pinjaman_minimal = $this->getMinLoanAmount();
        
        $dataDetail = [
            'nomor_titipan' => $data->noTitipan ?? null,
            'nomor_kredit' => $data->noKontrak ?? null,
            'nama_nasabah' => $data->namaNasabah ?? null,
            'cif' => $data->cif ?? null,
            'kode_cabang' => $data->kodeCabang ?? null,
            'nama_cabang' => $data->namaCabang ?? null,
            'tgl_jatuh_tempo' => Pegadaian::formatDateDmy($data->tglJatuhTempo),
            'tgl_titipan' => Pegadaian::formatDateDmy($data->tglTitipan),
            'nilai_taksiran_validasi' => $nilai_taksiran,
            'nilai_taksiran' => $nilai_taksiran,
            'deskripsi' => "Dapatkan Pinjaman Hingga ".Pegadaian::currencyIdr($sisa_limit)." Dengan Melakukan Gadai Titipan Emas.",
            'sisa_limit_validasi' => $sisa_limit,
            'sisa_limit' => Pegadaian::currencyIdr($sisa_limit),
            'jumlah_pinjaman_minimal_validasi' => $jumlah_pinjaman_minimal,
            'jumlah_pinjaman_minimal' => Pegadaian::currencyIdr($jumlah_pinjaman_minimal),
            'status_transaksi' => 'OP',
            'tenor' => $data->tenor ?? null,
            'pinjaman_aktif_validasi' => $pinjaman_aktif,
            'pinjaman_aktif' => Pegadaian::currencyIdr($pinjaman_aktif),
            'max_pinjaman_valdiasi' => $max_pinjaman,
            'max_pinjaman' => Pegadaian::currencyIdr($max_pinjaman),
        ];

        if ($pinjaman_aktif > 0) {
            $dataDetail['deskripsi'] = "Dapatkan Pinjaman Dengan Melakukan Gadai Titipan Emas.";
            $dataDetail['status_transaksi'] = 'MT';
        } 

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Sukses get detail titipan emas dan kredit';
        $this->response['data'] = $dataDetail ?? null;

        return $this->response;
    }

    public function validasiPopupNotifikasi($user_id, $request)
    {
        $user = $this->user_model->getUser($user_id);

        if (empty($user)) {
            $this->response['message'] = 'Data user tidak ditemukan';

            return $this->response;
        }

        if (empty($user->cif)) {
            $this->response['message'] = 'CIF tidak ditemukan!';

            return $this->response;
        }

        $data = $this->responValidasiPopupNotifikasi(['noKontrak' => $request['nomor_rekening']]);

        if (empty($data)) {
            $this->response['status'] = 'error';
            $this->response['message'] = 'Gagal Validasi Nomor Rekening';

            return $this->response;
        }
        
        $this->response = $data;
        return $this->response;
    }

    public function cekPengajuanPlafon($user_id)
    {
        $user = $this->user_model->getUser($user_id);
        $clientId = $this->config->item('core_post_username');
        $channelId = $this->config->item('core_client_id');

        $request = array(
            'cif' => $user->cif,
            'clientId' => $clientId,
            'channelId' => $channelId
        );

        $rest_portofolio_service = new RestSwitchingPortofolio_service();
        $rest_portofolio_tipemas = $rest_portofolio_service->portofolioTitipanEmas($request);

        if (empty($user->cif)) {
            $this->response['message'] = 'Data cif tidak ada';
            return $this->response;
        }

        if ($user->kyc_verified == '0' && $user->aktifasiTransFinansial == '0') {
            $this->response['message'] = 'Upgrade ke Akun Premium';
            return $this->response;
        }

        if ($user->kyc_verified == '1' && $user->aktifasiTransFinansial == '0') {
            $this->response['message'] = 'Upgrade ke Akun Premium';
            return $this->response;
        }

        if (empty($rest_portofolio_tipemas["data"])) {
            $this->response['message'] = 'Belum Memiliki Titipan Emas';
            return $this->response;
        }

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Sukses get cek pengajuan plafon titipan emas';

        return $this->response;
    }

    private function cekValidasiTanggal($data_validasi)
    {
        if (!array_key_exists('tglJatuhTempo', $data_validasi)) {
            return false;
        }

        $tgl_jatuh_tempo = new DateTime($data_validasi['tglJatuhTempo']);
        $tgl_sekarang = new DateTime();

        if ($tgl_sekarang > $tgl_jatuh_tempo) {
            return false;
        }

        $selisih_tanggal = $tgl_jatuh_tempo->diff($tgl_sekarang);

        if ($selisih_tanggal->y != 0 || $selisih_tanggal->m != 0 || $selisih_tanggal->d > 30) {
            return false;
        }

        return true;
    }

    /**
     * Func for store data to table transaction
     *
     * @param array $trx_product
     * @param array $trx
     * @param null $trx_id
     * @return array|bool|mixed
     */
    private function storeTrx(array $trx_product, array $trx, $trx_id = null)
    {
        $trx['relation_id'] = $trx_product['relation_id'];
        $trx['relation_table'] = $trx_product['relation_table'];
        
        if (empty($trx['relation_id']) || empty($trx['relation_table'])) {
            return null;
        }
        
        if (!empty($trx_id)) {
            unset($trx['trx_id']);
            return $this->transaction_model->updateById($trx_id, $trx);
        }
        
        return $this->transaction_model->save($trx);
    }

    public function cekPengajuanTitipanEmas($user_id)
    {
        $user = $this->user_model->getUser($user_id);

        if (empty($user->cif)) {
            $this->response['message'] = 'Data cif tidak ada';
            return $this->response;
        }

        if ($user->aktifasiTransFinansial == '0') {
            $this->response['message'] = [
                'title' => 'Upgrade ke Akun Premium',
                'desc' => 'Gunakan fitur ini dan dapatkan berbagai penawaran menarik dengan upgrade akun kamu melalui cabang terdekat.',
                'button' => [
                    [
                        'title' => 'Lihat Cabang Terdekat',
                        'callback' => 'CabangPegadaian',
                        'color' => 'white',
                    ],
                    [
                        'title' => 'Upgrade Sekarang',
                        'callback' => 'EKYC',
                        'color' => 'green',
                    ],
                ],
            ];

            return $this->response;
        }

        if ($user->kyc_verified == '0') {
            $this->response['message'] = [
                'title' => 'Upgrade ke Akun Premium',
                'desc' => 'Gunakan fitur ini dan dapatkan berbagai penawaran menarik dengan upgrade akun kamu melalui cabang terdekat.',
                'button' => [
                    [
                        'title' => 'Lihat Cabang Terdekat',
                        'callback' => 'CabangPegadaian',
                        'color' => 'green',
                    ]
                ],
            ];

            return $this->response;
        }

        $request = array(
            'cif' => $user->cif,
        );

        $rest_portofolio_tipemas = $this->rest_switching_portofolio_service->portofolioTitipanEmas($request);
        $dataCek = json_decode($rest_portofolio_tipemas['data']);

        if ($rest_portofolio_tipemas['responseCode'] == '14') {
            $this->response['message'] = [
                'title' => 'Belum Memiliki Titipan Emas',
                'desc' => 'Kamu harus memiliki titipan emas untuk melakukan gadai titipan emas',
                'button' => [
                    [
                        'title' => 'Informasi Selengkapnya',
                        'callback' => 'FAQTitipanEmas',
                        'color' => 'white',
                    ],
                    [
                        'title' => 'Lihat Cabang Terdekat',
                        'callback' => 'CabangPegadaian',
                        'color' => 'green',
                    ],
                ],
            ];
            
            return $this->response;
        }

        if ($rest_portofolio_tipemas['responseCode'] != "00") {
            $this->response['message'] = $rest_portofolio_tipemas['responseDesc'];

            return $this->response;
        }

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Sukses cek Pengajuan Titipan Emas';
        $this->response['data'] = $dataCek ?? null;

        return $this->response;
    }

    public function checkValidationLoanAmount($loan_amount, $rental_cost_fee, $insurance_surcharge, $admin_surcharge)
    {
        $loan_amount = (int)$loan_amount;
        $limit_loan_amount = (int)$rental_cost_fee + (int)$insurance_surcharge + (int)$admin_surcharge;

        return $limit_loan_amount > $loan_amount;
    }

    protected function getDataListRekening($payload)
    {
        $rest_list_portofolio = $this->rest_switching_gtef_service->listRekening($payload);

        if (empty($rest_list_portofolio['responseCode']) || $rest_list_portofolio['responseCode'] !== '00') {
            return null;
        }

        $items = json_decode($rest_list_portofolio['data']);

        if (empty($items)) {
            return null;
        }

        foreach ($items as $item) {
            $data[] = $this->mappingDataTitipan($item);
        }

        return $data ?? [];
    }

    protected function mappingDataTitipan($item)
    {
        $account_number = $item->noTitipan ?? null;
        $pinjaman_aktif = ($item->plafonDiambil != "0") ? Pegadaian::currencyIdr($item->plafonDiambil) : null;
        $status_transaksi = ($item->plafonDiambil == "0") ? "OP" : "MT";
        $sisa_limit_validasi = ($item->sisaLimit != "0") ? $item->sisaLimit : 0;
        $sisa_limit = Pegadaian::currencyIdr($sisa_limit_validasi);
        $jumlah_pinjaman_minimal_validasi = $this->getMinLoanAmount();
        $jumlah_pinjaman_minimal = Pegadaian::currencyIdr($jumlah_pinjaman_minimal_validasi);
        $tenor = $item->tenor ?? null;

        return [
            'account_number' => $account_number,
            'sisa_limit' => $sisa_limit,
            'sisa_limit_validasi' => $sisa_limit_validasi,
            'pinjaman_aktif' => $pinjaman_aktif,
            'jumlah_pinjaman_minimal' => $jumlah_pinjaman_minimal,
            'jumlah_pinjaman_minimal_validasi' => $jumlah_pinjaman_minimal_validasi,
            'tenor' => $tenor,
            'status_transaksi' => $status_transaksi,
        ];
    }

    private function getMinLoanAmount()
    {
        $min_gadai_amount = $this->master_model->getMinMaxAmountGtef('min_gadai_titipan_emas'); 

        return $min_gadai_amount ?? 0;
    }

    private function responValidasiPopupNotifikasi($payload)
    {
        $rest_list_portofolio = $this->rest_switching_gtef_service->cekStatus($payload);

        if (empty($rest_list_portofolio['responseCode']) || $rest_list_portofolio['responseCode'] !== '00') {
            return null;
        }

        $data_validasi = json_decode($rest_list_portofolio['data'], true);

        if (empty($data_validasi)) {
            return null;
        }

        // Titipan Emas sudah tidak aktif atau masih aktif
        // if ($this->titipanTidakAktif($data_validasi)) {
        //     return $this->response_service->getResponse('gtef.titipanSudahTidakAktif');
        // }

        // Waktu pengajuan gadai memasuki H-30 masa titipan berakhir
        if ($this->cekValidasiTanggal($data_validasi)) {
            return $this->response_service->getResponse('gtef.waktuTitipanSegeraBerakhir');
        }
        
        // cek kondisi harga emas turun
        // Tagihan (Jasa Bulanan, Denda Jasa Bulanan, Biaya Perpanjang Titipan) > Sisa Limit.
        // if ($this->cekHargaEmasTurun($data_validasi) && $this->totalTagihanLebihDariLimit($data_validasi)) {
        //     return $this->response_service->getResponse('gtef.hargaEmasTurun');
        // }

        // Tagihan (Jasa Bulanan, Denda Jasa Bulanan, Biaya Perpanjang Titipan) > Sisa Limit.
        // if ($this->totalTagihanLebihDariLimit($data_validasi)) {
        //     return $this->response_service->getResponse('gtef.kewajibanMelebihiSisaLimit');
        // }

        // Terdapat tagihan jasa bulanan dan atau Denda Jasa Bulanan, dan Biaya Perpanjang Titipan.
        // if ($this->tagihanJasaBulanan($data_validasi) && $this->dendaJasaBulanan($data_validasi) && $this->biayaPerpanjangTitipan($data_validasi)) {
        //     return $this->response_service->getResponse('gtef.masihMemilikiTagihanJasaBulanan_DendaJasaBulanan_BiayaPerpanjangTitipan');
        // }

        // Terdapat tagihan jasa bulanan dan atau Denda Jasa Bulanan
        // if ($this->tagihanJasaBulanan($data_validasi) && $this->dendaJasaBulanan($data_validasi)) {
        //     return $this->response_service->getResponse('gtef.masihMemilikiTagihanJasaBulanan_DendaJasaBulanan');
        // }
                
        // jika popup tidak muncul
        $this->response = $this->response_service->getResponse('');
        $this->response['code'] = '00';
        $this->response['status'] = 'success';
        $this->response['message'] = 'Sukses Get Notifikasi Popup';
        
        return $this->response;
    }

    private function titipanTidakAktif($data_validasi)
    {
        $is_titipan_aktif = (int) $data_validasi['isTitipanAktif'] ?? 0;

        if ($is_titipan_aktif > 0) {
           return false;
       }

        return true;
    }

    private function tagihanJasaBulanan($data_validasi)
    {
        $tagihan_jasa_bulanan = (int) $data_validasi['tagihanSM'] ?? 0;

        if ($tagihan_jasa_bulanan > 0) {
           return true;
       }

        return false;
    }

    private function dendaJasaBulanan($data_validasi)
    {
        $tagihan_denda_jasa_bulanan = (int) $data_validasi['tagihanDenda'] ?? 0;

        if ($tagihan_denda_jasa_bulanan > 0) {
            return true;
        }
        
        return false;
    }

    private function biayaPerpanjangTitipan($data_validasi)
    {
        $biaya_perpanjang_titipan = (int) $data_validasi['tagihanPerpanjang'] ?? 0;
        
        if ($biaya_perpanjang_titipan > 0) {
            return true;
        }
        
        return false;
    }

    private function cekHargaEmasTurun($data_validasi)
    {
        $nilai_taksiran = (int) $data_validasi['taksiran'] ?? 0;
        $nilai_taksiran_baru = (int) $data_validasi['nilaiTaksiranBaru'] ?? 0;

        if ($nilai_taksiran > $nilai_taksiran_baru) {
            return true;
        }

        return false;
    }
    
    private function totalTagihanLebihDariLimit($data_validasi)
    {
        $tagihan_jasa_bulanan = (int) $data_validasi['tagihanSM'] ?? 0;
        $tagihan_denda_jasa_bulanan = (int) $data_validasi['tagihanDenda'] ?? 0;
        $biaya_perpanjang_titipan = (int) $data_validasi['tagihanPerpanjang'] ?? 0;
        $total_tagihan = $tagihan_jasa_bulanan + $tagihan_denda_jasa_bulanan + $biaya_perpanjang_titipan;
        $sisa_limit = (int) $data_validasi['sisaLimit'] ?? 0;

        if ($total_tagihan > $sisa_limit) {
            return true;
        }

        return false;
    }

    public function addNoTitipanFavorite($dataInsert, $token)
    {
        
        $data = [
            'no_rekening'      => $dataInsert['no_rekening'],
            'nama_nasabah'     => $dataInsert['nama'],
            'tipe'             => $dataInsert['tipe'],
            'jenis_transaksi'  => $dataInsert['jenis_transaksi'],
            'user_AIID'        => $token->id,
            'kodeBank'         => $dataInsert['kode_bank'] ?? '',
            'namaBank'         => $dataInsert['nama_bank'] ?? '',
            'mpo_group'        => $dataInsert['group'] ?? '',
            'kode_layanan_mpo' => $dataInsert['kodeLayananMpo'] ?? ''
          ];

          log_message('debug', 'start of insert favorite ' . __FUNCTION__ . ' => ' . json_encode($data));
      
          $dataResponse = [
            'id'             => $this->User->addFavorite($data),
            'tipe'           => $data['tipe'],
            'jenisTransaksi' => $data['jenis_transaksi'],
            'noRekening'     => $data['no_rekening']
          ];

          return $dataResponse;
    }

    public function getTitipanPortofolio($cif)
    {
        $this->response['status']  = 'success';
        $this->response['message'] = 'Portofolio Titipan Emas tidak tersedia';
        $this->response['data']    = [];

        if (empty($cif)) {
            $this->response['message'] = 'CIF tidak ditemukan!';
            return $this->response;
        }

        $rest_list_portofolio = $this->rest_switching_gtef_service->listRekening(['cif' => $cif], false);
        
        if ($rest_list_portofolio['responseCode'] !== '00') {
            $this->response['message'] = $rest_list_portofolio['responseDesc'];
            $this->response['data'] = '';
            return $this->response;
        }

        $portofoliolist = json_decode($rest_list_portofolio['data'], false);
        foreach ($portofoliolist as $portofolio) {
            $tglJatuhTempo = new DateTime($portofolio->tglJatuhTempo);
            $tglSekarang = new DateTime();
            
            if ($tglSekarang > $tglJatuhTempo) {
                $portofolio->sisaHariTitipan = 0;
                continue;
            }

            $selisihTanggal = $tglJatuhTempo->diff($tglSekarang)->format("%a");
            $portofolio->sisaHariTitipan = "Sisa " .$selisihTanggal. " Hari";
        }

        $rest_list_portofolio['data'] = $portofoliolist;
        $this->response['message'] = 'Success Get Portofolio Titipan Emas';
        $this->response['data'] = $rest_list_portofolio['data'];

        return $this->response;
    }

    public function inquiryTitipanEmas($user_id, $request)
    {
        try {
            $user = $this->user_model->profile($user_id);
            if (empty($user)) {
                $this->response['message'] = 'Data user not found!';
                return $this->response;
            }

            $trx_type_func = $this->getClassFuncByTrxType("inquiry", $request['product']);
            $this->$trx_type_func($user, $request);

            return $this->response;
        } catch (Exception $e) {
            log_message('debug', 'ERROR MESSAGE' . __FUNCTION__ . ' => ' . $e->getMessage());
            return $this->response;
        }
    }

    public function inquiryPayments($user, $request)
    {
        $rest_gtef = $this->rest_switching_gtef_service->getRestDetailGtef($request);
        
        if ($rest_gtef['responseCode'] != "00") {
            $this->response['message'] = 'Nomor Titipan Emas anda tidak ditemukan';
            return $this->response;
        }

        $productCode = substr($request['noKontrak'], 7, 2);
        $gtef = json_decode($rest_gtef['data']);

        $biayaChannel = $this->payment_service->getBiayaPayment($this->jenisTransaksiTef, $productCode);

        $response_data = [
            'jenis_transaksi' => $this->jenisTransaksi,
            'titipan_berlaku_hingga' => $gtef->tglJatuhTempo,
            'nama_pemilik_titipan' => $gtef->namaNasabah,
            'nomor_titipan' => $gtef->noTitipan,
            'nilai_titipan' => $gtef->nilaiTitipan,
            'kategori_titipan' => $gtef->kategori,
            'cabang' => $gtef->namaCabang,
            'biaya_titipan' => $gtef->biayaTitip,
            'total' => $gtef->nilaiTitipan + $gtef->biayaTitip,
            'biaya_channel' => $biayaChannel
        ];
        
        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = '';
        $this->response['data'] = $response_data;

        return $this->response;
    }

    /**
     * Func to get dynamic name class inquiry transaction
     *
     * @param $type "inquiry" || "submit"
     * @param $trx_type
     * @return string
     */
    private function getClassFuncByTrxType($type, $trx_type)
    {
        return $type . ucfirst($trx_type);
    }

    public function inquiryPaymentGtef($amount, $jenisTransaksi, $norek, $token, $productCode)
    {
        $min_gadai_amount = $this->master_model->getMinMaxAmountGtef('min_gadai_titipan_emas');
        $val_taksiran = $this->master_model->getValTaksiranGtef('val_taksiran_gtef'); // val taksiran 300jt
        $min_kredit_aktif = $this->master_model->getValTaksiranGtef('min_val_kredit_aktif_gtef'); // val taksiran dibawah 300jt

        // val ini untuk jenis transaksi cicil saja
        if ($jenisTransaksi == 'CC' && $amount < $min_gadai_amount) {
            $this->response['message'] = 'Minimal pembayaran harus ' . Pegadaian::currencyIdr($min_gadai_amount);

            return $this->response;
        }

        // val ini untuk jenis transaksi cicil saja, inputan kelipatan 100 ribu
        if ($jenisTransaksi == 'CC' && $amount % $min_gadai_amount != 0 && $amount > 0) {
            $this->response['message'] = 'Pembayaran harus kelipatan ' . Pegadaian::currencyIdr($min_gadai_amount);

            return $this->response;
        }

        $data = array(
            'channelId' => $token->channelId,
            'clientId' => $this->config->item('core_post_username'),
            'amount' => $amount,
            'jenisTransaksi' => $jenisTransaksi,
            'norek' => $norek,
        );

        $inquiry = $this->rest_switching_gtef_service->inquiryGtefPayment($data);

        if ($inquiry['responseCode'] != '00') {
            $this->response['message'] = $inquiry['responseDesc'];
            return $this->response;
        }

        $dataInquiry = json_decode($inquiry['data']);

        //ini untuk perhitungan jenis transaksi cicil jika taksiran diatas 300 juta
        if ($dataInquiry->jenisTransaksi == 'CC' && $dataInquiry->taksiranBaru > $val_taksiran) {
            $kredit_aktif = $dataInquiry->taksiranBaru * 0.05; // ini Kredit aktif khusus taksiran diatas 300 juta
            $cicil_maksimal = $dataInquiry->up - $kredit_aktif; //cicil maksimal
            $potong = substr($cicil_maksimal, -5); //pemotongan 5 angka
            $pembulatan = $cicil_maksimal - $potong; //pembulatan 100000 kebawah
        }

        //ini untuk perhitungan jenis transaksi cicil jika taksiran dibawah 300 juta
        if ($dataInquiry->jenisTransaksi == 'CC' && $dataInquiry->taksiranBaru < $val_taksiran) {
            $cicil_maksimal = $dataInquiry->up - $min_kredit_aktif; //cicil maksimal
            $potong = substr($cicil_maksimal, -5); //pemotongan 5 angka
            $pembulatan = $cicil_maksimal - $potong; //pembulatan 100000 kebawah
        }

        // ini untuk jika hasil cicil maksimal = 0
        if ($dataInquiry->jenisTransaksi == 'CC' && $cicil_maksimal == '0') {
            $this->response['message'] = [
                'title' => 'Lakukan Pelunasan',
                'desc' => 'Cicilan kamu sudah maksimal, silahkan lakukan pelunasan',
                
            ];
            return $this->response;
        }
    
        //ini val untuk jika amount jenis transaksi cicil besar dari cicil maksimal
        if ($dataInquiry->jenisTransaksi == 'CC' && $amount > $cicil_maksimal) {
            $this->response['message'] = [
                'title' => 'Transaksi Melebihi Limit Maksimal',
                'desc' => 'Cicilan yang dapat dibayarkan ' . Pegadaian::currencyIdr($pembulatan),
                
            ];
            return $this->response;
        }
        
        $saveData = json_decode(json_encode($dataInquiry), true);
        $saveData['user_AIID'] = $token->id;

        $this->gadai_model->savePayment($saveData);

        $restData = $saveData;

        $restData['cicil_maksimal'] =  $pembulatan ?? 0;
        $restData['surcharge'] = $restData['surcharge'] ?? null;
        $restData['administrasi'] = $restData['administrasi'] ?? null;
        $restData['sewaModal'] = $restData['sewaModal'] ?? null;
        $restData['denda'] = $restData['denda'] ?? null;
        $restData['hariTarif'] = $restData['jumlahHariReal'] . '(' . $restData['jumlahHariTarif'] . '%' . ')';

        $restData['idTransaksi'] = $saveData['reffSwitching'];
        $restData['produkCode'] = $productCode;
        $restData['bookingCode'] = sprintf("%06d", mt_rand(1, 999999));

        $biayaChannel = $this->payment_service->getBiayaPayment($jenisTransaksi, $productCode);
        $restData['biayaChannel'] = $biayaChannel;

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Berhasil inquiry ' . $restData['namaProduk'];
        $this->response['data'] = $restData ?? null;

        return $this->response;
    }

    public function getNorekInquiryPayment($noKontrak)
    {
        $productCode = substr($noKontrak, 7, 2);
        $data = array(
            'noKontrak' => $noKontrak,
        );

        $cekNoKontrak = $this->rest_switching_gtef_service->cekNorekGtefPayment($data);
        $cekNoKontrak['namaProduk'] = 'Gtef';
        if ($productCode != '43') {
            $cekNoKontrak['namaProduk'] = 'Gadai';
        }
        
        if ($cekNoKontrak['responseCode'] != '00') {
            $this->response['message'] = 'tidak valid';
            $this->response['data'] = $cekNoKontrak;
            return $this->response;
        }

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'valid';
        $this->response['data'] =  $cekNoKontrak ?? null;

        return $this->response;
    }

    // untuk filtering rekening yang group gadai
    public function list_kredit_aktif_group_gadai($user_id)
    {
        $user = $this->user_model->getUser($user_id);
        if (empty($user)) {
            $this->response['message'] = 'Data user tidak ditemukan';
            return $this->response;
        }

        if (empty($user->cif)) {
            $this->response['message'] = 'Data cif tidak ada';
            return $this->response;
        }

        $items_gadai = $this->rest_switching_portofolio_service->portofolio_pinjaman(['cif' => $user->cif]);

        if ($items_gadai['responseCode'] != '00') {
            $this->response['message'] = $items_gadai['responseDesc'];
            return $this->response;
        }
        
        $data_gadai = json_decode($items_gadai['data']);
        $filter_gadai = array_filter(!empty($data_gadai) ? $data_gadai : [], array($this, "filter_gadai"));
        $list_rekening = array_merge($filter_gadai);
        
        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'List kredit aktif by cif';
        $this->response['data'] = $list_rekening;

        return $this->response;
    }

    public function checkOnboarding($user_id)
    {
        $user = $this->user_model->getUser($user_id);
        if (empty($user)) {
            $this->response['message'] = 'Data user tidak ditemukan';
            return $this->response;
        }

        if (empty($user->cif)) {
            $this->response['message'] = 'Data cif tidak ada';
            return $this->response;
        }

        $data = $this->rest_switching_gtef_service->restCheckOnboarding(['cif' => $user->cif]);

        $item['show_onboarding'] = true;
        $this->response['message'] = 'Tidak punya kredit gadai titipan emas aktif';
        
        if ($data['responseCode'] == '00') {
            $item['show_onboarding'] = false;
            $this->response['message'] = 'Punya kredit gadai titipan emas aktif';
        }

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['data'] = $item;
        
        return $this->response;
    }

    private function filter_gadai($var)
    {
        return($var->group == 'gadai');
    }

}
