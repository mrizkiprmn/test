<?php

/**
 * @property MpoModel mpo_model
 * @property RestSwitchingMpo_service rest_switching_mpo_service
 */
class Mpo_service extends MY_Service
{
    protected $response = [
        'code' => 101,
        'status' => 'error',
        'message' => 'Terjadi kesalahan mohon coba lagi',
        'data' => null
    ];

    public function __construct()
    {
        parent::__construct();

        $this->load->helper('Pegadaian');
        $this->load->model('MpoModel', 'mpo_model');
        $this->load->model('PaymentModel', 'payment_model');
        $this->load->model('GcashModel', 'gcash_model');
        $this->load->model('MasterModel', 'master_model');
        $this->load->model('ConfigModel', 'config_model');
        $this->load->model('BankModel', 'bank_model');
        $this->load->service('Gcash_service', 'Gcash_service');
        $this->load->service('RestSwitchingMpo_service', 'rest_switching_mpo_service');
        $this->load->service('RestSwitchingPaymentEmas_service', 'rest_switching_payment_emas_service');
        $this->load->service('RestSwitchingPayment_service', 'rest_switching_payment_service');
        $this->load->service('Notification_service', 'notification_service');
    }

    public function validateMpoTransaction($token, $idTransaksi)
    {

        $this->response['status']  = 'error';
        $this->response['message'] = 'Data transaksi tidak ditemukan';
        $this->response['data']    = '';
        $this->response['code'] = '102';

        $mpo = $this->mpo_model->getMpo($idTransaksi);

        if (empty($mpo)) {
            log_message('debug', __FUNCTION__ . ' Validasi Pembayaran MPO error ' . 'Data transaksi tidak ditemukan');

            return $this->response;
        }

        //Get info total kewajiban yang harus dibayar
        $newTotalKewajiban = $mpo->totalKewajiban + 2500;

        //Get gcash data
        $Gcash_service = new Gcash_service;
        $response = $Gcash_service->getGcash($token);
        
        log_message('debug', 'Get data for nominal validation:' . json_encode($response));

        if ($response['code'] != 200) {
            log_message('debug', __FUNCTION__ . ' Validasi Pembayaran MPO error ' . $response['message']);

            $this->response['status']  = 'error';
            $this->response['message'] = $response['message'];
            $this->response['data']    = '';

            return $this->response;
        }

        if ($response['data']['totalSaldo'] < $newTotalKewajiban) {
            log_message('debug', __FUNCTION__ . ' Validasi Pembayaran MPO error ' . 'Saldo tidak mencukupi');

            $this->response['status']  = 'error';
            $this->response['message'] = 'Saldo tidak mencukupi';
            $this->response['data']    = ['totalSaldo' => $response['data']['totalSaldo']];

            return $this->response;
        }

        $this->response['status']  = 'success';
        $this->response['message'] = 'Saldo mencukupi';

        log_message('debug', __FUNCTION__ . ' Validasi Pembayaran MPO ' . 'End');
        return $this->response;
    }

    public function getMasterMpoByGroup($group)
    {
        $mpo = $this->mpo_model->getKodeLayananAndKodeBiller(['groups' => $group]);
        $mpo_sell_price = $this->getSellPriceMpo($mpo);

        if (empty($mpo_sell_price)) {
            $this->response['message'] = 'Data Harga Mpo tidak ditemukan';
            return $this->response;
        }

        $mpo = $this->mpo_model->get(['groups' => $group]);
        foreach ($mpo as $item) {
            $data = $item;
            $sell_price = $this->mappingSellPriceMpo($mpo_sell_price, $item['kodeBiller'], $item['kodeLayananMpo']);
            $data['hargaJual'] = $sell_price;
            if (!empty($sell_price)) {
                $result[] = $data;
            }
        }

        if (empty($mpo)) {
            $this->response['message'] = 'Data tidak ditemukan';
            return $this->response;
        }

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Get Data Master Mpo Successfully';
        $this->response['data'] = $result;

        return $this->response;
    }

    public function getSellPriceMpo($mpo)
    {
        $payload = [
            'flag' => 'K',
            'listProductMpo' => $mpo
        ];

        $rest_mpo = $this->rest_switching_mpo_service->checkHarga($payload);

        if ($rest_mpo['responseCode'] != '00') {
            return [];
        }

        $data = json_decode($rest_mpo['data'], true);

        return $data['listLayananMpo'] ?? [];
    }

    public function latestTransactionByMpoGroup($user_id, $request)
    {
        $limit = $request['limit'] ?? 5;
        $mpo_group = $this->mpo_model->get(['groups' => $request['groups']]);

        if (empty($mpo_group)) {
            $this->response['message'] = 'Mpo Group tidak ditemukan';
            return $this->response;
        }

        $kode_layanan_mpo = array_column($mpo_group, 'kodeLayananMpo');
        $latest_transaction_mpo = $this->mpo_model->getTransactionByUserIdAndKodeLayananMpo($user_id, $kode_layanan_mpo, $limit);

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Get Latest Transaction Mpo Successfully';
        $this->response['data'] = $latest_transaction_mpo;

        return $this->response;
    }

    private function mappingSellPriceMpo($data_mpo_sell_price, $biller_code, $service_code)
    {
        foreach ($data_mpo_sell_price as $mpo_sell_price) {
            $biller_code_temp = $mpo_sell_price['kodeBiller'];
            $service_code_temp = $mpo_sell_price['kodeLayananMpo'];

            if ($biller_code_temp === $biller_code && $service_code_temp === $service_code) {
                return Pegadaian::currencyIdr($mpo_sell_price['hargaJual']);
            }
        }

        return null;
    }

    public function getFavoritePayment($total_transaksi, $token)
    {
        $gcash_inquiry = $this->Gcash_service->getGcash($token);

        $data_response = null;
        if (count($gcash_inquiry['data']['va']) != 0) {
            $data_response = $this->checkGcashAvailability($gcash_inquiry, $total_transaksi);
        }

        if (!empty($data_response)) {
            return $data_response;
        }

        $payment_data = $this->payment_model->getPaymentMpoByUserId($token->id);
        
        if (!$payment_data) {
            return $data_response;
        }

        $payment_data = $this->mappingPaymentTipe($payment_data);
        $data_response = $this->mappingLastPaymentData($payment_data);

        return $data_response;
    }

    private function mappingPaymentTipe($payment_data)
    {
        $arrayVA = array('BNI', 'FINPAY', 'VA_BCA', 'VA_BRI', 'VA_MANDIRI', 'VA_MAYBANK', 'VA_PERMATA');
        if (in_array($payment_data->payment, $arrayVA)) {
            $payment_data->payment_tipe = 'VA';
        }

        $arrayOthers = array('MANDIRI');
        if (in_array($payment_data->payment, $arrayOthers) !== false) {
            $payment_data->payment_tipe = 'CLICK_MANDIRI';
        }

        return $payment_data;
    }

    private function mappingLastPaymentData($payment_data)
    {
        $data_response = new stdClass();

        if ($payment_data->payment_tipe == 'VA') {
            $data = $this->mappingPaymentMethodDataVA($payment_data);

            $data_response->type = $payment_data->payment_tipe;
            $data_response->paymentMethodName = $data->payment_method_name;
            $data_response->paymentMethodID = $payment_data->payment;
            $data_response->image = $data->image;
            $data_response->biaya_channel = $this->ConfigModel->getBiayaPayment($payment_data->jenis_transaksi, 'BANK', $payment_data->kode_produk, $payment_data->kodeBankPembayar);
            $data_response->status = true;
        }

        if ($payment_data->payment_tipe == 'CLICK_MANDIRI') {
            $data = $this->mappingPaymentMethodDataOthers($payment_data);

            $data_response->type = $payment_data->payment_tipe;
            $data_response->paymentMethodName = $data->payment_method_name;
            $data_response->paymentMethodID = $payment_data->payment;
            $data_response->image = $data->image;
            $data_response->biaya_channel = $this->ConfigModel->getBiayaPayment($payment_data->jenis_transaksi, 'BANK', $payment_data->kode_produk, $payment_data->kodeBankPembayar);
            $data_response->status = true;
        }

        $data_response->isGcash = false;

        return $data_response;
    }

    //Khusus Pembayaran Maybank
    public function mpoPayment($payment, $mpo, $mpoProduct, $mpoGroup, $product, $newTotalKewajiban, $idTransaksi, $user, $token, $kodeBank)
    {
        $biayaTransaksi = $this->config_model->getRealBiayaPayment($mpo->jenisTransaksi, 'BANK', '50', $kodeBank);
        $biayaTransaksiDisplay = $this->config_model->getBiayaPayment($mpo->jenisTransaksi, 'BANK', '50', $kodeBank);

        $data = array(
            'amount' => $mpo->totalKewajiban + $biayaTransaksi,
            'channelId' => $token->channelId,
            'clientId' => $this->config->item('core_post_username'),
            'customerEmail' => $user->email,
            'customerName' => $user->nama,
            'customerPhone' => $user->no_hp,
            'flag' => 'K',
            'jenisTransaksi' => $mpo->jenisTransaksi,
            'keterangan' => 'Pembayaran MPO',
            'kodeProduk' => '50',
            'kodeBank' => $kodeBank,
            'reffSwitching' => $mpo->reffSwitching,
        );

        //MAYBANK
        $billingMayBank = $this->rest_switching_payment_emas_service->createBillingVAMayBank($data, $mpo->norek);

        if ($billingMayBank['responseCode'] != '00') {
            $this->response['message'] = $billingMayBank['responseDesc'];
            return $this->response;
        }

        $dataPrefix = $this->master_model->getPrefixBank('prefixMaybank');
        $billingData = json_decode($billingMayBank['data']);
        $virtualAccount = $billingData->vaNumber;
        $virtualAccountFormat = $dataPrefix . substr($virtualAccount, 0, 11);
        $tglExpired = $billingData->tglExpired;

        $updateData     = array(
            'reffSwitching'    => $mpo->reffSwitching,
            'virtualAccount'   => $virtualAccount,
            'tglExpired'       => $tglExpired,
            'kodeBankPembayar' => $kodeBank,
            'payment'          => $payment,
            'biayaTransaksi'   => $biayaTransaksiDisplay,
        );

        $this->mpo_model->update($updateData);

        $dateExpired = new DateTime($tglExpired);

        $redaksiPayment = $this->config_model->getRedaksiPayment($kodeBank, $virtualAccountFormat, $newTotalKewajiban);

        $templateData = [
            'paymentMethod'  => $payment,
            'nama'           => $mpo->nama,
            'va'             => $virtualAccountFormat,
            'amount'         => $newTotalKewajiban,
            'tglExpired'     => $dateExpired->format('d/m/Y H:i:s'),
            'trxId'          => $idTransaksi,
            'product'        => $product,
            'biayaTransaksi' => $biayaTransaksiDisplay,
            'totalKewajiban' => $mpo->totalKewajiban,
            'tglTransaksi'   => $mpo->updated_at,
            'noHp'           => $mpo->noHp,
            'nominalPulsa'   => filter_var($product, FILTER_SANITIZE_NUMBER_INT),
            'mpoProduct'     => $mpoProduct,
            'mpoGroup'       => $mpoGroup,
            'redaksi'        => $redaksiPayment,
            'idTambahan'     => $mpo->idTambahan,
            'norek'          => $mpo->norek,
            'namaPelanggan'  => $mpo->namaPelanggan,
            'tarifDaya'      => $mpo->segmen . '/' . $mpo->power,
            'periode'        => $mpo->periode,
            'keterangan2'    => $mpo->keterangan2,
            'administrasi'   => $mpo->administrasi,
            'npwp'           => $mpo->npwp,
            'jumlahTagihan'  => $mpo->jumlahTagihan,
            'hargaJual'      => $mpo->hargaJual,
            'jumlahBill'     => $mpo->jumlahBill
        ];

        $dataTagihan = null;
        if ($mpo->dataTagihan !== '') {
            $dataTagihan = json_decode($mpo->dataTagihan);
            $templateData['dataTagihan'] = $dataTagihan;
        }

        $this->notification_service->notificationMpo(
            $templateData,
            $mpo->totalKewajiban + $biayaTransaksi,
            $virtualAccountFormat,
            $tglExpired,
            $token,
            $product
        );

        $dataPayment = array(
            'idTransaksi' => $idTransaksi,
            'virtualAccount' => $virtualAccountFormat,
            'expired'        => $tglExpired,
            'now'            => date('Y-m-d H:i:s'),
            'redaksiPayment' => $redaksiPayment
        );

        //Set response
        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Mohon lakukan pembayaran ke no rekening yang tertera';
        $this->response['data'] = $dataPayment ?? null;

        return $this->response;
    }

    //TelkomPay/FinPay
    public function finpay_payment($payment, $mpo, $user)
    {
        $mpo->biaya_transaksi = $this->config_model->getRealBiayaPayment($mpo->jenisTransaksi, 'BANK', '50', $mpo->kode_bank);
        $mpo->payment = $payment;
        $amount = $mpo->totalKewajiban;

        // max_transaksi 10.000.000, max_transaksi_admin 10.001.500
        $max_transaksi = $this->config_model->whereByVariable('max_transaksi_finpay');
        $max_transaksi_msg = Pegadaian::currencyIdr($max_transaksi->value);
        $max_transaksi_admin = $max_transaksi->value + $mpo->biaya_transaksi;

        if ($amount > $max_transaksi->value && $amount < $max_transaksi_admin || $amount > $max_transaksi_admin) {
            $this->response['message'] = "Transaksi tidak dapat diproses, Maksimal transaksi $max_transaksi_msg";
            return $this->response;
        }

        $data = [
            'amount' => strval($amount + $mpo->biaya_transaksi),
            'channelId' => $mpo->akses_token->channelId,
            'clientId' => $this->config->item('core_post_username'),
            'customerEmail' => $user->email,
            'customerName' => $user->nama,
            'customerPhone' => $user->no_hp,
            'jenisTransaksi' => $mpo->jenisTransaksi,
            'keterangan' => 'Pembayaran MPO',
            'norek' => $mpo->norek,
            'productCode' => '50',
            'trxId' => $mpo->reffSwitching,
        ];

        //FinPay
        $billing_virtual = $this->rest_switching_payment_service->createBillingFinPay($data);

        if ($billing_virtual['responseCode'] != '00') {
            $this->response['message'] = $billing_virtual['responseDesc'] ?? '';
            return $this->response;
        }

        $mpo->data_billing = json_decode($billing_virtual['data']);
        $mpo->tgl_expired = date('Y-m-d H:i:s', time() + (60 * 180));

        $update_data = [
            'reffSwitching'    => $mpo->reffSwitching,
            'virtualAccount'   => $mpo->data_billing->payment_code,
            'tglExpired'       => $mpo->tgl_expired,
            'kodeBankPembayar' => $mpo->kode_bank,
            'payment'          => $payment,
            'biayaTransaksi'   => $mpo->biaya_transaksi,
        ];

        $this->mpo_model->update($update_data);

        $mpo->redaksi_payment = $this->config_model->getRedaksiPayment($mpo->kode_bank, $mpo->data_billing->payment_code, $mpo->newTotalKewajiban);

        $template_data = $this->notification_service->template_data_mpo_notifkasi($mpo);

        $data_tagihan = null;
        if ($mpo->dataTagihan !== '') {
            $data_tagihan = json_decode($mpo->dataTagihan);
            $template_data['dataTagihan'] = $data_tagihan;
        }

        $this->notification_service->notificationMpo(
            $template_data,
            $amount + $mpo->biaya_transaksi,
            $mpo->data_billing->payment_code,
            $mpo->tgl_expired,
            $mpo->akses_token,
            $mpo->product
        );

        $data_payment = [
            'idTransaksi'       => $mpo->idTransaksi,
            'virtualAccount'    => $mpo->data_billing->payment_code,
            'urlPayment'        => $mpo->data_billing->redirect_url,
            'expired'           => $mpo->tgl_expired,
            'now'               => date('Y-m-d H:i:s'),
            'redaksiPayment'    => $mpo->redaksi_payment
        ];

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Mohon lakukan pembayaran ke no rekening yang tertera';
        $this->response['data'] = $data_payment ?? null;

        return $this->response;
    }

    function checkGcashAvailability($gcash_inquiry, $total_transaksi)
    {
        foreach ($gcash_inquiry['data']['va'] as $va) {
            if ($gcash_inquiry['data']['totalSaldo'] >= $total_transaksi) {
                $va_data = $this->gcash_model->getVaDetailsByVa($va->virtualAccount);
                $data = $this->mappingPaymentMethodDataGcash($va_data);

                $data_response = new stdClass();
                $data_response->type = 'GCASH';
                $data_response->paymentMethodName = $data->payment_method_name;
                $data_response->paymentMethodID = $va_data->virtualAccount;
                $data_response->thumbnail = $va_data->thumbnail;
                $data_response->image = $data->image;
                $data_response->amount = $va_data->amount;
                $data_response->status = true;
                $data_response->isGcash = true;

                return $data_response;
            }
        }
    }

    function mappingPaymentMethodDataGcash($va_data)
    {
        $bank_data = $this->bank_model->getBankList($va_data->kodeBank);

        $data = new stdClass();
        $data->payment_method_name = "G-Cash $bank_data->title";
        $data->image = $bank_data->image;

        return $data;
    }

    function mappingPaymentMethodDataVA($payment_data)
    {
        $bank_data = $this->bank_model->getBankList($payment_data->kodeBankPembayar);

        $data = new stdClass();
        $data->payment_method_name = "$bank_data->title Virtual Account";
        $data->image = $bank_data->image;

        return $data;
    }

    function mappingPaymentMethodDataOthers($payment_data)
    {
        $bank_data = $this->bank_model->getBankList($payment_data->kodeBankPembayar);

        $data = new stdClass();
        $data->payment_method_name = "$bank_data->title Clickpay";
        $title = strtoupper($bank_data->title);
        $data->image = "PMCLICK$title";

        return $data;
    }
}
