<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;

class GoldcardAPI_service extends MY_Service
{
    /**
     * @var
     */
    private $client;
    private $apiUrl;
    private $response;

    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->apiUrl = getenv('GOLDCARD_API_BASE_URL') . getenv('GOLDCARD_API_URL');
        $this->response = [
            'code' => '',
            'status' => '',
            'message' => '',
            'data' => []
        ];
        $token = $this->getToken();
        $this->client = new Client([
            'base_uri' => getenv('GOLDCARD_API_HOST'),
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $token,
                'Content-Type' => 'application/json'
            ],
        ]);
    }

    public function postData($url, $data, $form, $method)
    {
        $this->response['data'] = [];
        $url = $this->apiUrl . $url;
        $body = $this->getData($form, $data);
        $response = $this->getResponse($this->client, $url, $method, $body);

        log_message('debug', 'REQUEST GOLDCARD API URI :' . $url . ' => ' . json_encode($data));
        log_message('debug', 'RESPONSE GOLDCARD API URI : ' . $url . ' => ' . json_encode($response));
        return $response;
    }

    private function getData($form, $data)
    {
        switch ($form) {
            case 'json':
                $body = [RequestOptions::JSON => $data];
                break;
            case 'form':
                $body = [RequestOptions::FORM_PARAMS => $data];
                break;
            case 'query':
                $body = [RequestOptions::QUERY => $data];
                break;
            default:
                $body = [];
                break;
        }
        return $body;
    }

    /**
     * Function get token call pds-api auth/login
     *
     * @param $data
     * @return string
     */
    private function getToken()
    {
        $goldcardClient = new Client([
            'base_uri' => getenv('GOLDCARD_API_HOST'),
            'auth' => [getenv('GOLDCARD_API_BASIC_USER'), getenv('GOLDCARD_API_BASIC_PASSWORD')],
            'query' => [
                'username' => getenv('GOLDCARD_API_TOKEN_USER'),
                'password' => getenv('GOLDCARD_API_TOKEN_PASSWORD')
            ]
        ]);

        // get token
        $url = getenv('GOLDCARD_API_BASE_URL') . '/token/get';
        $response = $this->getResponse($goldcardClient, $url);

        if ($response['code'] == '00') {
            return $response['data']['token'];
        }

        // refresh token if token expired
        $url = getenv('GOLDCARD_API_BASE_URL') . '/token/refresh';
        $response = $this->getResponse($goldcardClient, $url);
        if ($response['code'] != '00') {
            return '';
        }
        return $response['data']['token'];
    }


    /**
     * Function getResponse
     *
     * @param $guzzleClient
     * @param $url
     * @param string $method
     * @param string $data
     * @return mixed
     * @throws GuzzleException
     */
    private function getResponse($guzzleClient, $url = '', $method = 'GET', $data = [])
    {
        try {
            $rest_response = $guzzleClient->request($method, $url, $data);
        } catch (ClientException $e) {
            $rest_response = $e->getResponse();
        } catch (ConnectException $e) {
            $rest_response = $e->getResponse();
        } catch (BadResponseException $e) {
            $rest_response = $e->getResponse();
        } catch (GuzzleException $e) {
            $rest_response = $e->getMessage();
        } catch (Exception $e) {
            $rest_response = $e->getMessage();
        }
        $this->response = array_replace($this->response, json_decode($rest_response->getBody(), true) ?? $rest_response);
        return $this->response;
    }
}
