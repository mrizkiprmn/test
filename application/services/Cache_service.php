<?php

class Cache_service extends MY_Service
{
    const PREFIX_KEY = 'PDSAPI';
    const TOKEN_SWITCHING = 'token_switching';
    const TOKEN_REST_CORE = 'token_rest_core';
    const GC_REQUIREMENTS = 'gc_requirements';
    const TOKEN_JWT = 'token_jwt';
    const META_NOTIFICATION = 'meta_notification';
    const NOTIFICATION = 'notification';
    const NOTIFICATION_DETAIL = 'notification_detail';
    const TABEMAS = 'tabemas_listaccount';
    const LIST_MUTATION_TE = 'tabemas_listmutation';
    const TWO_MONTHS = '+2 month';

    public function setThenGet($key, $value, $expire = null)
    {
        $full_key = $this->getFullKey($key);
        $this->redis->setex($full_key, $value, $expire);

        return $this->redis->get($full_key);
    }

    public function getGcPayInquirySession($params)
    {
        $key = md5(json_encode($params));
        $full_key = $this->getFullKey($key);
        $data = $this->redis->get($full_key);

        return json_decode($data, true);
    }

    public function setGcPayInquirySession($params, $data)
    {
        if (empty($params) || empty($data)) {
            return '';
        }

        $key = md5(json_encode($params));
        $full_key = $this->getFullKey($key);
        $this->redis->setex($full_key, json_encode($params));

        return $this->getGcPayInquirySession($key);
    }

    public function unsetGcPayInquirySession($params)
    {
        $key = md5(json_encode($params));
        $full_key = $this->getFullKey($key);
        $this->redis->delete($full_key);

        return true;
    }

    public function getTokenJwt($key)
    {
        $full_key = implode(':', [ self::PREFIX_KEY, self::TOKEN_JWT, $key ]);
        return $this->redis->get($full_key);
    }

    public function setTokenJwt($key, $value)
    {
        $expire = getenv('JWT_EXP') ?: 3600;
        $full_key = implode(':', [ self::PREFIX_KEY, self::TOKEN_JWT, $key ]);

        $this->redis->setex($full_key, $value, $expire);

        return $this->getTokenJwt($key);
    }

    public function setTabemas($key, $value)
    {
        $full_key = implode(':', [ self::TABEMAS, $key ]);
        $expire = $this->getCurrentTtl($full_key, Pegadaian::monthsToSeconds(self::TWO_MONTHS));

        return $this->setThenGet($full_key, json_encode($value), $expire);
    }

    public function setListMutation($key, $value)
    {
        $full_key = implode(':', [ self::LIST_MUTATION_TE, $key ]);
        $expire = $this->getCurrentTtl($full_key, Pegadaian::monthsToSeconds(self::TWO_MONTHS));

        return $this->setThenGet($full_key, json_encode($value), $expire);
    }

    public function setNotificationDetail($key, $value)
    {
        $full_key = implode(':', [ self::NOTIFICATION_DETAIL, $key ]);
        $expire = $this->getCurrentTtl($full_key, Pegadaian::monthsToSeconds(self::TWO_MONTHS));

        return $this->setThenGet($full_key, json_encode($value), $expire);
    }

    public function setMetaNotification($key, $value)
    {
        $full_key = implode(':', [ self::META_NOTIFICATION, $key ]);
        $expire = $this->getCurrentTtl($full_key, Pegadaian::monthsToSeconds(self::TWO_MONTHS));

        return $this->setThenGet($full_key, json_encode($value), $expire);
    }

    public function getNotifications($key, $page, $limit)
    {
        $start = ($page - 1) * $limit;
        $end = $start + ($limit - 1);
        $full_key = implode(':', [$this->cache_service::PREFIX_KEY, $this->cache_service::NOTIFICATION, $key]);
        return $this->redis->lrange($full_key, $start, $end);
    }

    public function setLeftNotification($key, $value)
    {
        $full_key = implode(':', [ self::PREFIX_KEY, self::NOTIFICATION, $key ]);
        return $this->redis->lpush($full_key, $value);
    }

    public function setRightNotification($key, $value)
    {
        $full_key = implode(':', [ self::PREFIX_KEY, self::NOTIFICATION, $key ]);
        $expire = $this->getCurrentTtl($full_key, Pegadaian::monthsToSeconds(self::TWO_MONTHS));
        $this->redis->rpush($full_key, $value);
        return $this->redis->expire($full_key, $expire);
    }

    public function convertArrayObjectToString(array $valueArray)
    {
        $result = [];
        foreach ($valueArray as $value) {
            array_push($result, json_encode($value));
        }

        return $result;
    }

    public function convertArrayStringToObject(array $valueArray)
    {
        $result = [];
        foreach ($valueArray as $value) {
            array_push($result, json_decode($value));
        }

        return $result;
    }

    private function getCurrentTtl($key, $expire)
    {
        $time_expire = $this->redis->ttl($key);

        if ($time_expire > 0) {
            $expire = $time_expire;
        }

        return $expire;
    }

    private function getFullKey($key)
    {
        if (empty($key)) {
            Pegadaian::showError('redis key should be filled');
        }

        return self::PREFIX_KEY . ":{$key}";
    }
}
