<?php

/**
 * @property ProductMasterModel product_master_model
 * @property EmasModel emas_model
 * @property User user_model
 * @property MasterModel master_model
 * @property RestSwitchingPortofolio_service rest_switching_portofolio_service
 * @property RestSwitchingParam_service rest_switching_param_service
 * @property Gcash_service gcash_service
 * @property RestSwitchingSimulasi_service rest_switching_simulasi_service
 * @property RestCoreTabungan_service rest_core_tabungan_service
 * @property Main_service main_service
 * @property RestCoreParam_service rest_core_param_service
 * @property HargaEmasModel harga_emas_model
 */
class Emas_service extends MY_Service
{
    protected $pegadaian_helper;
    protected $response = [
        'code' => 101,
        'status' => 'error',
        'message' => 'Internal Server Error',
        'data' => null
    ];

    public function __construct()
    {
        parent::__construct();

        $this->load->model('ConfigModel', 'config_model');
        $this->load->model('ProductMasterModel', 'product_master_model');
        $this->load->model('EmasModel', 'emas_model');
        $this->load->model('User', 'user_model');
        $this->load->model('MasterModel', 'master_model');
        $this->load->model('ConfigModel', 'config_model');
        $this->load->model('PaymentModel', 'payment_model');
        $this->load->model('HargaEmasModel', 'harga_emas_model');
        $this->load->service('RestSwitchingPortofolio_service', 'rest_switching_portofolio_service');
        $this->load->service('RestSwitchingParam_service', 'rest_switching_param_service');
        $this->load->service('Gcash_service', 'gcash_service');
        $this->load->service('RestSwitchingSimulasi_service', 'rest_switching_simulasi_service');
        $this->load->service('RestCoreTabungan_service', 'rest_core_tabungan_service');
        $this->load->service('Main_service', 'main_service');
        $this->load->service('RestSwitchingPaymentEmas_service', 'rest_switching_payment_emas_service');
        $this->load->service('RestSwitchingPayment_service', 'rest_switching_payment_service');
        $this->load->service('Notification_service', 'notification_service');
        $this->load->service('Response_service', 'response_service');
        $this->load->service('RestCoreParam_service', 'rest_core_param_service');
        $this->load->service('Cache_service', 'cache_service');

        $this->load->helper('Pegadaian');
        $this->pegadaian_helper = new Pegadaian();
    }

    public function listMutations($norek)
    {
        $rest_data = null;

        if (empty($norek)) {
            return $this->response;
        }

        /*------------------------------- Start | Get EOD parameters -------------------------------*/
        $current_time_parameter = "H:i:s";
        $start_time_parameter = $this->master_model->getParameters('eod_start_time');
        $end_time_parameter = $this->master_model->getParameters('eod_end_time');
        $last_update_format_parameter = "d F Y | H:i";

        $borderTime = $this->main_service->getBorderTime($current_time_parameter, $start_time_parameter, $end_time_parameter);

        $eod_start_time = strtotime($borderTime['start_result']);
        $eod_end_time = strtotime($borderTime['end_result']);
        $current_time = strtotime(date($current_time_parameter));

        log_message('debug', 'start time: ' . $borderTime['start_result'] . ' -> '.$eod_start_time);
        log_message('debug', 'end time: ' . $borderTime['start_result'] . ' -> '.$eod_end_time);
        log_message('debug', 'current time: ' . date("Y-m-d " . date($current_time_parameter)) . ' -> '.$current_time);
        /*------------------------------- End | Get EOD parameters -------------------------------*/

        $is_eod = (($current_time >= $eod_start_time) && ($current_time <= $eod_end_time));

        $riwayat_message_outside_eod = "Maaf, sedang dilakukan perbaikan sistem. Silahkan coba beberapa saat lagi.";
        $riwayat_message_inside_eod = "Update riwayat dapat dilihat pukul " . $end_time_parameter . " - " . $start_time_parameter . ".";

        $riwayat_message = ($is_eod) ? $riwayat_message_inside_eod : $riwayat_message_outside_eod;

        log_message('debug', 'riwayat detail: ' . $riwayat_message);

        $cache_key = implode(':', [ $this->cache_service::PREFIX_KEY, $this->cache_service::LIST_MUTATION_TE, $norek ]);
        $cache_list = json_decode($this->redis->get($cache_key));

        $this->response['data']    = [
            'list' => $cache_list,
            'message' => $riwayat_message
        ];
        $this->response['code']    = 200;
        $this->response['status']  = 'success';
        $this->response['message'] = 'Rekening berhasil ditemukan';

        $rest_data = $this->rest_switching_portofolio_service->transaksiTabEmas(['norek' =>$norek]);

        if ($rest_data['responseCode'] == '14') {
            $this->response['data']['message'] = '';
            return $this->response;
        }

        if ($rest_data['responseCode'] == '00') {
            $data = json_decode($rest_data['data'], true);
            $data = $this->formatListMutations($data);
            $this->response['data'] = [
                'list' => $data,
                'message' => ''
            ];
            $this->cache_service->setListMutation($norek, $data);
        }

        return $this->response;
    }

    public function listMutation($getData)
    {
        $response = $this->response_service->getResponse('');

        $start_time_parameter = $this->master_model->getParameters('eod_start_time');
        $end_time_parameter = $this->master_model->getParameters('eod_end_time');
        $eod_start_time = strtotime(date("Y-m-d " . $start_time_parameter));
        $eod_end_time = strtotime(date("Y-m-d " . $end_time_parameter));
        $current_time = strtotime(date("Y-m-d H:i:s"));

        $is_eod = (($current_time >= $eod_start_time) && ($current_time <= $eod_end_time));

        $riwayat_message_outside_eod = "Maaf, sedang dilakukan perbaikan sistem. Silahkan coba beberapa saat lagi.";
        $riwayat_message_inside_eod = "Update riwayat dapat dilihat pukul " . $start_time_parameter . " - " . $end_time_parameter . ".";

        $riwayat_message = ($is_eod) ? $riwayat_message_inside_eod : $riwayat_message_outside_eod;

        $setData = [
            "norek" => $getData['no_rekening'],
            "limit" => $getData['limit'],
            "type" => "1",
            "page" => $getData['page']
        ];

        $date_diff_limit = $this->master_model->getParameters('max_date_difference_portofolio');
        if (!empty($getData['startDate']) && !empty($getData['endDate'])) {
            $setData['type'] = "2";
            $setData['startDate'] = $getData['startDate'];
            $setData['endDate'] = $getData['endDate'];

            $date_diff = date_diff(date_create($getData['startDate']), date_create($getData['endDate']))->days;
        };

        if (!empty($date_diff) && $date_diff >= $date_diff_limit) {
            $response['message'] = "rentang tanggal tidak boleh lebih dari $date_diff_limit hari";
            $response['data'] = null;
            return $response;
        }

        $transaksi_emas = $this->rest_switching_portofolio_service->pdsTrxTabEmas($setData);

        $response = $this->response_service->getResponse('general.succeeded');
        $response['message'] = 'Rekening berhasil ditemukan';
        $response['data']    = [
            'list' => null,
            'message' => $riwayat_message
        ];

        if (empty($transaksi_emas)) {
            return $response;
        }

        if ($transaksi_emas['responseCode'] != "00") {
            $response['message'] = $transaksi_emas['responseDesc'];
            return $response;
        }

        if ($transaksi_emas['responseCode'] == '00') {
            $data = json_decode($transaksi_emas['data'], true);
            $data = $this->formatListMutations($data);

            $response['message'] = 'Data Mutasi Rekening ditemukan';
            $response['data'] = [
                'list' => $data,
                'message' => ''
            ];
        }

        return $response;
    }

    public function selectTE($list_tabungan, $norek)
    {
        $tab_emas = [];
        foreach ($list_tabungan as $list) {
            if ($list->norek == $norek) {
                $tab_emas = $list;
            }
        }
        return $tab_emas ?? false;
    }

    public function listAccountNumber($cif)
    {
        if (empty($cif)) {
            return [];
        }

        $class         = 'rest_switching_portofolio_service';
        $method        = 'pdsTabEmas';
        $payload       = ['cif' => $cif];
        $use_rest_core = (bool)$this->config->item('rest_core_is_active');

        if ($use_rest_core) {
            $class   = 'rest_core_tabungan_service';
            $method  = 'byCif';
        }

        $rest_tab_emas = $this->{$class}->{$method}($payload);
        $rc = $rest_tab_emas['responseCode'] ?? null;
        $user = $this->user_model->getUserByLinkCif($cif);

        if ($rc != '00') {
            return $this->getCacheTabemas($user->user_AIID);
        }

        $tab_emas = $use_rest_core ? $rest_tab_emas['responseData'] : $rest_tab_emas['data'];
        $tab_emas = is_array($tab_emas) ? $tab_emas : json_decode($tab_emas, true);

        $tab_emas['primaryRekening'] = $this->getPrimaryRekening($cif, $tab_emas['listTabungan'] ?? []);

        // update data is_open_te
        $this->user_model->updateIsOpenTe($cif, $tab_emas);
        $this->cache_service->setTabemas($user->user_AIID, $tab_emas);

        return $tab_emas;
    }

    private function getCacheTabemas($cif)
    {
        $cache_key = implode(':', [ $this->cache_service::PREFIX_KEY, $this->cache_service::TABEMAS, $cif ]);
        $result = $this->redis->get($cache_key);

        if (empty($result)){
            return json_decode('{}');
        }

        return json_decode($result);
    }

    protected function formatListMutations($mutations)
    {
        $array_map = array_map('strtotime', array_column($mutations, 'tglTransaksi'));

        array_multisort(
            $array_map,
            SORT_DESC,
            $mutations
        );

        return $mutations;
    }

    public function simulasiTabemas($data)
    {
        //get data ProductCode dari ProductModel
        $productCode = $this->ProductMasterModel->get_productName('TABUNGAN EMAS');
        $data['productCode'] = $productCode->productCode;
        $data['jenisTransaksi'] = "BB";

        if ($data['amount'] == "0") {
            $this->response['message'] = 'Inputan tidak boleh nol';

            return $this->response;
        }

        $data['flag'] = "k";

        //get data dari RestSwitchingService
        $req = $this->rest_switching_simulasi_service->simulasiTabemas($data);

        if ($req['responseCode'] != "00") {
            $this->response['message'] = $req['responseDesc'];

            return $this->response;
        }

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Simulasi Jual Emas Berhasil';
        $this->response['data'] = json_decode($req['data']);

        return $this->response;
    }

    public function validatePaymentGcash($token, $idTransaksi)
    {
        $this->response['status']  = 'error';
        $this->response['message'] = 'Data transaksi tidak ditemukan';
        $this->response['data']    = '';
        $this->response['code'] = '102';

        //Mendapatkan payment berdasarkan id transaksi
        $checkPayment = $this->payment_model->getPaymentByTrxId($idTransaksi);

        if (empty($checkPayment)) {
            return $this->response;
        }

        //Get gcash data
        $response = $this->gcash_service->getGcash($token);

        log_message('debug', 'Get data for nominal validation:' . json_encode($response));

        if ($response['code'] != 200) {
            log_message('debug', __FUNCTION__ . ' Validasi Pembayaran Emas error ' . $response['message']);

            $this->response['message'] = $response['message'];

            return $this->response;
        }

        if ($response['data']['totalSaldo'] < $checkPayment->total_kewajiban) {
            log_message('debug', __FUNCTION__ . ' Validasi Pembayaran Emas error ' . 'Saldo tidak mencukupi');

            $this->response['message'] = 'Saldo tidak mencukupi';
            $this->response['data']    = ['totalSaldo' => $response['data']['totalSaldo']];

            return $this->response;
        }

        $this->response['status']  = 'success';
        $this->response['message'] = 'Saldo mencukupi';

        log_message('debug', __FUNCTION__ . ' Validasi Pembayaran Emas ' . 'End');
        return $this->response;
    }

    public function paymentOpenTabemas($payment, $checkPayment, $discountAmount, $user, $token, $kodeBank)
    {
        $trxId =$checkPayment->reffSwitching;
        $amount = $checkPayment->amount;
        $jenisTransaksi = 'OP';
        $kodeProduct = '62';
        $keterangan = 'PDSOP ' . $trxId;

        $biayaTransaksi = $this->config_model->getRealBiayaPayment($jenisTransaksi, 'BANK', $kodeProduct, $kodeBank);
        $biayaTransaksiDisplay = $this->config_model->getBiayaPayment($jenisTransaksi, 'BANK', $kodeProduct, $kodeBank);

        $data = array(
            'amount' => $amount + $biayaTransaksi - $discountAmount,
            'channelId' => $token->channelId,
            'clientId' => $this->config->item('core_post_username'),
            'customerEmail' => $user->email,
            'customerName' => $user->nama,
            'customerPhone' => $user->no_hp,
            'flag' => 'K',
            'jenisTransaksi' => $jenisTransaksi,
            'keterangan' => $keterangan,
            'kodeProduk' => $kodeProduct,
            'kodeBank' => $kodeBank,
            'reffSwitching' => $trxId,
        );

        $billingMayBank = $this->rest_switching_payment_emas_service->createBillingVAMayBank($data, $norek='');

        if ($billingMayBank['responseCode'] != '00') {
            $this->response['message'] = $billingMayBank['responseDesc'];
            return $this->response;
        }

        $dataPrefix = $this->master_model->getPrefixBank('prefixMaybank');
        $billingData = json_decode($billingMayBank['data']);
        $virtualAccount = $billingData->vaNumber;
        $virtualAccountFormat = $dataPrefix . substr($virtualAccount, 0, 11);
        $tglExpired = $billingData->tglExpired;

        $this->user_model->saveTabunganEmas(
            $token->id,
            $user->domisili,
            $user->nama,
            $user->jenis_identitas,
            $user->no_ktp,
            $user->tanggal_expired_identitas,
            $user->tempat_lahir,
            $user->tgl_lahir,
            $user->no_hp,
            $user->jenis_kelamin,
            $user->status_kawin,
            $user->id_kelurahan,
            $user->alamat,
            $user->nama_ibu,
            $user->kewarganegaraan,
            $user->kode_cabang,
            'K',
            $user->foto_url,
            $checkPayment->amount,
            $trxId
        );

        $paymentData = array(
            'tipe' => $payment,
            'payment' => $payment,
            'kodeBankPembayar' => $kodeBank,
            'virtual_account' => $virtualAccount,
        );

        //Simpan payment pada tahap ini, virtual account dan tgl expired masih kosong
        $this->payment_model->update($trxId, $paymentData);

        $this->notification_service->notificationOpenTabemasPayment(
            $payment,
            $amount + $biayaTransaksiDisplay,
            $virtualAccountFormat,
            $tglExpired,
            $trxId,
            $token
        );

        $dataPayment = array(
            'idTransaksi' => $trxId,
            'virtualAccount' => $virtualAccountFormat,
            'expired' => $tglExpired,
            'now' => date('Y-m-d H:i:s'),
        );

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Mohon lakukan pembayaran ke no rekening yang tertera';
        $this->response['data'] = $dataPayment ?? null;

        return $this->response;
    }

    public function paymentTopup($payment, $checkPayment, $customerData, $idTransaksi, $discountAmount, $user, $token, $kodeBank)
    {
        $biayaTransaksi = $this->config_model->getRealBiayaPayment($checkPayment->jenisTransaksi, 'BANK', '62', $kodeBank);
        $biayaTransaksiDisplay = $this->config_model->getBiayaPayment($checkPayment->jenisTransaksi, 'BANK', '62', $kodeBank);

        $data = array(
            'amount' => $checkPayment->total_kewajiban + $biayaTransaksi - $discountAmount,
            'channelId' => $token->channelId,
            'clientId' => $this->config->item('core_post_username'),
            'customerEmail' => $user->email,
            'customerName' => $user->nama,
            'customerPhone' => $user->no_hp,
            'flag' => 'K',
            'jenisTransaksi' => $checkPayment->jenisTransaksi,
            'keterangan' => 'PDSSL ' . $idTransaksi . ' ' . $checkPayment->no_rekening,
            'kodeProduk' => '62',
            'kodeBank' => $kodeBank,
            'reffSwitching' => $idTransaksi,
        );

        $billingMayBank = $this->rest_switching_payment_emas_service->createBillingVAMayBank($data, $checkPayment->no_rekening);

        if ($billingMayBank['responseCode'] != '00') {
            $this->response['message'] = $billingMayBank['responseDesc'];
            return $this->response;
        }

        $dataPrefix = $this->master_model->getPrefixBank('prefixMaybank');
        $billingData = json_decode($billingMayBank['data']);
        $virtualAccount = $billingData->vaNumber;
        $virtualAccountFormat = $dataPrefix . substr($virtualAccount, 0, 11);
        $tglExpired = $billingData->tglExpired;

        $updatePaymentData = array(
            'tipe' => $payment,
            'virtual_account' => $virtualAccount,
            'tanggal_expired' => $tglExpired,
            'payment' => $payment,
            'biayaTransaksi' => $biayaTransaksiDisplay,
            'kodeBankPembayar' => $kodeBank
        );

        //Simpan payment pada tahap ini, virtual account dan tgl expired masih kosong
        $this->payment_model->update($idTransaksi, $updatePaymentData);

        $this->notification_service->notificationTopupPayment(
            $idTransaksi,
            $customerData->cif,
            $checkPayment->total_kewajiban + $biayaTransaksi - $discountAmount,
            $virtualAccountFormat,
            $checkPayment,
            $tglExpired,
            $token
        );

        //Mendapatkan detail tabungan emas user
        $saldo = $this->rest_switching_payment_emas_service->getSaldo($checkPayment->no_rekening);

        $dataPayment = array(
            'idTransaksi' => $idTransaksi,
            'virtualAccount' => $virtualAccountFormat,
            'expired' => $tglExpired,
            'now' => date('Y-m-d H:i:s'),
            'saldo' => $saldo['saldo'],
            'saldoAkhir' => $saldo['saldoAkhir'],
            'saldoBlokir' => $saldo['saldoBlokir'],
            'saldoEfektif' => $saldo['saldoEfektif'],
        );

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Mohon lakukan pembayaran ke no rekening yang tertera';
        $this->response['data'] = $dataPayment ?? null;

        return $this->response;
    }

    public function paymentCetakEmas($payment, $checkPayment, $idTransaksi, $discountAmount, $user, $token, $kodeBank)
    {
        $biayaTransaksi = $this->config_model->getRealBiayaPayment($checkPayment->jenisTransaksi, 'BANK', '62', $kodeBank);
        $biayaTransaksiDisplay = $this->config_model->getBiayaPayment($checkPayment->jenisTransaksi, 'BANK', '62', $kodeBank);

        $data = array(
            'amount' => $checkPayment->totalKewajiban + $biayaTransaksiDisplay - $discountAmount,
            'channelId' => $token->channelId,
            'clientId' => $this->config->item('core_post_username'),
            'customerEmail' => $user->email,
            'customerName' => $user->nama,
            'customerPhone' => $user->no_hp,
            'flag' => 'K',
            'jenisTransaksi' => $checkPayment->jenisTransaksi,
            'keterangan' => 'PDSOD ' . $idTransaksi . ' ' . $checkPayment->no_rekening,
            'kodeProduk' => '62',
            'kodeBank' => $kodeBank,
            'reffSwitching' => $idTransaksi,
        );

        $createBillingMayBank = $this->rest_switching_payment_emas_service->createBillingVAMayBank($data, $checkPayment->no_rekening);

        if ($createBillingMayBank['responseCode'] != '00') {
            $this->response['message'] = $createBillingMayBank['responseDesc'];
            return $this->response;
        }

        $dataPrefix = $this->master_model->getPrefixBank('prefixMaybank');
        $billingData = json_decode($createBillingMayBank['data']);
        $virtualAccount = $billingData->vaNumber;
        $virtualAccountFormat = $dataPrefix . substr($virtualAccount, 0, 11);
        $tglExpired = $billingData->tglExpired;

        $updatePaymentData = array(
            'tipe' => $payment,
            'virtual_account' => $virtualAccount,
            'tanggal_expired' => $tglExpired,
            'payment' => $payment,
            'biayaTransaksi' => $biayaTransaksiDisplay,
            'kodeBankPembayar' => $kodeBank,
        );

        //Update payment
        $this->payment_model->update($idTransaksi, $updatePaymentData);

        $this->notification_service->notificationCetakPayment(
            $idTransaksi,
            $data['amount'],
            $virtualAccountFormat,
            $checkPayment,
            $tglExpired,
            $token
        );

        //get saldo terakhir
        $saldo = $this->rest_switching_payment_emas_service->getSaldo($checkPayment->no_rekening);

        $dataPayment = array(
            'idTransaksi' => $idTransaksi,
            'virtualAccount' => $virtualAccountFormat,
            'expired' => $tglExpired,
            'now' => date('Y-m-d H:i:s'),
            'saldo' => (string) $saldo['saldo'],
            'saldoAkhir' => $saldo['saldoAkhir'],
            'saldoBlokir' => $saldo['saldoBlokir'],
            'saldoEfektif' => $saldo['saldoEfektif']
        );

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Mohon lakukan pembayaran ke no rekening yang tertera';
        $this->response['data'] = $dataPayment ?? null;

        return $this->response;
    }

    protected function getPrimaryRekening($cif, $listTabungan)
    {
        if (empty($listTabungan)) {
            return null;
        }

        $user = $this->user_model->getUserByLinkCif($cif);
        if (empty($user->norek_utama)) {
            return $listTabungan[0];
        }

        foreach ($listTabungan as $tabungan) {
            log_message('info', '$tabungan->norek: ' . gettype($tabungan['norek']) . ' >< $user->norek_utama: ' . gettype($user->norek_utama));
            if ($tabungan['norek'] === $user->norek_utama) {
                return $tabungan;
            }
        }

        return null;
    }

    public function simulasi_GteUp($data)
    {
        $dataFlag = [
            "flag" => "K"
        ];

        //Mendapatkan harga emas yang berlaku saat ini
        $stl = $this->rest_switching_param_service->getHargaSTL($dataFlag);

        if ($stl['responseCode'] != '00' || !isset($stl['data'])) {
            $this->response['message'] = 'Terjadi kesalahan. Mohon coba beberapa saat lagi';
            return $this->response;
        }

        $stlData = json_decode($stl['data']);
        $hargaEmas = $stlData->hargaEmas;
        $rupiahAsli = $data['rupiah'];

        //Mendapatkan rasio taksiran
        $rasio = $this->emas_model->getRasioTaksiran($data['waktu']);

        if ($data['gram'] != 0) {
            $data['rupiah'] = $data['gram'] * $rasio * $hargaEmas;
        }

        if ($data['rupiah'] != 0) {
            $data['gram'] = $this->round_up(($data['rupiah'] / ( $rasio * $hargaEmas )), 4, PHP_ROUND_HALF_UP) ;
        }

        //Bulatkan uang pinjaman
        $golongan = $this->emas_model->getGolonganKCA($data['rupiah']);

        if (!isset($golongan->pembulatan)) {
            $this->response['message'] = 'Jumlah pinjaman harus lebih tinggi';
            return $this->response;
        }

        $pembulatan = $golongan->pembulatan;
        $rupiah = (int) ($pembulatan * floor($data['rupiah'] / $pembulatan));

        $data = array(
            'gram' => $data['gram'],
            'rupiah' => $rupiah,
            'rupiahAsli' => (int) $rupiahAsli,
            'golongan' => $golongan->golongan
        );

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Simulasi Berhasil';
        $this->response['data'] = $data ?? null;

        return $this->response;
    }

    protected function round_up($value, $places = 0)
    {
        if ($places < 0) {
            $places = 0;
        }
        $mult = pow(10, $places);
        return ceil($value * $mult) / $mult;
    }


    protected function getReducedSaldoTabunganEmas($list, $key)
    {
        return array_reduce($list, function ($acc, $item) use ($key) {
            $item = (array) $item;
            $acc += $item[$key];
            return $acc;
        }, 0);
    }

    public function finpay_payment_open_tabemas($payment, $check_payment)
    {
        $trxId = $check_payment->reffSwitching;
        $amount = $check_payment->amount;
        $jenis_transaksi = 'OP';
        $kode_product = '62';
        $keterangan = 'PDSOP';

        $biaya_transaksi = $this->config_model->getRealBiayaPayment($jenis_transaksi, 'BANK', $kode_product, $check_payment->kode_bank);

        // max_transaksi 10.000.000, max_transaksi_admin 10.001.500
        $max_transaksi = $this->config_model->whereByVariable('max_transaksi_finpay');
        $max_transaksi_msg = Pegadaian::currencyIdr($max_transaksi->value);
        $max_transaksi_admin = $max_transaksi->value + $biaya_transaksi;

        if ($amount > $max_transaksi->value && $amount < $max_transaksi_admin || $amount > $max_transaksi_admin) {
            $this->response['message'] = "Transaksi tidak dapat diproses, Maksimal transaksi $max_transaksi_msg";
            return $this->response;
        }

        $data = [
            'amount' => strval($amount + $biaya_transaksi - $check_payment->discountAmount),
            'channelId' => $check_payment->data_token->channelId,
            'clientId' => $this->config->item('core_post_username'),
            'customerEmail' => $check_payment->data_user->email,
            'customerName' => $check_payment->data_user->nama,
            'customerPhone' => $check_payment->data_user->no_hp,
            'flag' => 'K',
            'jenisTransaksi' => $jenis_transaksi,
            'keterangan' => $keterangan . ' ' . $trxId,
            'norek' => '',
            'productCode' => $kode_product,
            'trxId' => $trxId,
        ];

        $billing_virtual = $this->rest_switching_payment_service->createBillingFinPay($data);

        if ($billing_virtual['responseCode'] != '00') {
            $this->response['message'] = $billing_virtual['responseDesc'];
            return $this->response;
        }

        $billing_data = json_decode($billing_virtual['data']);
        $virtual_account = $billing_data->payment_code;
        $tgl_expired = date('Y-m-d H:i:s', time() + (60 * 180));

        $this->user_model->saveTabunganEmas(
            $check_payment->data_token->id,
            $check_payment->data_user->domisili,
            $check_payment->data_user->nama,
            $check_payment->data_user->jenis_identitas,
            $check_payment->data_user->no_ktp,
            $check_payment->data_user->tanggal_expired_identitas,
            $check_payment->data_user->tempat_lahir,
            $check_payment->data_user->tgl_lahir,
            $check_payment->data_user->no_hp,
            $check_payment->data_user->jenis_kelamin,
            $check_payment->data_user->status_kawin,
            $check_payment->data_user->id_kelurahan,
            $check_payment->data_user->alamat,
            $check_payment->data_user->nama_ibu,
            $check_payment->data_user->kewarganegaraan,
            $check_payment->data_user->kode_cabang,
            'K',
            $check_payment->data_user->foto_url,
            $amount,
            $trxId
        );

        $payment_data = [
            'tipe' => $payment,
            'payment' => $payment,
            'kodeBankPembayar' => $check_payment->kode_bank,
            'virtual_account' => $virtual_account,
        ];

        $this->payment_model->update($trxId, $payment_data);

        $this->notification_service->notificationOpenTabemasPayment(
            $payment,
            $amount + $biaya_transaksi,
            $virtual_account,
            $tgl_expired,
            $trxId,
            $check_payment->data_token
        );

        $data_payment = [
            'idTransaksi' => $trxId,
            'virtualAccount' => $virtual_account,
            'expired' => $tgl_expired,
            'now' => date('Y-m-d H:i:s'),
        ];

        $response = $this->response_service->getResponse('general.succeeded');
        $response['message'] = 'Mohon lakukan pembayaran ke no rekening yang tertera';
        $response['data'] = $data_payment ?? null;

        return $response;
    }

    //TelkomPay/Finpay
    public function payment_finpay_topup($payment, $check_payment)
    {
        $biaya_transaksi = $this->config_model->getRealBiayaPayment($check_payment->jenisTransaksi, 'BANK', '62', $check_payment->kode_bank);
        $keterangan = 'PDSSL';
        $amount = $check_payment->total_kewajiban;

        // max_transaksi 10.000.000, max_transaksi_admin 10.001.500
        $max_transaksi = $this->config_model->whereByVariable('max_transaksi_finpay');
        $max_transaksi_msg = Pegadaian::currencyIdr($max_transaksi->value);
        $max_transaksi_admin = $max_transaksi->value + $biaya_transaksi;

        if ($amount > $max_transaksi->value && $amount < $max_transaksi_admin || $amount > $max_transaksi_admin) {
            $this->response['message'] = "Transkasi tidak dapat diproses, Maksimal transaksi $max_transaksi_msg";
            return $this->response;
        }

        $data = [
            'amount' => strval($amount + $biaya_transaksi - $check_payment->discountAmount),
            'channelId' => $check_payment->data_token->channelId,
            'clientId' => $this->config->item('core_post_username'),
            'customerEmail' => $check_payment->data_user->email,
            'customerName' => $check_payment->data_user->nama,
            'customerPhone' => $check_payment->data_user->no_hp,
            'flag' => 'K',
            'jenisTransaksi' => $check_payment->jenisTransaksi,
            'keterangan' => $keterangan . ' ' . $check_payment->idTransaksi . ' ' . $check_payment->no_rekening,
            'norek' => $check_payment->norek,
            'productCode' => '62',
            'trxId' => $check_payment->idTransaksi,
        ];

        $billing_virtual = $this->rest_switching_payment_service->createBillingFinPay($data);

        if ($billing_virtual['responseCode'] != '00') {
            $this->response['message'] = $billing_virtual['responseDesc'];
            return $this->response;
        }

        $billing_data = json_decode($billing_virtual['data']);
        $virtual_account = $billing_data->payment_code;
        $tgl_expired = date('Y-m-d H:i:s', time() + (60 * 180));

        $update_payment_data = [
            'tipe' => $payment,
            'payment' => $payment,
            'virtual_account' => $virtual_account,
            'kodeBankPembayar' => $check_payment->kode_bank,
            'tanggal_expired' => $tgl_expired,
            'biayaTransaksi' => $biaya_transaksi,
        ];

        $this->payment_model->update($check_payment->idTransaksi, $update_payment_data);

        $this->notification_service->notificationTopupPayment(
            $check_payment->idTransaksi,
            $check_payment->data_customer->cif,
            $amount + $biaya_transaksi - $check_payment->discountAmount,
            $virtual_account,
            $check_payment,
            $tgl_expired,
            $check_payment->data_token
        );

        //Mendapatkan detail tabungan emas user
        $saldo = $this->rest_switching_payment_emas_service->getSaldo($check_payment->no_rekening);

        $data_payment = [
            'idTransaksi' => $check_payment->idTransaksi,
            'virtualAccount' => $virtual_account,
            'expired' => $tgl_expired,
            'now' => date('Y-m-d H:i:s'),
            'saldo' => strval($saldo['saldo']),
            'saldoAkhir' => $saldo['saldoAkhir'],
            'saldoBlokir' => $saldo['saldoBlokir'],
            'saldoEfektif' => $saldo['saldoEfektif'],
        ];

        $response = $this->response_service->getResponse('general.succeeded');
        $response['message'] = 'Mohon lakukan pembayaran ke no rekening yang tertera';
        $response['data'] = $data_payment ?? null;

        return $response;
    }

    public function payment_finpay_cetak_emas($payment, $check_payment)
    {
        $biaya_transaksi = $this->config_model->getBiayaPayment($check_payment->jenisTransaksi, 'BANK', '62', $check_payment->kode_bank);
        $keterangan = 'PDSOD';
        $amount = $check_payment->totalKewajiban;

        // max_transaksi 10.000.000, max_transaksi_admin 10.001.500
        $max_transaksi = $this->config_model->whereByVariable('max_transaksi_finpay');
        $max_transaksi_msg = Pegadaian::currencyIdr($max_transaksi->value);
        $max_transaksi_admin = $max_transaksi->value + $biaya_transaksi;

        if ($amount > $max_transaksi->value && $amount < $max_transaksi_admin || $amount > $max_transaksi_admin) {
            $this->response['message'] = "Transkasi tidak dapat diproses, Maksimal transaksi $max_transaksi_msg";
            return $this->response;
        }

        $data = [
            'amount' => strval($amount + $biaya_transaksi - $check_payment->discountAmount),
            'channelId' => $check_payment->data_token->channelId,
            'clientId' => $this->config->item('core_post_username'),
            'customerEmail' => $check_payment->data_user->email,
            'customerName' => $check_payment->data_user->nama,
            'customerPhone' => $check_payment->data_user->no_hp,
            'flag' => 'K',
            'jenisTransaksi' => $check_payment->jenisTransaksi,
            'keterangan' => $keterangan . ' ' . $check_payment->idTransaksi . ' ' . $check_payment->no_rekening,
            'norek' => $check_payment->norek,
            'productCode' => '62',
            'trxId' => $check_payment->idTransaksi,
        ];

        $billing_virtual = $this->rest_switching_payment_service->createBillingFinPay($data);

        if ($billing_virtual['responseCode'] != '00') {
            $this->response['message'] = $billing_virtual['responseDesc'];
            return $this->response;
        }

        $billing_data = json_decode($billing_virtual['data']);
        $virtual_account = $billing_data->payment_code;
        $tgl_expired = date('Y-m-d H:i:s', time() + (60 * 180));

        $update_payment_data = [
            'tipe' => $payment,
            'virtual_account' => $virtual_account,
            'tanggal_expired' => $tgl_expired,
            'payment' => $payment,
            'biayaTransaksi' => $biaya_transaksi,
            'kodeBankPembayar' => $check_payment->kode_bank,
        ];

        $this->payment_model->update($check_payment->idTransaksi, $update_payment_data);

        $this->notification_service->notificationCetakPayment(
            $check_payment->idTransaksi,
            $data['amount'],
            $virtual_account,
            $check_payment,
            $tgl_expired,
            $check_payment->data_token
        );

        $saldo = $this->rest_switching_payment_emas_service->getSaldo($check_payment->no_rekening);

        $data_payment = [
            'idTransaksi' => $check_payment->idTransaksi,
            'virtualAccount' => $virtual_account,
            'expired' => $tgl_expired,
            'now' => date('Y-m-d H:i:s'),
            'saldo' => (string) $saldo['saldo'],
            'saldoAkhir' => $saldo['saldoAkhir'],
            'saldoBlokir' => $saldo['saldoBlokir'],
            'saldoEfektif' => $saldo['saldoEfektif']
        ];

        $response = $this->response_service->getResponse('general.succeeded');
        $response['message'] = 'Mohon lakukan pembayaran ke no rekening yang tertera';
        $response['data'] = $data_payment ?? null;

        return $response;
    }

    public function getHargaEmas()
    {
        $class         = 'rest_switching_param_service';
        $method        = 'getHargaEmas';
        $payload       = ['flag' => 'K'];
        $use_rest_core = (bool)$this->config->item('rest_core_is_active');

        if ($use_rest_core) {
            $class   = 'rest_core_param_service';
            $method  = 'hargaEmas';
            $payload = [];
        }

        $rest_gold_price = $this->{$class}->{$method}($payload);

        if (empty($rest_gold_price['responseCode']) && $rest_gold_price['responseCode'] != 00) {
            return null;
        }

        $gold_price_data = $use_rest_core ? $rest_gold_price['responseData'] : $rest_gold_price['data'];
        $gold_price_data = is_array($gold_price_data) ? $gold_price_data : json_decode($gold_price_data, true);

        if (empty($gold_price_data)) {
            return null;
        }

        $gold_price      = $this->harga_emas_model->find([], ['column' => 'harga_emas_AIID', 'direction' => 'desc']);
        $effective_date  = new DateTime($gold_price_data['tglBerlaku']);
        $effective_date  = $effective_date->format('Y-m-d');

        $data = [
            'harga_jual'   => $gold_price_data['hargaJual'],
            'harga_beli'   => $gold_price_data['hargaBeli'],
            'waktu_update' => $gold_price_data['tglBerlaku']
        ];

        if ($effective_date == $gold_price->waktu_update) {
            $harga_emas_id = $gold_price->harga_emas_AIID;
        }

        $this->harga_emas_model->updateByIdOrInsert($harga_emas_id ?? null, $data);

        return $gold_price_data;
    }

    public function getHargaStl()
    {
        $stl = $this->rest_switching_param_service->getHargaSTL([ "flag" => "K" ]);

        if ($stl['responseCode'] == '96') {
            $this->response['message'] = $stl['responseDesc'];
            $this->response['code'] = $stl['responseCode'];
            return $this->response;
        }

        if ($stl['responseCode'] != '00' || !isset($stl['data'])) {
            $this->response['message'] = 'Terjadi kesalahan. Mohon coba beberapa saat lagi';
            return $this->response;
        }

        $stlData = json_decode($stl['data']);
        $hargaStl = $stlData->hargaEmas;
        $this->response = $this->response_service->getResponse('general.succeeded');
        $this->response['data'] = [ 'harga_stl' => $hargaStl ];

        return $this->response;
    }
}
