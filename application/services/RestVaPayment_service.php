<?php

/**
 * @property RestVa_service rest_va_service
 */
class RestVaPayment_service extends MY_Service
{
    /**
     * RestSwitchingPayment_service constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->service('RestVa_service', 'rest_va_service');
    }

    // Endpoint POST /va/bni/createbilling
    // payload data
    // amount string
    // customerEmail string
    // customerName string
    // customerPhone string
    // flag string
    // jenisTransaksi string
    // keterangan string
    // productCode string
    // norek string
    // trxId string
    // idPromosi string
    // discountAmount string
    public function createBillingVaBNI(array $data)
    {
        $url = '/va/bni/createbilling';
        $data['flag'] = 'K';

        return $this->rest_va_service->postData($url, $data);
    }

    // Endpoint POST /va/bni/createbilling
    // payload data
    // amount
    // bookingCode
    // cardNumber
    // jenisTransaksi
    // keterangan
    // noHp
    // norek
    // flag
    // productCode
    // tokenResponse
    // trxId
    // idPromosi
    // discountAmount
    public function createBillingVaMandiri(array $data)
    {
        $url = '/clickpay/mandiri/payment';
        $data['flag'] = 'K';

        return $this->rest_va_service->postData($url, $data);
    }

    // Endpoint POST /va/bri/createbilling
    // payload data
    // amount
    // jenisTransaksi
    // norek
    // productCode
    // customerEmail
    // customerName
    // customerPhone
    // keterangan
    // trxId
    // idPromosi
    // discountAmount
    public function createBillingVaBRI(array $data)
    {
        $url = '/va/bri/createbilling';
        $data['flag'] = 'K';

        return $this->rest_va_service->postData($url, $data);
    }

    // Endpoint POST /va/permata/createbilling
    // payload data
    // amount
    // jenisTransaksi
    // norek
    // productCode
    // customerEmail
    // customerName
    // customerPhone
    // keterangan
    // trxId
    // idPromosi
    // discountAmount
    public function createBillingVaPermata(array $data)
    {
        $url = '/va/permata/createbilling';
        $data['flag'] = 'K';

        return $this->rest_va_service->postData($url, $data);
    }
}
