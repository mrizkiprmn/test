<?php

/**
 * @property User user_model
 * @property Pegadaian Pegadaian
 * @property RestSwitchingMtonline_service rest_switching_mtonline_service
 */
class Mtonline_service extends MY_Service
{
    // Default Response
    protected $response = [
        'status' => 'error',
        'message' => 'Terjadi kesalahan, mohon coba beberapa saat lagi.',
        'data' => null,
        'code' => 101
    ];

    public function __construct()
    {
        parent::__construct();
        // init load model
        $this->load->model('User', 'user_model');
        // init load helper
        $this->load->helper('Pegadaian');
        //rest service
        $this->load->service('RestSwitchingMtonline_service', 'rest_switching_mtonline_service');
        $this->load->service('RestSwitchingPortofolio_service', 'rest_switching_portofolio_service');
    }

    public function limitPinjaman($user_id)
    {
        log_message('Debug', 'START OF SERVICE ' . __FUNCTION__ . ' => ' . json_encode($user_id));

        $user = $this->user_model->getUser($user_id);
        if (empty($user)) {
            $this->response['message'] = 'Data user not found';
            log_message('Debug', 'END OF SERVICE ' . __FUNCTION__ . ' => ' . json_encode($this->response));
            return $this->response;
        }

        if (empty($user->cif)) {
            $this->response['status'] = 'success';
            $this->response['message'] = 'Cif Tidak Ditemukan';
            $this->response['data'] = [
                'totalSisaLimit' => Pegadaian::currencyIdr('0'),
                'totalUp' => Pegadaian::currencyIdr('0'),
                'totalLimitPinjaman' => "Tidak Ada",
            ];
            
            log_message('Debug', 'END OF SERVICE ' . __FUNCTION__ . ' => ' . json_encode($this->response));
            return $this->response;
        }

        $rest_cek_limit = $this->getLimitPinjaman(['cif' => $user->cif]);

        if (empty($rest_cek_limit)) {
            $this->response['message'] = 'Data tidak ditemukan';
            log_message('Debug', 'END OF SERVICE ' . __FUNCTION__ . ' => ' . json_encode($this->response));
            return $this->response;
        }

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Get Limit Pinjaman successfully';
        $this->response['data'] = $rest_cek_limit;

        log_message('Debug', 'END OF SERVICE ' . __FUNCTION__ . ' => ' . json_encode($this->response));

        return $this->response;
    }

    public function portofolioPinjaman($user_id)
    {
        log_message('Debug', 'START OF SERVICE ' . __FUNCTION__ . ' => ' . json_encode($user_id));

        $user = $this->user_model->getUser($user_id);
        if (empty($user)) {
            $this->response['message'] = 'Data user not found';
            log_message('Debug', 'END OF SERVICE ' . __FUNCTION__ . ' => ' . json_encode($this->response));
            return $this->response;
        }

        if (empty($user->cif)) {
            $this->response['message'] = 'Data cif not found';
            log_message('Debug', 'END OF SERVICE ' . __FUNCTION__ . ' => ' . json_encode($this->response));
            return $this->response;
        }

        $items = $this->getPortofolioPinjaman(['cif' => $user->cif]);

        if (empty($items)) {
            $this->response['message'] = 'Data tidak ditemukan';
            log_message('Debug', 'END OF SERVICE ' . __FUNCTION__ . ' => ' . json_encode($this->response));
            return $this->response;
        }

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Get Data Portofolio Mt Online Successfully';
        $this->response['data'] = $items;

        log_message('Debug', 'END OF SERVICE ' . __FUNCTION__ . ' => ' . json_encode($this->response));

        return $this->response;
    }

    private function getLimitPinjaman($payload)
    {
        $rest_limit_pinjaman = $this->rest_switching_portofolio_service->portofolioSisaLimitMtOnline($payload);

        if (empty($rest_limit_pinjaman['responseCode']) || $rest_limit_pinjaman['responseCode'] !== '00') {
            return null;
        }
        
        $items = json_decode($rest_limit_pinjaman['data']);

        if (empty($items)) {
            return null;
        }

        $totalSisaLimit = !empty($items->totalSisaLimit) ? Pegadaian::currencyIdr($items->totalSisaLimit) : Pegadaian::currencyIdr('0');
        $totalUp = !empty($items->totalUp) ? Pegadaian::currencyIdr($items->totalUp) : Pegadaian::currencyIdr('0');
        $totalLimitPinjaman = !empty($items->totalLimitPinjaman) ? Pegadaian::currencyIdr($items->totalLimitPinjaman) : "Tidak Ada";

        $data = [
            'totalSisaLimit' => $totalSisaLimit,
            'totalUp' => $totalUp,
            'totalLimitPinjaman' => $totalLimitPinjaman,
        ];

        return $data ?? [];
    }

    private function getPortofolioPinjaman($payload)
    {
        $rest_list_portofolio = $this->rest_switching_portofolio_service->portofolioMtonline($payload);
        
        if (empty($rest_list_portofolio['responseCode']) || $rest_list_portofolio['responseCode'] !== '00') {
            return null;
        }

        $items = json_decode($rest_list_portofolio['data']);
   
        if (empty($items)) {
            return null;
        }

        foreach ($items as $item) {
            $detailJaminanKT = [];

            if (isset($item->detailJaminanKT)) {
                $detailJaminanKT = $this->detailJaminanKT($item->detailJaminanKT);
            }

            $data[] = [
                'group' => $item->group ?? null,
                'kodeCabang' => $item->kodeCabang ?? null,
                'kodeProduk' => $item->kodeProduk ?? null,
                'namaProduk' => $item->namaProduk ?? null,
                'noKontrak' => $item->noKontrak ?? null,
                'tglJatuhTempo' => $item->tglJatuhTempo ?? null,
                'tglKredit' => $item->tglKredit ?? null,
                'pinjamanAktif' => !empty($item->up) ? Pegadaian::currencyIdr($item->up) : Pegadaian::currencyIdr('0'),
                'kewajiban' => !empty($item->kewajiban) ? Pegadaian::currencyIdr($item->kewajiban) : Pegadaian::currencyIdr('0'),
                'limitPinjaman' => !empty($item->limitPinjaman) ? Pegadaian::currencyIdr($item->limitPinjaman) : Pegadaian::currencyIdr('0'),
                'kewajibanUntukValidasi' => !empty($item->kewajiban) ? (float) $item->kewajiban : 0,
                'limitPinjamanUntukValidasi' => !empty($item->limitPinjaman) ? (float) $item->limitPinjaman : 0,
                'agunan' => $detailJaminanKT['dataJaminan'] ?? [],
                'detailAgunan' => $detailJaminanKT['keteranganDataJaminan'] ?? "",
                'totalTaksiran' => $detailJaminanKT['totalTaksiran'] ?? 0,
            ];
        }
        
        return $data ?? [];
    }


    private function detailJaminanKT($detailJaminanKT)
    {
        $data = [];
        $keteranganDataJaminan = "";
        (float) $totalTaksiran = 0;

        foreach ($detailJaminanKT as $detailJaminan) {
            $dataJaminan = [];

            if (isset($detailJaminan->jumlah)) {
                $dataJaminan['jumlah'] = $detailJaminan->jumlah;
            }

            if (isset($detailJaminan->jenisPerhiasan)) {
                $dataJaminan['jenisPerhiasan'] = $detailJaminan->jenisPerhiasan;
                $dataJaminan['namaJenisPerhiasan'] = $this->jenisPerhiasan($detailJaminan->jenisPerhiasan);
            }

            if (isset($detailJaminan->tipeJaminan)) {
                $keyTipeJaminan = str_replace(' ', '', $detailJaminan->tipeJaminan);
                $dataJaminan['tipeJaminan'] = $detailJaminan->tipeJaminan;
            }

            if (isset($detailJaminan->karat)) {
                $dataJaminan['karat'] = $detailJaminan->karat;
            }

            if (isset($detailJaminan->berat)) {
                $dataJaminan['berat'] = $detailJaminan->berat;
            }

            $dataJaminan['beratKotor'] = $detailJaminan->beratKotor ?? null;
            $dataJaminan['keterangan'] = $detailJaminan->keterangan ?? null;

            $keteranganDataJaminan .= $detailJaminan->keterangan . ". ";
            $totalTaksiran =  $totalTaksiran + (float) $detailJaminan->taksiran;

            $data[] = [
                "kategori" => $dataJaminan['tipeJaminan'],
                "prosesKey" => $keyTipeJaminan,
                $keyTipeJaminan => $dataJaminan
            ];
        }

        if (!empty($data)) {
            $data = $this->prosesDataJaminan($data);
        }

        $returnData ["dataJaminan"] = $data ?? [];
        $returnData ["keteranganDataJaminan"] = $keteranganDataJaminan ?? "";
        $returnData ["totalTaksiran"] = $totalTaksiran ?? 0;
        
        return $returnData ?? [];
    }

    private function prosesDataJaminan($datas)
    {
        $returnData = [];

        foreach ($datas as $data) {
            if ($this->multiKeyExists($returnData, $data['prosesKey'])) {
                $returnData = $this->pushArrayByPosition($returnData, $data[$data['prosesKey']], $data['prosesKey']);
            } else {
                $returnData[] = [
                    "kategori" => $data['kategori'],
                    "prosesKey" => $data['prosesKey'],
                    $data['prosesKey'] => array($data[$data['prosesKey']])
                ];
            }
        }

        if (!empty($returnData)) {
            $returnData = $this->prosesUbahNamaKey($returnData);
        }

        return $returnData ?? [];
    }

    private function prosesUbahNamaKey(array $arr)
    {
        for ($literasi = 0; $literasi < count($arr); $literasi++) {
            $arr[$literasi]['detail'] = $arr[$literasi][$arr[$literasi]['prosesKey']];
            unset($arr[$literasi][$arr[$literasi]['prosesKey']]);
        }

        return $arr;
    }
    private function pushArrayByPosition(array $arrays, $dataInput, $key)
    {
        for ($literasi = 0; $literasi < count($arrays); $literasi++) {
            if (array_key_exists($key, $arrays[$literasi])) {
                array_push($arrays[$literasi][$key], $dataInput);
            }
        }

        return $arrays;
    }

    private function multiKeyExists(array $arr, $key)
    {
        if (array_key_exists($key, $arr)) {
            return true;
        }
    
        foreach ($arr as $element) {
            if (is_array($element)) {
                if ($this->multiKeyExists($element, $key)) {
                    return true;
                }
            }
        }
    
        return false;
    }

    private function jenisPerhiasan($jenis_perhiasan)
    {
        $arr_jenis_perhiasan = [
            "AN" => "Anting",
            "CN" => "Cincin",
            "GL" => "Gelang",
            "GW" => "Giwang",
            "KL" => "Kalung",
            "LI" => "Liontin",
            "LN" => "Lain",
        ];

        if (array_key_exists($jenis_perhiasan, $arr_jenis_perhiasan)) {
            return $arr_jenis_perhiasan[$jenis_perhiasan];
        }

        return $jenis_perhiasan;
    }

    public function cekPengajuan($user_id, $request)
    {
        log_message('Debug', 'START OF SERVICE ' . __FUNCTION__ . ' => ' . json_encode($user_id));
        $user = $this->user_model->getUser($user_id);

        if (empty($user)) {
            $this->response['message'] = 'Data user not found';
            log_message('Debug', 'END OF SERVICE ' . __FUNCTION__ . ' => ' . json_encode($this->response));
            return $this->response;
        }

        if (empty($user->cif)) {
            $this->response['message'] = 'Data cif not found';
            log_message('Debug', 'END OF SERVICE ' . __FUNCTION__ . ' => ' . json_encode($this->response));
            return $this->response;
        }

        $data = $this->responseCekPengajuan($user, $request);

        if (empty($data)) {
            $this->response['message'] = 'Data tidak ditemukan';
            log_message('Debug', 'END OF SERVICE ' . __FUNCTION__ . ' => ' . json_encode($this->response));
            return $this->response;
        }

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Berhasil Cek Pengajuan';
        $this->response['data'] = $data;

        log_message('Debug', 'END OF SERVICE ' . __FUNCTION__ . ' => ' . json_encode($this->response));
        return $this->response;
    }

    private function responseCekPengajuan($user, $request)
    {
        if ($user->aktifasiTransFinansial == '0' || $user->kyc_verified == '0') {
            $res['title'] = 'Upgrade ke Akun Premium';
            $res['desc'] = 'Gunakan fitur ini dan dapatkan berbagai penawaran menarik dengan upgrade akun kamu melalui aplikasi atau cabang terdekat.';
            $res['button'] =  [
                [
                    'title' => 'Lihat Cabang Terdekat',
                    'callback' => 'CabangPegadaian',
                    'color' => 'green',
                ],
            ];

            return $res;
        }

        if ($request['kewajiban'] > $request['sisa_limit']) {
            $res['title'] = 'Kewajiban Melebihi Sisa Limit';
            $res['desc'] = 'Biaya yang harus kamu bayar melebihi sisa limit. Pilih portofolio lainnya';
            $res['button'] =  [
                [
                    'title' => 'OK',
                    'callback' => '',
                    'color' => 'green',
                ],
            ];
            
            return $res;
        }

        $res['title'] = null;
        $res['desc'] = null;
        $res['button'] =  [
            [
                'title' => null,
                'callback' => null,
                'color' => null,
            ],
        ];
        
        return $res;
    }

    public function portofolioPinjamanDetail($user_id, $request)
    {
        log_message('Debug', 'START OF SERVICE ' . __FUNCTION__ . ' => ' . json_encode($user_id));
        $user = $this->user_model->getUser($user_id);

        if (empty($user)) {
            $this->response['message'] = 'Data user not found';
            log_message('Debug', 'END OF SERVICE ' . __FUNCTION__ . ' => ' . json_encode($this->response));
            return $this->response;
        }

        if (empty($user->cif)) {
            $this->response['message'] = 'Data cif not found';
            log_message('Debug', 'END OF SERVICE ' . __FUNCTION__ . ' => ' . json_encode($this->response));
            return $this->response;
        }
        
        $items = $this->getPortofolioPinjamanDetail(['norek' => $request['nomor_rekening']]);

        if (empty($items)) {
            $this->response['message'] = 'Data tidak ditemukan';
            log_message('Debug', 'END OF SERVICE ' . __FUNCTION__ . ' => ' . json_encode($this->response));
            return $this->response;
        }

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Get Detail Portofolio Mt Online Successfully';
        $this->response['data'] = $items;

        log_message('Debug', 'END OF SERVICE ' . __FUNCTION__ . ' => ' . json_encode($this->response));

        return $this->response;
    }

    private function getPortofolioPinjamanDetail($payload)
    {
        $rest_list_portofolio = $this->rest_switching_portofolio_service->portofolioMtonlineDetail($payload);
        
        if (empty($rest_list_portofolio['responseCode']) || $rest_list_portofolio['responseCode'] !== '00') {
            return null;
        }

        $item = json_decode($rest_list_portofolio['data']);
        
        if (empty($item)) {
            return null;
        }

        $data = [
            'group' => $item->group ?? null,
            'kodeCabang' => $item->kodeCabang ?? null,
            'namaCabang' => $item->namaCabang ?? null,
            'kodeProduk' => $item->kodeProduk ?? null,
            'namaProduk' => $item->namaProduk ?? null,
            'noKontrak' => $item->noKontrak ?? null,
            'tglJatuhTempo' => $item->tglJatuhTempo ?? null,
            'tglKredit' => $item->tglKredit ?? null,
            'pinjamanAktif' => !empty($item->up) ? Pegadaian::currencyIdr($item->up) : Pegadaian::currencyIdr('0'),
            'kewajiban' => !empty($item->kewajiban) ? Pegadaian::currencyIdr($item->kewajiban) : Pegadaian::currencyIdr('0'),
            'limitPinjaman' => !empty($item->limitPinjaman) ? Pegadaian::currencyIdr($item->limitPinjaman) : Pegadaian::currencyIdr('0'),
            'kewajibanUntukValidasi' => !empty($item->kewajiban) ? (float) $item->kewajiban : 0,
            'limitPinjamanUntukValidasi' => !empty($item->limitPinjaman) ? (float) $item->limitPinjaman : 0,
            'namaNasabah' =>  $item->namaNasabah ?? null,
            'cif' =>  $item->cif ?? null,
            'charges' =>  !empty($item->charges) ? Pegadaian::currencyIdr($item->charges) : Pegadaian::currencyIdr('0'),
        ];

        if (isset($item->detailJaminanKT)) {
            $detailJaminanKT = $this->detailJaminanKT($item->detailJaminanKT);
        }

        $data['agunan'] = $detailJaminanKT['dataJaminan'] ?? [];
        $data['detailAgunan'] = $detailJaminanKT['keteranganDataJaminan'] ?? [];
        $data['totalTaksiran'] = $detailJaminanKT['totalTaksiran'] ?? [];
        
        return $data ?? [];
    }

    private function detailAgunan($agunans)
    {
        foreach ($agunans as $agunan) {
            $dataAgunan[] = [
                "kategori" => $agunan->kategori,
                "detail" => $agunan->detail
            ];
        }

        return $dataAgunan ?? [];
    }

    public function checkValidationLoanAmount($loan_amount, $rental_cost_fee, $insurance_surcharge, $admin_surcharge)
    {
        $loan_amount = (int)$loan_amount;
        $limit_loan_amount = (int)$rental_cost_fee + (int)$insurance_surcharge + (int)$admin_surcharge;

        return $limit_loan_amount > $loan_amount;
    }

    public function pinjamanDiterima($mtonline)
    {
        $return_val = $mtonline->amount - $mtonline->total_surcharge;
        return $return_val ?? 0;
    }
}
