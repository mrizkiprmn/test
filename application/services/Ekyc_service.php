<?php

/**
 * @property ConfigModel config_model
 * @property EkycModel ekyc_model
 * @property RestSwitchingJswitch_service rest_switching_jswitch_service
 * @property RestSwitchingPortofolio_service rest_switching_portofolio_service
 * @property User user_model
 */
class Ekyc_service extends MY_Service
{
    private $flag_limit_submit_dukcapil = 10;
    private $code_invalid = 12;
    private $code_image_not_found = 99;
    protected $response = [
        'code' => 101,
        'status' => 'error',
        'message' => 'Terjadi kesalahan, cobalah beberapa saat lagi.',
        'data' => null
    ];

    public function __construct()
    {
        parent::__construct();
        $this->load->model('EkycModel', 'ekyc_model');
        $this->load->model('ConfigModel', 'config_model');
        $this->load->model('User', 'user_model');
        $this->load->service('RestSwitchingJswitch_service', 'rest_switching_jswitch_service');
        $this->load->service('RestSwitchingPortofolio_service', 'rest_switching_portofolio_service');
    }

    public function checkOpenTabEmas($user_id)
    {
        $user = $this->user_model->getUser($user_id);

        $rest_data = $this->rest_switching_portofolio_service->pdsCheckOpenTabEmas(['cif' => $user->cif ?? '']);

        $this->response['message'] = 'Data tidak ditemukan';
        $this->response['data']    = [
            'is_allowed_ekyc' => false,
            'text' => [
                'title' => 'Terjadi Kesalahan Sistem',
                'description' => 'Mohon coba beberapa saat lagi.',
            ]
        ];

        if ($rest_data['responseCode'] == '14') {
            $this->response['message'] = $rest_data['responseDesc'] ?? 'Terjadi Kesalahan Sistem';
            return $this->response;
        }

        if ($rest_data['responseCode'] != '00') {
            $this->response['message'] = $rest_data['responseDesc'] ?? 'Terjadi Kesalahan Sistem';
            $this->response['data'] = $rest_data;
            return $this->response;
        }

        $this->response['status']  = 'success';
        $this->response['message'] = 'Cek Open Tabungan Emas';

        $list_rekening  = json_decode($rest_data['data'], true);
        $is_open_te_pds = $this->checkIsOpenTePds($list_rekening);

        if (empty($is_open_te_pds)) {
            $this->response['data']['text'] = $this->setMessageOpenIsNotPds();
            return $this->response;
        }

        $config_ekyc = $this->config_model->getLimitSubmitEkycDukcapil();

        if (!empty($config_ekyc->limit) && ($config_ekyc->limit == 'true' && $config_ekyc->last_access >= date('Y-m-d'))) {
            $this->response['data']['text'] = [
                'title' => 'Kuota Harian Habis',
                'description' => 'Maaf, kuota upgrade akun premium hari ini habis. Coba lagi besok atau kunjungi cabang terdekat.'
            ];
            return $this->response;
        }

        $this->response['status'] = 'success';
        $this->response['data']['is_allowed_ekyc'] = true;

        return $this->response;
    }

    public function submitEkycSelfie($request, $user_id)
    {
        $this->response['status'] = 'success';
        $this->response['data'] = [
            'is_blocked' => false,
            'is_verified' => false,
            'text' => [
                'title' => 'Terjadi Kesalahan Sistem',
                'description' => 'Mohon coba beberapa saat lagi.',
            ],
        ];

        if (empty($ekyc = $this->ekyc_model->getbyUser($user_id))) {
            $this->response['message'] = 'Data user tidak ditemukan';
            return $this->response;
        }

        // check blocked first
        $this->checkBlockedEkyc($user_id);

        $face_base64 = trim(str_replace("↵", '', $request['faceBase64']), "\t\n\r\0\x0B");
        $encode_face_base64 = base64_encode($face_base64);
        $submit_dukcapil = $this->submitDukcapil($ekyc, $encode_face_base64);
        $ekyc = $this->ekyc_model->getbyUser($user_id);

        if ($submit_dukcapil['responseCode'] != '00') {
            $this->response['data']['is_blocked'] = $ekyc->error_count > 2;
            return $this->response;
        }

        $check_verification = $this->checkVerificationResultEkyc($submit_dukcapil);

        // check blocked after update ekyc
        $this->checkBlockedEkyc($user_id, 'NotVerified');

        if (!$check_verification) {
            $this->response['data']['is_blocked'] = $ekyc->error_count > 2;
            $this->response['data']['text'] = $this->setMessageNotVerifiedEkyc();
            return $this->response;
        }

        $upload_dir = $this->config->item('upload_dir');
        $dir_selfie = "{$upload_dir}/ekyc/selfie";

        if (is_dir($dir_selfie) === false) {
            mkdir($dir_selfie);
        }

        $dir_selfie_user = "{$dir_selfie}/$ekyc->user_id";

        if (is_dir($dir_selfie_user) === false) {
            mkdir($dir_selfie_user);
        }

        $files = glob($upload_dir . '/ekyc/selfie/' . $ekyc->user_id . '/*'); // get all file names

        foreach ($files as $file) {
            if (is_file($file)) {
                unlink($file);
            }
        }

        $config['upload_path'] = $dir_selfie_user;
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
        $config['max_size'] = 0;
        $config['max_width'] = 0;
        $config['max_height'] = 0;
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('selfieFile')) {
            $this->response['message'] = $this->upload->display_errors();
            return $this->response;
        }

        $data = $this->upload->data();

        $this->ekyc_model->update(
            $ekyc->user_id,
            ['image_file_selfie' => $data['file_name'], 'image_selfie' => $face_base64]
        );

        $this->response['message'] = 'Submit Ekyc Selfie Successfully';
        $this->response['data'] = [
            'is_blocked' => false,
            'is_verified' => true,
            'text' => $this->setMessageVerifiedEkyc()
        ];

        return $this->response;
    }

    public function submitDukcapil($ekyc, $image_base64)
    {
        $is_dummy = $this->config->item('use_fr_dukcapil') != 1;
        $payload  = [
            'customerId' => $ekyc->user_id ?? '',
            'image' => $image_base64 ?? '',
            'nik' => $ekyc->nik ?? '',
            'position' => $this->config->item('position'),
            'template' => $this->config->item('template'),
            'threshold' => $this->config->item('threshold'),
            'transactionSource' => $this->config->item('transaction_source'),
            'type' => $this->config->item('type')
        ];

        $rest_jswitch = $this->rest_switching_jswitch_service->kependudukanImage($payload, $is_dummy);
        $this->ekyc_model->update($ekyc->user_id, ['response_fr_dukcapil' => json_encode($rest_jswitch)]);
        $rest_jswitch['data'] = json_decode($rest_jswitch['data'], true);

        $this->setCountSubmitDukcapil($rest_jswitch, $ekyc);
        $this->setLimitSubmitDukcapil($rest_jswitch);

        if (!empty($rest_jswitch['data']['message'])) {
            $this->response['message'] = $rest_jswitch['data']['message'];
        }

        return $rest_jswitch;
    }

    private function checkIsOpenTePds($data)
    {
        $client_id_ekyc = $this->config_model->getClientIdEkyc();
        $client_id_open_te = array_unique(array_column($data, 'clientId'));

        foreach ($client_id_open_te as $client_id) {
            if (!in_array($client_id, $client_id_ekyc)) {
                return false;
            }
        }

        return true;
    }

    private function setCountSubmitDukcapil($rest_jswitch, $ekyc)
    {
        $rc_not_rr_count = [$this->code_invalid, 99, 500];
        $rc = $rest_jswitch['responseCode'] ?? null;
        $rc_fr_data = $rest_jswitch['data']['RC'] ?? null;
        
        $validate_image_not_found = $this->code_invalid == $rc && $rc_fr_data == $this->code_image_not_found;
        if (in_array($rc, $rc_not_rr_count) && !$validate_image_not_found) {
            return null;
        }

        $config_ekyc = $this->config_model->getLimitSubmitEkycDukcapil();
        if ($config_ekyc->last_access < date('Y-m-d')) {
            $this->config_model->updateLimitSubmitEkycDukcapil('false');
        }

        $check_verification = $this->checkVerificationResultEkyc($rest_jswitch);
        if ($check_verification && !$validate_image_not_found) {
            return null;
        }

        $error_count = $ekyc->error_count ?? 0;
        $rest_jswitch['errorCount'] = ++$error_count;
        $rest_jswitch['data'] = json_encode($rest_jswitch['data']);

        $field['error_count'] = $error_count;

        if ($error_count > 2) {
            $field['status'] = 4;
        }

        $this->ekyc_model->update($ekyc->user_id, $field);
        $this->config_model->updateLastAccessSubmitEkycDukcapil();
    }

    private function setLimitSubmitDukcapil($rest_jswitch)
    {
        $message = $rest_jswitch['data']['RC'] ?? '';

        if ($message != $this->flag_limit_submit_dukcapil) {
            return null;
        }

        $this->response['data']['text'] = [
            'title' => 'Kuota Harian Habis',
            'description' => 'Maaf, kuota upgrade akun premium hari ini habis. Coba lagi besok atau kunjungi cabang terdekat.',
        ];

        $this->config_model->updateLimitSubmitEkycDukcapil('true');
    }

    private function checkVerificationResultEkyc($response)
    {
        $data = $response['data'];

        if (is_string($response['data'])) {
            $data = json_decode($response['data'], true);
        }

        return $data['verificationResult'] ?? true;
    }

    private function checkBlockedEkyc($user_id, $message = 'Blocked')
    {
        $getMessage = "setMessage{$message}Ekyc";
        $ekyc = $this->ekyc_model->getbyUser($user_id);

        if ($ekyc->status == 4) {
            Pegadaian::showError($this->response['message'], [
                'is_blocked' => true,
                'is_verified' => false,
                'text' => $this->$getMessage()
            ], 200, 'success');
        }

        return true;
    }

    private function setMessageBlockedEkyc()
    {
        return [
            'title' => 'Terjadi Kesalahan',
            'description' => 'Silahkan datang ke cabang untuk melakukan upgrade akun premium.',
        ];
    }

    private function setMessageNotVerifiedEkyc()
    {
        return [
            'title' => 'Autentikasi Belum Berhasil',
            'description' => 'Autentikasi Foto dengan data Dukcapil belum berhasil',
        ];
    }

    private function setMessageVerifiedEkyc()
    {
        return [
            'title' => 'Autentikasi Berhasil',
            'description' => 'Autentikasi Foto dengan data Dukcapil berhasil',
        ];
    }

    private function setMessageOpenIsNotPds()
    {
        return [
            'title' => 'Mohon Maaf',
            'description' => 'Untuk sementara layanan ini hanya dapat digunakan oleh nasabah yang membuka Tabungan Emas melalui aplikasi Pegadaian Digital',
        ];
    }
}
