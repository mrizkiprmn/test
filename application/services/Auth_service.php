<?php

/**
 * @property AuditLoginModel audit_login_model
 * @property AuditLogModel audit_log_model
 * @property ConfigModel config_model
 * @property Emas_service emas_service
 * @property Gpoint_service gpoint_service
 * @property Main_service main_service
 * @property NotificationModel notification_model
 * @property Profile_service profile_service
 * @property ResetPasswordModel reset_password_model
 * @property RestSwitchingOtp_service rest_switching_otp_service
 * @property Cache_service cache_service
 * @property User user_model
 * @property UserRegisterModel user_register_model
 * @property PegawaiModel pegawai_model
 */
class Auth_service extends MY_Service
{
    protected $response = [
        'code' => 101,
        'status' => 'error',
        'message' => 'Terjadi kesalahan mohon coba lagi',
        'data' => null
    ];

    public function __construct()
    {
        parent::__construct();

        $this->load->helper('Message');
        $this->load->helper('Pegadaian');

        $this->load->model('AuditLoginModel', 'audit_login_model');
        $this->load->model('AuditLogModel', 'audit_log_model');
        $this->load->model('ConfigModel', 'config_model');
        $this->load->model('NotificationModel', 'notification_model');
        $this->load->model('PegawaiModel', 'pegawai_model');
        $this->load->model('ResetPasswordModel', 'reset_password_model');
        $this->load->model('User', 'user_model');
        $this->load->model('UserRegisterModel', 'user_register_model');

        $this->load->service('AuditLog_service', 'audit_log_service');
        $this->load->service('Emas_service', 'emas_service');
        $this->load->service('Gpoint_service', 'gpoint_service');
        $this->load->service('Main_service', 'main_service');
        $this->load->service('Profile_service', 'profile_service');
        $this->load->service('RestSwitchingOtp_service', 'rest_switching_otp_service');
        $this->load->service('Cache_service', 'cache_service');

        $this->load->helper('Pegadaian');
        $this->load->helper('Message');
    }

    public function login($request)
    {
        $authentication = $this->user_model->authentication($request['email']);
        $message = 'No HP dan password tidak cocok.';

        if (filter_var($request['email'], FILTER_VALIDATE_EMAIL)) {
            $message = 'Email dan password tidak cocok. Pastikan email telah terverifikasi!';
        }

        $this->response['message'] = $message;

        if (empty($authentication)) {
            return $this->response;
        }

        $audit_login_data = $this->audit_login_model->find(['user_id' => $authentication->user_AIID], 1);

        $authentication = $this->checkPasswordAuthentication($authentication, $request);
        if (empty($authentication)) {
            return $this->response;
        }

        $user = $this->user_model->profile($authentication->user_AIID);
        $token = $this->setTokenAuthentication($authentication, $request['agen'], $request['version']);

        if (empty($token)) {
            return $this->response;
        }

        $tab_emas = $this->emas_service->listAccountNumber($user->cif);

        // get update data user
        $user = $this->user_model->profile($authentication->user_AIID);
        $user->tabunganEmas = $tab_emas;
        // $user->point = $this->gpoint_service->getCustomerTotalPoint($user->id);
        $user->isFirstLogin = false;
        $user->isPegawai = $this->pegawai_model->getIsEmployeeByUser($user->cif, $authentication->no_hp);

        if (empty($audit_login_data)) {
            $user->isFirstLogin = true;
        }

        //get force update password
        $user->isForceUpdatePassword = $this->main_service->getForceUpdatePassword($request['password']);

        $this->response['status']  = 'success';
        $this->response['message'] = 'Login Successfully';
        $this->response['data'] = [
            'user'  => $user,
            'token' => $token
        ];

        return $this->response;
    }

    public function registerCheck($request)
    {
        if (empty($this->sendOtp($request['no_hp']))) {
            return $this->response;
        }

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Register Check Successfully';

        return $this->response;
    }

    public function resendOtp($request, $request_type = 'register')
    {
        if (!empty($request['request_type'])) {
            $request_type = $request['request_type'];
        }

        $rest_otp = $this->sendOtp($request['no_hp'], $request_type);

        if (empty($rest_otp)) {
            $this->response['message'] = 'Resend OTP Failed';
            $this->response['status']  = 'error';
            $this->response['data']    = $rest_otp;
            return $this->response;
        }

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Resend OTP Successfully';

        return $this->response;
    }

    public function registerOtp($request)
    {
        $rest_otp = $this->rest_switching_otp_service->otpCheck([
            'requestType' => 'register',
            'noHp' => $request['no_hp'],
            'token' => $request['otp'],
        ]);

        if (!empty($rest_otp) && $rest_otp['responseCode'] != '00') {
            $this->response['message'] = 'OTP yang kamu masukan salah';
            return $this->response;
        }

        $register = $this->user_register_model->insert(['no_hp' => $request['no_hp']]);
        if (empty($register)) {
            return $this->response;
        }

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Register OTP Successfully';
        $this->response['data'] = ['register_id' => $register->id];

        return $this->response;
    }

    public function register($request)
    {
        $register = $this->user_register_model->find(['id' => $request['register_id']]);
        if (empty($register)) {
            return $this->response;
        }

        $user_id = $this->user_model->register($request['nama'], $request['no_hp']);
        if (empty($user_id)) {
            return $this->response;
        }

        $emailVerifyToken = bin2hex(random_bytes(78));
        $this->user_model->updateById($user_id['userId'], [
            'nama' => $request['nama'],
            'email' => $request['email'],
            'password' => md5($request['password']),
            'fcm_token' => $request['fcm_token'],
            'status' => 1,
            'email_verification_token' => $emailVerifyToken,
        ]);

        $request['email'] = $request['no_hp'];
        $request['agen'] = $request['agen'] ?? 'android';
        $request['version'] = $request['version'] ?? '3';

        $response = $this->login($request);

        $this->user_register_model->drop(['id' => $request['register_id'], 'no_hp' => $request['no_hp']]);
        $this->sendNotificationRegister($response['data']['user'], $request['fcm_token'], $emailVerifyToken);

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Register Successfully';
        $this->response['data'] = $response['data'] ?? [];

        return $this->response;
    }

    public function sendNotificationRegister($user, $fcm_token, $email_verification_token)
    {
        $template = $this->load->view(
            'notification/email_notification',
            ['nama' => $user->nama, 'email' => $user->email],
            true
        );

        $type = NotificationModel::TYPE_ACCOUNT;
        $title = "Selamat Datang di Pegadaian Digital Service";
        $tagline = "Hai " . $user->nama . ', lakukan verifikasi email untuk melanjutkan registrasi';

        $notification = $this->notification_model->add($user->id, $type, 'html', $title, $tagline, $template);

        Message::sendNotifWelcome($fcm_token, $notification, $user->nama);
        Message::sendEmailVerificationEmail($user->email, $user->nama, $email_verification_token);
    }

    public function requestResetPassword($request)
    {
        $email = $request['email'];
        $type_request = 'hp';
        $authentication = $this->user_model->authentication($email);

        if (empty($authentication)) {
            $this->response['message'] = 'Profile tidak ditemukan atau email belum terverifikasi';
            return $this->response;
        }

        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $type_request = 'email';
        }

        $user = $this->user_model->getUser($authentication->user_AIID);
        if (!$this->user_model->isResetPasswordAvailable($user)) {
            $next_password_reset = new DateTime($user->next_password_reset);
            $diff = $next_password_reset->diff(new DateTime('now'));
            $this->response['message'] = "Kami mendeteksi Anda terlalu sering melakukan permintaan reset password. Mohon tunggu {$diff->i} menit lagi.";

            return $this->response;
        }

        // set data audit log
        $audit_log = [
            'user_id' => $user->user_AIID,
            'cif' => $user->cif,
            'nama_nasabah' => $user->nama,
            'no_hp' => $user->no_hp,
            'default_property' => '',
            'change_property' => '',
            'action' => $this->audit_log_model::ACTION_REQUEST,
            'modul' => $this->audit_log_model::MODUL_RESET_PASSWORD,
            'platform' => $this->audit_log_model::PLATFORM_PDS,
        ];

        // set other_property audit log
        $audit_other_property = [
            'device_id' => $request['device_id'] ?? '',
            'latitude'  => $request['latitude'] ?? '',
            'longitude' => $request['longitude'] ?? '',
            'timestamp' => $request['timestamp'] ?? '',
            'timezone'  => $request['timezone'] ?? '',
        ];

        $this->audit_log_model->add($audit_log, $audit_other_property);

        return $this->sendResetPassword($user, $type_request);
    }

    public function otpResetPassword($request)
    {
        return $this->rest_switching_otp_service->otpCheck([
            'requestType' => 'resetpassword',
            'noHp'        => $request['email'],
            'token'       => $request['otp'],
        ]);
    }

    public function checkOtpResetPassword($request)
    {
        $rest_otp = $this->otpResetPassword($request);

        if (($rest_otp['responseCode'] ?? null) != 00) {
            $this->response['message'] = 'Kode OTP yang dimasukan tidak valid';
            $this->response['status']  = 'error';
            $this->response['data']    = $rest_otp;

            return $this->response;
        }

        $this->response['status']  = 'success';
        $this->response['message'] = 'Kode OTP yang dimasukan valid!';

        return $this->response;
    }

    public function resetPasswordByOtp($request)
    {
        $no_hp = $request['email'];
        if (!$user = $this->reset_password_model->isValidPhone($no_hp)) {
            $this->response['message'] = 'Akun Anda Belum Aktif';
            return $this->response;
        }

        $user = $this->user_model->getUserByLinkCif($no_hp);
        $password = md5($request['password']);
        $this->user_model->updateUser($user->user_AIID, ['password' => $password,]);

        $this->audit_log_model->add([
            'user_id' => $user->user_AIID,
            'cif' => $user->cif,
            'nama_nasabah' => $user->nama,
            'no_hp' => $user->no_hp,
            'default_property' => $user->password,
            'change_property' => $password,
            'action' => $this->audit_log_model::ACTION_CHANGE,
            'modul' => $this->audit_log_model::MODUL_RESET_PASSWORD,
            'platform' => $this->audit_log_model::PLATFORM_PDS,
        ]);

        $this->response['status']  = 'success';
        $this->response['message'] = 'Password diperbaharui';

        return $this->response;
    }

    private function checkPasswordAuthentication($user, $request, $custom_response = [])
    {
        $now = new DateTime('now');
        $now = $now->format('Y-m-d H:i:s');

        if (empty($user)) {
            return null;
        }

        $this->storeAuditLogin($user, $request);
        // Send Response Blocked User
        $this->setResponseBlockLogin($user->user_AIID, $custom_response);

        if ($user->password === md5($request['password'])) {
            $this->user_model->updateById($user->user_AIID, ['try_login_date' => $now]);
            $this->setUnBlockedUser($user);
            
            return $user;
        }

        $this->setBlockedUser($user);
        $this->user_model->updateById($user->user_AIID, ['try_login_date' => $now]);
        // Send Response Blocked User
        $this->setResponseBlockLogin($user->user_AIID, $custom_response);
    }

    private function setBlockedUser($user)
    {
        $count = $user->wrong_password_count ?? 0;
        $wrong_count = (int)$count + 1;
        $now = new DateTime('now');

        // set wrong password to 0 if try_login_date < 1 day
        if (!empty($user->try_login_date)) {
            $try_login_date = new DateTime($user->try_login_date);
        }

        if (!empty($try_login_date) && $try_login_date->format('Y-m-d') < $now->format('Y-m-d')) {
            $wrong_count = 1;
        }

        // set wrong password to 0 if blocked_to_date < 1 day and wrong_password_count = 5
        if (!empty($user->blocked_to_date) && $user->wrong_password_count == $this->user_model::MAX_WRONG_PASSWORD) {
            $blocked_to_date = new DateTime($user->blocked_to_date);
        }

        if (!empty($blocked_to_date) && $blocked_to_date->format('Y-m-d H:i:s') < $now->format('Y-m-d H:i:s')) {
            $this->setUnBlockedUser($user);
            $wrong_count = 1;
        }

        $this->user_model->updateById($user->user_AIID, ['wrong_password_count' => $wrong_count]);

        $user = $this->user_model->getUser($user->user_AIID);

        if ($user->wrong_password_count == $this->user_model::MIN_WRONG_PASSWORD) {
            $time_min_wrong_password = $this->config_model->whereByVariable('min_wrong_password');
            $time = $time_min_wrong_password->value_2 ?? 'PT1H';
            $data_interval = new DateInterval($time);
            $now->add($data_interval);
            $blocked_to_date = $now->format('Y-m-d H:i:s');
            $this->user_model->setBlockUserById($user->user_AIID, $blocked_to_date);
            // send notification blocked one hour
            $this->profile_service->sendNotificationBlockedLoginOneHour($user);
        }

        if ($user->wrong_password_count == $this->user_model::MAX_WRONG_PASSWORD) {
            $time_max_wrong_password = $this->config_model->whereByVariable('max_wrong_password');
            $time = $time_max_wrong_password->value_2 ?? 'P1D';
            $data_interval = new DateInterval($time);
            $now->add($data_interval);
            $blocked_to_date = $now->format('Y-m-d H:i:s');
            $this->user_model->setBlockUserById($user->user_AIID, $blocked_to_date);
            // send notification blocked one day
            $this->profile_service->sendNotificationBlockedLoginOneDay($user);
        }
    }

    private function setUnBlockedUser($user)
    {
        $this->user_model->setUnBlockUserById($user->user_AIID);
    }

    private function setTokenAuthentication($user, $agen, $version)
    {
        try {
            $access_token = bin2hex(random_bytes(78));
            $channel_id = $this->getChannelIdFromAgen($agen);

            if (empty($channel_id)) {
                $this->response['message'] = 'Agen or Channel not found!';
                return null;
            }

            $token = authorization::generateToken([
                'id'           => $user->user_AIID,
                'email'        => $user->email,
                'nama'         => $user->nama,
                'no_hp'        => $user->no_hp,
                'access_token' => $access_token,
                'agen'         => $agen,
                'channelId'    => $channel_id,
                'version'      => $version
            ]);

            $this->cache_service->setTokenJwt($user->user_AIID, $access_token);

            return $token;
        } catch (Exception $e) {
            return null;
        }
    }

    private function storeAuditLogin($user, $request)
    {
        $time = new DateTime('now');

        if (!empty($request['timestamp'])) {
            $time = new DateTime($request['timestamp']);
        }

        $audit_login = [
            'user_id' => $user->user_AIID ?? null,
            'channel_id' => $this->getChannelIdFromAgen($request['agen'] ?? null),
            'device_id' => $request['device_id'] ?? null,
            'ip' => $request['ip'] ?? null,
            'latitude' => $request['latitude'] ?? null,
            'longitude' => $request['longitude'] ?? null,
            'timezone' => $request['timezone'] ?? null,
            'timestamp' => $time->format('Y-m-d H:i:s'),
            'brand' => $request['brand'] ?? null,
            'os_version' => $request['os_version'] ?? null,
            'browser' => $request['browser'] ?? null,
        ];

        $this->audit_login_model->insert($audit_login);
    }

    private function getChannelIdFromAgen($agen)
    {
        if ($agen == $this->user_model::AGEN_MOBILE) {
            return $this->user_model::CHANNEL_MOBILE;
        }

        if ($agen == $this->user_model::AGEN_WEB) {
            return $this->user_model::CHANNEL_WEB;
        }

        if ($agen == $this->user_model::AGEN_ANDROID) {
            return $this->user_model::CHANNEL_ANDROID;
        }

        return null;
    }

    private function setResponseBlockLogin($user_id, $custom_response = [])
    {
        $user = $this->user_model->getUser($user_id);
        $now = new DateTime('now');
        $now = $now->format('Y-m-d H:i:s');

        if (!empty($user->blocked_to_date) && $user->blocked_to_date > $now) {
            $blocked_to_date = new DateTime($user->blocked_to_date);
            $counter = $user->wrong_password_count ?? '-';
            $blocked_to_date = $blocked_to_date->format('d M Y H:i') ?? '-';
            $message_default = "User atau password yang kamu masukkan salah {$counter} kali. Akun akan diblokir sampai dengan $blocked_to_date";
            Pegadaian::showError(
                !empty($custom_response['message']) ? $custom_response['message'] : $message_default,
                null,
                !empty($custom_response['code']) ? $custom_response['code'] : 200
            );
        }
    }

    private function sendOtp($no_hp, $requestType = 'register')
    {
        $rest_send_otp = $this->rest_switching_otp_service->otpSend([
            'requestType' => $requestType,
            'noHp' => $no_hp
        ]);

        if (!empty($rest_send_otp) && $rest_send_otp['responseCode'] == '00') {
            return true;
        }

        return false;
    }

    private function sendResetPassword($user, $request_type)
    {
        $date = new DateTime('now');
        $date->add(new DateInterval('PT30M'));
        $this->user_model->updateById($user->user_AIID, ['next_password_reset' => $date->format('Y-m-d H:i:s')]);

        if ($request_type == 'hp') {
            return $this->sendResetPasswordHp($user);
        }

        return $this->sendResetPasswordEmail($user);
    }

    private function sendResetPasswordEmail($user)
    {
        $date = new DateTime('now');
        $date->add(new DateInterval('PT30M'));

        $token = bin2hex(random_bytes(78));

        $this->reset_password_model->insert([
            'user_AIID' => $user->user_AIID,
            'token' => $token,
            'valid_until' => $date->format('Y-m-d H:i:s'),
            'type' => 'email'
        ]);

        Message::sendResetPasswordEmail($user->email, $user->nama, $token);

        $this->response['status']   = 'success';
        $this->response['message'] = 'Petunjuk reset password telah dikirimkan ke email anda';
        $this->response['method']  = 'email';

        return $this->response;
    }

    private function sendResetPasswordHp($user)
    {
        $date = new DateTime('now');
        $date->add(new DateInterval('PT30M'));

        $this->reset_password_model->insert([
            'user_AIID' => $user->user_AIID,
            'no_hp' => $user->no_hp,
            'valid_until' => $date->format('Y-m-d H:i:s'),
            'type' => 'phone'
        ]);

        $send_otp = $this->sendOTP($user->no_hp, "resetpassword");

        if ($send_otp == false) {
            $this->response['status'] = 'error';
            return $this->response;
        }

        $this->response['status']  = 'success';
        $this->response['message'] = "Kode reset password telah dikirimkan ke nomor {$user->no_hp}. Masukan kode untuk melakukan reset password pada aplikasi Pegadaian Digital Service";
        ;
        $this->response['method']  = 'phone';

        return $this->response;
    }

    public function cekPasswordSaatIni($passLama, $token)
    {
        $user = $this->user_model->getUser($token->id);
        $count = $user->wrong_password_count ?? 0;
        $wrong_count = (int)$count;
    
        if ($count == 2 || $count == 4) {
            $wrong_count = (int)$count + 1;
        }
        
        $custom_response = [
            'message' => 'Akun dikeluarkan dari aplikasi karena password salah ' . $wrong_count . ' kali',
            'code' => 401
        ];
        
        $authentication = $this->checkPasswordAuthentication($user, $passLama, $custom_response);
        
        if (!empty($authentication)) {
            $passLama['nama'] = $authentication->nama;
            $passLama['email'] = $authentication->email;
            $passLama['nohp'] = $authentication->no_hp;
        
            $this->response['code'] = 200;
            $this->response['status'] = 'success';
            $this->response['message'] = 'Password lama kamu sesuai';
            $this->response['data'] = $passLama ?? [];

            return $this->response;
        }

        $max_wrong = 2;

        if (!empty($user->blocked_to_date)) {
            $max_wrong = 4;
        }

        if ($user->wrong_password_count == '5') {
            $max_wrong = 7;
        }

        $this->response['message'] = 'Password salah. Kamu memiliki ' . ($max_wrong - $user->wrong_password_count) . ' kesempatan lagi';

        return $this->response;
    }

    public function sendPasswordBaru($password, $token)
    {
        $dataPass = [
            'password' => $password['passwordLama']
        ];

        if (empty($password['tipe'])) {
            
            // untuk ngecek lagi Validasai Password Saat Ini
            $cek = $this->cekPasswordSaatIni($dataPass, $token);
        }
        
        // hanya menampilkan Validasai error Password Saat Ini
        if (empty($password['tipe']) && $cek['status'] == 'error') {
        
            return $this->response;
        }

        $this->user_model->updateUser($token->id, ['password' => md5($password['passwordBaru'])]);
        
        $data = [
            'title' => 'Password Berhasil Diubah',
            'desc' => 'Selamat! Kamu berhasil mengubah password akun Pegadaian Digital',
            'buttonText' => 'Oke',
            'color' => 'green'
        ];

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Password kamu telah diperbarui';
        $this->response['data'] = $data ?? [];

        return $this->response;
    }
}
