<?php

use Symfony\Component\VarDumper\Cloner\Data;
use RioAstamal\AngkaTerbilang\Terbilang;

class KrasidaTe_service extends MY_Service
{
    const TRX_TYPE_OPEN = "OP";
    const TRX_PRODUCT_CODE = '35';
    const TRX_TYPE = '2';

    public function __construct()
    {
        parent::__construct();

        
        $this->load->model('User', 'userModel');
        $this->load->model('ProductMasterModel');
        $this->load->model('NotificationModel', 'notification_model');
        $this->load->model('MasterModel', 'master_model');
        $this->load->service('Emas_service', 'emas_service');
        $this->load->service('RestSwitchingPortofolio_service', 'rest_switching_portofolio_service');
        $this->load->service('RestSwitchingKrasidaTe_service', 'rest_switching_kte_service');
        $this->load->service('Notification_service', 'notification_service');
        $this->load->service('Response_service', 'response_service');
        $this->load->service('RestQr_service', 'rest_qr_service');
        $this->load->library('Pegadaian');
        $this->load->library('pdf');
        $this->terbilang = new Terbilang();
    }

    protected $response = [
        'code' => 101,
        'status' => 'error',
        'message' => 'Internal Server Error',
        'data' => null
    ];

    public function calculateDataFromQr($data)
    {
        $response = $this->response_service->getResponse('');
        $rest_scan_barcode = $this->rest_qr_service->getScanBarcode($data['url']);

        if ($rest_scan_barcode['code'] != '00' || empty($rest_scan_barcode)) {
            $response['data'] = [
                "title" => "QR Code tidak ditemukan",
                "description" => "Pastikan QR Code kamu sudah sesuai untuk melakukan transaksi ini"
            ];
            return $response;
        }

        $scan_barcode_data = $rest_scan_barcode['data'];
        
        $simulasi_data = [
            "up" => $scan_barcode_data['transactionAmount'],
            "flag" => "K",
            "productCode" => $this::TRX_TYPE_OPEN
        ];

        $rest_simulasi_kte = $this->rest_switching_kte_service->simulasiKrasidaTe($simulasi_data);
        
        if ($rest_simulasi_kte['responseCode'] != "00") {
            $response['message'] = $rest_simulasi_kte['responseDesc'];
            return $response;
        };

        $rest_simulasi_kte_data = json_decode($rest_simulasi_kte['data'], true);

        $merchant = [
            'logo' => $this->config->item('merchant_image'),
            'nama' => $this->config->item('merchant_name')
        ];

        $result = [
            'merchant' => $merchant,
            'perhiasan' => $scan_barcode_data['data'],
            'totalHarga' => $scan_barcode_data['transactionAmount'],
            'transactionCode' => $scan_barcode_data['transactionCode'],
            'saldoBlokir' => $rest_simulasi_kte_data['saldoBlokir'],
            'saldoBlokirInfo' => 'Saldo tabungan emas yang dikunci sebagai jaminan.',
            'nomorRekeningInfo' => 'Rekening tabungan emas yang akan dikunci saldonya sebagai jaminan.',
            'totalTaksiran' => $rest_simulasi_kte_data['taksiran']
        ];
        
        $response = $this->response_service->getResponse('general.succeeded');
        $response['message'] = 'Perhitungan Data QR Berhasil';
        $response['data'] = $result;

        return $response;
    }

    public function validateSaldoBlokir($data, $token)
    {
        $response = $this->response_service->getResponse('');
        $user = $this->user_model->getUser($token->id);

        $inquiry_data = [
            'noRek' => $data['noRekening'],
            'cif' => $user->cif,
            'lockGram' => $data['saldoBlokir']
        ];

        $rest_validate_kte = $this->rest_switching_kte_service->validateKrasidaTe($inquiry_data);
        
        if ($rest_validate_kte['responseCode'] == "12") {
            $response['message'] = 'Saldo tidak mencukupi';
            return $response;
        };

        if ($rest_validate_kte['responseCode'] != "00") {
            return $response;
        };

        $response = $this->response_service->getResponse('general.succeeded');
        return $response;
    }

    public function getListTenor($data)
    {
        $response = $this->response_service->getResponse('');
        $inquiry_data = [
            'up' => $data['totalHarga'],
            'flag' => 'K'
        ];

        $rest_list_tenor_kte = $this->rest_switching_kte_service->listTenorKrasidaTe($inquiry_data);
        
        if ($rest_list_tenor_kte['responseCode'] != "00") {
            $response['message'] = $rest_list_tenor_kte['responseDesc'];
            return $response;
        };

        $rest_list_tenor_kte_data = json_decode($rest_list_tenor_kte['data'], true);
        
        if (empty($rest_list_tenor_kte_data)) {
            $response['message'] = 'Data tidak ditemukan';
            return $response;
        }

        $result = $this->formatListTenor($rest_list_tenor_kte_data);

        $response = $this->response_service->getResponse('general.succeeded');
        $response['message'] = 'Get List Tenor Berhasil!';
        $response['data'] = $result;

        return $response;
    }

    public function inquiryCicilEmas($data, $token)
    {
        $response = $this->response_service->getResponse('');
        $user = $this->user_model->getUser($token->id);

        if (empty($user)) {
            $response['message'] = 'Data user tidak ditemukan';
            return $response;
        }

        $inquiry_data = [
            'noRek' => $data['noRekening'],
            'up' => $data['totalHarga'],
            'cif' => $user->cif,
            'gram' => $data['saldoBlokir'],
            'tenor' => $data['tenor'],
            'jenisTransaksi' => $this::TRX_TYPE_OPEN,
            'kodeMerchant' => $this->config->item('merchant_code'),
            'namaMerchant' => $this->config->item('merchant_name')
        ];

        $rest_inquiry_kte = $this->rest_switching_kte_service->inquiryKrasidaTe($inquiry_data);

        if ($rest_inquiry_kte['responseCode'] != "00") {
            $response['message'] = $rest_inquiry_kte['responseDesc'];
            return $response;
        }

        $rest_inquiry_kte_data = json_decode($rest_inquiry_kte['data'], true);

        if (empty($rest_inquiry_kte_data)) {
            $response['message'] = 'Data tidak ditemukan';
            return $response;
        }

        $response_data = new stdClass();
        foreach ($rest_inquiry_kte_data as $key => $value) {
            $camel_case_key = Pegadaian::getCamelCase($key);
            $response_data->{$camel_case_key} = $value;
        }

        $response = $this->response_service->getResponse('general.succeeded');
        $response['message'] = 'Summary Cicilan Emas Berhasil';
        $response['data'] = $response_data;

        return $response;
    }

    public function submitCicilEmas($token, $data)
    {
        $response = $this->response_service->getResponse('');
        $response['message'] = 'Submit Krasida Te Gagal';

        $user = $this->user_model->getUser($token->id);

        if (empty($user)) {
            $response['message'] = 'Data user tidak ditemukan';
            return $response;
        }

        if (!$this->User->isValidPIN2($token->id, $data['pin'])) {
            $response['message'] = 'PIN tidak valid';
            return $response;
        }

        $open_data = [
            "noRek" => $data['norek'],
            "noHp" => $user->no_hp,
            "up" => $data['up'],
            "cif" => $user->cif,
            "tenor" => $data['tenor'],
            "reffSwitching" => $data['reffSwitching'],
            "surcharge" => $data['surcharge'],
            "tipeTransaksi" => $this::TRX_TYPE,
            "totalKewajiban" => $data['totalKewajiban'],
            "productCode" => $this::TRX_PRODUCT_CODE,
            "gram" => $data['lockGram'],
            "jenisTransaksi" => $this::TRX_TYPE_OPEN,
            "ReffIdSwitching" => $data['reffSwitching'],
            "administrasi" => $data['administrasi'],
            "tglTransaksi" => $data['tglTransaksi'],
            "angsuran" => $data['angsuran'],
            "itemPerhiasan" => $data['itemPerhiasan'],
            'kodeMerchant' => $this->config->item('merchant_code'),
            'namaMerchant' => $this->config->item('merchant_name')
        ];

        $rest_open_kte = $this->rest_switching_kte_service->openKrasidaTe($open_data);

        if ($rest_open_kte['responseCode'] != "00") {
            $response['data'] = $rest_open_kte;
            return $response;
        }

        $rest_open_data = json_decode($rest_open_kte['data'], true);
        
        $kte_notif_data = $open_data;
        $kte_notif_data['noKredit'] = $rest_open_data['noKredit'];
        $kte_notif_data['url_pdf'] = $this->savePdf($open_data, $user, $rest_open_data);

        $list_tab_emas = $this->emas_service->listAccountNumber($user->cif)['listTabungan'];
        
        $tab_emas_for_notif = [];
        foreach ($list_tab_emas as $tab_emas) {
            if ($tab_emas['norek'] == $open_data['noRek']) {
                $tab_emas_for_notif = $tab_emas;
            }
        }
        
        $kte_notif_data['tab_emas'] = $tab_emas_for_notif;
        $this->krasidaTeNotification($kte_notif_data);

        $this->rest_qr_service->getSuccessQr($data['transactionCode']);

        $resultData['totalPinjaman'] = Pegadaian::currencyIdr($open_data['up']);
        $resultData['nomorRekening'] = $open_data['noRek'];
        $resultData['saldoEmasBlokir'] = $open_data['gram'];

        $response = $this->response_service->getResponse('general.succeeded');
        $response['message'] = 'Submit Cicilan Emas Berhasil';
        $response['data'] = $resultData;

        return $response;
    }

    public function riwayatKrasidaTe($token)
    {
        $response = $this->response_service->getResponse('');
        $user = $this->user_model->getUser($token->id);

        if (empty($user)) {
            $response['message'] = 'Data user tidak ditemukan';
            return $response;
        }

        $rest_portofolio = $this->rest_switching_portofolio_service->pinjaman(['cif' => $user->cif]);
        
        if ($rest_portofolio['responseCode'] != '00') {
            $response['data'] = $rest_portofolio;
            return $response;
        }
        
        $rest_portofolio_data = json_decode($rest_portofolio['data'], true);

        $rest_kte_riwayat_data = [];
        foreach ($rest_portofolio_data as $key => $value) {
            if ($value['kodeProduk'] == $this::TRX_PRODUCT_CODE) {
                array_push($rest_kte_riwayat_data, $value);
            }
        }

        $response = $this->response_service->getResponse('general.succeeded');
        $response['message'] = 'Get Riwayat KTE Berhasil';
        $response['data'] = $this->formatListRiwayat($rest_kte_riwayat_data);

        return $response;
    }

    public function detailriwayatKrasidaTe($request)
    {
        $response = $this->response_service->getResponse('');

        $rest_riwayat_kte = $this->rest_switching_portofolio_service->detailRiwayatKrasidaTe(["noKontrak" => $request['noKontrak']]);

        if ($rest_riwayat_kte['responseCode'] != '00') {
            $response['data'] = $rest_riwayat_kte;
            return $response;
        }

        $rest_riwayat_kte_data = json_decode($rest_riwayat_kte['data'], true);

        $totalHarga = 0;
        foreach ($rest_riwayat_kte_data['listDetailItemPerhiasan'] as $perhiasan) {
            $totalHarga += $perhiasan['subTotalHarga'];
        }

        $rest_riwayat_kte_data['totalHargaPerhiasan'] = strval($totalHarga);
        $rest_riwayat_kte_data['kodeMerchant'] = $this->config->item('merchant_name');

        $response = $this->response_service->getResponse('general.succeeded');
        $response['message'] = 'Get Riwayat KTE Berhasil';
        $response['data'] = $rest_riwayat_kte_data;

        return $response;
    }

    private function savePdf($open_data, $user, $rest_open_data)
    {
        $pdf_data = new stdClass();
        $pdf_data->user = $user;
        
        foreach ($open_data as $key => $value) {
            $pdf_data->{$key} = $value;
        }

        $pdf_data->namaPinca = $rest_open_data['dataAkad']['namaPinca'];
        $pdf_data->namaOutlet = $rest_open_data['dataAkad']['namaOutlet'];
        $pdf_data->kodeOutlet = $rest_open_data['dataAkad']['kodeCabang'];

        $pdf_data->upTerbilang = $this->terbilang->terbilang($pdf_data->up);
        $pdf_data->angsuranTerbilang = $this->terbilang->terbilang($pdf_data->angsuran);
        $pdf_data->administrasiTerbilang = $this->terbilang->terbilang($pdf_data->administrasi);
        $pdf_data->tenorTerbilang = $this->terbilang->terbilang($pdf_data->tenor);
        $pdf_data->taksiranTerbilang = $this->terbilang->terbilang($pdf_data->up);

        $smPercentTerbilang = explode("%", $rest_open_data['dataAkad']['smPercent']);
        $pdf_data->smPercentTerbilang = $this->terbilang->terbilang($smPercentTerbilang[0]);

        $dendaPercentTerbilang = explode("%", $rest_open_data['dataAkad']['dendaPercent']);
        $pdf_data->dendaPercentTerbilang = $this->terbilang->terbilang($dendaPercentTerbilang[0]);

        $data_kecamatan = $this->master_model->getKelurahan($user->id_kelurahan, "");
        $pdf_data->kelurahan = $data_kecamatan[0]->namaKelurahan;
        $pdf_data->kecamatan = $data_kecamatan[0]->namaKecamatan;
        $pdf_data->kabupaten = $data_kecamatan[0]->namaKabupaten;
        $pdf_data->provinsi = $data_kecamatan[0]->namaProvinsi;
        $pdf_data->taksiran = Pegadaian::currencyIdr(floatval($pdf_data->up));
        $pdf_data->up = Pegadaian::currencyIdr($pdf_data->up);
        $pdf_data->angsuran = Pegadaian::currencyIdr($pdf_data->angsuran);
        $pdf_data->administrasi = Pegadaian::currencyIdr($pdf_data->administrasi);
        $pdf_data->hari = Pegadaian::getDaysInIndonesia();
        $pdf_data->noKredit = $rest_open_data['noKredit'];
        $pdf_data->tglKredit = date('d-m-Y', strtotime($rest_open_data['tglKredit']));
        $pdf_data->tglJatuhTempo = date('d-m-Y', strtotime($rest_open_data['tglJatuhTempo']));
        $date = new DateTime($rest_open_data['tglJatuhTempo']);
        $pdf_data->tglJT = $date->format('j');
        $pdf_data->smPercent = $rest_open_data['dataAkad']['smPercent'];
        $pdf_data->dendaPercent = $rest_open_data['dataAkad']['dendaPercent'];
        
        include "phpqrcode/qrlib.php";
        $url = "{$this->config->item('upload_dir')}/akad_kte/";
        $contentCustomer = array(
            'cif' => $user->cif,
            'noKredit' => $pdf_data->noKredit,
            'timestamp' => date("Y-m-d H:i:s"),
        );

        $pdf_data->fullpathCustomer = Pegadaian::encode_img_base64($this->generateQrDataPdf($contentCustomer, $url));

        $contentPinca = array(
            'nik' => $rest_open_data['dataAkad']['nikPinca'],
            'kodeCabang' => $rest_open_data['dataAkad']['kodeCabang'],
            'noKredit' => $pdf_data->noKredit,
            'timestamp' => date("Y-m-d H:i:s")
        );

        $pdf_data->fullpathPinca = Pegadaian::encode_img_base64($this->generateQrDataPdf($contentPinca, $url));
        
        $this->pdf->setPaper('A4', 'potrait');
        $this->pdf->filename = "{$rest_open_data['noKredit']}.pdf";
        $this->pdf->load_view($url, 'akad_kte_pdf', $pdf_data);

        return $this->config->item('asset_url') . "akad_kte/{$rest_open_data['noKredit']}.pdf";
    }

    private function generateQrDataPdf($content, $qr_img_dir)
    {
        if (!file_exists($qr_img_dir)) {
            mkdir($qr_img_dir);
        }

        $dataQR = json_encode($content);
        $hash_kodeBooking = sha1(rand());
        $qr_img_file = $hash_kodeBooking . ".png";
        QRCode::png($dataQR, $qr_img_dir . $qr_img_file, 'H', 2, 0);

        return "{$this->config->item('upload_dir')}/akad_kte/{$qr_img_file}";
    }

    public function isAlreadyCicilEmas($token)
    {
        $response = $this->response_service->getResponse('');
        $user = $this->user_model->getUser($token->id);

        if (empty($user)) {
            $response['message'] = 'Data user tidak ditemukan';
            return $response;
        }

        $rest_portofolio = $this->rest_switching_portofolio_service->pinjaman(['cif' => $user->cif]);

        if ($rest_portofolio['responseCode'] != '00') {
            $response['data'] = $rest_portofolio;
            return $response;
        }

        $rest_portofolio_data = json_decode($rest_portofolio['data'], true);

        $is_already_cicil_emas = false;
        if (count($rest_portofolio_data) != 0) {
            $is_already_cicil_emas = true;
        }

        $result = [
            "isAlreadyCicilEmas" => $is_already_cicil_emas
        ];

        $response = $this->response_service->getResponse('general.succeeded');
        $response['message'] = 'Check Cicil Emas Berhasil';
        $response['data'] = $result;

        return $response;
    }

    private function krasidaTeNotification($request)
    {
        $data = new stdClass();
        $data->item_perhiasan = $request['itemPerhiasan'];
        $data->total_harga = $request['up'];
        $data->url_pdf = $request['url_pdf'];
        $data->merchant = $this->config->item('merchant_name');

        $title = 'Transaksi Cicil Perhiasan di Merchant Berhasil';

        $params = [];
        $params['phoneNumber'] = $request['noHp'];
        $params['cif'] = $request['cif'];
        $params['emailSubject'] = $title;
        $params['contentTitle'] = $title;
        $params['contentDescription'] = ['Transaksi cicil perhiasan kamu sudah kami terima dengan rincian sebagai berikut.'];
        $params['contentList'] = [
            ['key' => 'Tanggal Pengajuan', 'value' => $request['tglTransaksi']],
            ['key' => 'Nomor Kredit', 'value' => $request['noKredit']],
            ['key' => 'Referensi', 'value' => $request['reffSwitching']],
            ['key' => 'Saldo Emas Blokir', 'value' => $request['gram'] . " gram"],
            ['key' => 'No Rekening', 'value' => $request['noRek']],
            ['key' => 'Tenor', 'value' => $request['tenor'] . " bulan"],
            ['key' => 'Angsuran', 'value' => Pegadaian::currencyIdr($request['angsuran'])],
            ['key' => 'Biaya Administrasi', 'value' => Pegadaian::currencyIdr($request['administrasi'])]
        ];
        $params['type'] = $this->notification_model::TYPE_KRASIDA_TE;
        $params['notificationTitle'] = "Cicil Perhiasan di Merchant";
        $params['notificationDescription'] = "Transaksi cicil perhiasan berhasil";
        $params['additional'] = $this->load->view('mail/krasida_te/additional_krasida_te', ['data' => $data], true);

        $params['data'] = [
            "noRekening" => $request['tab_emas']['norek'],
            "saldoEfektif" => $request['tab_emas']['saldoEfektif'],
            "saldoEmas" => $request['tab_emas']['saldoEmas'],
            "saldoBlokir" => $request['tab_emas']['saldoBlokir']
        ];

        $this->notification_service->globalSendNotification($params, '');

        $this->response['code'] = 200;
        $this->response['status'] = 'Success';
        $this->response['message'] = 'Notification successfully sent';

        return $this->response;
    }

    private function formatListTenor($data)
    {
        $array_map = array_map('intval', array_column($data, 'tenor'));

        array_multisort(
            $array_map,
            SORT_ASC,
            $data
        );

        return $data;
    }

    protected function formatListRiwayat($mutations)
    {
        $array_tglKredit = array_map('strtotime', array_column($mutations, 'tglKredit'));
        $array_noKontrak = array_map('intval', array_column($mutations, 'noKontrak'));

        array_multisort(
            $array_tglKredit,
            SORT_DESC,
            $array_noKontrak,
            SORT_DESC,
            $mutations
        );

        return $mutations;
    }
}
