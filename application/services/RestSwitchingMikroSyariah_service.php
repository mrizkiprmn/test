<?php

use GuzzleHttp\Exception\GuzzleException;

class RestSwitchingMikroSyariah_service extends MY_Service
{
    /**
     * RestSwitchingMikroSyariah_service constructor.
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->load->service('RestSwitching_service', 'rest_switching_service');
    }

    public function mikroSyariahPdsHaji(array $data)
    {
        $url = '/mikrosyariah/pdshaji';
        
        return $this->rest_switching_service->postData($url, $data);
    }
}
