<?php

/**
 * @property RestSwitching_service rest_switching_service
 */

class RestSwitchingMtonline_service extends MY_Service
{
    /**
     * @var
     */
    private $base_path = 'mt-online';

    /**
     * RestSwitchingMtonline_service constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->service('RestSwitching_service', 'rest_switching_service');
    }

    public function inquiryOpen($data)
    {
        $url = "gadai/inquiry-minta-tambah";
        
        return $this->rest_switching_service->postData($url, $data);
    }

    public function open($data)
    {
        $url = "gadai/minta-tambah";

        return $this->rest_switching_service->postData($url, $data);
    }
}
