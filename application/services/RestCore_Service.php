<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\RequestOptions;

/**
 * @property Cache_service cache_service
 */
class RestCore_Service extends MY_Service
{
    // Private @var
    private $rest_core_url_auth;
    private $rest_core_url;
    private $rest_core_grant_type;
    private $rest_core_client_id;
    private $rest_core_client_secret;
    private $channel_id = '6017';
    private $client_id  = '9997';

    public function __construct()
    {
        parent::__construct();

        $this->load->helper('Pegadaian');
        $this->load->service('Cache_service', 'cache_service');

        $this->validationConfig();
    }

    public function postDataAuth($counter = 0)
    {
        if ($counter == 2) {
            Pegadaian::showError('Token Invalid after retry !');
        }

        $client   = $this->authentication();
        $response = json_decode($client->getBody()->getContents());
        $token    = $response->access_token ?? null;
        $expires  = $response->expires_in ?? null;

        if (!empty($token) && !empty($expires)) {
            $token = $this->cache_service->setThenGet($this->cache_service::TOKEN_REST_CORE, $token, $expires);
        }

        return $token;
    }

    public function postData($url, $data, $counter = 0, $channel_id = '', $client_id = '')
    {
        $rest_response = null;

        try {
            if (empty($channel_id)) {
                $data['channelId'] = $this->channel_id;
            }

            if (empty($client_id)) {
                $data['clientId'] = $this->client_id;
            }

            $headers = [
                'Accept'        => 'application/json',
                'Authorization' => 'Bearer ' . $this->token(),
                'Content-Type'  => 'application/json'
            ];

            $rest_response = $this->client($this->rest_core_url, $headers)->post($url, [RequestOptions::JSON => $data]);
        } catch (BadResponseException $e) {
            $rest_response = $e->getResponse();
        } catch (ConnectException $e) {
            $rest_response = $e->getResponse();
        } catch (Exception $e) {
            Pegadaian::showError('Exception RestCore client', $e->getMessage());
        }

        $response = json_decode($rest_response->getBody(), true);

        log_message('debug', 'start of CALL RestCore URL : ' . $url, $data);
        log_message('debug', 'end of CALL RestCore URL : ' . $url, $response);

        $counter++;
        $new_request = $this->refreshToken($rest_response, $url, $data, $counter);

        if ($new_request) {
            return $new_request;
        }

        return $response;
    }

    public function token()
    {
        // get from cache data first
        $key = implode(':', [ $this->cache_service::PREFIX_KEY, $this->cache_service::token_rest_core ]);
        $token = $this->redis->get($key);

        if (!empty($token)) {
            return $token;
        }

        return $this->postDataAuth();
    }

    private function validationConfig()
    {
        $this->rest_core_url_auth = getenv('REST_CORE_URL_AUTH') ?? false;
        $this->rest_core_url = getenv('REST_CORE_URL') ?? false;
        $this->rest_core_grant_type = getenv('REST_CORE_GRANT_TYPE') ?? false;
        $this->rest_core_client_id = getenv('REST_CORE_CLIENT_ID') ?? false;
        $this->rest_core_client_secret = getenv('REST_CORE_CLIENT_SECRET') ?? false;

        if (empty($this->rest_core_url_auth) ||
            empty($this->rest_core_url) ||
            empty($this->rest_core_grant_type) ||
            empty($this->rest_core_client_id) ||
            empty($this->rest_core_client_secret)
        ) {
            Pegadaian::showError('Environment RestCore config not setup !');
        }

        return true;
    }

    private function client($base_url, $headers = [])
    {
        $headers['Content-Type'] = 'application/x-www-form-urlencoded';

        return new Client([
            'base_uri' => $base_url,
            'headers'  => $headers,
            'verify'   => false,
        ]);
    }

    private function refreshToken($rest_response, $url, $data, $counter)
    {
        $response_code = $rest_response->getStatusCode();

        if ($response_code == 403) {
            $this->postDataAuth($counter);
            return $this->postData($url, $data, $counter);
        }

        return false;
    }

    protected function authentication()
    {
        $data = [
            'grant_type'    => $this->rest_core_grant_type,
            'client_id'     => $this->rest_core_client_id,
            'client_secret' => $this->rest_core_client_secret,
        ];

        try {
            return $this->client($this->rest_core_url_auth)->post('token', [
                RequestOptions::FORM_PARAMS => $data
            ]);
        } catch (BadResponseException $exception) {
            return $exception->getResponse();
        }
    }
}
