<?php

/**
 * @property CollateralModel collateral_model
 * @property FinancingModel financing_model
 * @property MasterModel master_model
 * @property User user_model
 * @property RestSwitchingOtp_service rest_switching_otp_service
 * @property Notification_service notification_service
 * @property RestLos_service rest_los_service
 */
class Financing_service extends MY_Service
{
    protected $response;
    private $vehicle_guarantee = 'KN';

    /**
     * Financing_service constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('CollateralModel', 'collateral_model');
        $this->load->model('FinancingModel', 'financing_model');
        $this->load->model('MasterModel', 'master_model');
        $this->load->model('User', 'user_model');
        $this->load->service('RestSwitchingOtp_service', 'rest_switching_otp_service');
        $this->load->service('Notification_service', 'notification_service');
        $this->load->service('RestLos_service', 'rest_los_service');

        $this->response = [
            'code' => 400,
            'status' => 'error',
            'message' => 'Terjadi Kesalahan, silahkan coba lagi.',
            'data' => null
        ];
    }

    public function simulation($request)
    {
        $simulation = $this->getSimulation($request['loan_amount']);
        $simulation_currency = Pegadaian::currencyIdr($simulation);
        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Simulation data';
        $this->response['data'] = $this->getComponentSimulationLoanAmount($simulation_currency);

        return $this->response;
    }

    public function getDataForm($request, $user)
    {
        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Get data successfully';

        $page = $request['page'] ?? '';
        $financing = $this->financing_model->find(['user_id' => $user->id, 'is_sent' => false]);

        switch (true) {
            case $page == 1:
                $this->response['data'] = $this->getFormOne($request);
                break;
            case ($page == 2 && !empty($financing)):
                $this->response['data'] = $this->getFormTwo($financing);
                break;
            case ($page == 4 && !empty($financing)):
                $this->response['data'] = $this->getFormFour($financing);
                break;
            default:
                $this->response['data'] = $financing ?? null;
                break;
        }

        return $this->response;
    }

    public function postDataForm($request, $user)
    {
        $financing = $this->financing_model->find(['user_id' => $user->id, 'is_sent' => false]);
        $page = $request['page'] ?? null;

        switch (true) {
            case $page == 1:
                $financing = $this->storeFromOne($request, $user, $financing);
                break;
            case ($page == 2 && !empty($financing)):
                $financing = $this->storeFormTwo($request, $financing);
                break;
            case ($page == 3 && !empty($financing)):
                $financing = $this->storeFormThree($request, $financing);
                break;
            case (!empty($financing)):
                $financing = $this->storeFormAll($request, $user, $financing);
                break;
            default:
                return $this->response;
                break;
        }

        if (empty($financing)) {
            return $this->response;
        }

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Data successfully saved';
        $this->response['data'] = $financing;

        return $this->response;
    }

    public function listRiwayat($user)
    {
        $query = [
            'user_id' => $user->id,
            'is_sent' => '1',
        ];

        $financing_active = $this->financing_model->getFinancingIsSentActive($query);

        if (empty($financing_active)) {
            $this->response['message'] = 'Data tidak ditemukan!';
            return $this->response;
        }

        $this->response['status'] = 'success';
        $this->response['message'] = 'success get data';
        $this->response['data'] = $financing_active;

        return $this->response;
    }

    public function detail($user, $code_booking)
    {
        $query = [
            'user_id' => $user->id,
            'is_sent' => '1',
        ];

        $this->response['status']  = 'success';
        $this->response['message'] = 'success get data';

        if (!empty($code_booking)) {
            $query['code_booking']  = $code_booking;

            $financing_active = $this->financing_model->getFinancingByCodeBooking($query);
            $financing = $this->getDetailData($financing_active);

            $this->response['data'] = $financing;

            return $this->response;
        }

        $financing_active = $this->financing_model->getFinancingActive($query);

        if (!empty($financing_active)) {
            $financing = $this->getDetailData($financing_active);
            $this->response['data'] = $financing;

            return $this->response;
        }

        $this->response['code']    = 400;
        $this->response['status']  = 'error';
        $this->response['message'] = 'Data tidak ditemukan!';

        return $this->response;
    }

    public function notification($request)
    {
        log_message('debug', 'START ' . __FUNCTION__ . ' => ' . json_encode($request));

        if (empty($request)) {
            $this->response['code'] = 500;
            $this->response['status'] = 'error';
            $this->response['message'] = 'maaf terjadi kesalahan pada server, silahkan coba lagi';

            return $this->response;
        }

        $query = ['code_booking' => $request['code_booking']];
        $dataFinancing = $this->financing_model->find($query);

        if (empty($dataFinancing)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = 'Data dengan code booking tidak ditemukan ' . ' ' . $request['code_booking'];

            return $this->response;
        }

        $dataUser = $this->user_model->getUser($dataFinancing->user_id);

        if (empty($dataUser)) {
            $this->response['code'] = 400;
            $this->response['status'] = 'error';
            $this->response['message'] = 'Data user dengan code booking tidak ditemukan ' . ' ' . $dataFinancing->user_id;

            return $this->response;
        }

        /*
         * Persitence Financing
         */
        if (!$this->updateFinancingFlow($request, $request['statusNotif'])) {
            $this->response['code'] = 500;
            $this->response['status'] = 'error';
            $this->response['message'] = 'Terjadi Kesalahan pada persistence data';

            return $this->response;
        }

        /*
         * Notification
         * statusNotif = 0 isBypass for just update status
         */
        if ($request['statusNotif'] != '0') {
            $this->response = $this->notification_service->losNotification($dataFinancing, $dataUser, $request['statusNotif'], $request);
        }

        /*
        * Auto Link CIF
        */
        if ($request['statusNotif']=='5') {
            $this->user_model->updateUser($dataFinancing->user_id, ['cif'=>$request['cif']]);
        }

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Notification successfully!';

        log_message('debug', 'END ' . __FUNCTION__ . ' => ' . json_encode($request));

        return $this->response;
    }

    public function tracking($code_booking)
    {
        $query     = ['code_booking' => $code_booking];
        $financing = $this->financing_model->find($query);
        $list_status_los = Pegadaian::getStatusLos();

        foreach ($list_status_los as $status_los) {
            $component = $this->getComponentStatusHistoryLos($financing, $status_los);
            if (!empty($component)) {
                $list_status[] = $component;
            }
        }

        $this->response['status']  = 'success';
        $this->response['message'] = 'success get data tracking';
        $this->response['data']    = $list_status ?? [] ;

        return $this->response;
    }

    private function getComponentStatusHistoryLos($financing, $status_los)
    {
        $status_histories = $financing->status_history ?? [];
        $next_statuses    = $financing->request_los ?? [];

        if (!empty($next_statuses)) {
            $next_statuses = json_decode($next_statuses)->next_status ?? [];
        }

        // Get History Status
        $i = 0;
        $len = count($status_histories);
        foreach ($status_histories as $status_history) {
            if ($status_history->code_status == $status_los) {
                $desc = $i == $len - 1 ? $financing->status == '9.3' ? false : true : true;
                return [
                    'code' => $status_los,
                    'title' => Pegadaian::geTitleLos($status_los),
                    'description' => Pegadaian::getDescriptionStatusLos($status_los, $desc),
                    'timestamp' => Pegadaian::formatDateLos($status_history->selesai_diproses ?? $status_history->tanggal_mulai_proses),
                    'status' => 3
                ];
            }
            $i++;
        }

        // Get Current Status
        if ($financing->status == $status_los) {
            return [
                'code' => $status_los,
                'title' => Pegadaian::geTitleLos($status_los),
                'description' => Pegadaian::getDescriptionStatusLos($status_los, $financing->status == '9.3' ? false : true),
                'timestamp' => Pegadaian::formatDateLos($financing->created_at),
                'status' => in_array($financing->status, array_merge(FinancingModel::$status_end, ['1.0'])) ? 3 : 2
            ];
        }

        // Get Next Status
        foreach ($next_statuses as $next_status) {
            if ($next_status->code_status == $status_los) {
                if (!in_array($status_los, FinancingModel::$status_end)) {
                    $desc_status = Pegadaian::getDescriptionStatusLos($status_los);
                }

                return [
                    'code' => $status_los,
                    'title' => Pegadaian::geTitleLos($status_los),
                    'description' => $desc_status ?? '-',
                    'timestamp' => ' ',
                    'status' => 1
                ];
            }
        }

        return [];
    }

    public function setValidationResponse($errors)
    {
        foreach ($errors as $key => $error) {
            $index = null;
            $panel = null;
            $field = $key;
            $arr   = explode('.', $key);

            if ($arr[0] == 'collateral_type') {
                $panel = 'collaterals';
                $field = 'collateral_type';
                $index = $arr[1];
            }

            if ($arr[0] == 'market_price') {
                $panel = 'collaterals';
                $field = 'market_price';
                $index = $arr[1];
            }

            if ($arr[0] == 'type') {
                $panel = 'collaterals';
                $field = 'type';
                $index = $arr[1];
            }

            if ($arr[0] == 'id') {
                $panel = 'collaterals';
                $field = 'id';
                $index = $arr[1];
            }

            if ($arr[0] == 'vehicle_number') {
                $panel = 'collaterals';
                $field = 'vehicle_number';
                $index = $arr[1];
            }

            if ($arr[0] == 'production_year') {
                $panel = 'collaterals';
                $field = 'production_year';
                $index = $arr[1];
            }

            $arr_validations = ['field' => $field, 'message' => $error];

            if ($index !== null) {
                $arr_validations['panel'] = $panel;
                $arr_validations['index'] = $index;
            }

            $err_validations[] = $arr_validations;
        }

        return $err_validations ?? [];
    }

    protected function getCollateralTypeDetail($collateral_type)
    {
        $collateral_types = [];
        $list_vehicle_guarantee = [];
        $transportation_type = [];

        if ($collateral_type == $this->vehicle_guarantee) {
            $list_vehicle_guarantee = $this->rest_los_service->listVehicleGuarantee();
        }

        foreach ($list_vehicle_guarantee as $vehicle_guarantee) {
            $transportation_type[] = ['label' => $vehicle_guarantee->dataDesc, 'value' => $vehicle_guarantee->dataId];
        }

        if (!empty($transportation_type)) {
            $collateral_types[] = $this->getComponentTransportationType($transportation_type);
        }

        $collateral_types[] = $this->getComponentMarketPlace();

        return $collateral_types;
    }

    protected function getFormOne($request = [])
    {
        $data  = [];
        $param = $request['param'] ?? null;
        $count_collateral = $request['count_collateral'] ?? 0;
        $rule_collateral  = (int) $count_collateral < 2;

        if ($param === 'true') {
            $rule_collateral = false;
        }

        $data['hint_simulation_loan_amount'] = [
            'name'  => 'hint_simulation_loan_amount',
            'value' => 'Merupakan estimasi uang pinjaman berdasarkan perkiraan nilai agunan.'
        ];

        $data['is_qualified_collateral'] = [
            'name'  => 'is_qualified_collateral',
            'value' => $rule_collateral,
            'reset' => $param === 'true'
        ];

        $data['loan_amount'] = $this->getComponentLoanAmount();

        switch (true) {
            case $param == 'init':
                $collateral_types = $this->rest_los_service->listGuarantee();
                break;
            case $param == 'additional':
                $collateral_types = $this->rest_los_service->listGuarantee('jaminanTambahan');
                break;
            default:
                $data['components'] = $this->getCollateralTypeDetail($param);
        }

        if (!empty($collateral_types)) {
            foreach ($collateral_types as $item) {
                $data_collateral[] = ['label' => $item->dataDesc, 'value' => $item->dataId];
            }
            $data['components'][] = $this->getComponentCollateralType($data_collateral ?? [], $count_collateral);
        }

        if ($param == '' || $param === 'true') {
            $data['components'] = [];
        }

        return $data;
    }

    protected function getFormTwo($financing)
    {
        $tenors = $this->getLabelTenors($financing);

        if (empty($tenors)) {
             $this->response['status'] = 'error';
             $this->response['message'] = 'Angsuran dari los tidak tersedia';

            return null;
        }

        $simulation = $this->getSimulation($financing->loan_amount);
        $simulation_currency = Pegadaian::currencyIdr($simulation);

        return [
            'tenors' => $this->getLabelTenors($financing),
            'collaterals' => $this->getLabelCollaterals($financing->collaterals),
            'simulation' => $this->getComponentSimulationLoanAmount($simulation_currency)
        ];
    }

    protected function getFormFour($financing)
    {
        return [
            'details' => $this->getLabelDetailFinancings($financing),
            'collaterals' => $this->getLabelDetailCollaterals($financing->collaterals)
        ];
    }

    protected function storeFromOne($request, $user, $financing)
    {
        $data_financing = [
            'user_id' => $user->id,
            'loan_amount' => $request['loan_amount'] ?? null,
            'is_have_business' => $request['is_have_business'] ?? null,
            'status' => $request['status'] ?? null,
        ];

        $financing = $this->storeForm($request, $financing, $data_financing);

        $rest_body = [
            'tenor' => $financing->tenor ?? '1',
            'loan_amount' => $financing->loan_amount,
            'is_have_business' => $financing->is_have_business,
            'is_collateral' => empty($financing->collaterals) ? '0' : '1'
        ];

        $rest_lost = $this->rest_los_service->matrixProduct($rest_body);

        if (empty($rest_lost)) {
            return null;
        }

        return $this->storeForm($request, $financing, ['code_product' => $rest_lost[0]->productid ?? null]);
    }

    protected function storeFormTwo($request, $financing)
    {
        $selected_tenor = $request['tenor'];
        $list_tenor = $this->rest_los_service->simulationCredit([
            'code_product' => $financing->code_product,
            'loan_amount' => $financing->loan_amount
        ]);


        foreach ($list_tenor as $tenor) {
            if ($tenor->tenorId == $selected_tenor) {
                $installment = $tenor->installment;
                break;
            }
        }

        $data_financing = [
            'tenor' => $request['tenor'],
            'installment' => $installment ?? null
        ];

        $financing = $this->financing_model->updateByID($financing->id, $data_financing);

        if (!empty($request['collaterals'])) {
            $this->storeCollateral($request, $financing);
        }

        return $financing;
    }

    protected function storeFormThree($request, $financing)
    {
        $data_financing = ['code_outlet' => $request['code_outlet']];

        return $this->financing_model->updateByID($financing->id, $data_financing);
    }

    protected function storeFormAll($request, $user, $financing)
    {
        $code_promo = $request['code_promo'] ?? '';

        $body[] = [
            'content' => [
                'nik'              => $user->noKTP ?? '',
                'alamat'           => $user->alamat ?? '',
                'kelurahan'        => $user->idKelurahan ?? '',
                'cif'              => $user->cif ?? '',
                'nohp'             => $user->noHP ?? '',
                'createby'         => $user->nama,
                'kodeoutlet'       => $financing->code_outlet ?? '',
                'up'               => $financing->loan_amount ?? '',
                'productId'        => $financing->product_id ?? '03',
                'tenor'            => $financing->tenor ?? '',
                'kepemilikanusaha' => $financing->is_have_business ?? '',
                'branchid'         => $financing->code_outlet ?? '',
                'kodepromo'        => strtoupper($code_promo)
            ],
            'table' => [
                'tablenm' => 'pengajuan'
            ]
        ];

        foreach ($financing->collaterals as $key => $collateral) {
            $table_name = $key == 0 ? 'jaminanUtama' : 'jaminanTambahan' . $key;
            $detail = $collateral->detail ?? null;
            $body[] = [
                'content' => [
                    'tipejaminan' => !empty($detail->collateral_type) ? $detail->collateral_type : $detail->collateral_type ?? '',
                    'hargapasar'  => $detail->market_price ?? '',
                    'nopolisi'    => $detail->vehicle_number ?? '',
                    'tahun'       => $detail->production_year ?? '',
                ],
                'table' => ['tablenm' => $table_name]
            ];
        }

        $rest_los = $this->rest_los_service->inquiry($body);

        if (empty($rest_los)) {
            return null;
        }

        $rest_los_encode = json_encode($rest_los);

        $data_financing = [
            'response_los' => $rest_los_encode,
            'status'       => $rest_los->status->code_status ?? '',
            'status_desc'  => $rest_los->status->desc_status ?? '',
            'is_sent'      => 1,
            'code_booking' => $rest_los->code_booking ?? '',
            'code_promo'   => $code_promo,
            'request_los'  => $rest_los_encode,
        ];

        $financing = $this->financing_model->updateByID($financing->id, $data_financing);

        $data = [
            'statusNotif' => '1',
            'tenor'       => $financing->tenor ?? '',
            'up'          => $financing->loan_amount ?? '',
            'angsuran'    => $financing->installment ?? '',
            'cif'         => $user->cif ?? '',
        ];

        $data = array_merge($data, json_decode($rest_los_encode, true));
        $dataUser = $this->user_model->getUser($financing->user_id);

        $this->response = $this->notification_service->losNotification($financing, $dataUser, '1', $data);

        return $financing;
    }

    private function storeForm($request, $financing, $data_financing)
    {
        if (!empty($financing)) {
            $financing = $this->financing_model->updateByID($financing->id, $data_financing);
        } else {
            $financing = $this->financing_model->insert($data_financing);
        }

        if (!empty($request['collaterals'])) {
            $this->storeCollateral($request, $financing);
        }

        return $this->financing_model->find(['id' => $financing->id]);
    }

    private function storeCollateral($request, $financing)
    {
        $collaterals = $request['collaterals'];

        if (!empty($collaterals[0]['id'])) {
            return $this->updateCollateral($collaterals);
        }

        $this->collateral_model->dropByFinancingID($financing->id);

        foreach ($collaterals as $collateral) {
            if ($collateral['collateral_type'] === 'true') {
                break;
            }

            $collateral_data = $collateral;
            $collateral_data['collateral_type'] = !empty($collateral_data['type']) ? $collateral_data['type'] : $collateral_data['collateral_type'];
            unset($collateral_data['type']);
            $data_collateral = [
                'financing_id' => $financing->id,
                'type' => $collateral['collateral_type'],
                'detail' => $collateral_data,
            ];

            $this->collateral_model->insert($data_collateral);
        }

        return true;
    }

    private function updateCollateral($collaterals)
    {
        foreach ($collaterals as $key => $collateral) {
            $collateral_data = $this->collateral_model->find(['id' => $collateral['id']]);
            unset($collateral['id']);

            if (empty($collateral_data)) {
                return null;
            }

            $detail = $collateral_data->detail;
            $detail->vehicle_number  = $collateral['vehicle_number'] ?? '';
            $detail->production_year = $collateral['production_year'] ?? '';
            $detail = json_encode($detail);

            $this->collateral_model->updateByID($collateral_data->id, ['detail' => $detail]);
        }
    }

    private function getLabelTenors($financing)
    {
        $rest_los_tenor = $this->rest_los_service->simulationCredit([
            'code_product' => $financing->code_product,
            'loan_amount' => $financing->loan_amount
        ]);

        foreach ($rest_los_tenor as $item) {
            if (empty($item->installment)) {
                continue;
            }

            $tenors[] = [
                'label' => $item->tenorDesc ?? '',
                'name' => 'tenor',
                'value' => $item->tenorId ?? '',
                'installment' => $item->installment,
                'label_installment' => Pegadaian::currencyIdr($item->installment) . '/bulan'
            ];
        }

        return $tenors ?? [];
    }

    private function getLabelCollaterals($collateral)
    {
        $list_guarantee = $this->rest_los_service->listVehicleGuarantee();

        foreach ($collateral as $item) {
            if ($item->type == $this->vehicle_guarantee) {
                $components = $this->getComponentCollaterals($item);

                $data[] = [
                    'label' => $this->setLabelCollateral($item->detail->collateral_type, $list_guarantee),
                    'id' => $item->id ?? '',
                    'market_place_label' => Pegadaian::currencyIdr($item->detail->market_price) ?? '',
                    'components' => $components ?? []
                ];
            }
        }

        return $data ?? [];
    }

    private function getComponentCollaterals($collateral)
    {
        $components = [
            $this->getComponentVehicleNumber($collateral->detail->vehicle_number ?? ''),
            $this->getComponentProductionYear($collateral->detail->production_year ?? '')
        ];

        if ($collateral->detail->collateral_type == 'KNMB') {
            $components = [
                $this->getComponentProductionYear($collateral->detail->production_year ?? '')
            ];
        }

        return $components;
    }

    private function getLabelDetailFinancings($financing)
    {
        $outlet = $this->master_model->getSingleCabang($financing->code_outlet);

        return [
            ['label' => 'Pengajuan Pinjaman', 'text' => Pegadaian::currencyIdr($financing->loan_amount)],
            ['label' => 'Tenor', 'text' => "$financing->tenor bulan"],
            ['label' => 'Angsuran', 'text' => Pegadaian::currencyIdr($financing->installment)],
            ['label' => 'Outlet Pencairan', 'text' => $outlet->nama ?? '']
        ];
    }

    private function getLabelDetailCollaterals($collaterals)
    {
        $list_guarantee = $this->rest_los_service->listGuarantee('jaminanTambahan');

        foreach ($collaterals as $collateral) {
            $collateral_detail = $collateral->detail;
            if ($collateral->type == $this->vehicle_guarantee) {
                $vehicle_guarantee = $this->rest_los_service->listVehicleGuarantee();
                $items = [
                    ['label' => 'Nomor Polisi', 'text' => $collateral_detail->vehicle_number ?? ''],
                    ['label' => 'Tahun Pembuatan', 'text' => $collateral_detail->production_year ?? ''],
                ];
            }

            $data[] = [
                'title' => $this->setLabelCollateral($collateral_detail->collateral_type, $vehicle_guarantee ?? $list_guarantee),
                'contents' => $items ?? [],
            ];

            $items = [];
            $vehicle_guarantee = null;
        }

        return $data ?? [];
    }

    private function setLabelCollateral($collateral_detail, $los_collateral)
    {
        foreach ($los_collateral as $detail) {
            if ($detail->dataId == $collateral_detail) {
                return $detail->dataDesc;
            }
        }

        return '';
    }

    private function getComponentCollateralType($options, $count)
    {
        $default_options[] = ['label' => 'Pilih Jenis Agunan', 'value' => ''];

        if ($count < 1) {
            $default_options[] = ['label' => 'Tidak Ada Agunan', 'value' => 'true'];
        }

        $options = array_merge($default_options, $options);

        return [
            'label' => 'Jenis Agunan',
            'name' => 'collateral_type',
            'type' => 'dropdown',
            'value' => '',
            'error_message' => '',
            'options' => $options
        ];
    }

    private function getComponentMarketPlace()
    {
        return [
            'label' => 'Perkiraan Harga Pasar Agunan',
            'name' => 'market_price',
            'type' => 'money',
            'value' => '',
            'error_message' => '',
            'hint' => 'Rp. 0',
            'length' => 16
        ];
    }

    private function getComponentVehicleNumber($value)
    {
        return [
            'label' => 'Nomor Polisi',
            'name' => 'vehicle_number',
            'type' => 'text',
            'value' => $value,
            'error_message' => '',
            'length' => 11
        ];
    }

    private function getComponentProductionYear($value)
    {
        return [
            'label' => 'Tahun Pembuatan',
            'name' => 'production_year',
            'type' => 'numeric',
            'value' => $value,
            'error_message' => '',
            'length' => 4
        ];
    }

    private function getComponentTransportationType($options)
    {
        $default_options[] = ['label' => 'Pilih Jenis Kendaraan', 'value' => ''];

        $options = array_merge($default_options, $options);

        return [
            'label' => 'Jenis Kendaraan',
            'name' => 'type',
            'type' => 'dropdown',
            'value' => '',
            'error_message' => '',
            'options' => $options
        ];
    }

    private function getComponentSimulationLoanAmount($simulation_loan_amount)
    {
        return [
            'label' => 'Perkiraan Uang Pinjaman',
            'name' => 'simulation_loan_amount',
            'type' => 'money',
            'value' => $simulation_loan_amount ?? '',
            'error_message' => '',
            'length' => 11
        ];
    }

    private function getComponentLoanAmount()
    {
        return [
            'label' => 'Rupiah',
            'name' => 'loan_amount',
            'type' => 'money',
            'value' => '',
            'error_message' => '',
            'hint' => 'Input Rupiah',
            'length' => 11
        ];
    }

    private function updateFinancingFlow($data, $statusNotif)
    {
        $financing = $this->financing_model->find(['code_booking' => $data['code_booking']]);

        $flow_financing = [
            'status' => $data['status']['code_status'],
            'status_history' => json_encode($data['status_history']),
            'status_desc' => $data['status']['desc_status'],
            'pic_current_status'=>$data['status']['pic'],
            'request_los' => json_encode($data),
            'update_at' => date('Y-m-d H:i:s')
        ];

        if ($statusNotif == '5') {
            $flow_financing = [
                'code_outlet' => $data['code_outlet'],
                'loan_amount' => $data['up_approval'] ?? $data['up'],
                'installment' => $data['angsuran'],
                'tenor' => $data['tenor'],
                'status' => $data['status']['code_status'],
                'status_desc' => $data['status']['desc_status'],
                'pic_current_status'=>$data['status']['pic'],
                'status_history' => json_encode($data['status_history']),
                'request_los' => json_encode($data),
                'update_at' => date('Y-m-d H:i:s'),
            ];

            if (!empty($financing)) {
                $this->updateFinancingCollateralFlow($data, $financing);
                $financing = $this->financing_model->find(['code_booking' => $data['code_booking']]);
            }
        }

        return $this->financing_model->updateByID($financing->id, $flow_financing);
    }

    private function updateFinancingCollateralFlow($data, $financing)
    {
        $collaterals['collaterals'] = [];
        $data_collaterals = $data['collateral'] ?? [];

        foreach ($data_collaterals as $collateral) {
            $detail = [
                'collateral_type' => $collateral['rubrikid'],
                'type' => $collateral['tipejaminan'],
                'market_price' => $collateral['harga_pasar'],
            ];

            if ($collateral['rubrikid'] == 'KN') {
                $detail['vehicle_number'] = $collateral['no_polisi'];
                $detail['production_year'] = $collateral['thn_pembuatan'];
            }

            $collaterals['collaterals'][] = $detail;
        }

        $this->storeCollateral($collaterals, $financing);
    }

    private function getDetailData($financing)
    {
        $outlet              = $this->master_model->getSingleCabang($financing->code_outlet);
        $financing_data      = $this->getFormFour($financing);
        $status_notification = $financing->request_los->statusNotif ?? null;
        $status_histories    = $financing->request_los->status_history ?? [];

        $financing_data['code_booking'] = $financing->code_booking;

        $financing_data['outlet'] = [
            'value'   => $outlet->nama ?? '-',
            'address' => $outlet->alamat ?? '-',
            'phone'   => $outlet->telepon ?? '-'
        ];

        $status = $financing->status != '9.0' ? $financing->status : '8.0';

        if ($financing->status == '9.3') {
            foreach ($status_histories as $status_history) {
                $status = $status_history->code_status;
            }
        }

        $financing_data['status'] = [
            'value'       => Pegadaian::geTitleLos($status),
            'description' => Pegadaian::getDescriptionStatusLos($status, $financing->status == '9.3' ? false : true),
            'timestamp'   => Pegadaian::formatDateLos($financing->created_at),
        ];

        $result[] = [
            'text'  => 'Pengajuan telah disetujui. Lakukan pencairan pinjaman di outlet pencairan.',
            'screen' => ''
        ];

        if ($status_notification != '5') {
            $result = [
                [
                    'text' => 'Pengajuan belum disetujui.',
                    'screen' => ''
                ],
                [
                    'text' => 'Lakukan pengajuan ulang.',
                    'screen' => 'Pembiayaan'
                ]
            ];
        }

        if (in_array($status_notification, FinancingModel::$status_end_notification)) {
            $financing_data['results'] = $result ?? [];
        }

        $financing_data['collaterals'] = empty($financing_data['collaterals']) ? 'Tidak Ada Data Agunan' : $financing_data['collaterals'];

        ksort($financing_data);

        return $financing_data;
    }

    private function getSimulation($value)
    {
        $simulation = (70 / 100) * $value;

        return (int) round($simulation);
    }
}
