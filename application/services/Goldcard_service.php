<?php

use GuzzleHttp\Exception\GuzzleException;

/**
 * @property GoldcardAPI_service goldcard_api_service
 * @property Cache_service cache_service
 * @property RestSwitchingCustomer_service rest_switching_customer_service
 * @property RestSwitchingGoldcard_service rest_switching_goldcard_service
 * @property Payment_service payment_service
 * @property Response_service response_service
 * @property Notification_service notification_service
 */
class Goldcard_service extends MY_Service
{
    // Variable Constant
    private const MinimumSaldoEfektif = 0.1000;
    private const ConstPlafonLimit = 0.8;
    private const ConstTaksiranEmas = 0.94;

    protected $response;

    public function __construct()
    {
        parent::__construct();
        // init load model
        $this->load->model('BankModel', 'bank_model');
        $this->load->model('User', 'user_model');
        $this->load->model('ConfigModel', 'config_model');

        $this->load->service('GoldcardAPI_service', 'goldcard_api_service');
        $this->load->service('Cache_service', 'cache_service');
        $this->load->service('RestSwitchingCustomer_service', 'rest_switching_customer_service');
        $this->load->service('RestSwitchingGoldcard_service', 'rest_switching_goldcard_service');
        $this->load->service('Payment_service', 'payment_service');
        $this->load->service('Response_service', 'response_service');
        $this->load->service('Notification_service', 'notification_service');

        $this->response = [
            'code'    => 101,
            'status'  => 'error',
            'message' => 'Internal Server Error',
            'data'    => null
        ];
    }

    public function requirementUmur($data_goldcard, $umur, $umur_menikah)
    {
        $date = new DateTime($data_goldcard->tglLahir);
        $now = new DateTime();
        $tahun_umur = $now->diff($date);

        // Jika umur lebih dari 21
        if ($tahun_umur->y >= $umur) {
            return true;
        }

        // Jika umur lebih dari 17 dan sudah menikah
        if ($tahun_umur->y >= $umur_menikah && $data_goldcard->statusKawin == '1') {
            return true;
        }

        return false;
    }

    public function isKYC($data_goldcard, $requirement)
    {
        $kyc = $data_goldcard->isKYC;
        switch ($kyc) {
            case $requirement:
                $data = true;
                break;
            default:
                $data = false;
        }

        return $data;
    }

    public function isAktifasiFinansial($data_goldcard, $requirement)
    {
        $aktivasiFinansial = $data_goldcard->aktifasiTransFinansial;
        switch ($aktivasiFinansial) {
            case $requirement:
                $data = true;
                break;
            default:
                $data = false;
        }

        return $data;
    }

    public function isOpenTE($data_goldcard, $requirement)
    {
        $openTE = $data_goldcard->isOpenTe;
        switch ($openTE) {
            case $requirement:
                $data = true;
                break;
            default:
                $data = false;
        }

        return $data;
    }

    public function getRequirements()
    {
        $url = '/product/requirements';
        $cache_key = implode(':', [ $this->cache_service::PREFIX_KEY, $this->cache_service::GC_REQUIREMENTS ]);
        // get from cache data first
        $requirements = json_decode($this->redis->get($cache_key) ?? '');

        if (empty($requirements)) {
            $this->response = $this->goldcard_api_service->postData($url, [], '', 'GET');
            $requirements = $this->cache_service->setThenGet($this->cache_service::GC_REQUIREMENTS, json_encode($this->response['data']));
            $requirements = json_decode($requirements, true);
        }

        return $requirements;
    }

    public function isRegisterGTE($data_goldcard, $requirement)
    {
        $request = ([
            'cif' => $data_goldcard->cif
        ]);

        $isRegisterGTE = $this->rest_switching_goldcard_service->isRegisterGte((array) $request);

        if (empty($isRegisterGTE) || $isRegisterGTE['responseCode'] != "00") {
            return false;
        }
        $status = json_decode($isRegisterGTE['data'])->isRegisterGte ? 1 : 0;
        if ($status != $requirement) {
            return false;
        }
        return true;
    }

    public function messageGoldcard($type_validation)
    {
        $message = [];
        $message["message_header"] = null;
        $message["message_content"] = null;
        $message["labelTop"] = null;
        $message["labelBottom"] = null;
        $message["buttonTop"] = null;
        $message["buttonBottom"] = null;
        $function_name = explode(".", $type_validation)[0];
        switch ($function_name) {
            case 'ajukan':
                $message["screen_name"] = "Goldcard";
                $message["message_content"] = "Status pengajuan data valid";
                $message["labelTop"] = "Oke";
                $message["labelBottom"] = "";
                break;
            case 'ajukan_limit':
                $message["screen_name"] = "GoldCardPengajuanLimit";
                break;
            case 'pilih_te':
                $message["screen_name"] = "GoldCardPilihRekening";
                break;
            default:
                $message["screen_name"] = "Goldcard";
        }

        switch ($type_validation) {
            case 'ajukan.umur':
                $message["message_header"] = "Usia Belum Mencukupi";
                $message["message_content"] = "Kartu Emas hanya dapat diajukan setelah kamu berusia 21 tahun atau 17 tahun jika sudah menikah.";
                break;
            case 'ajukan.kyc':
                $message["message_header"] = "Belum Verifikasi Data";
                $message["message_content"] = "Lakukan proses verifikasi data di cabang Pegadaian terdekat.";
                break;
            case 'ajukan.aktivasifinansial':
                $message["message_header"] = "Upgrade ke Akun Premium";
                $message["message_content"] = "Gunakan fitur ini dan berbagai penawaran menarik dengan upgrade akun kamu melalui aplikasi atau cabang terdekat.";
                $message["labelTop"] = "Lihat Cabang Terdekat";
                $message["labelBottom"] = "Upgrade Sekarang";
                $message["buttonTop"] = "CabangPegadaian";
                break;
            case 'ajukan.opente':
                $message["message_header"] = "Buka Tabungan Emas";
                $message["message_content"] = "Kamu harus memiliki tabungan emas untuk melanjutkan transaksi.";
                $message["labelTop"] = "Nanti Saja";
                $message["labelBottom"] = "Buka Tabungan Emas";
                $message["buttonBottom"] = "BukaTabunganEmasWizard";
                break;
            case 'ajukan.registrasigte':
                $message["message_header"] = "Belum Registrasi Gadai";
                $message["message_content"] = "Registrasi gadai tabungan emas kamu di cabang terdekat untuk dapat menikmati fitur ini.";
                break;
            case 'ajukan.sukses':
                $message['screen_name'] = "GoldCardPilihRekening";
                break;
            case 'ajukan.sukses.prosesPengajuanLimit':
                $message['screen_name'] = "GoldCardPengajuanLimit";
                break;
            case 'ajukan.sukses.prosesInputDataDiri':
                $message['screen_name'] = "GoldCardInputDataDiri";
                break;
            case 'ajukan.sukses.prosesInputDataPekerjaan':
                $message['screen_name'] = "GoldCardDataPekerjaan";
                break;
            case 'ajukan.sukses.prosesInputDataPengirimanKartu':
                $message['screen_name'] = "GoldCardPengirimanKartu";
                break;
            case 'ajukan.sukses.prosesStatusPengajuan':
                $message['screen_name'] = "GoldCardStatusPengajuan";
                break;
            case 'ajukan.sukses.prosesAktif':
                $message['screen_name'] = "AktivasiGoldCardBerhasil";
                $message['message_header'] = "Aktivasi Kartu Emas Berhasil";
                $message['message_content'] = "Selamat! Aktivasi kartu emas berhasil. Ubah PIN kamu untuk mulai bertransaksi di berbagai merchant pilihan.";
                break;
            case 'ajukan.sukses.prosesDitolak':
                $message['screen_name'] = "PengajuanGoldCardGagal";
                $message['message_header'] = "Pengajuan Gold Card Belum Berhasil";
                $message['message_content'] = "Data yang kamu masukkan tidak sesuai. Mohon lakukan pengajuan kembali menggunakan data yang sesuai.";
                break;
            case 'ajukan_limit.goldcardError':
                $message["screen_name"] = "GoldCard";
                $message['message_header'] = "Pengajuan kartu emas sudah dilakukan sebelumnya";
                break;
            case 'ajukan_limit.DividedBy10Thousand':
                $message["message_content"] = "Pengajuan limit harus memiliki kelipatan Rp 10.000";
                break;
            case 'ajukan_limit.MaxLimitGoldcard':
                $message["message_content"] = "Pengajuan limit maksimal Rp 999.990.000";
                break;
            case 'ajukan_limit.MinLimitGoldcard':
                $message["message_content"] = "Pengajuan limit minimal Rp 3.000.000";
                break;
            case 'ajukan_limit.InsufficientBalance':
                $message["message_content"] = "Saldo efektif kamu tidak mencukupi";
                break;
            case 'ajukan_limit.MinSaldoEfektif':
                $message["message_content"] = "Sisa saldo efektif minimal 0,1 gram";
                break;
            case 'ajukan_limit.TENotFound':
                $message["message_content"] = "Tabungan emas tidak ada";
                break;
            case 'ajukan_limit.sukses':
                $message['screen_name'] = "GoldCardFormDataDiri";
                $message["message_content"] = "Status pengajuan limit valid";
                break;
            case 'gold_card_submission.mismatchData':
                $message['message_header'] = 'Data Tidak Sesuai';
                $message['message_content'] = 'Periksa kembali data kamu. Kamu juga dapat memperbarui data melalui aplikasi atau cabang terdekat.';
                $message['screen_name'] = "GoldCardFormDataDiri";
                $message['gold_card_submission_validation'] = "false";
                break;
            case 'gold_card_submission.sukses':
                $message['message_content'] = 'Berhasil menyimpan data.';
                $message['screen_name'] = "GoldCardPengirimanKartu";
                $message['gold_card_submission_validation'] = "true";
                break;
            case 'gold_card_switching_getCustomerData.gagal':
                $message['message_content'] = 'Terjadi kesalahan. Gagal mengambil data.';
                break;
            case 'pilih_te.TENotFound':
                $message["message_content"] = "Tabungan emas tidak ada";
                break;
            case 'pilih_te.MinSaldoTE':
                $message["message_content"] = "Saldo efektif tabungan emas tidak mencukupi";
                break;
            case 'pilih_te.sukses':
                $message['screen_name'] = "GoldCardPengajuanLimit";
                $message["message_content"] = "Status pengajuan data valid";
                break;
            case 'cek_saldo_te.TENotFound':
                $message["message_content"] = "Tabungan emas tidak ditemukan";
                break;
            case 'list_te.sukses':
                $message['screen_name'] = "GoldCardPengajuanLimit";
                $message["message_content"] = "Status pengajuan data valid";
                break;
            case 'list_te.ErrorServer':
                $message['screen_name'] = "GoldCardPengajuanLimit";
                $message["message_content"] = "Terjadi kesalahan, mohon coba beberapa saat lagi";
                break;
            case 'list_te.TENotFound':
                $message["message_content"] = "Tabungan emas tidak ada";
                break;
            case 'get_application_number.error':
                $message["message_header"] = "Get Application Number";
                $message["message_content"] = "Terjadi kesalahan, mohon coba beberapa saat lagi.";
                break;
            case 'aktivasi_kartu.validasiTanggal':
                $message["code"] = 33;
                $message["message_header"] = "Error";
                $message["message_content"] = "Tanggal tidak valid";
                break;
            case 'aktivasi_kartu.dataSalah':
                $message["code"] = 15;
                $message['message_header'] = 'Data Tidak Sesuai';
                $message['message_content'] = 'Periksa data kartu kamu, lalu coba kembali.';
                break;
            case 'aktivasi_kartu.validasiBirthDate':
                $message["code"] = 11;
                $message["message_header"] = "Error";
                $message["message_content"] = "Tanggal lahir yang dimasukkan tidak sesuai.";
                break;
            case 'aktivasi_kartu.gagal':
                $message["code"] = 99;
                $message['message_header'] = 'Terjadi Kesalahan';
                $message['message_content'] = 'Terjadi kesalahan. Gagal aktivasi kartu.';
                break;
            case 'aktivasi_kartu.updateAccountNumberGagal':
                $message["code"] = 99;
                $message["message"] = "Terjadi kesalahan ketika menyimpan Goldcard Account Number";
                break;
            case 'data_pekerjaan.sukses':
                $message["message_content"] = "Berhasil menyimpan data.";
                break;
            case 'gc_hist_transaksi.sukses':
                $message['message_content'] = 'Get data transaksi sukses';
                break;
            case 'gc_hist_transaksi.gagal':
                $message['message_content'] = 'Get data transaksi gagal';
                break;
            case 'cek_saldo.notFound':
                $message['message_content'] = 'Tidak dapat menemukan akun kartu emas anda';
                break;
            case 'cetak_tagihan.notFound':
                $message['message_content'] = 'Tidak dapat menemukan cetak tagihan kartu emas anda';
                break;
            case 'check_tagihan_gte.tagihanFound':
                $message["status"] = "success";
                $message["message"] = "Tagihan ditemukan.";
                break;
            case 'check_tagihan_gte.tagihanNotFound':
                $message["status"] = "error";
                $message["message"] = "Tagihan tidak ditemukan.";
                break;
            case 'check_tagihan_gte.norekNotFound':
                $message["status"] = "error";
                $message["message"] = "No rekening tidak ditemukan.";
                break;
            case 'get_saving_account.error':
                $message = [];
                $message["status"] = "error";
                $message["message"] = "Terjadi kesalahan";
                break;
            case 'get_saving_account.success':
                $message = [];
                $message["status"] = "success";
                $message["message"] = "Data berhasil dikirim";
                break;
            default:
                $message = "validasi gagal";
        }
        return $message;
    }

    public function setAlamatPengiriman($applicationNumber, $data_goldcard)
    {
        $url = '/registrations/address';

        log_message('debug', __FUNCTION__ . " : Set alamat pengiriman Goldcard: " . json_encode($data_goldcard));
        $data = [
            "applicationNumber" => $applicationNumber,
            "cardDeliver" => (int) $data_goldcard["cardDeliver"],
        ];

        $response = $this->goldcard_api_service->postData($url, $data, 'json', 'POST');
        return $response;
    }

    public function getAlamatPengiriman($applicationNumber)
    {
        $url = '/registrations/address';

        log_message('debug', __FUNCTION__ . " : Get alamat pengiriman Goldcard: " . json_encode($applicationNumber));
        $data = [
            "applicationNumber" => $applicationNumber
        ];

        $response = $this->goldcard_api_service->postData($url, $data, 'json', 'GET');
        if ($response['code'] != "00") {
            return $response;
        }

        $cardDeliverData = $response['data']['cardDeliver'];
        $kantorData = $response['data']['office'];
        $domisiliData = $response['data']['domicile'];

        // set returned address value
        $response['data'] = [
            "cardDeliver" => $cardDeliverData,
            "kantor" => [
                "alamat" => trim("{$kantorData['addressLine1']} {$kantorData['addressLine2']} {$kantorData['addressLine3']}", " "),
                "provinsi" => $kantorData['province'],
                "kota" => $kantorData['city'],
                "kecamatan" => $kantorData['subdistrict'],
                "kelurahan" => $kantorData['village'],
                "kodePos" => $kantorData['zipcode']
            ],
            "domisili" => [
                "alamat" => trim("{$domisiliData['addressLine1']} {$domisiliData['addressLine2']} {$domisiliData['addressLine3']}", " "),
                "provinsi" => $domisiliData['province'],
                "kota" => $domisiliData['city'],
                "kecamatan" => $domisiliData['subdistrict'],
                "kelurahan" => $domisiliData['village'],
                "kodePos" => $domisiliData['zipcode']               
            ],
        ];

        return $response;
    }

    public function DividedBy10Thousand($nominal_limit)
    {
        if ((int) $nominal_limit % 10000 != 0) {
            return false;
        }
        return true;
    }

    public function MaxLimitGoldcard($nominal_limit, $requirement)
    {
        if ($nominal_limit > $requirement) {
            return false;
        }
        return true;
    }

    public function MinLimitGoldcard($nominal_limit, $requirement)
    {
        if ($nominal_limit < $requirement) {
            return false;
        }
        return true;
    }

    public function cekSaldoEfektif($nominal_limit, $harga_emas, $saldo_efektif)
    {
        // besaran plafon limit adalah 80% dari harga taksiran emas sebesar 94% dari stl
        // harga emas dikalikan 100 karena besaran stl merupakan perhitungan harga emas per 0.01 gram
        $plafon_limit = self::ConstPlafonLimit * self::ConstTaksiranEmas * ($harga_emas * 100);

        // limit dalam gram merupakan konversi nominal limit kedalam gram dari nilai nominal dibagi dengan plafon limit
        // dengan roundup akurasi 0.0001 gram
        $limit_dalam_gram = ceil(($nominal_limit / $plafon_limit) * 10000) / 10000;

        // validasi saldo efektif tidak mencukupi
        if ($saldo_efektif < $limit_dalam_gram) {
            return "errorSaldoTidakMencukupi";
        }

        // validasi minimum saldo efektif
        if ($saldo_efektif - $limit_dalam_gram < self::MinimumSaldoEfektif) {
            return "errorSaldoMinimum";
        }
        return "valid";
    }

    public function getGoldLimit($nominal_limit, $harga_emas)
    {
        $plafon_limit = Round($harga_emas / 100 * 94, 4);
        $limit_dalam_gram = Round($nominal_limit / $plafon_limit / 100, 4);
        return $limit_dalam_gram;
    }

    public function setLimitPengajuan($params)
    {
        $goldLimit = $this->getGoldLimit($params['cardLimit'], $params['harga_emas']);
        $url = '/registrations/card-limit';

        log_message('debug', __FUNCTION__ . " : Set limit pengajuan Goldcard: " . $params['appNumber']);

        $data = [
            "applicationNumber" => $params['appNumber'],
            "cardLimit" => (int) $params['cardLimit'],
            "goldLimit" => (float) $goldLimit
        ];


        $response = $this->goldcard_api_service->postData($url, $data, 'json', 'POST');

        return $response;
    }

    public function inquiryKenaikanLimit($params)
    {
        $url = '/update-limit/increase/inquiry';

        log_message('debug', __FUNCTION__ . " : Inquiry kenaikan limit Goldcard with Account Number: " . $params['accNumber']);

        // get requirement from Goldcard API
        $requirements = $this->getRequirements();

        // check multiply of 10.000
        if (!$this->DividedBy10Thousand($params['nominalLimit'])) {
            $errorMessage = $this->response_service->getResponse('inquiry_kenaikan_limit.DividedBy10Thousand');
            return $errorMessage;
        }

        // check max limit 999.990.000
        if (!$this->MaxLimitGoldcard($params['nominalLimit'], $requirements['limit_pengajuan_max'])) {
            $errorMessage = $this->response_service->getResponse('inquiry_kenaikan_limit.MaxLimitGoldcard');
            return $errorMessage;
        }

        $data = [
            "accountNumber" => $params['accNumber'],
            "nominalLimit" => (int) $params['nominalLimit']
        ];

        // inquiry ke goldcard api
        $inquiryKenaikanLimit = $this->goldcard_api_service->postData($url, $data, 'json', 'POST');
        if ($inquiryKenaikanLimit['code'] == '11') {
            $response = $this->response_service->getResponse('inquiry_kenaikan_limit.NPWP');
            $response['data']['refId'] = $inquiryKenaikanLimit['data']['refId'];
            return $response;
        }

        if ($inquiryKenaikanLimit['code'] == '12') {
            $response = $this->response_service->getResponse('inquiry_kenaikan_limit.PengajuanLimitBaruDalamProses');
            $response['message'] = $inquiryKenaikanLimit['message'];
            $response['data']['message_content'] =  $inquiryKenaikanLimit['message'];
            return $response;
        }

        if ($inquiryKenaikanLimit['code'] == '14') {
            $response = $this->response_service->getResponse('inquiry_kenaikan_limit.ErrorCore');
            $response['message'] = $inquiryKenaikanLimit['message'];
            $response['data']['message_content'] =  $inquiryKenaikanLimit['message'];
            return $response;
        }

        if ($inquiryKenaikanLimit['code'] != '00') {
            $response = $this->response_service->getResponse('');
            $response['code'] = $inquiryKenaikanLimit['code'];
            $response['message'] = $inquiryKenaikanLimit['message'];
            return $response;
        }

        $response = $this->response_service->getResponse('inquiry_kenaikan_limit.success');
        $response['data']['refId'] = $inquiryKenaikanLimit['data']['refId'];
        return $response;
    }

    public function submitKenaikanLimit($params)
    {
        $url = '/update-limit/increase';

        log_message('debug', __FUNCTION__ . " : Submit kenaikan limit Goldcard with Ref ID: " . $params['refID']);

        $data = [
            "npwpImageBase64" => $params['npwp'],
            "refId" => $params['refID']
        ];

        // inquiry ke goldcard api
        $submitKenaikanLimit = $this->goldcard_api_service->postData($url, $data, 'json', 'POST');
        if ($submitKenaikanLimit['code'] == '11') {
            $response = $this->response_service->getResponse('inquiry_kenaikan_limit.NPWP');
            return $response;
        }
        if ($submitKenaikanLimit['code'] != '00') {
            $response = $this->response_service->getResponse('');
            $response['code'] = $submitKenaikanLimit['code'];
            $response['message'] = $submitKenaikanLimit['message'];
            return $response;
        }

        $response = $this->response_service->getResponse('submit_kenaikan_limit.success');
        return $response;
    }

    public function selectTE($list_tabungan, $norek)
    {
        $tab_emas = [];
        foreach ($list_tabungan as $list) {
            if ($list->norek == $norek) {
                $tab_emas = $list;
            }
        }
        return $tab_emas ?? false;
    }

    public function setNorek($applicationNumber, $norek)
    {
        $url = '/registrations/saving-account';

        log_message('debug', __FUNCTION__ . " : Set norek goldcard: " . json_encode($applicationNumber));
        $data = [
            "applicationNumber" => $applicationNumber,
            "accountNumber" => $norek
        ];
        $response = $this->goldcard_api_service->postData($url, $data, 'json', 'POST');
        return $response;
    }

    public function getCustomerData($user)
    {
        log_message('debug', __FUNCTION__ . " : Get customer data: " . json_encode($user));
        $data = ([
            'nik' => $user->noIdentitas,
            'flag' => '9'
        ]);
        $rest_customer_data = $this->rest_switching_customer_service->getCustomerData($data);
        log_message('debug', __FUNCTION__ . " : Get Core customer data: " . json_encode($rest_customer_data));
        return $rest_customer_data;
    }

    public function postDataDiriToGoldcard($data)
    {
        $url = '/registrations/personal-informations';
        $sanitizedName = $this->gcNameSanitizer($data[0]->nama);

        $data = [
            "applicationNumber" => $data[0]->GoldCardApplicationNumber,
            "firstName" => $sanitizedName['firstName'],
            "lastName" => $sanitizedName['lastName'],
            "cardName" => $sanitizedName['cardName'],
            "npwp" => $data[0]->noNPWP,
            "nik" => $data[0]->noKTP,
            "birthPlace" => $data[0]->tempatLahir,
            "birthDate" => $data[0]->tglLahir,
            "sex" => $this->setDataJenisKelamin($data[0]->jenisKelamin),
            "homeStatus" => $this->config->item('home_status'),
            "nationality" => $data[0]->kewarganegaraan,
            "education" => $this->setDataPendidikan($data[1]->pendidikan ?? ''),
            "maritalStatus" => (int) $data[1]->statusKawin,
            "motherName" => $data[0]->namaIbu,
            "homePhoneNumber" => $data[1]->telp ?? $data[0]->noHP,
            "email" => $data[0]->email,
            "ktpImageBase64" => $data[2]['foto_ktp_base_64'],
            "npwpImageBase64" => $data[2]['foto_npwp_base_64'],
            "selfieImageBase64" => $data[2]['foto_diri_base_64'],
            "addressLine1" => $data[2]['addressLine1'],
            "province" => $data[2]['province'],
            "addressCity" => $data[2]['addressCity'],
            "subdistrict" => $data[2]['subdistrict'],
            "village" => $data[2]['village'],
            "relativePhoneNumber" => $data[2]['no_hp_kerabat']
        ];

        $response = $this->goldcard_api_service->postData($url, $data, 'json', 'POST');
        return $response;
    }

    private function gcNameSanitizer($name)
    {
        $patterns = ['/prof/', '/drs/', '/dra/', '/sdr/'];
        $name = strtolower($name);
        // remove every string after comma, because its titles
        $name = substr($name, 0, strpos($name, ',')) ?: $name;
        // remove special characters
        $name = preg_replace('/[^A-Za-z0-9\-]/', ' ', $name);
        // remove patterns string
        $name = preg_replace($patterns, '', $name);
        // remove double spaces
        $name = preg_replace('!\s+!', ' ', (trim($name)));
        $names = explode(' ', strtoupper($name));
        $newNames = [];

        // remove words that less then 3 chars
        for ($idx = 0; $idx < count($names); $idx++) {
            if (strlen($names[$idx]) < 3) {
                continue;
            }

            $newNames[] = $names[$idx];
        }

        $nameStr = implode(' ', $newNames);
        $sanitizedName['firstName'] = $this->setDataNamaDepan($nameStr);
        $sanitizedName['lastName'] = $this->setDataNamaBelakang($nameStr);
        $sanitizedName['cardName'] = $nameStr;

        return $sanitizedName;
    }

    public function setDataNamaDepan($nama)
    {
        if (substr_count($nama, " ") > 0) {
            return substr($nama, 0, strpos($nama, " "));
        }
        return $nama;
    }

    public function setDataNamaBelakang($nama)
    {
        if (substr_count($nama, " ") > 0) {
            return substr($nama, strpos($nama, " ") + 1);
        }
        return '';
    }

    public function setDataJenisKelamin($sex)
    {
        switch ($sex) {
            case 'L':
                return 1;
            case 'P':
                return 2;
        }
    }

    public function setDataPendidikan($pendidikan)
    {
        switch ($pendidikan) {
            case 'S3':
                return 6;
            case 'S2':
                return 5;
            case 'S1':
                return 4;
            case 'D3':
                return 3;
            case 'D2':
                return 3;
            case 'D1':
                return 3;
            case 'SMA':
                return 2;
            default:
                return 1;
        }
    }

    public function MinSaldoTE($requirement, $harga_emas, $saldo_efektif)
    {
        $plafon_limit = Round($harga_emas / 100 * 94, 4);
        $saldo_rupiah = $plafon_limit * $saldo_efektif * 100;
        if ($requirement > $saldo_rupiah) {
            return false;
        }
        return true;
    }

    public function postRegistrations($user)
    {
        $url = '/registrations';

        log_message('debug', __FUNCTION__ . " : Get Applications Number");

        $data = [
            "cif" => $user->cif,
            "handPhoneNumber" => $user->noHP,
            "applicationNumber" => $user->GoldCardApplicationNumber,
            "branchCode" => $user->kodeCabang
        ];

        $response = $this->goldcard_api_service->postData($url, $data, 'json', 'POST');

        // do update goldcard application number into user db
        $this->User->updateGoldCardAppNumber($user->id, $response['data']['applicationNumber']);

        return $response;
    }

    public function getSaldoGoldcard($inquiryTabEmas, $norek, $cif)
    {
        // mendapatkan saldo rekening tabungan emas
        $listTabungan = json_decode($inquiryTabEmas->data)->listTabungan;
        $arrayIndex = array_search($norek, array_column($listTabungan, 'norek'));
        if (strval($arrayIndex) == null) {
            return false;
        }

        $dataRekeningEmas = $listTabungan[$arrayIndex];
        $saldoBlokir = $dataRekeningEmas->saldoBlokir;

        // mendapatkan saldo blokir kartu emas
        $request = [
            "cif" => (string) $cif,
            "noRek" => (string) $norek
        ];

        $saldoGcBlokir = $this->rest_switching_goldcard_service->inquiryGoldcard($request);

        if (!in_array($saldoGcBlokir['responseCode'], ['00', '14'])) {
            return false;
        }

        if (!empty($saldoGcBlokir['data'])) {
            $saldoBlokir = json_decode($saldoGcBlokir['data'])->saldo ?? $saldoBlokir;
        }

        $saldo = [
            'saldoEmas' => $dataRekeningEmas->saldoEmas,
            'saldoEfektif' => $dataRekeningEmas->saldoEfektif,
            'saldoBlokir' => $saldoBlokir
        ];

        return $saldo;
    }

    public function getProgresStatusPengajuan($data)
    {
        if (empty($data['currentStep'])) {
            return 'ajukan.sukses';
        }

        if ($data['applicationStatus'] == "application_ongoing") {
            $currentStep = $data['currentStep'];

            switch ($currentStep) {
                case 0:
                    return 'ajukan.sukses';
                case 1:
                    return 'ajukan.sukses.prosesPengajuanLimit';
                case 2:
                    return 'ajukan.sukses.prosesInputDataDiri';
                case 3:
                    return 'ajukan.sukses.prosesInputDataPekerjaan';
                default:
                    return 'ajukan.sukses.prosesInputDataPengirimanKartu';
            }
        }

        if ($data['applicationStatus'] == "active") {
            return 'ajukan.sukses.prosesAktif';
        }

        $listStatus = ['inactive', 'rejected', 'card_suspended'];
        if (in_array($data['applicationStatus'], $listStatus)) {
            return 'ajukan.sukses.prosesDitolak';
        }

        // Default return screen GoldCardStatusPengajuan
        return 'ajukan.sukses.prosesStatusPengajuan';
    }

    public function setFinalisasi($user)
    {
        $url = '/registrations/final';

        log_message('debug', __FUNCTION__ . " : Set finalisasi goldcard: " . $user->GoldCardApplicationNumber);
        $data = ([
            "applicationNumber" => $user->GoldCardApplicationNumber
        ]);

        $response = $this->goldcard_api_service->postData($url, $data, 'json', 'POST');
        if ($response['code'] != "00") {
            // reset goldcard application number
            $this->User->resetGoldcardAccountNumber($user->id);
            return $this->response_service->getResponse('gcPengajuan.gagal');
        }

        return $this->response_service->getResponse('general.succeeded');
    }

    public function getStatusPengajuan($applicationNumber)
    {
        $url = '/registrations/application-status';

        log_message('debug', __FUNCTION__ . " : Get Status Pengajuan");

        $data = (["applicationNumber" => $applicationNumber]);

        $response = $this->goldcard_api_service->postData($url, $data, 'json', 'POST');

        return $response;
    }

    public function cekAktivasi($token)
    {
        $url = '/activations/inquiry';
        $user = $this->User->profile($token->id);
        $data = ["applicationNumber" => $user->GoldCardApplicationNumber];
        $response = $this->goldcard_api_service->postData($url, $data, 'json', 'POST');
        $resp = $this->response_service->getResponse('');
        $resp['status'] = 'success';

        if ($response['code'] == "22") {
            $resp = $this->response_service->getResponse('gold_card_cek_aktivasi.expired');
            $resp['status'] = 'error';
        }

        if ($response['code'] == "55") {
            $resp = $this->response_service->getResponse('gold_card_cek_aktivasi.stlTurunTopUp');
            $resp['status'] = 'error';
        }

        if ($response['code'] == "44") {
            $resp = $this->response_service->getResponse('gold_card_cek_aktivasi.stlTurunPotong');
            $resp['status'] = 'error';
        }

        if (!in_array($response['code'], ['00', '22', '44', '55'])) {
            $resp = $this->response_service->getResponse('');
            $resp['status'] = 'error';
            $resp['code'] = $response['code'];
            $resp['message'] = $response['message'];
            return $resp;
        }

        $resp['code'] = $response['code'];
        $resp['message'] = $response['message'];
        $resp['data']['message_header'] = $response['message'];
        $resp['data']['message_content'] = $response['description'] ?? '';

        return $resp;
    }

    public function validasiTanggal($tanggal_berlaku)
    {
        $tanggalBerlaku = DateTime::createFromFormat("m/y", $tanggal_berlaku);
        $today = new DateTime();

        return $tanggalBerlaku->format('Y-m') >= $today->format('Y-m');
    }

    public function cekAktivasiKartu($applicationNumber, $params)
    {
        $url = '/activations';

        //Validasi Tanggal
        if (!$this->validasiTanggal($params['tanggal_berlaku'])) {
            return $this->messageGoldcard('aktivasi_kartu.validasiTanggal');
        }

        $tanggal_berlaku = str_replace("/", "", $params['tanggal_berlaku']);
        log_message('debug', __FUNCTION__ . " : Cek validasi goldcard: " . json_encode($applicationNumber));

        $data = ([
            "applicationNumber" => $applicationNumber,
            "firstSixDigits" => $params['nomor_kartu_pertama'],
            "lastFourDigits" => $params['nomor_kartu_terakhir'],
            "expDate" => $tanggal_berlaku,
            "birthDate" => $params['tanggal_lahir']
        ]);

        $response = $this->goldcard_api_service->postData($url, $data, 'json', 'POST');
        switch ($response['code']) {
            case '00':
                return $response;
                break;
            case '15':
                return $this->messageGoldcard('aktivasi_kartu.dataSalah');
                break;
            case '11':
                return $this->messageGoldcard('aktivasi_kartu.dataSalah');
                break;
            default:
                return $this->messageGoldcard('aktivasi_kartu.gagal');
                break;
        }
        return $this->messageGoldcard('aktivasi_kartu.gagal');
    }

    function filterRekening($tab_emas, $cif)
    {
        log_message('debug', __FUNCTION__ . " : Filter rekening induk is register gte");

        $request = ([
            'cif' => $cif
        ]);

        $cekRekInduk = $this->rest_switching_goldcard_service->cekRekInduk((array) $request);
        if (empty($cekRekInduk) || $cekRekInduk['responseCode'] != "00") {
            return false;
        }

        $list_emas = json_decode($tab_emas->data)->listTabungan;
        $cek = json_decode($cekRekInduk['data'])->listRekeningUtama;

        // Mendapatkan list rekening tabungan induk is register gte
        $tab_emas_reggte = [];
        foreach ($cek as $list) {
            if ($list->isRegGTE) {
                array_push($tab_emas_reggte, $list->norek);
            }
        }

        // Memfilter portofolio rekening tabungan emas yang merupakan tab induk is register gte
        $tab_emas_filter = [];
        foreach ($list_emas as $list) {
            if (in_array($list->norek, $tab_emas_reggte)) {
                array_push($tab_emas_filter, $list);
            }
        }

        if (empty($tab_emas_filter)) {
            return false;
        }

        return $tab_emas_filter;
    }

    public function postDataPekerjaan($data)
    {
        $url = '/registrations/occupation';

        log_message('debug', __FUNCTION__ . " : Post data pekerjaan goldcard: " . json_encode($data));

        $data = ([
            'applicationNumber' => $data[0]->GoldCardApplicationNumber,
            'jobCategory' => (int) $data[1]['pekerjaan'],
            'jobStatus' => (int) $data[1]['status_pekerjaan'],
            'totalEmployee' => (int) $data[1]['jumlah_karyawan'],
            'company' => $data[1]['nama_perusahaan'],
            'officeAddress1' => $data[1]['alamat_kantor'],
            'officePhone' => $data[1]['nomor_telepon_kantor'],
            'income' => (int) $this->setDataJumlahPendapatan($data[1]['jumlah_pendapatan']),
            'workSince' => $this->workSinceFormatter($data[1]['lama_bekerja']),
            'jobBidangUsaha' => $this->config->item('jobBidangUsaha'),
            'jobSubBidangUsaha' => $this->config->item('jobSubBidangUsaha'),
            'officeProvince' => $data[1]['provinsi_kantor'],
            'officeCity' => $data[1]['kota_kantor'],
            'officeSubdistrict' => $data[1]['kecamatan_kantor'],
            'officeVillage' => $data[1]['kelurahan_kantor'],
        ]);
        $response = $this->goldcard_api_service->postData($url, $data, 'json', 'POST');
        return $response;
    }

    public function workSinceFormatter($workSince)
    {
        $bulan = date('m');
        $tahun = substr(date('Y') - $workSince, 2);
        return $bulan . "/" . $tahun;
    }

    public function setDataJumlahPendapatan($pendapatan)
    {
        if ($pendapatan <= $this->config->item('pendapatan_minimum')) {
            return rand($this->config->item('pendapatan_minimum'), $this->config->item('pendapatan_acak_batas_atas'));
        }
        return $pendapatan;
    }

    public function updateAccountNumber($id, $accountNumber)
    {
        log_message('debug', __FUNCTION__ . " : Update Goldcard Account Number");

        $update = $this->User->updateGoldCardAccountNumber($id, $accountNumber);
        if (!$update) {
            return $this->messageGoldcard('aktivasi_kartu.updateAccountNumberGagal');
        }

        return true;
    }

    public function getRiwayatTransaksi($token, $reqRiwayat)
    {
        log_message('debug', __FUNCTION__ . " : Get Riwayat Transaksi");
        $url = '/transactions/history';

        // Get account number
        $accountNumber = $this->User->getUser($token->id)->goldcard_account_number;
        $reqRiwayat['accountNumber'] = $accountNumber;

        $response = $this->goldcard_api_service->postData($url, $reqRiwayat, 'query', 'GET');

        if ($response['code'] != "00") {
            log_message('debug', 'end of ' . __FUNCTION__ . ' request with response: ' . json_encode($response));
            return false;
        }
        return $response;
    }

    public function getSaldo($token)
    {
        $url = '/transactions/balance';

        log_message('debug', __FUNCTION__ . " : Get Saldo Kartu Emas");

        // Get account number
        $accountNumber = $this->User->getUser($token->id)->goldcard_account_number;
        $data = ([
            "accountNumber" => $accountNumber
        ]);

        $response = $this->goldcard_api_service->postData($url, $data, 'query', 'GET');
        return $response;
    }

    public function getCetakTagihan($token)
    {
        $url = '/billings/statements';

        log_message('debug', __FUNCTION__ . " : Get Cetak Tagihan Kartu Emas");

        // Get account number
        $accountNumber = $this->User->getUser($token->id)->goldcard_account_number;
        $data = ([
            "accountNumber" => $accountNumber
        ]);

        $response = $this->goldcard_api_service->postData($url, $data, 'query', 'GET');
        return $response;
    }

    public function gcGetListGte($token)
    {
        $user = $this->User->getUser($token->id);
        $data = ([
            "accountNumber" => $user->goldcard_account_number
        ]);
        $savingAccount = $this->goldcard_api_service->postData('/update-limit/account-by-accnumber', $data, 'query', 'GET');

        if ($savingAccount['code'] != "00") {
            return $this->messageGoldcard('check_tagihan_gte.norekNotFound');
        }

        $request = [
            'cif' => $user->cif,
            'noRek' => $savingAccount['data']['savingAccount']
        ];

        $cekPortofolio = $this->rest_switching_goldcard_service->cekPortofolio($request);

        if ($cekPortofolio['responseCode'] != "00") {
            return $this->response_service->getResponse('list_gte.error');
        }

        $gte = json_decode($cekPortofolio['data']);
        // Get list GTE GoldCard
        $list_gte_goldcard = [];

        foreach ($gte->listKredit as $list) {
            array_push($list_gte_goldcard, $list);
        }

        if (empty($list_gte_goldcard)) {
            return $this->response_service->getResponse('list_gte.notFound');
        }

        $response = $this->response_service->getResponse('list_gte.success');
        $response['data'] = $list_gte_goldcard;
        return $response;
    }

    public function validatePaymentInquiry($params, $token)
    {
        $user = $this->User->getUser($token->id);
        $params['account_number'] = $user->goldcard_account_number;
        // get biaya channel from all banks
        $biayaChannel = $this->payment_service->getBiayaChannel("CC", "32");
        // get payment id from the session
        $inquiryData = $this->cache_service->getGcPayInquirySession($params);

        // if payment id existed the return the response success
        if (!empty($inquiryData)) {
            $response = $this->response_service->getResponse('gcInquiryPayment.sukses');
            $response['data'] = ['biayaChannel' => $biayaChannel];

            return $response;
        }

        // if payment amount is less or equal to zero then return error message
        if ($params['payment_amount']  <= 0) {
            $response = $this->response_service->getResponse('gcInquiryPayment.PaymentNotValid');
            $response['message'] = "Jumlah pembayaran harus diisi";

            return $response;
        }

        $url = '/transactions/payment/inquiry';
        $data = [
            'accountNumber' => $params['account_number'],
            'paymentAmount' => (int) $params['payment_amount']
        ];

        $gcResponse = $this->goldcard_api_service->postData($url, $data, 'json', 'POST');

        // billing not found
        if ($gcResponse['code'] == '11') {
            return $this->response_service->getResponse('gcInquiryPayment.trxNotFound');
        }

        // Mapping error tagihan tidak sesuai
        if ($gcResponse['code'] == '22') {
            $response = $this->response_service->getResponse('gcInquiryPayment.PaymentNotValid');
            $response['message'] = $gcResponse['message'];

            return $response;
        }

        if ($gcResponse['code'] != '00') {
            return $this->response_service->getResponse('gcInquiryPayment.gagal');
        }

        // set payment id session
        $this->cache_service->setGcPayInquirySession($params, $gcResponse['data']);
        // set response
        $response = $this->response_service->getResponse('gcInquiryPayment.sukses');
        $response['data'] = ['biayaChannel' => $biayaChannel];

        return $response;
    }

    public function checkTagihanGTE($token)
    {
        log_message('debug', __FUNCTION__ . " : Check Tagihan GTE");

        $user = $this->User->getUser($token->id);
        $data = ([
            "accountNumber" => $user->goldcard_account_number
        ]);
        $savingAccount = $this->goldcard_api_service->postData('/update-limit/account-by-accnumber', $data, 'query', 'GET');

        if ($savingAccount['code'] != "00") {
            return $this->messageGoldcard('check_tagihan_gte.norekNotFound');
        }

        $params = [
            "cif" => $user->cif,
            "noRek" => $savingAccount['data']['savingAccount']
        ];

        $response = $this->rest_switching_goldcard_service->cekPortofolio($params);

        if ($response['responseCode'] == "00") {
            return $this->messageGoldcard('check_tagihan_gte.tagihanFound');
        }

        return $this->messageGoldcard('check_tagihan_gte.tagihanNotFound');
    }

    public function gcCreatePayment($params, $token)
    {
        $method = $params['payment'];

        if (!in_array($params['payment'], ['WALLET', 'GCASH'])) {
            $method = 'BANK';
        }

        $user = $this->User->getUser($token->id);
        // get payment id
        $inquiryData = $this->cache_service->getGcPayInquirySession([
            'account_number' => $user->goldcard_account_number,
            'payment_amount' => $params['amount']
        ]);

        if (empty($inquiryData)) {
            return $this->response_service->getResponse('gcCreatePayment.notInquired');
        }

        $params = array_merge($inquiryData, $params);
        $params['kode_produk'] = '58';
        $params['kode_bank'] = $this->ConfigModel::KODE_BANK_PAYMENT_METHOD[$params['payment']];
        $params['biayaTransaksi'] = $this->ConfigModel->getRealBiayaPayment($params['jenisTransaksi'], $method, $params['kode_produk'], $params['kode_bank']);
        $params['keterangan'] = 'Pembayaran tagihan kartu emas';
        $params['totalKewajiban'] = $params['amount'] + $params['surcharge'];
        // do create payment
        $response = $this->payment_service->createPayment($params, $user);

        if ($response['code'] != '00') {
            return $this->response_service->getResponse('gcCreatePayment.failed');
        }

        // unset inquiry data session
        $this->cache_service->unsetGcPayInquirySession([
            'account_number' => $user->goldcard_account_number,
            'payment_amount' => $params['amount']
        ]);
        // TODO: send notification

        return $response;
    }

    public function gcBlockCard($params, $token)
    {
        $url = '/cards/block';

        log_message('debug', __FUNCTION__ . " : Blokir kartu goldcard: " . json_encode($params));

        $user = $this->User->getUser($token->id);

        $data = ([
            'accountNumber' => $user->goldcard_account_number,
            'reason' => $params['alasan'],
            'reasonCode' => $params['kode_alasan'],
        ]);

        $response = $this->goldcard_api_service->postData($url, $data, 'json', 'POST');
        if ($response['code'] != '00') {
            return $this->response_service->getResponse('gcBlockCard.gagal');
        }

        return $this->response_service->getResponse('gcBlockCard.sukses');
    }

    public function gcResetRegistration($token)
    {
        $url = '/registrations/reset';

        log_message('debug', __FUNCTION__ . " : Reset pengajuan goldcard");

        $user = $this->User->profile($token->id);

        $data = ([
            'applicationNumber' => $user->GoldCardApplicationNumber,
        ]);

        $response = $this->goldcard_api_service->postData($url, $data, 'json', 'POST');
        if ($response['code'] != '00') {
            return $this->response_service->getResponse('gcResetRegistration.gagal');
        }

        $update = $this->User->resetGoldcardAccountNumber($user->id);
        if (!$update) {
            return $this->response_service->getResponse('gcResetRegistration.gagal');
        }

        return $this->response_service->getResponse('gcResetRegistration.sukses');
    }

    public function getNorekPilihan($token)
    {
        $url = '/update-limit/account-by-accnumber';

        log_message('debug', __FUNCTION__ . " : Get Norek Pilihan");

        // Get application number
        $user = $this->User->profile($token->id);
        $data = ([
            "accountNumber" => $user->goldCardAccountNumber
        ]);

        $savingAccount = $this->goldcard_api_service->postData($url, $data, 'query', 'GET');

        $response =  $this->messageGoldcard('get_saving_account.success');
        $response['data'] = $savingAccount['data'];

        if ($savingAccount['code'] != "00") {
            $response =  $this->messageGoldcard('get_saving_account.error');
        }

        return $response;
    }

    public function gcInquiry($token)
    {
        $user = $this->User->profile($token->id);
        $accountNumber = $user->goldCardAccountNumber || '';
        $appNumber = $user->GoldCardApplicationNumber || '';

        // jika nasabah belum daftar dan belum aktivasi
        if (empty($accountNumber) && empty($appNumber)) {
            return $this->response_service->getResponse('gcInquiry.NF');
        }

        // jika nasabah telah aktivasi
        if (!empty($accountNumber)) {
            return $this->response_service->getResponse('gcInquiry.accFound');
        }

        // jika nasabah sedang dalam proses pengajuan
        // postRegistrations untuk get application number
        $regData = $this->postRegistrations($user);

        if ($regData['code'] != 00) {
            return $this->response_service->getResponse('gcInquiry.accFound');
        }

        $statusExcluded = ['inactive', 'rejected', 'card_suspended', 'application_ongoing', 'active'];
        $response = $this->response_service->getResponse('gcInquiry.appFound');

        if (!in_array($regData['data']['applicationStatus'], $statusExcluded)) {
            $response['data']['screen_name'] = "GoldCardStatusPengajuan";
        }

        return $response;
    }
}
