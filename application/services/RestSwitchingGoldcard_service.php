<?php

use GuzzleHttp\Exception\GuzzleException;

class RestSwitchingGoldcard_service extends MY_Service
{
    /**
     * RestSwitchingGpoint_service constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->service('RestSwitching_service', 'rest_switching_service');
    }

    /**
     * Endpoint POST /goldcard/checkreggte
     *
     * @param array $data
     * @return mixed
     * @throws GuzzleException
     */
    public function isRegisterGte(array $data)
    {
        $url = '/goldcard/checkreggte';

        return $this->rest_switching_service->postData($url, $data);
    }

    public function inquiryGoldcard(array $data)
    {
        $url = '/goldcard/inquiry';

        return $this->rest_switching_service->postData($url, $data);
    }

    public function openGoldcard(array $data)
    {
        $url = '/goldcard/open';

        return $this->rest_switching_service->postData($url, $data);
    }

    public function closeGoldcard(array $data)
    {
        $url = '/goldcard/close';

        return $this->rest_switching_service->postData($url, $data);
    }

    public function cekRekInduk(array $data)
    {
        $url = '/goldcard/inquiry/rekInduk';
        return $this->rest_switching_service->postData($url, $data);
    }

    public function cekPortofolio(array $data)
    {
        $url = '/goldcard/gte/inquiry';
        return $this->rest_switching_service->postData($url, $data);
    }
}
