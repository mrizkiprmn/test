<?php

use GuzzleHttp\Exception\GuzzleException;

class RestSwitchingOtp_service extends MY_Service
{
    /**
     * RestSwitchingGpoint_service constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->service('RestSwitching_service', 'rest_switching_service');
    }

    // Endpoint POST /otp/validate
    public function otpValidate(array $data)
    {
        $url = '/otp/validate';

        return $this->rest_switching_service->postData($url, $data);
    }

    // Endpoint POST /otp/send
    public function otpSend(array $data)
    {
        $url = '/otp/send';

        return $this->rest_switching_service->postData($url, $data);
    }

    // Endpoint POST /otp/check
    public function otpCheck(array $data)
    {
        $url = '/otp/check';

        return $this->rest_switching_service->postData($url, $data);
    }
}
