<?php

/**
 * @property BankModel bank_model
 * @property ConfigModel config_model
 * @property User user_model
 * @property Pegadaian Pegadaian
 * @property RestSwitchingPayment_service rest_switching_payment_service
 */

class Response_service extends MY_Service
{
    const GENERAL_SUCCESS = 'general.succeeded';

    // Default Response
    protected $response = [];

    public function __construct()
    {
        parent::__construct();

        $this->response = [
            'code' => 101,
            'status' => 'error',
            'message' => 'Internal Server Error',
            'data' => [
                'screen_name' => '', // to redirect to screen name
                'message_header' => '', // pesan modal untuk line pertama
                'message_content' => '', // pesan modal untuk line kedua
                'labelTop' => '', // button text bagian atas
                'labelBottom' => '', // button text bagian bawah
                'buttonTop' => '', // screen name untuk redirect after tap/click button bagian atas
                'buttonBottom' => '' // screen name untuk redirect after tap/click button bagian bawah
            ]
        ];

        // init load model
        $this->load->model('BankModel', 'bank_model');
        $this->load->model('User', 'user_model');
        $this->load->model('ConfigModel', 'config_model');
        // init load helper
        $this->load->helper('Pegadaian');
    }

    public function getResponse($type_validation)
    {
        $data = $this->response['data'];

        switch ($type_validation) {
            case self::GENERAL_SUCCESS:
                $this->response['code'] = '200';
                $this->response['status'] = 'success';
                $this->response['message'] = 'Data berhasil dikirim';
                break;
            case 'gcInquiryPayment.gagal':
                $this->response['code'] = 99;
                $this->response['message'] = 'Inquiry pembayaran tidak valid';
                break;
            case 'gcInquiryPayment.trxNotFound':
                $this->response['code'] = 11;
                $this->response['message'] = 'Anda belum memiliki tagihan';
                break;
            case 'gcInquiryPayment.PaymentNotValid':
                $this->response['code'] = 22;
                break;
            case 'gcInquiryPayment.sukses':
                $this->response['code'] = '00';
                $this->response['status'] = 'success';
                $this->response['message'] = 'Validasi inquiry pembayaran sukses';
                break;
            case 'gcCreatePayment.notInquired':
                $this->response['message'] = 'Silahkan melakukan kembali inquiry pembayaran';
                break;
            case 'gcCreatePayment.failed':
                $this->response['message'] = 'Gagal melakukan pembayaran';
                break;
            case 'list_gte.error':
                $this->response['message'] = "Terjadi kesalahan, mohon coba beberapa saat lagi";
                break;
            case 'list_gte.notFound':
                $this->response['message'] = "Data Gadai Tabungan Emas tidak ditemukan";
                break;
            case 'list_gte.success':
                $this->response['code'] = '00';
                $this->response['status'] = 'success';
                $this->response['message'] = "Get data Gadai Tabungan Emas sukses";
                break;
            case 'gcBlockCard.gagal':
                $this->response['code'] = '99';
                $this->response['message'] = "Terjadi kesalahan blokir kartu, mohon coba beberapa saat lagi.";
                break;
            case 'gcBlockCard.sukses':
                $this->response['code'] = '00';
                $this->response['status'] = 'success';
                $this->response['message'] = "Blokir Kartu Berhasil";
                $data['message_header'] = "Blokir Kartu Berhasil";
                $data['message_content'] = "Segera hubungi call center 14017 untuk penggantian kartu emas kamu.";
                $data['buttonTop'] = "Hubungi Call Center";
                break;
            case 'gcResetRegistration.sukses':
                $this->response['code'] = '00';
                $this->response['status'] = 'success';
                $this->response['message'] = "Reset Registrasi Goldcard Berhasil";
                break;
            case 'gcResetRegistration.gagal':
                $this->response['code'] = '99';
                $this->response['message'] = "Terjadi kesalahan reset registrasi goldcard, mohon coba beberapa saat lagi.";
                break;
            case 'inquiry_kenaikan_limit.success':
                $this->response['code'] = '00';
                $this->response['status'] = 'success';
                $this->response['message'] = "Berhasil inquiry kenaikan limit Kartu Emas";
                $data['message_header'] = 'Konfirmasi Ubah Limit';
                $data['message_content'] = 'Apakah kamu yakin ingin mengubah limit kartu emas?';
                $data['labelTop'] = 'Kembali';
                $data['labelBottom'] = 'Ya, Ubah Limit';
                $data['buttonTop'] = 'GoldCardRiwayatTransaksi';
                $data['buttonBottom'] = '_function';
                break;
            case 'inquiry_kenaikan_limit.NPWP':
                $this->response['code'] = '11';
                $this->response['status'] = 'error';
                $this->response['message'] = "Upload NPWP kamu untuk mendapatkan limit lebih dari 50 juta";
                $data['message_header'] = 'Upload NPWP';
                $data['message_content'] = 'Upload NPWP kamu untuk mendapatkan limit lebih dari 50 juta';
                $data['labelTop'] = 'Kembali';
                $data['labelBottom'] = 'Upload NPWP';
                $data['buttonTop'] = 'GoldCardRiwayatTransaksi';
                $data['buttonBottom'] = 'GoldCardGetDataNpwp';
                break;
            case 'inquiry_kenaikan_limit.PengajuanLimitBaruDalamProses':
                $this->response['code'] = '12';
                $this->response['status'] = 'error';
                $data['message_header'] = 'Terjadi Kesalahan';
                $data['labelTop'] = null;
                $data['labelBottom'] = 'Oke';
                $data['buttonTop'] = null;
                $data['buttonBottom'] = 'GoldCardRiwayatTransaksi';
                break;
            case 'inquiry_kenaikan_limit.ErrorCore':
                $this->response['code'] = '14';
                $this->response['status'] = 'error';
                $data['message_header'] = 'Terjadi Kesalahan';
                $data['labelBottom'] = 'Oke';
                $data['buttonTop'] = null;
                $data['buttonBottom'] = 'GoldCardRiwayatTransaksi';
                break;
            case 'inquiry_kenaikan_limit.DividedBy10Thousand':
                $this->response['code'] = "99";
                $this->response['message'] = "Pengajuan limit harus memiliki kelipatan Rp 10.000";
                $data = 'validasi gagal';
                break;
            case 'inquiry_kenaikan_limit.MaxLimitGoldcard':
                $this->response['code'] = "99";
                $this->response['message'] = "Pengajuan limit maksimal Rp 999.990.000";
                $data = 'validasi gagal';
                break;
            case 'submit_kenaikan_limit.success':
                $this->response['code'] = "00";
                $this->response['status'] = 'success';
                $this->response['message'] = "Berhasil submit kenaikan limit Kartu Emas ";
                break;
            case 'gold_card_cek_aktivasi.expired':
                $this->response['status'] = 'success';
                $data["labelBottom"] = 'Oke, Batalkan Pengajuan';
                $data["buttonBottom"] = '_function';
                break;
            case 'gold_card_cek_aktivasi.stlTurunTopUp':
                $this->response['status'] = 'success';
                $data["labelTop"] = 'Nanti Saja';
                $data["labelBottom"] = 'Top Up Tabungan Emas';
                $data["buttonBottom"] = 'EmasBeli';
                $data["buttonTop"] = '';
                break;
            case 'gold_card_cek_aktivasi.stlTurunPotong':
                $this->response['status'] = 'success';
                $data["labelTop"] = 'Nanti Saja';
                $data["labelBottom"] = 'Lanjutkan Aktivasi';
                $data["buttonBottom"] = 'GoldCardAktivasi';
                $data["buttonTop"] = '';
                break;
            case 'gcInquiry.NF':
                $this->response['code'] = '00';
                $this->response['status'] = 'success';
                $this->response['message'] = "Silahkan melakukan pengajuan kartu emas";
                $data['screen_name'] = "GoldCard";
                break;
            case 'gcInquiry.errPostReg':
                $this->response['code'] = '99';
                $this->response['status'] = 'error';
                $this->response['message'] = "Terjadi kesalahan ketika mengambil data registrasi";
                break;
            case 'gcInquiry.accFound':
                $this->response['code'] = '00';
                $this->response['status'] = 'success';
                $this->response['message'] = "Anda telah memiliki no rekening kartu emas";
                $data['screen_name'] = "GoldCardRiwayatTransaksi";
                break;
            case 'gcInquiry.appFound':
                $this->response['code'] = '00';
                $this->response['status'] = 'success';
                $this->response['message'] = "Anda telah memiliki pengajuan kartu emas yang sedang berjalan";
                $data['screen_name'] = "GoldCard";
                break;
            case 'gcPengajuan.gagal':
                $this->response['code'] = '99';
                $this->response['status'] = 'error';
                $this->response['message'] = "Mohon maaf pengajuan Kartu Emas anda belum berhasil diajukan, silahkan ajukan kembali Kartu Emas anda.";
                $data["message_header"] = "Kartu Emas Gagal Diajukan";
                $data["message_content"] = "Mohon maaf pengajuan Kartu Emas anda belum berhasil diajukan, silahkan ajukan kembali Kartu Emas anda.";
                $data["labelBottom"] = 'Oke';
                $data["buttonBottom"] = 'GoldCard';
                $data["labelTop"] = null;
                $data['buttonTop'] = null;
                break;
            case 'gtefInquiry.pinjamanMelebihiLimitPinjamanPerhari':
                $this->response['code'] = '00';
                $this->response['status'] = 'success';
                $this->response['message'] = "Pinjaman Melebihi Limit";
                $data['screen_name'] = 'GTEF02InputPinjaman';
                $data['message_header'] = 'Pinjaman Melebihi Limit';
                $data['message_content'] = 'Pinjaman kamu melebihi limit transaksi pencairan  Rp. 500 juta per hari.';
                $data['labelTop'] = 'Oke';
                $data['buttonTop'] = 'GTEF02InputPinjaman';
                $data['buttonColorTop'] = 'green';
                break;
            case 'gtefInquiry.pinjamanMelebihiSisaLimit':
                $this->response['code'] = '00';
                $this->response['status'] = 'success';
                $this->response['message'] = "Pinjaman Melebihi Limit";
                $data['screen_name'] = 'GTEF02InputPinjaman';
                $data['message_header'] = 'Pinjaman Melebihi Limit';
                $data['message_content'] = 'Pinjaman kamu melebihi sisa limit pinjaman.';
                $data['labelTop'] = 'Oke';
                $data['buttonTop'] = 'GTEF02InputPinjaman';
                $data['buttonColorTop'] = 'green';
                break;
            case 'gtefInquiry.kewajibanMelebihiPinjaman':
                $this->response['code'] = '00';
                $this->response['status'] = 'success';
                $this->response['message'] = "Kewajiban Melebihi Pinjaman";
                $data['screen_name'] = 'GTEF02InputPinjaman';
                $data['message_header'] = 'Kewajiban Melebihi Pinjaman';
                $data['message_content'] = 'Jumlah tagihan kamu lebih besar dari pengajuan pinjaman. Lakukan pembayaran untuk melanjutkan gadai titipan emas.';
                $data['labelTop'] = 'Kembali';
                $data['buttonTop'] = 'GTEF02InputPinjaman';
                $data['buttonColorTop'] = 'white';
                $data['labelBottom'] = 'Bayar Sekarang';
                $data['buttonBottom'] = 'bayarGadai';
                $data['buttonColorBottom'] = 'green';
                break;
            case 'gtef.kycVerified':
                $this->response['code'] = '00';
                $this->response['status'] = 'success';
                $this->response['message'] = "Belum Ada E-KYC";
                $data['message_header'] = 'Upgrade ke Akun Premium';
                $data['message_content'] = 'Gunakan fitur ini dan dapatkan berbagai penawaran menarik dengan upgrade akun kamu melalui aplikasi atau cabang terdekat.';
                $data['labelTop'] = 'Lihat Cabang Terdekat';
                $data['buttonTop'] = 'CabangPegadaian';
                $data['buttonColorTop'] = 'green';
                break;
            case 'gtef.aktifasiTransFinansial':
                $this->response['code'] = '00';
                $this->response['status'] = 'success';
                $this->response['message'] = "Belum Aktifasi Transaksi Finansial";
                $data['message_header'] = 'Upgrade ke Akun Premium';
                $data['message_content'] = 'Gunakan fitur ini dan dapatkan berbagai penawaran menarik dengan upgrade akun kamu melalui aplikasi atau cabang terdekat.';
                $data['labelTop'] = 'Lihat Cabang Terdekat';
                $data['buttonTop'] = 'CabangPegadaian';
                $data['buttonColorTop'] = 'white';
                $data['labelBottom'] = 'Upgrade Sekarang';
                $data['buttonBottom'] = 'EKYC';
                $data['buttonColorBottom'] = 'green';
                break;
            case 'gtef.noTitipanTidakAda':
                $this->response['code'] = '00';
                $this->response['status'] = 'success';
                $this->response['message'] = "Data list rekening tidak ditemukan";
                $data['message_header'] = 'Belum Memiliki Titipan Emas';
                $data['message_content'] = 'Kamu harus memiliki titipan emas untuk melakukan gadai titipan emas.';
                $data['labelTop'] = 'Informasi Selengkapnya';
                $data['buttonTop'] = 'FAQGadaiTitipanEmas';
                $data['buttonColorTop'] = 'white';
                $data['labelBottom'] = 'Lihat Cabang Terdekat';
                $data['buttonBottom'] = 'CabangPegadaian';
                $data['buttonColorBottom'] = 'green';
                break;
            case 'gtef.waktuTitipanSegeraBerakhir':
                $this->response['code'] = '00';
                $this->response['status'] = 'success';
                $this->response['message'] = "Waktu Titipan Segera Berakhir";
                $data['screen_name'] = 'GTEF01PilihRekening';
                $data['message_header'] = 'Waktu Titipan Segera Berakhir';
                $data['message_content'] = 'Pinjaman kamu akan terpotong pada saat pencairan pinjaman untuk perpanjangan masa titipan.';
                $data['labelTop'] = 'Mengerti';
                $data['buttonTop'] = 'GTEF02InputPinjaman';
                $data['buttonColorTop'] = 'green';
                break;
            case 'gtef.hargaEmasTurun':
                $this->response['code'] = '00';
                $this->response['status'] = 'success';
                $this->response['message'] = "Harga Emas Turun";
                $data['screen_name'] = 'GTEF01PilihRekening';
                $data['message_header'] = 'Harga Emas Turun';
                $data['message_content'] = 'Kamu memiliki tagihan karena harga emas turun. Mohon lakukan pembayaran ke cabang terdekat.';
                $data['labelTop'] = 'Kembali';
                $data['buttonTop'] = 'GTEF01PilihRekening';
                $data['buttonColorTop'] = 'white';
                $data['labelBottom'] = 'Lihat Cabang Terdekat';
                $data['buttonBottom'] = 'CabangPegadaian';
                $data['buttonColorBottom'] = 'green';
                break;
            case 'gtef.kewajibanMelebihiSisaLimit':
                $this->response['code'] = '00';
                $this->response['status'] = 'success';
                $this->response['message'] = "Kewajiban Melebihi Sisa Limit";
                $data['screen_name'] = 'GTEF01PilihRekening';
                $data['message_header'] = 'Kewajiban Melebihi Sisa Limit';
                $data['message_content'] = 'Jumlah Tagihan Kamu melebihi Sisa Limit Pinjaman. Mohon lakukan pembayaran untuk melanjutkan gadai titipan emas.';
                $data['labelTop'] = 'Kembali';
                $data['buttonTop'] = 'GTEF01PilihRekening';
                $data['buttonColorTop'] = 'white';
                $data['labelBottom'] = 'Bayar Sekarang';
                $data['buttonBottom'] = 'PembayaranTitipan';
                $data['buttonColorBottom'] = 'green';
                break;
            case 'gtef.masihMemilikiTagihanJasaBulanan_DendaJasaBulanan_BiayaPerpanjangTitipan':
                $this->response['code'] = '00';
                $this->response['status'] = 'success';
                $this->response['message'] = "Masih Memiliki Tagihan";
                $data['screen_name'] = 'GTEF01PilihRekening';
                $data['message_header'] = 'Masih Memiliki Tagihan';
                $data['message_content'] = 'Pinjaman Kamu akan terpotong pada saat pencairan pinjaman untuk perpanjangan masa titipan dan pembayaran tagihan.';
                $data['labelTop'] = 'Mengerti';
                $data['buttonTop'] = 'GTEF02InputPinjaman';
                $data['buttonColorTop'] = 'green';
                break;
            case 'gtef.masihMemilikiTagihanJasaBulanan_DendaJasaBulanan':
                $this->response['code'] = '00';
                $this->response['status'] = 'success';
                $this->response['message'] = "Masih Memiliki Tagihan";
                $data['screen_name'] = 'GTEF01PilihRekening';
                $data['message_header'] = 'Masih Memiliki Tagihan';
                $data['message_content'] = 'Kamu memiliki tagihan sewa modal. Pinjaman kamu akan terpotong pada saat pencairan pinjaman.';
                $data['labelTop'] = 'Mengerti';
                $data['buttonTop'] = 'GTEF02InputPinjaman';
                $data['buttonColorTop'] = 'green';
                break;
            case 'gtef.titipanSudahTidakAktif':
                $this->response['code'] = '00';
                $this->response['status'] = 'success';
                $this->response['message'] = "Waktu Titipan Berakhir";
                $data['screen_name'] = 'GTEF01PilihRekening';
                $data['message_header'] = 'Waktu Titipan Berakhir';
                $data['message_content'] = 'Masa titipan kamu berakhir, Lakukan Perpanjangan Titipan Emas di Fitur Titipan Emas atau Datang Ke Outlet, Segera';
                $data['labelTop'] = 'Mengerti';
                $data['buttonTop'] = 'GTEF01PilihRekening';
                $data['buttonColorTop'] = 'green';
                break;
            default:
                $data = [];
        }

        $this->response['data'] = $data;

        return $this->response;
    }

    public function fValidationErrResponse($fValidationErr)
    {
        $this->response['message'] = 'Data tidak lengkap';
        $this->response['data'] = $fValidationErr;

        return $this->response;
    }
}
