<?php

/**
 * @property AuditLogModel audit_log_model
 */
class AuditLog_service extends MY_Service
{
    protected $response = [
        'status' => 'error',
        'message' => 'Terjadi kesalahan mohon coba beberapa saat lagi.',
        'data' => null
    ];

    /**
     * AuditLog_service constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('AuditLogModel', 'audit_log_model');
    }

    public function logResetPinPds($user_id, $cif, $nama_nasabah, $no_hp, $default_property, $change_property)
    {
        $data = [
            'user_id' => $user_id,
            'action' => $this->audit_log_model::ACTION_CHANGE,
            'modul' => $this->audit_log_model::MODUL_RESET_PIN,
            'cif' => (!empty($cif) ? $cif : ""),
            'nama_nasabah' => $nama_nasabah,
            'no_hp' => $no_hp,
            'default_property' => $default_property,
            'change_property' => $change_property,
        ];

        $this->insertLogResetPds($data);
    }

    public function logLinkCifPds($user_id, $cif, $nama_nasabah, $no_hp, $default_property, $change_property)
    {
        $data = [
            'user_id' => $user_id,
            'action' => $this->audit_log_model::ACTION_CHANGE,
            'modul' => $this->audit_log_model::MODUL_LINK_CIF,
            'cif' => (!empty($cif) ? $cif : ""),
            'nama_nasabah' => $nama_nasabah,
            'no_hp' => $no_hp,
            'default_property' => $default_property,
            'change_property' => $change_property,
        ];

        $this->insertLogResetPds($data);
    }

    public function logLinkCifPassion($user_id, $cif, $nama_nasabah, $no_hp, $default_property, $change_property)
    {
        $data = [
            'user_id' => $user_id,
            'action' => $this->audit_log_model::ACTION_CHANGE,
            'modul' => $this->audit_log_model::MODUL_LINK_CIF,
            'cif' => (!empty($cif) ? $cif : ""),
            'nama_nasabah' => $nama_nasabah,
            'no_hp' => $no_hp,
            'default_property' => $default_property,
            'change_property' => $change_property,
        ];

        $this->insertLogResetPassion($data);
    }

    public function logUnlinkCifAdmin($user_id, $cif, $nama_nasabah, $no_hp, $default_property, $change_property)
    {
        $data = [
            'user_id' => $user_id,
            'action' => $this->audit_log_model::ACTION_CHANGE,
            'modul' => $this->audit_log_model::MODUL_UNLINK_CIF,
            'cif' => (!empty($cif) ? $cif : ""),
            'nama_nasabah' => $nama_nasabah,
            'no_hp' => $no_hp,
            'default_property' => $default_property,
            'change_property' => $change_property,
        ];

        $this->insertLogResetAdmin($data);
    }

    public function logAktifasiFinansialPassion($user_id, $cif, $nama_nasabah, $no_hp, $default_property, $change_property)
    {
        $data = [
            'user_id' => $user_id,
            'action' => $this->audit_log_model::ACTION_CHANGE,
            'modul' => $this->audit_log_model::MODUL_AKTIFASI_FINANSIAL,
            'cif' => (!empty($cif) ? $cif : ""),
            'nama_nasabah' => $nama_nasabah,
            'no_hp' => $no_hp,
            'default_property' => $default_property,
            'change_property' => $change_property,
        ];

        $this->insertLogResetPassion($data);
    }

    public function logAktifasiFinansialPds($user_id, $cif, $nama_nasabah, $no_hp, $default_property, $change_property)
    {
        $data = [
            'user_id' => $user_id,
            'action' => $this->audit_log_model::ACTION_CHANGE,
            'modul' => $this->audit_log_model::MODUL_AKTIFASI_FINANSIAL,
            'cif' => (!empty($cif) ? $cif : ""),
            'nama_nasabah' => $nama_nasabah,
            'no_hp' => $no_hp,
            'default_property' => $default_property,
            'change_property' => $change_property,
        ];

        $this->insertLogResetPds($data);
    }

    function insertLogResetPds($data)
    {
        
        $data['platform'] = $this->audit_log_model::PLATFORM_PDS;

        $this->audit_log_model->add($data);
    }

    function insertLogResetPassion($data)
    {
        
        $data['platform'] = $this->audit_log_model::PLATFORM_PASSION;

        $this->audit_log_model->add($data);
    }

    function insertLogResetAdmin($data)
    {
        
        $data['platform'] = $this->audit_log_model::PLATFORM_ADMIN;

        $this->audit_log_model->add($data);
    }
}
