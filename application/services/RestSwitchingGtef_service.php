<?php

/**
 * @property RestSwitching_service rest_switching_service
 */

class RestSwitchingGtef_service extends MY_Service
{
    /**
     * @var
     */
    private $base_path = 'gadai-titipan-emas';

    /**
     * RestSwitchingGpoint_service constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->service('RestSwitching_service', 'rest_switching_service');
    }

    // Endpoint POST /gadai-titipan-emas/all
    public function listRekening($data)
    {
        $url = "$this->base_path/all";

        return $this->rest_switching_service->postData($url, $data);
    }

    public function cekStatus($data)
    {
        $url = "$this->base_path/cek-titipan";
        return $this->rest_switching_service->postData($url, $data);
    }

    public function getListGadaiTitipanEmas($request)
    {
        $url = "$this->base_path/get-list-gadai-titipan-emas";

        return $this->rest_switching_service->postData($url, $request);
    }

    public function getRestDetailGtef($data_Kontrak)
    {
        $url = "$this->base_path/get-titipan";
        
        return $this->rest_switching_service->postData($url, $data_Kontrak);
    }

    // Endpoint POST /gadai-titipan-emas/inquiry-open
    public function inquiryOpen($data)
    {
        $url = "$this->base_path/inquiry-open";

        return $this->rest_switching_service->postData($url, $data);
    }

    // Endpoint POST /gadai-titipan-emas/open
    public function open($data)
    {
        $url = "$this->base_path/open";

        return $this->rest_switching_service->postData($url, $data);
    }

    public function inquiryGtefPayment($data)
    {
        $url = "$this->base_path/inquiry";
    
        return $this->rest_switching_service->postData($url, $data);
    }

    public function cekNorekGtefPayment($data)
    {
        $url = "$this->base_path/cek-no-kontrak";
    
        return $this->rest_switching_service->postData($url, $data);
    }

    public function restCheckOnboarding($data)
    {
        $url = "$this->base_path/cek-cif";

        return $this->rest_switching_service->postData($url, $data);
    }
}