<?php

class RestSwitchingKrasidaTe_service extends MY_Service
{
    /**
     * RestSwitchingKrasidaTe_service constructor.
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->load->service('RestSwitching_service', 'rest_switching_service');
    }

    public function inquiryKrasidaTe(array $data)
    {
        $url = '/kte/inquiry';
        
        return $this->rest_switching_service->postData($url, $data);
    }

    public function openKrasidaTe(array $data)
    {
        $url = '/kte/open';
        
        return $this->rest_switching_service->postData($url, $data);
    }

    public function validateKrasidaTe(array $data)
    {
        $url = '/kte/validate';

        return $this->rest_switching_service->postData($url, $data);
    }

    public function listTenorKrasidaTe(array $data)
    {
        $url = '/kte/tenor-angsuran';

        return $this->rest_switching_service->postData($url, $data);
    }

    public function simulasiKrasidaTe(array $data)
    {
        $url = '/kte/simulasi';

        return $this->rest_switching_service->postData($url, $data);
    }
}
