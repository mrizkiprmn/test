<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

class RestLos_service extends MY_Service
{
    /**
     * @var
     */
    protected $client;
    protected $config_model;
    private $rc_success = '01';
    private $prefix_url;

    /**
     * RestLos_service constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('ConfigModel');
        $this->load->helper('Pegadaian');

        $this->config_model = new ConfigModel();
        $this->prefix_url = $this->config->item('los_p4d_prefix_url');
        $this->client = new Client([
            'base_uri' => $this->config->item('los_p4d_api_url'),
            'headers'  => [
                'Authorization' => $this->token(),
                'Content-Type'  => 'application/x-www-form-urlencoded',
            ]
        ]);
    }

    /**
     * Function postData to client api los
     *
     * @param $url
     * @param $data
     * @return ResponseInterface|null
     */
    public function postData($url, $data)
    {
        try {
            $client = $this->client->post($url, [RequestOptions::JSON => $data]);
            log_message('debug', 'RestLos_service url => ' . $url);
            log_message('debug', 'RestLos_service data => ' . json_encode($data));

            $result = $client->getBody()->getContents() ?? null;
            $result = !empty($result) ? json_decode($result) : null;

            if (!empty($result->error) && $result->error == 'Invalid Token') {
                $this->refreshToken();
                log_message('debug', 'RestLos_service  => ' . json_encode($result));
                Pegadaian::showError('Terjadi kesalahan pada sistem, silahkan coba beberapa saat lagi', $result);
            }

            return $result;
        } catch (BadResponseException $e) {
            log_message('debug', 'RestLos_service  => ' . json_encode($e->getMessage()));
            Pegadaian::showError('Terjadi kesalahan pada sistem, silahkan coba beberapa saat lagi', $e->getMessage());
        } catch (ConnectException $e) {
            log_message('debug', 'RestLos_service  => ' . json_encode($e->getMessage()));
            Pegadaian::showError('Terjadi kesalahan pada sistem, silahkan coba beberapa saat lagi', $e->getMessage());
        } catch (Exception $e) {
            log_message('debug', 'RestLos_service  => ' . json_encode($e->getMessage()));
            Pegadaian::showError('Terjadi kesalahan pada sistem, silahkan coba beberapa saat lagi', $e->getMessage());
        }

        return null;
    }

    // Endpoint LOS API tipe jaminan utama atau jaminan tambahan
    public function listGuarantee($key = 'jaminanUtama')
    {
        $result = null;
        try {
            $type = 'listtipejaminan';
            $url  = "$this->prefix_url/getDataMaster";
            $body['type'] = $type;

            if (!empty($response = $this->postData($url, $body))) {
                $result = $response;
            }

            return $this->setResponseData($result ?? null, $key);
        } catch (BadResponseException $exception) {
            return $result;
        }
    }

    // Endpoint LOS API tipe jaminan kendaraan
    public function listVehicleGuarantee()
    {
        try {
            $type = 'tipejaminankendaraan';
            $url  = "$this->prefix_url/getDataMaster";
            $body['type'] = $type;

            if (!empty($response = $this->postData($url, $body))) {
                $result = $response;
            }

            return $this->setResponseData($result ?? null, $type);
        } catch (BadResponseException $exception) {
            return null;
        }
    }

    // Endpoint LOS API simulasi produk
    public function matrixProduct($data)
    {
        try {
            $type = 'simulasiproduk';
            $url  = "$this->prefix_url/getDataMaster";
            $body = [
                'type'             => $type,
                'plafond'          => $data['loan_amount'] ?? '',
                'kepemilikanusaha' => $data['is_have_business'] ?? '',
                'tenor'            => $data['tenor'] ?? '',
                'adaagunan'        => $data['is_collateral'] ?? ''
            ];

            if (!empty($response = $this->postData($url, $body))) {
                $result = $response;
            }

            return $this->setResponseData($result ?? null, $type);
        } catch (BadResponseException $exception) {
            return null;
        }
    }

    // Endpoint LOS API simulasi kredit
    public function simulationCredit($data)
    {
        try {
            $type = 'simulasikredit';
            $url  = "$this->prefix_url/getDataMaster";
            $body = [
                'type'      => $type,
                'plafond'   => $data['loan_amount'] ?? '',
                'productId' => $data['code_product'] ?? ''
            ];

            if (!empty($response = $this->postData($url, $body))) {
                $result = $response;
            }

            return $this->setResponseData($result ?? null, $type);
        } catch (BadResponseException $exception) {
            return null;
        }
    }

    // Endpoint LOS API simulasi kredit
    public function inquiry($data)
    {
        try {
            $type = 'pengajuankredit';
            $url  = "$this->prefix_url/setData";
            $body = [
                'action' => $type,
                'userid' => $this->config->item('los_p4d_username'),
                'tc'     => '1.0',
                'data'   => $data
            ];

            if (!empty($response = $this->postData($url, $body))) {
                $result = $response;
            }

            return $this->setResponseData($result ?? null, 'responseData');
        } catch (BadResponseException $exception) {
            return null;
        }
    }

    /**
     * Function get token
     *
     * @return string|null
     * @throws Exception
     */
    protected function token()
    {
        $now = new DateTime();
        $config_los = $this->config_model->getLosP4dToken();

        if (!empty($config_los->value) && $config_los->last_update > $now->format('Y-m-d H:i:s')) {
            return $config_los->value;
        }

        return $this->refreshToken();
    }

    /**
     * Function RefreshToken LOS
     *
     * @return string|null
     * @throws Exception
     */
    protected function refreshToken()
    {
        $now  = new DateTime();
        $data = [
            'userid'   => $this->config->item('los_p4d_username'),
            'password' => $this->config->item('los_p4d_password'),
        ];

        $rest    = $this->getToken($data);
        $token   = $rest->access_token ?? null;
        $expires = $rest->expires_in ?? null;

        if (!empty($token) && !empty($expires)) {
            $days      = intval(intval($expires) / (3600 * 24));
            $update_at = date('Y-m-d', strtotime($now->format('Y-m-d'). ' + '.$days.' days'));
            $this->config_model->updateLosP4dAccessToken($token, $update_at);
        }

        return $token->access_token ?? null;
    }

    /**
     * Function get token call los
     *
     * @param $data
     * @return string
     */
    protected function getToken($data)
    {
        $client   = new Client(['base_uri' => $this->config->item('los_p4d_api_url')]);
        $response = null;
        try {
            $response = $client->post("/{$this->prefix_url}/userlogin", [
                RequestOptions::JSON => $data
            ]);
        } catch (BadResponseException $e) {
            log_message('debug', 'RestLos_service  => ' . json_encode($e->getMessage()));
            Pegadaian::showError('Terjadi Kesalahan Jaringan ke LOS', $e->getMessage());
        } catch (ConnectException $e) {
            log_message('debug', 'RestLos_service  => ' . json_encode($e->getMessage()));
            Pegadaian::showError('Terjadi Kesalahan Jaringan ke LOS', $e->getMessage());
        }

        if (!empty($response)) {
            $response = json_decode($response->getBody()->getContents());
        }

        return $response;
    }

    /**
     * Function setResponseData api los
     *
     * @param $response
     * @param $key
     * @return object|null
     */
    protected function setResponseData($response, $key)
    {
        $rc = $response->responseCode ?? null;

        if ($rc !== $this->rc_success) {
            return null;
        }

        log_message('debug', "RestLos_service {$key} response => " . json_encode($response));

        return $response->$key ?? null;
    }
}
