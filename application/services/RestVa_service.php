<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;

class RestVa_service extends MY_Service
{
    /**
     * @var
     */
    private $client;
    private $client_id;
    private $channel_id;
    private $helper_pegadaian;

    /**
     * RestSwitching_service constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $token = $this->token();

        if (empty($token)) {
            show_error('Token Rest Switching not found !', 400);
        }

        $this->client_id  = $this->config->item('core_post_username');
        $this->channel_id = '6017';
        $this->client     = new Client([
            'base_uri' => $this->config->item('core_VA_API_URL'),
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . $token,
                'Content-Type' => 'application/json'
            ],
        ]);
    }

    /**
     * Function PostData to restSwitching
     *
     * @param $url
     * @param $data
     * @param string $channel_id
     * @param string $client_id
     * @return mixed
     * @throws GuzzleException
     */
    public function postData($url, $data, $channel_id = '', $client_id = '')
    {
        try {
            if (empty($channel_id)) {
                $data['channelId'] = $this->channel_id;
            }

            if (empty($client_id)) {
                $data['clientId'] = $this->client_id;
            }

            log_message('debug', 'REQUEST REST VA URI :' . $url . ' => ' . json_encode($data));
            $rest_response = $this->client->request('POST', $url, [
                RequestOptions::JSON => $data,
            ]);
        } catch (ClientException $e) {
            $rest_response = $e->getResponse();
        } catch (ConnectException $e) {
            $rest_response = $e->getResponse();
        } catch (Exception $e) {
            $rest_response = $e->getMessage();
        }

        if (empty($rest_response)) {
            show_error('something went wrong when trying to access rest va', 500);
        }

        log_message('debug', 'REQUEST REST VA URI :' . $url . ' => ' . json_encode($rest_response));
        $response = !empty($rest_response->getBody()) ? json_decode($rest_response->getBody(), true) : $rest_response;
        log_message('debug', 'RESPONSE REST VA URI : ' . $url . ' => ' . json_encode($response));

        // Handle jika dari core tidak menyediakan field vaNumber
        if (!isset($response->data)) {
            return $response;
        }

        $_tempResponseData = json_decode($response->data, true);

        if (!isset($_tempResponseData['vaNumber'])) {
            $_tempResponseData['vaNumber'] = $_tempResponseData['virtualAccount'];
        }

        $response->data = json_encode($_tempResponseData);

        return $response;
    }

    /**
     * Function get core token
     *
     * @return string|null
     */
    private function token()
    {

        $this->load->service('RestSwitching_service');

        return $token = $this->RestSwitching_service->token();
    }
}
