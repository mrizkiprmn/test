<?php

use GuzzleHttp\Exception\GuzzleException;

/**
 * @property GcashModel gcash_model
 * @property User User
 * @property RestSwitchingGcash_service rest_switching_gcash_service
 */
class Gcash_service extends MY_Service
{
    protected $response = [
        'code' => 101,
        'status' => 'error',
        'message' => 'Internal Server Error',
        'data' => null
    ];
    
    public function __construct()
    {
        parent::__construct();

        $this->load->model('GcashModel', 'gcash_model');
        $this->load->service('RestSwitchingGcash_service', 'rest_switching_gcash_service');
    }

    /**
     * Method ini digunakan untuk mendapatkan data total saldo, VA, dan VA Available. Selain itu format response sudah disesuaikan dengan PDS API
     * @param $token: merupakan object yang berisi id dan channelId, etc.
     * @return array
     */
    public function getGcash($token)
    {
        $user = $this->User->getUser($token->id);

        $results = $this->getVirtualAccount($token, $user->no_hp);

        $vaAvailable = array_column($this->gcash_model->getBankParams() ?? [], 'kode');

        if (empty($results)) {
            $this->response['data'] = [
                "totalSaldo" => 0.0,
                "va" => [],
                "vaAvailable" => $vaAvailable
            ];
            return $this->response;
        }
        
        $db_va = $this->gcash_model->getVA($token->id);
        
        $vaAvailable = array_filter($vaAvailable, function ($item) use ($db_va) {
            return (!in_array($item, array_column($db_va, 'kodeBank')));
        });

        $this->response['code']    = 200;
        $this->response['status']  = 'success';
        $this->response['message'] = 'Check Data User';
        $this->response['data']    = [
            "totalSaldo" => $this->getReducedSaldoGCash($db_va, 'amount'),
            "va" => $db_va,
            "vaAvailable" => array_values($vaAvailable)
        ];

        return $this->response;
    }

    /**
     * Method ini digunakan untuk mendapatkan data VA dari rest switching
     * @param $token: merupakan object yang berisi id dan channelId, etc.
     * @param $channel_id: merupakan id channel (web/mobile)
     * @param $no_hp: merupakan nomor handpone pengguna
     * @param $flag: Dari kodingan sebelumnya kebanyakan menggunakan K, tetapi disini dibuat jadi argument agar mudah diubah
     * @return object
     */
    public function getVirtualAccount($token, $no_hp, $flag = 'K')
    {
        try {
            if ((!$token) || empty($no_hp)) {
                return [];
            }

            $rest_params = [
                'channelId' => $token->channelId,
                'noHp' => $no_hp,
                'flag' => $flag
            ];
            
            $rest_gcash_rekening = $this->rest_switching_gcash_service->gcashRekening($rest_params);

            if ($rest_gcash_rekening['responseCode'] !== '00') {
                return $this->getCachedVA($token->id);
            }

            $response = json_decode($rest_gcash_rekening['data']);

            $results = $this->getConvertedObject($token->id, $response->listRekening ?? []);
            $this->gcash_model->saveOrUpdateVA($results);

            $response->listRekening = $results;
            
            return $response;
        } catch (GuzzleException $e) {
            log_message('debug', 'END ' . __FUNCTION__ . ' => ' . $e->getMessage());
            return [];
        }
    }

    /**
     * Method ini digunakan untuk penyesuaian nama key seperti nama column di DB
     * @param $token_id: merupakan id yang digenerate saat login
     * @param $results: merupakan sebuah array of object yang setiap objectnya harus mengandung key 'saldoAkhir', 'tglExpired', 'vaNumber', dan 'kodeBank'
     * @return object
     */
    protected function getConvertedObject($token_id, $results)
    {
        if (!$token_id) {
            return [];
        }

        $results = array_filter($results, function ($data) {
            $keys = ['saldoAkhir', 'tglExpired', 'vaNumber', 'kodeBank'];
            return !array_diff_key(array_flip($keys), (array) $data);
        });

        log_message('debug', 'Results: '.json_encode($results));

        return array_map(function ($data) use ($token_id) {
            return (object) [
                'user_AIID' => $token_id,
                'amount' => $this->getGcashBalance($data),
                'tglExpired' => $data->tglExpired,
                'virtualAccount' => $data->vaNumber,
                'kodeBank' => $data->kodeBank,
                'lastUpdate' => date('Y-m-d H:i:s')
            ];
        }, $results);
    }

    protected function getReducedSaldoGCash($list, $key)
    {
        return array_reduce($list, function ($acc, $item) use ($key) {
            $item = (array) $item;
            $acc += $item[$key];
            return $acc;
        }, 0);
    }

    protected function getCachedVA($token_id)
    {
        $results = $this->gcash_model->getVA($token_id);
        $user = $this->User->getUser($token_id);

        $va = new stdClass();
        $va->noHp = $user->no_hp;
        $va->listRekening = $results;

        return $va;
    }

    protected function getGcashBalance($data)
    {
        $balance = 0.00;
        $data    = [
            'bankCode' => $data->kodeBank,
            'flag' => 'K',
            'vaNumber' => $data->vaNumber
        ];

        $rest_gcash_balance = $this->rest_switching_gcash_service->gCashBalance($data);
        $gcash_balance = $rest_gcash_balance['data'] ?? null;

        if (!empty($gcash_balance)) {
            $data = json_decode($gcash_balance);


            $balance = $data->balance ?? $balance;
        }

        return $balance;
    }
}
