<?php

use GuzzleHttp\Exception\GuzzleException;

class RestSwitchingCustomer_service extends MY_Service
{
    /**
     * RestSwitchingGpoint_service constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->service('RestSwitching_service', 'rest_switching_service');
    }

    /**
     * Endpoint POST /customer/inquiry
     *
     * @param array $data
     * @return mixed
     */
    public function customerInquiry(array $data)
    {
        $url = '/customer/inquiry';

        return $this->rest_switching_service->postData($url, $data);
    }

    /**
     * Endpoint POST /customer/reggte
     *
     * @param array $data
     * @return mixed
     */
    public function customerReggte(array $data)
    {
        $url = '/customer/reggte';

        return $this->rest_switching_service->postData($url, $data);
    }

    /**
     * Endpoint POST /customer/activation
     *
     * @param array $data
     * @return mixed
     */
    public function customerActivation(array $data)
    {
        $url = '/customer/activation';

        return $this->rest_switching_service->postData($url, $data);
    }

    /**
     * Endpoint POST /customer/fulldata
     *
     * @param array $data
     * @return mixed
     */
    public function getCustomerData(array $data)
    {
        $url = '/customer/fulldata';

        return $this->rest_switching_service->postData($url, $data);
    }
}
