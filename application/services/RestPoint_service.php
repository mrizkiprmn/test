<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Psr\Http\Message\ResponseInterface;

class RestPoint_service extends MY_Service
{
    /**
     * @var
     */
    private $url;
    private $username;
    private $password;
    private $prefix;
    private $client;

    /**
     * RestGpoint_service constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->url      = $this->config->item('point_api_url');
        $this->username = $this->config->item('point_api_username');
        $this->password = $this->config->item('point_api_password');
        $this->prefix   = $this->config->item('point_api_prefix');

        $this->client = new Client([
            'base_uri' => $this->url,
            'headers' => [
                'Authorization' => $this->getAuthorization(),
                'Content-Type' => 'application/json',
            ]
        ]);
    }

    /**
     * @param $url
     * @param array $query
     * @return ResponseInterface
     */
    public function getData($url, $query = [])
    {
        $result = null;

        try {
            $data = ['query' => $query, 'verify' => false];
            $rest = $this->client->get("$this->prefix/$url", $data);

            $response = $rest->getBody()->getContents();
        } catch (BadResponseException $e) {
            $response = $e->getMessage();
        }

        $result = json_decode($response);

        log_message('debug', 'start of CALL RestGpoint URL : ' . $url, $data);
        log_message('debug', 'end of CALL RestGpoint URL : ' . $url, $result ?? $response);

        return  $result;
    }

    /**
     * @param $cif
     * @return mixed|null
     */
    public function getCustomerTotalPoint($cif)
    {
        $uri = "nasabah/total_point";

        return $this->getData($uri, ['cif_number' => $cif]);
    }

    public function getAuthorization()
    {
        if (empty($this->username) && empty($this->password)) {
            return null;
        }

        return base64_encode("$this->username:$this->password");
    }
}
