<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class RestZeroBounce_service extends MY_Service
{
    /**
     * @var
     */
    private $client;
    private $config_client;

    /**
     * RestZeroBounce_service constructor.
     */
    public function __construct()
    {
        parent::__construct();
        // load config client url, api key and active
        $this->configZeroBounce();
        // setup client constructor
        $this->client = new Client(['base_uri' => $this->config_client->zero_bounce_url]);
    }

    public function getData($url, array $query = [])
    {
        log_message('debug', 'Request RestZeroBounce_service URI : ' . $url . ' => ' . json_encode($query));

        try {
            $query['api_key'] = $this->config_client->zero_bounce_api_key;

            $rest_response = $this->client->request('GET', $url, ['query' => $query]);
        } catch (GuzzleException $e) {
            $e_message = $e->getMessage();
        }

        $response = !empty($rest_response) ? json_decode($rest_response->getBody(), true) : null;
        $result   = [
            'status'  => !empty($e_message) ? 'error' : 'success',
            'message' => !empty($e_message) ? $e_message : 'Validate Email Zero Bounce Successfully',
            'data'    => $response
        ];

        log_message('debug', 'Response RestZeroBounce_service URI : ' . $url . ' => ' . json_encode($result));

        return $result;
    }

    public function validateEmail($email)
    {
        if ($this->config_client->zero_bounce_is_dummy == 1) {
            return $this->validateEmailDummy($email);
        }

        return $this->getData('validate', ['email' => $email, 'ip_address' => '']);
    }

    protected function validateEmailDummy($email)
    {
        $parts = explode("@", $email);

        $response = [
            'address' => $email,
            'status' => 'valid',
            'sub_status' => null,
            'free_email' => null,
            'did_you_mean' => null,
            'account' => $parts[0] ?? null,
            'domain' => $parts[1] ?? null,
            'domain_age_days' => null,
            'smtp_provider' => null,
            'mx_found' => null,
            'mx_record' => null,
            'firstname' => null,
            'lastname' => null,
            'gender' => null,
            'country' => null,
            'region' => null,
            'city' => null,
            'zipcode' => null,
            'processed_at' => date('d.m.Y H:i:s'),
            'is_dummy' => true,
        ];

        $result = [
            'status' => 'success',
            'message' => 'Dummy Validate Email Zero Bounce Successfully',
            'data' => $response
        ];

        log_message('debug', 'Response RestZeroBounce_service URI : ' . 'validate' . ' => ' . json_encode($result));

        return $result;
    }

    private function configZeroBounce()
    {
        $this->config_client = new stdClass();
        $this->config_client->zero_bounce_url = $this->config->item('zero_bounce_url');
        $this->config_client->zero_bounce_prefix = $this->config->item('zero_bounce_prefix');
        $this->config_client->zero_bounce_api_key = $this->config->item('zero_bounce_api_key');
        $this->config_client->zero_bounce_is_dummy = $this->config->item('zero_bounce_is_dummy');

        if (empty($this->config_client->zero_bounce_url) && empty($this->config_client->zero_bounce_api_key)) {
            Pegadaian::showError('Zero Bounce Client URL and API KEY not setup, please setup your .env');
        }

        return $this->config_client;
    }
}
