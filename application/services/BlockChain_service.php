<?php

/**
 * @property RestBlockChain_service rest_block_chain_service
 * @property PaymentModel payment_model
 * @property Response_service response_service
 */
class BlockChain_service extends MY_Service
{
    /**
     * @var array
     */
    protected $response = [
        'code'    => 101,
        'status' => 'error',
        'message' => 'Terjadi kesalahan mohon coba beberapa saat lagi.',
        'data' => null
    ];

    public function __construct()
    {
        parent::__construct();

        $this->load->model('PaymentModel', 'payment_model');

        $this->load->service('Response_service', 'response_service');
        $this->load->service('RestBlockChain_service', 'rest_block_chain_service');
    }

    public function storeGold($reff_switching)
    {
        $this->response['message'] = 'Data tidak ditemukan';
        $payment = $this->payment_model->getPaymentByTrxId($reff_switching);

        if (empty($payment)) {
            return $this->response;
        }

        if ($payment->is_paid != '1') {
            $this->response['message'] = 'Data flag is_paid is false';
            return $this->response;
        }

        $payload = $this->mappingDataGold($payment);

        if (empty($payload)) {
            return $this->response;
        }

        $payload['channel_id'] = env('BLOCK_CHAIN_CHANNEL_ID');
        $payload['reff_switching'] = $payment->id_transaksi;
        $payload['reff_core']  = !empty($payment->reffCore) ? $payment->reffCore : '';

        $this->response = $this->response_service->getResponse('general.succeeded');
        $this->response['message'] = 'Store Data BlockChain';
        $this->response['data'] = $payload;

        return $this->response;
    }

    protected function postGoldData($data)
    {
        $url = '/api';

        $this->rest_block_chain_service->post($url, $data);
    }

    protected function mappingDataGold($payment)
    {
        if ($payment->jenis_transaksi == 'SL') {
            return self::topUpGold($payment);
        }

        if ($payment->jenis_transaksi == 'BB') {
            return self::buyBackGold($payment);
        }

        if ($payment->jenis_transaksi == 'OP') {
            return self::openGold($payment);
        }

        if ($payment->jenis_transaksi == 'TR') {
            return self::transferGold($payment);
        }

        if ($payment->jenis_transaksi == 'OD') {
            return self::printGold($payment);
        }

        return null;
    }

    private function topUpGold($payment)
    {
        return [
            'no_rekening' => $payment->no_rekening,
            'gram_transaksi' => $payment->gram,
            'nilai_transaksi' => $payment->nilaiTransaksi,
            'harga' => $payment->harga,
            'satuan' => $payment->satuan,
        ];
    }

    private function buyBackGold($payment)
    {
        return [
            'no_rekening' => $payment->norek,
            'gram_transaksi' => $payment->gramTransaksi,
            'nilai_transaksi' => $payment->nilaiTransaksi,
            'harga' => $payment->hargaBeli,
        ];
    }

    private function openGold($payment)
    {
        return [
            'no_rekening' => $payment->norek,
            'nama_nasabah' => $payment->namaNasabah,
            'gram_transaksi' => $payment->gram,
        ];
    }

    private function transferGold($payment)
    {
        return [
            'no_rekening' => $payment->norek,
            'no_rekening_tujuan' => $payment->norekTujuan,
            'gram_transaksi' => $payment->saldoEmasTujuan,
        ];
    }

    private function printGold($payment)
    {
        return [
            'total_keping' => $payment->totalKeping,
            'total_berat' => $payment->totalBerat,
            'total_biaya' => $payment->totalKewajiban,
            'no_buku' => $payment->noBuku,
        ];
    }
}
