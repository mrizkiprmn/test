<?php


/**
 * @property RestCore_Service rest_core_service
 */
class RestCoreTabungan_service extends MY_Service
{
    private $prefix_url = 'konven/tabungan';
    /**
     * RestCoreKonven_service constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->service('RestCore_Service', 'rest_core_service');
    }

     // Endpoint POST /konven/gadai/by/cif
    public function byCif(array $data)
    {
        $url = "{$this->prefix_url}/byCif";

        return $this->rest_core_service->postData($url, $data);
    }
}
