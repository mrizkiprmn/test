<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\RequestOptions;

class RestBlockChain_service extends MY_Service
{
    // Private @var
    private $block_chain_url;

    public function __construct()
    {
        parent::__construct();

        $this->load->helper('Pegadaian');

        self::validationConfig();
    }

    public function post($url, $data)
    {
        $rest_response = null;

        try {
            $headers = [
                'Accept'       => 'application/json',
                'Content-Type' => 'application/json'
            ];

            $rest_response = $this->client($this->block_chain_url, $headers)
                ->post($url, [RequestOptions::JSON => $data]);
        } catch (BadResponseException $e) {
            $rest_response = $e->getResponse();
        } catch (ConnectException $e) {
            $rest_response = $e->getResponse();
        } catch (Exception $e) {
            Pegadaian::showError('Exception BlockChain client', $e->getMessage());
        }

        $response = json_decode($rest_response->getBody(), true);

        log_message('debug', 'start of CALL BlockChain URL : ' . $url, $data);
        log_message('debug', 'end of CALL BlockChain URL : ' . $url, $response);

        return $response;
    }

    private function validationConfig()
    {
        $this->block_chain_url = getenv('BLOCK_CHAIN_URL') ?? false;

        if (empty($this->block_chain_url)) {
            Pegadaian::showError('Environment Block Chain config not setup !');
        }

        return true;
    }

    private function client($base_url, $headers = [])
    {
        $headers['Content-Type'] = 'application/x-www-form-urlencoded';

        return new Client([
            'base_uri' => $base_url,
            'headers' => $headers,
            'verify' => false,
        ]);
    }
}
