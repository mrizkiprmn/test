<?php

/**
 * @property Response_service response_service
 * @property RestSwitchingSimulasi_service rest_switching_simulasi_service
 */
class Mulia_service extends MY_Service
{
    protected $response = [
        'code' => 101,
        'status' => 'error',
        'message' => 'Internal Server Error',
        'data' => null
    ];

    public function __construct()
    {
        parent::__construct();

        $this->load->model('ConfigModel', 'config_model');
        $this->load->model('GpoinModel', 'gpoin_model');
        $this->load->model('MasterModel', 'master_model');
        $this->load->model('MuliaModel', 'mulia_model');

        $this->load->service('Gcash_service', 'gcash_service');
        $this->load->service('Notification_service', 'notification_service');
        $this->load->service('RestSwitchingPayment_service', 'restswitching_payment_service');
        $this->load->service('RestSwitchingPaymentEmas_service', 'rest_switching_payment_emas_service');
        $this->load->service('RestSwitchingPaymentEmas_service', 'rest_switching_payment_emas_service');
        $this->load->service('RestSwitchingSimulasi_service', 'rest_switching_simulasi_service');
    }

    public function validatePaymentGcash($token, $idTransaksi)
    {
        $this->response['status']  = 'error';
        $this->response['message'] = 'Data transaksi tidak ditemukan';
        $this->response['data']    = '';
        $this->response['code'] = '102';
        
        $gpoin = $this->gpoin_model->get_promo($idTransaksi);
        
        $checkPayment = $this->mulia_model->getPaymentByTrxId($idTransaksi, $gpoin);
        
        if (empty($checkPayment)) {
            return $this->response;
        }
        
        $response = $this->gcash_service->getGcash($token);
        
        log_message('debug', 'Get data for nominal validation:' . json_encode($response));

        if ($response['code'] != 200) {
            log_message('debug', __FUNCTION__ . ' Validasi Pembayaran Mulia error ' . $response['message']);

            $this->response['message'] = $response['message'];

            return $this->response;
        }

        if ($response['data']['totalSaldo'] < $checkPayment->totalKewajiban) {
            log_message('debug', __FUNCTION__ . ' Validasi Pembayaran Mulia error ' . 'Saldo tidak mencukupi');

            $this->response['message'] = 'Saldo tidak mencukupi';
            $this->response['data']    = ['totalSaldo' => $response['data']['totalSaldo']];

            return $this->response;
        }

        $this->response['status']  = 'success';
        $this->response['message'] = 'Saldo mencukupi';

        log_message('debug', __FUNCTION__ . ' Validasi Pembayaran Mulia ' . 'End');
        return $this->response;
    }

    //Khusus Pembayaran Maybank
    public function muliaPayment($payment, $checkPayment, $idTransaksi, $gpoin, $user, $token, $kodeBank)
    {
    
        $biayaTransaksi = $this->config_model->getRealBiayaPayment($checkPayment->jenisTransaksi, 'BANK', '37', $kodeBank);

        $data = array(
            'amount' => $checkPayment->totalKewajiban + $biayaTransaksi,
            'channelId' => $token->channelId,
            'clientId' => $this->config->item('core_post_username'),
            'customerEmail' => $user->email,
            'customerName' => $user->nama,
            'customerPhone' => $user->no_hp,
            'flag' => 'K',
            'jenisTransaksi' => 'OP',
            'keterangan' => "PDSOP " . $idTransaksi . " Mulia",
            'kodeProduk' => '37',
            'kodeBank' => $kodeBank,
            'reffSwitching' => $checkPayment->reffSwitching,
            'norek' => $checkPayment->tenor
        );

        $billingMayBank = $this->rest_switching_payment_emas_service->createBillingVAMayBank($data, '');

        if ($billingMayBank['responseCode'] != '00') {
            $this->response['message'] = $billingMayBank['responseDesc'];
            return $this->response;
        }
 
        $dataPrefix = $this->master_model->getPrefixBank('prefixMaybank');
        $billingData = json_decode($billingMayBank['data']);
        $virtualAccount = $billingData->vaNumber;
        $virtualAccountFormat = $dataPrefix . substr($virtualAccount, 0, 11);
        $tglExpired = $billingData->tglExpired;

        $updatePaymentData = array(
            'tipe' => $payment,
            'virtualAccount' => $virtualAccount,
            'tglExpired' => $tglExpired,
            'payment' => $payment,
            'biayaTransaksi' => $biayaTransaksi,
            'kodeBankPembayar' => $kodeBank,
            'channelId' => $token->channelId
        );

        $this->mulia_model->updatePayment($idTransaksi, $updatePaymentData);

        $dateExpired = new DateTime($tglExpired);

        // Get Cabang Detail
        $outlet = $this->master_model->getSingleCabang($checkPayment->kodeOutlet);

        $templateData = [
            'nama'           => $token->nama,
            'namaNasabah'    => $token->nama,
            'payment'        => $payment,
            'nama'           => $token->nama,
            'va'             => $virtualAccountFormat,
            'totalKewajiban' => $checkPayment->hargaSetelahDiscount,
            'tglExpired'     => $dateExpired->format('d/M/Y H:i:s'),
            'tglExpiredUnformated' => $tglExpired,
            'reffSwitching'  => $idTransaksi,
            'trxId'          => $idTransaksi,
            'biayaTransaksi' => $biayaTransaksi,
            'namaCabang'     => $outlet->nama,
            'alamatCabang'   => $outlet->alamat . ', ' . $outlet->kelurahan . ', ' . $outlet->kecamatan . ', ' . $outlet->kabupaten . ', ' . $outlet->provinsi,
            'telpCabang'     => $outlet->telepon,
            'item'           => json_decode($checkPayment->kepingMuliaJson),
            'tglTransaksi'   => $checkPayment->tglTransaksi,
            'totalHarga'     => $checkPayment->totalHarga,
            'administrasi'   => $checkPayment->administrasi,
            'uangMuka'       => $checkPayment->nominalUangMuka,
            'biayaTransaksi' => $biayaTransaksi,
            'jenisLogamMulia' => $checkPayment->jenisLogamMulia,
            'realTglTransaksi' => $checkPayment->realTglTransaksi,
            'pokokPembiayaan' => $checkPayment->pokokPembiayaan,
            'tenor' => $checkPayment->tenor,
            'angsuran' => $checkPayment->angsuran,
            // Promo
            'promoCode'       => $checkPayment->promoCode,
            'discountAmount'  => $checkPayment->discountAmount
        ];

        $this->notification_service->notificationCicilPayment($token, $templateData);

        $dataPayment = array(
            'idTransaksi' => $idTransaksi,
            'virtualAccount' => $virtualAccountFormat,
            'expired' => $tglExpired,
            'now' => date('Y-m-d H:i:s'),
        );

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Mohon lakukan pembayaran ke no rekening yang tertera';
        $this->response['data'] = $dataPayment ?? null;

        return $this->response;
    }

    //Khusus Pembayaran FINPAY
    public function finpay_payment($payment, $check_payment)
    {
        $check_payment->biaya_transaksi = $this->config_model->getRealBiayaPayment($check_payment->jenisTransaksi, 'BANK', '37', $check_payment->kode_bank);
        $check_payment->payment = $payment;
        $amount = $check_payment->totalKewajiban;

        // max_transaksi 10.000.000, max_transaksi_admin 10.001.500
        $max_transaksi = $this->config_model->whereByVariable('max_transaksi_finpay');
        $max_transaksi_msg = Pegadaian::currencyIdr($max_transaksi->value);
        $max_transaksi_admin = $max_transaksi->value + $check_payment->biaya_transaksi;

        if ($amount > $max_transaksi->value && $amount < $max_transaksi_admin || $amount > $max_transaksi_admin) {
            $this->response['message'] = "Transaksi tidak dapat diproses, Maksimal transaksi $max_transaksi_msg";
            return $this->response;
        }

        $data = [
            'amount' => strval($amount + $check_payment->biaya_transaksi),
            'channelId' => $check_payment->data_token->channelId,
            'clientId' => $this->config->item('core_post_username'),
            'customerEmail' => $check_payment->data_user->email,
            'customerName' => $check_payment->data_user->nama,
            'customerPhone' => $check_payment->data_user->no_hp,
            'flag' => 'K',
            'jenisTransaksi' => 'OP',
            'keterangan' => "PDSOP " . $check_payment->idTransaksi . " Mulia",
            'norek' => '',
            'productCode' => '37',
            'trxId' => $check_payment->reffSwitching
        ];

        $billing_virtual = $this->restswitching_payment_service->createBillingFinPay($data);

        if ($billing_virtual['responseCode'] != '00') {
            $this->response['message'] = $billing_virtual['responseDesc'];
            return $this->response;
        }

        $check_payment->data_billing = json_decode($billing_virtual['data']);
        $check_payment->tgl_expired = date('Y-m-d H:i:s', time() + (60 * 180));

        $update_payment_data = [
            'tipe' => $check_payment->payment,
            'virtualAccount' => $check_payment->data_billing->payment_code,
            'tglExpired' => $check_payment->tgl_expired,
            'payment' => $check_payment->payment,
            'biayaTransaksi' => $check_payment->biaya_transaksi,
            'kodeBankPembayar' => $check_payment->kode_bank,
            'channelId' => $check_payment->data_token->channelId
        ];

        $this->mulia_model->updatePayment($check_payment->idTransaksi, $update_payment_data);

        $check_payment->date_expired = new DateTime($check_payment->tgl_expired);
        $check_payment->data_outlet = $this->master_model->getSingleCabang($check_payment->kodeOutlet);
        $check_payment->redaksi_payment = $this->config_model->getRedaksiPayment($check_payment->kode_bank, $check_payment->data_billing->payment_code, $check_payment->hargaSetelahDiscount);

        $template_data = $this->notification_service->template_data_mulia_notifkasi($check_payment);

        $this->notification_service->notificationCicilPayment($check_payment->data_token, $template_data);

        $data_payment = [
            'idTransaksi' => $check_payment->idTransaksi,
            'virtualAccount' => $check_payment->data_billing->payment_code,
            'expired' => $check_payment->tgl_expired,
            'now' => date('Y-m-d H:i:s'),
        ];

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Mohon lakukan pembayaran ke no rekening yang tertera';
        $this->response['data'] = $data_payment ?? null;

        return $this->response;
    }

    public function simulation($request)
    {
        $uang_muka   = $request['uang_muka'];
        $total_harga = $request['total_harga'];
        $list_denom  = json_decode($request['item'], true) ?? [];

        foreach ($list_denom as $denom) {
            $total_gram  = $total_gram ?? 0;
            $total_gram += $denom['gram'] * $denom['qty'];
            $denoms[]    = ['keping' => (string) $denom['qty'], 'denom' => (string) $denom['gram']];
        }

        $uang_muka_repiah = ($uang_muka / $total_harga) * 100;
        $uang_muka_percent_round = round($uang_muka_repiah);
        $uang_muka_hasil_percent_round = ($uang_muka_percent_round / 100) * $total_harga;
        $uang_muka_tunai_decimal = round($uang_muka_hasil_percent_round);

        $payload = [
            'uangMuka'  => $uang_muka_percent_round,
            'idVendor'  => $request['vendor'],
            'listDenom' => $denoms ?? [],
            'tenor'     => $request['tenor'],
            'flag'      => 'K',
        ];

        $simulasi = $this->rest_switching_simulasi_service->muliaSimulasi($payload);

        if ($simulasi['responseCode'] != '00') {
            $this->response['message'] = $simulasi['responseMessage'] ?? $this->response['message'];
            $this->response['data']    = $simulasi['responseData'] ?? $this->response['data'];
            return $this->response;
        }

        $data = json_decode($simulasi['data']);
        $data->totalGramLM = $total_gram ?? 0;
        $data->uangMukaRound = $uang_muka_tunai_decimal;

        $response = $this->response_service->getResponse('general.succeeded');
        $response['message'] = 'Simulasi Mulia successfully';
        $response['data'] = $data ?? null;

        return $response;
    }
}
