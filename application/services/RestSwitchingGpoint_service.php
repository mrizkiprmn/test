<?php

class RestSwitchingGpoint_service extends MY_Service
{
    /**
     * RestSwitchingGpoint_service constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->service('RestSwitching_service', 'rest_switching_service');
    }

    // Endpoint POST /gpoin/cektransaksi
    public function cekTransaksi($data)
    {
        $url = '/gpoin/cektransaksi';

        return $this->rest_switching_service->postData($url, $data);
    }

    // Endpoint POST /gpoin/vouchers/list
    public function vocherList(array $data)
    {
        $url = '/gpoin/vouchers/list';

        return $this->rest_switching_service->postData($url, $data);
    }

    // Endpoint POST /gpoin/promo/list
    public function promoList(array $data)
    {
        $url = '/gpoin/promo/list';

        return $this->rest_switching_service->postData($url, $data);
    }

    // Endpoint POST /gpoin/get/milestone
    public function mileStone($data)
    {
        
         $url = '/gpoin/get/milestone';

         return $this->rest_switching_service->postData($url, $data);
    }

    // Endpoint POST /gpoin/vouchers/history
    public function voucherHistory($data)
    {
        $url = '/gpoin/vouchers/history';

        return $this->rest_switching_service->postData($url, $data);
    }

    // Endpoint POST /gpoin/referral/rank
    public function referralRank($data)
    {
        $url=  '/gpoin/referral/rank';

        return $this->rest_switching_service->postData($url, $data);
    }

    // Endpoint POST /gpoin/rewards/rejected
    public function rewardsReject($data)
    {
        $url=  '/gpoin/rewards/rejected';

        return $this->rest_switching_service->postData($url, $data);
    }
}
