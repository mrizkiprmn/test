<?php

use GuzzleHttp\Exception\GuzzleException;

class RestSwitchingPaymentEmas_service extends MY_Service
{
    /**
     * RestSwitchingPaymentEmas_service constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->service('RestSwitching_service', 'rest_switching_service');
    }

    public function createBillingVAMayBank($data, $noRek)
    {
        $url = '/va/create';

        if ($data['jenisTransaksi'] != 'OP') {
            $data['norek'] = $noRek;
        }
       
        return $this->rest_switching_service->postData($url, $data);
    }

    function getSaldo($norek)
    {
        
        $saldo = array(
            'saldo' => 0,
            'saldoAkhir' => 0,
            'saldoBlokir' => 0,
            'saldoEfektif' => 0
        );
        
        $detailTabungan = $this->detailTabunganEmas($norek, '6017');
        if ($detailTabungan['responseCode'] == '00') {
            if (isset($detailTabungan['data']) && $detailTabungan['data'] != null) {
                $portof = json_decode($detailTabungan['data']);
                $saldo['saldo'] = $portof->saldo - $portof->saldoBlokir;
                $saldo['saldoAkhir'] = $portof->saldo;
                $saldo['saldoBlokir'] = $portof->saldoBlokir;
                $saldo['saldoEfektif'] = $portof->saldoEfektif;
            }
        }
        return $saldo;
    }

    function detailTabunganEmas($norek, $channelId = '6017')
    {
        $url = '/portofolio/dettabemas';
        $data = array(
            'channelId' => $channelId,
            'clientId' => $this->config->item('core_post_username'),
            'norek' => $norek
        );
        return $this->rest_switching_service->postData($url, $data);
    }
}
