<?php

/**
 * @property RestSwitching_service rest_switching_service
 */
class RestSwitchingPortofolio_service extends MY_Service
{
    /**
     * RestSwitchingGpoint_service constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->service('RestSwitching_service', 'rest_switching_service');
    }

     // Endpoint POST /portofolio/pds/checkopentabemas
    public function pdsCheckOpenTabEmas($data)
    {
        $url = '/portofolio/pds/checkopentabemas';

        return $this->rest_switching_service->postData($url, $data);
    }

    /**
     * Endpoint POST /portofolio/pds/tabemas
     *
     * @param $data
     * @return mixed
     */
    public function pdsTabEmas($data)
    {
        $url = '/portofolio/pds/tabemas';

        return $this->rest_switching_service->postData($url, $data);
    }

    /**
     * Endpoint POST /portofolio/titipanemas
     *
     * @param $data
     * @return mixed|null
     */
    public function portofolioTitipanEmas($data)
    {
        $url = '/portofolio/titipanemas';

        return $this->rest_switching_service->postData($url, $data);
    }

    public function transaksiTabEmas($data)
    {
        $url = "/portofolio/trxtabemas";

        return $this->rest_switching_service->postData($url, $data);
    }

    // Endpoint POST portofolio/mtonline
    public function portofolioMtonline($data)
    {
        $url = "portofolio/mtonline";

        return $this->rest_switching_service->postData($url, $data);
    }

    public function portofolioSisaLimitMtOnline($data)
    {
        $url = "portofolio/getsisalimitmtonline";

        return $this->rest_switching_service->postData($url, $data);
    }

    public function portofolioMtonlineDetail($payload)
    {
        $url = "/portofolio/detpinjamanmtonline";

        return $this->rest_switching_service->postData($url, $payload);
    }

    public function detailTabunganEmas($data)
    {
        $url = '/portofolio/dettabemas';

        return $this->rest_switching_service->postData($url, $data);
    }

    public function pdsTrxTabEmas($data)
    {
        $url = "/portofolio/pds/trxtabemas";

        return $this->rest_switching_service->postData($url, $data);
    }

    public function pinjaman($data)
    {
        $url = "/portofolio/pinjaman";

        return $this->rest_switching_service->postData($url, $data);
    }

    public function detailRiwayatKrasidaTe(array $data)
    {
        $url = '/portofolio/kte/dettrx';

        return $this->rest_switching_service->postData($url, $data);
    }

    public function portofolio_pinjaman($data)
    {
        $url = '/portofolio/pinjaman';

        return $this->rest_switching_service->postData($url, $data);
    }
}
