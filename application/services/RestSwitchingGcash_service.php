<?php

/**
 * @property RestSwitching_service rest_switching_service
 */
class RestSwitchingGcash_service extends MY_Service
{
    /**
     * RestSwitchingMpo_service constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->service('RestSwitching_service', 'rest_switching_service');
    }

    // Endpoint POST /gcash/balance
    public function gCashBalance(array $data)
    {
        $url = '/gcash/balance';

        return $this->rest_switching_service->postData($url, $data);
    }

    // Endpoint POST /gcash/balance
    public function gCashInquiry(array $data)
    {
        $url = '/gcash/balance';

        return $this->rest_switching_service->postData($url, $data);
    }

    /**
     * Endpoint POST /gcash/rekening
     *
     * @param $data
     * @return mixed
     * @throws GuzzleException
     */
    public function gcashRekening($data)
    {
        $url = '/gcash/rekening';

        return $this->rest_switching_service->postData($url, $data);
    }
}
