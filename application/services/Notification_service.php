<?php

/**
 * @property ApplyMtonlineModel mtonline_model
 * @property EmasModel emas_model
 * @property GpoinModel gpoin_model
 * @property ConfigModel config_model
 * @property MasterModel master_model
 * @property MuliaModel mulia_model
 * @property NotificationModel notification_model
 * @property PaymentModel payment_model
 * @property TransactionModel transaction_model
 * @property User user_model
 */
class Notification_service extends MY_Service
{
    const SUCCESS_SENT = 'Notification successfully sent';
    const USER_NOT_FOUND = 'Data user tidak ditemukan';
    const NOTIF_DATE_FORMAT = 'd/M/Y h:i';
    const RESPONSE_SUCCESS_STRING = 'general.succeeded';

    protected $pegadaian_helper;
    protected $response = [
        'code' => 101,
        'status' => 'error',
        'message' => 'Internal Server Error',
        'data' => null
    ];

    public function __construct()
    {
        parent::__construct();

        $this->load->model('GpoinModel', 'gpoin_model');
        $this->load->model('MuliaModel', 'mulia_model');
        $this->load->model('NotificationModel', 'notification_model');
        $this->load->model('PaymentModel', 'payment_model');
        $this->load->model('EmasModel', 'emas_model');
        $this->load->model('User', 'user_model');
        $this->load->model('TransactionModel', 'transaction_model');
        $this->load->model('ApplyMtonlineModel', 'mtonline_model');
        $this->load->model('MikroModel', 'mikro_model');
        $this->load->model('GadaiModel', 'gadai_model');
        $this->load->model('ConfigModel', 'config_model');
        $this->load->model('MasterModel', 'master_model');
        $this->load->model('TransactionGtefModel', 'transaction_gtef_model');
        $this->load->service('Payment_service', 'payment_service');
        $this->load->service('God_service', 'god_service');
        $this->load->service('Cache_service', 'cache_service');
        $this->load->service('Response_service', 'response_service');
        $this->load->helper('Pegadaian');
        $this->load->helper('Message');

        $this->pegadaian_helper = new Pegadaian();
    }

    public function referrerNotification($reff_switching)
    {
        $this->response['code'] = 200;

        $gpoint = $this->gpoin_model->get_promo($reff_switching);
        if (empty($gpoint)) {
            $this->response['status'] = 'Error';
            $this->response['message'] = 'Data gpoin tidak ditemukan';

            return $this->response;
        }


        $payment = $this->payment_service->getPaymentByReffSwitching($reff_switching);

        if (empty($payment)) {
            $this->response['status'] = 'Error';
            $this->response['message'] = 'Data payment tidak ditemukan';

            return $this->response;
        }

        $user_referrer = $this->user_model->CheckRef($gpoint->promoCode);
        if (empty($user_referrer)) {
            $this->response['status'] = 'Error';
            $this->response['message'] = 'Data user referrer tidak ditemukan';

            return $this->response;
        }

        $user_referral = $this->user_model->getUser($payment->user_AIID);
        if (empty($user_referral)) {
            $this->response['status'] = 'Error';
            $this->response['message'] = 'Data user referral tidak ditemukan';

            return $this->response;
        }

        $now  = new DateTime();
        $data = new stdClass();

        $data->date = $now->format(self::NOTIF_DATE_FORMAT);
        $data->user = $user_referrer;
        $data->referral_name = $user_referral->nama;
        $data->referrer_name = $user_referrer->nama;
        $data->referensi_no = $gpoint->reffSwitching;
        $data->transaction_type = $payment->jenis_transaksi ?? $payment->jenisTransaksi;
        $data->type_reward = $gpoint->type;
        $data->type = $this->notification::TYPE_PROMO;
        $data->reward = Pegadaian::currencyIdr($gpoint->value);
        $data->title = 'Ajak Sahabat';
        $data->tagline = "Selamat, kamu dapat $gpoint->type $data->reward! ayo ajak lagi sahabat kamu untuk transaksi";
        $data->html_notification = 'mail/gpoint/referrer/mobile_notification';
        $data->html_content = 'mail/gpoint/referrer/_content_mobile';
        $data->type_notification = $this->notification::TYPE_PROMO;

        $this->sendNotificationMobile($data);

        $this->response['status'] = 'Success';
        $this->response['message'] = self::SUCCESS_SENT;

        return $this->response;
    }

    public function pointNotification($request)
    {
        $cif = $request['cif'];
        $reff_core = $request['reff_core'];
        $point = $request['point'];
        $channel = $request['channel'];
        $payment_method = $request['payment_method'];
        $payment_date = $request['payment_date'];
        $transaction_type = $request['transaction_type'];

        $user = $this->user_model->getUserByCif($cif);

        if (empty($user)) {
            $this->response['status'] = 'Error';
            $this->response['message'] = 'User tidak ditemukan';

            return $this->response;
        }

        $now  = new DateTime();
        $data = new stdClass();
        $data->date = $now->format(self::NOTIF_DATE_FORMAT);
        $data->user = $user;
        $data->transaction_type = $transaction_type;
        $data->payment_method = $payment_method;
        $data->channel = $channel;
        $data->point = $point;
        $data->reff_core = $reff_core;
        $data->payment_date = $this->pegadaian_helper::formatDate($payment_date);
        $data->title = 'Pegadaian Poin';
        $data->tagline = "Selamat, kamu mendapatkan {$point} point";
        $data->html_notification = 'mail/gpoint/point/mobile_notification';
        $data->html_content = 'mail/gpoint/point/_content_mobile';
        $data->email_notification = 'mail/gpoint/point/email_notification';
        $data->type_notification = $this->notification::TYPE_POINT;
        $data->type = $this->notification::TYPE_POINT;

        $this->sendNotificationMobile($data);
        $this->sendNotificationEmail($data);

        $this->response['status'] = 'Success';
        $this->response['message'] = self::SUCCESS_SENT;

        return $this->response;
    }

    public function sendNotificationMobile(stdClass $data)
    {
        $notification_mobile = $this->setNotificationMobile($data->html_notification, $data->html_content, $data);
        $notification = $this->notification_model->add(
            $data->user->user_AIID,
            $data->type,
            $this->notification_model::CONTENT_TYPE_HTML,
            $data->title,
            $data->tagline,
            $notification_mobile['html'],
            $notification_mobile['content'],
            $data->transaction_type
        );

        Message::sendFCMNotif($data->user->fcm_token, [
            "id"      => $notification,
            "tipe"    => $data->transaction_type ?? '',
            "title"   => $data->title,
            "tagline" => $data->tagline,
            "content" => $notification['html'],
            "token"   => $data->user->no_hp
        ]);
    }

    public function sendNotificationEmail(stdClass $data)
    {
        $notification_email = $this->setNotificationEmail($data->email_notification, $data);

        Message::sendEmail(
            $data->user->email,
            $data->title,
            $notification_email
        );
    }

    private function setNotificationMobile(string $html_notification, $html_content, $data)
    {
        return [
            'html' => $this->load->view($html_notification, ['data' => $data], true),
            'content' => $this->load->view($html_content, ['data' => $data], true)
        ];
    }

    private function setNotificationEmail(string $email_content, $data)
    {
        return $this->load->view($email_content, ['data' => $data], true);
    }

    /**
     * @param $tblFinancing
     * @param $user
     * @param $status (1 = inquiry, 2 = approval, 3 = konfirmasi, 4 = surveyor, 5 = disbursal, 6 = belom disetjui)
     * @param $request
     * @return array
     * @throws Exception
     */
    public function losNotification($tblFinancing, $user, $status, $request)
    {
        if (empty($user)) {
            $this->response['status'] = 'Error';
            $this->response['message'] = self::USER_NOT_FOUND;

            return $this->response;
        }

        $this->response['code'] = 200;

        $now = new DateTime();
        $data = new stdClass();

        $data->date = $now->format(self::NOTIF_DATE_FORMAT);
        $data->user = $user;
        $data->nama = $user->nama;
        $data->cif = $request['cif'];
        $data->nama_usaha = $request['nama_usaha'] ?? '';
        $data->deskriptsi_usaha = $request['deskripsi_usaha'] ?? '';
        $data->nama_cabang = $request['nama_cabang'] ?? '';
        $data->kodeBooking = $tblFinancing->code_booking;
        $data->status = $tblFinancing->status;
        $data->title = 'Pembiayaan';

        $data = $this->checkRedaksionalNotif($status, $data, $request);

        $this->sendMicroNotificationMobile($data);

        $this->response['status'] = 'Success';
        $this->response['message'] = self::SUCCESS_SENT;

        return $this->response;
    }

    protected function checkRedaksionalNotif($status, $data, $request)
    {
        $data->tenor = $request['tenor'] . ' bulan';

        switch ($status) {
            case '1':
                $data->subject = 'Pengajuan Pembiayaan Diterima';
                $data->tagline = 'Pengajuan Pembiayaan Diterima';
                $data->up = $this->pegadaian_helper->currencyIdr($request['up']);
                $data->angsuran = $this->pegadaian_helper->currencyIdr($request['angsuran']);
                $data->html_notification = 'mail/los/inquiry/mobile_notification';
                $data->html_content = 'mail/los/inquiry/notif_pengajuan';
                $data->type = 'MICRO_APPLY';
                break;
            case '2':
                $data->subject = 'Pengajuan Pembiayaan Disetujui';
                $data->tagline = 'Pengajuan Pembiayaan Disetujui';
                $data->up = $this->pegadaian_helper->currencyIdr($request['up']);
                $data->up_approval = !empty($request['up_approval']) ? $this->pegadaian_helper->currencyIdr($request['up_approval']) : '-';
                $data->angsuran = $this->pegadaian_helper->currencyIdr($request['angsuran']) . ' /' . $request['pola_angsuran'];
                $data->sewa_modal = $this->pegadaian_helper->currencyIdr($request['sewa_modal']) . ' /' . $request['pola_angsuran'];
                $data->admin = $this->pegadaian_helper->currencyIdr($request['admin']);
                $data->html_notification = 'mail/los/survey/disetujui/mobile_notification';
                $data->html_content = 'mail/los/survey/disetujui/notif_disetujui';
                $data->type = 'MICRO_CONFIRMATION';
                break;
            case '3':
                $data->subject = 'Pengajuan Pembiayaan Dibatalkan';
                $data->tagline = 'Pengajuan Pembiayaan Dibatalkan';
                $data->html_notification = 'mail/los/konfirmasi/mobile_notification';
                $data->html_content = 'mail/los/konfirmasi/notif_konfirmasi';
                $data->type = 'MICRO_CANCEL';
                break;
            case '4':
                $data->subject = 'Proses Survey Pengajuan Pembiayaan';
                $data->tagline = 'Proses Survey Pengajuan Pembiayaan';
                $data->nama_surveyor = $request['nama_surveyor'];
                $data->noHp = $request['nohp_surveyor'];
                $data->html_notification = 'mail/los/survey/mobile_notification';
                $data->html_content = 'mail/los/survey/notif_survey';
                $data->type = 'MICRO_SURVEY';
                break;
            case '5':
                $data->subject = 'Pencairan Pembiayaan';
                $data->tagline = 'Pencairan Pembiayaan';
                $data->up = $this->pegadaian_helper->currencyIdr($request['up']);
                $data->angsuran = $this->pegadaian_helper->currencyIdr($request['angsuran']) . ' /' . $request['pola_angsuran'];
                $data->nama_usaha = $request['nama_usaha'];
                $data->deskriptsi_usaha = $request['deskripsi_usaha'];
                $data->nama_cabang = $request['nama_cabang'];
                $data->up_approval = $this->pegadaian_helper->currencyIdr($request['up_approval']);
                $data->sewa_modal = $this->pegadaian_helper->currencyIdr($request['sewa_modal']) . ' /' . $request['pola_angsuran'];
                $data->admin = $this->pegadaian_helper->currencyIdr($request['admin']);
                $data->bukti_agunan = $request['collateral'][0]['no_kepemilikan'] ?? '';
                $data->alamat = $request['alamat_cabang'];
                $data->html_notification = 'mail/los/disbursal/mobile_notification';
                $data->html_content = 'mail/los/disbursal/notif_disbursal';
                $data->type = 'MICRO_DISBURSE';
                $data->bottom = $this->getCoupon('MC', $request['up']);
                break;
            case '6':
                $data->subject = 'Pengajuan Pembiayaan Belum Disetujui';
                $data->tagline = 'Pengajuan Pembiayaan Belum Disetujui';
                $data->html_notification = 'mail/los/survey/ditolak/mobile_notification';
                $data->html_content = 'mail/los/survey/ditolak/notif_ditolak';
                $data->type = 'MICRO_REJECT';
                break;
            default:
                break;
        }

        $data->status_notif = $status;

        return $data;
    }

    protected function sendMicroNotificationMobile(stdClass $data)
    {
        $notification_mobile = $this->setNotificationMobile($data->html_notification, $data->html_content, $data);
        $notification = $this->notification_model->add(
            $data->user->user_AIID,
            $this->notification_model::TYPE_LOS,
            $this->notification_model::CONTENT_TYPE_HTML,
            $data->title,
            $data->tagline,
            $notification_mobile['html'],
            $notification_mobile['content'],
            !empty($data->transaction_type) ? $data->transaction_type : ''
        );

        Message::sendFCMNotif($data->user->fcm_token, [
            "id" => $notification,
            "tipe" => $data->type ?? '',
            "title" => $data->title,
            "tagline" => $data->tagline,
            "content" => $notification['html'],
            "token" => $data->user->no_hp,
            "cif" => $data->cif ?? '',
            "status_notification" => $data->status_notif ?? ''
        ]);
    }

    public function gcGenerateNotif($data)
    {
        $content = '';
        $data['title'] = $data['contentTitle'];
        $content = $this->load->view('mail/goldcard/notif_goldcard', $data, true);
        $message = $this->load->view('mail/email_template_top', $data['contentTitle'], true);
        $message = $message . $content;
        $message = $message . $this->load->view('mail/email_template_bottom', array(), true);
        $mobile = $this->load->view('notification/top_template', $data['contentTitle'], true);
        $mobile = $mobile . $content;
        $mobile = $mobile . $this->load->view('notification/bottom_template', array(), true);

        return array(
            'email'   => $message,
            'mobile'  => $mobile,
            'minimal' => $content,
        );
    }

    public function gcSendNotification($params, $user, $method)
    {
        $mailBody = $this->gcGenerateNotif($params);
        // mengirimkan notifikasi aplikasi ke user
        if ($method == '' || $method == 'mobile') {
            $idNotif = $this->NotificationModel->add(
                $user->user_AIID,
                'goldcard',
                NotificationModel::CONTENT_TYPE_HTML,
                $params['notificationTitle'],
                $params['notificationDescription'],
                $mailBody['mobile'],
                $mailBody['minimal'],
                "goldcard"
            );

            //Kirim notifikasi ke device user
            Message::sendFCMNotif(
                $this->User->getFCMToken($user->user_AIID),
                [
                    "id"      => $idNotif,
                    "tipe"    => "goldcard",
                    "title"   => $params['notificationTitle'],
                    "tagline" => $params['notificationDescription'],
                    "content" => $params['notificationDescription'],
                    "token"   => $user->no_hp,
                ]
            );
        }

        //mengirimkan email ke user
        if ($method == '' || $method == 'email') {
            Message::GoldcardSendEmail(
                $user->email,
                $mailBody['email'],
                $params['emailSubject']
            );
        }
    }

    public function titipanEmasNotification($reff_switching, $tglJatuhTempo)
    {
        $this->response['code'] = 200;

        // Tbl Transaction
        $transaction = $this->transaction_model->findByReffSwitchingGtef($reff_switching);

        if (empty($transaction)) {
            $this->response['status'] = 'Error';
            $this->response['message'] = 'Data Transaksi tidak ditemukan';

            return $this->response;
        }

        $user = $this->user_model->getUser($transaction->user_id);

        if (empty($user)) {
            $this->response['status'] = 'Error';
            $this->response['message'] = self::USER_NOT_FOUND;

            return $this->response;
        }


        $now  = new DateTime();
        $data = new stdClass();

        $data->subject = "Gadai Titipan Emas Fisik Berhasil";
        $data->date = $now->format(self::NOTIF_DATE_FORMAT);
        $data->email = $user->email ?? null;
        $data->nama = $user->nama ?? null;
        $data->user_AIID = $user->user_AIID ?? null;
        $data->fcm_token = $user->fcm_token ?? null;
        $data->no_hp = $user->no_hp ?? null;

        $data->nomor_kredit = $transaction->credit_number ??  null;
        $data->transaction_type = $transaction->type ?? null;
        $data->tanggal_kredit = !empty($transaction->credit_date) ? Pegadaian::formatDateLos($transaction->credit_date) : null;
        $data->sewa_modal =  $transaction->cost ??  null;
        $data->uang_pinjaman = !empty($transaction->amount) ? Pegadaian::currencyIdr($transaction->amount) : null;
        $data->biaya_administrasi =  !empty($transaction->admin_surcharge) ? Pegadaian::currencyIdr($transaction->admin_surcharge) : null;
        $data->biaya_asuransi = !empty($transaction->insurance_surcharge) ? Pegadaian::currencyIdr($transaction->insurance_surcharge) : null;
        $data->biaya_transfer = !empty($transaction->transfer_surcharge) ? Pegadaian::currencyIdr($transaction->transfer_surcharge) : null;
        $data->jumlah_diterima = !empty($transaction->amount_approved) ? Pegadaian::currencyIdr($transaction->amount_approved) : null;
        $data->barang_jaminan = "Titipan Emas Fisik";
        $data->nomor_titipan = $transaction->tef_no ?? null;

        // bank
        $data->rekening_tujuan = $transaction->nomor_rekening ?? null;
        $data->atas_nama = $transaction->nama_pemilik ?? null;
        $data->nama_bank = $transaction->nama ?? null;

        // tagihan perpanjang masa titipan tampil jika pengajuannya h-30 dari tanggal jatuh tempo
        $data->tagihan_perpanjang_masa_titipan = null;
        if ($this->cekValidasiTanggal($tglJatuhTempo)) {
            $data->tagihan_perpanjang_masa_titipan = !empty($transaction->deposit_cost) ? Pegadaian::currencyIdr($transaction->deposit_cost) : null;
        }

        // tanggal sewa modal itu tanggal 5 di bulan depan setelah tanggal kredit
        $data->tanggal_sewa_modal = !empty($transaction->credit_date) ? Pegadaian::formatDateLos($this->tglSewaModal($transaction->credit_date)) : null;

        $data->title = "Gadai Titipan Emas Fisik Berhasil";
        $data->tagline = "Pinjaman dengan Nomor Kredit " . $data->nomor_kredit . " telah berhasil ditransfer ke rekening tujuan";
        $data->html_notification = 'mail/gtef/mobile_notification';
        $data->html_content = 'mail/gtef/_content_mobile';

        $this->sendNotificationTitipanEmas($data);

        $this->response['status'] = 'Success';
        $this->response['message'] = self::SUCCESS_SENT;

        return $this->response;
    }

    private function sendNotificationTitipanEmas($data)
    {
        $notification_mobile = $this->setNotificationMobile($data->html_notification, $data->html_content, $data);
        $notification = $this->notification_model->add(
            $data->user_AIID,
            $this->notification_model::TYPE_GTEF,
            $this->notification_model::CONTENT_TYPE_HTML,
            $data->title,
            $data->tagline,
            $notification_mobile['html'],
            $notification_mobile['content'],
            $data->transaction_type
        );

        Message::sendFCMNotif($data->fcm_token, [
            "id"      => $notification,
            "tipe"    => $data->transaction_type,
            "title"   => $data->title,
            "tagline" => $data->tagline,
            "content" => $notification['html'],
            "token"   => $data->no_hp
        ]);

        $this->load->helper('message');

        $emailTemplate = $this->generateMessageEmailTitipanEmas($data);
        Message::sendEmail($data->email, $data->subject, $emailTemplate);
    }

    private function generateMessageEmailTitipanEmas($data)
    {
        $content = $this->load->view('mail/gtef/_content_mobile', $data, true);

        $email = $this->load->view('mail/email_template_top', array('title' => $data->subject), true);
        $email = $email . $content;
        $email = $email . $this->load->view('mail/email_template_bottom', array(), true);

        return $email;
    }

    private function cekValidasiTanggal($tglJatuhTempo)
    {
        $tglJatuhTempo = new DateTime($tglJatuhTempo);
        $tglSekarang = new DateTime();
        $selisihTanggal = $tglJatuhTempo->diff($tglSekarang);

        if ($selisihTanggal->y == 0 && $selisihTanggal->d < 30) {
            return true;
        }

        return false;
    }

    private function tglSewaModal($credit_date)
    {
        $tglSewaModal = date("Y-m-05", strtotime("+1 month", strtotime($credit_date)));

        return $tglSewaModal;
    }

    private function generateMessageEmailMtOnline($data)
    {
        $content = $this->load->view('mail/mtonline/_content_mobile', $data, true);

        $email = $this->load->view('mail/email_template_top', array('title' => $data->subject), true);
        $email = $email . $content;
        $email = $email . $this->load->view('mail/email_template_bottom', array(), true);

        return $email;
    }

    public function sendNotifPaymentGtef($data, $user)
    {
        $template = $this->generateNotifPaymentGtef($data);

        $mobileTemplate = $template['mobile'];
        $emailTemplate = $template['email'];
        $minimalTemplate = $template['minimal'];

        //Simpan notifikasi baru
        $notifId = $this->notification_model->add(
            $user->user_AIID,
            NotificationModel::TYPE_GADAI,
            NotificationModel::CONTENT_TYPE_HTML,
            $data['namaProduk'],
            "Pembayaran gadai dengan nomor kredit " . $data['no_kredit'] . " berhasil.",
            $mobileTemplate,
            $minimalTemplate,
            $data['jenis_transaksi']
        );

        //Kirim notifikasi pembayaran ke device user
        Message::sendFCMNotif(
            $this->user_model->getFCMToken($user->user_AIID),
            [
                "id" => $notifId,
                "tipe" => $data['jenis_transaksi'],
                "title" => $data['namaProduk'],
                "tagline" => "Pembayaran gadai dengan nomor kredit " . $data['no_kredit'] . " berhasil.",
                "content" => "",
                "token" => $user->no_hp,
            ]
        );

        //Kirim Email Notifikasi
        $this->load->helper('message');
        Message::sendEmail($user->email, $data['namaProduk'], $emailTemplate);
    }

    public function generateNotifPaymentGtef($data)
    {
        $judul = $data['namaProduk'] . ' Berhasil';
        $subject = $judul;

        $contentMobile = $this->load->view('mail/gtef/payment/content_payment_gtef', $data, true);
        $contentEmail = $this->load->view('mail/gtef/payment/email_payment_gtef', $data, true);

        $email = $this->load->view('mail/email_template_top', array('title' => $subject), true);
        $email = $email . $contentEmail;
        $email = $email . $this->load->view('mail/email_template_bottom', array(), true);

        $mobile = $this->load->view('notification/top_template', array('title' => $subject), true);
        $mobile = $mobile . $contentMobile;
        $mobile = $mobile . $this->load->view('notification/bottom_template', array(), true);

        return array(
            'email' => $email,
            'mobile' => $mobile,
            'minimal' => $contentMobile, $contentEmail
        );
    }

    public function getCoupon($transaction_type, $value, $cif = '')
    {
        // Set Locale of month
        $english = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        $bahasa = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

        // Get the constraints
        $constraints = $this->pegadaian_helper->getCouponConstraints($transaction_type, $value);

        log_message('debug', 'getCoupon - Constraints => ' . json_encode($constraints));

        // Sort the constraint. Make sure that the order of constraints meet our requirement
        usort($constraints, function ($a, $b) {
            return $a['transaction_amount'] < $b['transaction_amount'];
        });
        $coupon_reward = $this->pegadaian_helper->calculateCoupon($constraints, $value);
        // Set it up here, to avoid a logic inside html file
        $coupon_is_active = (strtotime(date('Y-m-d')) <= strtotime($this->config->item('coupon_end_date')));
        // Get draw date of the promo season
        $coupon_draw_date = str_ireplace($english, $bahasa, date("d F Y", strtotime($this->config->item('coupon_draw_date'))));
        // Get message for cif is linked yet from config table
        $coupon_fail_message = $this->master_model->getParameters('coupon_fail_message');
        // Get message for cif is linked from config table
        // The message must contains %s for $coupon_reward and $coupon_draw_date
        $coupon_success_message = $this->master_model->getParameters('coupon_success_message');
        $coupon_success_message = sprintf($coupon_success_message, $coupon_reward, $coupon_draw_date);

        $coupon_message = $coupon_fail_message;
        // Check whether cif is linked, than get the right message
        if ((!empty($cif)) && $coupon_reward > 0) {
            $coupon_message = $coupon_success_message;
        }

        $result = [
            'coupon_is_active' => $coupon_is_active,
            'coupon_message' => $coupon_message
        ];

        log_message('debug', 'getCoupon - result => ' . json_encode($result) . ' - ' . date('Y-m-d'));

        return $result;
    }

    /**
     * @param $tblFinancing
     * @param $user
     * @param $status (1 = inquiry, 2 = approval, 3 = konfirmasi, 4 = surveyor, 5 = disbursal, 6 = belom disetjui)
     * @param $request
     * @return array
     * @throws Exception
     */

    public function godRefundNotification($kode_booking, $amount)
    {

        $booking = $this->god_service->getData($kode_booking);

        if (empty($booking)) {
            $this->response['status'] = 'Error';
            $this->response['message'] = 'Data booking tidak ditemukan';

            return $this->response;
        }

        $user = $this->user_model->profile($booking->user_AIID);

        if (empty($user)) {
            $this->response['status'] = 'Error';
            $this->response['message'] = self::USER_NOT_FOUND;

            return $this->response;
        }

        $title = 'Pengembalian Dana Gadai Express';
        
        $params = [];
        $params['phoneNumber'] = $user->noHP;
        $params['cif'] = $user->cif;
        $params['emailSubject'] = $title;
        $params['contentTitle'] = $title;
        $params['contentDescription'] = ['Selamat transaksi Gadai Express kamu sudah sukses, kamu mendapat pengembalian dana ke e-wallet / metode transaksi yang dipilih sebelumnya, dengan rincian :'];
        $params['contentList'] = [
            ['key' => 'Jumlah', 'value' => $this->pegadaian_helper->currencyIdr($amount)],
            ['key' => 'Kode Booking', 'value' => $kode_booking]
        ];
        $params['type'] = $this->notification_model::TYPE_REFUND_GOD;
        $params['notificationTitle'] = $title;
        $params['notificationDescription'] = $title;

        $this->globalSendNotification($params, '');

        $this->response['code'] = 200;
        $this->response['status'] = 'Success';
        $this->response['message'] = self::SUCCESS_SENT;

        return $this->response;
    }

    //notifikasi khusus pembayaran MayBank
    public function notificationOpenTabemasPayment($payment, $amount, $virtualAccountFormat, $tglExpired, $trxId, $token)
    {

        $template = $this->generateOpenTabemasNotif($token->nama, $payment, $amount, $virtualAccountFormat, $tglExpired, $trxId);
        $emailTemplate = $template['email'];
        $mobileTemplate = $template['mobile'];
        $minimalTemplate = $template['minimal'];

        //Simpan notifikasi baru
        $idNotif = $this->NotificationModel->add(
            $token->id,
            NotificationModel::TYPE_EMAS,
            NotificationModel::CONTENT_TYPE_HTML,
            $this->ConfigModel->getNamaProduk('OP', '62'),
            "Segera Bayar Rp" . number_format($amount, 0, ",", ".") . " ke " . $virtualAccountFormat,
            $mobileTemplate,
            $minimalTemplate,
            "OP"
        );

        //Kirim notifikasi pembayaran ke device user
        Message::sendFCMNotif(
            $this->User->getFCMToken($token->id),
            [
                "id" => $idNotif,
                "tipe" => "OP",
                "title" => $this->ConfigModel->getNamaProduk('OP', '62'),
                "tagline" => "Segera Bayar Rp" . number_format($amount, 0, ",", ".") . " ke " . $virtualAccountFormat,
                "content" => "Bayar ke " . $virtualAccountFormat . " sebelum " . $tglExpired,
                "paymentType" => $payment,
                "token" => $token->no_hp,
            ]
        );

        //Kirim Email Notifikasi
        $this->load->helper('message');
        Message::sendEmailOpenTabunganPayment($token->email, $emailTemplate);
    }

    //notifikasi khusus pembayaran MayBank
    public function notificationTopupPayment($idTransaksi, $cif, $amount, $virtualAccountFormat, $checkPayment, $tglExpired, $token)
    {
        $template = $this->generateBeliEmasNotif($idTransaksi, $cif, $virtualAccountFormat);

        $emailTemplate = $template['email'];
        $mobileTemplate = $template['mobile'];
        $minimalTemplate = $template['minimal'];

        $notifId = $this->NotificationModel->add(
            $token->id,
            NotificationModel::TYPE_EMAS,
            NotificationModel::CONTENT_TYPE_HTML,
            $this->ConfigModel->getNamaProduk('SL', '62'),
            "Segera bayar Rp. " . number_format($amount, 0, ",", ".") . ' ke ' . $virtualAccountFormat,
            $mobileTemplate,
            $minimalTemplate,
            "SL"
        );

        //Kirim notifikasi pembayaran ke device user
        Message::sendFCMNotif(
            $this->User->getFCMToken($token->id),
            [
                "id" => $notifId,
                "tipe" => "SL",
                "title" => $this->ConfigModel->getNamaProduk('SL', '62'),
                "tagline" => "Segera bayar Rp. " . number_format($amount, 0, ",", ".") . ' ke ' . $virtualAccountFormat,
                "content" => "Bayar ke " . $virtualAccountFormat . " sebelum " . $tglExpired,
                "token" => $token->no_hp,
            ]
        );


        //Kirim Email Notifikasi
        $this->load->helper('message');
        Message::sendEmailBeliEmas($token->email, $checkPayment->no_rekening, $emailTemplate);
    }

    //notifikasi khusus pembayaran MayBank
    public function notificationCetakPayment($idTransaksi, $amount, $virtualAccountFormat, $checkPayment, $tglExpired, $token)
    {
        $template = $this->generateCetakEmasNotif($idTransaksi, $virtualAccountFormat);

        $mobileTemplate = $template['mobile'];
        $emailTemplate = $template['email'];
        $minimalTemplate = $template['minimal'];

        //Simpan notifikasi baru
        $notifId = $this->NotificationModel->add(
            $token->id,
            NotificationModel::TYPE_EMAS,
            NotificationModel::CONTENT_TYPE_HTML,
            $this->ConfigModel->getNamaProduk('OD', '62'),
            "Segera bayar Rp. " . number_format($amount, 0, ",", ".") . ' ke ' . $virtualAccountFormat,
            $mobileTemplate,
            $minimalTemplate,
            "OD"
        );

        //Kirim notifikasi pembayaran ke device user
        Message::sendFCMNotif(
            $this->User->getFCMToken($token->id),
            [
                "id" => $notifId,
                "tipe" => "OD",
                "title" => $this->ConfigModel->getNamaProduk('OD', '62'),
                "tagline" => "Segera bayar Rp. " . number_format($amount, 0, ",", ".") . ' ke ' . $virtualAccountFormat,
                "content" => "Bayar ke " . $virtualAccountFormat . " sebelum " . $tglExpired,
                "token" => $token->no_hp,
            ]
        );


        //Kirim Email Notifikasi
        $this->load->helper('message');
        Message::sendEmail($token->email, 'Order Cetak Tabungan Emas', $emailTemplate);
    }

    public function notificationCicilPayment($token, $templateData)
    {
        $template = $this->generateNotifCicil($templateData);

        $emailTemplate   = $template['email'];
        $mobileTemplate  = $template['mobile'];
        $minimalTemplate = $template['minimal'];

        $totalBayar = $templateData['totalKewajiban'] + $templateData['biayaTransaksi'];
        $fTotalBayar = number_format($totalBayar, 0, ",", ".");

        $subtitle = "Segera Bayar Rp " . $fTotalBayar . " ke " . $templateData['va'];

        // Simpan notifikasi baru
        $idNotif = $this->NotificationModel->add(
            $token->id,
            NotificationModel::TYPE_MPO,
            NotificationModel::CONTENT_TYPE_HTML,
            $this->ConfigModel->getNamaProduk('OP', '37'),
            $subtitle,
            $mobileTemplate,
            $minimalTemplate,
            "ML"
        );

        //Kirim notifikasi pembayaran ke device user
        Message::sendFCMNotif(
            $this->User->getFCMToken($token->id),
            [
                "id"      => $idNotif,
                "tipe"    => "ML",
                "title"   => $this->ConfigModel->getNamaProduk('OP', '37'),
                "tagline" => $subtitle,
                "content" => "Bayar ke " . $templateData['va'] . " sebelum " . $templateData['tglExpired'],
                "token"   => $token->no_hp,
            ]
        );

        //Kirim Email Notifikasi
        $this->load->helper('message');
        Message::sendEmail($token->email, "Pembelian Mulia", $emailTemplate);
    }

    //notifikasi pembayaran MayBank
    public function notificationMpo($templateData, $amount, $virtualAccountFormat, $tglExpired, $token, $product)
    {
        $template = $this->generateNotifMpo($templateData);

        $emailTemplate   = $template['email'];
        $mobileTemplate  = $template['mobile'];
        $minimalTemplate = $template['minimal'];

        $subtitle = "Segera Bayar Rp " .
            number_format($amount, 0, ",", ".") . " ke " . $virtualAccountFormat;

        //Simpan notifikasi baru
        $productName = $this->ConfigModel->getNamaProduk('MP', '50');

        $idNotif = $this->NotificationModel->add(
            $token->id,
            NotificationModel::TYPE_MPO,
            NotificationModel::CONTENT_TYPE_HTML,
            $productName,
            $subtitle,
            $mobileTemplate,
            $minimalTemplate,
            "MP"
        );

        //Kirim notifikasi pembayaran ke device user
        Message::sendFCMNotif(
            $this->User->getFCMToken($token->id),
            [
                "id"      => $idNotif,
                "tipe"    => "MP",
                "title"   => $this->ConfigModel->getNamaProduk('MP', '50'),
                "tagline" => $subtitle,
                "content" => "Bayar ke " . $virtualAccountFormat . " sebelum " . $tglExpired,
                "token"   => $token->no_hp,
            ]
        );

        //Kirim Email Notifikasi
        $this->load->helper('message');
        Message::sendEmail($token->email, "Pembelian " . $product, $emailTemplate);
    }

    //notifikasi pembayaran MayBank
    public function notificationMikro($checkPayment, $virtualAccountFormat, $tglExpired, $idTransaksi, $token, $productCode)
    {
        $template = $this->generatePaymentNotifMikro($idTransaksi, $virtualAccountFormat);

        $mobileTemplate = $template['mobile'];
        $emailTemplate = $template['email'];
        $minimalTemplate = $template['minimal'];

        //Simpan notifikasi baru
        $notifId = $this->NotificationModel->add(
            $token->id,
            NotificationModel::TYPE_GADAI,
            NotificationModel::CONTENT_TYPE_HTML,
            $this->ConfigModel->getNamaProduk($checkPayment->jenisTransaksi, $productCode),
            "No Kredit " . $checkPayment->norek,
            $mobileTemplate,
            $minimalTemplate,
            "MC"
        );

        //Kirim notifikasi pembayaran ke device user
        Message::sendFCMNotif(
            $this->User->getFCMToken($token->id),
            [
                "id" => $notifId,
                "tipe" => "MC",
                "title" => $this->ConfigModel->getNamaProduk($checkPayment->jenisTransaksi, $productCode),
                "tagline" => "No Kredit " . $checkPayment->norek,
                "content" => "Bayar ke " . $virtualAccountFormat . " sebelum " . $tglExpired,
                "token" => $token->no_hp
            ]
        );

        //Kirim Email Notifikasi
        $this->load->helper('message');
        Message::sendEmail($token->email, "Konfirmasi Pembayaran Angsuran Nomor " . $checkPayment->norek, $emailTemplate);
    }

    public function simpanNotifikasiMtonline($user, $inquiry_open_data)
    {
        $data = new stdClass();

        $data->user_AIID = $user->user_AIID ?? $user->id;
        $data->customer_name = $inquiry_open_data->trx_mtonline->customer_name ?? "";
        $data->contract_number = $inquiry_open_data->trx_mtonline->contract_number ?? "";
        $data->credit_number = $inquiry_open_data->trx_mtonline->credit_number ?? "";
        $data->tanggal_kredit = !empty($inquiry_open_data->trx_mtonline->credit_date) ? date('d M Y', strtotime($inquiry_open_data->trx_mtonline->credit_date)) : null;
        $data->tanggal_transaksi = !empty($inquiry_open_data->trx_mtonline->transaction_date) ? Pegadaian::formatDateLos($inquiry_open_data->trx_mtonline->transaction_date) : null;
        $data->tanggal_jatuh_tempo = !empty($inquiry_open_data->trx_mtonline->due_date) ? date('d M Y', strtotime($inquiry_open_data->trx_mtonline->due_date)) : null;
        $data->jumlah_hari_real = $inquiry_open_data->trx_mtonline->jumlah_hari_real ?? "";
        $data->jumlah_hari_tarif = $inquiry_open_data->trx_mtonline->jumlah_hari_tarif ?? "";
        $data->uang_pinjaman = !empty($inquiry_open_data->trx_mtonline->amount) ? Pegadaian::currencyIdr($inquiry_open_data->trx_mtonline->amount) : null;
        $data->total_uang_pinjaman = !empty($inquiry_open_data->total_uang_pinjaman) ? Pegadaian::currencyIdr($inquiry_open_data->total_uang_pinjaman) : null;
        $data->sewa_modal = !empty($inquiry_open_data->trx_mtonline->tagihan_sewa_modal) ? Pegadaian::currencyIdr($inquiry_open_data->trx_mtonline->tagihan_sewa_modal) : null;
        $data->admin_surcharge = !empty($inquiry_open_data->trx_mtonline->admin_surcharge) ? Pegadaian::currencyIdr($inquiry_open_data->trx_mtonline->admin_surcharge) : null;
        $data->insurance_surcharge = !empty($inquiry_open_data->trx_mtonline->insurance_surcharge) ? Pegadaian::currencyIdr($inquiry_open_data->trx_mtonline->insurance_surcharge) : null;
        $data->surcharge = !empty($inquiry_open_data->trx_mtonline->surcharge) ? Pegadaian::currencyIdr($inquiry_open_data->trx_mtonline->surcharge) : null;
        $data->total_surcharge = !empty($inquiry_open_data->trx_mtonline->total_surcharge) ? Pegadaian::currencyIdr($inquiry_open_data->trx_mtonline->total_surcharge) : null;
        $data->amount_approved = !empty($inquiry_open_data->trx_mtonline->amount_approved) ? Pegadaian::currencyIdr($inquiry_open_data->trx_mtonline->amount_approved) : null;
        $data->bank_name = $inquiry_open_data->trx_mtonline->bank->bank_name ?? $inquiry_open_data->trx_mtonline->bank->namaBank;
        $data->bank_account_number = $inquiry_open_data->trx_mtonline->bank->account_number ?? $inquiry_open_data->trx_mtonline->bank->virtualAccount;
        $data->bank_customer_name = $inquiry_open_data->trx_mtonline->bank->customer_name ?? "";
        $data->transaction_type = $inquiry_open_data->trx_mtonline->type_transaction ?? "";

        $data->subject = "Tambah Pinjaman Berhasil";
        $data->title = "Tambah Pinjaman Berhasil";
        $data->tagline = "Terima Kasih telah melakukan pengajuan Tambah Pinjaman";
        $data->html_notification = 'mail/mtonline/mobile_notification';
        $data->html_content = 'mail/mtonline/_content_mobile';

        $notification_mobile = $this->setNotificationMobile($data->html_notification, $data->html_content, $data);

        $notification = $this->notification_model->add(
            $data->user_AIID,
            $this->notification_model::TYPE_GADAI,
            $this->notification_model::CONTENT_TYPE_HTML,
            $data->title,
            $data->tagline,
            $notification_mobile['html'],
            $notification_mobile['content'],
            !empty($data->transaction_type) ? $data->transaction_type : ''
        );

        Message::sendFCMNotif($this->user_model->getFCMToken($data->user_AIID), [
            "id" => $notification,
            "tipe" => !empty($data->transaction_type) ? $data->transaction_type : '',
            "title" => $data->title,
            "tagline" => $data->tagline,
            "content" => $notification['html'],
            "token" => $user->noHP
        ]);

        //Kirim Email Notifikasi
        $this->load->helper('message');
        $emailTemplate = $this->generateMessageEmailMtOnline($data);
        Message::sendEmail($user->email, $data->subject, $emailTemplate);

        return $notification;
    }

    public function pencairanMtonlineNotification($request)
    {
        $data_transaction = $this->transaction_model->find(['reff_switching' => $request['trxId']]);
        if ($data_transaction == null) {
            return $this->response;
        }

        $user = $this->user_model->getUser($data_transaction->user_id);
        if (empty($user)) {
            $this->response['status'] = 'Error';
            $this->response['message'] = self::USER_NOT_FOUND;

            return $this->response;
        }

        // update status is_disbursal di table apply_mtonline, setelah itu get data terbarunya
        $update_get = $this->mtonline_model->updateById($data_transaction->relation_id, ['is_disbursed' => 1]);
        if ($update_get == null) {
            return $this->response;
        }

        $data = new stdClass();

        $data->user_AIID = $user->user_AIID ?? $user->id;
        $data->customer_name = $update_get->customer_name ?? "";
        $data->contract_number = $update_get->contract_number ?? "";
        $data->credit_number = $update_get->credit_number ?? "";
        $data->product_name = $update_get->product_name ?? "";
        $data->tanggal_kredit = !empty($update_get->credit_date) ? date('d M Y', strtotime($update_get->credit_date)) : null;
        $data->tanggal_transaksi = !empty($update_get->transaction_date) ? Pegadaian::formatDateLos($update_get->transaction_date) : null;
        $data->tanggal_jatuh_tempo = !empty($update_get->due_date) ? date('d M Y', strtotime($update_get->due_date)) : null;
        $data->jumlah_hari_real = $update_get->jumlah_hari_real ?? "";
        $data->jumlah_hari_tarif = $update_get->jumlah_hari_tarif ?? "";
        $data->uang_pinjaman = !empty($update_get->amount) ? Pegadaian::currencyIdr($update_get->amount) : null;
        $data->total_uang_pinjaman = !empty($update_get->total_amount) ? Pegadaian::currencyIdr($update_get->total_amount) : null;
        $data->sewa_modal = !empty($update_get->tagihan_sewa_modal) ? Pegadaian::currencyIdr($update_get->tagihan_sewa_modal) : null;
        $data->admin_surcharge = !empty($update_get->admin_surcharge) ? Pegadaian::currencyIdr($update_get->admin_surcharge) : null;
        $data->insurance_surcharge = !empty($update_get->insurance_surcharge) ? Pegadaian::currencyIdr($update_get->insurance_surcharge) : null;
        $data->surcharge = !empty($update_get->surcharge) ? Pegadaian::currencyIdr($update_get->surcharge) : null;
        $data->total_surcharge = !empty($update_get->total_surcharge) ? Pegadaian::currencyIdr($update_get->total_surcharge) : null;
        $data->amount_approved = !empty($update_get->amount_approved) ? Pegadaian::currencyIdr($update_get->amount_approved) : null;

        $data->bank_name = $update_get->bank->bank_name ?? $update_get->bank->namaBank;
        $data->bank_account_number = $update_get->bank->account_number ?? $update_get->bank->virtualAccount;
        $data->bank_customer_name = $update_get->bank->customer_name ?? "";
        $data->transaction_type = $update_get->type_transaction ?? "";

        $data->title = "Pencairan Berhasil";
        $data->subject = "Transaksi " . $data->credit_number . " - " . $data->product_name . " Sukses";
        $data->tagline = "Tambah pinjaman telah berhasil ditransfer ke rekening tujuan";
        $data->html_notification = 'mail/mtonline/mobile_notification';
        $data->html_content = 'mail/mtonline/_content_mobile';

        $notification_mobile = $this->setNotificationMobile($data->html_notification, $data->html_content, $data);

        $notification = $this->notification_model->add(
            $data->user_AIID,
            $this->notification_model::TYPE_GADAI,
            $this->notification_model::CONTENT_TYPE_HTML,
            $data->title,
            $data->tagline,
            $notification_mobile['html'],
            $notification_mobile['content'],
            !empty($data->transaction_type) ? $data->transaction_type : ''
        );

        Message::sendFCMNotif($this->User->getFCMToken($data->user_AIID), [
            "id" => $notification,
            "tipe" => !empty($data->transaction_type) ? $data->transaction_type : '',
            "title" => $data->title,
            "tagline" => $data->tagline,
            "content" => $notification['html'],
            "token" => $user->no_hp
        ]);

        //Kirim Email Notifikasi
        $this->load->helper('message');
        $emailTemplate = $this->generateMessageEmailMtOnline($data);
        Message::sendEmail($user->email, $data->subject, $emailTemplate);

        $this->response['status'] = 'Success';
        $this->response['message'] = self::SUCCESS_SENT;

        return $this->response;
    }

    //notifikasi pembayaran MayBank
    public function notificationPaymentGadai($checkPayment, $virtualAccountFormat, $tglExpired, $idTransaksi, $token, $productCode)
    {
        $template = $this->generatePaymentGadaiNotif($idTransaksi, $virtualAccountFormat);

        $mobileTemplate = $template['mobile'];
        $emailTemplate = $template['email'];
        $minimalTemplate = $template['minimal'];

        //Simpan notifikasi baru
        $notifId = $this->NotificationModel->add(
            $token->id,
            NotificationModel::TYPE_GADAI,
            NotificationModel::CONTENT_TYPE_HTML,
            $this->ConfigModel->getNamaProduk($checkPayment->jenisTransaksi, $productCode),
            "Pembayaran Gadai",
            $mobileTemplate,
            $minimalTemplate,
            "GD"
        );

        //Kirim notifikasi pembayaran ke device user
        Message::sendFCMNotif(
            $this->User->getFCMToken($token->id),
            [
                "id" => $notifId,
                "tipe" => "GD",
                "title" => $this->ConfigModel->getNamaProduk($checkPayment->jenisTransaksi, $productCode),
                "tagline" => "Pembayaran Gadai",
                "content" => "Bayar ke " . $virtualAccountFormat . " sebelum " . $tglExpired,
                "token" => $token->no_hp
            ]
        );

        //Kirim Email Notifikasi
        $this->load->helper('message');
        Message::sendEmail($token->email, "Pembayaran Gadai " . $checkPayment->norek, $emailTemplate);
    }

    //detail notifikasi pembayaran MayBank
    protected function generateOpenTabemasNotif($nama, $payment, $amount, $va, $tglExpired, $trxId)
    {

        $checkPayment = $this->payment_model->getPaymentByTrxId2($trxId, true);
        $redaksiPayment = $this->config_model->getRedaksiPayment($checkPayment['kodeBankPembayar'], $checkPayment['virtual_account'], $checkPayment['total_kewajiban']);
        $subject = "Pengajuan Pembukaan Rekening Tabungan Emas Pegadaian";

        //Convert tanggal expired ke dd-mm-yyy H:i:s
        $date = new DateTime($tglExpired);

        $viewData = array(

            'nama' => $nama,
            'payment' => $payment,
            'va' => $va,
            'amount' => $amount,
            'tglExpired' => $date->format('d/m/Y H:i:s'),
            'trxId' => $trxId,
            'redaksi' => $redaksiPayment,
        );

        $content = $this->load->view('mail/notif_opentabemas', $viewData, true);

        $message = $this->load->view('mail/email_template_top', array('title' => $subject), true);
        $message = $message . $content;
        $message = $message . $this->load->view('mail/email_template_bottom', array(), true);

        $mobile = $this->load->view('notification/top_template', array('title' => $subject), true);
        $mobile = $mobile . $content;
        $mobile = $mobile . $this->load->view('notification/bottom_template', array(), true);

        return array(
            'email' => $message,
            'mobile' => $mobile,
            'minimal' => $content,
        );
    }

    //detail notifikasi pembayaran MayBank
    protected function generateBeliEmasNotif($trxId, $cif, $virtualAccountFormat)
    {
        $checkPayment = $this->payment_model->getPaymentByTrxId2($trxId, true);
        $redaksiPayment = $this->config_model->getRedaksiPayment($checkPayment['kodeBankPembayar'], $checkPayment['virtual_account'], $checkPayment['total_kewajiban']);

        // Get promo (if Any)
        $gpoin = $this->GpoinModel->get_promo($trxId);
        $promoCode = '';
        $idPromosi = '';
        $discountAmount = 0;
        $promoAmount = 0;
        if ($gpoin != '') {
            if ($gpoin->type == 'discount') {
                $discountAmount = $gpoin->value;
            }
            $promoCode = $gpoin->promoCode;
            $promoAmount = $gpoin->value;
            $idPromosi = $gpoin->idPromosi;
        };
        $checkPayment['promoCode'] = $promoCode;
        $checkPayment['promoAmount'] = $promoAmount;
        $checkPayment['discountAmount'] = $discountAmount;

        $user = $this->User->getUser($checkPayment['user_AIID']);
        $checkPayment['namaNasabahUser'] = $user->nama;
        $checkPayment['cif'] = $cif;
        $checkPayment['va'] = $virtualAccountFormat;
        $checkPayment['redaksi'] = $redaksiPayment;

        $subject = "Konfirmasi Pembelian Tabungan Emas Pegadaian " . $checkPayment['norek'];

        $content = $this->load->view('mail/notif_beliemas', $checkPayment, true);

        $email = $this->load->view('mail/email_template_top', array('title' => $subject), true);
        $email = $email . $content;
        $email = $email . $this->load->view('mail/email_template_bottom', array(), true);

        $mobile = $this->load->view('notification/top_template', array('title' => $subject), true);
        $mobile = $mobile . $content;
        $mobile = $mobile . $this->load->view('notification/bottom_template', array(), true);

        return array(
            'email' => $email,
            'mobile' => $mobile,
            'minimal' => $content,
        );
    }

    //detail notifikasi Pembayaran MayBank
    function generateCetakEmasNotif($trxId, $virtualAccountFormat)
    {
        $checkPayment = $this->payment_model->getPaymentByTrxId2($trxId, true);
        $redaksiPayment = $this->config_model->getRedaksiPayment($checkPayment['kodeBankPembayar'], $checkPayment['virtual_account'], $checkPayment['total_kewajiban']);
        $checkPayment['va'] = $virtualAccountFormat;
        $checkPayment['redaksi'] = $redaksiPayment;

        $subject = "Konfirmasi Order Cetak Tabungan Emas Pegadaian " . $checkPayment['norek'];

        $content = $this->load->view('mail/notif_cetakemas', $checkPayment, true);

        $email = $this->load->view('mail/email_template_top', array('title' => $subject), true);
        $email = $email . $content;
        $email = $email . $this->load->view('mail/email_template_bottom', array(), true);

        $mobile = $this->load->view('notification/top_template', array('title' => $subject), true);
        $mobile = $mobile . $content;
        $mobile = $mobile . $this->load->view('notification/bottom_template', array(), true);

        return array(
            'email' => $email,
            'mobile' => $mobile,
            'minimal' => $content,
        );
    }

    public function generateNotifCicil($data)
    {
        $subject = "Pembelian Logam Mulia";

        $content = $this->load->view('mail/mulia/inquiry', $data, true);

        $message = $this->load->view('mail/email_template_top', array('title' => $subject), true);
        $message = $message . $content;
        $message = $message . $this->load->view('mail/email_template_bottom', array(), true);

        $mobile = $this->load->view('notification/top_template', array('title' => $subject), true);
        $mobile = $mobile . $content;
        $mobile = $mobile . $this->load->view('notification/bottom_template', array(), true);

        return array(
            'email'   => $message,
            'mobile'  => $mobile,
            'minimal' => $content,
        );
    }

    public function globalGenerateNotif($data)
    {
        $content = '';
        $data['title'] = $data['contentTitle'];
        $content = $this->load->view('mail/global_notif', $data, true);
        $message = $this->load->view('mail/email_template_top', $data['contentTitle'], true);
        $message = $message . $content;
        $message = $message . $this->load->view('mail/email_template_bottom', array(), true);
        $mobile = $this->load->view('notification/top_template', $data['contentTitle'], true);
        $mobile = $mobile . $content;
        $mobile = $mobile . $this->load->view('notification/bottom_template', array(), true);

        return array(
            'email'   => $message,
            'mobile'  => $mobile,
            'minimal' => $content,
        );
    }

    //detail notifikasi khusus pembayaran MayBank
    public function generateNotifMpo($data)
    {
        $mpoProduct = $data['mpoProduct'];
        $subject = "Pembelian " . $data['product'];

        $content = '';

        if ($mpoProduct->groups === 'seluler') {
            $content = $this->load->view('mail/mpo/notif_payment_mpo_seluler', $data, true);
        }
        if ($mpoProduct->groups == 'listrik' && $mpoProduct->namaLayanan == 'PLN Prepaid') {
            $content = $this->load->view('mail/mpo/notif_payment_mpo_plnpre', $data, true);
        }
        if ($mpoProduct->groups === 'listrik' && $mpoProduct->namaLayanan === 'PLN Postpaid') {
            $data['periode'] = $this->_formatPLNTglPeriode($data['periode']);
            $content = $this->load->view('mail/mpo/notif_payment_mpo_plnpost', $data, true);
        }
        if ($mpoProduct->groups == 'air') {
            $content = $this->load->view('mail/mpo/notif_payment_mpo_pdam', $data, true);
        }
        if ($mpoProduct->groups == 'asuransi') {
            $content = $this->load->view('mail/mpo/notif_payment_mpo_bpjs', $data, true);
        }
        if ($mpoProduct->groups == 'telkom' && $mpoProduct->namaLayanan == 'Telkom') {
            $content = $this->load->view('mail/mpo/notif_payment_mpo_telkom', $data, true);
        }
        if ($mpoProduct->groups == 'telkom') {
            $content = $this->load->view('mail/mpo/notif_payment_mpo_halo', $data, true);
        }
        if ($mpoProduct->groups == 'voucher') {
            $content = $this->load->view('mail/mpo/notif_payment_mpo_ewallet', $data, true);
            if ($data['paymentMethod'] == 'GCASH') {
                $content = $this->load->view('mail/mpo/notif_payment_mpo_ewallet_success', $data, true);
            }
        }

        $message = $this->load->view('mail/email_template_top', array('title' => $subject), true);
        $message = $message . $content;
        $message = $message . $this->load->view('mail/email_template_bottom', array(), true);

        $mobile = $this->load->view('notification/top_template', array('title' => $subject), true);
        $mobile = $mobile . $content;
        $mobile = $mobile . $this->load->view('notification/bottom_template', array(), true);

        return array(
            'email'   => $message,
            'mobile'  => $mobile,
            'minimal' => $content,
        );
    }

    function _formatPLNTglPeriode($str)
    {
        $arrPeriode = json_decode($str);
        $strFormat = '';
        if ($arrPeriode) {
            $c = 1;
            foreach ($arrPeriode as $a) {
                $withoutSpace = str_replace(" ", "", $a);
                if ($withoutSpace !== "") {
                    $d = DateTime::createFromFormat('Ym', $withoutSpace);
                    $strFormat = $strFormat . $d->format('M Y');
                    if ($c < count($arrPeriode)) {
                        $strFormat = $strFormat . ', ';
                    }
                }
                $c++;
            }
            return $strFormat;
        }
    }


    //detail notifikasi khusus pembayaran MayBank
    function generatePaymentNotifMikro($trxId, $virtualAccountFormat)
    {
        $payment = $this->mikro_model->getPaymentByTrxId($trxId);
        $redaksi = $this->config_model->getRedaksiPayment($payment->kodeBankPembayar, $payment->virtual_account, $payment->totalKewajiban);
        $subject = null;
        $viewData = array();

        $subject = "Pembayaran Angsuran No " . $payment->norek;

        $amount = $payment->totalKewajiban;
        if ($payment->jenisTransaksi == "TB") {
            $subject = "Transaksi Pelunasan Angsuran " . $payment->norek . " Sukses";
            $amount = $payment->totalKewajiban  - $payment->saldoRekeningPendamping;
        }

        $fTglExpired = new DateTime($payment->tanggal_expired);
        $viewData = array(
            'amount' => $amount,
            'namaNasabah' => $payment->namaNasabah,
            'va' => $virtualAccountFormat,
            'tglExpired' => $fTglExpired->format('d/m/Y H:i:s'),
            'norek' => $payment->norek,
            'payment' => $payment->tipe,
            'trxId' => $payment->reffSwitching,
            'namaProduk' => $payment->namaProduk,
            'biayaTransaksi' => $payment->biayaTransaksi,
            'redaksi' => $redaksi
        );

        $content = $this->load->view('mail/notif_payment_mikro', $viewData, true);

        $email = $this->load->view('mail/email_template_top', array('title' => $subject), true);
        $email = $email . $content;
        $email = $email . $this->load->view('mail/email_template_bottom', array(), true);

        $mobile = $this->load->view('notification/top_template', array('title' => $subject), true);
        $mobile = $mobile . $content;
        $mobile = $mobile . $this->load->view('notification/bottom_template', array(), true);

        return array(
            'email' => $email,
            'mobile' => $mobile,
            'minimal' => $content
        );
    }

    //detail notifikasi khusus pembayaran MayBank
    function generatePaymentGadaiNotif($trxId, $virtualAccountFormat)
    {
        $payment = $this->gadai_model->getPaymentByTrxId($trxId);
        $redaksi = $this->config_model->getRedaksiPayment($payment->kodeBankPembayar, $payment->virtual_account, $payment->totalKewajiban);

        if ($payment->jenisTransaksi == 'CC') {
            $jt = 'Cicil';
        }
        if ($payment->jenisTransaksi == 'UG') {
            $jt = 'Ulang Gadai';
        }
        if ($payment->jenisTransaksi == 'TB') {
            $jt = 'Tebus Gadai';
        }

        $subject = "Pembayaran Gadai " . $payment->norek . ' - ' . $jt;
        $fTglExpired = new DateTime($payment->tanggal_expired);

        $viewData = array(
            'totalKewajiban' => $payment->totalKewajiban,
            'namaNasabah' => $payment->namaNasabah,
            'jenisTransaksi' => $jt,
            'va' => $virtualAccountFormat,
            'tglExpired' => $fTglExpired->format('d/m/Y H:i:s'),
            'norek' => $payment->norek,
            'payment' => $payment->payment,
            'trxId' => $payment->reffSwitching,
            'biayaTransaksi' => $payment->biayaTransaksi,
            'redaksi' => $redaksi
        );

        $content = $this->load->view('mail/notif_payment_gadai', $viewData, true);

        $email = $this->load->view('mail/email_template_top', array('title' => $subject), true);
        $email = $email . $content;
        $email = $email . $this->load->view('mail/email_template_bottom', array(), true);

        $mobile = $this->load->view('notification/top_template', array('title' => $subject), true);
        $mobile = $mobile . $content;
        $mobile = $mobile . $this->load->view('notification/bottom_template', array(), true);

        return array(
            'email' => $email,
            'mobile' => $mobile,
            'minimal' => $content
        );
    }
    public function globalSendNotification($params, $method)
    {
        // validasi nomer telepon
        $user = $this->User->getUser($params['phoneNumber']);

        if (!$user) {
            $this->response['message'] = 'Nomor telepon tidak ditemukan';
            return $this->response;
        }

        $params['name'] = $user->nama;
        $mailBody = $this->globalGenerateNotif($params);

        // mengirimkan notifikasi aplikasi ke user
        if ($method == '' || $method == 'mobile') {
            $idNotif = $this->notification_model->add(
                $user->user_AIID,
                $params['type'] ?? 'global',
                NotificationModel::CONTENT_TYPE_HTML,
                $params['notificationTitle'],
                $params['tagline'] ?? $params['notificationDescription'],
                $mailBody['mobile'],
                $mailBody['minimal'],
                $params['type'] ?? 'global'
            );

            //Kirim notifikasi ke device user
            Message::sendFCMNotif(
                $this->User->getFCMToken($user->user_AIID),
                [
                    "id"      => $idNotif,
                    "tipe"    => $params['type'] ?? 'global',
                    "title"   => $params['notificationTitle'],
                    "tagline" => $params['tagline'] ?? $params['notificationDescription'],
                    "content" => $params['notificationDescription'],
                    "token"   => $user->no_hp,
                    "data"    => $params['data'] ?? []
                ]
            );
        }

        //mengirimkan email ke user
        if ($method == '' || $method == 'email') {
            Message::globalSendEmail(
                $user->email,
                $mailBody['email'],
                $params['emailSubject']
            );
        }

        $this->response = [
            'code' => '00',
            'status' => 'success',
            'message' => 'Berhasil mengirimkan notifikasi',
            'data' => null
        ];

        return $this->response;
    }

    public function everyWhereNotification($request)
    {

        $user = $this->user_model->getUserByCif('1015205814');

        if (empty($user)) {
            $this->response['status'] = 'Error';
            $this->response['message'] = 'User tidak ditemukan';

            return $this->response;
        }

        $now  = new DateTime();
        $data = new stdClass();
        $data->date = $now->format(self::NOTIF_DATE_FORMAT);
        $data->user = $user;
        $data->title = 'BELI TABUNGAN EMAS';
        $data->tagline = "0, 0789 gram/Rp.50.000 sukses";
        $data->html_notification = 'mail/gpoint/point/mobile_notification_test';
        $data->html_content = 'mail/gpoint/point/_content_mobile_test';
        $data->type_notification = $this->notification_model::TYPE_POINT;
        $data->transaction_type = 'SL';

        $this->sendNotificationMobile($data);

        $this->response['status'] = 'Success';
        $this->response['message'] = self::SUCCESS_SENT;

        return $this->response;
    }

    public function template_data_mpo_notifkasi($check_data_template)
    {

        return [
            'paymentMethod'  => $check_data_template->payment,
            'nama'           => $check_data_template->nama,
            'va'             => $check_data_template->data_billing->payment_code,
            'amount'         => $check_data_template->newTotalKewajiban,
            'tglExpired'     => $check_data_template->tgl_expired,
            'trxId'          => $check_data_template->idTransaksi,
            'product'        => $check_data_template->product,
            'biayaTransaksi' => $check_data_template->biaya_transaksi,
            'totalKewajiban' => $check_data_template->totalKewajiban,
            'tglTransaksi'   => $check_data_template->updated_at,
            'noHp'           => $check_data_template->noHp,
            'nominalPulsa'   => filter_var($check_data_template->data_produk, FILTER_SANITIZE_NUMBER_INT),
            'mpoProduct'     => $check_data_template->data_produk,
            'mpoGroup'       => $check_data_template->data_group,
            'redaksi'        => $check_data_template->redaksi_payment,
            'idTambahan'     => $check_data_template->idTambahan,
            'norek'          => $check_data_template->norek,
            'namaPelanggan'  => $check_data_template->namaPelanggan,
            'tarifDaya'      => $check_data_template->segmen . '/' . $check_data_template->power,
            'periode'        => $check_data_template->periode,
            'keterangan2'    => $check_data_template->keterangan2,
            'administrasi'   => $check_data_template->administrasi,
            'npwp'           => $check_data_template->npwp,
            'jumlahTagihan'  => $check_data_template->jumlahTagihan,
            'hargaJual'      => $check_data_template->hargaJual,
            'jumlahBill'     => $check_data_template->jumlahBill,
            'url_payment'    => $check_data_template->data_billing->redirect_url
        ];
    }

    public function template_data_mulia_notifkasi($check_data_template)
    {

        return [
            'nama'           => $check_data_template->data_token->nama,
            'namaNasabah'    => $check_data_template->data_token->nama,
            'payment'        => $check_data_template->payment,
            'nama'           => $check_data_template->data_token->nama,
            'va'             => $check_data_template->data_billing->payment_code,
            'totalKewajiban' => $check_data_template->hargaSetelahDiscount,
            'tglExpired'     => $check_data_template->date_expired->format('d/M/Y H:i:s'),
            'tglExpiredUnformated' => $check_data_template->tgl_expired,
            'reffSwitching'  => $check_data_template->idTransaksi,
            'trxId'          => $check_data_template->idTransaksi,
            'biayaTransaksi' => $check_data_template->biaya_transaksi,
            'namaCabang'     => $check_data_template->data_outlet->nama,
            'alamatCabang'   => $check_data_template->data_outlet->alamat . ', ' . $check_data_template->data_outlet->kelurahan . ', ' . $check_data_template->data_outlet->kecamatan . ', ' . $check_data_template->data_outlet->kabupaten . ', ' . $check_data_template->data_outlet->provinsi,
            'telpCabang'     => $check_data_template->data_outlet->telepon,
            'redaksi'        => $check_data_template->redaksi_payment,
            'item'           => json_decode($check_data_template->kepingMuliaJson),
            'tglTransaksi'   => $check_data_template->tglTransaksi,
            'totalHarga'     => $check_data_template->totalHarga,
            'administrasi'   => $check_data_template->administrasi,
            'uangMuka'       => $check_data_template->nominalUangMuka,
            'jenisLogamMulia' => $check_data_template->jenisLogamMulia,
            'realTglTransaksi' => $check_data_template->realTglTransaksi,
            'pokokPembiayaan' => $check_data_template->pokokPembiayaan,
            'tenor' => $check_data_template->tenor,
            'angsuran' => $check_data_template->angsuran,
            'promoCode'       => $check_data_template->promoCode,
            'discountAmount'  => $check_data_template->discountAmount
        ];
    }

    public function notification_reg_auto_debit($user, $data)
    {
        $title = "Tambah Transaksi Auto Debit untuk produk {$data['jenis_transaksi']} {$data['nama_product']} berhasil";
        if ($data['tipe_transaksi'] == 'SL') {
            $title = "Tambah Transaksi Auto Debit untuk produk {$data['jenis_transaksi']} berhasil";
        }
        $content = 'Selamat, Penambahan transaksi auto debit kamu berhasil';
        $contentDesc = 'Terima kasih telah menambahkan transaksi auto debit.';
        $title_notification = 'TRANSAKSI AUTO DEBIT';

        $params = [];
        $params['phoneNumber'] = $user->no_hp;
        $params['cif'] = $user->cif;
        $params['emailSubject'] = $title;
        $params['contentTitle'] = $content;
        $params['contentDescription'] = [$contentDesc];
        $params['contentList'] = [];
        $params['type'] = $this->notification_model::TYPE_DEBIT;
        $params['notificationTitle'] = $title_notification;
        $params['notificationDescription'] = $title;
        $params['additional'] = $this->load->view('mail/notif_reg_auto_debit_success', $data, true);

        $this->globalSendNotification($params, '');

        $this->response['code'] = 200;
        $this->response['status'] = 'Success';
        $this->response['message'] = self::SUCCESS_SENT;

        return $this->response;
    }

    public function getNotification($user_id, $data)
    {
        $this->response = $this->response_service->getResponse('');
        $page = $data['page'] ?? 0;
        $search = $data['search'] ?? null;
        $filter = $data['filter'] ?? null;
        $offset = $this->notification_model::LIMIT * $page;

        $cache_key = implode(':', [$this->cache_service::PREFIX_KEY, $this->cache_service::META_NOTIFICATION, $user_id]);
        $metaNotifDataFromRedis = json_decode($this->redis->get($cache_key), true);
        $notifDataFromRedis = $this->cache_service->getNotifications($user_id, $page + 1, $this->notification_model::LIMIT);
        $responseData = $this->cache_service->convertArrayStringToObject($notifDataFromRedis);

        if (!empty($metaNotifDataFromRedis) && count($responseData) >= $this->notification_model::LIMIT) {
            $this->response = $this->response_service->getResponse(self::RESPONSE_SUCCESS_STRING);
            $this->response = array_merge($this->response, $metaNotifDataFromRedis);
            $this->response['message'] = 'Notifikasi berhasil ditemukan';
            $this->response['data'] = $responseData;

            return $this->response;
        }

        $getNotification = $this->NotificationModel->getNotification($user_id, $offset, $search, $filter);

        if (!$getNotification) {
            return $this->response;
        }

        $config = [
            'base_url' => base_url() . 'notification/',
            'total_rows' => $this->NotificationModel->countNotif($user_id),
            'per_page' => $this->notification_model::LIMIT,
            'use_page_numbers' => true,
            'page_query_string' => true,
            'query_string_segment' => 'page'
        ];

        $this->pagination->initialize($config);

        $data  = [];
        foreach ($getNotification as $n) {
            $n->attachment = $this->NotificationModel->getAttachment($n->id);
            $data[] = $n;
        }

        $unreadNotif = $this->NotificationModel->countUnreadNotif($user_id);
        $readedNotif = $config['total_rows'] - $unreadNotif;

        $setDataRedis = [
            'totalUnread' => $unreadNotif,
            'totalReaded' => $readedNotif,
            'totalData' => $config['total_rows'],
            'page' => $page,
        ];

        $this->cache_service->setMetaNotification("{$user_id}", $setDataRedis);
        $redisStringArray = $this->cache_service->convertArrayObjectToString($data);
        $this->cache_service->setRightNotification("{$user_id}", $redisStringArray);

        $this->response = $this->response_service->getResponse(self::RESPONSE_SUCCESS_STRING);
        $this->response['message'] = 'Notifikasi berhasil ditemukan';
        $this->response['totalUnread'] = $unreadNotif;
        $this->response['totalReaded'] = $readedNotif;
        $this->response['totalData'] = $config['total_rows'];
        $this->response['page'] = $page;
        $this->response['data'] = $data;

        return $this->response;
    }

    public function getDetailNotification($token, $notifId)
    {
        $this->response = $this->response_service->getResponse(self::RESPONSE_SUCCESS_STRING);
        $cache_key = implode(':', [$this->cache_service::PREFIX_KEY, $this->cache_service::NOTIFICATION_DETAIL, $notifId]);
        $notifDetails = json_decode($this->redis->get($cache_key));

        if (!empty($notifDetails)) {
            $this->response['message'] = 'Detail Notifikasi berhasil ditemukan dari redis';
            $this->response['data'] = $notifDetails;

            return $this->response;
        }

        return $this->getNotificationFromDb($token, $notifId);
    }

    private function getNotificationFromDb($token, $notifId)
    {
        $notifDetails = $this->NotificationModel->getDetail($token->id, $notifId);

        if (!$notifDetails) {
            return $this->response;
        }

        $notifDetails->attachment = $this->NotificationModel->getAttachment($notifId);

        if (empty($notifDetails)) {
            $this->response['status'] = 'success';
            $this->response['message'] = 'Detail Notifikasi tidak ditemukan';
            return $this->response;
        }

        $this->cache_service->setNotificationDetail($notifId, $notifDetails);
        $this->response['message'] = 'Detail Notifikasi berhasil ditemukan';
        $this->response['data'] = $notifDetails;

        return $this->response;
    }

    public function setNotificationToRedis($data)
    {
        $cache_key = implode(':', [$this->cache_service::PREFIX_KEY, $this->cache_service::META_NOTIFICATION, $data['user_AIID']]);
        $metaNotifData = json_decode($this->redis->get($cache_key), true);
        $notifDataFromRedis = $this->cache_service->getNotifications($data['user_AIID'], 1, $this->notification_model::LIMIT);

        if (!empty($notifDataFromRedis)) {
            $user_id = $data['user_AIID'];
            $data['contentType'] = $data['content_type'];
            $data['lastUpdate'] = date('Y-m-d H:i:s');
            $data['attachment'] = [];

            unset($data['user_AIID']);
            unset($data['file_jatuh_tempo_id']);
            unset($data['isi']);
            unset($data['isi_minimal']);
            unset($data['payload']);
            unset($data['content_type']);

            $metaNotifData['totalUnread'] += 1;
            $this->cache_service->setMetaNotification($user_id, $metaNotifData);
            $notifDataForRedis = json_encode($data);
            $this->cache_service->setLeftNotification($user_id, $notifDataForRedis);
        }
    }

    public function notifikasiGTEF($trx, $jenis_notif = "pencairan")
    {
        $user = $this->user_model->getUser($trx->user_id);
        if (empty($user)) {
            $this->response['status'] = 'Error';
            $this->response['message'] = 'Data user tidak ditemukan';

            return $this->response;
        }

        $phone_number = $user->no_hp;
        $bank_name = $trx->bank->bank_name ?? $trx->bank->namaBank;
        $account_number_bank = $trx->bank->account_number ?? $trx->bank->virtualAccount;

        $data = array();
        $data['Nomor_Referensi'] = $trx->reff_switching ?? null;
        $data['Nama'] = $trx->customer_name ?? null;
        $data['Nomor_Kredit'] = $trx->credit_number ?? null;
        $data['Tanggal_Sewa_Modal'] = !empty($trx->lease_payment_date) ? Pegadaian::formatDateDmy($trx->lease_payment_date) : null;
        $data['Sewa_Modal'] = !empty($trx->rental_cost) ? Pegadaian::currencyIdr($trx->rental_cost). " /Hari" : null;
        $data['Uang_Pinjaman'] = !empty($trx->amount) ? Pegadaian::currencyIdr($trx->amount) : null;

        if ($trx->trx_type == "OP") {
            $data['Jangka_Waktu'] = !empty($trx->tenor) ? $trx->tenor." hari" : null;
            $data['Biaya_Administrasi'] = !empty($trx->admin_surcharge) ? Pegadaian::currencyIdr($trx->admin_surcharge) : null;
        }
        
        $data['Biaya_Transfer'] = !empty($trx->transfer_surcharge) ? Pegadaian::currencyIdr($trx->transfer_surcharge) : null;
        $data['Tagihan_Perpanjang_Masa_Titipan'] = !empty($trx->deposit_extension_fee) ? Pegadaian::currencyIdr($trx->deposit_extension_fee) : null;
        $data['Tagihan_Sewa_Modal'] = !empty($trx->rental_cost_fee) ? Pegadaian::currencyIdr($trx->rental_cost_fee) : null;
        $data['Denda_Keterlambatan_Sewa_Modal'] = !empty($trx->late_charge_rental_cost) ? Pegadaian::currencyIdr($trx->late_charge_rental_cost) : null;
        $data['Jumlah_Diterima'] = !empty($trx->amount_approved) ? Pegadaian::currencyIdr($trx->amount_approved) : null;
        $data['Barang_Jaminan'] = "Titipan Emas Fisik";
        $data['Nomor_Titipan'] =  $trx->tef_no ?? null;
        $data['Tanggal_Jatuh_Tempo'] = !empty($trx->due_date) ? Pegadaian::formatDateDmy($trx->due_date) : null;
        $data['Tanggal_Lelang'] = !empty($trx->auction_date) ? Pegadaian::formatDateDmy($trx->auction_date) : null;
        $data['Rekening_Tujuan'] =  "{$bank_name} - {$account_number_bank}";
        $data['Atas_Nama'] = $trx->bank->customer_name ?? null;
        
        $contentList = array();
        foreach ($data as $key => $value) {
            if ($value != null) {
                $contentList[] = ['key' => str_replace("_", " ", $key), 'value' => $value];
            }
        }

        $subject = "Gadai Titipan Emas Fisik Berhasil";
        $contentDescription = ['Selamat, Pengajuan Gadai Titipan Emas Fisik kamu telah berhasil dilakukan', 'Berikut merupakan rincian kredit kamu'];
        $tagline = "Pinjaman dengan nomor kredit {$data['Nomor_Kredit']} telah berhasil dilakukan.";
        $contentFooter = ['Pinjaman akan dikirim ke rekening tujuan.'];

        if ($jenis_notif === "pencairan") {
            $contentDescription = ['Selamat, Pencairan Gadai Titipan Emas Fisik kamu telah berhasil di transfer', 'Berikut merupakan rincian kredit kamu'];
            $tagline = "Pinjaman dengan nomor kredit {$data['Nomor_Kredit']} telah berhasil ditransfer ke rekening tujuan.";
            $contentFooter = ['Pinjaman telah dikirim ke rekening tujuan.'];
        }
        
        $params = [
            'phoneNumber' => $phone_number,
            'emailSubject' => $subject,
            'contentTitle' => $subject,
            'contentDescription' => $contentDescription,
            'contentList' => $contentList,
            'contentFooter' => $contentFooter,
            'type' => $this->notification_model::TYPE_GTEF,
            'jenisTransaksi' => $trx->trx_type,
            'notificationTitle' => $subject,
            'notificationDescription' => $tagline,
            'tagline' => $tagline,
        ];

        $response = $this->globalSendNotification($params, '');
        return $response;
    }
}
