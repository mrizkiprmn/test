<?php

/**
 * @property RestPoint_service rest_gpoint_service
 * @property Response_service response_service
 * @property User user_model
 */
class Gpoint_service extends MY_Service
{
    protected $response;

    public function __construct()
    {
        parent::__construct();

        $this->load->model('User', 'user_model');
        $this->load->helper('Pegadaian');
        $this->load->service('RestSwitchingGpoint_service', 'rest_switching_gpoint_service');
        $this->load->service('RestPoint_service', 'rest_gpoint_service');
        $this->load->service('Response_service', 'response_service');
        $this->response = [
            'code'    => 101,
            'status'  => 'error',
            'message' => 'Internal Server Error',
            'data'    => null
        ];
    }

    public function transactionTypeValidate($jenis)
    {
        switch ($jenis) {
            case 'LM':
                $res = [
                    'productCode' => '37',
                    'transactionType' => 'OP'
                ];
                break;
            case 'OP':
                $res = [
                    'productCode' => '62',
                    'transactionType' => 'OP'
                ];
                break;
            case 'SL':
                $res = [
                    'productCode' => '62',
                    'transactionType' => 'SL'
                ];
                break;
            case 'GTE':
                $res = [
                    'productCode' => '32',
                    'transactionType' => 'OP'
                ];
                break;
            case 'MIKRO':
                $res = [
                    'productCode' => '80',
                    'transactionType' => 'OP'
                ];
                break;
            default:
                $res = [
                    'productCode' => '',
                    'transactionType' => ''
                ];
                break;
        }

        return $res;
    }

    public function transactionDate()
    {
        $t = microtime(true);
        $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
        return date('Y-m-d H:i:s.' . substr($micro, 0, 3), $t);
    }

    public function totalKewajiban($data)
    {
        switch ($data['productCode']) {
            case '62':
                $totalKewajiban = $data['totalKewajiban'];
                break;
            case '37':
                $totalKewajiban = $data['totalKewajiban'];
                break;
            case '32':
                $totalKewajiban = $data['totalKewajiban'];
                break;
        }
        if (($totalKewajiban > 0) && ($data['type'] == 'discount')) {
            return $totalKewajiban - $data['value'];
        } else {
            return $totalKewajiban;
        }
    }

    public function messageGpoint($data)
    {
        $product_code = $data['productCode'];
        $type_reward  = $data['typeRewards'];
        $value        = Pegadaian::currencyIdr($data['value']);

        switch ($product_code) {
            case 'MP':
            case '01':
                $product_Code = "voucher {$type_reward}";
                break;
            default:
                $product_Code = "{$type_reward}";
                break;
        }

        return "Selamat Anda mendapatkan {$product_Code} sebesar {$value}";
    }

    public function getListVouchers($user, $page, $limit, $product = '', $type = '', $start_date = '', $end_date = '')
    {
        $this->response['status']  = 'success';
        $this->response['message'] = 'Voucher tidak tersedia';
        $this->response['data']    = [
            'vouchers' => [
                'total_count' => 0, 'list' => []
            ]
        ];

        if ((empty($transaction_type) || $transaction_type == 'SL') && empty($user->cif)) {
            $this->response['message'] = 'CIF tidak ditemukan!';

            return $this->response;
        }

        $payload_voucher = [
            'limit'           => $limit,
            'page'            => $page,
            'startDate'       => $start_date,
            'endDate'         => $end_date,
            'productCode'     => $product,
            'transactionType' => $type,
            'cif'             => $user->cif ?? ''
        ];

        $rest_list_voucher = $this->rest_switching_gpoint_service->vocherList($payload_voucher);
        $rest_code = $rest_list_voucher['responseCode'] ?? null;

        if (empty($rest_code) || ($rest_code != 00 && $rest_code != 99)) {
            $this->response['status'] = 'error';
            $this->response['data']   = $rest_list_voucher;

            return $this->response;
        }

        if (!empty($rest_list_voucher['data'])) {
            $vouchers = json_decode($rest_list_voucher['data'], true) ?? [];
            $this->response['message'] = 'List data Voucher';
            $this->response['data']['vouchers'] = [
                'total_count' => $vouchers['rewards']['totalCount'] ?? $this->response['data']['voucher']['total_count'],
                'list' => $this->setScreenMobileDataListVoucher($vouchers['rewards']['listVoucher'])
            ];
        }

        return $this->response;
    }

    public function getListPromotions($product = '', $type = '')
    {
        $this->response['status']  = 'success';
        $this->response['message'] = 'Promo tidak tersedia';
        $this->response['data']    = [
            'promotions' => [
                'total_count' => 0, 'list' => []
            ]
        ];

        // Return Empty for product code micro
        if ($product == '80') {
            return $this->response;
        }

        $payload_promo = ['product' => $product, 'transactionType' => $type];
        $rest_list_promotions = $this->rest_switching_gpoint_service->promoList($payload_promo);
        $rest_code = $rest_list_promotions['responseCode'] ?? null;

        if (empty($rest_code) || ($rest_code != 00 && $rest_code != 99)) {
            $this->response['status'] = 'error';
            $this->response['data']   = $rest_list_promotions;

            return $this->response;
        }

        if (!empty($rest_list_promotions['data'])) {
            $promotions = json_decode($rest_list_promotions['data'], true) ?? [];
            $promosi = $promotions['promos'];

            foreach ($promosi as $pr) {
                if ($pr['isPrivate'] != 1) {
                    $listPromo[] = $pr;
                }
            }
            
            $this->response['message'] = 'List data Promotions';
            $this->response['data']['promotions'] = [
                'total_count' => count($promotions['promos']),
                'list' => $listPromo ?? [],
            ];
        }

        return $this->response;
    }

    public function showMileStone($user)
    {
        $this->response['code']    = '200';
        $this->response['status']  = 'success';
        $this->response['message'] = 'Show Milestone';
        $this->response['data'] = [
            'milestone' => [
                'stage' => $this->config->item('stage'),
                'limitRewardCounter' => 0,
                'limitReward' => (string) ($this->config->item('stage') * $this->config->item('limit_reward_counter')),
                'totalRewardCounter' => 0,
                'totalReward' => 0,
                'ranking' => null,
                'link' => ''
            ]

        ];

        if (empty($user->cif) && empty($user->referralCode)) {
            return $this->response;
        }

        $data = [
            'cif' => $user->cif,
            'referralCode' => $user->referralCode
        ];

        $rest_switching_service = new RestSwitchingGpoint_service();
        $rest_switching         = $rest_switching_service->mileStone($data);
        $response_code          = $rest_switching['responseCode'] ?? null;

        if ($response_code != 00) {
            return $this->response;
        }

        $data      = json_decode($rest_switching['data']);
        $milestone = $data->dataGpoin->milestone ?? null;

        $this->response['data']['milestone'] = [
            'stage'              => $milestone->stage ?? $this->response['data']['milestone']['stage'],
            'limitRewardCounter' => $milestone->limitRewardCounter ?? $this->response['data']['milestone']['limitRewardCounter'],
            'limitReward'        => $milestone->limitReward ?? $this->response['data']['milestone']['limitReward'],
            'totalRewardCounter' => $milestone->totalRewardCounter ?? $this->response['data']['milestone']['totalRewardCounter'],
            'totalReward'        => $milestone->totalReward ?? $this->response['data']['milestone']['totalReward'],
            'ranking'            => $milestone->ranking->noRanking ?? $this->response['data']['milestone']['ranking'],
            'link'               => $this->config->item('web_sahabat_pegadaian') ?? 'https://sahabatpegadaian.com/program-ajak-sahabat/'
        ];

        return $this->response;
    }

    public function checkTransactionGpoint($user, $transaction_type, $referral)
    {
        $this->response['code']    = '200';
        $this->response['status']  = 'success';
        $this->response['message'] = 'Cek History Gpoint';

        if (!empty($referral)) {
            $rest_gpoint_service = new RestSwitchingGpoint_service();
            $rest_gpoint         = $rest_gpoint_service->cekTransaksi([
                'cif'         => $user->cif,
                'productCode' => $transaction_type['productCode']
            ]);

            if (!empty($rest_gpoint['responseCode']) && $rest_gpoint['responseCode'] !== '00') {
                $this->response['status']  = 'error';
                $this->response['message'] = $rest_gpoint['responseDesc'];

                return $this->response;
            }
        }

        return $this->response;
    }

    public function dataInquiryReferral($inquiry)
    {
        $discAmount = 0;
        $journalAccount = '';
        $voucherCode = '';
        $voucherName = '';
        $cif = '';
        $dataInq = json_decode($inquiry->data);
        $dataArr = (array) $dataInq;

        foreach ($dataArr['rewards'] as $val) {
            // hitung discAmount Referral
            if ($val->reference == 'referral') {
                $discAmount += floatval($val->value);
                $type = $val->type;
                $journalAccount .= ';' . $val->journalAccount;
                $voucherCode .= ';' . $val->voucherCode;
                $voucherName .= ';' . $val->voucherName;
                $cif .= ';' . $val->cif;
            }
        }
        $data = [
            'discountAmount' => $discAmount,
            'value' => $discAmount,
            'type' => $type,
            'journalAccount' => $journalAccount,
            'voucherCode' => $voucherCode,
            'voucherName' => $voucherName,
            'cif' => $cif,
            'refTrx' => $dataArr['refTrx'],
            'idPromosi' => $dataArr['idPromosi'],
            'reffSwitching' => $dataArr['reffSwitching'],
            'rewards' => $dataArr['rewards']
        ];
        return $data;
    }

    public function getVoucherHistory($user, $page, $limit)
    {
        $this->response['status']  = 'success';
        $this->response['message'] = 'Voucher History tidak tersedia';
        $this->response['data']    = [
            'vouchers' => [
                'total_count' => 0, 'list' => []
            ]
        ];

        $payload_voucher_history = [
            'limit' => $limit,
            'page'  => $page,
            'cif'   => $user->cif ?? ''
        ];

        $rest_list_voucher_history = $this->rest_switching_gpoint_service->voucherHistory($payload_voucher_history);
        $rest_code = $rest_list_voucher_history['responseCode'] ?? null;

        if (empty($rest_code) || ($rest_code != 00 && $rest_code != 99)) {
            $this->response['status'] = 'error';
            $this->response['data']   = $rest_list_voucher_history;

            return $this->response;
        }

        if (!empty($rest_list_voucher_history['data'])) {
            $vouchers = json_decode($rest_list_voucher_history['data'], true) ?? [];
            $this->response['message'] = 'List data Voucher History';
            $this->response['data']['vouchers'] = [
                'total_count' => $vouchers['totalCount'],
                'list' => $this->mappingVoucherHistory($vouchers['listVoucher']),
            ];
        }

        return $this->response;
    }

    public function getListRanking($user, $period = '')
    {
        $this->response['code']    = '200';
        $this->response['status']  = 'success';
        $this->response['message'] = 'List Ranking';
        $this->response['data'] = [
            'link' =>'',
            'list' => [],
            'user' => null
        ];

        if (empty($period)) {
            $period = new DateTime('now');
            $period = $period->format('Y-m-d');
        }

        $period     = date('Y-m-d', strtotime($period));
        $start_date = date('Y-m-01', strtotime($period));
        $end_date   = date('Y-m-t', strtotime($period));

        $rest_switching_service = new RestSwitchingGpoint_service();

        $referral_code   = $user->referral_code ?? '';
        $rest_switching  = $rest_switching_service->referralRank([
            'referralCode' => $referral_code,
            'startDate'    => $start_date,
            'endDate'      => $end_date
        ]);

        $response_code = $rest_switching['responseCode'] ?? null;

        if ($response_code != 00) {
            return $this->response;
        }

        $data = json_decode($rest_switching['data'], true);
        $data = array_filter($data);

        foreach ($data as $ranking) {
            $user = $this->User->getUserByReferralCode($ranking['referralCode']);

            $this->response['data']['list'][] = [
                'noRanking' => $ranking['noRanking'],
                'referralCode' => $ranking['referralCode'],
                'totalUsed' => $ranking['totalUsed'],
                'name' => !empty($user) ? $user->nama : '',
            ];

            if (!empty($referral_code) && $ranking['isReferralCode'] == 'true') {
                $this->response['data']['user'] = $ranking;
                $this->response['data']['user']['name'] = $user->nama;
            }
        }
        $this->response['data']['link'] = $this->config->item('web_ranking_sahabat_pegadaian') ?? 'https://sahabatpegadaian.com/pegadaian-ajak-sahabat';

        return $this->response;
    }

    public function getCustomerTotalPoint($user_id)
    {
        $this->response = $this->response_service->getResponse($this->response_service::GENERAL_SUCCESS);
        $this->response['message'] = 'Get data point successfully';
        $this->response['data'] = ['point' => 0];

        $user = $this->user_model->getUser($user_id);
        $cif  = $user->cif ?? null;

        if (empty($cif)) {
            return $this->response;
        }

        $rest_point = $this->rest_gpoint_service->getCustomerTotalPoint($cif);

        if (empty($rest_point->success)) {
            return $this->response;
        }

        $this->response['data'] = $rest_point->results->total_point ?? 0;

        return $this->response;
    }
    
    public function seedPegawai()
    {
        $this->load->library('CSVReader');
        $this->load->model('PegawaiModel');

        $this->PegawaiModel->deleteAll();

        $this->response['code']    = '101';
        $this->response['status']  = 'error';
        $this->response['message'] = 'Seed Data Pegawai';
        $this->response['data']    = [];

        $base_file  = $this->config->item('base_url');
        $file       = $base_file.'/uploaded/pegawai/pegawai.csv';
        $csv_reader = new CSVReader();
        $csv_reader->separator = ',';
        $csv_data = $csv_reader->parse_file($file);
        $data = [];
        foreach ($csv_data as $items) {
            foreach ($items as $key => $item) {
                if ($key !== 'no') {
                    $data[$key] = $item;
                }
            }

            $data_pegawai = [
                'nip'  => $data['nip'],
                'nama' => $data['nama'],
                'ktp'  => $data['ktp'],
                'hp_1' => $data['hp_1'],
                'hp_2' => $data['hp_2'],
                'cif'  => $data['cif'],
                'ket'  => $data['ket'],
            ];

            $pegawai = $this->PegawaiModel->insert($data_pegawai);

            if ($pegawai != 'sukses') {
                return $this->response;
            }
        }

        $this->response['code']    = '200';
        $this->response['status']  = 'success';
        $this->response['message'] = 'Seed Data Pegawai';
        $this->response['data']    = [];

        return $this->response;
    }

    public function rewardReject($user, $rest_switching)
    {
        $gpoint = $this->GpoinModel->check($rest_switching);

        if (empty($gpoint)) {
            $this->response['message'] = 'Data reward tidak ditemukan';

            return $this->response;
        }

        $payload_reward_reject = [
            'cif' => $user->cif ?? '',
            'refTrx' => $gpoint->refTrx ?? '',
            'reffIdSwitching' => $gpoint->reffSwitching ?? ''
        ];

        $rest = $this->rest_switching_gpoint_service->rewardsReject($payload_reward_reject);
        $rest_code = $rest['responseCode'] ?? null;

        if (empty($rest_code) || ($rest_code != 00 && $rest_code != 99)) {
            $this->response['message'] = 'Pembatalan Promo Gagal';
            $this->response['data']    = $rest['responseDesc'] ?? null;

            return $this->response;
        }

        $this->response['status']  = 'success';
        $this->response['message'] = 'Pembatalan Promo Sukses';
        $this->response['data']    = $gpoint;

        return $this->response;
    }

    public function getDataPoint($user_id)
    {
        $user = $this->user_model->getUser($user_id);
        $authorize = $this->rest_gpoint_service->getAuthorization();

        $params = [
            'token' => $authorize ?? '',
            'name' => $user->nama,
            'cif' => $user->cif ?? '',
            'account_type' => 'Akun '. $this->user_model->getAccountType($user) ?? ''
        ];

        $data = [
            'url' => $this->config->item('point_url'),
            'params' => $params
        ];

        $this->response['status'] = 'success';
        $this->response['message'] = 'Get data point successfully';
        $this->response['data'] = $data;

        return $this->response;
    }

    private function setScreenMobileDataListVoucher($vouchers)
    {
        foreach ($vouchers as $voucher) {
            $temp_voucher = $this->setterVoucher($voucher);
            switch ($temp_voucher['productCode']) {
                case ($temp_voucher['productCode'] == '32' && $temp_voucher['transactionType'] == 'OP');
                    $temp_voucher['screen'] = 'TabunganEmasWizard';
                    break;
                case ($temp_voucher['productCode'] == '37' && $temp_voucher['transactionType'] == 'OP');
                    $temp_voucher['screen'] = 'MuliaLogam';
                    break;
                case ($temp_voucher['productCode'] == '62' && $temp_voucher['transactionType'] == 'OP');
                    $temp_voucher['screen'] = 'BukaTabunganEmasWizard';
                    break;
                case ($temp_voucher['productCode'] == '62' && $temp_voucher['transactionType'] == 'SL');
                    $temp_voucher['screen'] = 'EmasBeli';
                    break;
                default;
                    $temp_voucher['screen'] = '';
                    break;
            }

            $data[] = $temp_voucher;
        }

        return $data ?? $vouchers;
    }

    private function mappingVoucherHistory($vouchers)
    {
        foreach ($vouchers as $voucher) {
            $items[] = $this->setterVoucher($voucher);
        }

        return $items ?? [];
    }

    private function setterVoucher($data)
    {
        return [
            'id'                 => $data['id'] ?? '',
            'name'               => $data['name'] ?? '',
            'description'        => $data['description'] ?? '',
            'startDate'          => $data['startDate'] ?? '',
            'endDate'            => $data['endDate'] ?? '',
            'point'              => $data['point'] ?? '',
            'type'               => $data['type'] ?? '',
            'stock'              => $data['stock'] ?? '',
            'productCode'        => $data['productCode'] ?? '',
            'transactionType'    => $data['transactionType'] ?? '',
            'promoCode'          => $data['promoCode'] ?? '',
            'minLoanAmount'      => $data['minLoanAmount'] ?? '',
            'termsAndConditions' => $data['termsAndConditions'] ?? '',
            'howToUse'           => $data['howToUse'] ?? '',
            'status'             => $data['status'] ?? '',
            'createdAt'          => $data['createdAt'] ?? '',
            'imageUrl'           => !empty($data['imageUrl']) ? $this->config->item('asset_url') . $data['imageUrl'] : '',
        ];
    }
}
