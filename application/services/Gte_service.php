<?php

/**
 * @property MasterModel master_model
 * @property ProductMasterModel product_master_model
 * @property RestSwitchingPortofolio_service rest_switching_portofolio_service
 */
class Gte_service extends MY_Service
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('ProductMasterModel', 'product_master_model');
        $this->load->model('MasterModel', 'master_model');
        $this->load->service('RestSwitchingPortofolio_service', 'rest_switching_portofolio_service');
        $this->load->service('RestSwitchingSimulasi_service', 'rest_switching_simulasi_service');
    }

    protected $response = [
        'code' => 101,
        'status' => 'error',
        'message' => 'Internal Server Error',
        'data' => null
    ];

    public function simulasiGte($data)
    {
        $gte_min_sisa_saldo = $this->master_model->getHargaGadaiEmas('gte_min_sisa_saldo');

        $datarek = array(
            'norek' => $data['norek']
        );

        //get Rekening berdasarkan Cif
        $daftarRekening = $this->rest_switching_portofolio_service->detailTabunganEmas($datarek);

        if ($daftarRekening['responseCode'] == '12') {
            $this->response['message'] = 'Saldo emas tidak mencukupi';
            return $this->response;
        }

        //get Saldo efektif dari rekening
        $saldoEfektif = json_decode($daftarRekening['data'])->saldoEfektif ?? '';
        

        //get productCode
        $productCode = $this->product_master_model->get_productName('GADAI TABUNGAN EMAS');
        $data['productCode'] = $productCode->productCode;
        $data['jenisTransaksi'] = "OP";

        if ($data['gram'] != "0") {
            $data['tipe'] = "1";
        }

        if ($data['amount'] != "0") {
            $data['tipe'] = "2";
        }

        //get data dari RestSwitchingService
        $res = $this->rest_switching_simulasi_service->simulasiGte($data);

        if ($res['responseCode'] == "15") {
            $this->response['message'] = 'GTE dengan Tabungan Pegawai Internal Hanya untuk tenor 120 Hari';
            return $this->response;
        }
        
        if ($res['responseCode'] != "00") {
            $this->response['message'] = $res['responseDesc'];
            return $this->response;
        }
        
        $hasilgram = json_decode($res['data'])->gram ?? 0;

        $transaksiGram = $saldoEfektif - $hasilgram;

        if (!empty($data['amount']) && $hasilgram > $saldoEfektif) {
            $this->response['message'] = 'Saldo emas tidak mencukupi';
            return $this->response;
        }

        if ($transaksiGram < $gte_min_sisa_saldo) {
            $this->response['message'] = 'Saldo mengendap minimal ' . $gte_min_sisa_saldo . ' gram.';
            return $this->response;
        }

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Simulasi Gadai Tabungan Emas Berhasil';
        $this->response['data'] = json_decode($res['data']);

        return $this->response;
    }
}
