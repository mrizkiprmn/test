<?php

/**
 * @property RekeningBankGadaiModel rekening_bank_gadai_model
 * @property RestCoreGadai_service rest_core_gadai_service
 * @property User user_model
 * @property Mikro_service mikro_service
 * @property RestSwitchingPortofolio_service rest_switching_portofolio_service
 */
class Gadai_service extends MY_Service
{
    /**
     * @var array
     */
    protected $response = [
        'code' => 101,
        'status' => 'error',
        'message' => 'Terjadi kesalahan mohon coba lagi',
        'data' => null
    ];

    public function __construct()
    {
        parent::__construct();
        // init load model
        $this->load->model('RekeningBankGadaiModel', 'rekening_bank_gadai_model');
        $this->load->model('GadaiModel', 'gadai_model');
        $this->load->model('User', 'user_model');
        $this->load->model('ConfigModel', 'config_model');
        $this->load->model('MasterModel', 'master_model');
        $this->load->service('Mikro_service', 'mikro_service');
        $this->load->service('RestCoreGadai_service', 'rest_core_gadai_service');
        $this->load->service('RestSwitchingPaymentEmas_service', 'rest_switching_payment_emas_service');
        $this->load->service('RestSwitchingPayment_service', 'restswitching_payment_service');
        $this->load->service('RestSwitchingPortofolio_service', 'rest_switching_portofolio_service');
        $this->load->service('Notification_service', 'notification_service');
        $this->load->service('Response_service', 'response_service');
    }

    public function updateGadaiGosend($data_gadai, $data_gosend)
    {
        $code_booking = $data_gadai->kodeBooking;

        switch ($code_booking[0]) {
            case 1:
                $data = $this->updateGadai($data_gadai, $data_gosend, 'gadai_perhiasan');
                break;
            case 2:
                $data = $this->updateGadai($data_gadai, $data_gosend, 'gadai_logam_mulia');
                break;
            default:
                $data = null;
        }

        return $data;
    }

    private function updateGadai($data_gadai, $data_gosend, $table)
    {
        $status_gosend_no_pickup = ['Enroute Pickup', 'Out For Pickup', 'Out For Delivery'];
        $fcm_gadai_no_pickup = [0, 99, 98];
        $update_data = [];

        if (in_array($data_gosend->status, $status_gosend_no_pickup) && in_array($data_gadai->fcm, $fcm_gadai_no_pickup)) {
            $update_data['status'] = 5;
            $update_data['status_fcm'] = 1;
        }

        if ($data_gosend->status == 'Driver not found' && $data_gadai->statusFcm == 0) {
            $update_data['status'] = 8;
            if ($data_gadai->statusFcm == 0) {
                $update_data['status_fcm'] = 99;
            }
        }

        if ($data_gosend->status == 'Completed') {
            $update_data['status'] = 9;
            if ($data_gadai->statusFcm == 1) {
                $update_data['status_fcm'] = 2;
            }
        }

        if ($data_gosend->status == 'Cancelled') {
            $update_data['status'] = 8;
            if ($data_gadai->statusFcm == 1 || $data_gadai->statusFcm == 0) {
                $update_data['status_fcm'] = 98;
            }
        }

        // Except condition query on service :v because call dynamic table
        log_message('debug', 'update data => ' . json_encode($update_data) . ' table  => ' . $table);
        $this->db
            ->where('kode_booking', $data_gadai->kodeBooking)
            ->update($table, $update_data);

        return $data_gadai->kodeBooking;
    }

    //Khusus Pembayaran Maybank
    public function paymentGadai($payment, $checkPayment, $productCode, $idTransaksi, $user, $token, $kodeBank)
    {
        $biayaTransaksi = $this->config_model->getRealBiayaPayment($checkPayment->jenisTransaksi, 'BANK', $productCode, $kodeBank);
        $biayaTransaksiDisplay = $this->config_model->getBiayaPayment($checkPayment->jenisTransaksi, 'BANK', $productCode, $kodeBank);

        $data = array(
            'amount' => $checkPayment->totalKewajiban + $biayaTransaksi,
            'channelId' => $token->channelId,
            'clientId' => $this->config->item('core_post_username'),
            'customerEmail' => $user->email,
            'customerName' => $user->nama,
            'customerPhone' => $user->no_hp,
            'flag' => 'K',
            'jenisTransaksi' => $checkPayment->jenisTransaksi,
            'keterangan' => "PDS" . $checkPayment->jenisTransaksi . " " . $idTransaksi . " " . $checkPayment->norek,
            'kodeProduk' => $productCode,
            'kodeBank' => $kodeBank,
            'reffSwitching' => $idTransaksi,
        );

        $billingMayBank = $this->rest_switching_payment_emas_service->createBillingVAMayBank($data, $checkPayment->norek);

        if ($billingMayBank['responseCode'] != '00') {
            $this->response['message'] = $billingMayBank['responseDesc'];
            return $this->response;
        }

        $dataPrefix = $this->master_model->getPrefixBank('prefixMaybank');
        $billingData = json_decode($billingMayBank['data']);
        $virtualAccount = $billingData->vaNumber;
        $virtualAccountFormat = $dataPrefix . substr($virtualAccount, 0, 11);
        $tglExpired = $billingData->tglExpired;

        $updatePaymentData = array(
        'tipe' => $payment,
        'virtual_account' => $virtualAccount,
        'tanggal_expired' => $tglExpired,
        'payment' => $payment,
        'biayaTransaksi' => $biayaTransaksiDisplay,
        'kodeBankPembayar' => $kodeBank,
        );

        //Buat payment baru
        //Simpan payment
        $this->gadai_model->updatePayment($idTransaksi, $updatePaymentData);

        $this->notification_service->notificationPaymentGadai(
            $checkPayment,
            $virtualAccountFormat,
            $tglExpired,
            $idTransaksi,
            $token,
            $productCode
        );

        $dataPayment = array(
            'idTransaksi' => $idTransaksi,
            'virtualAccount' => $virtualAccountFormat,
            'expired'        => $tglExpired,
            'now'            => date('Y-m-d H:i:s')
        );

        //Set response
        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Mohon lakukan pembayaran ke no rekening yang tertera';
        $this->response['data'] = $dataPayment ?? null;

        return $this->response;
    }

    public function getListRekeningUser($user)
    {
        return $this->rekening_bank_gadai_model->getByUserId($user->user_AIID);
    }

    public function storeRekeningUser($user_id, $kode_bank, $nomor_rekening, $nama_pemilik)
    {
        $rekening = $this->rekening_bank_gadai_model->find([
            'user_id' => $user_id,
            'nomor_rekening' => $nomor_rekening
        ]);

        if (empty($rekening)) {
            $this->rekening_bank_gadai_model->insert([
                'user_id' => $user_id,
                'nomor_rekening' => $nomor_rekening,
                'kode_bank' => $kode_bank,
                'nama_pemilik' => $nama_pemilik,
            ]);
        }
    }

    public function getDetailKontrakHtml($params)
    {
        // get detail gadai
        $detailGadai = $this->gadai_model->getDetailKontrakGod($params['kode_booking'], $params['jenis_gadai']);

        // if detail gadai not exist
        if (!$detailGadai) {
            $this->response['message'] = 'Detail gadai tidak tersedia!';
            log_message('debug', 'response ' . __FUNCTION__ . ' data: ' . json_encode($detailGadai));
            return;
        }

        $dataGadai = $this->gadai_model->getPaymentGadaiByKodeBooking($params['kode_booking']);

        // generate gada qr code image
        include "phpqrcode/qrlib.php";
        $qr_img_dir = $this->config->item('upload_dir') . '/user/gadai/';

        if (!file_exists($qr_img_dir)) {
            mkdir($qr_img_dir);
        }

        // set qr code data
        $isi_teks = array(
            'namaNasabah' => $detailGadai->nasabah,
            'cif' => $detailGadai->cif,
            'no_ktp' => $detailGadai->noIdentitas,
            'kodeOtp' => $detailGadai->kodeOtp,
            'tglBooking' => date('d-m-Y', strtotime($detailGadai->tglBooking))
        );

        $dataQR = json_encode($isi_teks);
        $hash_kodeBooking = sha1($params['kode_booking']);
        $qr_img_file = $hash_kodeBooking . ".png";

        // save qrcode if not exist
        if (!file_exists($qr_img_dir . $qr_img_file)) {
            QRCode::png($dataQR, $qr_img_dir . $qr_img_file, 'H', 5, 0);

            if ($params['jenis_gadai'] == '2') {
                $this->gadai_model->simpanBarcode('', $params['kode_booking'], $isi_teks, $qr_img_file);
            } else {
                $this->gadai_model->simpanBarcodePerhiasan('', $params['kode_booking'], $isi_teks, $qr_img_file);
            }
        }

        // set template data
        $templateData = array(
            'noPengajuan' => $params['kode_booking'],
            'barcode' =>  $this->config->item('asset_url') . 'user/gadai/' . $qr_img_file,
            'tglLelang' => $detailGadai->tglLelang,
            'tglTransaksi' => $detailGadai->tglTransaksi,
            'administrasi' => $detailGadai->administrasi,
            'serialNumber' => $detailGadai->serialNumber,
            'sewaModal' => $detailGadai->sewaModal,
            'tglJatuhTempo' => $detailGadai->tglJatuhTempo,
            'biayaProsesLelang' => $detailGadai->biayaProsesLelang ?: '1',
            'biayaLelang' => $detailGadai->biayaLelang ?: '2',
            'nasabah' => $detailGadai->nasabah,
            'kodeNamaCabang' => $detailGadai->kodeNamaCabang,
            'biayaAdmBjpdl' => '1' ?? $detailGadai->biayaAdmBjpdl,
            'biayaBjdplMax' => '8.5' ?? $detailGadai->biayaBjdplMax,
            'tanggal_pembayaran' => $detailGadai->tanggal_pembayaran,
            'jumlahHariTarif' => $detailGadai->jumlahHariTarif,
            'taksir_ulang_admin' => $detailGadai->taksir_ulang_admin,
            'namaPetugas' => $detailGadai->nama,
            'noIdentitas' => $detailGadai->noIdentitas,
            'jalan' => $detailGadai->jalan,
            'rubrik' => $detailGadai->rubrik,
            'taksiran' => $detailGadai->taksiran,
            'up' => $detailGadai->up,
            'tglKredit' => $detailGadai->tglKredit,
            'cif' => $detailGadai->cif,
            'no_kredit' => $detailGadai->no_kredit,
            'nama_group' => $detailGadai->nama_group,
            'nama_cabang' => $detailGadai->nama_cabang,
            'namaPinca' => $detailGadai->namaPinca,
            'detailGadai' => $detailGadai,
            'checkPayment' => $dataGadai,
            'asuransi' => $detailGadai->asuransi,
            'diterimaNasabah' => $detailGadai->diterimaNasabah
        );

        // template for payment status
        if ($dataGadai->count > 0) {
            $content = $this->load->view('mail/lihat_detail_gadai', $templateData, true);
        } else {
            $content = $this->load->view('mail/belum_pembayaran_detail_gadai', $templateData, true);
        }

        $this->response['code'] = '00';
        $this->response['status'] = 'success';
        $this->response['message'] = 'Lihat Surat Bukti Gadai';
        $this->response['html'] = $content;

        return $this->response;
    }

    public function getPortofolio($user_id)
    {
        $user = $this->user_model->getUser($user_id);

        if (empty($user->cif)) {
            $this->response['message'] = 'Data CIF tidak ditemukan';
            return $this->response;
        }

        switch (true) {
            case (bool) $this->config->item('rest_core_is_active'):
                $portofolio_gadai = $this->getPortofolioByCif($user->cif);
                $portofolio_micro = $this->mikro_service->getPortofolio($user_id);
                $portofolio_data  = array_merge($portofolio_gadai, $portofolio_micro);
                break;
            default:
                $rest_portofolio = $this->rest_switching_portofolio_service->pinjaman(['cif' => $user->cif]);
                $portofolio_data = json_decode($rest_portofolio['data'], true);
                break;
        }

        $this->response['code'] = 200;
        $this->response['status'] = 'success';
        $this->response['message'] = 'Get portofolio gadai successfully';
        $this->response['data'] = $portofolio_data;

        return $this->response;
    }

    public function getPortofolioByCif($cif)
    {
        $rest_data = $this->rest_core_gadai_service->byCif(['cif' => $cif]);
        $this->response['message'] = $rest_data['responseMessage'] ?? $this->response['message'];

        if (!empty($rest_data['responseCode']) && $rest_data['responseCode'] != '00' && $rest_data['responseCode'] != '14') {
            return [];
        }

        return $rest_data['responseData'] ?? [];
    }

    //Pembayaran Telkompay/Finpay
    public function payment_finpay_gadai($payment, $check_payment)
    {
        $biaya_transaksi = $this->config_model->getRealBiayaPayment($check_payment->jenisTransaksi, 'BANK', $check_payment->productCode, $check_payment->kode_bank);
        $amount = $check_payment->totalKewajiban;

        // max_transaksi 10.000.000, max_transaksi_admin 10.001.500
        $max_transaksi = $this->config_model->whereByVariable('max_transaksi_finpay');
        $max_transaksi_msg = Pegadaian::currencyIdr($max_transaksi->value);
        $max_transaksi_admin = $max_transaksi->value + $biaya_transaksi;

        if ($amount > $max_transaksi->value && $amount < $max_transaksi_admin || $amount > $max_transaksi_admin) {
            $this->response['message'] = "Transaksi tidak dapat diproses, Maksimal transaksi $max_transaksi_msg";
            return $this->response;
        }

        $data = [
            'amount' => strval($amount + $biaya_transaksi),
            'channelId' => $check_payment->data_token->channelId,
            'clientId' => $this->config->item('core_post_username'),
            'customerEmail' => $check_payment->data_user->email,
            'customerName' => $check_payment->data_user->nama,
            'customerPhone' => $check_payment->data_user->no_hp,
            'flag' => 'K',
            'jenisTransaksi' => $check_payment->jenisTransaksi,
            'keterangan' => "PDS" . $check_payment->jenisTransaksi . " " . $check_payment->idTransaksi . " " . $check_payment->norek,
            'norek' => $check_payment->norek,
            'productCode' => $check_payment->productCode,
            'trxId' => $check_payment->idTransaksi,
        ];

        $billing_virtual = $this->restswitching_payment_service->createBillingFinPay($data);

        if ($billing_virtual['responseCode'] != '00') {
            $this->response['message'] = $billing_virtual['responseDesc'];
            return $this->response;
        }

        $data_billing = json_decode($billing_virtual['data']);
        $tgl_expired = date('Y-m-d H:i:s', time() + (60 * 180));

        $update_payment_data = [
            'tipe' => $payment,
            'virtual_account' => $data_billing->payment_code,
            'tanggal_expired' => $tgl_expired,
            'payment' => $payment,
            'biayaTransaksi' => $biaya_transaksi,
            'kodeBankPembayar' => $check_payment->kode_bank,
        ];

        $this->gadai_model->updatePayment($check_payment->idTransaksi, $update_payment_data);

        $this->notification_service->notificationPaymentGadai(
            $check_payment,
            $data_billing->payment_code,
            $tgl_expired,
            $check_payment->idTransaksi,
            $check_payment->data_token,
            $check_payment->productCode
        );

        $data_payment = [
            'idTransaksi' => $check_payment->idTransaksi,
            'virtualAccount' => $data_billing->payment_code,
            'expired'        => $tgl_expired,
            'now'            => date('Y-m-d H:i:s')
        ];

        $response = $this->response_service->getResponse('general.succeeded');
        $response['message'] = 'Mohon lakukan pembayaran ke no rekening yang tertera';
        $response['data'] = $data_payment ?? null;

        return $response;
    }
}
