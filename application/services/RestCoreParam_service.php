<?php


/**
 * @property RestCore_Service rest_core_service
 */
class RestCoreParam_service extends MY_Service
{
    private $prefix_url = 'konven/param';
    /**
     * RestCoreKonven_service constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->service('RestCore_Service', 'rest_core_service');
    }

     // Endpoint POST /konven/param/hargaemas
    public function hargaEmas(array $data)
    {
        $url = "{$this->prefix_url}/hargaemas";

        return $this->rest_core_service->postData($url, $data);
    }
}
