<?php

/**
 * @property RestSwitching_service rest_switching_service
 */
class RestSwitchingSimulasi_service extends MY_Service
{
    /**
     * RestSwitchingSimulasi_service constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->service('RestSwitching_service', 'rest_switching_service');
    }

    public function simulasiTabemas($data)
    {
        $url = '/simulasi/tabemas';
        
        return $this->rest_switching_service->postData($url, $data);
    }

    public function simulasiGte($data)
    {
        $url = '/simulasi/gte';

        return $this->rest_switching_service->postData($url, $data);
    }

    public function muliaSimulasi($data)
    {
        $url = '/mulia/simulasi';

        return $this->rest_switching_service->postData($url, $data);
    }
}
