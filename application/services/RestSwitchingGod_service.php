<?php

/**
 * @property RestSwitching_service rest_switching_service
 */
class RestSwitchingGod_service extends MY_Service
{
    /**
     * @var
     */
    private $client;

    /**
     * RestGosend_service constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->service('RestSwitching_service', 'rest_switching_service');
    }

    /**
     * Endpoint POST /midtrans/paymid
     *
     * @param $data
     * @return mixed|null
     */
    public function payMidtransGod($data)
    {
        $url = '/midtrans/paymid';

        return $this->rest_switching_service->postData($url, $data);
    }

    /**
     * Endpoint POST /god/cancel
     *
     * @param $data
     * @return mixed|null
     */
    public function godCancel($data)
    {
        $url = '/god/cancel';

        return $this->rest_switching_service->postData($url, $data);
    }

    /**
     * Endpoint POST /midtrans/get-status
     *
     * @param $data
     * @return mixed|null
     */
    public function statusMidtrans($data)
    {
        $url = '/midtrans/get-status';

        return $this->rest_switching_service->postData($url, $data);
    }
}
