<?php
defined('BASEPATH') || exit('No direct script access allowed');

use Predis\Autoloader as PredisAutoloader;
use Predis\Client as PredisClient;

class Redis extends PredisClient
{

    private $redis_scheme, $redis_host, $redis_port, $redis_password;

    public $redis, $redis_expiry;

    public function __construct()
    {
        PredisAutoloader::register();
        $this->validationConfig();

        parent::__construct([
            'scheme'   => $this->redis_scheme,
            'host'     => $this->redis_host,
            'port'     => $this->redis_port,
            'password' => $this->redis_password
        ]);
    }


    public function setex($key, $value, $expire_ttl = null)
    {
        if (empty($expire_ttl)) {
            $expire_ttl = $this->redis_expiry;
        }

        return parent::setex($key, $expire_ttl, $value);
    }

    private function validationConfig()
    {
        $validation           = true;
        $this->redis_scheme   = getenv('REDIS_SCHEME');
        $this->redis_host     = getenv('REDIS_HOST');
        $this->redis_port     = getenv('REDIS_PORT');
        $this->redis_password = getenv('REDIS_PASS');
        $this->redis_expiry   = getenv('REDIS_EXPIRY');

        if (empty($this->redis_scheme)) {
            $validation = false;
        }

        if (empty($this->redis_host)) {
            $validation = false;
        }

        if (empty($this->redis_port)) {
            $validation = false;
        }

        if (!$validation) {
            showError('Environment redis not setup !');
        }
    }
}
