<?php defined('BASEPATH') or exit('No direct script access allowed');
/**
 * CodeIgniter DomPDF Library
 *
 * Generate PDF's from HTML in CodeIgniter
 *
 * @packge        CodeIgniter
 * @subpackage        Libraries
 * @category        Libraries
 * @author        Ardianta Pargo
 * @license        MIT License
 * @link        https://github.com/ardianta/codeigniter-dompdf
 */

use Dompdf\Dompdf;

class Pdf extends Dompdf
{
    public function __construct()
    {
        parent::__construct();
    }

    protected function ci()
    {
        return get_instance();
    }

    public function load_view($url, $view, $data = array())
    {
        $html = $this->ci()->load->view($view, $data, true);

        $this->load_html($html);
        $this->render();
        $output = $this->output();
        file_put_contents($url . $this->filename, $output);
    }
}
