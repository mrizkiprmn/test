<?php

require_once APPPATH . 'libraries/JWT.php';

use \Firebase\JWT\JWT;

class Authorization
{
    public static function validateToken($token)
    {
        $CI =& get_instance();
        $CI->load->model('user');
        $CI->load->service('Cache_service', 'cache_service');

        $key       = $CI->config->item('jwt_key');
        $algorithm = $CI->config->item('jwt_algorithm');
        $decoded   = null;

        try {

            $decoded = JWT::decode($token, $key, array($algorithm));
            $session_token = $CI->cache_service->getTokenJwt($decoded->id);

            if ($decoded->access_token != $session_token) {
                $decoded = null;
            }
        } catch (Exception $e) {
            $decoded = null;
        }

        return $decoded;
    }

    public static function generateToken($data)
    {
        $CI  = &get_instance();
        $key = $CI->config->item('jwt_key');
        $exp = $CI->config->item('jwt_exp');
        $data['exp'] = $exp;

        return JWT::encode($data, $key);
    }

    public static function tokenIsExist($headers)
    {
        return (array_key_exists('Authorization', $headers) && !empty($headers['Authorization']));
    }

    public static function validateMuliaToken($token)
    {
        $CI =& get_instance();
        $CI->load->model('user');
        $key = $CI->config->item('jwt_key');
        $algorithm = $CI->config->item('jwt_algorithm');
        $decoded = null;

        try {
            $decoded = JWT::decode($token, $key, array($algorithm));
        } catch (Exception $e) {
            $decoded = null;
        }

        return $decoded;
    }
}
