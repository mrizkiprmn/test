<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Message
{
    const TYPE_PROFILE = "profile";
    const TYPE_EMAS = "emas";
    const TYPE_GADAI = "gadai";
    const TYPE_MICRO = "micro";
    const TYPE_MPO = "mpo";
    const TYPE_UANG_KELEBIHAN = "uang_kelebihan";
    
    /**
     * Kirim email reset password ke email user
     * @param string $email
     * @param string $nama
     * @param string $token hash code yang digunakan sebagai token pada url
     */
    public static function sendResetPasswordEmail($email, $nama, $token)
    {
        $ci =& get_instance();
        $viewData = array(
            'nama' => $nama,
            'email' => $email,
            'token' => $token
        );
        $message = $ci->load->view('mail/reset_password', $viewData, true);

        
        self::sendEmail($email, 'Reset Password Pegadaian Digital Service', $message);
    }

    public static function sendEmailVerificationEmail($email, $nama, $token)
    {
        if (!self::emailCheck($email)) {
            return false;
        }

        $ci =& get_instance();
        log_message("info", "Email Verification Token = " . $token);

        $message = $ci->load->view('mail/email_verification', [
            'nama' => $nama,
            'email' => $email,
            'token' => $token
        ], true);

        self::sendEmail($email, 'Verifikasi Email ' . $nama, $message, false);
    }
    
    public static function sendEmailSuccessResetPin($email, $nama)
    {
        $subject = "Notifikasi Reset Pin". $nama;
        $ci =& get_instance();

        $viewData = array(
            'nama' => $nama,
            'tanggal' => date("d-m-Y H:i:s")
        );
        // print_r($viewData); exit;
        $message = $ci->load->view('mail/email_notif_sukses_resetpin', $viewData, true);
        self::sendEmail($email, $subject, $message);
    }

    public static function sendEmailOpenTabunganPayment($email, $message)
    {
        
        $subject = "Pengajuan Pembukaan Rekening Tabungan Emas Pegadaian";
        self::sendEmail($email, $subject, $message);
    }
    
    public static function sendEmailOpenTabungaEmasSuccess($email, $message)
    {
        $subject = "Selamat Rekening Tabungan Emas Anda Sudah Aktif";
        self::sendEmail($email, $subject, $message);
    }
    
    public static function sendEmailBeliEmas($email, $rekeningEmas, $message)
    {
        $subject = "Konfirmasi Pembelian Tabungan Emas Pegadaian ".$rekeningEmas;
        self::sendEmail($email, $subject, $message);
    }
    
    public static function sendEmailBeliEmasSuccess($email, $body)
    {
        $subject = "Selamat Top Up Tabungan Emas Berhasil";
        self::sendEmail($email, $subject, $body);
    }
    
    public static function GoldcardSendEmail($email, $body, $subject)
    {
        self::sendEmail($email, $subject, $body);
    }
    
    public static function sendEmailMicro($email, $nama, $noPengajuan)
    {
        $ci =& get_instance();
        
        $viewData = array(
            'nama' => $nama,
            'noPengajuan' => $noPengajuan
        );
        
        $message = $ci->load->view('mail/notif_micro', $viewData, true);
        self::sendEmail($email, 'Pembiayaan Usaha', $message);
    }
    
    public static function sendEmailKYC($email, $nama, $status, $deskripsi)
    {
        $ci =& get_instance();
        
        $viewData = array(
            'nama' => $nama,
            'status' => $status,
            'deskripsi' => $deskripsi,
        );
        
        $message = $ci->load->view('mail/notif_kyc', $viewData, true);
        self::sendEmail($email, 'KYC', $message);
    }


    public static function sendEmailApprovalKYC($email, $nama, $status, $judul, $kycMessage, $cabang)
    {
        $ci =& get_instance();
        
        $viewData = array(
            'nama' => $nama,
            'status' => $status,
            'judul' => $judul,
            'pesan' => $kycMessage,
            'cabang' => $cabang,
        );
        
        $message = $ci->load->view('mail/notif_kyc_approval', $viewData, true);
        self::sendEmail($email, $judul, $message);
    }
    
    public static function sendEmailGeneral($email, $title, $text)
    {
        $ci =& get_instance();
        
        $viewData = array(
            'title' => $title,
            'message' => $text,
        );
        
        $message = $ci->load->view('mail/email_template', $viewData, true);
        self::sendEmail($email, $title, $message);
    }


    public static function sendNotifWelcome($token, $idNotif, $nama)
    {
        $title = "Verifikasi Email";
        $body = "Hai ".$nama.',  Selamat datang di Pegadaian Digital Service';
        $type = self::TYPE_PROFILE;
        $data = array(
            'title' => $title,
            'body' => $body,
            'type' => $type,
            'id' => $idNotif
        );
        self:: sendFCMNotif($token, $data);
    }
    
    public static function sendNotifOpenTabunganEmas()
    {
        $ci =& get_instance();
        
        //Send email notification
    }

    public static function sendEmail($email, $subject, $message, $is_verify_email = true)
    {
        $send_email_verify = self::sendEmailUsingVerify($email, $is_verify_email);

        if (!$send_email_verify) {
            return false;
        }

        try {
            log_message('debug', 'SendEmail: ' . $email . ' => ' . $subject);

            $ci = &get_instance();
            $ci->load->library('PHPMailer_library');

            $mail = $ci->phpmailer_library->load();
            $mail->IsSMTP();                                            // Set mailer to use SMTP
            $mail->Host = $ci->config->item('pds_email_host');          // Specify main and backup server
            $mail->Port = $ci->config->item('pds_email_port');          // Set the SMTP port
            $mail->SMTPAuth = true;                                     // Enable SMTP authentication
            $mail->Username = $ci->config->item('pds_email_username');  // SMTP username
            $mail->Password = $ci->config->item('pds_email_password');  // SMTP password
            $mail->SMTPSecure = 'tls';                                  // Enable encryption, 'ssl' also accepted
            $mail->From = $ci->config->item('pds_email_from');
            $mail->FromName = $ci->config->item('pds_email_fromname');
            $mail->AddAddress($email);                                  // Add a recipient
            $mail->IsHTML(true);                                        // Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body    = $message;
            $mail->AltBody = '';

            if (!$mail->Send()) {
                log_message('debug', 'Failed send email to: ' . $email);
            }

            log_message('debug', 'SendEmail: ' . $email . ' => ' . $subject . ' success');
        } catch (Exception $e) {
            log_message('debug', 'SendEmail Caught exception: ' . $email . ' => ' . $subject . ' ' . $e->getMessage());
        }
    }

    /**
     * Kirim notifikasi FCM
     * @param string|array $to devicet token id
     * @param string $type tipe pesan yang dikirimkan, lihat const
     * @param string $title judul pesan
     * @param string $body isi pesan
     * @param array $data data pesan
     */
    public static function sendFCMNotif($to, $data)
    {
        if (isset($data['id'])) {
            $data['idNotif'] = $data['id'];
        }

        $CI = &get_instance();
        $server_key = $CI->config->item('firebase_server_key');

        $notificationObject['badge'] = '0';
        $notificationObject['sound'] = 'default';
        if (isset($data['title'])) {
            $notificationObject['title'] = $data['title'];
        }

        if (isset($data['tagline'])) {
            $notificationObject['body'] = $data['tagline'];
        }

        log_message("info", "SEND FCM NOTIF TO: " . $to . "|| Data: " . json_encode($data));

        $messageData = json_encode(array(
            'notification' => $notificationObject,
            'data' => $data,
            'registration_ids' => [$to],
        ));

        //FCM API end-point
        $url = 'https://fcm.googleapis.com/fcm/send';

        //header with content_type api key
        $headers = array(
            'Content-Type:application/json',
            'Authorization:key=' . $server_key
        );

        log_message('debug', "Param FCM" . $messageData);

        //CURL request to route notification to FCM connection server (provided by Google)
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $messageData);
        $result = curl_exec($ch);
        if (json_decode($result)->failure == '1') {
            log_message('debug', 'FCM NOT SENT: '.json_encode($data).' Reason: '.$result);
        }
        log_message('debug', 'FCM SENT: '.json_encode($data));
        log_message('debug', 'FCM SENT: '. $result);
        curl_close($ch);
    }

    public static function generateBankNotif($viewData)
    {
        $ci = &get_instance();
        $subject = "Rekening Bank";
        $content = $ci->load->view('mail/notif_rekening_bank', $viewData, true);
        $email = $ci->load->view('mail/email_template_top', array('title' => $subject), true);
        $email = $email . $content;
        $email = $email . $ci->load->view('mail/email_template_bottom', array(), true);
        $mobile = $ci->load->view('notification/top_template', array('title' => $subject), true);
        $mobile = $mobile . $content;
        $mobile = $mobile . $ci->load->view('notification/bottom_template', array(), true);

        return array(
            'email' => $email,
            'mobile' => $mobile,
            'minimal' => $content
        );
    }

    public static function emailCheck($email)
    {
        $ci = &get_instance();
        $ci->load->service('RestZeroBounce_service');

        $rest_zero_bounce = new RestZeroBounce_service();
        $validator_email  = $rest_zero_bounce->validateEmail($email);

        if ($validator_email['status'] !== 'success') {
            return false;
        }

        if (!empty($validator_email['data']['error'])) {
            log_message('debug', "RestZeroBounce_service validator is Limit and Bypass validation");
            log_message('debug', "RestZeroBounce_service message => " . $validator_email['data']['error']);
            return true;
        }

        $is_valid_email  = $validator_email['data']['status'] == 'valid';
        $log_email_check = $is_valid_email ? 'true' : 'false';

        log_message('debug', "RestZeroBounce_service validator : {$email} => {$log_email_check}");

        return $is_valid_email;
    }

    public static function sendEmailUsingVerify($email, $use_verify_email)
    {
        if (!$use_verify_email) {
            log_message('debug', "verifyEmailUserCheck Use :  => false");
            return true;
        }

        log_message('debug', "verifyEmailUserCheck Use : => true");

        return self::verifyEmailUserCheck($email);
    }

    public static function verifyEmailUserCheck($email)
    {
        $ci = &get_instance();
        $ci->load->model('User');

        $user_model = new User();
        $is_verify_email = $user_model->isVerifiedEmail($email);
        $log_verify_email = $is_verify_email ? '1' : '0';

        log_message('debug', "verifyEmailUserCheck Result : {$email} => {$log_verify_email}");

        return $is_verify_email;
    }

    public static function globalSendEmail($email, $body, $subject)
    {
        self::sendEmail($email, $subject, $body);
    }
}
