<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pegadaian
{
    const DATE_FORMAT = 'Y-m-d';

    public static function generateReferralCode($prefix = 'PDS')
    {
        $random = substr(uniqid(), 8, 15);
        $code = $prefix == 'PDS' ? $prefix . $random : $prefix;

        return (strtoupper($code));
    }

    public static function currencyIdr($data)
    {
        $currency = number_format($data, 0, ".", ".");

        return "Rp {$currency}";
    }

    public static function getStatusLos()
    {
        return [
            '1.0',
            '1.1',
            '2.0',
            '6.0',
            '7.0',
            '8.0',
        ];
    }

    public static function geTitleLos($status)
    {
        $list_title = [
            '1.0' => 'Pengajuan.',
            '1.1' => 'Checking',
            '2.0' => 'Survey',
            '6.0' => 'Persetujuan',
            '7.0' => 'Konfirmasi',
            '8.0' => 'Pencairan',
            '9.0' => 'Approve',
            '9.3' => 'Reject',
        ];

        return !empty($list_title[$status]) ? $list_title[$status] : '';
    }

    public static function getDescriptionStatusLos($status, $flag_approve = true)
    {
        $confirmation = 'Sesuai dengan hasil konfirmasi kamu, pengajuan kredit kamu telah disetujui.';
        $approved     = 'Pengajuan kredit kamu telah disetujui.';
        $checked      = 'Pengajuan kamu sedang dalam proses pengecekan oleh pihak Pegadaian.';
        $survey       = 'Pihak Pegadaian akan menghubungi kamu untuk melakukan survey tempat usaha/kerja.';

        if ($flag_approve == false) {
            $approved     = 'Untuk sementara Pengajuan kamu belum bisa kami setujui';
            $confirmation = 'Sesuai dengan hasil konfirmasi kamu, kamu belum menyetujui fasilitas kredit yang di berikan.';
            $checked      = $approved;
            $survey       = $approved;
        }

        $list_description = [
            '1.0' => 'Proses pengajuan pembiayaan dapat berlangsung hingga 2 hari kerja.',
            '1.1' => $checked,
            '2.0' => $survey,
            '6.0' => $approved,
            '7.0' => $confirmation,
            '8.0' => 'Pengajuan pembiayaan kamu sudah selesai, silahkan lakukan pencairan di outlet Pegadaian yang dipilih.',
        ];

        return !empty($list_description[$status]) ? $list_description[$status] : '';
    }

    public static function formatDateLos($value)
    {
        return date('d M Y H.i', strtotime($value));
    }

    public static function showError($message, $data = null, $status_code = 200, $status = 'error')
    {
        $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');

        switch ($status_code) {
            case 403:
                $text = 'Forbidden';
                break;
            case 401:
                $text = 'Unauthorized';
                break;
            default:
                $text = 'OK';
                break;
        }

        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');
        header($protocol . ' ' . $status_code . ' ' . $text);

        echo json_encode(
            [
                'code'    => $status_code,
                'status'  => $status,
                'message' => $message,
                'data'    => $data
            ]
        );

        exit;
    }

    public static function showErrorValidation($validation)
    {
        foreach ($validation as $key => $value) {
            $error[] = [
                'field' => $key,
                'message' => $value
            ];
        }

        return $error ?? [];
    }

    public static function getDescriptionTransactionType($transaction_type, $product_code)
    {
        if (empty($product_code)) {
            return '';
        }

        if ($product_code == '37' && $transaction_type == 'OP') {
            return 'Cicil Emas';
        }

        if ($product_code == '32' && $transaction_type == 'OP') {
            return 'Gadai Tabungan Emas';
        }

        if ($product_code != '62') {
            return '';
        }

        switch ($transaction_type) {
            case 'OP':
                return 'Buka Tabungan Emas';
                break;
            case 'TR':
                return 'Transfer Emas';
                break;
            case 'BB':
                return 'Jual Emas';
                break;
            case 'SL':
                return 'Topup Emas';
                break;
            case 'OD':
                return 'Order Cetak Emas';
                break;
        }

        return '';
    }

    public static function formatDate($value)
    {
        $format_date = strtotime($value);

        return !empty($format_date) ? date('d M Y', $format_date) : null;
    }

    public static function calculateCoupon($constraints, $value)
    {
        $result = 0;
        foreach ($constraints as $constraint) {
            $division = $value / $constraint['transaction_amount'];
            if (intval($division) > 0) {
                $result = $constraint['coupon_reward'] * intval($division);
                $value = $value - ($constraint['transaction_amount'] * intval($division));
                break;
            }
        }

        if ($result > 0) {
            $result += self::calculateCoupon($constraints, $value);
        }

        return $result;
    }

    public static function getCouponConstraints($transaction_type, $value)
    {
        switch ($transaction_type) {
            case 'SL':
            case 'LM':
            case 'GTE':
                $result = [
                    [
                        "transaction_amount" => 500000,
                        "coupon_reward" => 25
                    ],
                    [
                        "transaction_amount" => 100000,
                        "coupon_reward" => 3
                    ]
                ];
                break;
            case 'OP':
                $result = [
                    [
                        "transaction_amount" => 50000,
                        "coupon_reward" => 10
                    ]
                ];
                break;
            case 'MC':
                $result = [
                    [
                        "transaction_amount" => 1000000,
                        "coupon_reward" => 50
                    ]
                ];
                break;
            case 'MPO':
                $result = [
                    [
                        "transaction_amount" => $value,
                        "coupon_reward" => 3
                    ]
                ];
                break;
            default:
                $result = [];
                break;
        }

        return $result;
    }

    /**
     * Validasi JSON key
     * @param string $keys list key yang akan di cek
     * @param string $arr JSON yang di cek
     * @return bool|array return TRUE apabila seluruh key ada, dan return array bila terdapat key yang tidak ada
     */
    public static function validateJSON(array $keys, array $arr)
    {
        $array = array_diff_key(array_flip($keys), $arr);

        if (!empty($array)) {
            $error = array();
            foreach ($array as $key => $value) {
                $error[$key] = "The " . $key . " field is required";
            }
            return $error;
        }

        return true;
    }

    /**
     * Generate ID string by timestamp until milli second
     * @return string
     */
    public static function getMillSecID()
    {
        $t = microtime(true);
        $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
        $d = new DateTime(date('Y-m-d H:i:s.' . $micro, $t));

        return $d->format('YmdHisu');
    }

    /**
     * Method ini digunakan untuk menerjemahkan istilah asing ke bahasa Indonesia menggunakan regular expression
     * @param $source: merupakan string yang ingin diterjemahkan (Contoh: January => Januari).
     * @return string
     */
    public static function getLocalization($source)
    {
        $patterns = ['/Jan[^uary]/', '/Feb[^ruary]/', '/Mar[^ch]/', '/Apr[^il]/', '/May/', '/Jun[^e]/', '/Jul[^y]/', '/Aug[^ust]/', '/Sep[^tember]/', '/Oct[^ober]/', '/Nov[^ember]/', '/Dec[^ember]/', '/January/', '/February/', '/March/', '/April/', '/June/', '/July/', '/August/', '/September/', '/October/', '/November/', '/December/'];
        $replacements = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des', 'Januari', 'Februari', 'Maret', 'April', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

        $result = preg_replace($patterns, $replacements, $source);

        return $result;
    }

    /**
     * Method ini digunakan untuk mengubah snake_case menjadi camelCase
     * @param $source: merupakan string yang ingin dibuah
     * @return string
     */
    public static function getCamelCase($source)
    {
        return lcfirst(self::getPascalCase($source));
    }

    /**
     * Method ini digunakan untuk mengubah snake_case menjadi PascalCase
     * @param $source: merupakan string yang ingin dibuah
     * @return string
     */
    public static function getPascalCase($source)
    {
        return preg_replace('/[\s]/', '', ucwords(preg_replace('/[\_-]/', ' ', $source)));
    }

    /**
     * Method ini digunakan untuk mengubah camelCase menjadi snake_case
     * @param $source: merupakan string yang ingin dibuah
     * @return string
     */
    public static function getSnakeCase($source)
    {
        $source = self::getCamelCase($source);
        return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $source));
    }

    /**
     * Method ini digunakan untuk mengubah key di dalam object menjadi snake_case, camelCase, PascalCase
     * @param $object: merupakan objek yang berisi key dan value
     * @param $function: merupakan method lain yang akan dipanggil dari dalam method ini
     * @return string
     */
    public static function getObjectWithConvertedKey($object, $function)
    {
        $object = (array) $object;
        $results = array_map(function ($item) use ($object, $function) {
            $temp = [];
            $temp[$function($item)] = $object[$item];
            return $temp;
        }, array_keys($object));

        return array_reduce($results, function ($acc, $item) {
            $key = key((array) $item);
            $acc[$key] = $item[$key];
            return $acc;
        }, []);
    }

    /**
     * Method ini digunakan untuk mengubah key di dalam object menjadi snake_case
     * @param $object: merupakan objek yang berisi key dan value
     * @return string
     */
    public static function getObjectWithSnakeCaseKey($object)
    {
        return self::getObjectWithConvertedKey($object, function ($key) {
            return self::getSnakeCase($key);
        });
    }

    /**
     * Method ini digunakan untuk mengubah key di dalam object menjadi camelCase
     * @param $object: merupakan objek yang berisi key dan value
     * @return string
     */
    public static function getObjectWithCamelCaseKey($object)
    {
        return self::getObjectWithConvertedKey($object, function ($key) {
            return self::getCamelCase($key);
        });
    }

    /**
     * Method ini digunakan untuk mengubah key di dalam object menjadi PascalCase
     * @param $object: merupakan objek yang berisi key dan value
     * @return string
     */
    public static function getObjectWithPascalCaseKey($object)
    {
        return self::getObjectWithConvertedKey($object, function ($key) {
            return self::getPascalCase($key);
        });
    }

    /**
     * Method ini digunakan untuk mendapatkan list dengan format penulisan key of object berupa snake_case
     * @param $list: merupakan array of object yang ingin diubah forman penulisan "key"nya
     * @return string
     */
    public static function getListWithSnakeCaseKey($list)
    {
        $list = (array) $list;
        return array_map(function ($item) {
            $item = (array) $item;
            $item = self::getObjectWithSnakeCaseKey($item);
            return $item;
        }, $list);
    }

    /**
     * Method ini digunakan untuk mendapatkan list dengan format penulisan key of object berupa camelCase
     * @param $list: merupakan array of object yang ingin diubah forman penulisan "key"nya
     * @return string
     */
    public static function getListWithCamelCaseKey($list)
    {
        $list = (array) $list;
        return array_map(function ($item) {
            $item = (array) $item;
            $item = self::getObjectWithCamelCaseKey($item);
            return $item;
        }, $list);
    }

    /**
     * Method ini digunakan untuk mendapatkan list dengan format penulisan key of object berupa PascalCase
     * @param $list: merupakan array of object yang ingin diubah forman penulisan "key"nya
     * @return string
     */
    public static function getListWithPascalCaseKey($list)
    {
        $list = (array) $list;
        return array_map(function ($item) {
            $item = (array) $item;
            $item = self::getObjectWithPascalCaseKey($item);
            return $item;
        }, $list);
    }
    
    public static function checkRekeningNumeric($str)
    {
        $strArray = explode("_", $str);
        $strRegex = '/^[0-9]+$/';

        if (count($strArray) == 1 && preg_match($strRegex, $str)) {
            return true;
        }
        
        $ci = &get_instance();
        
        foreach ($strArray as $st) {
            if (!preg_match($strRegex, $st)) {
                $ci->form_validation->set_message('checkRekeningNumeric', 'The no_rekening field must contain only numbers.');
                return false;
            }
        }
        
        if (strlen($strArray[0]) < 3 || strlen($strArray[0]) > 5) {
            $ci->form_validation->set_message('checkRekeningNumeric', 'Kode Area tidak boleh kurang dari 3 atau lebih dari 5 digit');
            return false;
        }
        
        return true;
    }

    public static function validationPassword($password, $field_name = 'Password')
    {
        $lowercase    = preg_match('@[a-z]@', $password);
        $uppercase    = preg_match('@[A-Z]@', $password);
        $number       = preg_match('@[0-9]@', $password);
        $is_have_word = preg_match_all('/gadai/', strtolower($password));
        $validation   = [
            'is_valid' => true,
            'message'  => ""
        ];

        if (empty($password)) {
            $validation['is_valid'] = false;
            $validation['message']  = "{$field_name} harus diisi.";
            return $validation;
        }

        if (empty($lowercase)) {
            $validation['is_valid'] = false;
            $validation['message']  = "{$field_name} harus terdapat satu huruf kecil.";
            return $validation;
        }

        if (empty($uppercase)) {
            $validation['is_valid'] = false;
            $validation['message']  = "{$field_name} harus terdapat satu huruf kapital.";
            return $validation;
        }

        if (!$number) {
            $validation['is_valid'] = false;
            $validation['message']  = "{$field_name} harus terdapat angka.";
            return $validation;
        }

        if ($is_have_word) {
            $validation['is_valid'] = false;
            $validation['message']  = "{$field_name} Hindari menggunakan kata gadai.";
            return $validation;
        }

        if (strlen($password) < 8) {
            $validation['is_valid'] = false;
            $validation['message']  = "{$field_name} harus terdapat minimal 8 karakter.";
            return $validation;
        }

        return $validation;
    }

    // function to find whether
    // the string has all same
    // characters or not.
    public static function allCharactersSame($s, $c)
    {
        $n = strlen($s);
        for ($i = 0; $i < $n; $i++) {
            if ($s[$i] != $c) {
                return false;
            }
        }

        return true;
    }

    public static function getDaysInIndonesia()
    {
        $hari = date("D");

        switch ($hari) {
            case 'Sun':
                $hari_ini = "Minggu";
                break;

            case 'Mon':
                $hari_ini = "Senin";
                break;

            case 'Tue':
                $hari_ini = "Selasa";
                break;

            case 'Wed':
                $hari_ini = "Rabu";
                break;

            case 'Thu':
                $hari_ini = "Kamis";
                break;

            case 'Fri':
                $hari_ini = "Jumat";
                break;

            case 'Sat':
                $hari_ini = "Sabtu";
                break;

            default:
                $hari_ini = "Tidak di ketahui";
                break;
        }

        return $hari_ini;
    }

    public static function encode_img_base64($img_path = false, $img_type = 'png')
    {
        if ($img_path) {
            //convert image into Binary data
            $img_data = fopen($img_path, 'rb');
            $img_size = filesize($img_path);
            $binary_image = fread($img_data, $img_size);
            fclose($img_data);
    
            //Build the src string to place inside your img tag
            $img_src = "data:image/".$img_type.";base64,".str_replace("\n", "", base64_encode($binary_image));
    
            return $img_src;
        }
    
        return false;
    }

    public static function monthsToSeconds($monthsAdded) {
        $two_month = date(self::DATE_FORMAT, strtotime($monthsAdded));
        return strtotime($two_month);
    }

    /**
     * Method ini digunakan untuk mendapatkan nilai berformat tanggal tanpa waktu
     * @param $value: merupakan string yang ingin diubah ke format tanggal
     * @return string
     */
    public static function formatDateDmy($value, $localization = true)
    {
        if ($localization) {
            return self::getLocalization(date('d F Y', strtotime($value)));
        }
        
        return date('d F Y', strtotime($value));
    }
}
