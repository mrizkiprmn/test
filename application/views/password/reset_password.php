<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Pegadaian Digital Service</title>

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        .banner {
            border: 1px solid #0F941A;
            background-color: #5EBD3B;
            padding: 5px;
            text-decoration: none;
            color: white;
        }

        .btn-primary-pegadaian {
            border: 1px solid #0F941A;
            background-color: #5EBD3B;
            color: white;
        }
        
        .btn-danger-pegadaian {
            border: 1px solid #a94442;
            background-color: #a94442;
            color: white;
        }

        .btn-primary-pegadaian:hover{
            background-color: #0F941A;
            color: white !important;
        }

        .title {
            font-weight: bold;
            font-size: 24;
        }

        #err-msg {
            color: #a94442;
        }

        @import url('https://fonts.googleapis.com/icon?family=Material+Icons');

        #togglePassword {
            margin-left: -30px;
            cursor: pointer;
        }

        #password {
            width: 100%;
        }

        #password-input {
            overflow: hidden;
            white-space: nowrap;
        }

        /* Add a green icon and a checkmark when the requirements are right */
        .valid {
            color: #00ab4e;
        }

        /* Add a grey icon when the requirements are wrong */
        .invalid {
            color: #959595;
        }

        .display-none {
            display: none;
        }
    </style>
</head>
<body>
    <div class="banner">
        <div class="container">
            <h3>Pegadaian Digital Service </h3>
        </div>

    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default" style="margin-top: 60px">
                    <div class="panel-body">
                        <h3 class="control-label title">Reset Password</h3>
                        <hr>
                        <form method="post" method="post" action="<?php echo base_url();?>auth/reset_password">
                            <input type="hidden" value="<?php echo $token?>" name="token">
                            <div class="form-group <?php echo form_error('password') != null ? 'has-error': '' ?>">
                                <label for="password" class="control-label">Password Baru</label>
                                <br>
                                <div id="password-input">
                                    <input type="password" name="password" id="password" placeholder="" required>
                                    <i class="far fa-eye" id="togglePassword"></i>
                                </div>

                                <?php
                                if (form_error('password') != null) {
                                    echo '<div class="help-block" id="err-msg" >'.form_error('password').'</div>';
                                }
                                ?>
                                <div id="message">
                                    <p>
                                        <p id="length" class="invalid">
                                            <i class="fa fa-circle" id="length-uncheck" aria-hidden="true"></i>
                                            <i class="fa fa-check-circle display-none" id="length-check" aria-hidden="true"></i>
                                            Minimal 8 karakter
                                        </p>
                                        <p id="capital" class="invalid">
                                            <i class="fa fa-circle" id="capital-uncheck" aria-hidden="true"></i>
                                            <i class="fa fa-check-circle display-none" id="capital-check" aria-hidden="true"></i>
                                            Terdapat huruf kapital
                                        </p>
                                        <p id="number" class="invalid">
                                            <i class="fa fa-circle" id="number-uncheck" aria-hidden="true"></i>
                                            <i class="fa fa-check-circle display-none" id="number-check" aria-hidden="true"></i>
                                            Terdapat angka
                                        </p>
                                </div>
                            </div>

                            <div class="form-group pull-right">
                                <button type="submit" class="btn btn-danger-pegadaian disabled" id="button-submit" disabled>Ubah Password</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <script type="text/javascript">
        jQuery(document).on("click", ".icon-eye.show-pwd", function() {
            jQuery(this).toggleClass("active");
            var input = jQuery(this).parent().find("input");
            if (input.attr("type") == "text")
                input.attr("type", "password");
            else
                input.attr("type", "text");
        });

        $(document).ready(function() {
            var myInput = document.getElementById("password");
            var capital = document.getElementById("capital");
            var number = document.getElementById("number");
            var length = document.getElementById("length");
        });

        const togglePassword = document.querySelector('#togglePassword');
        const password = document.querySelector('#password');

        togglePassword.addEventListener('click', function(e) {
            const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
            password.setAttribute('type', type);
            this.classList.toggle('fa-eye-slash');
        });

        var myInput = document.getElementById("password");
        var capital = document.getElementById("capital");
        var number = document.getElementById("number");
        var length = document.getElementById("length");
        var buttonSubmit = document.getElementById("button-submit");

        var capitalFlag = 0;
        var numberFlag = 0;
        var lengthFlag = 0;

        myInput.onkeyup = function() {

            // Validasi upper case untuk tampilan front
            var upperCaseLetters = /[A-Z]/g;
            if (myInput.value.match(upperCaseLetters)) {
                capital.classList.remove("invalid");
                capital.classList.add("valid");
                document.getElementById("capital-check").style.display = "inline";
                document.getElementById("capital-uncheck").style.display = "none";
                capitalFlag = 1;
            } else {
                capital.classList.remove("valid");
                capital.classList.add("invalid");
                document.getElementById("capital-check").style.display = "none";
                document.getElementById("capital-uncheck").style.display = "inline";
                capitalFlag = 0;
            }

            // Validasi number untuk tampilan front
            var numbers = /[0-9]/g;
            if (myInput.value.match(numbers)) {
                number.classList.remove("invalid");
                number.classList.add("valid");
                document.getElementById("number-check").style.display = "inline";
                document.getElementById("number-uncheck").style.display = "none";
                numberFlag = 1;
            } else {
                number.classList.remove("valid");
                number.classList.add("invalid");
                document.getElementById("number-check").style.display = "none";
                document.getElementById("number-uncheck").style.display = "inline";
                numberFlag = 0;
            }

            // Validasi length untuk tampilan front
            if (myInput.value.length >= 8) {
                length.classList.remove("invalid");
                length.classList.add("valid");
                document.getElementById("length-check").style.display = "inline";
                document.getElementById("length-uncheck").style.display = "none";
                lengthFlag = 1;
            } else {
                length.classList.remove("valid");
                length.classList.add("invalid");
                document.getElementById("length-check").style.display = "none";
                document.getElementById("length-uncheck").style.display = "inline";
                lengthFlag = 0;
            }

            if (capitalFlag == 1 && numberFlag == 1 && lengthFlag == 1) {
                buttonSubmit.classList.remove("disabled");
                document.getElementById("button-submit").removeAttribute("disabled");
                document.getElementById("button-submit").classList.remove("btn-danger-pegadaian");
                document.getElementById("button-submit").classList.add("btn-primary-pegadaian");
            } else {
                buttonSubmit.classList.add("disabled");
                document.getElementById("button-submit").setAttribute("disabled", "");
                document.getElementById("button-submit").classList.add("btn-danger-pegadaian");
                document.getElementById("button-submit").classList.remove("btn-primary-pegadaian");
            }
        }
    </script>
</body>
</html>
