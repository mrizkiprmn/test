<center><b>Perjanjian Utang Piutang dengan Jaminan Gadai</b></center>
<center><b>Produk Pegadaian Kredit Angsuran Sistem Gadai (Krasida)</b></center>
<center>Nomor : <?= $noKredit; ?></center>

<br>
Pada hari ini <?= $hari;?> tanggal <?= date('d-m-Y'); ?> bertempat di PT PEGADAIAN (Persero) (<?= $namaOutlet; ?>),
kami yang bertanda tangan di bawah ini:

<br><br>
<table>
    <tr>
        <td style="text-align: justify; vertical-align: top;">
            I.
        </td>
        <td style="text-align: justify; vertical-align: top;">
            Nama <?= $namaPinca; ?>, jabatan Pemimpin CP/UPC, dalam hal ini bertindak dan atas nama PT PEGADAIAN (Persero), yang selanjutnya disebut PEGADAIAN.
        </td>
    </tr>
    <tr>
        <td style="text-align: justify; vertical-align: top;">
            II.
        </td>
        <td style="text-align: justify; vertical-align: top;">
            Nama <?= $user->nama; ?>, No KTP <?= $user->no_ktp; ?>, alamat <?= $user->alamat; ?> Kode POS <?= $user->kodepos; ?> Kelurahan <?= $kelurahan; ?> Kecamatan <?= $kecamatan; ?> Kotamadya/Kabupaten <?= $kabupaten; ?> Provinsi <?= $provinsi; ?>, dalam hal ini bertindak untuk dan atas nama diri sendiri/perusahaan* yang saya pimpin
            <br>
            <br>selanjutnya disebut NASABAH.
        </td>
    </tr>
</table>

<br>

<p style="text-align: justify;">PEGADAIAN dan NASABAH yang selanjutnya bersama-sama disebut PARA PIHAK, sepakat
dan setuju untuk membuat Perjanjian Pemberian Fasilitas Kredit dengan Jaminan Titipan Emas,
yang selanjutnya disebut Perjanjian Utang Piutang dengan Jaminan Gadai, dengan syarat-syarat dan ketentuan sebagai berikut :</p>

<br>
<table>
    <tr>
        <td style="text-align: justify; vertical-align: top;">
            1.
        </td>
        <td style="text-align: justify; vertical-align: top;">
            PEGADAIAN memberikan fasilitas kredit kepada NASABAH, sejumlah <?= $up;?>
            (<?= $upTerbilang;?> rupiah)  dan NASABAH menyatakan sepakat dan setuju serta menerimanya
            sebagai utang dari PEGADAIAN.
        </td>
    </tr>
    <tr>
        <td style="text-align: justify; vertical-align: top;">
            2.
        </td>
        <td style="text-align: justify; vertical-align: top;">
            NASABAH menyatakan telah berutang kepada PEGADAIAN dan berkewajiban untuk
            membayar Pelunasan Uang Pinjaman ditambah Sewa Modal dan/atau Denda sebagaimana
            ketentuan angka 8 Perjanjian ini serta biaya-biaya yang timbul atas penjualan/lelang barang
            jaminan.
        </td>
    </tr>
    <tr>
        <td style="text-align: justify; vertical-align: top;">
            3.
        </td>
        <td style="text-align: justify; vertical-align: top;">
            Pinjaman/kredit untuk jangka waktu kredit selama <?= $tenor; ?> (<?= $tenorTerbilang;?>) bulan terhitung mulai
            tanggal <?= $tglKredit;?> sampai dengan tanggal <?= $tglJatuhTempo;?> (yaitu tanggal jatuh tempo dari angsuran
            bulanan terakhir).
        </td>
    </tr>
    <tr>
        <td style="text-align: justify; vertical-align: top;">
            4.
        </td>
        <td style="text-align: justify; vertical-align: top;">
            Barang yang diserahkan sebagai jaminan dengan penetapan nilai taksiran sebesar
            <?= $taksiran; ?> (<?= $taksiranTerbilang; ?> rupiah) yakni berupa Titipan Emas sebagaimana yang tertera dalam rekening
            tabungan emas milik NASABAH adalah milik NASABAH dan/atau kepemilikan
            sebagaimana Pasal 1977 KUH Perdata dan menjamin bukan berasal dari hasil kejahatan,
            tidak dalam obyek sengketa dan/atau sita jaminan. Dan selanjutnya dilakukan pemblokiran
            sebesar nilai titipan emas sesuai ketentuan yang berlaku di PEGADAIAN atas pencairan
            kredit tersebut.
        </td>
    </tr>
    <tr>
        <td style="text-align: justify; vertical-align: top;">
            5.
        </td>
        <td style="text-align: justify; vertical-align: top;">
            NASABAH menerima dan setuju terhadap uraian barang jaminan dan penetapan
            besarnya Taksiran Barang Jaminan sebagaimana ketentuan angka 4 Perjanjian ini.
        </td>
    </tr>
    <tr>
        <td style="text-align: justify; vertical-align: top;">
            6.
        </td>
        <td style="text-align: justify; vertical-align: top;">
            Jika terjadi penurunan nilai Taksiran Barang Jaminan dalam masa Perjanjian yang
            mengakibatkan nilai taksiran baru lebih rendah dari sisa uang pinjaman, maka NASABAH
            wajib mengangsur pokok pinjaman untuk menyesuaikan dengan nilai taksiran yang baru atau
            menyetujui penambahan barang jaminan yang selanjutnya dilakukan pemblokiran atas
            penambahan barang jaminan berupa titipan emas.
        </td>
    </tr>
    <tr>
        <td style="text-align: justify; vertical-align: top;">
            7.
        </td>
        <td style="text-align: justify; vertical-align: top;">
            PEGADAIAN (Persero) akan memberikan penggantian kerugian apabila barang jaminan
            yang berada dalam penguasaan PEGADAIAN mengalami kerusakan atau hilang yang tidak
            disebabkan oleh suatu bencana alam (force majeure) yang ditetapkan Pemerintah.
            NASABAH menyatakan sepakat dan setuju dengan ketentuan penggantian kerugian yang
            berlaku di PEGADAIAN dan pengantian kerugian diberikan setelah diperhitungkan dengan
            besarnya sisa utang ditambah sewa modal dan/atau denda yang masih harus dibayar.
        </td>
    </tr>
    <tr>
        <td style="text-align: justify; vertical-align: top;">
            8.
        </td>
        <td style="text-align: justify; vertical-align: top;">
            NASABAH menyatakan sepakat dan setuju atas besaran Tarif SM, Tarif Denda dan Tarif
            Biaya Administrasi yang ditetapkan oleh PEGADAIAN, yaitu:

            <table>
                <tr>
                    <td style="text-align: justify; vertical-align: top;">
                        a.
                    </td>
                    <td style="text-align: justify; vertical-align: top;">
                        Tarif Sewa Modal sebesar <?= $smPercent; ?> (<?= $smPercentTerbilang;?> perseratus) per bulan flat atau <?= $smPercent; ?> (<?= $smPercentTerbilang;?> perseratus)
                        per bulan efektif dari uang pinjaman, dengan menggunakan metode perhitungan
                        pencatatan anuitas, yaitu jumlah angsuran setiap bulan sama, dengan porsi angsuran
                        pokok pinjaman dan sewa modal berbeda setiap bulan. Besaran Sewa Modal dihitung
                        berdasar sisa pokok pinjaman yang belum dibayar;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: justify; vertical-align: top;">
                        b.
                    </td>
                    <td style="text-align: justify; vertical-align: top;">
                        Tarif Denda sebesar <?= $dendaPercent; ?> (<?= $dendaPercentTerbilang; ?> perseratus) dibagi 30 (tiga puluh) setiap harinya, yang
                        dibayar apabila terjadi keterlambatan pembayaran angsuran bulanan;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: justify; vertical-align: top;">
                        c.
                    </td>
                    <td style="text-align: justify; vertical-align: top;">
                        Tarif Biaya Administrasi sebesar <?= $administrasi; ?> (<?= $administrasiTerbilang;?>) yang dibayar setelah
                        penandatanganan Perjanjian.
                    </td>
                </tr>
            </table>

        </td>
    </tr>
    <tr>
        <td style="text-align: justify; vertical-align: top;">
            9.
        </td>
        <td style="text-align: justify; vertical-align: top;">
            NASABAH berkewajiban membayar pokok uang pinjaman ditambah dengan sewa modal
            dengan cara angsuran yakni sebesar <?= $angsuran; ?> (<?= $angsuranTerbilang;?> rupiah) yang dibayarkan setiap bulan,
            selambat-lambatnya tanggal <?= $tglJT; ?> yang merupakan tanggal jatuh tempo setiap bulannya.
            Apabila terjadi keterlambatan pembayaran angsuran, NASABAH dikenakan denda sesuai
            ketentuan angka 8 Perjanjian ini.
        </td>
    </tr>
    <tr>
        <td style="text-align: justify; vertical-align: top;">
            10.
        </td>
        <td style="text-align: justify; vertical-align: top;">
            NASABAH dapat melakukan pelunasan utang sebelum jangka waktu pinjaman berakhir
            dengan melakukan pembayaran sekaligus sebesar seluruh angsuran yang menunggak
            berikut dendanya (jika ada) dan membayar sisa angsuran termasuk sewa modal sesuai
            dengan perhitungan berdasarkan ketentuan yang berlaku di PEGADAIAN.
        </td>
    </tr>
    <tr>
        <td style="text-align: justify; vertical-align: top;">
            11.
        </td>
        <td style="text-align: justify; vertical-align: top;">
            NASABAH dapat melakukan pembayaran kewajiban dengan cara pelunasan atau angsuran
            di seluruh Outlet PEGADAIAN dan/atau channel pembayaran yang bekerjasama dengan
            PEGADAIAN
        </td>
    </tr>
    <tr>
        <td style="text-align: justify; vertical-align: top;">
            12.
        </td>
        <td style="text-align: justify; vertical-align: top;">
            PEGADAIAN melakukan pembukaan blokir saldo titipan emas yang diserahkan sebagai
            jaminan kredit Setelah NASABAH melakukan pelunasan kredit sebagai bentuk penyerahan
            Jaminan kepada NASABAH.
        </td>
    </tr>
    <tr>
        <td style="text-align: justify; vertical-align: top;">
            13.
        </td>
        <td style="text-align: justify; vertical-align: top;">
            NASABAH dinyatakan cidera janji (wanprestasi) apabila memenuhi salah satu dan/atau
            seluruh kriteria sebagai berikut :

            <table>
                <tr>
                    <td style="text-align: justify; vertical-align: top;">
                        a.
                    </td>
                    <td style="text-align: justify; vertical-align: top;">
                        NASABAH tidak melakukan pembayaran angsuran bulanan selama 2 (dua) bulan
                        berturut-turut, atau;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: justify; vertical-align: top;">
                        b.
                    </td>
                    <td style="text-align: justify; vertical-align: top;">
                        NASABAH masih memiliki tunggakan kewajiban setelah jangka waktu pinjaman berakhir;
                    </td>
                </tr>
            </table>

        </td>
    </tr>
    <tr>
        <td style="text-align: justify; vertical-align: top;">
            14.
        </td>
        <td style="text-align: justify; vertical-align: top;">
            PEGADAIAN berkewajiban memberikan Surat Peringatan kepada NASABAH apabila
            belum memenuhi kewajibannya sesuai Perjanjian ini dan apabila Nasabah tidak memenuhi
            surat peringatan tersebut secara serta merta dinyatakan cidera janji (wanprestasi).
        </td>
    </tr>
    <tr>
        <td style="text-align: justify; vertical-align: top;">
            15.
        </td>
        <td style="text-align: justify; vertical-align: top;">
            PEGADAIAN berhak melakukan penjualan barang jaminan melalui lelang apabila:

            <table>
                <tr>
                    <td style="text-align: justify; vertical-align: top;">
                        a.
                    </td>
                    <td style="text-align: justify; vertical-align: top;">
                        NASABAH sudah diberikan Surat Peringatan dan tetap tidak menyelesaikan
                        kewajibannya;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: justify; vertical-align: top;">
                        b.
                    </td>
                    <td style="text-align: justify; vertical-align: top;">
                        NASABAH dinyatakan cidera janji (wanprestasi) sebagaimana dimaksud angka 12
                        Perjanjian ini.
                    </td>
                </tr>
            </table>

        </td>
    </tr>
    <tr>
        <td style="text-align: justify; vertical-align: top;">
            16.
        </td>
        <td style="text-align: justify; vertical-align: top;">
            Waktu Pelaksanaan Lelang:

            <table>
                <tr>
                    <td style="text-align: justify; vertical-align: top;">
                        a.
                    </td>
                    <td style="text-align: justify; vertical-align: top;">
                        Jika NASABAH tidak melakukan pembayaran angsuran bulanan selama 2 (dua) bulan
                        berturut turut, maka pelaksanaan lelang dilakukan paling cepat pada bulan ke-3
                        setelah tanggal jatuh tempo angsuran;
                    </td>
                </tr>
                <tr>
                    <td style="text-align: justify; vertical-align: top;">
                        b.
                    </td>
                    <td style="text-align: justify; vertical-align: top;">
                        Jika NASABAH belum melakukan pembayaran pelunasan dan masih memiliki tunggakan
                        kewajiban setelah jangka waktu pinjaman berakhir, maka pelaksanaan lelang dilakukan
                        paling cepat pada bulan ke-2 setelah jangka waktu pinjaman berakhir.
                    </td>
                </tr>
            </table>

        </td>
    </tr>
    <tr>
        <td style="text-align: justify;vertical-align: top;">
            17.
        </td>
        <td style="text-align: justify; vertical-align: top;">
            Hasil penjualan lelang barang jaminan setelah dikurangi Uang Pinjaman, Sewa Modal,
            Denda (jika &nbsp; ada) dan Bea Lelang sesuai peraturan perundang-undangan yang berlaku,
            merupakan kelebihan yang menjadi hak nasabah. Jangka waktu pengambilan uang
            kelebihan lelang selama satu tahun sejak &nbsp; tanggal lelang, dan Jika lewat waktu dari jangka
            waktu pengambilan uang kelebihan, NASABAH &nbsp; menyatakan setuju untuk menyalurkan uang
            kelebihan tersebut sebagai dana kepedulian sosial yang &nbsp; pelaksanaannya diserahkan kepada
            PEGADAIAN. Jika hasil penjualan lelang barang jaminan tidak &nbsp; mencukupi untuk melunasi
            kewajiban nasabah berupa Uang Pinjaman, Sewa Modal, Denda (jika ada) &nbsp; dan Bea Lelang
            maka NASABAH wajib membayar kekurangan tersebut.
        </td>
    </tr>
    <tr>
        <td style="text-align: justify; vertical-align: top;">
            18.
        </td>
        <td style="text-align: justify; vertical-align: top;">
            NASABAH menyatakan setuju bahwa pengembalian uang kelebihan lelang (apabila ada) maka dilakukan dengan cara :

            <table>
                <tr>
                    <td style="text-align: justify; vertical-align: top;">
                        a.
                    </td>
                    <td style="text-align: justify; vertical-align: top;">
                        Diambil secara tunai.
                    </td>
                </tr>
                <tr>
                    <td style="text-align: justify; vertical-align: top;">
                        b.
                    </td>
                    <td style="text-align: justify; vertical-align: top;">
                        Top Up Tabungan Emas.
                    </td>
                </tr>
            </table>

            Dengan persyaratan menunjukan KTP asli, Perjanjian asli, dan Buku Rekening Tabungan
            Emas apabila Barang Jaminan berupa titipan emas.
        </td>
    </tr>
    <tr>
        <td style="text-align: justify; vertical-align: top;">
            19.
        </td>
        <td style="text-align: justify; vertical-align: top;">
            Bilamana NASABAH meninggal dunia dan terdapat hak dan kewajiban terhadap
            PEGADAIAN ataupun sebaliknya, maka hak dan kewajiban dibebankan kepada ahli waris
            NASABAH sesuai dengan ketentuan waris dalam hukum Republik Indonesia.
        </td>
    </tr>
    <tr>
        <td style="text-align: justify; vertical-align: top;">
            20.
        </td>
        <td style="text-align: justify; vertical-align: top;">
            NASABAH menyatakan tunduk dan mengikuti segala peraturan yang berlaku di
            PEGADAIAN sepanjang ketentuan yang menyangkut Utang Piutang Dengan Jaminan Gadai
        </td>
    </tr>
    <tr>
        <td style="text-align: justify; vertical-align: top;">
            21.
        </td>
        <td style="text-align: justify; vertical-align: top;">
            Apabila terjadi perselisihan dikemudian hari akan diselesaikan secara musyawarah untuk
            mufakat dan apabila tidak tercapai kesepakatan akan diselesaikan melalu Lembaga Alternatif
            Penyelesaian Sengketa di Bidang Usaha Pergadaian sesuai Undang-undang yang berlaku.
        </td>
    </tr>
</table>

<br><br>
<b>PERJANJIAN INI TELAH DISESUAIKAN DENGAN KETENTUAN PERATURAN
    PERUNDANG-UNDANGAN TERMASUK KETENTUAN PERATURAN OTORITAS JASA
    KEUANGAN.</b>

<br><br>
Demikian Perjanjian ini berlaku dan mengikat antara PEGADAIAN dengan NASABAH, sejak
Perjanjian ini ditandatangani oleh PARA PIHAK sampai dengan penyelesaian kewajiban
kredit/pinjaman.

<br><br>
<center><?= $hari;?>, tanggal <?= date('d-m-Y'); ?></center>

<br>

<table style=" margin-left: auto;margin-right: auto;">
    <tr>
        <td style="text-align: left; vertical-align: top;width: 300px;">
            <center>PEGADAIAN</center>,
            <br>
            <br>
            <br><center><img src=<?= $fullpathPinca;?>></center>
            <br><center><?= $namaPinca; ?><center>
            <br><center>Pemimpin Unit Kerja</center>
        </td>
        <td style="text-align: justify; vertical-align: top;width: 60px;">
        </td>
        <td style="text-align: right; vertical-align: top;width: 300px;">
            <center>NASABAH</center>,
            <br>
            <br>
            <br><center><img src=<?= $fullpathCustomer;?>></center>
            <br><center><?= $user->nama; ?></center>
            <br>
        </td>
    </tr>
</table>