<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Socket.IO Chat Example</title>
</head>
<body>
    <script src="<?php echo base_url()?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/socket.io-client/socket.io.js"></script>
  
  <script type="text/javascript">
    $(function(){
      var socket = io('http://'+document.domain);

      setTimeout(function(){
        socket.emit('room', 'pds-web');
      }, 1000);

      setTimeout(function(){
        socket.emit('sent_to_room', 'hello?');
      }, 3000);

      socket.on('message', function(data){
        console.log(data);
      })

    })
  </script>
</body>
</html>
