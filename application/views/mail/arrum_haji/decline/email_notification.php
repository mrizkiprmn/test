<?= $this->load->view('mail/email_template_top', ['title' => $data->title ?? '-'], true) ?>

Nasabah yang terhormat,
<br>
<br>
Mohon maaf pengajuan Arrum Haji melalui aplikasi Pegadaian Digital ditolak. Silakan ajukan kembali pembiayaan Arrum Haji
dengan melakukan pengecekan data diri dan dokumen yang diisiikan pada formulir.
<br>
<br>
Email ini dihasilkan otomatis oleh sistem dan dimohon untuk tidak membalas. Jika kamu ingin mendapat informasi lanjut,
silakan hubungi Call Center di 1500 569.
<br>
<br>
Terimakasih sudah menggunakan aplikasi Pegadaian Digital
<br>
<br>
<br>
Salam hangat.
<br>
PT Pegadaian (Persero)

<?= $this->load->view('mail/email_template_bottom', [], true) ?>

