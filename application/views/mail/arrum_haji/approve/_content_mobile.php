Nasabah yang terhormat,
<br>
<br>
Selamat! Pengajuan Arrum Haji melalui aplikasi Pegadaian Digital dengan nomor pengajuan <?= $data->arrum_haji->booking_id ?? '-' ?> sudah diterima.
Kamu akan dihubungi oleh petugas dari Pegadaian untuk proses selanjutnya. Silakan siapkan dokumen-dokumen berikut untuk
dibawah ke Outlet Pegadaian yang dipilih:
<br>
<br>
<table>
    <tr>
        <td>Name</td>
        <td>:</td>
        <td><strong> <?= $data->user->nama ?? '' ?></strong></td>
    </tr>
    <tr>
        <td>CIF</td>
        <td>:</td>
        <td><strong><?= $data->user->cif ?? ''; ?></strong></td>
    </tr>
    <tr>
        <td>Alamat</td>
        <td>:</td>
        <td><strong><?= $data->user->alamat ?? '' ?></strong></td>
    </tr>
    <tr>
        <td>Jaminan</td>
        <td>:</td>
        <td>
            <strong>
                <?php
                if (!empty($data) && !empty($data->arrum_haji->agunan)) {
                    foreach ($data->arrum_haji->agunan as $agunan) {
                        $collaterals[] = $agunan->label;
                    }
                }
                echo implode(', ', $collaterals ?? [])
                ?>
            </strong>
        </td>
    </tr>
    <tr>
        <td>Tanggal Pengajuan</td>
        <td>:</td>
        <td><strong><?= $data->arrum_haji->applied_at ?? '' ?></strong></td>
    </tr>
</table>
<br>
<br>
Email ini dihasilkan otomatis oleh sistem dan dimohon untuk tidak membalas. Jika kamu ingin mendapat informasi lanjut, silakan hubungi Call Center di 1500 569.
<br/> <br/>
Terimakasih sudah menggunakan aplikasi Pegadaian Digital
<br/> <br/>
Salam hangat,
PT Pegadaian (Persero)"
