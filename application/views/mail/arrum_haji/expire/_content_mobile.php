Nasabah yang terhormat,
<br>
<br>
Mohon maaf pengajuan Arrum Haji melalui aplikasi Pegadaian Digital dengan nomor pengajuan
<?= $data->arrum_haji->booking_id ?? '-' ?> sudah tidak berlaku lagi karena melewati batas 30 hari kerja. Silakan ulangi pengajuan pembiayaan Arrum Haji.
<br>
<br>
Email ini dihasilkan otomatis oleh sistem dan dimohon untuk tidak membalas. Jika kamu ingin mendapat informasi lanjut, silakan hubungi Call Center di 1500 569.
<br>
<br>
Terimakasih sudah menggunakan aplikasi Pegadaian Digital
<br>
<br>
Salam hangat,
PT Pegadaian (Persero)"
