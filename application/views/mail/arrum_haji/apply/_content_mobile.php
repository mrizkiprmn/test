Hi, <?php echo $data->arrum_haji->user_name ?> <br></br>

Kamu telah menjadwalkan kedatangan untuk
melakukan pendaftaran Pembiayaan Ibadah Haji
dengan atas nama. <br></br>

Peserta : <?php echo $data->arrum_haji->user_name ?>
<br></br>

Pastikan nasabah yang melakukan pendaftaran
Pembiayaan Ibadah Haji wajib hadir dan membawa
barang jaminan dan biaya-biaya pada :

<br></br>
<table>
    <tr>
        <td>Outlet Pengajuan</td>
        <td>&nbsp;&nbsp;&nbsp; : <?php echo $data->arrum_haji->outlet_pengajuan ?></td>
    </tr>
    <tr>
        <td>Biaya Administrasi</td>
        <td>&nbsp;&nbsp;&nbsp; : <?php echo Pegadaian::currencyIdr($data->arrum_haji->mu_nah_akad) ?></td>
    </tr>
    <tr>
        <td>Biaya Imbal Jasa Kafalah</td>
        <td>&nbsp;&nbsp;&nbsp; : <?php echo Pegadaian::currencyIdr($data->arrum_haji->ijk) ?></td>
    </tr>
    <tr>
        <td>Biaya Buka Rekening</td>
        <td>&nbsp;&nbsp;&nbsp; : <?php echo Pegadaian::currencyIdr($data->arrum_haji->biaya_buka_rekening) ?></td>
    </tr>
    <tr>
        <td>Total Biaya Pengajuan</td>
        <td>&nbsp;&nbsp;&nbsp; : <?php echo Pegadaian::currencyIdr($data->arrum_haji->ijk + $data->arrum_haji->biaya_buka_rekening + $data->arrum_haji->mu_nah_akad) ?></td>
    </tr>
</table>

<br></br>

Dokumen yang wajib dibawa:

<br></br>

<table>
    <tr>
        <td>Copy KTP yang masih berlaku</td>
        <td>(6 lembar)</td>
    </tr>
    <tr>
        <td>Copy Kartu Keluarga</td>
        <td>(6 lembar)</td>
    </tr>
    <tr>
        <td>Copy Akta Nikah/Akta Kelahiran/Ijazah</td>
        <td>(6 lembar)</td>
    </tr>
    <tr>
        <td>Surat Keterangan Sehat dari Puskesmas</td>
        <td>(6 lembar)</td>
    </tr>
    <tr>
        <td>Pas Foto Berwarna Latar Putih, 80% Wajah</td>
        <td></td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ukuran 3 x 4</td>
        <td>(25 lembar)</td>
    </tr>
    <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ukuran 4 x 6</td>
        <td>(10 Lembar)</td>
    </tr>
    <tr>
        <td>Materai 6000</td>
        <td>(4 lembar)</td>
    </tr>
</table>

<br><br>
Untuk info lebih lanjut, hubungi kantor pegadaian
terdekat.
<br><br>

Terimakasih

<br>
<br>