Hi, <?php echo $nama ?> <br></br>

Terima kasih telah melakukan pengajuan gadai. <br></br>

<table class="table table-responsive">
    <tr>
        <td>No Pengajuan</td>
        <td>:</td>
        <td> <strong><?php echo $noPengajuan?></strong></td>
    </tr>
    <tr>
        <td>Tanggal Pengajuan</td>
        <td>:</td>
        <td> <strong><?php echo $detailGadai->tglBooking ?></strong></td>
    </tr>
    <tr>
        <td>Nama</td>
        <td>:</td>
        <td> <strong><?php echo $nama ?></strong></td>
    </tr>  
    <tr>
        <td>Jenis/No ID</td>
        <td>:</td>
        <td> <strong><?php echo $detailGadai->tipeIdentitas=='10' ? 'KTP':'Paspor'; echo '/'.$detailGadai->noIdentitas ?></strong></td>
    </tr>
    <tr>
        <td>Alamat</td>
        <td>:</td>
        <td> <strong><?php echo $detailGadai->jalan ?></strong></td>
    </tr>
    
    <tr>
        <td>No HP</td>
        <td>:</td>
        <td> <strong><?php echo $detailGadai->noHp ?></strong></td>
    </tr>
    <tr>
        <td>Detail Barang Jaminan</td>
        <td>:</td>
        <td> <strong><?php echo $detailGadai->jaminan ?></strong></td>
    </tr>    
    <tr>
        <td>Perkiraan Uang Pinjaman</td>
        <td>:</td>
        <td> <strong><?php echo 'Rp. '. number_format($detailGadai->taksiranBawah, 0, ",", ".") .' s/d Rp.'.number_format($detailGadai->taksiranAtas, 0, ",", ".") ?></strong></td>
    </tr>
    
</table>
<br>
Selanjutnya silahkan datang ke outlet kami untuk verifikasi pengajuan Anda:
<table class="table table-responsive">
    <tr>
        <td>Nama Outlet</td>
        <td>:</td>
        <td> <strong><?php echo $namaOutlet ?></strong></td>
    </tr>
    <tr>
        <td>Alamat</td>
        <td>:</td>
        <td> <strong><?php echo $alamatOutlet ?></strong></td>
    </tr>
    <tr>
        <td>Telp</td>
        <td>:</td>
        <td> <strong><?php echo '<a href="tel:'.$teleponOutlet.'"><span class="fa fa-phone"></span> '.$teleponOutlet.'</a>'; ?></strong></td>
    </tr>
    <tr>
        <td>Jadwal Kedatangan</td>
        <td>:</td>
        <td> <strong><?php echo $tanggalDatang.' '.$waktuDatang ?></strong></td>
    </tr>
</table>

<br>

Terima Kasih       
