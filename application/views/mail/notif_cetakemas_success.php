Hi, <?php echo $namaNasabah ?> 
<br></br><br>

<?php if ($isTerima == "1") :?>
Selamat Cetakan Emas Anda Sudah Tersedia.

<br /><br />
<table border="0">                                                       
    <tr>
        <td>Metode Pembayaran</td>
        <td style="width: 10px">:</td>
        <td><strong><?php if ($payment=='MANDIRI') {
            echo 'MANDIRI CLICK PAY';
                    } elseif ($payment=='BNI') {
                        echo 'TRANSFER VIRTUAL ACCOUNT BNI';
                    } ?></strong></td>
    </tr>
    <tr>
        <td>Tanggal</td>
        <td>:</td>
        <td><strong><?php $tglTrx = new DateTime($tglTransaksi);
        echo $tglTrx->format('d/M/Y H:i:s'); ?></strong></td>
    </tr>
    <tr>
        <td>Referensi</th>
        <td>:</td>
        <td><strong><?php echo $id_transaksi ?></strong></td>
    </tr>    
    <tr>
        <td>No Rekening</td>
        <td>:</td>
        <td><strong><?php echo $norek ?></strong></td>
    </tr>
    <tr>
        <td>Nama Nasabah</td>
        <td>:</td>
        <td><strong><?php echo $namaNasabah ?></strong></td>
    </tr>
   <tr>
        <td>Biaya Channel</td>
        <td>:</td>
        <td><strong>Rp. <?php echo number_format($biayaTransaksi, 0, ",", ".") ?></strong></td>
    </tr>
    <tr>
        <td>Biaya Cetak</td>
        <td>:</td>
        <td><strong>Rp. <?php
                    $arr = json_decode($order, true);
                    echo number_format(array_sum(array_column($arr, 'totalBiaya')), 0, ",", ".");
                    
        ?></strong></td>
    </tr>
    <tr>
        <td>Total Tagihan</td>
        <td>:</td>
        <td><strong>Rp. <?php echo number_format($totalKewajiban+$biayaTransaksi, 0, ",", ".") ?></strong></td>
    </tr>
</table>

<br><br>

Detail Cetak Emas: <br><br>
<table class="table table-responsive table-bordered">
    <tr>
        <th>No</th>
        <th>Berat</th>
        <th>Biaya Cetak</th>
        <th>Keping</th>
        <th>Total Biaya</th>
    </tr>
    <?php
        $arrOrder = json_decode($order);
        $x = 1;
    foreach ($arrOrder as $o) {
        echo '<tr>';
        echo '<td style="align:center">'.$x.'</td>';
        echo '<td style="align:center">'.$o->berat.'</td>' ;
        echo '<td style="align:right"> Rp. '.number_format($o->biaya, 0, ",", ".").'</td>' ;
        echo '<td style="align:center">'.$o->keping.'</td>' ;
        echo '<td style="align:right"> Rp. '.number_format($o->totalBiaya, 0, ",", ".").'</td>' ;
        echo '</tr>';
        $x++;
    }
    ?>
</table>

<br /><br />
Silahkan untuk mengambil hasil cetakan ke outlet Pegadaian berikut:
<br /><br />
<table border="0">                                                       
    <tr>
        <td>Cabang</td>
        <td style="width: 10px">:</td>
        <td>: <strong><?php echo $namaCabang; ?></strong></td>
    </tr>
    <tr>
        <td>Alamat Cabang</td>
        <td>:</td>
        <td><strong><?php echo $alamatCabang; ?></strong></td>
    </tr>
    <tr>
        <td>No Telp</td>
        <td>:</td>
        <td><strong><?php echo '<a href="tel:'.$noTelpCabang.'"><span class="fa fa-phone"><span> '.$noTelpCabang.'</a>'; ?></strong></td>
    </tr>
</table>
<br>
Terima kasih


<?php endif; ?>


<?php if ($isTerima == "0") :?>
Selamat Order Cetak Tabungan Emas Berhasil.

<br /><br />
<table border="0">                                                       
    <tr>
        <td>Metode Pembayaran</td>
        <td style="width: 10px">:</td>
        <td><strong><?php if ($payment=='MANDIRI') {
            echo 'MANDIRI CLICK PAY';
                    } elseif ($payment=='BNI') {
                        echo 'TRANSFER VIRTUAL ACCOUNT BNI';
                    } ?></strong></td>
    </tr>
    <tr>
        <td>Tanggal</td>
        <td>:</td>
        <td><strong><?php $tglTrx = new DateTime($tglTransaksi);
        echo $tglTrx->format('d/M/Y H:i:s'); ?></strong></td>
    </tr>
    <tr>
        <td>Referensi</th>
        <td>:</td>
        <td><strong><?php echo $id_transaksi ?></strong></td>
    </tr>    
    <tr>
        <td>No Rekening</td>
        <td>:</td>
        <td><strong><?php echo $norek ?></strong></td>
    </tr>
    <tr>
        <td>Nama Nasabah</td>
        <td>:</td>
        <td>: <strong><?php echo $namaNasabah ?></strong></td>
    </tr>
   <tr>
        <td>Biaya Channel</td>
        <td>:</td>
        <td><strong>Rp. <?php echo number_format($biayaTransaksi, 0, ",", ".") ?></strong></td>
    </tr>
    <tr>
        <td>Biaya Cetak</td>
        <td>:</td>
        <td><strong>Rp. <?php
                    $arr = json_decode($order, true);
                    echo number_format(array_sum(array_column($arr, 'totalBiaya')), 0, ",", ".");
                    
        ?></strong></td>
    </tr>
    <tr>
        <td>Total Tagihan</td>
        <td>:</td>
        <td><strong>Rp. <?php echo number_format($totalKewajiban+$biayaTransaksi, 0, ",", ".") ?></strong></td>
    </tr>
</table>

<br><br>

Detail Cetak Emas: <br><br>
<table class="table table-responsive table-bordered">
    <tr>
        <th>No</th>
        <th>Berat</th>
        <th>Biaya Cetak</th>
        <th>Keping</th>
        <th>Total Biaya</th>
    </tr>
    <?php
        $arrOrder = json_decode($order);
        $x = 1;
    foreach ($arrOrder as $o) {
        echo '<tr>';
        echo '<td style="align:center">'.$x.'</td>';
        echo '<td style="align:center">'.$o->berat.'</td>' ;
        echo '<td style="align:right"> Rp. '.number_format($o->biaya, 0, ",", ".").'</td>' ;
        echo '<td style="align:center">'.$o->keping.'</td>' ;
        echo '<td style="align:right"> Rp. '.number_format($o->totalBiaya, 0, ",", ".").'</td>' ;
        echo '</tr>';
        $x++;
    }
    ?>
</table>

<br /><br />
Cetakan emas sedang diorder. Selanjutnya kami akan memberikan informasi ketika Emas Cetakan sudah selesai.
<br /><br />
Outlet pengambilan cetakan emas<br />
<table border="0">                                                       
    <tr>
        <td>Cabang</td>
        <td style="width: 10px">:</td>
        <td><strong><?php echo $namaCabang; ?></strong></td>
    </tr>
    <tr>
        <td>Alamat Cabang</td>
        <td>:</td>
        <td><strong><?php echo $alamatCabang; ?></strong></td>
    </tr>
    <tr>
        <td>No Telp</td>
        <td>:</td>
        <td><strong><?php echo $noTelpCabang; ?></strong></td>
    </tr>
</table>
<br>
Terima kasih


<?php endif; ?>

