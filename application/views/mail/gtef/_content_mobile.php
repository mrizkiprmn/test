Hi, <?= $data->customer_name ?? '' ?>
<br>
<br>
Selamat, Pengajuan Gadai Titipan Emas Fisik Kamu Telah Berhasil dilakukan 
<br>
<br>
Berikut merupakan rincian kredit kamu
<br>
<br>
<table>
    <tr>
        <td>Nomor Referensi</td>
        <td>:</td>
        <td><strong> <?= $data->nomor_referensi ?? '' ?></strong></td>
    </tr>
    <tr>
        <td>Nama</td>
        <td>:</td>
        <td><strong> <?= $data->customer_name ?? '' ?></strong></td>
    </tr>
    <tr>
        <td>Nomor Kredit</td>
        <td>:</td>
        <td><strong><?= $data->credit_number ?? ''; ?></strong></td>
    </tr>
    <tr>
        <td>Tanggal Sewa Modal</td>
        <td>:</td>
        <td><strong><?= $data->tanggal_sewa_modal ?? '' ?></strong></td>
    </tr>
    <tr>
        <td>Sewa Modal</td>
        <td>:</td>
        <td><strong><?= $data->sewa_modal ?? '' ?></strong></td>
    </tr>
    <tr>
        <td>Uang Pinjaman</td>
        <td>:</td>
        <td><strong><?= $data->uang_pinjaman ?? '' ?></strong></td>
    </tr>

    <?php if ($data->tenor != null) : ?>
    <tr>
        <td>Jangka Waktu</td>
        <td>:</td>
        <td><strong><?= $data->tenor ?? '' ?></strong></td>
    </tr>
    <?php endif; ?>

    <?php if ($data->admin_surcharge != null) : ?>
    <tr>
        <td>Biaya Administrasi</td>
        <td>:</td>
        <td><strong><?= $data->admin_surcharge ?? '' ?></strong></td>
    </tr>
    <?php endif; ?>

    <tr>
        <td>Biaya Transfer</td>
        <td>:</td>
        <td><strong><?= $data->transfer_surcharge ?? '' ?></strong></td>
    </tr>

    <?php if ($data->tagihan_perpanjang_masa_titipan != null) : ?>
        <tr>
            <td>Tagihan Perpanjang Masa Titipan</td>
            <td>:</td>
            <td><strong><?= $data->tagihan_perpanjang_masa_titipan ?? '' ?></strong></td>
        </tr>
    <?php endif; ?>

    <?php if ($data->tagihan_sewa_modal != null) : ?>
        <tr>
            <td>Tagihan Sewa Modal</td>
            <td>:</td>
            <td><strong><?= $data->tagihan_sewa_modal ?? '' ?></strong></td>
        </tr>
    <?php endif; ?>

    <?php if ($data->denda_keterlambatan_sewa_modal != null) : ?>
        <tr>
            <td>Denda Keterlambatan Sewa Modal</td>
            <td>:</td>
            <td><strong><?= $data->denda_keterlambatan_sewa_modal ?? '' ?></strong></td>
        </tr>
    <?php endif; ?>

    <tr>
        <td>Jumlah Diterima</td>
        <td>:</td>
        <td><strong><?= $data->jumlah_diterima ?? '' ?></strong></td>
    </tr>
    <tr>
        <td>Barang Jaminan</td>
        <td>:</td>
        <td><strong><?= $data->barang_jaminan ?? '' ?></strong></td>
    </tr>
    <tr>
        <td>Nomor Titipan</td>
        <td>:</td>
        <td><strong><?= $data->nomor_titipan ?? '' ?></strong></td>
    </tr>
    <tr>
        <td>Tanggal Jatuh Tempo</td>
        <td>:</td>
        <td><strong><?= $data->tanggal_jatuh_tempo ?? '' ?></strong></td>
    </tr>
    <tr>
        <td>Tanggal Lelang</td>
        <td>:</td>
        <td><strong><?= $data->tanggal_lelang ?? '' ?></strong></td>
    </tr>
    <tr>
        <td>Rekening Tujuan</td>
        <td>:</td>
        <td><strong><?= $data->bank_account_number ?? '' ?></strong></td>
    </tr>
    <tr>
        <td>Atas Nama</td>
        <td>:</td>
        <td><strong><?= $data->bank_customer_name ?? '' ?></strong></td>
    </tr>
</table>

<br>
<br>
Pinjaman telah dikirim ke rekening tujuan.

<br/> <br/>
Terima kasih.
<br/> <br/>