
Hi, <?php echo $nama_Nasabah ?>
<br /> <br />

<td><?php echo "Terima Kasih Telah Melakukan Pembayaran"?></td>

<br /> <br />

<td><?php echo "Transaksi Kamu Berhasil"?></td>

<br /> <br />

<table>
    <tr>
        <td>Jenis Transaksi</td>
        <td>:</td>
        <td> <strong><?php echo $namaProduk ?></strong></td>
    </tr>
    <tr>
        <td>Nomor Kredit</td>
        <td>:</td>
        <td> <strong><?php echo $no_kredit ?></strong></td>
    </tr> 
    <tr>
        <td>Referensi</td>
        <td>:</td>
        <td> <strong><?php echo $referensi ?></strong></td>
    </tr>  
    <tr>
        <td>Nama Nasabah</td>
        <td>:</td>
        <td> <strong><?php echo $nama_Nasabah ?></strong></td>
    </tr>
    <tr>
        <td>Tanggal Jatuh Tempo</td>
        <td>:</td>
        <td> <strong><?php echo $tanggalJatuhTempo ?></strong></td>
    </tr>
    <tr>
        <td>Tanggal Lelang</td>
        <td>:</td>
        <td> <strong><?php echo $tanggalLelang ?></strong></td>
    </tr> 
    <tr>
        <td>Batas Waktu Pembayaran</td>
        <td>:</td>
        <td> <strong><?php echo $batasWaktu ?></strong></td>
    </tr>  
    <tr>
        <td>Uang Pinjaman Cicil</td>
        <td>:</td>
        <td><strong>Rp. <?php echo number_format($uangPinjamanCicil, 0, ",", ".") ?></strong></td>
    </tr>
    <tr>
        <td>Sewa Modal</td>
        <td>:</td>
        <td><strong>Rp. <?php echo number_format($sewaModal, 0, ",", ".") ?></strong></td>
    </tr>
    <tr>
        <td>Biaya Administrasi</td>
        <td>:</td>
        <td><strong>Rp. <?php echo number_format($biayaChannel, 0, ",", ".") ?></strong></td>
    </tr> 
    <tr>
        <td>Biaya Asuransi</td>
        <td>:</td>
        <td><strong>Rp. <?php echo number_format($biayaAsuransi, 0, ",", ".") ?></strong></td>
    </tr>  
    <tr>
        <td>Biaya Transaksi</td>
        <td>:</td>
        <td><strong>Rp. <?php echo number_format($biayaTransaksi, 0, ",", ".") ?></strong></td>
    </tr>
    <tr>
        <td>Total Pembayaran</td>
        <td>:</td>
        <td><strong>Rp. <?php echo number_format($nominalPembayaran, 0, ",", ".") ?></strong></td>
    </tr>  
    <tr>
        <td>Metode Pembayaran</td>
        <td>:</td>
        <td> <strong><?php echo $MetodePembayaran ?></strong></td>
    </tr>
</table>
<br /> <br />
<?php echo "Terima Kasih" ?>
<br /> <br />
