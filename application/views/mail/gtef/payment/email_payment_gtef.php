<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>

    <meta charset="utf-8" http-equiv="Content-Type" content="text/html" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
    <meta name="format-detection" content="telephone=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
    <title>Pegadaian Digital Service</title>
    <style type="text/css">
        
        /* ==> Importing Fonts <== */
        @import url(https://fonts.googleapis.com/css?family=Fredoka+One);
        @import url(https://fonts.googleapis.com/css?family=Quicksand);
        @import url(https://fonts.googleapis.com/css?family=Open+Sans);

        /* ==> Global CSS <== */
        .ReadMsgBody{width:100%;background-color:#ffffff;}
        .ExternalClass{width:100%;background-color:#ffffff;}
        .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{line-height:100%;}
        html{width: 100%;}
        body{-webkit-text-size-adjust:none;-ms-text-size-adjust:none;margin:0;padding:0;}
        table{border-spacing:0;border-collapse:collapse;}
        table td{border-collapse:collapse;}
        img{display:block !important;}
        a{text-decoration:none;color:#00A74E;}
        

        /* ==> Responsive CSS For Tablets <== */
        @media only screen and (max-width:640px) {
            body{width:auto !important;}
            table[class="tab-1"] {width:450px !important;}
            table[class="tab-2"] {width:47% !important;text-align:left !important;}
            table[class="tab-3"] {width:100% !important;text-align:center !important;}
            img[class="img-1"] {width:100% !important;height:auto !important;}
        }

        /* ==> Responsive CSS For Phones <== */
        @media only screen and (max-width:480px) {
            body { width: auto !important; }
            table[class="tab-1"] {width:290px !important;}
            table[class="tab-2"] {width:100% !important;text-align:left !important;}
            table[class="tab-3"] {width:100% !important;text-align:center !important;}
            img[class="img-1"] {width:100% !important;}
        }

    </style>
</head>
<body>
<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">

    <!-- =======> START ======== -->
    <!--    CUSTOMAR THINKING    -->
    <!-- ======================= -->
    <tr>
        <td align="center">
            <table class="tab-1" align="center" cellspacing="0" cellpadding="0" width="600">
                <tr>
                    <td  style="font-family: 'open sans', sans-serif; font-size: 15px; color: #7c7c7c;">
                        Hi, <?php echo $nama_Nasabah ?>
                        <br /> <br /> 
                    </td>
                </tr>

                <tr>
                    <td>
                        <?php echo "Terima Kasih Telah Melakukan Pembayaran"?>
                        <br /> <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo "Transaksi Kamu Berhasil"?>
                        <br /> <br />
                    </td>
                </tr>

                <tr>
                    <td>

                        <table>
                            <tr>
                                <td>Jenis Transaksi</td>
                                <td>:</td>
                                <td> <strong><?php echo $namaProduk ?></strong></td>
                            </tr>
                            <tr>
                                <td>Nomor Kredit</td>
                                <td>:</td>
                                <td> <strong><?php echo $no_kredit ?></strong></td>
                            </tr> 
                            <tr>
                                <td>Referensi</td>
                                <td>:</td>
                                <td> <strong><?php echo $referensi ?></strong></td>
                            </tr>  
                            <tr>
                                <td>Nama Nasabah</td>
                                <td>:</td>
                                <td> <strong><?php echo $nama_Nasabah ?></strong></td>
                            </tr>
                            <tr>
                                <td>Tanggal Jatuh Tempo</td>
                                <td>:</td>
                                <td> <strong><?php echo $tanggalJatuhTempo ?></strong></td>
                            </tr>
                            <tr>
                                <td>Tanggal Lelang</td>
                                <td>:</td>
                                <td> <strong><?php echo $tanggalLelang ?></strong></td>
                            </tr> 
                            <tr>
                                <td>Batas Waktu Pembayaran</td>
                                <td>:</td>
                                <td> <strong><?php echo $batasWaktu ?></strong></td>
                            </tr>  
                            <tr>
                                <td>Uang Pinjaman Cicil</td>
                                <td>:</td>
                                <td><strong>Rp. <?php echo number_format($uangPinjamanCicil, 0, ",", ".") ?></strong></td>
                            </tr>
                            <tr>
                                <td>Sewa Modal</td>
                                <td>:</td>
                                <td><strong>Rp. <?php echo number_format($sewaModal, 0, ",", ".") ?></strong></td>
                            </tr>
                            <tr>
                                <td>Biaya Administrasi</td>
                                <td>:</td>
                                <td><strong>Rp. <?php echo number_format($biayaChannel, 0, ",", ".") ?></strong></td>
                            </tr> 
                            <tr>
                                <td>Biaya Asuransi</td>
                                <td>:</td>
                                <td><strong>Rp. <?php echo number_format($biayaAsuransi, 0, ",", ".") ?></strong></td>
                            </tr>  
                            <tr>
                                <td>Biaya Transaksi</td>
                                <td>:</td>
                                <td><strong>Rp. <?php echo number_format($biayaTransaksi, 0, ",", ".") ?></strong></td>
                            </tr>
                            <tr>
                                <td>Total Pembayaran</td>
                                <td>:</td>
                                <td><strong>Rp. <?php echo number_format($nominalPembayaran, 0, ",", ".") ?></strong></td>
                            </tr>  
                            <tr>
                                <td>Metode Pembayaran</td>
                                <td>:</td>
                                <td> <strong><?php echo $MetodePembayaran ?></strong></td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                    <br /> <br />
                    <?php echo "Terima Kasih"?>
                    <br /> <br />
                    </td>
                </tr>

                <tr>
                    <td>
                    <?php echo "Transfer dana ke rekening BCA, Mandiri, BRI dan BTN dilakukan maksimal 1X24 jam di hari kerja."?>
                    </td>
                </tr>
                <tr>
    </tr>
</table>
</body>
</html>