Hi, <?php echo $nama ?> <br></br>

Terima kasih telah melakukan pengajuan Pembiayaan Usaha. <br></br>

<table class="table table-responsive">
    <tr>
        <td>No Pengajuan</td>
        <td>:</td>
        <td> <strong><?php echo $noPengajuan?></strong></td>
    </tr>
    <tr>
        <td>Tanggal Pengajuan</td>
        <td>:</td>
        <td> <strong><?php echo $tglPengajuan ?></strong></td>
    </tr> 
    <tr>
        <td>Jenis/No ID</td>
        <td>:</td>
        <td> <strong><?php echo $jenisId.' / '.$noId ?></strong></td>
    </tr>
    <tr>
        <td>Alamat</td>
        <td>:</td>
        <td> <strong><?php echo $alamat ?></strong></td>
    </tr>
    
    <tr>
        <td>No HP</td>
        <td>:</td>
        <td> <strong><?php echo $noHp ?></strong></td>
    </tr>
    <tr>
        <td>Kebutuhan Modal</td>
        <td>:</td>
        <td> <strong>Rp. <?php echo number_format($kebutuhanModal, 0, ",", ".") ?></strong></td>
    </tr>    
    <tr>
        <td>Agunan</td>
        <td>:</td>
        <td> <strong><?php echo ucfirst($agunan) ?></strong></td>
    </tr>
    <tr>
        <td>Merk</td>
        <td>:</td>
        <td> <strong><?php echo $merk ?></strong></td>
    </tr>
    <tr>
        <td>Tahun Pembuatan</td>
        <td>:</td>
        <td> <strong><?php echo $tahunPembuatan ?></strong></td>
    </tr>    
    <tr>
        <td>Nama Usaha</td>
        <td>:</td>
        <td> <strong><?php echo $namaUsaha ?></strong></td>
    </tr>
    <tr>
        <td>Jenis Usaha</td>
        <td>:</td>
        <td> <strong><?php echo $jenisUsaha ?></strong></td>
    </tr>    
    <tr>
        <td>Alamat Usaha</td>
        <td>:</td>
        <td> <strong><?php echo $alamatUsaha ?></strong></td>
    </tr>
    
</table>

<br>
<br>
Selanjutnya petugas kami akan mengubungi anda.
<br>
<table class="table table-responsive">
    <tr>
        <td>Nama Outlet</td>
        <td>:</td>
        <td> <strong><?php echo $namaOutlet ?></strong></td>
    </tr>
    <tr>
        <td>Alamat Outlet</td>
        <td>:</td>
        <td> <strong><?php echo $alamatOutlet ?></strong></td>
    </tr>    
    <tr>
        <td>No Telepon</td>
        <td>:</td>
        <td> <strong><?php echo $telpOutlet ?></strong></td>
    </tr>
</table>

<br><br>
Terima Kasih       

<br>
<br>