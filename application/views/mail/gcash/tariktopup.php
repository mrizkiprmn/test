Hi, <?php echo $nama ?>
<br /></br /><br />
Transaksi <?php echo $jenisTransaksi ?> G-Cash anda Sukses dengan rincian 
<br /><br />
<table class="table table-responsive">
    <tr>
        <td>Reff ID</td>
        <td>:</td>
        <td> <strong><?php echo $reffSwitching?></strong></td>
    </tr>
    <tr>
        <td>Tanggal</td>
        <td>:</td>
        <td> <strong><?php $_tglTransaksi = new DateTime($tglTransaksi); echo $_tglTransaksi->format('d/m/Y H:i:s') ?></strong></td>
    </tr>
    <tr>
        <td>Jenis Transaksi</td>
        <td>:</td>
        <td> <strong><?php echo $jenisTransaksi; ?></strong></td>
    </tr>
    <tr>
        <td>No G-Cash</td>
        <td>:</td>
        <td> <strong><?php echo $gcashId; ?></strong></td>
    </tr>
    <tr>
        <td>Nama</td>
        <td>:</td>
        <td> <strong><?php echo $nama ?></strong></td>
    </tr>
    <tr>
        <td>Nominal</td>
        <td>:</td>
        <td> <strong>Rp <?php echo number_format($amount, 0, ',', '.') ?></strong></td>
    </tr>
    <tr>
        <td>Bank</td>
        <td>:</td>
        <td> <strong>G-Cash <?php echo $bankName ?></strong></td>
    </tr>
    <tr>
        <td>Status</td>
        <td>:</td>
        <td> <strong><?php echo $status; ?></strong></td>
    </tr>
</table>
<br>
Jika Anda membutuhkan informasi lebih lanjut, bisa hubungi Call Center kami di 1500-569 / WA: 081233007773 / 08589995993
<br>
Terima kasih telah menggunakan Layanan G-Cash Pegadaian.


<br /><br />

