Hi, <?php echo $nama ?>
<br /></br /><br />
Selamat aktivasi layanan Saldo G-Cash Anda Sukses!
<br /><br />
<table class="table table-responsive">
    <tr>
        <td>Reff ID</td>
        <td>:</td>
        <td> <strong><?php echo $trxId?></strong></td>
    </tr>
    <tr>
        <td>Tanggal</td>
        <td>:</td>
        <td> <strong><?php echo date('d/m/Y'); ?></strong></td>
    </tr>
    <tr>
        <td>CIF</td>
        <td>:</td>
        <td> <strong><?php echo $cif; ?></strong></td>
    </tr>
    <tr>
        <td>Nama</td>
        <td>:</td>
        <td> <strong><?php echo $nama ?></strong></td>
    </tr>
    <tr>
        <td>Nomor G-Cash</td>
        <td>:</td>
        <td> <strong><?php echo $virtualAccount ?></strong></td>
    </tr>
    <tr>
        <td>Bank</td>
        <td>:</td>
        <td> <strong><?php echo $bankName ?></strong></td>
    </tr>
    <tr>
        <td>Status</td>
        <td>:</td>
        <td> <strong>Aktif</strong></td>
    </tr>
</table>
<br>
Jika Anda membutuhkan informasi lebih lanjut, bisa hubungi Call Center kami di 1500-569 / WA: 081233007773 / 08589995993
<br>
Terima kasih telah menggunakan Layanan G-Cash Pegadaian.


<br /><br />

