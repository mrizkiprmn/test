Hi, <?php echo $name ?>
<br /><br />
<?php if (!empty($contentDescription)) {
    foreach ($contentDescription as $value) {
        echo $value;
        echo "<br />";
    }
} ?>
<?php if (!empty($contentList)) {
    echo "<br />";
    echo "<table border='0'>";
    foreach ($contentList as $list) {
        echo "<tr>";
        echo "<td>";
        if (empty($list['key'])) {
            echo "";
        } else {
            echo $list['key'];
        }
        echo "</td>";
        echo "<td>: <strong>";
        if (empty($list['value'])) {
            echo "";
        } else {
            echo $list['value'];
        }
        echo "</strong></td>";
        echo "</tr>";
    }
    echo "</table>";
} ?>
<br />
<?php if (!empty($contentFooter)) {
    foreach ($contentFooter as $value) {
        echo $value;
        echo "<br />";
    }
} ?>
<br />
Terima kasih