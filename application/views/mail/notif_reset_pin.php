Hi, <?php echo $nama ?> 
<br /></br /><br />
Anda telah melakukan permintaan reset PIN Transaksi Finansial melalui:

<br /><br />
<table class="table table-responsive">
    <tr>
        <td>Cabang Pegadaian</td>
        <td>:</td>
        <td> <strong><?php echo $cabang ?></strong></td>
    </tr>
    <tr>
        <td>Waktu</td>
        <td>:</td>
        <td> <strong><?php echo $waktu; ?></strong></td>
    </tr>    
</table>

<br><br>
PIN Transaksi sementara Anda adalah <strong><?php echo $pin?></strong>.
<br><br>
Setelah Anda menerima email ini, untuk keamanan transaksi anda, mohon segera ganti PIN Transaksi Finansial melalui aplikasi Pegadaian Digital. 
<br><br>
Terima Kasih
<br><br>




