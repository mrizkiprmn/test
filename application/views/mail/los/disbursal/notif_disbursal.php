Hi, <?= $data->nama ?? '' ?>
<br>
<br>
Sesuai hasil konfirmasi kamu, berikut fasilitas kredit yang disetujui :
<br>
<br/>
<table>
    <tr>
        <td>Nama</td>
        <td>:</td>
        <td><strong> <?= $data->nama ?? '-' ?></strong></td>
    </tr>
    <tr>
        <td>No. CIF</td>
        <td>:</td>
        <td><strong> <?= $data->cif ?? '-' ?></strong></td>
    </tr>
    <tr>
        <td>Nama Usaha</td>
        <td>:</td>
        <td><strong> <?= $data->nama_usaha ?? '-' ?></strong></td>
    </tr>
    <tr>
        <td>Deskripsi Usaha</td>
        <td>:</td>
        <td><strong> <?= $data->deskriptsi_usaha ?? '-' ?></strong></td>
    </tr>
    <tr>
        <td>Pengajuan Pinjaman</td>
        <td>:</td>
        <td><strong> <?= $data->up ?? '-' ?></strong></td>
    </tr>
    <tr>
        <td>Tenor</td>
        <td>:</td>
        <td><strong><?= $data->tenor ?? '-' ?></strong></td>
    </tr>
    <tr>
        <td>Outlet Pengajuan</td>
        <td>:</td>
        <td><strong> <?= $data->nama_cabang ?? '-' ?></strong></td>
    </tr>
    <tr>
        <td>Pinjaman Disetujui</td>
        <td>:</td>
        <td><strong> <?= $data->up_approval ?? '-' ?></strong></td>
    </tr>
    <tr>
        <td>Sewa Modal</td>
        <td>:</td>
        <td><strong><?= $data->sewa_modal ?? '-'; ?></strong></td>
    </tr>
    <tr>
        <td>Biaya Administrasi</td>
        <td>:</td>
        <td><strong><?= $data->admin ?? '-'; ?></strong></td>
    </tr>
    <tr>
        <td>Angsuran</td>
        <td>:</td>
        <td><strong><?= $data->angsuran ?? '-' ?></strong></td>
    </tr>
    <tr>
        <td>Bukti Kepemilikan Agunan No</td>
        <td>:</td>
        <td><strong><?= $data->bukti_agunan ?? '-' ?></strong></td>
    </tr>
</table>

<br>
Silahkan datang ke <strong><?= $data->nama_cabang ?? '-' ?></strong>,&nbsp; <strong><?= $data->alamat ?? '-' ?> </strong>
<br>
Terima kasih.