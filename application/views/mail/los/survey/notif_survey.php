Hi, <?= $data->nama ?? '' ?>
<br>
<br>
Tim kami sedang melakukan survey.
<br>
<br/>
Survey dilaksanakan oleh
<br>
<br/>
<table>
    <tr>
        <td>Nama Surveyor</td>
        <td>:</td>
        <td><strong> <?= $data->nama_surveyor ?? '-' ?></strong></td>
    </tr>
    <tr>
        <td>Nomor Telephone</td>
        <td>:</td>
        <td><strong><?= $data->noHp ?? '-'; ?></strong></td>
    </tr>
</table>

<br>
Untuk informasi lebih lanjut. Silahkan hubungi Tim survey kami diatas.
<br>
<br/>
Terima kasih.

<br/> <br/>