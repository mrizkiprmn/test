Hi, <?= $data->nama ?? '' ?>
<br>
<br/>
<p align="justify">
    Sesuai dengan hasil konfirmasi kamu ke Tim kami, kamu belum menyetujui fasilitas kredit yang diberikan.
    Manfaatkan  produk-produk Pegadaian lainnya  <a href="https://www.pegadaian.co.id/produk">di sini </a>
</p>
<br>
Terima Kasih
<br/>
<br/> <br/>