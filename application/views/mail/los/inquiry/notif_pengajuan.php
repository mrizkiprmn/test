Hi, <?= $data->nama ?? '' ?>
<br>
<br>
Kami telah menerima pengajuan pembiayaan kamu.
<br>
<br/>
Berikut merupakan rincian pengajuan kamu
<br>
<br/>
<table>
    <tr>
        <td>Pengajuam Pinjaman</td>
        <td>:</td>
        <td><strong> <?= $data->up ?? '-' ?></strong></td>
    </tr>
    <tr>
        <td>Tenor</td>
        <td>:</td>
        <td><strong><?= $data->tenor ?? '-' ?></strong></td>
    </tr>
    <tr>
        <td>Angsuran</td>
        <td>:</td>
        <td><strong><?= $data->angsuran ?? '-' ?></strong></td>
    </tr>
</table>

<br>
Tim kami akan segera menghubungi kamu dan memproses pengajuan kamu
<br>
Terima kasih.

<br/> <br/>