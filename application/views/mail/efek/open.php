Hi, <?php echo $nama ?>
<br /></br /><br />
Terima kasih telah melakukan pengajuan gadai.

<br /><br />
<table class="table table-responsive">
    <tr>
        <td>No Pengajuan</td>
        <td>:</td>
        <td> <strong><?php echo $bookingId; ?></strong></td>
    </tr>
    <tr>
        <td>Tanggal Pengajuan</td>
        <td>:</td>
        <td> <strong><?php $_tglPengajuan = new DateTime($tanggalPengajuan); echo $_tglPengajuan->format('d-m-Y') ?></strong></td>
    </tr>
    <tr>
        <td>Nama</td>
        <td>:</td>
        <td> <strong><?php echo $namaNasabah ?></strong></td>
    </tr>
    <tr>
        <td>Jenis/No ID</td>
        <td>:</td>
        <td> <strong><?php echo $ktp ?></strong></td>
    </tr>
    <tr>
        <td>Alamat</td>
        <td>:</td>
        <td> <strong><?php echo $alamat ?></strong></td>
    </tr>
    <tr>
        <td>No HP</td>
        <td>:</td>
        <td> <strong><?php echo $noHp ?></strong></td>
    </tr>
    <tr>
        <td>Detail Barang Jaminan</td>
        <td>:</td>
        <td> <strong>Saham</strong></td>
    </tr>
    <tr>
        <td>Perkiraan Uang Pinjaman</td>
        <td>:</td>
        <td> <strong><?php echo 'Rp '.number_format($minUp, 0, ",", ".").' s/d '.number_format($maxUp, 0, ",", ".") ?> </strong></td>
    </tr>
</table>

<br>

List Jaminan:
 
<br>

<table class="table table-responsive table-bordered">
    <thead>
        <tr>
            <th style="text-align: center">No</th>
            <th style="text-align: center">Kode dan Nama Saham</th>
            <th style="text-align: center">Jumlah LOT</th>
            <th style="text-align: center">Jumlah Lembar</th>
        </tr>
    </thead>
    <tbody>
        <?php for ($i = 0; $i < count($listJaminan); $i++) : ?>
        <tr>
            <td><?php echo $i+1; ?></td>
            <td><?php echo $listJaminan[$i]['kode'].' '.$listJaminan[$i]['nama'] ?></td>
            <td><?php echo $listJaminan[$i]['qty'] ?></td>
            <td><?php echo $listJaminan[$i]['qty'] * $listJaminan[$i]['satuan']  ?></td>
        </tr>        
        <?php endfor;?>
    </tbody>    
</table>

<br><br>

Untuk kelengkapan Dokumen dapat di download di LINK berikut:

<ol>
    <li><a href="<?php echo base_url()?>uploaded/efek/SyaratKetentuan.docx">Syarat dan Kelengkapan Dokumen Individu</a></li>
    <li><a href="<?php echo base_url()?>uploaded/efek/FormulirDataNasabah.xlsx">FDN/FPBK dan KYC Individu</a></li>
</ol>

Silahkan Lengkapi Dokumen tersebut dan Menandatangani kemudian menyerahkan ke Outlet Gadai Efek untuk dengan alamat :

<br><br>
<strong>Unit Gadai Efek</strong> <br> 
Kantor Pusat PT Pegadaian <br>
Jalan Kramat Raya 162 Jakarta Pusat <br>
10430 <br>

<br><br>
Catatan :<br>
1. Nilai Diatas Merupakan Nilai Simulasi hari ini.<br>
2. Nilai Taksiran dan Pinjaman sebenarnya akan mengikuti nilai saat persetujuan

<br><br>
Apabila Anda memiliki pertanyaan dapat menghubungi Petugas Kami pada :<br>

<strong>021 -31555550 ext 100</strong><br>
Email: <strong><a href="mailto:gadai.efek@pegadaian.co.id">gadai.efek@pegadaian.co.id</strong>
<br /><br />
