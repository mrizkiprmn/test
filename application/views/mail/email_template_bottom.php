                            <br /><br />
                            <?php if (!empty($coupon_is_active) && $coupon_is_active) {?>
                                <?=$coupon_message?>
                                <br /><br />
                            <?php } ?>
                            </td>
                        </tr>
                        <tr><td height="60"></td></tr>
                        <tr>
                            <td style="background: #EAEAEA; font-family: 'open sans', sans-serif; font-size: 0.7em; color: #7c7c7c;">
                                <table>
                                    <tr>
                                        <td>
                                            <img src="<?php echo base_url() ?>assets/sign-warning-icon.png" style="width: 30px; padding-left: 5px; padding-right: 5px;">
                                        </td>
                                        <td style="padding: 5px;">
                                            Segala bentuk informasi seperti nomor kontak, alamat e-mail, atau password kamu bersifat rahasia. Jangan menginformasikan data-data tersebut kepada siapa pun, termasuk kepada pihak yang mengatasnamakan Pegadaian Digital Service. 
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="background: #F3F3F3; font-family: 'open sans', sans-serif; font-size: 0.7em; color: #7c7c7c;">
                                <table>
                                    <tr>
                                        <td style="padding: 5px; width: 300px;">
                                            PT Pegadaian (Persero)<br>
                                            Jl.Kramat Raya No.162 - Jakarta Pusat - 10430<br>
                                            Telp. 1500569, WhatsApp. 0813-2443-2443<br>
                                            Email : <a href="mailto:customer.care@pegadaian.co.id">customer.care@pegadaian.co.id</a><br>
                                            Website : <a href="http://www.pegadaian.co.id">http://www.pegadaian.co.id</a>
                                        </td>
                                        <td style="text-align: right !important; padding-left: 10px;">
                                            <img src="<?php echo base_url() ?>assets/googleplay-download-grey.png" style="width: 100px;">
                                        </td>
                                        <td style="text-align: right !important; padding-left: 10px;">
                                            <img src="<?php echo base_url() ?>assets/App-Store-Icon.png" style="width: 100px;">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-family: 'open sans', sans-serif; font-size: 0.7em; color: #7c7c7c; text-align: center;padding : 5px; border-bottom: 1px solid #F3F3F3">
                                Harap jangan membalas e-mail ini, karena e-mail ini dikirimkan secara otomatis oleh sistem.
                            </td>
                        </tr>
                    </table>
                 </td>
            </tr>
        </table>
    </body>
</html>