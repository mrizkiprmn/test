Hi, <?php echo $nama ?>
<br>
Terima kasih telah melakukan pembelian <?php echo $product ?>.
<br>
Rincian Transaksi:

<table>
    <tr>
        <td>No Jurnal</td>
        <td>:</td>
        <td> <strong><?php echo $reffCore; ?></strong></td>
    </tr>
    <tr>
        <td>No Reff</td>
        <td>:</td>
        <td> <strong><?php echo $trxId; ?></strong></td>
    </tr>
    <tr>
        <td>Tgl Transaksi</td>
        <td>:</td>
        <td> <strong><?php echo DateTime::createFromFormat('Y-m-d H:i:s', $tglTransaksi)->format('d/m/Y'); ?></strong></td>
    </tr>
    <tr>
        <td>Produk</td>
        <td>:</td>
        <td> <strong><?php echo $product ?></strong></td>
    </tr>
    <tr>
        <td>No HP </td>
        <td>:</td>
        <td> <strong><?php echo $norek; ?></strong></td>
    </tr>
    <tr>
        <td>Harga</td>
        <td>:</td>
        <td> <strong>Rp <?php echo number_format($totalKewajiban, 0, ",", "."); ?></strong></td>
    </tr>

    <!-- PAYMENT METHOD BNI -->
    <?php
    if ($paymentMethod == 'BNI'
                || $paymentMethod == 'VA_BCA'
                || $paymentMethod == 'VA_MANDIRI'
                || $paymentMethod == 'VA_BRI'
                || $paymentMethod == 'VA_PERMATA'
                || $paymentMethod == 'VA_MAYBANK'
                || $paymentMethod == 'FINPAY') :
        ?>
    <tr>
        <td>Metode Pembayaran</td>
        <td>:</td>
        <td>
            <strong>
            <?php
            if ($paymentMethod == 'BNI') {
                echo 'Virtual Account BNI';
            } elseif ($paymentMethod == 'VA_BCA') {
                echo 'Virtual Account BCA';
            } elseif ($paymentMethod == 'VA_MANDIRI') {
                echo 'Virtual Account Mandiri';
            } elseif ($paymentMethod == 'VA_PERMATA') {
                echo 'Virtual Account Permata';
            } elseif ($paymentMethod == 'VA_BRI') {
                echo 'Virtual Account BRI';
            } elseif ($paymentMethod == 'VA_MAYBANK') {
                echo 'Virtual Account Maybank';
            } elseif ($paymentMethod == 'FINPAY') {
                echo 'Virtual Account FINPAY';
            }
            ?>
            </strong>
        </td>
    </tr>    
    <tr>
        <td>No Rekening VA</td>
        <td>:</td>
        <td> <strong><?php echo $va ?></strong></td>
    </tr>
    <tr>
        <td>Biaya </td>
        <td>:</td>
        <td> <strong>Rp <?php echo number_format($biayaTransaksi, 0, ",", ".") ?></strong></td>
    </tr>
    <tr>
        <td>Nominal Transfer</td>
        <td>:</td>
        <td> <strong>Rp <?php echo number_format($totalKewajiban + $biayaTransaksi, 0, ",", ".") ?> Include PPN</strong></td>
    </tr>    
    <?php endif ?>

    <!-- PAYMENT METHOD MANDIRI -->
    <?php if ($paymentMethod == 'Mandiri') : ?>
    <tr>
        <td>Metode Pembayaran</td>
        <td>:</td>
        <td> <strong>Mandiri Click Pay</strong></td>
    </tr>  
    <tr>
        <td>No Rekening VA</td>
        <td>:</td>
        <td> <strong><?php echo $va ?></strong></td>
    </tr>
    <tr>
        <td>Biaya </td>
        <td>:</td>
        <td> <strong>Rp <?php echo number_format($biayaTransaksi, 0, ",", ".") ?></strong></td>
    </tr>
    <tr>
        <td>Nominal Transfer</td>
        <td>:</td>
        <td> <strong>Rp <?php echo number_format($totalKewajiban + $biayaTransaksi, 0, ",", ".") ?> Include PPN</strong></td>
    </tr>
    <tr>
        <td>Reff Biller:</td>
        <td>:</td>
        <td> <strong><?php echo $reffBiller ?></strong></td>
    </tr>
    <?php endif; ?>

    <!-- PAYMENT METHOD WALLET -->
    <?php if ($paymentMethod == 'WALLET') : ?>
    <tr>
        <td>Metode Pembayaran</td>
        <td>:</td>
        <td> <strong>WALLET</strong></td>
    </tr>
    <tr>
        <td>Reff MPO:</td>
        <td>:</td>
        <td> <strong><?php echo $reffMpo ?></strong></td>
    </tr>
    <tr>
        <td>Serial Number:</td>
        <td>:</td>
        <td> <strong><?php echo $serialNumber ?></strong></td>
    </tr>
    <tr>
        <td>SID:</td>
        <td>:</td>
        <td> <strong><?php echo $sid ?></strong></td>
    </tr>
    <?php endif; ?>

    <tr>
        <td>Waktu Pembayaran</td>
        <td>:</td>
        <td> <strong><?php echo date('d/m/Y H:i:s'); ?></strong></td>
    </tr>
    <tr>
        <td>Status </td>
        <td>:</td>
        <td> 
            <strong><?php echo $paymentStatus['label'] ?></strong>. 
            <?php echo $paymentStatus['code'] == '2' ? $paymentStatus['message'] : '' ?>
        </td>
    </tr>


</table>

<br>

<?php echo $mpoFooter ?>





