<strong>Informasi Tambah Transaksi</strong>
    <table class="table table-responsive">
        <tr>
            <td>Jenis Transaksi</td>
            <td>:</td>
            <td> <strong><?php echo $jenis_transaksi ?></strong></td>
        </tr>
        <tr>
            <td>Produk</td>
            <td>:</td>
            <td> <strong><?php echo $nama_product ?></strong></td>
        </tr>
        <tr>
            <td>Nomor Kredit</td>
            <td>:</td>
            <td> <strong><?php echo $no_kredit; ?></strong></td>
        </tr>
        <tr>
            <td>Nama</td>
            <td>:</td>
            <td> <strong><?php echo $nama_nasabah; ?></strong></td>
        </tr>
        <tr>
            <td>Jumlah Pendebitan</td>
            <td>:</td>
            <td> <strong><?php echo $jumlah_debit ?></strong></td>
        </tr>
        <tr>
            <td>Tanggal Pendebitan</td>
            <td>:</td>
            <td> <strong><?php echo $tanggal_pendebitan ?></strong></td>
        </tr>
        <tr>
            <td>Jangka Waktu Pendebitan</td>
            <td>:</td>
            <td> <strong><?php echo $jangka_waktu_debit ?></strong></td>
        </tr>
        <tr>
            <td>Tanggal / Waktu Registrasi</td>
            <td>:</td>    
            <td> <strong> <?php echo $jam_registrasi ?></strong></td>
        </tr>
        
        <tr>
            <td>Tanggal Mulai</td>
            <td>:</td>    
            <td> <strong> <?php echo $tanggal_mulai ?></strong></td>
        </tr>
        <tr>
            <td>Tanggal Selesai</td>
            <td>:</td>
            <td> <strong><?php echo $tanggal_selesai ?></strong></td>
        </tr>
    </table>

    <br />
    <strong>Informasi Bank</strong>
    <table class="table table-responsive">
        <tr>
            <td>Bank</td>
            <td>:</td>
            <td> <strong><?php echo $nama_bank ?></strong></td>
        </tr>
        <tr>
            <td>Nomor Rekening</td>
            <td>:</td>
            <td> <strong><?php echo $norek_bank ?></strong></td>
        </tr>
        <tr>
            <td>Nomor Handphone</td>
            <td>:</td>
            <td> <strong><?php echo $no_hp ?></strong></td>
        </tr>
    </table>
