Hi, <?php echo $nama ?> 
<br /></br /><br />
Terima kasih telah melakukan pembayaran.
<br /><br /><br />
Transaksi Anda Berhasil:
<br /><br />
<table class="table table-responsive">
    <tr>
        <td>Tanggal Transaksi</td>
        <td>:</td>
        <td> <strong>
           <?php $tr = new DateTime($tglTransaksi);
            echo $tr->format('d/m/Y H:i:s'); ?>     
            </strong></td>
    </tr>
    <tr>
        <td>Referensi</td>
        <td>:</td>
        <td> <strong><?php echo $reffSwitching; ?></strong></td>
    </tr>
    <tr>
        <td>Nama Produk</td>
        <td>:</td>
        <td> <strong><?php echo $namaProduk ?></strong></td>
    </tr>
    <tr>
        <td>No Kredit</td>
        <td>:</td>
        <td> <strong><?php echo $norek; ?></strong></td>
    </tr> 
    <tr>
        <td>Jumlah</td>
        <td>:</td>
        <td> <strong>Rp. <?php echo number_format($amount, 0, ",", "."); ?></strong></td>
    </tr>
    <tr>
        <td>Biaya</td>
        <td>:</td>
        <td> <strong>Rp. <?php echo number_format($surcharge, 0, ",", "."); ?></strong></td>
    </tr>
    <tr>
        <td>Administrasi</td>
        <td>:</td>
        <td> <strong>Rp. <?php echo number_format($administrasi, 0, ",", "."); ?></strong></td>
    </tr>
    <tr>
        <td>Total</td>
        <td>:</td>
        <td> <strong>Rp. <?php echo number_format($totalKewajiban, 0, ",", "."); ?></strong></td>
    </tr>
    


</table>
<br><br>

Terima Kasih


