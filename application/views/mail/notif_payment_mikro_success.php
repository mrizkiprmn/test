Hi, <?php echo $nama ?> 
<br /></br /><br />
Terima kasih telah melakukan pembayaran.
<br /><br /><br />
Transaksi Anda Berhasil:
<br /><br />

<?php if ($jenisTransaksi === "TB" && $produk === "KRASIDA") : ?>
    <table class="table table-responsive">
        <tr>
            <td>Jenis Transaksi</td>
            <td>:</td>
            <td> <strong><?= ($jenisTransaksi === "TB") ? "Pelunasan Angsuran Mikro" :"Angsuran Mikro" ?></strong></td>
        </tr>
        <tr>
            <td>Produk</td>
            <td>:</td>
            <td> <strong><?php echo $produk ?></strong></td>
        </tr>
        <tr>
            <td>No Kredit</td>
            <td>:</td>
            <td> <strong><?php echo $norek; ?></strong></td>
        </tr>
        <tr>
            <td>Referensi</td>
            <td>:</td>
            <td> <strong><?php echo $trxId; ?></strong></td>
        </tr>
        <tr>
            <td>Nama Nasabah</td>
            <td>:</td>
            <td> <strong><?php echo $namaNasabah; ?></strong></td>
        </tr>
        <tr>
            <td>Tarif Sewa Modal</td>
            <td>:</td>
            <td> <strong><?php echo $tarifSewaModal; ?></strong></td>
        </tr>
        <tr>
            <td>Sisa Pokok</td>
            <td>:</td>
            <td> <strong>Rp. <?php echo number_format($sisaPokok, 0, ",", "."); ?></strong></td>
        </tr>
        <tr>
            <td>Sisa Sewa Modal</td>
            <td>:</td>
            <td> <strong>Rp. <?php echo number_format($sisaSM, 0, ",", "."); ?></strong></td>
        </tr>
        <tr>
            <td>Tunggakan Pokok</td>
            <td>:</td>
            <td> <strong>Rp. <?php echo number_format($tunggakanPokok, 0, ",", "."); ?></strong></td>
        </tr>
        <tr>
            <td>Tunggakan Sewa Modal</td>
            <td>:</td>
            <td> <strong>Rp. <?php echo number_format($tunggakanSM, 0, ",", "."); ?></strong></td>
        </tr>
        <tr>
            <td>Denda Tunggakan</td>
            <td>:</td>
            <td> <strong>Rp. <?php echo number_format($denda, 0, ",", "."); ?></strong></td>
        </tr>
        <tr>
            <td>Total Tunggakan</td>
            <td>:</td>
            <td> <strong>Rp. <?php echo number_format($totalTunggakan, 0, ",", "."); ?></strong></td>
        </tr>
        <tr>
            <td>Diskon Pelunasan</td>
            <td>:</td>
            <td> <strong>Rp. <?php echo number_format($diskonPelunasan, 0, ",", "."); ?></strong></td>
        </tr>
        <tr>
            <td>Biaya Channel</td>
            <td>:</td>
            <td> <strong>Rp. <?php echo number_format($biayaTransaksi, 0, ",", "."); ?></strong></td>
        </tr>
        <tr>
            <td>Total</td>
            <td>:</td>
            <td> <strong>Rp. <?php echo number_format($total + $biayaTransaksi, 0, ",", "."); ?></strong></td>
        </tr>
        <tr>
            <td>Metode Pembayaran</td>
            <td>:</td>
            <td> <strong><?php echo $payment; ?></strong></td>
        </tr>
    </table>
<?php else : ?>
    <table class="table table-responsive">
        <tr>
            <td>Metode Pembayaran</td>
            <td>:</td>
            <td> <strong><?php echo $payment; ?></strong></td>
        </tr>
        <tr>
            <td>Referensi</td>
            <td>:</td>
            <td> <strong><?php echo $trxId; ?></strong></td>
        </tr>
        <tr>
            <td>Produk</td>
            <td>:</td>
            <td> <strong><?php echo $produk ?></strong></td>
        </tr>
        <tr>
            <td>No Kredit</td>
            <td>:</td>
            <td> <strong><?php echo $norek; ?></strong></td>
        </tr>
        <tr>
            <td>Nama Nasabah</td>
            <td>:</td>
            <td> <strong><?php echo $namaNasabah; ?></strong></td>
        </tr>
        <tr>
            <td>Angsuran Ke</td>
            <td>:</td>
            <td> <strong><?php echo $angsuranKe ?></strong></td>
        </tr>
        <tr>
            <td>Angsuran</td>
            <td>:</td>
            <td> <strong>Rp. <?php echo number_format($angsuran, 0, ",", "."); ?></strong></td>
        </tr>
        <tr>
            <td>Administrasi</td>
            <td>:</td>
            <td> <strong>Rp. <?php echo number_format($administrasi, 0, ",", "."); ?></strong></td>
        </tr> 
        <tr>
            <td>Denda</td>
            <td>:</td>
            <td> <strong>Rp. <?php echo number_format($denda, 0, ",", "."); ?></strong></td>
        </tr> 
        <tr>
            <td>Biaya Channel</td>
            <td>:</td>
            <td> <strong>Rp. <?php echo number_format($biayaTransaksi, 0, ",", "."); ?></strong></td>
        </tr>
        <tr>
            <td>Total</td>
            <td>:</td>
            <td> <strong>Rp. <?php echo number_format($total + $biayaTransaksi, 0, ",", "."); ?></strong></td>
        </tr>
    </table>
<?php endif; ?>
<br><br>

Terima Kasih