Hi, <?= $data->referrer_name ?? '' ?>
<br>
<br>
Selamat, kamu dapat <?= $data->type_reward ?? '' ?> <?= $data->reward ?? '' ?>
<br>
<br>
<table>
    <tr>
        <td>Tanggal</td>
        <td>:</td>
        <td><strong> <?= $data->date ?? '' ?></strong></td>
    </tr>
    <tr>
        <td>No. Referensi</td>
        <td>:</td>
        <td><strong><?= $data->referensi_no ?? ''; ?></strong></td>
    </tr>
    <tr>
        <td>Nama Sahabat</td>
        <td>:</td>
        <td><strong><?= $data->referral_name ?? '' ?></strong></td>
    </tr>
</table>

<br>
<br>
Terima kasih.

<br/> <br/>
Untuk info lebih lanjut, mohon hubungi cabang pegadaian terdekat. Terima kasih
<br/> <br/>