<?php
$tagline = $data->tagline ?? '-';
$point = $data->point ?? '-';
$transaction_type = $data->transaction_type ?? '-';
$payment_method = $data->payment_method ?? '';
$reff_core = $data->reff_core ?? '-';
$payment_date = $data->payment_date ?? '-';
$channel = $data->channel ?? '-';

if (!empty($payment_method)) {
    $payment_method = "dengan pembayaran {$payment_method}";
}
?>

<?= $this->load->view('mail/email_template_top', ['title' => $tagline], true) ?>

    Hi, <?= $data->user->nama ?? '' ?>
    <br>

    <p align="justify">
        Selamat, kamu mendapatkan <strong><?= $point ?></strong> poin
        dari transaksi <strong style="font-weight:bold"><?= $transaction_type ?></strong>
        via <?= $channel ?> <?= $payment_method ?>
    </p>

    <br>
    <table>
        <tr>
            <td>Reff</td>
            <td>:</td>
            <td><strong><?= $reff_core ?></strong></td>
        </tr>
        <tr>
            <td>Tanggal</td>
            <td>:</td>
            <td><strong><?= $payment_date ?></strong></td>
        </tr>
    </table>

<?= $this->load->view('mail/email_template_bottom', [], true) ?>