Hi, <?= $data->customer_name ?? '' ?>
<br>
<br>
<?= $data->tagline ?? '' ?>, dengan rincian sebagai berikut 
<br>
<br>
<table>
    <tr>
        <td>Nomor Kredit </td>
        <td>:</td>
        <td><strong> <?= $data->credit_number ?? '' ?></strong></td>
    </tr>
    <tr>
        <td>Tanggal Kredit</td>
        <td>:</td>
        <td><strong><?= $data->tanggal_kredit ?? '' ?></strong></td>
    </tr>
    <tr>
        <td>Tanggal Jatuh Tempo</td>
        <td>:</td>
        <td><strong><?= $data->tanggal_jatuh_tempo ?? '' ?></strong></td>
    </tr>
    <tr>
        <td>Hari Real (Hari Tarif)</td>
        <td>:</td>
        <td><strong><?= $data->jumlah_hari_real ?? '' ?> ( <?= $data->jumlah_hari_tarif ?? '' ?> ) </strong></td>
    </tr>
    <tr>
        <td>Uang Pinjaman Tambahan</td>
        <td>:</td>
        <td><strong><?= $data->uang_pinjaman ?? '' ?></strong></td>
    </tr>
    <tr>
        <td>Total Uang Pinjaman</td>
        <td>:</td>
        <td><strong><?= $data->total_uang_pinjaman ?? '' ?></strong></td>
    </tr>
    <tr style="height: 10px;">
        <td></td>
        <td></td>
        <td></td>
    </tr>    
    <tr>
        <td>Sewa Modal</td>
        <td>:</td>
        <td><strong><?= $data->sewa_modal ?? '' ?></strong></td>
    </tr>
    <tr>
        <td>Administrasi</td>
        <td>:</td>
        <td><strong><?= $data->admin_surcharge ?? '' ?></strong></td>
    </tr>
    <tr>
        <td>Biaya Asuransi</td>
        <td>:</td>
        <td><strong><?= $data->insurance_surcharge ?? '' ?></strong></td>
    </tr>
    <tr>
        <td>Jasa Channel</td>
        <td>:</td>
        <td><strong><?= $data->surcharge ?? '' ?></strong></td>
    </tr>
    <tr>
        <td>Total Biaya</td>
        <td>:</td>
        <td><strong><?= $data->total_surcharge ?? '' ?></strong></td>
    </tr>
    <tr>
        <td>Nominal Yang Diterima</td>
        <td>:</td>
        <td><strong><?= $data->amount_approved ?? '' ?></strong></td>
    </tr>
    <tr style="height: 10px;">
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>Nama Bank</td>
        <td>:</td>
        <td><strong><?= $data->bank_name ?? '' ?></strong></td>
    </tr>
    <tr>
        <td>No. Rekening</td>
        <td>:</td>
        <td><strong><?= $data->bank_account_number ?? '' ?></strong></td>
    </tr>
    <tr>
        <td>Nama Pemilik</td>
        <td>:</td>
        <td><strong><?= $data->bank_customer_name ?? '' ?></strong></td>
    </tr>
</table>

<br/> <br/>
Terimakasih.
<br/> <br/>