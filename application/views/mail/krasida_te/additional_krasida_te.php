<b>Rincian Pembelian</b>

<br></br>

merchant &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : &nbsp; <b><?php echo $data->merchant;?></b>
<br></br>

<table border="1">
    <tr>
        <td style="text-align: center; vertical-align: middle;">Jenis Perhiasan</td>
        <td style="text-align: center; vertical-align: middle;">Keterangan</td>
        <td style="text-align: center; vertical-align: middle;">Berat (Gram)</td>
        <td style="text-align: center; vertical-align: middle;">Kadar</td>
        <td style="text-align: center; vertical-align: middle;">Jumlah</td>
        <td style="text-align: center; vertical-align: middle;">Harga/Barang</td>
    </tr>
    <?php
    if (!empty($data) && !empty($data->item_perhiasan)) {
        foreach ($data->item_perhiasan as $item) {
            ?>
            <tr>
                <td style="text-align: center; vertical-align: middle;"><?php echo $item['jenisPerhiasan']; ?></td>
                <td style="text-align: center; vertical-align: middle;"><?php echo $item['namaPerhiasan']; ?></td>
                <td style="text-align: center; vertical-align: middle;"><?php echo $item['berat']; ?></td>
                <td style="text-align: center; vertical-align: middle;"><?php echo $item['karat']; ?></td>
                <td style="text-align: center; vertical-align: middle;"><?php echo $item['jumlah']; ?></td>
                <td style="text-align: center; vertical-align: middle;"><?php echo Pegadaian::currencyIdr($item['subTotalHarga']); ?></td>
            </tr>
            <?php
        }
    }

    ?>

    <tr rowspan="2">
        <td colspan="6" style="text-align: right; vertical-align: middle;"><b>Total Harga Perhiasan &nbsp;&nbsp; <?php echo Pegadaian::currencyIdr($data->total_harga) ?></b></td>
    </tr>
</table>

<br>
<br>
<center><a href=<?php echo $data->url_pdf;?>><button style="background-color: #4CAF50;color: white;width:80%;height:30px">Download Perjanjian</button></a></center>
</br>