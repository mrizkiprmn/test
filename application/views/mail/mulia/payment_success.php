
    Pembelian Logam Mulia Berhasil
    <br /><br />
    Hi, <?php echo $namaNasabah ?>
    <br />
    Pembelian Logam Mulia Anda sudah kami terima dengan rincian sebagai berikut :
    <br /><br />
    <table class="table table-responsive">
        <tr>
            <td>Reff Id</td>
            <td>:</td>
            <td> <strong><?php echo $trxId; ?></strong></td>
        </tr>
        <tr>
            <td>Tanggal </td>
            <td>:</td>
            <td>
                <strong><?php echo $tglTransaksi; ?></strong>
            </td>
        </tr>
        <tr>
            <td>Metode Pembelian</td>
            <td>:</td>
            <td> <strong>Angsuran</strong></td>
        </tr>
        <tr>
            <td>No Kredit</td>
            <td>:</td>
            <td> <strong><?php echo $noKredit; ?></strong></td>
        </tr>
        <tr>
            <td>Jumlah Pembiayaan</td>
            <td>:</td>
            <td>Rp <strong><?php echo number_format($pokokPembiayaan, 0, ',', '.'); ?></strong></td>
        </tr>
        <tr>
            <td>Status</td>
            <td>:</td>
            <td> <strong>Sukses</strong></td>
        </tr>
        <tr>
            <td>Cabang</td>
            <td>:</td>
            <td>
                <strong>
                    <?php echo $namaCabang.' - '.$alamatCabang.' <br> <a href="tel:'.$telpCabang.'"><span class="fa fa-phone"></span> '.$telpCabang.'</a>' ?>
                </strong>
            </td>
        </tr>
    </table>

    <br />
    Rincian Pembelian
    <table class="table table-responsive table-bordered">
        <thead>
            <tr>
                <th width="1%" style="text-align: center">No</th>
                <th style="text-align: center">Jenis Logam Mulia</th>
                <th style="text-align: center">Denom (gram)</th>
                <th style="text-align: center">Jumlah Keping</th>
                <th style="text-align: center">Total Gram</th>
                <th style="text-align: center">Total Harga</th>
                
            </tr>
        </thead>
        <tbody>
        <?php
            $totalKeping = 0;
            $no=1;
            $totalGram = 0;
            $_totalHarga = 0;
        foreach ($item as $i) : ?>
        <tr>
            <td style="text-align: center"><?php echo $no;?></td>
            <td style="text-align: center"><?php echo $jenisLogamMulia; ?></td>
            <td style="text-align: center"><?php echo $i->denom;?></td>
            <td style="text-align: center"><?php echo $i->qty;?></td>
            <td style="text-align: center"><?php echo $i->qty * $i->denom ;?></td>
            <td style="text-align: left; white-space: nowrap">Rp <?php echo number_format($i->qty * $i->harga, 0, ',', '.') ;?></td>

            
        </tr>
            <?php
            $no++;
            $totalGram = $totalGram + ( $i->qty * $i->denom);
            $totalKeping = $totalKeping + $i->qty;
            $_totalHarga = $_totalHarga + ($i->qty * $i->harga);
        endforeach;
        ?>
        <tr>
            <th colspan="3">Total</th>
            <th style="text-align: center"><?php echo $totalKeping; ?></th>
            <th style="text-align: center"><?php echo $totalGram; ?></th>
            <th style="text-align: left; white-space: nowrap"">Rp <?php echo number_format($_totalHarga, 0, ',', '.');?></th>
        </tr>
        </tbody>
    </table>

    <br />

    <table class="table table-responsive">
        <tr>
            <td>Harga Dasar</td>
            <td>:</td>
            <td>Rp <strong><?php echo number_format($totalHarga, 0, ',', '.'); ?></strong></td>
        </tr>
        <tr>
            <td>Uang Muka</td>
            <td>:</td>
            <?php if ($payment == 'GCASH') : ?>
            <td><strong><?php echo number_format($uangMuka, 0, ',', '.'); ?> %</strong></td>
            <?php else : ?>
            <td><strong>Rp <?php echo number_format($uangMuka, 0, ',', '.'); ?> </strong></td>
            <?php endif; ?>

        </tr>
        <tr>
            <td>Sisa Pembiayaan</td>
            <td>:</td>
            <td>Rp <strong><?php echo number_format($pokokPembiayaan, 0, ',', '.'); ?></strong></td>
        </tr>
        <tr>
            <td>Jangka Waktu</td>
            <td>:</td>
            <td> <strong><?php echo $tenor; ?> Bulan</strong></td>
        </tr>
        <tr>
            <td>Angsuran/Bulan</td>
            <td>:</td>
            <td>Rp <strong><?php echo number_format($angsuran, 0, ',', '.');
            ; ?></strong></td>
        </tr>
        <tr>
            <th colspan="3" style="text-align: left">Total Bayar Awal</th>
        </tr>
        <tr>
            <td>Administrasi</td>
            <td>:</td>
            <td>Rp <strong><?php echo number_format($administrasi, 0, ',', '.');
            ; ?></strong></td>
        </tr>
        <?php
        if ($promoCode != '') {
            echo "<tr>";
            echo "<td>Promo ";
            echo $promoCode;
            echo "</td>";
            echo "<td>:</td>";
            echo "<td>Rp <strong>";
            echo number_format($promoAmount, 0, ',', '.');
            echo "</strong></td>";
            echo "</tr>";
        }
        ?>
        <!-- <tr>
            <td>Promo <?= $promoCode ?></td>
            <td>:</td>
            <td>Rp <strong><?php echo number_format($promoAmount, 0, ',', '.'); ?></strong></td>
        </tr> -->
        <!-- <tr>
            <td>Jumlah Pembayaran</td>
            <td>:</td>
            <td>Rp <strong><?php echo number_format($totalKewajiban, 0, ',', '.');
            ; ?></strong></td>
        </tr> -->
        <tr>
            <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td>Metode Pembayaran</td>
            <td>:</td>
            <td>
                <strong>
                    <?php
                    if ($payment == 'BNI') {
                        echo 'Virtual Account BNI';
                    } elseif ($payment == 'VA_BCA') {
                        echo 'Virtual Account BCA';
                    } elseif ($payment == 'VA_MANDIRI') {
                        echo 'Virtual Account Mandiri';
                    } elseif ($payment == 'VA_BRI') {
                        echo 'Virtual Account BRI';
                    } elseif ($payment == 'VA_MAYBANK') {
                        echo 'Virtual Account Maybank';
                    } elseif ($payment == 'FINPAY') {
                        echo 'Virtual Account FINPAY';
                    }
                    ?>
                </strong>
            </td>
        </tr>        
        <tr>
            <td>Rekening Tujuan</td>
            <td>:</td>
            <td> <strong><?php echo $va; ?></strong></td>
        </tr>
        <tr>
            <td>Jumlah Pembayaran</td>
            <td>:</td>
            <td>Rp <strong><?php echo number_format($totalKewajiban, 0, ',', '.');
            ; ?></strong></td>
        </tr>
        <tr>
            <td>Biaya Channel</td>
            <td>:</td>
            <td>Rp <strong><?php echo number_format($biayaTransaksi, 0, ',', '.'); ?></strong></td>
        </tr>
       
        <tr>
            <td>Total Tagihan</td>
            <td>:</td>
            <td>Rp 
                <strong>
                    <?php echo number_format($totalKewajiban + $biayaTransaksi - $discountAmount, 0, ',', '.'); ?>
                </strong>
            </td>
        </tr>
        <tr>
            <td>Tanggal Pembayaran</td>
            <td>:</td>
            <td> <strong><?php echo $tglPembayaran; ?></strong></td>
        </tr>
    </table>

    <br>

   
    <div class="text-center">
         Klik link di bawah untuk mengunduh Kontrak Perjanjian Mulia Pegadaian Anda <br>
         <a href="<?php echo $kontrakUrl ?>">
            <img src="<?php echo base_url()?>assets/downloadkontrak.png" class="img-fluid" style="max-width: 150px;">
        </a>
    </div>
    <br><br />
    Terima Kasih