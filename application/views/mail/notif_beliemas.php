
<?php if ($payment == 'BNI') : ?>
    Hi, <?php echo $namaNasabahUser ?> 
    <br /></br /><br />
    Terima kasih telah melakukan pengajuan Pembelian Tabungan Emas Pegadaian.
    <br /><br /><br />
    Silakan melakukan pembayaran untuk melanjutkan transaksi Anda.
    <br /><br />

    <table class="table table-responsive">
        <tr>
            <td>Jenis Transaksi</td>
            <td>:</td>
            <td> <strong>Top Up Tabungan Emas</strong></td>
        </tr>
        <tr>
            <td>No Rekening Tabungan Emas</td>
            <td>:</td>
            <td> <strong><?php echo $norek == '' ? $no_rekening : $norek; ?></strong></td>
        </tr>
        <tr>
            <td>Referensi</td>
            <td>:</td>
            <td> <strong><?php echo $id_transaksi; ?></strong></td>
        </tr>
        <tr>
            <td>Nama Nasabah</td>
            <td>:</td>
            <td> <strong><?php echo $namaNasabah; ?></strong></td>
        </tr>
        <tr>
            <td>CIF</td>
            <td>:</td>
            <td> <strong><?php echo $cif != "" ? $cif : "-"; ?></strong></td>
        </tr>
        <tr>
            <td>Batas Waktu Pembayaran </td>
            <td>:</td>
            <td> <strong><?php
                    $date = new DateTime($tanggal_expired);
                    echo $date->format('d/M/Y H:i:s');
            ?></strong></td>
        </tr>
        <tr>
            <td>Biaya Channel</td>
            <td>:</td>    
            <td> <strong>Rp <?php echo number_format($biayaTransaksi, 0, ",", ".") ?></strong></td>
        </tr>
        <?php
        if ($promoCode != '') {
            echo "<tr>";
            echo "<td>Promo ";
            echo $promoCode;
            echo "</td>";
            echo "<td>:</td>";
            echo "<td>Rp <strong>";
            echo number_format($promoAmount, 0, ',', '.');
            echo "</strong></td>";
            echo "</tr>";
        }
        ?>
        <!-- <tr>
            <td>Promo <?= $promoCode ?></td>
            <td>:</td>
            <td>Rp <strong><?php echo number_format($promoAmount, 0, ',', '.'); ?></strong></td>
        </tr> -->
        <tr>
            <td>Nominal Pembayaran</td>
            <td>:</td>    
            <td> <strong>Rp <?php echo number_format($totalKewajiban + $biayaTransaksi - $discountAmount, 0, ",", ".") ?></strong></td>
        </tr>
        <tr>
            <td>Metode Pembayaran</td>
            <td>:</td>
            <td> <strong>Virtual Account BNI</strong></td>
        </tr>
        <tr>
            <td>Rekening Tujuan</td>
            <td>:</td>
            <td> <strong><?php echo $virtual_account ?></strong></td>
        </tr>
    </table>

    <br /><br />
    <strong>Tata Cara Pembayaran:</strong>
    <br /><br>

    VIA ATM BNI
    <br />
    <ol>
        <li>Masukkan Kartu Anda.</li>
        <li>Pilih Bahasa.</li>
        <li>Masukkan PIN ATM Anda.</li>
        <li>Pilih "Menu Lainnya".</li>
        <li>Pilih "Transfer".</li>
        <li>Pilih "Virtual Account Billing".</li>
        <li>Masukkan nomor rekening tujuan dengan 16 digit Nomor Virtual Account (contoh: <?php echo $virtual_account ?>).</li>
        <li>Konfirmasi, apabila telah sesuai, lanjutkan transaksi.</li>
        <li>Transaksi telah selesai.</li>
    </ol>
    <br />

    iBank Personal
    <br />
    <ol>
        <li>Ketik alamat https://ibank.bni.co.id kemudian klik "Enter".</li>
        <li>Masukkan User ID dan Password.</li>
        <li>Klik menu "TRANSFER" kemudian pilih "TAMBAH REKENING FAVORIT". Jika menggunakan desktop untuk menambah rekening, pada menu "Transaksi" lalu pilih "Info & Administrasi Transfer" kemudian "Atur Rekening Tujuan" lalu "Tambah Rekening Tujuan".</li>
        <li>Masukkan nomor Virtual Account sebagai nomor rekening tujuan (contoh: <?php echo $virtual_account ?>).</li>
        <li>Masukkan Kode Otentikasi Token. Nomor rekening tujuan berhasil ditambahkan.</li>
        <li>Kembali ke menu "TRANSFER". Pilih "TRANSFER ANTAR REKENING BNI", kemudian pilih rekening tujuan. Sistem akan mengkonfirmasi nama pemilik Virtual Account dan nominal transfer.</li>
        <li>Lalu masukkan kode otentikasi token.</li>
        <li>Transfer Anda Telah Berhasil.</li>
    </ol>
    <br />

    Mobile Banking
    <br />
    <ol>
        <li>Buka aplikasi Mobile Banking BNI</li>
        <li>Pilih menu Transfer</li>
        <li>Pilih menu Virtual Account Billing</li>
        <li>Masukkan nomor rekening tujuan denan 16 digit Nomor Virtual Account (contoh: <?php echo $virtual_account ?>)</li>
        <li>Sistem akan secara otomatis menampilkan nominal transfer dan nama sesuai tagihan atau kewajiban Anda</li>
        <li>Pilih "Proses" kemudian "Setuju"</li>
        <li>Masukan password transaksi</li>
        <li>Transaksi berhasil</li>
    </ol>
    <br />

    SMS Banking
    <br />
    <ol>
        <li>Buka aplikasi SMS Banking BNI</li>
        <li>Pilih menu Transfer</li>
        <li>Pilih menu Trf rekening BNI</li>
        <li>Masukkan nomor rekening tujuan dengan 16 digit Nomor Virtual Account (contoh: <?php echo $virtual_account ?>).</li>
        <li>Masukkan nominal transfer sesuai tagihan atau kewajiban Anda. Nominal yang berbeda tidak dapat diproses.</li>
        <li>Pilih &ldquo;Proses&rdquo; kemudian &ldquo;Setuju&rdquo;</li>
        <li>Reply sms dengan ketik pin sesuai perintah</li>
        <li>Transaksi Berhasil</li>
    </ol>
    Atau Dapat juga langsung mengetik sms dengan format:&nbsp;<br> <strong><span>TRF[SPASI]NomorVA[SPASI]NOMINAL</strong>&nbsp;<br> dan kemudian kirim ke 3346&nbsp;<br> Contoh : TRF <?php echo $virtual_account ?> <?php echo $totalKewajiban + $biayaTransaksi - $discountAmount ?></span>
    <br /><br />    

    ATM Bersama
    <br />
    <ol>
        <li>Masukkan kartu ke mesin ATM Bersama.</li>
        <li>Pilih "Transaksi Lainnya".</li>
        <li>Pilih menu "Transfer".</li>
        <li>Pilih "Transfer ke Bank Lain".</li>
        <li>Masukkan kode bank BNI (009) dan 16 Digit Nomor Virtual Account (contoh: <?php echo $virtual_account ?>).</li>
        <li>Masukkan nominal transfer sesuai tagihan atau kewajiban Anda. Nominal yang berbeda tidak dapat diproses.</li>
        <li>Konfirmasi rincian Anda akan tampil di layar, cek dan tekan 'Ya' untuk melanjutkan.</li>
        <li>Transaksi Berhasil.</li>
    </ol>
    <br />

    Transfer Dari Bank Lain
    <br />
    <ol>
        <li>Pilih menu "Transfer antar bank" atau "Transfer online antarbank".</li>
        <li>Masukkan kode bank BNI (009) atau pilih bank yang dituju yaitu BNI.</li>
        <li>Masukan 16 Digit Nomor Virtual Account pada kolom rekening tujuan, (contoh: <?php echo $virtual_account ?>).</li>
        <li>Masukkan nominal transfer sesuai tagihan atau kewajiban Anda. Nominal yang berbeda tidak dapat diproses.</li>
        <li>Masukkan jumlah pembayaran : <?php echo $totalKewajiban + $biayaTransaksi - $discountAmount ?>.</li>
        <li>Konfirmasi rincian Anda akan tampil di layar, cek dan apabila sudah sesuai silahkan lanjutkan transaksi sampai dengan selesai.</li>
        <li>Transaksi Berhasil.</li>
    </ol>
    <br /><br />

    Terima Kasih
<?php endif; ?>

<?php if ($payment == 'WALLET' || $payment == 'GCASH') : ?>
    Hi, <?php echo $namaNasabah ?> 
    <br></br><br>
    Selamat Top Up Tabungan Emas Berhasil.

    <br /><br />
    <table border="0">                                                       
        <tr>
            <td>Metode Pembayaran</td>
            <td>: <strong><?php echo $payment; ?></strong></td>
        </tr>
        <tr>
            <td>Tanggal</td>
            <td>: <strong><?php $tglTrx = new DateTime($tglTransaksi);
            echo $tglTrx->format('d/M/Y'); ?></strong></td>
        </tr>
        <tr>
            <td>Referensi</th>
            <td>: <strong><?php echo $id_transaksi ?></strong></td>
        </tr>    
        <tr>
            <td>No Rekening</td>
            <td>: <strong><?php echo $norek == '' ? $no_rekening : $norek; ?></strong></td>
        </tr> 
        <tr>
            <td>Nama Nasabah</td>
            <td>: <strong><?php echo $namaNasabah ?></strong></td>
        </tr>
        <tr>
            <td>CIF</td>
            <td>: <strong><?php echo $cif != "" ? $cif : '-'; ?></strong></td>
        </tr>
        <tr>
            <td>Harga/<?php echo $satuan ?>gr</td>
            <td>: <strong>Rp. <?php echo number_format($harga, 0, ",", ".") ?></strong></td>
        </tr>
        <tr>
            <td>Top Up</td>
            <td>: <strong><?php echo number_format($gram, 4, ",", ".") ?> gram</strong></td>
        </tr>
        <tr>
            <td>Nominal</td>
            <td>: <strong>Rp. <?php echo number_format($amount, 0, ",", ".") ?></strong></td>
        </tr>
        <tr>
            <td>Biaya</td>
            <td>: <strong>Rp. <?php echo number_format($surcharge, 0, ",", ".") ?></strong></td>
        </tr>
        <tr>
            <td>Biaya Channel</td>
            <td>: <strong>Rp. <?php echo number_format($biayaTransaksi, 0, ",", ".") ?></strong></td>
        </tr>
        <?php
        if ($promoCode != '') {
            echo "<tr>";
            echo "<td>Promo ";
            echo $promoCode;
            echo "</td>";
            echo "<td>:</td>";
            echo "<td>Rp <strong>";
            echo number_format($promoAmount, 0, ',', '.');
            echo "</strong></td>";
            echo "</tr>";
        }
        ?>
        <!-- <tr>
            <td>Promo <?= $promoCode ?></td>
            <td>:</td>
            <td>Rp <strong><?php echo number_format($promoAmount, 0, ',', '.'); ?></strong></td>
        </tr> -->
        <tr>
            <td>Total</td>
            <td>: <strong>Rp. <?php echo number_format($totalKewajiban + $biayaTransaksi - $discountAmount, 0, ",", ".") ?></strong></td>
        </tr>
    </table>

    <br /><br />
    Terima kasih

<?php endif; ?>

<?php if ($payment == 'VA_BCA') : ?>
    Hi, <?php echo $namaNasabahUser ?> 
    <br /></br /><br />
    Terima kasih telah melakukan pengajuan Pembelian Tabungan Emas Pegadaian.
    <br /><br /><br />
    Silakan melakukan pembayaran untuk melanjutkan transaksi Anda.
    <br /><br />

    <table class="table table-responsive">
        <tr>
            <td>Jenis Transaksi</td>
            <td>:</td>
            <td> <strong>Top Up Tabungan Emas</strong></td>
        </tr>
        <tr>
            <td>No Rekening Tabungan Emas</td>
            <td>:</td>
            <td> <strong><?php echo $norek == '' ? $no_rekening : $norek; ?></strong></td>
        </tr>
        <tr>
            <td>Referensi</td>
            <td>:</td>
            <td> <strong><?php echo $id_transaksi; ?></strong></td>
        </tr>
        <tr>
            <td>Nama Nasabah</td>
            <td>:</td>
            <td> <strong><?php echo $namaNasabah; ?></strong></td>
        </tr>
        <tr>
            <td>CIF</td>
            <td>:</td>
            <td> <strong><?php echo $cif != "" ? $cif : "-"; ?></strong></td>
        </tr>
        <tr>
            <td>Batas Waktu Pembayaran </td>
            <td>:</td>
            <td> <strong><?php
                    $date = new DateTime($tanggal_expired);
                    echo $date->format('d/M/Y H:i:s');
            ?></strong></td>
        </tr>
        <tr>
            <td>Biaya Channel</td>
            <td>:</td>    
            <td> <strong>Rp <?php echo number_format($biayaTransaksi, 0, ",", ".") ?></strong></td>
        </tr>
        <?php
        if ($promoCode != '') {
            echo "<tr>";
            echo "<td>Promo ";
            echo $promoCode;
            echo "</td>";
            echo "<td>:</td>";
            echo "<td>Rp <strong>";
            echo number_format($promoAmount, 0, ',', '.');
            echo "</strong></td>";
            echo "</tr>";
        }
        ?>
        <!-- <tr>
            <td>Promo <?= $promoCode ?></td>
            <td>:</td>
            <td>Rp <strong><?php echo number_format($promoAmount, 0, ',', '.'); ?></strong></td>
        </tr> -->
        <tr>
            <td>Nominal Pembayaran</td>
            <td>:</td>    
            <td> <strong>Rp <?php echo number_format($totalKewajiban + $biayaTransaksi - $discountAmount, 0, ",", ".") ?></strong></td>
        </tr>
        <tr>
            <td>Metode Pembayaran</td>
            <td>:</td>
            <td> <strong>Virtual Account BCA</strong></td>
        </tr>
        <tr>
            <td>Rekening Tujuan</td>
            <td>:</td>
            <td> <strong><?php echo $virtual_account ?></strong></td>
        </tr>
    </table>

    <br /><br />
    <strong>Tata Cara Pembayaran:</strong>
    <br /><br>

     Via ATM BCA
    <br />
    <ol>
        <li>Masukkan Kartu ATM BCA & PIN.</li>
        <li>Pilih menu Transaksi Lainnya &gt; Transfer &gt; ke Rekening BCA Virtual Account.</li>
        <li>Masukkan Nomor Billing Anda (Contoh: <?php echo $virtual_account; ?>).</li>
        <li>Di halaman konfirmasi, pastikan detil pembayaran sudah sesuai seperti No VA, Nama, Perus/Produk dan Total Tagihan.</li>
        <li>Pastikan nama Anda dan Total Tagihan benar.</li>
        <li>Jika sudah benar, klik Ya.</li>
        <li>Simpan struk transaksi sebagai bukti pembayaran.</li>
    </ol>
    <br />

    Via mobile banking BCA (m-BCA)

    <br />
    <ol>
        <li>Lakukan log in pada aplikasi BCA Mobile.</li>
        <li>Pilih menu m-BCA, kemudian masukkan kode akses m-BCA.</li>
        <li>Pilih m-Transfer &gt; BCA Virtual Account.</li>
        <li>Pilih dari Daftar Transfer, atau masukkan Nomor Billing Anda (Contoh: <?php echo $virtual_account; ?>).</li>
        <li>Pastikan nama Anda dan Total Tagihan sudah benar.</li>
        <li>Kemudian klik Ok dan masukkan pin m-BCA.</li>
        <li>Pembayaran selesai. Simpan notifikasi yang muncul sebagai bukti pembayaran.</li>
    </ol>
    <br />

    Via KlikBCA Individual
    <br />
    <ol>
        <li>Lakukan log in pada aplikasi Klik BCA Individual (https://ibank.klikbca.com/).</li>
        <li>Masukkan User ID dan PIN.</li>
        <li>Pilih Transfer Dana &gt; Transfer ke BCA Virtual Account.</li>
        <li>Masukkan Nomor Billing Anda (Contoh: <?php echo $virtual_account; ?>).</li>
        <li>Pastikan nama Anda dan Total Tagihan benar.</li>
        <li>Jika sudah benar, klik Lanjutkan.</li>
        <li>Cetak nomor referensi sebagai bukti transaksi Anda.</li>
    </ol>
    <br /><br />

    Terima Kasih
<?php endif; ?>

<?php if ($payment == 'VA_MANDIRI') : ?>
    Hi, <?php echo $namaNasabahUser ?> 
    <br /></br /><br />
    Terima kasih telah melakukan pengajuan Pembelian Tabungan Emas Pegadaian.
    <br /><br /><br />
    Silakan melakukan pembayaran untuk melanjutkan transaksi Anda.
    <br /><br />

    <table class="table table-responsive">
        <tr>
            <td>Jenis Transaksi</td>
            <td>:</td>
            <td> <strong>Top Up Tabungan Emas</strong></td>
        </tr>
        <tr>
            <td>No Rekening Tabungan Emas</td>
            <td>:</td>
            <td> <strong><?php echo $norek == '' ? $no_rekening : $norek; ?></strong></td>
        </tr>
        <tr>
            <td>Referensi</td>
            <td>:</td>
            <td> <strong><?php echo $id_transaksi; ?></strong></td>
        </tr>
        <tr>
            <td>Nama Nasabah</td>
            <td>:</td>
            <td> <strong><?php echo $namaNasabah; ?></strong></td>
        </tr>
        <tr>
            <td>CIF</td>
            <td>:</td>
            <td> <strong><?php echo $cif != "" ? $cif : "-"; ?></strong></td>
        </tr>
        <tr>
            <td>Batas Waktu Pembayaran </td>
            <td>:</td>
            <td> <strong><?php
                    $date = new DateTime($tanggal_expired);
                    echo $date->format('d/M/Y H:i:s');
            ?></strong></td>
        </tr>
        <tr>
            <td>Biaya Channel</td>
            <td>:</td>    
            <td> <strong>Rp <?php echo number_format($biayaTransaksi, 0, ",", ".") ?></strong></td>
        </tr>
        <?php
        if ($promoCode != '') {
            echo "<tr>";
            echo "<td>Promo ";
            echo $promoCode;
            echo "</td>";
            echo "<td>:</td>";
            echo "<td>Rp <strong>";
            echo number_format($promoAmount, 0, ',', '.');
            echo "</strong></td>";
            echo "</tr>";
        }
        ?>
        <!-- <tr>
            <td>Promo <?= $promoCode ?></td>
            <td>:</td>
            <td>Rp <strong><?php echo number_format($promoAmount, 0, ',', '.'); ?></strong></td>
        </tr> -->
        <tr>
            <td>Nominal Pembayaran</td>
            <td>:</td>    
            <td> <strong>Rp <?php echo number_format($totalKewajiban + $biayaTransaksi - $discountAmount, 0, ",", ".") ?></strong></td>
        </tr>
        <tr>
            <td>Metode Pembayaran</td>
            <td>:</td>
            <td> <strong>Virtual Account Mandiri</strong></td>
        </tr>
        <tr>
            <td>Rekening Tujuan</td>
            <td>:</td>
            <td> <strong><?php echo $virtual_account ?></strong></td>
        </tr>
    </table>

    <br /><br />
    <strong>Tata Cara Pembayaran:</strong>
    <br /><br>

    Via ATM Bank Mandiri

    <br />
    <ol>
    <li>Masukkan kartu ATM dan Pin</li>
    <li>Pilih Menu "Bayar/Beli"</li>
    <li>Pilih menu "Lainnya", hingga menemukan menu "Multipayment"</li>
    <li>Masukkan kode biller Pegadaian Mikro/Digital 50025, lalu pilih Benar</li>
    <li>Masukkan "Nomor Virtual Account" Pegadaian (<?php echo $virtual_account; ?>), lalu pilih tombol Benar</li>
    <li>Masukkan Angka "1 untuk memilih tagihan, lalu pilih tombol Ya</li>
    <li>Akan muncul konfirmasi pembayaran, lalu pilih tombol Ya</li>
    <li>Simpan struk sebagai bukti pembayaran anda</li>
    </ol>
    <br />

    Via Internet Banking atau Mandiri Online

    <br />
    <ol>
    <li>Login Mandiri Online dengan memasukkan username dan password</li>
    <li>Pilih menu "Pembayaran"</li>
    <li>Pilih menu "Multipayment"</li>
    <li>Pilih penyedia jasa "Pegadaian Mikro/Digital"</li>
    <li>Masukkan "Nomor Virtual Account" (<?php echo $virtual_account; ?>) dan "Nominal" yang akan dibayarkan , lalu pilih Lanjut</li>
    <li>setelah muncul tagihan, pilih Konfirmasi</li>
    <li>Masukkan PIN/ challange code token</li>
    <li>Transaksi selesai, simpan bukti bayar anda</li>
    </ol>
    <br /><br />

    Terima Kasih
<?php endif; ?>

<?php if ($payment == 'VA_BRI') : ?>
    Hi, <?php echo $namaNasabahUser ?> 
    <br /></br /><br />
    Terima kasih telah melakukan pengajuan Pembelian Tabungan Emas Pegadaian.
    <br /><br /><br />
    Silakan melakukan pembayaran untuk melanjutkan transaksi Anda.
    <br /><br />

    <table class="table table-responsive">
        <tr>
            <td>Jenis Transaksi</td>
            <td>:</td>
            <td> <strong>Top Up Tabungan Emas</strong></td>
        </tr>
        <tr>
            <td>No Rekening Tabungan Emas</td>
            <td>:</td>
            <td> <strong><?php echo $norek == '' ? $no_rekening : $norek; ?></strong></td>
        </tr>
        <tr>
            <td>Referensi</td>
            <td>:</td>
            <td> <strong><?php echo $id_transaksi; ?></strong></td>
        </tr>
        <tr>
            <td>Nama Nasabah</td>
            <td>:</td>
            <td> <strong><?php echo $namaNasabah; ?></strong></td>
        </tr>
        <tr>
            <td>CIF</td>
            <td>:</td>
            <td> <strong><?php echo $cif != "" ? $cif : "-"; ?></strong></td>
        </tr>
        <tr>
            <td>Batas Waktu Pembayaran </td>
            <td>:</td>
            <td> <strong><?php
                    $date = new DateTime($tanggal_expired);
                    echo $date->format('d/M/Y H:i:s');
            ?></strong></td>
        </tr>
        <tr>
            <td>Biaya Channel</td>
            <td>:</td>    
            <td> <strong>Rp <?php echo number_format($biayaTransaksi, 0, ",", ".") ?></strong></td>
        </tr>
        <?php
        if ($promoCode != '') {
            echo "<tr>";
            echo "<td>Promo ";
            echo $promoCode;
            echo "</td>";
            echo "<td>:</td>";
            echo "<td>Rp <strong>";
            echo number_format($promoAmount, 0, ',', '.');
            echo "</strong></td>";
            echo "</tr>";
        }
        ?>
        <!-- <tr>
            <td>Promo <?= $promoCode ?></td>
            <td>:</td>
            <td>Rp <strong><?php echo number_format($promoAmount, 0, ',', '.'); ?></strong></td>
        </tr> -->
        <tr>
            <td>Nominal Pembayaran</td>
            <td>:</td>    
            <td> <strong>Rp <?php echo number_format($totalKewajiban + $biayaTransaksi - $discountAmount, 0, ",", ".") ?></strong></td>
        </tr>
        <tr>
            <td>Metode Pembayaran</td>
            <td>:</td>
            <td> <strong>Virtual Account BRI</strong></td>
        </tr>
        <tr>
            <td>Rekening Tujuan</td>
            <td>:</td>
            <td> <strong><?php echo $virtual_account ?></strong></td>
        </tr>
    </table>

    <br /><br />
    <strong>Tata Cara Pembayaran:</strong>
    <br /><br>

    Via ATM BANK BRI

    <br />
    <ol>
        <li>Masukan Kartu ATM dan PIN</li>
        <li>Pilih Menu "Transaksi Lain"</li>
        <li>Pilih menu "Pembayaran", hingga menemukan menu "Pegadaian"</li>
        <li>Pilih Menu "Pembayaran Angsuran"</li>
        <li>Masukan "Nomor Virtual Account" Pegadaian, lalu pilih tombol Benar</li>
        <li>Akan muncul konfirmasi pembayaran, lalu pilih tombol Ya</li>
        <li>Simpan struk sebagai bukti pembayaran Anda</li>
    </ol>
    <br />

    Via BRI Internet Banking

    <br />
    <ol>
        <li>Login ke Internet Banking BRI Anda</li>
        <li>Pilih Menu "Pembayaran (Tagihan)"</li>
        <li>Pilih Menu "Multipayment"</li>
        <li>Pilih Jenis Tagihan "Pegadaian"</li>
        <li>Pilih Jenis Tagihan "Tagihan Pegadaian"</li>
        <li>Masukan "Nomor Virtual Account"</li>
        <li>Setelah Muncul Tagihan Masukan "Password" dan "mToken"</li>
        <li>Pilih tombol "Kirim"</li>
        <li>Transaksi selesai, simpan bukti bayar Anda.</li>
    </ol>
    <br /><br />

    Terima Kasih
<?php endif; ?>

<?php if ($payment == 'VA_PERMATA') : ?>
    Hi, <?php echo $namaNasabahUser ?> 
    <br /></br /><br />
    Terima kasih telah melakukan pengajuan Pembelian Tabungan Emas Pegadaian.
    <br /><br /><br />
    Silakan melakukan pembayaran untuk melanjutkan transaksi Anda.
    <br /><br />

    <table class="table table-responsive">
        <tr>
            <td>Jenis Transaksi</td>
            <td>:</td>
            <td> <strong>Top Up Tabungan Emas</strong></td>
        </tr>
        <tr>
            <td>No Rekening Tabungan Emas</td>
            <td>:</td>
            <td> <strong><?php echo $norek == '' ? $no_rekening : $norek; ?></strong></td>
        </tr>
        <tr>
            <td>Referensi</td>
            <td>:</td>
            <td> <strong><?php echo $id_transaksi; ?></strong></td>
        </tr>
        <tr>
            <td>Nama Nasabah</td>
            <td>:</td>
            <td> <strong><?php echo $namaNasabah; ?></strong></td>
        </tr>
        <tr>
            <td>CIF</td>
            <td>:</td>
            <td> <strong><?php echo $cif != "" ? $cif : "-"; ?></strong></td>
        </tr>
        <tr>
            <td>Batas Waktu Pembayaran </td>
            <td>:</td>
            <td> <strong><?php
                    $date = new DateTime($tanggal_expired);
                    echo $date->format('d/M/Y H:i:s');
            ?></strong></td>
        </tr>
        <tr>
            <td>Biaya Channel</td>
            <td>:</td>    
            <td> <strong>Rp <?php echo number_format($biayaTransaksi, 0, ",", ".") ?></strong></td>
        </tr>
        <?php
        if ($promoCode != '') {
            echo "<tr>";
            echo "<td>Promo ";
            echo $promoCode;
            echo "</td>";
            echo "<td>:</td>";
            echo "<td>Rp <strong>";
            echo number_format($promoAmount, 0, ',', '.');
            echo "</strong></td>";
            echo "</tr>";
        }
        ?>
        <!-- <tr>
            <td>Promo <?= $promoCode ?></td>
            <td>:</td>
            <td>Rp <strong><?php echo number_format($promoAmount, 0, ',', '.'); ?></strong></td>
        </tr> -->
        <tr>
            <td>Nominal Pembayaran</td>
            <td>:</td>    
            <td> <strong>Rp <?php echo number_format($totalKewajiban + $biayaTransaksi - $discountAmount, 0, ",", ".") ?></strong></td>
        </tr>
        <tr>
            <td>Metode Pembayaran</td>
            <td>:</td>
            <td> <strong>Virtual Account Permata</strong></td>
        </tr>
        <tr>
            <td>Rekening Tujuan</td>
            <td>:</td>
            <td> <strong><?php echo $virtual_account ?></strong></td>
        </tr>
    </table>

    <br /><br />
    <strong>Tata Cara Pembayaran:</strong>
    <br /><br>

    Via ATM PermataBank

    <br />
    <ol>
    <li>Masukkan Kartu ATM PermataBank & PIN.</li>
    <li>Pilih menu Transaksi Lainnya > Pembayaran > Pembayaran lainnya > Virtual Account.</li>
    <li>Silahkan masukkan Kode Prefix (8470) + No Billing anda. Contoh: 8470<?php echo $virtual_account; ?>.</li>
    <li>Pastikan nama Anda dan Total Tagihan benar.</li>
    <li>Klik BENAR jika nama dan nominal pembayaran sudah sesuai.</li>
    <li>Pilih rekening yang akan didebet.</li>
    <li>Tekan YA jika ingin melanjutkan transaksi dan TIDAK jika transaksi selesai.</li>
    <li>Simpan struk transaksi sebagai bukti pembayaran.</li>
    </ol>
    <br />

    Via Internet Banking PermataBank

    <br />
    <ol>
    <li>Silahkan login internet banking kemudian pilih Menu Pembayaran</li>
    <li>Lalu pilih sub menu Pembayaran Tagihan dan klik Virtual Account</li>
    <li>Silahkan pilih rekening anda lalu masukkan Kode Prefix (8470) + No Billing anda (contoh: 8470<?php echo $virtual_account; ?>) lalu klik Lanjut</li>
    <li>Masukkan jumlah nominal tagihan pada bagian Total Pembayaran sesuai dengan invoice yang dikirimkan. Kemudian klik Submit</li>
    <li>Tunggu sebentar hingga anda memperoleh SMS notifikasi yang berisi sebuah KODE. Setelah itu masukkan KODE tersebut</li>
    <li>Proses transfer internet banking telah selesai.</li>
    </ol>
    <br />

    Via Mobile Banking PermataBank

    <br />
    <ol>
    <li>Silahkan login mobile banking yang dimiliki Permata Bank</li>
    <li>Lalu klik Menu Pembayaran Tagihan dan pilih Menu Virtual Account</li>
    <li>Kemudian pilih Tagihan Anda dan pilih Daftar Tagihan Baru</li>
    <li>Silahkan masukkan Kode Prefix (8470) + No Billing. (Contoh: 8470<?php echo $virtual_account; ?>) sebagai Nomor Tagihan. Apabila selesai silahkan klik Konfirmasi.</li>
    <li>Masukkan jumlah nominal tagihan sesuai dengan invoice. Apabila selesai silahkan klik Konfirmasi</li>
    <li>Masukkan Response Code dan klik Konfirmasi apabila telah selesai</li>
    <li>Proses transfer telah selesai.</li>
    </ol>
    <br /><br />

    Terima Kasih
<?php endif; ?>

<?php if ($payment == 'VA_MAYBANK' || $payment == 'FINPAY') : ?>
    Hi, <?php echo $namaNasabah ?> 
    <br></br><br>
    Terima kasih telah melakukan pengajuan Pembelian Tabungan Emas Pegadaian.
    <br /><br /><br />
    Silakan melakukan pembayaran untuk melanjutkan transaksi Anda.
    <br /><br />

    <table class="table table-responsive">
        <tr>
            <td>Jenis Transaksi</td>
            <td>:</td>
            <td> <strong>Top Up Tabungan Emas</strong></td>
        </tr>
        <tr>
            <td>No Rekening Tabungan Emas</td>
            <td>:</td>
            <td> <strong><?php echo $norek == '' ? $no_rekening : $norek; ?></strong></td>
        </tr>
        <tr>
            <td>Referensi</td>
            <td>:</td>
            <td> <strong><?php echo $id_transaksi; ?></strong></td>
        </tr>
        <tr>
            <td>Nama Nasabah</td>
            <td>:</td>
            <td> <strong><?php echo $namaNasabah; ?></strong></td>
        </tr>
        <tr>
            <td>CIF</td>
            <td>:</td>
            <td> <strong><?php echo $cif != "" ? $cif : "-"; ?></strong></td>
        </tr>
        <tr>
            <td>Batas Waktu Pembayaran </td>
            <td>:</td>
            <td> <strong><?php
                    $date = new DateTime($tanggal_expired);
                    echo $date->format('d/M/Y H:i:s');
            ?></strong></td>
        </tr>
        <tr>
            <td>Biaya Channel</td>
            <td>:</td>    
            <td> <strong>Rp <?php echo number_format($biayaTransaksi, 0, ",", ".") ?></strong></td>
        </tr>
        <tr>
            <td>Nominal Pembayaran</td>
            <td>:</td>    
            <td> <strong>Rp <?php echo number_format($totalKewajiban + $biayaTransaksi - $discountAmount, 0, ",", ".") ?></strong></td>
        </tr>
        <tr>
            <td>Metode Pembayaran</td>
            <td>:</td>
            <td> <strong>
                <?php
                if ($payment == 'VA_MAYBANK') {
                    echo 'Virtual Account Maybank';
                } elseif ($payment == 'FINPAY') {
                    echo 'Virtual Account FINPAY';
                }
                ?>
            </strong></td>
        </tr>
        <tr>
            <td>Rekening Tujuan</td>
            <td>:</td>
            <td> <strong><?php echo $va ?></strong></td>
        </tr>
    </table>

    <br /><br />
    <strong>Tata Cara Pembayaran:</strong>
    <br /><br>

    <?php foreach ($redaksi as $value) : ?>
            <strong><?php echo $value['title']?></strong><br>
            <?php echo $value['description']; ?> 
    <?php endforeach; ?>

    Terima Kasih
<?php endif; ?>