<?= $this->load->view('mail/email_template_top', ['title' => $data->title ?? '-'], true) ?>
<?= $data->message ?? '-' ?>

    <br>
    <br>
    <table>
        <tr>
            <td>Riwayat Gagal Login</td>
            <td>:</td>
            <td><strong><?= $data->last_try_login ?? '-' ?> WIB</strong></td>
        </tr>
    </table>
    <br>

    Jika kamu merasa tidak melakukan aktivitas ini, silakan memperbarui password akun Pegadaian Digital dengan
    mengikuti langkah sebagai berikut:

    <ol>
        <li>Buka dan login aplikasi Pegadaian Digital.</li>
        <li>Masuk menu Pengaturan.</li>
        <li>Pilih menu Password & PIN.</li>
        <li>Pilih menu Password.</li>
        <li>Silakan untuk mengisi Password Lama dan mengisi Password Baru.</li>
        <li>Ketuk Proses.</li>
        <li>Password akun Pegadaian Digital kamu telah diperbarui.</li>
    </ol>

    <br>
    Untuk informasi lebih lanjut, hubungi kami melalui call center di 1500 569 atau email ke
    customer.care@pegadaian.co.id

<?= $this->load->view('mail/email_template_bottom', [], true) ?>