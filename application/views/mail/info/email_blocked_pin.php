<?= $this->load->view('mail/email_template_top', ['title' => $data->title ?? '-'], true) ?>
<?= $data->message ?? '-' ?>

    <br>
    <br>
    <table>
        <tr>
            <td>Riwayat Gagal PIN</td>
            <td>:</td>
            <td><strong><?= $data->last_access_time ?? '-'?> WIB</strong></td>
        </tr>
    </table>
    <br>

    Untuk mengaktifkan kembali PIN kamu, silahkan melakukan reset PIN Pegadaian Digital dengan mengikuti langkah sebagai berikut:

    <ol>
        <li>Request OTP ke outlet Pegadaian terdekat.</li>
        <li>Mengisi form pengajuan Reset PIN & menyerahkan KTP.</li>
        <li>Masuk menu Pengaturan di Aplikasi Pegadaian Digital.</li>
        <li>Pilih menu Password & PIN.</li>
        <li>Pilih menu Lupa PIN.</li>
        <li>Silakan masukkan OTP yang dikirimkan oleh outlet Pegadaian.</li>
        <li>Masukkan PIN baru.</li>
        <li>Masukkan ulang PIN baru.</li>
        <li>Ketuk Reset PIN.</li>
        <li>PIN Pegadaian Digital kamu telah diperbarui & aktif kembali.</li>
    </ol>


    <br>
    <br>
    Untuk informasi lebih lanjut, hubungi kami melalui call center di 1500 569 atau email ke
    customer.care@pegadaian.co.id

<?= $this->load->view('mail/email_template_bottom', [], true) ?>