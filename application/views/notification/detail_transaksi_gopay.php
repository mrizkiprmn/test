<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Pegadaian Digital Service</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <style>
            .banner {
                border: 1px solid #0F941A;
                background-color: #5EBD3B;
                padding: 5px;
                text-decoration: none;
                color: white;
            }

            .btn-primary-pegadaian {
                border: 1px solid #0F941A;
                background-color: #5EBD3B;
                color: white;
            }

            .btn-primary-pegadaian:hover{
                background-color: #0F941A;
                color: white !important;
            }
            .greenColor{
                color: #199f56;
                font-weight: 400;
                padding-bottom: 16px;
                border-bottom: 4px solid #dadada;
                margin-bottom: 32px;
            }
            .m-b-34{
                margin-bottom: 34px;
            }
            .width50p{
                width: 50%;
                float: left;
            }
            .width100p{
                width: 100%;
                float: left;
            }
            .container {
                padding-right: 8px;
                padding-left: 8px;
                margin-right: auto;
                margin-left: auto;
            }

        </style>
    </head>
    <body>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default" style="margin-top: 60px">
                    <div class="panel-body">
                        <div>
                            Hi, <?php echo $detailGadai->namaNasabah ?>
                            <br><br>
                            Pembayaran Kurir Pickup Delivery Berhasil
                            <br><br><br>
                            <div>
                                <div class="width100p">Metode</div>
                                <div class="width50p">Pembayaran</div>
                                <div class="width50p">: Go-Pay</div>
                                <br><br>
                                <div class="clearfix"></div>
                                <div class="width50p">Tanggal</div>
                                <div class="width50p">: 
                                    <?php echo $detailGadai->tglBooking ?>
                                </div>
                                <br><br>
                                <div class="clearfix"></div>
                                <div class="width50p">Referensi</div>
                                <div class="width50p" style="word-break: break-all;">: </div>
                                <br><br>
                                <div class="clearfix"></div>
                                <div class="width50p">Nama Nasabah</div>
                                <div class="width50p">: 
                                    <?php echo $detailGadai->namaNasabah ?>
                                </div>
                                <br><br>
                                <div class="clearfix"></div>
                                <div class="width50p">Nominal</div>
                                <div class="width50p">: Rp. 
                                    <?php echo $detailGadai->amountDelivery ?>
                                </div>
                                <div class="clearfix"></div>
                                <div class="width50p">Biaya</div>


                                <div class="width50p">: Rp. 0,-</div>
                                <div class="clearfix"></div>
                                <div class="width50p">Biaya Channel</div>
                                <div class="width50p">: Rp. 0,-</div>
                                <div class="clearfix"></div>
                                <div class="width50p">Total</div>
                                <div class="width50p">: Rp. 
                                    <?php echo $detailGadai->amountDelivery ?>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div>
                                <br><br><br>
                                Terimakasih
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>
</html>