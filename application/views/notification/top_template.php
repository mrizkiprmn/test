<!doctype html>
<html lang="en">
    <head>
        <title>Pegadaian Digital</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">    

        <style>
            @import url(https://fonts.googleapis.com/css?family=Quicksand);

            .pegadaian-heading{
                font-family: 'Quicksand', sans-serif; 
                font-weight: 700; 
                letter-spacing: 1px; 
                font-size: 15px; 
                color: #02A84F; 
                border-bottom: 3px solid #DADADA; 
                padding: 8px 5px;
            }

            body{
                font-family: 'Quicksand', sans-serif;
                font-size: 12px;
            }

            .btn-download-mulia {
                -moz-box-shadow:inset 0px 1px 0px 0px #caefab;
                -webkit-box-shadow:inset 0px 1px 0px 0px #caefab;
                box-shadow:inset 0px 1px 0px 0px #caefab;
                background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #77d42a), color-stop(1, #5cb811));
                background:-moz-linear-gradient(top, #77d42a 5%, #5cb811 100%);
                background:-webkit-linear-gradient(top, #77d42a 5%, #5cb811 100%);
                background:-o-linear-gradient(top, #77d42a 5%, #5cb811 100%);
                background:-ms-linear-gradient(top, #77d42a 5%, #5cb811 100%);
                background:linear-gradient(to bottom, #77d42a 5%, #5cb811 100%);
                filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#77d42a', endColorstr='#5cb811',GradientType=0);
                background-color:#77d42a;
                -moz-border-radius:4px;
                -webkit-border-radius:4px;
                border-radius:4px;
                border:1px solid #268a16;
                display:inline-block;
                cursor:pointer;
                color:#306108 !important;
                font-family:Arial;
                font-size:15px;
                font-weight:bold;
                padding:6px 24px;
                text-decoration:none;
                text-shadow:0px 1px 0px #aade7c;
                text-decoration: none;
            }
            .btn-download-mulia:hover {
                background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #5cb811), color-stop(1, #77d42a));
                background:-moz-linear-gradient(top, #5cb811 5%, #77d42a 100%);
                background:-webkit-linear-gradient(top, #5cb811 5%, #77d42a 100%);
                background:-o-linear-gradient(top, #5cb811 5%, #77d42a 100%);
                background:-ms-linear-gradient(top, #5cb811 5%, #77d42a 100%);
                background:linear-gradient(to bottom, #5cb811 5%, #77d42a 100%);
                filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#5cb811', endColorstr='#77d42a',GradientType=0);
                background-color:#5cb811;
                color: white;
                text-decoration: none;
            }
            .btn-download-mulia:active {
                position:relative;
                top:1px;
            }
            .download-mulia {
                text-align: center
            }

        </style>

    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <img src="<?php echo base_url() ?>assets/pegadaian_logo.png" alt="Customer" class="img-fluid">
                    <br>
                    <h1 class="pegadaian-heading"><?php echo $title ?></h1>
                    <br>
                    
                

