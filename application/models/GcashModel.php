<?php
defined('BASEPATH') or exit('No direct script access allowed');

class GcashModel extends CI_Model
{
    function getVA($userId)
    {
        $va =  $this->db
                    ->select('gcash.*, ref_bank.nama as namaBank')
                    ->select('ref_bank.thumbnail')
                    ->join('ref_bank', 'gcash.kodeBank=ref_bank.kode')
                    ->where('user_AIID', $userId)
                    ->get('gcash')
                    ->result();
        $return = array();
        foreach ($va as $v) {
            $v->thumbnail = $this->config->item('bank_image_path').$v->thumbnail;
            $return[] = $v;
        }

        return $return;
    }

    function addVA($data)
    {
        // Check terlebih dahulu agar VA tidak duplicate
        $cek = $this->db->where([
            'virtualAccount' => $data['virtualAccount'],
            'kodeBank' => $data['kodeBank']
        ])->get('gcash');

        if ($cek->num_rows() == 0) {
            $this->db->insert('gcash', $data);
        } else {
        }
    }

    /**
     * Method ini digunakan untuk menyimpan dan memperbarui data VA gcash di tabel gcash
     * @param $list: merupakan array VA gcash
     * @return void
     */
    function saveOrUpdateVA($list)
    {
        foreach ($list as $data) {
            $is_found = $this->isVAExist(['virtualAccount' => $data->virtualAccount, 'kodeBank' => $data->kodeBank]);

            if ($is_found) {
                $this->db->where('virtualAccount', $data->virtualAccount)->update('gcash', $data);
                continue;
            }

            $data->createdAt = $data->lastUpdate;
            $this->db->insert('gcash', $data);
        }
    }

    /**
     * Method ini digunakan untuk memeriksa apakah no_rekening yang terdapat pada argument ada datanya di tabel gcash
     * @param $gcash: merupakan object yang berisi kondisi pencarian
     * @return boolean
     */
    function isVAExist($data)
    {
        $result = $this->db->select('id')->where($data)->get('gcash');

        return ($result->num_rows() > 0);
    }

    function getVADetails($id)
    {
        $cek = $this->db->where('id', $id)->get('gcash');
        if ($cek->num_rows() > 0) {
            return $cek->row();
        }
        return false;
    }

    function getVaDetailsByVa($va)
    {
        $va =  $this->db
                    ->select('gcash.*, ref_bank.nama as namaBank')
                    ->select('ref_bank.thumbnail')
                    ->join('ref_bank', 'gcash.kodeBank=ref_bank.kode')
                    ->where('virtualAccount', $va)
                    ->get('gcash');
        if ($va->num_rows() > 0) {
            $row = $va->row();
            $row->thumbnail = $this->config->item('bank_image_path').$row->thumbnail;
            return $row;
        }
                    
        return false;
    }

    function addHistory($data)
    {
        $this->db->insert('gcash_history', $data);
    }

    function getHistory($userId, $virtualAccount)
    {
        return $this->db->where([
            'virtualAccount' => $virtualAccount,
            'user_AIID' => $userId
        ])->get('gcash_history')->result();
    }

    function clearVA($userId)
    {
        $this->db->where('user_AIID', $userId)->delete('gcash');
    }

    function getBankParams()
    {
        $bank = $this->db->where('showOnGcash', '1')->get('ref_bank')->result();
        $imagePath = $this->config->item('bank_image_path');
        $return = [];
        foreach ($bank as $b) {
            $b->thumbnail = $imagePath.$b->thumbnail;
            $return[] = $b;
        }
        return $return;
    }

    function addPayment($data)
    {
        $this->db->insert('payment_gcash', $data);
    }

    function getPayment($trxId)
    {
        $cek = $this->db->where('reffSwitching', $trxId)->get('payment_gcash');
        if ($cek->num_rows() > 0) {
            return $cek->row();
        }

        return false;
    }

    function updatePayment($trxId, $data)
    {
        $this->db->where('reffSwitching', $trxId)->update('payment_gcash', $data);
    }

    function isUserGcash($userId, $gcash_id)
    {
        if ($gcash_id == null) {
            return false;
        }
        
        $where = array(
            'id' => $gcash_id,
            'user_AIID' => $userId
        );
        
        $cek = $this->db->where($where)->get('gcash');
       
        if ($cek->num_rows() > 0) {
            return true;
        }
        
        return false;
    }

    function getVaDetailsById($gcash_id)
    {
        $va =  $this->db
                    ->select('gcash.*, ref_bank.nama as namaBank')
                    ->select('ref_bank.thumbnail')
                    ->select('user.nama as customer_name')
                    ->join('ref_bank', 'gcash.kodeBank=ref_bank.kode')
                    ->join('user', 'gcash.user_AIID=user.user_AIID')
                    ->where('gcash.id', $gcash_id)
                    ->get('gcash');

        if ($va->num_rows() > 0) {
            $row = $va->row();
            $row->thumbnail = $this->config->item('bank_image_path').$row->thumbnail;
            
            return $row;
        }
                    
        return false;
    }
}
