<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PaymentModel extends CI_Model
{
    const PRODUCT_CODE = '62';

    function add($data)
    {
        $this->db->insert('payment', $data);
    }

    function check($trxId, $tglPembayaran)
    {
        $cek = $this->db
            ->where(['id_transaksi' => $trxId, 'status' => 0])
            ->get('payment');

        if ($cek->num_rows() > 0) {
            return $cek->row();
        }

        return false;
    }

    function paid($trxId, $tglPembayaran)
    {
        $this->db->where(['id_transaksi' => $trxId, 'status' => 0])->update('payment', ['status' => '1', 'tanggal_pembayaran' => $tglPembayaran]);
    }

    /**
     * Method untuk mendapatkan detail payment berdasarkan id transaksi
     * Pengecekan dilakukan di dua table yaitu payment dan payment_gadai
     *
     * @param type $trxIdzG
     * @return array|bool|mixed
     */
    function getPaymentByTrxId($trxId, $rowArray = false)
    {
        $cek = $this->db
            ->select('payment.*, tabungan_emas.nama as namaNasabah')
            ->join('tabungan_emas', 'tabungan_emas.no_rekening=payment.no_rekening', 'left')
            ->where('payment.id_transaksi', $trxId)
            ->get('payment');

        if ($cek->num_rows() > 0) {
            return $rowArray ? $cek->row_array() : $cek->row();
        }

        return false;
    }

    function getPaymentByReffSwitching($reff_switching)
    {
        $payment = $this->db
            ->select('payment.user_AIID')
            ->select('payment.jenis_transaksi')
            ->select('payment.jenisTransaksi')
            ->select('tabungan_emas.nama as namaNasabah')
            ->select('payment.reffSwitching as reff_switching')
            ->select('payment.payment as payment')
            ->select('payment.tglTransaksi as payment_date')
            ->join('tabungan_emas', 'tabungan_emas.no_rekening=payment.no_rekening', 'left')
            ->where('payment.id_transaksi', $reff_switching)
            ->get('payment');

        if (!empty($payment->num_rows()) && $payment->num_rows() > 0) {
            return $payment->row();
        }

        return null;
    }

    function getPaymentByTrxId2($trxId, $rowArray = false)
    {
        $cek = $this->db
            ->select('payment.*, user.email, user.fcm_token, user.nama, user.no_hp')
            ->join('user', 'user.user_AIID=payment.user_AIID')
            ->where('reffSwitching', $trxId)
            ->get('payment');

        if ($cek->num_rows() > 0) {
            return $rowArray ? $cek->row_array() : $cek->row();
        }

        return false;
    }

    function findByReffSwitching($trxId)
    {
        $cek = $this->db
            ->select('payment.*, user.email, user.fcm_token, user.nama, user.no_hp')
            ->join('user', 'user.user_AIID=payment.user_AIID')
            ->where('reffSwitching', $trxId)
            ->get('payment');

        if ($cek->num_rows() > 0) {
            $cek->row();
        }

        return null;
    }

    /**
     * Method untuk melakukan update payment berdasarkan id transaksi
     * @param String $trxId id transaksi
     * @param array $data field dan data yang diupdate
     */
    function update($trxId, $data)
    {
        $this->db->where('id_transaksi', $trxId)->update('payment', $data);
    }

    function checkInqPayment($table, $reffSwitching)
    {
        if ($reffSwitching == '' || $reffSwitching == null) {
            return '';
        }

        return $this->db->where(['reffSwitching' => $reffSwitching])->get($table)->row();
    }

    function getPaymentMpoByUserId($user_id)
    {
        $whereArray = ['user_AIID' => $user_id, 'payment !=' => '', 'payment !=' => 'GCASH'];

        $cek = $this->db
            ->select('payment.payment, payment.tipe, payment.amount, payment.kodeBankPembayar, payment.jenis_transaksi, payment.kode_produk')
            ->join('ref_bank', 'payment.kodeBankPembayar=ref_bank.kode')
            ->where($whereArray)
            ->order_by('tglTransaksi', 'DESC')
            ->get('payment');

        if ($cek->num_rows() > 0) {
            return $cek->row();
        }

        return false;
    }
}
