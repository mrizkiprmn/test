<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ArrumHajiAgunanModel extends CI_Model
{
    protected $table = 'booking_arrum_haji_agunan';

    function get(array $query = [])
    {
        $table = $this->db;
        if (!empty($query)) {
            $table = $table->where($query);
        }

        $item = $table->get($this->table);

        if ($item && $item->num_rows() > 0) {
            $rows = $item->result();
        }

        return $rows ?? [];
    }

    function find(array $query = [])
    {
        $table = $this->db;

        if (!empty($query)) {
            $table = $table->where($query);
        }

        $item = $table->get($this->table);

        if ($item && $item->num_rows() > 0) {
            $row = $item->row();
        }

        return $row ?? null;
    }

    function getByCodeBooking($code_booking)
    {
        return self::get(['booking_id' => $code_booking]);
    }

    function deleteByCodeBooking($code_booking)
    {
        $this->db->where('booking_id', $code_booking)->delete($this->table);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();

            return false;
        }

        $this->db->trans_commit();
    }
}
