<?php

defined('BASEPATH') or exit('No direct script access allowed');

class HargaEmasModel extends CI_Model
{
    protected $table = 'harga_emas';

    function insert(array $fields)
    {
        $this->db->trans_begin();
        $this->db->insert($this->table, $fields);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();

            return false;
        }

        $this->db->trans_commit();

        return true;
    }

    function find(array $query = [], array $order_by = [])
    {
        $table = $this->db;

        if (!empty($query)) {
            $table = $table->where($query);
        }

        $item = $this->orderBy($table, $order_by['column'] ?? null, $order_by['direction'] ?? null)
            ->get($this->table);

        if ($item && $item->num_rows() > 0) {
            $gold_price = $item->row();
        }

        if (!empty($gold_price)) {
            $updated_time = new DateTime($gold_price->waktu_update);
            $updated_time = $updated_time->format('Y-m-d');
            $gold_price->waktu_update = $updated_time;
        }

        return $gold_price ?? null;
    }

    function updateByID($id, array $fields)
    {
        $this->db->trans_begin();
        $this->db->where('harga_emas_AIID', $id)->update($this->table, $fields);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();

            return false;
        }

        $this->db->trans_commit();

        return true;
    }

    function orderBy($query, $column, $direction)
    {
        if (empty($column) && empty($direction)) {
            return $query;
        }

        return $query->order_by($column, $direction);
    }

    function updateByIdOrInsert($harga_emas_AIID, $field)
    {
        if (!empty($harga_emas_AIID)) {
            return self::updateByID($harga_emas_AIID, $field);
        }

        self::insert($field);
    }
}
