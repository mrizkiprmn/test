<?php

defined('BASEPATH') or exit('No direct script access allowed');

class RekeningBankGadaiModel extends CI_Model
{
    private static $table = 'rekening_bank_gadai';

    public function __construct()
    {
        parent::__construct();
    }
    /**
     * @param array $query
     * @return array
     */
    function get(array $query)
    {
        $model = $this->db->where($query)->get(self::$table);
        if ($model && $model->num_rows() > 0) {
            $models = $model->result();
            foreach ($models as $model) {
                $ref_bank_query = $this->db->where('kode', $model->kode_bank)->get('ref_bank');
                $ref_bank = $ref_bank_query->row() ?? null;
                $item = $model;
                $item->nama_bank = $ref_bank->nama ?? '-';
                $item->thumbnail = $this->config->item('bank_image_path').$ref_bank->thumbnail ?? '-';
                $items[] = $item;
            }
        }

        return $items ?? [];
    }

    function find(array $query)
    {
        $model = $this->db->where($query)->get(self::$table);

        if ($model && $model->num_rows() > 0) {
            $item = $model->row();
        }

        return $item ?? null;
    }

    function insert(array $fields)
    {
        $fields['created_at'] = date('Y-m-d H:i:s');

        $this->db->trans_begin();
        $this->db->insert(self::$table, $fields);
        $this->db->trans_complete();

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return null;
        }

        $this->db->trans_commit();

        return $this->db->insert_id();
    }

    /**
     * @param $user_id
     * @return array
     */
    function getByUserId($user_id)
    {
        return self::get(['user_id' => $user_id]);
    }
}
