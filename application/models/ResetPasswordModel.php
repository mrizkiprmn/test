<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ResetPasswordModel extends CI_Model
{
    function insert($data)
    {
        $this->db->where('user_AIID', $data['user_AIID'])->update('reset_password', ['is_valid'=>'0']);

        $this->db->insert('reset_password', $data);

        return $this->db->insert_id();
    }

    function isValid($token)
    {
        $query = $this->db->where(['token' => $token, 'is_valid' => '1'])->get('reset_password');

        if ($query->num_rows() < 0) {
            return false;
        }

        $item = $query->row();

        if (empty($item->valid_until)) {
            return false;
        }

        $now        = new DateTime("now");
        $validUntil = new DateTime($item->valid_until);

        if ($now < $validUntil) {
            return true;
        }

        return false;
    }

    /**
     * Get user id berdasarkan reset password token
     * @param $token
     */
    function getUserId($token)
    {
        return $this->db->select('user_AIID')->where('token', $token)->where('valid_until >', date('Y-m-d'))->get('reset_password')->row()->user_AIID;
    }

    function off($token)
    {
        $this->db->where('token', $token)->where('valid_until >', date('Y-m-d'))->update('reset_password', array('is_valid'=>'0'));
    }

    function isValidPhone($phone)
    {
        $where = array(
            'no_hp' => $phone,
            'status' => '1'
        );
        $cek = $this->db->where($where)
                        ->get('user');
        if ($cek->num_rows() > 0) {
            return $cek->row();
        } else {
            return false;
        }
    }
}
