<?php

defined('BASEPATH') or exit('No direct script access allowed');

class GpoinModel extends CI_Model
{

    function add($dataArr, $data)
    {
        $sql = "INSERT INTO gpoin (
            refTrx,
            `type`,
            journalAccount,
            `value`,
            voucherCode,
            voucherName,
            discountAmount,
            noHp,
            cif,
            promoCode,
            reffSwitching,
            idPromosi
            )
            values('" . $dataArr['refTrx'] . "',
            '" . $dataArr['rewards']->type . "',
            '" . $dataArr['rewards']->journalAccount . "',
            '" . $dataArr['rewards']->value . "',
            '" . $dataArr['rewards']->voucherCode . "',
            '" . $dataArr['rewards']->voucherName . "',
            '" . $dataArr['rewards']->discountAmount . "',
            '" . $data['phone'] . "',
            '" . $data['cif'] . "',
            '" . $data['promoCode'] . "',
            '" . $dataArr['reffSwitching'] . "',
            '" . $dataArr['idPromosi'] . "'
             )";


        return $this->db->query($sql);
    }

    function add_multi_rewards($data, $dataArr)
    {
        $sql = "INSERT INTO gpoin (
            refTrx,
            `type`,
            journalAccount,
            `value`,
            voucherCode,
            voucherName,
            discountAmount,
            noHp,
            cif,
            promoCode,
            reffSwitching,
            idPromosi
            )
            values('" . $dataArr['refTrx'] . "',
            '" . $dataArr['type'] . "',
            '" . $dataArr['journalAccount'] . "',
            '" . $dataArr['value'] . "',
            '" . $dataArr['voucherCode'] . "',
            '" . $dataArr['voucherName'] . "',
            '" . $dataArr['discountAmount'] . "',
            '" . $data['phone'] . "',
            '" . $dataArr['cif'] . "',
            '" . $data['promoCode'] . "',
            '" . $data['reffSwitching'] . "',
            '" . $dataArr['idPromosi'] . "')";

        return $this->db->query($sql);
    }

    function get_promo($idTransaksi)
    {
        $cek = $this->db->where(['reffSwitching' => $idTransaksi])->get('gpoin');
        if ($cek->num_rows() > 0) {
            return $cek->row();
        } else {
            return '';
        }
    }

    function check($refSwitch)
    {
        $where = ['reffSwitching' => $refSwitch];
        $data = $this->db->where($where)
            ->get('gpoin');
        if ($data->row()) {
            return $data->row();
        } else {
            return '';
        }
    }
    function check_promo_referral($refTrx)
    {
        $where = ['refTrx' => $refTrx];
        $data = $this->db->where($where)
            ->get('gpoin');
        if ($data->row()) {
            return $data->row();
        } else {
            return '';
        }
    }

    function update_trans_id($dataArr, $promoCode)
    {
        $dataUpdate = array(
            // 'reffSwitching' => $dataArr['reffSwitching'],
            'refTrx' => $dataArr['refTrx'],
            'type' => $dataArr['rewards']->type,
            'journalAccount' => $dataArr['rewards']->journalAccount,
            'value' => $dataArr['rewards']->value,
            'voucherCode' => $dataArr['rewards']->voucherCode,
            'voucherName' => $dataArr['rewards']->voucherName,
            'discountAmount' => $dataArr['rewards']->discountAmount,
            // 'noHp' => $dataArr[''],
            // 'cif' => $dataArr[''],
            'promoCode' => $promoCode,
            'idPromosi' => $dataArr['idPromosi']

        );

        $this->db->where('reffSwitching', $dataArr['reffSwitching'])->update('gpoin', $dataUpdate);
    }


    function cancel_gpoin($data)
    {
        $dataUpdate = array(
            'idPromosi' => '',
            'value' => 0,
            'discountAmount' => 0
        );
        $this->db->where('reffSwitching', $data['reffIdSwitching'])->update('gpoin', $dataUpdate);
    }
}
