<?php
defined('BASEPATH') or exit('No direct script access allowed');

class LoggerModel extends CI_Model
{
    function save($method_name, $call_type, $open_data)
    {
        $ip = $this->input->ip_address();
        $time = date("Y-m-d H:i:s");

        $data = array(
            'method_name' => $method_name,
            'ip' => $ip,
            'time' => $time,
            'call_type' => $call_type,
            'open_data' => json_encode($open_data)
        );

        $this->db->insert('logger', $data);
    }
}
