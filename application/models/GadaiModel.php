<?php
defined('BASEPATH') or exit('No direct script access allowed');

class GadaiModel extends CI_Model
{
    const PRODUCT_CODE = '32';

    public function generateBooking($jenisGadai, $startValue)
    {
        // default digit for kode booking
        $digit = 5;
        // default existing kode booking
        $kodeBooking = '';
        $dataGadai = $this->db->query("select kode_booking from {$jenisGadai}
            order by cast(kode_booking as unsigned) DESC limit 1;");

        // when there's kode booking yet
        if (empty($dataGadai)) {
            return $startValue . str_pad($kodeBooking, $digit, 0, STR_PAD_LEFT);
        }

        // get last kode booking
        $kodeBooking = $dataGadai->row()->kode_booking;
        // remove one char prefix
        $kodeBooking = substr($kodeBooking, 1);
        $digit = strlen($kodeBooking) ?: $digit;
        $newKodeBooking = (string) ((int) $kodeBooking + 1);

        // when the digit is increasing
        if (strlen($newKodeBooking) >= $digit) {
            $digit = strlen($newKodeBooking);
        }

        // when all of the digits are the last number
        if (Pegadaian::allCharactersSame($kodeBooking, '9')) {
            $newKodeBooking = '1';
        }

        // get the new kode booking
        $kodeBooking = str_pad($newKodeBooking, $digit, 0, STR_PAD_LEFT);

        return $startValue . $kodeBooking;
    }

    function getPaymentByBookingCode($kode_booking)
    {
        $cek = $this->db->where('bookingCode', $kode_booking)->get('payment_gadai');
        if ($cek->num_rows() > 0) {
            return true;
        }
        return  false;
    }

    function cekKodeBookingLogamMulia($kodeBooking)
    {
        $cek = $this->db->where('kode_booking', $kodeBooking)->get('gadai_logam_mulia');
        if ($cek->num_rows() > 0) {
            return true;
        }
        return  false;
    }

    function cekKodeBookingPerhiasan($kodeBooking)
    {
        $cek = $this->db->where('kode_booking', $kodeBooking)->get('gadai_perhiasan');
        if ($cek->num_rows() > 0) {
            return true;
        }
        return  false;
    }

    function getByKodeBookingLogamMulia($kode_booking)
    {
        $cek = $this->db->select('gadai_logam_mulia.*')
                      ->join('ref_cabang', 'gadai_logam_mulia.kode_outlet=ref_cabang.kode_outlet')
                      ->select('ref_cabang.nama_outlet')
                      ->select('ref_cabang.alamat')
                      ->select('ref_cabang.latitude')
                      ->select('ref_cabang.longitude')
                      ->select('ref_cabang.telepon')
                      ->select('gadai_logam_mulia.status')
                      ->where('kode_booking', $kode_booking)
                      ->get('gadai_logam_mulia');
        return $cek->row();
    }

    function getByKodeBookingPerhiasan($kode_booking)
    {
        $cek = $this->db->select('gadai_perhiasan.*')
                      ->join('ref_cabang', 'gadai_perhiasan.kode_outlet=ref_cabang.kode_outlet')
                      ->select('ref_cabang.nama_outlet')
                      ->select('ref_cabang.alamat')
                      ->select('ref_cabang.latitude')
                      ->select('ref_cabang.longitude')
                      ->select('ref_cabang.telepon')
                      ->select('gadai_perhiasan.status')
                      ->where('kode_booking', $kode_booking)
                      ->get('gadai_perhiasan');
        return $cek->row();
    }

    function getDataGadaiUnCompletedLogamMulia($user_AIID)
    {
        $cek = $this->db->where('user_AIID', $user_AIID)
                        ->where("(gadai_logam_mulia.status != 2) AND (gadai_logam_mulia.status != 4) AND (gadai_logam_mulia.status != 91)  AND (gadai_logam_mulia.status != 93) AND (jenis_layanan=1)")
                        ->join('ref_cabang', 'gadai_logam_mulia.kode_outlet=ref_cabang.kode_outlet')
                        ->select('gadai_logam_mulia.*')
                        ->select('ref_cabang.nama_outlet')
                        ->select('ref_cabang.alamat')
                        ->select('ref_cabang.latitude')
                        ->select('ref_cabang.longitude')
                        ->select('ref_cabang.telepon')
                        ->order_by('tanggal_booking', 'desc')
                        ->limit(1)
                        ->get('gadai_logam_mulia');
        return $cek;
    }

    function getDataGadaiUnCompletedLogamMuliaByKodeBooking($kode_booking)
    {
        $cek = $this->db->where('kode_booking', $kode_booking)

                        ->where("(gadai_logam_mulia.status != 2) AND (gadai_logam_mulia.status != 4) AND (gadai_logam_mulia.status != 91)  AND (gadai_logam_mulia.status != 93) AND (jenis_layanan=1)")
                        ->join('ref_cabang', 'gadai_logam_mulia.kode_outlet=ref_cabang.kode_outlet')
                        ->select('gadai_logam_mulia.*')
                        ->select('ref_cabang.nama_outlet')
                        ->select('ref_cabang.alamat')
                        ->select('ref_cabang.latitude')
                        ->select('ref_cabang.longitude')
                        ->select('ref_cabang.telepon')
                        ->order_by('tanggal_booking', 'desc')
                        ->limit(1)
                        ->get('gadai_logam_mulia');
        return $cek;
    }

    function getDataGadaiUnCompletedPerhiasan($user_AIID)
    {
        $cek = $this->db->where('user_AIID', $user_AIID)
                        ->where("(gadai_perhiasan.status != 2) AND (gadai_perhiasan.status != 4) AND (gadai_perhiasan.status != 91)  AND (gadai_perhiasan.status != 93) AND (jenis_layanan=1)")
                        ->join('ref_cabang', 'gadai_perhiasan.kode_outlet=ref_cabang.kode_outlet')
                        ->select('gadai_perhiasan.*')
                        ->select('ref_cabang.nama_outlet')
                        ->select('ref_cabang.alamat')
                        ->select('ref_cabang.latitude')
                        ->select('ref_cabang.longitude')
                        ->select('ref_cabang.telepon')
                        ->order_by('tanggal_booking', 'desc')
                        ->limit(1)
                        ->get('gadai_perhiasan');
        return $cek;
    }

    function getDataGadaiUnCompletedPerhiasanByKodeBooking($kode_booking)
    {
        $cek = $this->db->where('kode_booking', $kode_booking)
                        ->where("(gadai_perhiasan.status != 2) AND (gadai_perhiasan.status != 4) AND (gadai_perhiasan.status != 91)  AND (gadai_perhiasan.status != 93) AND (jenis_layanan=1)")
                        ->join('ref_cabang', 'gadai_perhiasan.kode_outlet=ref_cabang.kode_outlet')
                        ->select('gadai_perhiasan.*')
                        ->select('ref_cabang.nama_outlet')
                        ->select('ref_cabang.alamat')
                        ->select('ref_cabang.latitude')
                        ->select('ref_cabang.longitude')
                        ->select('ref_cabang.telepon')
                        ->order_by('tanggal_booking', 'desc')
                        ->limit(1)
                        ->get('gadai_perhiasan');
        return $cek;
    }

    function getStatusFcmPerhiasan($kode_booking)
    {
        $res = $this->db
            ->select('status_fcm')
            ->where('kode_booking', $kode_booking)->get('gadai_perhiasan');
        return $res->row();
    }

    function getStatusFcmLogamMulia($kode_booking)
    {
        $res = $this->db
            ->select('status_fcm')
            ->where('kode_booking', $kode_booking)->get('gadai_logam_mulia');
        return $res->row();
    }

    //region Perhiasan
    public function saveGadaiPerhiasan(
        $idUser,
        $kodeBooking,
        $jenis,
        $kadar,
        $beratKotor,
        $beratBersih,
        $kodeOutlet,
        $waktuKedatangan,
        $foto = array(),
        $taksiranAtas,
        $taksiranBawah,
        $jenisPromo
    ) {
        $jsFoto = json_encode($foto);
        $data = array(
            'user_AIID' => $idUser,
            'jenis_perhiasan' => $jenis,
            'kadar_emas' => $kadar,
            'berat_kotor' => $beratKotor,
            'berat_bersih' => $beratBersih,
            'foto_json' => $jsFoto,
            'kode_outlet' => $kodeOutlet,
            'jadwal_kedatangan' => $waktuKedatangan,
            'kode_booking' => $kodeBooking,
            'taksiran_atas' => $taksiranAtas,
            'taksiran_bawah' => $taksiranBawah,
            'jenisPromo' => $jenisPromo
        );

        $this->db->insert('gadai_perhiasan', $data);
    }

    public function saveGadaiPerhiasanGod(
        $idUser,
        $kodeBooking,
        $jenis,
        $kadar,
        $beratBersih,
        $hargaSatuan,
        $kodeOutlet,
        $jadwalKedatangan,
        //$foto = array(),
        $jarak_to_outlet,
        $taksiranAtas,
        $taksiranBawah,
        $jenisPromo,
        $nominal_pengajuan_nasabah,
        $catatan,
        $lokasi_nasabah,
        $idJaminan
    ) {
        //$jsFoto = json_encode($foto);
        $jenis = json_encode($jenis);
        $kadar = json_encode($kadar);
        $beratBersih = json_encode($beratBersih);
        $catatan = json_encode($catatan);
        $hargaSatuan = json_encode($hargaSatuan);
        $data = array(
            'user_AIID' => $idUser,
            'jenis_perhiasan_god' => $jenis,
            'kadar_emas_god' => $kadar,
            'berat_bersih_god' => $beratBersih,
            //'foto_json' => $jsFoto,
            'kode_outlet' => $kodeOutlet,
            'jadwal_kedatangan' => $jadwalKedatangan,
            'kode_booking' => $kodeBooking,
            'jarak_to_outlet' => $jarak_to_outlet,
            'taksiran_atas' => $taksiranAtas,
            'taksiran_bawah' => $taksiranBawah,
            'jenisPromo' => $jenisPromo,
            'nominal_pengajuan_nasabah' => $nominal_pengajuan_nasabah,
            'jenis_layanan' => '1',
            'catatan' => $catatan,
            'lokasi_nasabah' => $lokasi_nasabah,
            'harga_satuan_keping' => $hargaSatuan,
            'id_jaminan' => $idJaminan
        );

        $this->db->insert('gadai_perhiasan', $data);
        // echo $this->db->last_query();
        // exit;
    }
    //add syukron
    public function saveGadaiPerhiasanFotoGod(
        $user_AIID,
        $kodeBooking,
        $foto = array()
    ) {
        $jsFoto = json_encode($foto);

        $data = array(
            'foto_json' => $jsFoto,
            'status' => '6'
        );
        $this->db->where('kode_booking', $kodeBooking)->update('gadai_perhiasan', $data);
    }
    
    function getDataPerhiasans($idUser)
    {
        return $this->db->select('
            gadai_perhiasan_AIID as id,
            jenis_perhiasan,
            jenis_perhiasan_god,
            kadar_emas,
            kadar_emas_god,
            berat_kotor,
            berat_bersih,
            berat_bersih_god,
            foto_json,
            status,
            kode_outlet,
            kode_booking
        ')->where('user_AIID', $idUser)
        ->limit(10, 0)
        ->get('gadai_perhiasan')->result();
    }

    function getDataPerhiasan($idUser, $id)
    {
        return $this->db->select('
                gadai_perhiasan_AIID as id,
                jenis_perhiasan,
                jenis_perhiasan_god,
                kadar_emas,
                kadar_emas_god,
                berat_kotor,
                berat_bersih,
                berat_bersih_god,
                foto_json,
                waktu_update,
                jadwal_kedatangan,
                kode_booking,
                nama_outlet as namaOutlet,
                alamat as alamatOutlet,
                telepon as teleponOutlet,
                gadai_perhiasan.status as status
            ')
            ->select('kel.nama_kelurahan as kelurahanOutlet, kec.nama_kecamatan  as kecamatanOutlet')
            ->select('kab.nama_kabupaten as kabupatenOutlet, prov.nama_provinsi as provinsiOutlet')
            ->where(array(
                'user_AIID' => $idUser,
                'gadai_perhiasan_AIID' => $id
            ))
            ->join('ref_cabang', 'ref_cabang.kode_outlet=gadai_perhiasan.kode_outlet')
            ->join('ref_kelurahan kel', 'ref_cabang.kode_kelurahan=kel.kode_kelurahan')
            ->join('ref_kecamatan kec', 'kel.kode_kecamatan=kec.kode_kecamatan')
            ->join('ref_kabupaten kab', 'kec.kode_kabupaten=kab.kode_kabupaten')
            ->join('ref_provinsi prov', 'kab.kode_provinsi=prov.kode_provinsi')
            ->get('gadai_perhiasan')->row();
    }

    // region Gadai Logam Mulia
    public function simpanLogamMulia(
        $idUser,
        $keping = array(),
        $totalBeratKeping,
        $foto = array(),
        $kodeOutlet,
        $waktuKedatangan,
        $kodeBooking,
        $taksiranAtas,
        $taksiranBawah,
        $jenisPromo
    ) {
        $foto = json_encode($foto);
        $this->db->insert('gadai_logam_mulia', array(
            'user_AIID' => $idUser,
            'keping_json' => json_encode($keping),
            'total_keping' => $totalBeratKeping,
            'foto_json' => $foto,
            'kode_outlet' => $kodeOutlet,
            'jadwal_kedatangan' => $waktuKedatangan,
            'kode_booking' => $kodeBooking,
            'taksiran_atas' => $taksiranAtas,
            'taksiran_bawah' => $taksiranBawah,
            'jenisPromo' => $jenisPromo
        ));

        $id = $this->db->insert_id();
        $this->simpanDetailLogam($id, $keping);
    }

    // region Gadai Logam Mulia
    public function simpanLogamMuliaGod(
        $idUser,
        $keping = array(),
        $jmlkeping = array(),
        $harga_satuan = array(),
        $totalBeratKeping,
        $kodeOutlet,
        $jadwalKedatangan,
        $kodeBooking,
        $taksiranAtas,
        $taksiranBawah,
        $jenisPromo,
        $nominal_pengajuan_nasabah,
        $jarak_to_outlet,
        $lokasi_nasabah,
        $idJaminan = array()
    ) {
        $this->db->insert('gadai_logam_mulia', array(
            'user_AIID' => $idUser,
            'keping_json' => $keping,
            'jumlah_keping' => $jmlkeping,
            'total_keping' => $totalBeratKeping,
            // 'foto_json' => $foto,
            'kode_outlet' => $kodeOutlet,
            'jadwal_kedatangan' => $jadwalKedatangan,
            'kode_booking' => $kodeBooking,
            'taksiran_atas' => $taksiranAtas,
            'taksiran_bawah' => $taksiranBawah,
            'jenisPromo' => $jenisPromo,
            'jenis_layanan' => '1',
            'nominal_pengajuan_nasabah' => $nominal_pengajuan_nasabah,
            'jarak_to_outlet' => $jarak_to_outlet,
            'harga_satuan_keping' => $harga_satuan,
            'lokasi_nasabah' => $lokasi_nasabah,
            'id_jaminan' => $idJaminan
        ));

        $id = $this->db->insert_id();
        $this->simpanDetailLogam($id, json_decode($keping, true));
    }

    public function simpanBarcode(
        $user_AIID,
        $kodeBooking,
        $isi_teks,
        $namafile
    ) {
        $field=array(
            'barcode_image' => $namafile
        );
        $this->db->where('kode_booking', $kodeBooking)->update('gadai_logam_mulia', $field);
    }

    public function simpanBarcodePerhiasan(
        $user_AIID,
        $kodeBooking,
        $isi_teks,
        $namafile
    ) {
        $field=array(
            'barcode_image' => $namafile
        );
        $this->db->where('kode_booking', $kodeBooking)->update('gadai_perhiasan', $field);
    }

    public function simpanAdminNotifikasi(
        $idUser,
        $kodeOutlet
    ) {
        $this->db->insert('admin_notifikasi', array(
            'user_AIID' => $idUser,
            'kode_outlet' => $kodeOutlet
        ));
        $id = $this->db->insert_id();
    }

    public function simpanLogamMuliaFotoGod(
        $user_AIID,
        $kode_booking,
        // $beratKeping,
        // $totalBeratKeping,
        $foto = array()
    ) {
        $foto = json_encode($foto);
        $field = array(
          'user_AIID' => $user_AIID,
          'foto_json' => $foto,
          'status' => '6'
        );
       
        $this->db->where('kode_booking', $kode_booking)->update('gadai_logam_mulia', $field);
    }

    public function simpanDetailLogam($id, $beratKeping)
    {
        $data = array();
        foreach ($beratKeping as $val) {
            $data[] = array(
                'gadai_logam_mulia_AIID' => $id,
                'berat_keping' => $val['berat_keping']
            );
        }

        $this->db->insert_batch('gadai_logam_mulia_detail', $data);
    }

    function getDataLogams($idUser)
    {
        $res = $this->db
            ->select('gadai_logam_mulia_grup_AIID as id,
                keping_json,
                foto_json,
                total_keping,
                status,
                waktu_update,
                kode_outlet,
                kode_booking')
            ->where('user_AIID', $idUser)
            ->limit(10, 0)
            ->get('gadai_logam_mulia');
        return $res->result();
    }

    function getStatusTaksirUlangLogamMuliaByKodeBooking($kodeBooking)
    {
        $res = $this->db
            ->select('status, user_AIID')
            ->where('kode_booking', (int)$kodeBooking)->get('gadai_logam_mulia');
        return $res->row();
    }

    function getStatusTaksirUlangPerhiasanByKodeBooking($kodeBooking)
    {
        $res = $this->db
            ->select('status, user_AIID')
            ->where('kode_booking', (int)$kodeBooking)->get('gadai_perhiasan');
        return $res->row();
    }

    function getStatusLogamMuliaByKodeBooking($kodeBooking)
    {
        $res = $this->db
            ->select('status, respon_nasabah')
            ->where('kode_booking', (int)$kodeBooking)->get('gadai_logam_mulia');
        return $res->row();
    }

    function getStatusPerhiasanByKodeBooking($kodeBooking)
    {
        $res = $this->db
            ->select('status, respon_nasabah')
            ->where('kode_booking', $kodeBooking)->get('gadai_perhiasan');
        return $res->row();
    }
    
    /**
     * Update data user berdasarkan id user
     * @param integer $id
     * @param array $field
     */
    public function updateUser($id, $field = array())
    {
        $this->db->where('user_AIID', $id)->update('user', $field);
    }
    
    function saveRekening(
        $userId,
        $nama,
        $jenis_identitas,
        $no_identitas,
        $tanggal_expired_identitas,
        $tempat_lahir,
        $tanggal_lahir,
        $no_hp,
        $jenis_kelamin,
        $status_kawin,
        $kode_kelurahan,
        $jalan,
        $ibu_kandung,
        $foto_ktp
    ) {
        $flipDateLahir = date('Y-m-d', strtotime($tanggal_lahir));

        $updateProfile = array(
            'no_ktp' => $no_identitas,
            'nama' => $nama,
            'jenis_kelamin' => $jenis_kelamin,
            'tempat_lahir' => $tempat_lahir,
            'tgl_lahir' => $flipDateLahir,
            'id_kelurahan' => $kode_kelurahan,
            'alamat' => $jalan,
            'nama_ibu' => $ibu_kandung,
            'foto_ktp_url' => $foto_ktp
        );
        $this->updateUser($userId, $updateProfile);
        
        $saveGadai = array(
            'nama' => $nama,
            'jenis_identitas' => $jenis_identitas,
            'no_identitas' => $no_identitas,
            'tanggal_expired_identitas' => $tanggal_expired_identitas,
            'tempat_lahir' => $tempat_lahir,
            'tanggal_lahir' => $tanggal_lahir,
            'no_hp' => $no_hp,
            'jenis_kelamin' => $jenis_kelamin,
            'status_kawin' => $status_kawin,
            'kode_kelurahan' => $kode_kelurahan,
            'jalan' => $jalan,
            'ibu_kandung' => $ibu_kandung,
            'foto_ktp' => $foto_ktp,
            'user_AIID' => $userId
        );
        
        $cekRek = $this->db->where('user_AIID', $userId)->get('rekening_gadai');
        if ($cekRek->num_rows() > 0) {
            $this->db->where('user_AIID', $userId)->update('rekening_gadai', $saveGadai);
            return $cekRek->row()->id;
        } else {
            $this->db->insert('rekening_gadai', $saveGadai);
            return $this->db->insert_id();
        }
    }
    
    function isRekeningGadaiExist($no_rekening)
    {
        if ($no_rekening==null) {
            return false;
        }
        
        $cek = $this->db->where('no_rekening', $no_rekening)->get('rekening_gadai');
        if ($cek->num_rows() > 0) {
            return $cek->row();
        }
        
        return false;
    }

    function getDataLogam($idUser, $id)
    {
        return $this->db
            ->select('
                gadai_logam_mulia_grup_AIID as id,
            	keping_json,
            	total_keping,
            	foto_json,
            	waktu_update,
            	jadwal_kedatangan,
            	kode_booking,
            	nama_outlet as namaOutlet,
            	alamat as alamatOutlet,
            	telepon as teleponOutlet,
            	gadai_logam_mulia.status as status
            ')
            ->select('kel.nama_kelurahan as kelurahanOutlet, kec.nama_kecamatan  as kecamatanOutlet')
            ->select('kab.nama_kabupaten as kabupatenOutlet, prov.nama_provinsi as provinsiOutlet')
            ->where(array(
                'user_AIID' => $idUser,
                'gadai_logam_mulia_grup_AIID' => $id
            ))
            ->join('ref_cabang', 'ref_cabang.kode_outlet=gadai_logam_mulia.kode_outlet')
            ->join('ref_kelurahan kel', 'ref_cabang.kode_kelurahan=kel.kode_kelurahan')
            ->join('ref_kecamatan kec', 'kel.kode_kecamatan=kec.kode_kecamatan')
            ->join('ref_kabupaten kab', 'kec.kode_kabupaten=kab.kode_kabupaten')
            ->join('ref_provinsi prov', 'kab.kode_provinsi=prov.kode_provinsi')
            ->get('gadai_logam_mulia')->row();
    }

    function getDataLogamGod($kodeBooking)
    {
        return $this->db
                        ->select('gadai_logam_mulia_grup_AIID as id')
                        ->select('kode_booking as kodeBooking')
                        ->select('status')
                        ->select('keping_json')
                        ->select('jumlah_keping')
                        ->select('tanggal_booking as tglBooking')
                        ->select('taksiran_atas as taksiranAtas')
                        ->select('taksiran_bawah as taksiranBawah')
                        ->select('up as taksiranDisetujui')
                        ->select('nominal_pengajuan_nasabah as pengajuanNasabah')
                        ->select('no_order_gosend as noOrderGosend')
                        ->select('lokasi_nasabah as lokasiNasabah')
                        ->where('kode_booking', $kodeBooking)
                        ->get('gadai_logam_mulia')
                        ->result();
    }

    function getDataPerhiasanGod($kodeBooking)
    {
        return $this->db
                        ->select('gadai_perhiasan_AIID as id')
                        ->select('jenis_perhiasan_god')
                        ->select('berat_bersih_god')
                        ->select('status')
                        ->select('kode_booking as kodeBooking')
                        ->select('tanggal_booking as tglBooking')
                        ->select('taksiran_atas as taksiranAtas')
                        ->select('taksiran_bawah as taksiranBawah')
                        ->select('up as taksiranDisetujui')
                        ->select('no_order_gosend as noOrderGosend')
                        ->select('nominal_pengajuan_nasabah as pengajuanNasabah')
                        ->select('lokasi_nasabah as lokasiNasabah')
                        ->where('kode_booking', $kodeBooking)
                        ->get('gadai_perhiasan')
                        ->result();
    }
    // endregion

    // region Gadai Elektronik
    public function simpanElektronik(
        $idUser,
        $jenis,
        $merk,
        $tipe,
        $kondisiBarang,
        $hargaJual,
        $foto = array(),
        $kodeOutlet,
        $waktuKedatangan,
        $kodeBooking,
        $tipeJaminan,
        $taksiranAtas,
        $taksiranBawah,
        $jenisPromo
    ) {
        $foto = json_encode($foto);
        $this->db->insert('gadai_elektronik', array(
            'user_AIID' => $idUser,
            'jenis' => $jenis,
            'id_merek_elektronik' => $merk,
            'tipe' => $tipe,
            'kondisi_barang' => $kondisiBarang,
            'harga_jual' => $hargaJual,
            'foto_json' => $foto,
            'kode_outlet' => $kodeOutlet,
            'jadwal_kedatangan' => $waktuKedatangan,
            'kode_booking' => $kodeBooking,
            'tipe_jaminan' => $tipeJaminan,
            'taksiran_bawah' => $taksiranBawah,
            'taksiran_atas' => $taksiranAtas,
            'jenisPromo' => $jenisPromo
        ));
    }

    function getDataElektroniks($idUser)
    {
        return $this->db->select('
            	gadai_elektronik_AIID as id,
            	gadai_elektronik.jenis as jenis,
            	ref_elektronik.merek as merek,
            	tipe,
            	gadai_elektronik.jenis,
            	kondisi_barang,
            	harga_jual,
            	foto_json,
            	status,
            	kode_outlet,
            	kode_booking
        ')
            ->where('user_AIID', $idUser)
            ->limit(10, 0)
            ->join('ref_elektronik', 'ref_elektronik.id=gadai_elektronik.id_merek_elektronik')
            ->get('gadai_elektronik')->result();
    }

    function getDataElektronik($idUser, $id)
    {
        return $this->db
            ->select('
                gadai_elektronik_AIID as id,
            	gadai_elektronik.jenis as jenis,
            	tipe,
            	ref_elektronik.merek as merek,
            	foto_json,
            	waktu_update,
            	jadwal_kedatangan,
            	kode_booking,
            	nama_outlet as namaOutlet,
            	alamat as alamatOutlet,
            	telepon as teleponOutlet,
            	gadai_elektronik.status as status
            ')
            ->select('kel.nama_kelurahan as kelurahanOutlet, kec.nama_kecamatan  as kecamatanOutlet')
            ->select('kab.nama_kabupaten as kabupatenOutlet, prov.nama_provinsi as provinsiOutlet')
            ->where(array(
                'user_AIID' => $idUser,
                'gadai_elektronik_AIID' => $id
            ))
            ->join('ref_elektronik', 'ref_elektronik.id=gadai_elektronik.id_merek_elektronik')
            ->join('ref_cabang', 'ref_cabang.kode_outlet=gadai_elektronik.kode_outlet')
            ->join('ref_kelurahan kel', 'ref_cabang.kode_kelurahan=kel.kode_kelurahan')
            ->join('ref_kecamatan kec', 'kel.kode_kecamatan=kec.kode_kecamatan')
            ->join('ref_kabupaten kab', 'kec.kode_kabupaten=kab.kode_kabupaten')
            ->join('ref_provinsi prov', 'kab.kode_provinsi=prov.kode_provinsi')
            ->get('gadai_elektronik')->row();
    }
    // endregion

    // region Gadai Kendaraan
    public function simpanKendaraan(
        $idUser,
        $jenis,
        $merk,
        $type,
        $deskripsi,
        $isiSilinder,
        $tahunPembuatan,
        $hargaJual,
        $foto = array(),
        $kodeOutlet,
        $waktuKedatangan,
        $kodeBooking,
        $no_polisi,
        $no_bpkb,
        $nama_bpkb,
        $no_stnk,
        $no_mesin,
        $taksiran,
        $no_rangka,
        $tipeJaminan,
        $taksiranAtas,
        $taksiranBawah,
        $jenisPromo
    ) {
        $foto = json_encode($foto);
        $this->db->insert('gadai_kendaraan', array(
            'user_AIID' => $idUser,
            'jenis_kendaraan' => $jenis,
            'id_merek_kendaraan' => $merk,
            'tipe' => $type,
            'deskripsi' => $deskripsi,
            'isi_silinder' => $isiSilinder,
            'tahun_pembuatan' => $tahunPembuatan,
            'harga_jual' => $hargaJual,
            'foto_json' => $foto,
            'kode_outlet' => $kodeOutlet,
            'jadwal_kedatangan' => $waktuKedatangan,
            'kode_booking' => $kodeBooking,
            'no_polisi' => $no_polisi,
            'no_bpkb' => $no_bpkb,
            'nama_bpkb' => $nama_bpkb,
            'no_stnk' => $no_stnk,
            'no_mesin' => $no_mesin,
            'taksiran' => $taksiran,
            'no_rangka' => $no_rangka,
            'tipe_jaminan' => $tipeJaminan,
            'taksiran_atas' => $taksiranAtas,
            'taksiran_bawah' => $taksiranBawah,
            'jenisPromo' => $jenisPromo
        ));
    }

    function getDataKendaraans($idUser)
    {
        return $this->db->select('
            	gadai_kendaraan_AIID as id,
            	jenis_kendaraan as jenis,
            	ref_kendaraan.merek as merek,
            	tipe,
            	deskripsi as kondisi_barang,
            	harga_jual,
                no_polisi as noPolisi,
                no_bpkb as noBPKB,
                nama_bpkb as namaBPKB,
                no_stnk as noSTNK,
                no_mesin as noMesin,
                taksiran,
                no_rangka as noRangka,
            	foto_json,
            	status,
            	kode_outlet,
            	kode_booking
        ')
            ->where('user_AIID', $idUser)
            ->limit(10, 0)
            ->join('ref_kendaraan', 'ref_kendaraan.id=gadai_kendaraan.id_merek_kendaraan')
            ->get('gadai_kendaraan')->result();
    }

    function getDataKendaraan($idUser, $id)
    {
        return $this->db->select('
            	gadai_kendaraan_AIID as id,
            	jenis_kendaraan as jenis,
            	ref_kendaraan.merek as merek,
            	tipe,
            	tahun_pembuatan as tahun,
                no_polisi as noPolisi,
                no_bpkb as noBPKB,
                nama_bpkb as namaBPKB,
                no_stnk as noSTNK,
                no_mesin as noMesin,
                taksiran,
                no_rangka as noRangka,
            	foto_json,
            	jadwal_kedatangan,
            	kode_booking,
            	nama_outlet as namaOutlet,
            	alamat as alamatOutlet,
            	telepon as teleponOutlet,
            	gadai_kendaraan.status as status
        ')
            ->select('kel.nama_kelurahan as kelurahanOutlet, kec.nama_kecamatan  as kecamatanOutlet')
            ->select('kab.nama_kabupaten as kabupatenOutlet, prov.nama_provinsi as provinsiOutlet')
            ->where(array(
                'user_AIID' => $idUser,
                'gadai_kendaraan_AIID' => $id
            ))
            ->join('ref_kendaraan', 'ref_kendaraan.id=gadai_kendaraan.id_merek_kendaraan')
            ->join('ref_cabang', 'ref_cabang.kode_outlet=gadai_kendaraan.kode_outlet')
            ->join('ref_kelurahan kel', 'ref_cabang.kode_kelurahan=kel.kode_kelurahan')
            ->join('ref_kecamatan kec', 'kel.kode_kecamatan=kec.kode_kecamatan')
            ->join('ref_kabupaten kab', 'kec.kode_kabupaten=kab.kode_kabupaten')
            ->join('ref_provinsi prov', 'kab.kode_provinsi=prov.kode_provinsi')
            ->get('gadai_kendaraan')->row();
    }
    // endregion

    // region Gadai Smartphone
    public function simpanSmartphone(
        $idUser,
        $merk,
        $type,
        $kondisi,
        $tahunBeli,
        $hargaJual,
        $foto = array(),
        $kodeOutlet,
        $waktuKedatangan,
        $kodeBooking,
        $taksiranAtas,
        $taksiranBawah,
        $jenisPromo
    ) {
        $foto = json_encode($foto);
        $this->db->insert('gadai_handphone', array(
            'user_AIID' => $idUser,
            'id_merek_smartphone' => $merk,
            'tipe' => $type,
            'tahun_beli' => $tahunBeli,
            'kondisi_smartphone' => $kondisi,
            'harga_pasaran' => $hargaJual,
            'foto_json' => $foto,
            'kode_outlet' => $kodeOutlet,
            'jadwal_kedatangan' => $waktuKedatangan,
            'kode_booking' => $kodeBooking,
            'taksiran_atas' => $taksiranAtas,
            'taksiran_bawah' => $taksiranBawah,
            'jenisPromo' => $jenisPromo
        ));
    }

    function getDataSmartphones($idUser)
    {
        return $this->db->select('
            	gadai_handphone_AIID as id,
            	ref_smartphone.merek as merek,
            	tipe,
            	kondisi_smartphone as kondisi_barang,
            	harga_pasaran as harga_jual,
            	foto_json,
            	status,
            	kode_outlet,
            	kode_booking
        ')
            ->where('user_AIID', $idUser)
            ->limit(10, 0)
            ->join('ref_smartphone', 'ref_smartphone.id=gadai_handphone.id_merek_smartphone')
            ->get('gadai_handphone')->result();
    }

    function getDataSmartphone($idUser, $id)
    {
        return $this->db
            ->select('
                gadai_handphone_AIID as id,
            	ref_smartphone.merek as merek,
            	tipe,
            	kondisi_smartphone as kondisi_barang,
            	foto_json,
            	waktu_update,
            	jadwal_kedatangan,
            	kode_booking,
            	nama_outlet as namaOutlet,
            	alamat as alamatOutlet,
            	telepon as teleponOutlet,
            	gadai_handphone.status as status
            ')
            ->select('kel.nama_kelurahan as kelurahanOutlet, kec.nama_kecamatan  as kecamatanOutlet')
            ->select('kab.nama_kabupaten as kabupatenOutlet, prov.nama_provinsi as provinsiOutlet')
            ->where(array(
                'user_AIID' => $idUser,
                'gadai_handphone_AIID' => $id
            ))
            ->join('ref_smartphone', 'ref_smartphone.id=gadai_handphone.id_merek_smartphone')
            ->join('ref_cabang', 'ref_cabang.kode_outlet=gadai_handphone.kode_outlet')
            ->join('ref_kelurahan kel', 'ref_cabang.kode_kelurahan=kel.kode_kelurahan')
            ->join('ref_kecamatan kec', 'kel.kode_kecamatan=kec.kode_kecamatan')
            ->join('ref_kabupaten kab', 'kec.kode_kabupaten=kab.kode_kabupaten')
            ->join('ref_provinsi prov', 'kab.kode_provinsi=prov.kode_provinsi')
            ->order_by('merek', 'asc')
            ->get('gadai_handphone')->row();
    }
    // endregion

    // region Gadai Laptop
    public function simpanLaptop(
        $idUser,
        $merk,
        $type,
        $deskripsi,
        $kapasitasHDD,
        $tahunPembelian,
        $hargaJual,
        $foto = array(),
        $kodeOutlet,
        $waktuKedatangan,
        $kodeBooking,
        $taksiranAtas,
        $taksiranBawah,
        $jenisPromo
    ) {
        $foto = json_encode($foto);
        $this->db->insert('gadai_laptop', array(
            'user_AIID' => $idUser,
            'id_merek_laptop' => $merk,
            'tipe' => $type,
            'deskripsi' => $deskripsi,
            'kapasitas_harddisk' => $kapasitasHDD,
            'tahun_pembelian' => $tahunPembelian,
            'harga_jual' => $hargaJual,
            'foto_json' => $foto,
            'kode_outlet' => $kodeOutlet,
            'jadwal_kedatangan' => $waktuKedatangan,
            'kode_booking' => $kodeBooking,
            'taksiran_atas' => $taksiranAtas,
            'taksiran_bawah' => $taksiranBawah,
            'jenisPromo' => $jenisPromo
        ));
    }

    function getDataLaptops($idUser)
    {
        return $this->db->select('
            	gadai_laptop_AIID as id,
            	ref_laptop.merek as merek,
            	tipe,
            	deskripsi as kondisi_barang,
            	harga_jual,
            	foto_json,
            	status,
            	kode_outlet,
            	kode_booking
        ')
            ->where('user_AIID', $idUser)
            ->limit(10, 0)
            ->join('ref_laptop', 'ref_laptop.id=gadai_laptop.id_merek_laptop')
            ->get('gadai_laptop')->result();
    }

    function getDataLaptop($idUser, $id)
    {
        return $this->db
            ->select('
                gadai_laptop_AIID as id,
            	ref_laptop.merek as merek,
            	tipe,
            	deskripsi as kondisi_barang,
            	foto_json,
            	waktu_update,
            	jadwal_kedatangan,
            	kode_booking,
            	nama_outlet as namaOutlet,
            	alamat as alamatOutlet,
            	telepon as teleponOutlet,
            	gadai_laptop.status as status
            ')
            ->select('kel.nama_kelurahan as kelurahanOutlet, kec.nama_kecamatan  as kecamatanOutlet')
            ->select('kab.nama_kabupaten as kabupatenOutlet, prov.nama_provinsi as provinsiOutlet')
            ->where(array(
                'user_AIID' => $idUser,
                'gadai_laptop_AIID' => $id
            ))
            ->join('ref_laptop', 'ref_laptop.id=gadai_laptop.id_merek_laptop')
            ->join('ref_cabang', 'ref_cabang.kode_outlet=gadai_laptop.kode_outlet')
            ->join('ref_kelurahan kel', 'ref_cabang.kode_kelurahan=kel.kode_kelurahan')
            ->join('ref_kecamatan kec', 'kel.kode_kecamatan=kec.kode_kecamatan')
            ->join('ref_kabupaten kab', 'kec.kode_kabupaten=kab.kode_kabupaten')
            ->join('ref_provinsi prov', 'kab.kode_provinsi=prov.kode_provinsi')
            ->get('gadai_laptop')->row();
    }
    // endregion

    /**
     * Get Presentase Gadai Dari config
     * @param string $jenis Jenis gadai
     * @return array Minimal Value, Maximal Value
     */
    function getPresentase($jenis)
    {
        $resPresentase = $this->db->select('value, value_2')
            ->where('variable', 'gadai_' . $jenis . '_presentase_up')
            ->get('config');

        if ($resPresentase->num_rows() > 0) {
            return array(
                ($resPresentase->row()->value / 100),
                ($resPresentase->row()->value_2 / 100));
        }
        return array(0, 0);
    }

    function simpanResponNasabah($userId, $kodeBookingBatal, $jenisGadai)
    {
        return $this->db->where(array(
              'user_AIID' => $userId,
              'kode_booking' => $kodeBookingBatal))
              ->update('gadai_'.$jenisGadai, array(
              'status' => '94',
              'respon_nasabah' => '0'));
    }

    function simpanApproveNasabah($user_AIID, $kodeBooking, $jenisGadai)
    {
        return $this->db->where(array(
              'user_AIID' => $user_AIID,
              'kode_booking' => $kodeBooking))
              ->update('gadai_'.$jenisGadai, array(
              'respon_nasabah' => '2'));
    }

    /**
     * Get All Data from all gadai
     */
    function getAllGadai($idUser, $page)
    {
        $maxItem = 3 * 6;
        $index = $page * $maxItem;
        return $this->db->query("
        SELECT * FROM ((SELECT 
            user_AIID,
            gadai_perhiasan_AIID as id,
            concat(jenis_perhiasan, \" \", berat_bersih, \" \", 'gram') as judul,
            jenis_perhiasan_god as detail_gadai_jenis,
            berat_bersih_god as detail_gadai_berat,
            'PERHIASAN' as jenis_gadai,
            status,
            jenis_layanan as tipeLayanan,
            waktu_update,
            kode_booking,
            CASE
                WHEN status = 1 THEN \"PickupAndDeliveryService\"
                WHEN status = 2 THEN \"DetailGadai\"
                WHEN status = 4 THEN \"PengambilanBarang\"
                WHEN status = 5 && status_fcm = 0 THEN \"LookingDriver\"
                WHEN status = 5 && status_fcm = 1 THEN \"SerahTerimaBarang\"
                WHEN status = 6 THEN \"DetailTransaksiAfterGopay\"
                WHEN status = 7 && respon_nasabah = 1 THEN \"PengajuanDisetujui\"
                WHEN status = 8 THEN \"LookingDriver\"
                WHEN status = 9 THEN \"DetailTransaksiAfterGopay\"
                WHEN status = 92 THEN \"PengambilanBarang\"
                WHEN status = 94 THEN \"PengambilanBarang\"
                WHEN status = 0 THEN \"PengambilanBarang\"
                WHEN status = 7 && respon_nasabah = 2 && otp = 0 THEN \"VerifyOTP\"
                WHEN status = 7 && respon_nasabah = 2 && otp = 1 THEN \"PilihNoRekeningPenerima\"
                ELSE \"Completed\"
            END AS screenName
        FROM 
            gadai_perhiasan 
        WHERE user_AIID='" . $idUser . "')
        UNION
        (SELECT 
        user_AIID,
            gadai_logam_mulia.gadai_logam_mulia_grup_AIID as id, 
            (SELECT CONCAT(SUM(gadai_logam_mulia_detail.berat_keping), \" gram\") 
                FROM gadai_logam_mulia_detail 
                WHERE gadai_logam_mulia_detail.gadai_logam_mulia_AIID=gadai_logam_mulia.gadai_logam_mulia_grup_AIID 
                GROUP BY gadai_logam_mulia_detail.gadai_logam_mulia_AIID) as judul, 
            jumlah_keping as detail_gadai_jenis,
            keping_json as detail_gadai_berat,
            'LOGAM_MULIA' as jenis_gadai,
            status,
            jenis_layanan as tipeLayanan,
            waktu_update,
            kode_booking,
            CASE
                WHEN status = 1 THEN \"PickupAndDeliveryServiceLM\"
                WHEN status = 2 THEN \"DetailGadaiLM\"
                WHEN status = 4 THEN \"PengambilanBarangLM\"
                WHEN status = 5 && status_fcm = 0 THEN \"LookingDriverLM\"
                WHEN status = 5 && status_fcm = 1 THEN \"SerahTerimaBarangLM\"
                WHEN status = 6 THEN \"DetailTransaksiAfterGopayLM\"
                WHEN status = 7 && respon_nasabah = 1 THEN \"PengajuanDisetujuiLM\"
                WHEN status = 8 THEN \"LookingDriverLM\"
                WHEN status = 9 THEN \"DetailTransaksiAfterGopayLM\"
                WHEN status = 92 THEN \"PengambilanBarangLM\"
                WHEN status = 94 THEN \"PengambilanBarangLM\"
                WHEN status = 0 THEN \"PengambilanBarangLM\"
                WHEN status = 7 && respon_nasabah = 2 && otp = 0 THEN \"VerifyOTPLM\"
                WHEN status = 7 && respon_nasabah = 2 && otp = 1 THEN \"PilihNoRekeningPenerimaLM\"
                ELSE \"Completed\"
            END AS screenName 
        FROM 
            gadai_logam_mulia 
        WHERE user_AIID='" . $idUser . "')
        UNION
        (SELECT 
        user_AIID,
            gadai_handphone_AIID as id, 
            concat(ref_smartphone.merek, ' ', tipe) as judul, 
            concat('') as detail_gadai_jenis,
            concat('') as detail_gadai_berat,
            'HANDPHONE' as jenis_gadai,
            status,
            jenis_layanan as tipeLayanan,
            waktu_update,
            kode_booking,
            CASE
                WHEN status = 2 THEN \"Completed\"
            END AS screenName 
        FROM 
            gadai_handphone 
        INNER JOIN 
            ref_smartphone ON (ref_smartphone.id=gadai_handphone.id_merek_smartphone) 
        WHERE user_AIID='" . $idUser . "')
        UNION
        (SELECT 
        user_AIID,
            gadai_elektronik.gadai_elektronik_AIID as id, 
            concat(gadai_elektronik.tipe, ' ', ref_elektronik.merek) as judul,
            concat('') as detail_gadai_jenis,
            concat('') as detail_gadai_berat,
            'ELEKTRONIK' as jenis_gadai,
            status,
            jenis_layanan as tipeLayanan,
            waktu_update,
            kode_booking,
            CASE
                WHEN status = 2 THEN \"Completed\"
            END AS screenName
        FROM 
            gadai_elektronik 
        INNER JOIN 
            ref_elektronik ON (gadai_elektronik.id_merek_elektronik=ref_elektronik.id)
        WHERE user_AIID='" . $idUser . "')
        UNION
        (SELECT 
        user_AIID,
            gadai_kendaraan_AIID as id,
            concat(ref_kendaraan.merek, ' ', tipe) as judul,
            concat('') as detail_gadai_jenis,
            concat('') as detail_gadai_berat,
            'KENDARAAN' as jenis_gadai,
            status,
            jenis_layanan as tipeLayanan,
            waktu_update,
            kode_booking,
            CASE
                WHEN status = 2 THEN \"Completed\"
            END AS screenName
        FROM 
            gadai_kendaraan 
        INNER JOIN 
            ref_kendaraan ON (ref_kendaraan.id=gadai_kendaraan.id_merek_kendaraan)
        WHERE user_AIID='" . $idUser . "')
        UNION
        (SELECT 
        user_AIID,
            gadai_efek.id,
            'Efek' as judul,
            concat('') as detail_gadai_jenis,
            concat('') as detail_gadai_berat,
            'EFEK' as jenis_gadai,
            concat('') as tipeLayanan,
            status,
            waktu_update,
            kode_booking,
            CASE
                WHEN status = 2 THEN \"Completed\"
            END AS screenName
        FROM 
            gadai_efek 

        WHERE user_AIID='" . $idUser . "')
        UNION
        (SELECT 
        user_AIID,
            gadai_laptop_AIID as id, 
            concat(ref_laptop.merek, ' ', tipe) as judul,
            concat('') as detail_gadai_jenis,
            concat('') as detail_gadai_berat,
            'LAPTOP' as jenis_gadai,
            status,
            jenis_layanan as tipeLayanan,
            waktu_update,
            kode_booking,
            CASE
                WHEN status = 2 THEN \"Completed\"
            END AS screenName
        FROM 
            gadai_laptop 
        INNER JOIN 
            ref_laptop ON (ref_laptop.id=gadai_laptop.id_merek_laptop) 
        WHERE user_AIID='" . $idUser . "'
        ORDER BY waktu_update DESC)) AS gadai ORDER BY waktu_update DESC LIMIT " . $maxItem . " OFFSET " . $index);
    }

    function getTotalGadai($idUser)
    {
        return $this->db->query("
            SELECT SUM(total) as total FROM 
            (
                (SELECT COUNT(*) AS total FROM gadai_elektronik WHERE user_AIID='". $idUser ."') UNION ALL
                (SELECT COUNT(*) AS total FROM gadai_handphone WHERE user_AIID='". $idUser ."') UNION ALL
                (SELECT COUNT(*) AS total FROM gadai_kendaraan WHERE user_AIID='". $idUser ."') UNION ALL
                (SELECT COUNT(*) AS total FROM gadai_logam_mulia WHERE user_AIID='". $idUser ."') UNION ALL
                (SELECT COUNT(*) AS total FROM gadai_perhiasan WHERE user_AIID='". $idUser ."') UNION ALL
                (SELECT COUNT(*) AS total FROM gadai_laptop WHERE user_AIID='". $idUser ."')
            ) AS gadai
        ")->row();
    }

    /// Masters
    function ref_elektronik($tipe, $id)
    {
        if ($tipe) {
            $this->db->where('jenis', $tipe);
        }

        if ($id) {
            $this->db->where('id', $id);
        }
        return $this->db->order_by('merek', 'asc')->get('ref_elektronik')->result();
    }

    function ref_kendaraan($tipe, $id)
    {
        if ($tipe) {
            $this->db->where('jenis', $tipe);
        }

        if ($id) {
            $this->db->where('id', $id);
        }

        return $this->db->order_by('merek', 'asc')->get('ref_kendaraan')->result();
    }

    function ref_laptop($id)
    {
        if ($id) {
            $this->db->where('id', $id);
        }

        return $this->db->get('ref_laptop')->result();
    }

    function ref_smartphone($id)
    {
        if ($id) {
            $this->db->where('id', $id);
        }

        return $this->db->order_by('merek', 'asc')->get('ref_smartphone')->result();
    }
    
    /**
     * Method untuk mendapatkan data gadai berdasarkan kode booking
     * Untuk kecepatan, pengecekan dilakukan di beberapa tabel
     * - gadai_elektronik
     * - gadai_handphone
     * - gadai_kendaraan
     * - gadai_laptop
     * - gadai_logam_mulia
     * - gadai_perhiasan
     * @param string $kodeBooking kode booking gadai
     * @return array|boolean $data jika ditemukan, false jika tidak ditemukan
     */
    function getGadaiByKodeBooking($kodeBooking)
    {
        if ($kodeBooking == null) {
            return false;
        }
        
        
            
        $cekElektronik = $this->db
                                ->select('user.user_AIID, user.nama, user.fcm_token, user.email, gadai_elektronik.*')
                                ->join('user', 'gadai_elektronik.user_AIID=user.user_AIID')
                                ->where('kode_booking', $kodeBooking)->get('gadai_elektronik');
        if ($cekElektronik->num_rows() > 0) {
            return ['type'=>'Elektronik', 'data' => $cekElektronik->row(), 'table' => 'gadai_elektronik'];
        }
        
        $cekHandphone = $this->db
                                ->select('user.user_AIID, user.nama, user.fcm_token, user.email, gadai_handphone.*')
                                ->join('user', 'gadai_handphone.user_AIID=user.user_AIID')
                                ->where('kode_booking', $kodeBooking)->get('gadai_handphone');
        if ($cekHandphone->num_rows() > 0) {
            return ['type'=>'Handphone', 'data' => $cekHandphone->row(), 'table' => 'gadai_handphone'];
        }
        
        $cekKendaraan = $this->db
                                ->select('user.user_AIID, user.nama, user.fcm_token, user.email, gadai_kendaraan.*')
                                ->join('user', 'gadai_kendaraan.user_AIID=user.user_AIID')
                                ->where('kode_booking', $kodeBooking)->get('gadai_kendaraan');
        if ($cekKendaraan->num_rows() > 0) {
            return ['type'=>'Kendaraan', 'data' => $cekKendaraan->row(), 'table' => 'gadai_kendaraan'];
        }
        
        $cekLaptop = $this->db
                            ->select('user.user_AIID, user.nama, user.fcm_token, user.email, gadai_laptop.*')
                            ->join('user', 'gadai_laptop.user_AIID=user.user_AIID')
                            ->where('kode_booking', $kodeBooking)->get('gadai_laptop');
        if ($cekLaptop->num_rows() > 0) {
            return ['type'=>'Laptop', 'data' => $cekLaptop->row(), 'table' => 'gadai_laptop'];
        }
        
        $cekLogamMulia = $this->db
                                ->select('user.user_AIID, 
                                        user.nama,
                                        user.alamat, 
                                        user.fcm_token, 
                                        user.email, 
                                        gadai_logam_mulia.*,
                                        payment_gadai.administrasi,
                                        payment_gadai.asuransi,
                                        payment_gadai.biayaProsesLelang,
                                        payment_gadai.biayaLelang,
                                        payment_gadai.kodeNamaCabang,
                                        payment_gadai.biayaAdmBjpdl,
                                        payment_gadai.biayaBjdplMax,
                                        payment_gadai.namaPinca,
                                        payment_gadai.noID,
                                        payment_gadai.namaNasabah,
                                        payment_gadai.cif,
                                        payment_gadai.rubrik,
                                        ref_cabang.email as email_outlet')
                                ->join('user', 'gadai_logam_mulia.user_AIID=user.user_AIID')
                                ->join('payment_gadai', 'gadai_logam_mulia.kode_booking=payment_gadai.bookingCode', 'LEFT')
                                ->join('ref_cabang', 'gadai_logam_mulia.kode_outlet=ref_cabang.kode_outlet')
                                ->where('kode_booking', $kodeBooking)->get('gadai_logam_mulia');
        if ($cekLogamMulia->num_rows() > 0) {
            return ['type'=>'Logam Mulia', 'data' => $cekLogamMulia->row(), 'table' => 'gadai_logam_mulia'];
        }
        
        $cekPerhiasan = $this->db
                                ->select('user.user_AIID, 
                                        user.nama, 
                                        user.alamat, 
                                        user.fcm_token, 
                                        user.email, 
                                        gadai_perhiasan.*, 
                                        payment_gadai.administrasi,
                                        payment_gadai.asuransi,
                                        payment_gadai.biayaProsesLelang,
                                        payment_gadai.biayaLelang,
                                        payment_gadai.kodeNamaCabang,
                                        payment_gadai.biayaAdmBjpdl,
                                        payment_gadai.biayaBjdplMax,
                                        payment_gadai.namaPinca,
                                        payment_gadai.noID,
                                        payment_gadai.namaNasabah,
                                        payment_gadai.cif,
                                        payment_gadai.rubrik,
                                        ref_cabang.email as email_outlet')
                                ->join('user', 'gadai_perhiasan.user_AIID=user.user_AIID')
                                ->join('payment_gadai', 'gadai_perhiasan.kode_booking=payment_gadai.bookingCode', 'LEFT')
                                ->join('ref_cabang', 'gadai_perhiasan.kode_outlet=ref_cabang.kode_outlet')
                                ->where('kode_booking', $kodeBooking)->get('gadai_perhiasan');
        if ($cekPerhiasan->num_rows() > 0) {
            return ['type'=>'Perhiasan', 'data' => $cekPerhiasan->row(), 'table' => 'gadai_perhiasan'];
        }

        return false;
    }
    
    /**
     * Method untuk mengupdate pengajuan gadai setelah mendapatkan notifikasi dari Core
     * Pegadaian.
     * @param string $kodeBooking
     * @param array $data
     * @return void
     */
    function gadaiDiproses($kodeBooking, $table, $status, $data = null)
    {
        $updateData = null;
        
        if ($status == '1') {
            $updateData = array(
                'no_kredit' => $data->noKredit,
                'tanggal_kredit' => $data->tglKredit,
                'tanggal_jatuh_tempo' => $data->tglJatuhTempo,
                'tanggal_lelang' => $data->tglLelang,
                'up' => $data->up,
                'bunga' => $data->bunga,
                'sewa_modal' => $data->sewaModal,
                'sewa_modal_maksimal' => $data->sewaModalMaksimal,
                'taksiran' => $data->taksiran,
                'status' => '2'
            );
        } else {
            $updateData = array(
                 'status' => '0'
            );
        }


        $this->db->where('kode_booking', $kodeBooking)->update($table, $updateData);
    }

    function gadaiDiprosesGod($user_AIID, $kodeBooking, $table, $status, $data = null)
    {
        $updateData = null;
        
        if ($status == '1') {
            $contentData = array(
                'user_AIID'=>$user_AIID,
                'bookingCode'=> $kodeBooking,
                'administrasi'=> $data->administrasi,
                'asuransi'=> $data->asuransi,
                'biayaProsesLelang'=> $data->biayaProsesLelang,
                'biayaLelang'=> $data->biayaLelang,
                'kodeNamaCabang'=> $data->kodeNamaCabang,
                'biayaAdmBjpdl'=> $data->biayaAdmBjpdl,
                'biayaBjdplMax'=> $data->biayaBjdplMax,
                'namaPinca'=> $data->namaPinca,
                'noID'=> $data->noID,
                'namaNasabah'=> $data->namaNasabah,
                'cif'=> $data->cif,
                'alamat'=> $data->alamat,
                'rubrik'=> $data->rubrik,
                'tglJatuhTempo'=>$data->tglJatuhTempo,
                'tglKredit'=>$data->tglKredit,
                'tglLelang'=>$data->tglLelang,
                'sewaModal'=>$data->sewaModal,
                'up' => $data->up
            );
        } else {
            $updateData = array(
                 'status' => '0'
            );
        }

        //cek ketersediaan kode booking di payment_gadai
        $checkPaymentGadai= $this->getPaymentGadaiByKodeBooking($kodeBooking);
        if ($checkPaymentGadai->count > 0) {
            $this->db->where('bookingCode', $kodeBooking)->update('payment_gadai', $contentData);
        } else {
            $this->db->insert('payment_gadai', $contentData);
        }
    }

    function getPaymentGadaiByKodeBooking($kodeBooking)
    {
        $count = $this->db->select('count(*) as count')->where('bookingCode', $kodeBooking)->get('payment_gadai')->row();
        return $count;
    }

    function getCheckKodeBooking($kode_booking, $table)
    {
        $count = $this->db->select('count(*) as count')->where('kode_booking', $kode_booking)->get($table)->row();
        return $count;
    }

    function getInitialPerhiasan($jenisPerhiasanGod)
    {
        if ($jenisPerhiasanGod=='Kalung') {
            $jenisPerhiasanGod = 'KL';
        } elseif ($jenisPerhiasanGod == 'Gelang') {
            $jenisPerhiasanGod = 'GL';
        } elseif ($jenisPerhiasanGod == 'Cincin') {
            $jenisPerhiasanGod = 'CN';
        } elseif ($jenisPerhiasanGod == 'Giwang') {
            $jenisPerhiasanGod = 'GW';
        } elseif ($jenisPerhiasanGod == 'Anting') {
            $jenisPerhiasanGod = 'AN';
        } elseif ($jenisPerhiasanGod == 'Liontin') {
            $jenisPerhiasanGod = 'LI';
        } else {
            $jenisPerhiasanGod = 'LN';
        }
        return $jenisPerhiasanGod;
    }

    function getDetailGadaiByKodeBooking($kodeBooking)
    {
        if ($kodeBooking==null) {
            return false;
        }

        $selectId = "nama as namaNasabah, jenis_identitas as tipeIdentitas, no_ktp as noIdentitas, tanggal_expired_identitas as tglExpiredId, "
                . "cif, nama_ibu as ibuKandung, tempat_lahir as tempatLahir, tgl_lahir as tglLahir, "
                . "jenis_kelamin as jenisKelamin, no_hp as noHp, kewarganegaraan, status_kawin as statusKawin,"
                . "alamat as jalan, id_kelurahan as idKelurahan";
        
        if ($kodeBooking[0] == "1") {
            $jenis_layanan = $this->db->select('jenis_layanan')
                                        ->where('kode_booking', $kodeBooking)
                                        ->get('gadai_perhiasan')->row();
            if ($jenis_layanan) {
                if ($jenis_layanan->jenis_layanan == 1) {
                    $cek = $this->db->select($selectId)
                                ->select('"Perhiasan" as jaminan')
                                ->select('"KT" as rubrik')
                                ->select('"KTEP" as tipeJaminan')
                                ->select('jenis_perhiasan as jenisPerhiasan')
                                ->select('foto_json as fotoJaminan')
                                ->select('kadar_emas as kadarEmas')
                                ->select('berat_bersih as beratBersih')
                                ->select('berat_kotor as beratKotor')
                                ->select('kode_outlet as kodeOutlet')
                                ->select('FROM_UNIXTIME(jadwal_kedatangan) as  jadwalKedatangan')
                                ->select('kode_booking as kodeBooking')
                                ->select('tanggal_booking as tglBooking')
                                ->select('gadai_perhiasan.status as statusGod')
                                ->select('no_kredit as noKredit')
                                ->select('tanggal_kredit as tglKredit')
                                ->select('up')
                                ->select('taksiran')
                                ->select('taksiran_atas as taksiranAtas')
                                ->select('taksiran_bawah as taksiranBawah')
                                ->select('bunga')
                                ->select('sewa_modal as sewaModal')
                                ->select('sewa_modal_maksimal as sewaModalMaksimal')
                                ->select('(CASE jenisPromo 
                                        WHEN null THEN ""
                                        WHEN "" THEN ""
                                        WHEN "undefined" THEN "" 
                                        ELSE jenisPromo END) as jenisPromo')
                                ->select('jenis_pickup as jenisPickup')
                                ->select('no_order_gosend as kodeDelivery')
                                ->select('amount as amountDelivery')
                                // ->select('kodeBank')
                                // ->select('norekBank')
                                // ->select('customerNameBank')
                                ->select('jenis_layanan as tipeLayanan')
                                ->select('id_order_midtrans as idOrderMidtrans')
                                ->where('kode_booking', $kodeBooking)
                                ->join('user', 'gadai_perhiasan.user_AIID=user.user_AIID')
                                ->get('gadai_perhiasan');
                } else {
                    $cek = $this->db->select($selectId)
                                ->select('"Perhiasan" as jaminan')
                                ->select('"KT" as rubrik')
                                ->select('"KTEP" as tipeJaminan')
                                ->select('jenis_perhiasan as jenisPerhiasan')
                                ->select('foto_json as fotoJaminan')
                                ->select('kadar_emas as kadarEmas')
                                ->select('berat_bersih as beratBersih')
                                ->select('berat_kotor as beratKotor')
                                ->select('kode_outlet as kodeOutlet')
                                ->select('FROM_UNIXTIME(jadwal_kedatangan) as  jadwalKedatangan')
                                ->select('kode_booking as kodeBooking')
                                ->select('tanggal_booking as tglBooking')
                                ->select('gadai_perhiasan.status')
                                ->select('no_kredit as noKredit')
                                ->select('tanggal_kredit as tglKredit')
                                ->select('up')
                                ->select('taksiran')
                                ->select('taksiran_atas as taksiranAtas')
                                ->select('taksiran_bawah as taksiranBawah')
                                ->select('bunga')
                                ->select('sewa_modal as sewaModal')
                                ->select('sewa_modal_maksimal as sewaModalMaksimal')
                                ->select('jenisPromo')
                                ->select('jenis_layanan as tipeLayanan')
                                ->select('id_order_midtrans as idOrderMidtrans')
                                ->where('kode_booking', $kodeBooking)
                                ->join('user', 'gadai_perhiasan.user_AIID=user.user_AIID')
                                ->get('gadai_perhiasan');
                }
            
                if ($cek->num_rows() > 0) {
                    $r = $cek->row();
                    $fotoList = json_decode($r->fotoJaminan);
                    $resFotoList = array();
                    $baseUrl = $this->config->item('base_url');
                    if (!empty($fotoList)) {
                        foreach ($fotoList as $f) {
                            $resFotoList[] = $baseUrl.'uploaded/user/gadai/'.$f->nama_foto;
                        }
                    }
                    $r->fotoJaminan = $resFotoList;

                    if ($r->tipeLayanan == 1) {
                        $listJaminan = $this->db->select('jenis_perhiasan_god as jenisPerhiasanGod')
                                                ->select('berat_bersih_god as beratBersihGod')
                                                ->select('kadar_emas_god as kadarEmasGod')
                                                ->select('harga_satuan_keping as hargaTaksiran')
                                                ->select('catatan')
                                                ->select('id_jaminan as idJaminan')
                                                ->where('kode_booking', $kodeBooking)
                                                ->get('gadai_perhiasan')->row();
                        // begin list item perhiasan
                        $jenisPerhiasanGod = json_decode($listJaminan->jenisPerhiasanGod);
                        $kadarEmasGod = json_decode($listJaminan->kadarEmasGod);
                        $beratBersihGod = json_decode($listJaminan->beratBersihGod);
                        $hargaTaksiran = json_decode($listJaminan->hargaTaksiran);
                        $catatan = json_decode($listJaminan->catatan);
                        $id_jaminan = json_decode($listJaminan->idJaminan);
                        $perhiasan = array();
                        $result = array();
                        for ($i=0; $i< count($jenisPerhiasanGod); $i++) {
                            $perhiasan['idJaminan'] = $id_jaminan[$i]->id_jaminan;
                            $perhiasan['karat'] = $kadarEmasGod[$i]->kadar;
                            $perhiasan['beratBersih'] = $beratBersihGod[$i]->beratBersih;
                            $perhiasan['jumlah'] = 1;
                            $perhiasan['description'] = $catatan[$i]->catatan;
                            $perhiasan['jenisPerhiasan'] = $this->getInitialPerhiasan($jenisPerhiasanGod[$i]->jenis_perhiasan);
                            $perhiasan['hargaTaksiran'] = $hargaTaksiran[$i]->hargaSatuan;
                            $result[]=$perhiasan;
                        }
                        $r->listJaminan = $result;
                    }
                    
                    if ($r->jenisPerhiasan=='Kalung') {
                        $r->jenisPerhiasan = 'KL';
                    } elseif ($r->jenisPerhiasan == 'Gelang') {
                        $r->jenisPerhiasan = 'GL';
                    } elseif ($r->jenisPerhiasan == 'Cincin') {
                        $r->jenisPerhiasan = 'CN';
                    } elseif ($r->jenisPerhiasan == 'Giwang') {
                        $r->jenisPerhiasan = 'GW';
                    } elseif ($r->jenisPerhiasan == 'Anting') {
                        $r->jenisPerhiasan = 'AN';
                    } elseif ($r->jenisPerhiasan == 'Liontin') {
                        $r->jenisPerhiasan = 'LI';
                    } else {
                        $r->jenisPerhiasan = 'LN';
                    }
                    
                    return $r;
                }
            }
            return false;
        } elseif ($kodeBooking[0] == "2") {
            $jenis_layanan = $this->db->select('jenis_layanan')
                                        ->where('kode_booking', $kodeBooking)
                                        ->get('gadai_logam_mulia')->row();
            
            if ($jenis_layanan) {
                if ($jenis_layanan->jenis_layanan == 1) {
                    $cek = $this->db->select($selectId)
                                ->select('"Logam Mulia" as jaminan')
                                ->select('"KT" as rubrik')
                                ->select('"KTML" as tipeJaminan')
                                ->select('foto_json as fotoJaminan')
                                ->select('kode_outlet as kodeOutlet')
                                ->select('FROM_UNIXTIME(jadwal_kedatangan) as  jadwalKedatangan')
                                ->select('kode_booking as kodeBooking')
                                ->select('tanggal_booking as tglBooking')
                                ->select('gadai_logam_mulia.status as statusGod')
                                ->select('no_kredit as noKredit')
                                ->select('tanggal_kredit as tglKredit')
                                ->select('up')
                                ->select('taksiran')
                                ->select('taksiran_atas as taksiranAtas')
                                ->select('taksiran_bawah as taksiranBawah')
                                ->select('bunga')
                                ->select('sewa_modal as sewaModal')
                                ->select('sewa_modal_maksimal as sewaModalMaksimal')
                                ->select('(CASE jenisPromo 
                                        WHEN null THEN ""
                                        WHEN "" THEN ""
                                        WHEN "undefined" THEN "" 
                                        ELSE jenisPromo END) as jenisPromo')
                                ->select('jenis_pickup as jenisPickup')
                                ->select('no_order_gosend as kodeDelivery')
                                ->select('amount as amountDelivery')
                                // ->select('kodeBank')
                                // ->select('norekBank')
                                // ->select('customerNameBank')
                                ->select('jenis_layanan as tipeLayanan')
                                ->select('id_order_midtrans as idOrderMidtrans')
                                ->where('kode_booking', $kodeBooking)
                                ->join('user', 'gadai_logam_mulia.user_AIID=user.user_AIID')
                                ->get('gadai_logam_mulia');
                } else {
                    $cek = $this->db->select($selectId)
                                ->select('"Logam Mulia" as jaminan')
                                ->select('"KT" as rubrik')
                                ->select('"KTML" as tipeJaminan')
                                ->select('keping_json as beratKeping')
                                ->select('foto_json as fotoJaminan')
                                ->select('kode_outlet as kodeOutlet')
                                ->select('FROM_UNIXTIME(jadwal_kedatangan) as  jadwalKedatangan')
                                ->select('kode_booking as kodeBooking')
                                ->select('tanggal_booking as tglBooking')
                                ->select('gadai_logam_mulia.status')
                                ->select('no_kredit as noKredit')
                                ->select('tanggal_kredit as tglKredit')
                                ->select('up')
                                ->select('taksiran')
                                ->select('taksiran_atas as taksiranAtas')
                                ->select('taksiran_bawah as taksiranBawah')
                                ->select('bunga')
                                ->select('sewa_modal as sewaModal')
                                ->select('sewa_modal_maksimal as sewaModalMaksimal')
                                ->select('jenisPromo')
                                ->select('jenis_layanan as tipeLayanan')
                                ->select('id_order_midtrans as idOrderMidtrans')
                                ->where('kode_booking', $kodeBooking)
                                ->join('user', 'gadai_logam_mulia.user_AIID=user.user_AIID')
                                ->get('gadai_logam_mulia');
                }
            
                if ($cek->num_rows() > 0) {
                    $r = $cek->row();
                    $fotoList = json_decode($r->fotoJaminan);
                    $resFotoList = array();
                    $baseUrl = $this->config->item('base_url');
                    if (!empty($fotoList)) {
                        foreach ($fotoList as $f) {
                            $resFotoList[] = $baseUrl.'uploaded/user/gadai/'.$f->nama_foto;
                        }
                    }
                    $r->fotoJaminan = $resFotoList;

                    if ($r->tipeLayanan == 1) {
                        $listJaminan = $this->db->select('keping_json as beratKeping')
                                                ->select('jumlah_keping as jumlahKeping')
                                                ->select('harga_satuan_keping as hargaTaksiran')
                                                ->select('id_jaminan as idJaminan')
                                                ->where('kode_booking', $kodeBooking)
                                                ->get('gadai_logam_mulia')->row();
                        // begin list item perhiasan
                        $beratKeping = json_decode($listJaminan->beratKeping);
                        $jumlahKeping = json_decode($listJaminan->jumlahKeping);
                        $hargaTaksiran = json_decode($listJaminan->hargaTaksiran);
                        $id_jaminan = json_decode($listJaminan->idJaminan);

                        $kepings = array();
                        $result = array();
                        for ($i=0; $i< count($beratKeping); $i++) {
                            $kepings['idJaminan'] = $id_jaminan[$i]->id_jaminan;
                            $kepings['beratKeping'] = $beratKeping[$i]->berat_keping;
                            $kepings['jumlahKeping'] = $jumlahKeping[$i]->jumlah_keping;
                            $kepings['hargaTaksiran'] = $hargaTaksiran[$i]->harga_satuan;
                            $result[]=$kepings;
                        }
                        
                        $r->listJaminan = $result;
                    } else {
                        $resKepings = array();
                        $kepings = json_decode($r->beratKeping);
                        foreach ($kepings as $k) {
                            $resKepings[] = $k->berat_keping;
                        }

                        $r->beratKeping = $resKepings;
                    }
                    
                    return $r;
                }
            }
            return false;
        } elseif ($kodeBooking[0] == "3") {
            $cek = $this->db->select($selectId)
                            ->select('"Handphone" as jaminan')
                            ->select('"EL" as rubrik')
                            ->select('"ELHP" as tipeJaminan')
                            ->select('merek, tipe, kondisi_smartphone as kondisi')
                            ->select('tahun_beli as tahunBeli')
                            ->select('harga_pasaran as hargaPasaran')
                            ->select('foto_json as fotoJaminan')
                            ->select('kode_outlet as kodeOutlet')
                            ->select('FROM_UNIXTIME(jadwal_kedatangan) as  jadwalKedatangan')
                            ->select('kode_booking as kodeBooking')
                            ->select('tanggal_booking as tglBooking')
                            ->select('gadai_handphone.status')
                            ->select('no_kredit as noKredit')
                            ->select('tanggal_kredit as tglKredit')
                            ->select('up')
                            ->select('taksiran')
                            ->select('taksiran_atas as taksiranAtas')
                            ->select('taksiran_bawah as taksiranBawah')
                            ->select('bunga')
                            ->select('sewa_modal as sewaModal')
                            ->select('sewa_modal_maksimal as sewaModalMaksimal')
                            ->select('jenisPromo')
                            ->where('kode_booking', $kodeBooking)
                            ->join('ref_smartphone', 'gadai_handphone.id_merek_smartphone=ref_smartphone.id')
                            ->join('user', 'gadai_handphone.user_AIID=user.user_AIID')
                            ->get('gadai_handphone');
            if ($cek->num_rows() > 0) {
                $r = $cek->row();
                $fotoList = json_decode($r->fotoJaminan);
                $resFotoList = array();
                $baseUrl = $this->config->item('base_url');
                foreach ($fotoList as $f) {
                    $resFotoList[] = $baseUrl.'uploaded/user/gadai/'.$f->nama_foto;
                }
                $r->fotoJaminan = $resFotoList;
                
                return $cek->row();
            }
            return false;
        } elseif ($kodeBooking[0] == "4") {
            $cek = $this->db->select($selectId)
                            ->select('"Elektronik" as jaminan')
                            ->select('"EL" as rubrik')
                            ->select('tipe_jaminan as tipeJaminan')
                            ->select('merek, gadai_elektronik.tipe, gadai_elektronik.jenis, kondisi_barang as kondisi')
                            ->select('harga_jual as hargaJual')
                            ->select('foto_json as fotoJaminan')
                            ->select('kode_outlet as kodeOutlet')
                            ->select('FROM_UNIXTIME(jadwal_kedatangan) as  jadwalKedatangan')
                            ->select('kode_booking as kodeBooking')
                            ->select('tanggal_booking as tglBooking')
                            ->select('gadai_elektronik.status')
                            ->select('no_kredit as noKredit')
                            ->select('tanggal_kredit as tglKredit')
                            ->select('up')
                            ->select('taksiran')
                            ->select('taksiran_atas as taksiranAtas')
                            ->select('taksiran_bawah as taksiranBawah')
                            ->select('bunga')
                            ->select('sewa_modal as sewaModal')
                            ->select('sewa_modal_maksimal as sewaModalMaksimal')
                            ->select('jenisPromo')
                            ->where('kode_booking', $kodeBooking)
                            ->join('ref_elektronik', 'ref_elektronik.id=gadai_elektronik.id_merek_elektronik')
                            ->join('user', 'gadai_elektronik.user_AIID=user.user_AIID')
                            ->get('gadai_elektronik');
            if ($cek->num_rows() > 0) {
                $r = $cek->row();
                $fotoList = json_decode($r->fotoJaminan);
                $resFotoList = array();
                $baseUrl = $this->config->item('base_url');
                foreach ($fotoList as $f) {
                    $resFotoList[] = $baseUrl.'uploaded/user/gadai/'.$f->nama_foto;
                }
                $r->fotoJaminan = $resFotoList;
                
                return $cek->row();
            }
            return false;
        } elseif ($kodeBooking[0] == "5") {
            $cek = $this->db->select($selectId)
                            ->select('"Kendaraan" as jaminan')
                            ->select('"KN" as rubrik')
                            ->select('"tipeJaminan " as tipeJaminan')
                            ->select('jenis_kendaraan as jenisKendaraan')
                            ->select('merek')
                            ->select('tipe')
                            ->select('deskripsi')
                            ->select('isi_silinder as isiSilinder')
                            ->select('tahun_pembuatan as tahunPembuatan')
                            ->select('harga_jual as hargaJual')
                            ->select('no_polisi as noPolisi')
                            ->select('no_bpkb as noBPKB')
                            ->select('no_stnk as noSTNK')
                            ->select('no_mesin as noMesin')
                            ->select('no_rangka as noRangka')
                            ->select('foto_json as fotoJaminan')
                            ->select('FROM_UNIXTIME(jadwal_kedatangan) as  jadwalKedatangan')
                            ->select('kode_booking as kodeBooking')
                            ->select('kode_outlet as kodeOutlet')
                            ->select('tanggal_booking as tglBooking')
                            ->select('gadai_kendaraan.status')
                            ->select('no_kredit as noKredit')
                            ->select('tanggal_kredit as tglKredit')
                            ->select('up')
                            ->select('taksiran')
                            ->select('taksiran_atas as taksiranAtas')
                            ->select('taksiran_bawah as taksiranBawah')
                            ->select('bunga')
                            ->select('sewa_modal as sewaModal')
                            ->select('sewa_modal_maksimal as sewaModalMaksimal')
                            ->select('jenisPromo')
                            ->where('kode_booking', $kodeBooking)
                            ->join('ref_kendaraan', 'ref_kendaraan.id=gadai_kendaraan.id_merek_kendaraan')
                            ->join('user', 'gadai_kendaraan.user_AIID=user.user_AIID')
                            ->get('gadai_kendaraan');
            if ($cek->num_rows() > 0) {
                $r = $cek->row();
                
                if ($r->jenisKendaraan == 'Motor') {
                    $r->tipeJaminan = 'KNSM';
                } elseif ($r->jenisKendaraan == 'Mobil') {
                    $r->tipeJaminan = 'KNML';
                }
                
                $fotoList = json_decode($r->fotoJaminan);
                $resFotoList = array();
                $baseUrl = $this->config->item('base_url');
                foreach ($fotoList as $f) {
                    $resFotoList[] = $baseUrl.'uploaded/user/gadai/'.$f->nama_foto;
                }
                $r->fotoJaminan = $resFotoList;
                
                return $r;
            }
            return false;
        } elseif ($kodeBooking[0] == "6") {
                $cek = $this->db->select($selectId)
                            ->select('"Laptop" as jaminan')
                            ->select('"EL" as rubrik')
                            ->select('"ELKM" as tipeJaminan')
                            ->select('merek')
                            ->select('tipe')
                            ->select('deskripsi')
                            ->select('kapasitas_harddisk as kapasitasHDD')
                            ->select('tipe_processor as tipeProcessor')
                            ->select('kapasitas_ram as kapasitasRAM')
                            ->select('tahun_pembelian as tahunPembelian')
                            ->select('harga_jual as hargaJual')
                            ->select('foto_json as fotoJaminan')
                            ->select('FROM_UNIXTIME(jadwal_kedatangan) as  jadwalKedatangan')
                            ->select('kode_booking as kodeBooking')
                            ->select('kode_outlet as kodeOutlet')
                            ->select('tanggal_booking as tglBooking')
                            ->select('gadai_laptop.status')
                            ->select('no_kredit as noKredit')
                            ->select('tanggal_kredit as tglKredit')
                            ->select('up')
                            ->select('taksiran')
                            ->select('taksiran_atas as taksiranAtas')
                            ->select('taksiran_bawah as taksiranBawah')
                            ->select('bunga')
                            ->select('sewa_modal as sewaModal')
                            ->select('sewa_modal_maksimal as sewaModalMaksimal')
                            ->select('jenisPromo')
                            ->where('kode_booking', $kodeBooking)
                            ->join('ref_laptop', 'ref_laptop.id=gadai_laptop.id_merek_laptop')
                            ->join('user', 'gadai_laptop.user_AIID=user.user_AIID')
                            ->get('gadai_laptop');
            if ($cek->num_rows() > 0) {
                $r = $cek->row();
                $fotoList = json_decode($r->fotoJaminan);
                $resFotoList = array();
                $baseUrl = $this->config->item('base_url');
                foreach ($fotoList as $f) {
                    $resFotoList[] = $baseUrl.'uploaded/user/gadai/'.$f->nama_foto;
                }
                $r->fotoJaminan = $resFotoList;
                
                return $cek->row();
            }
            return false;
        } else {
            return false;
        }
    }

    function getDetailKontrakGod($kodeBooking, $jenis_gadai)
    {
        if ($kodeBooking==null) {
            return false;
        }
        
        
        $selectId = "ref_user_mikro.nama, user.nama as nasabah,user.user_AIID,user.email, jenis_identitas as tipeIdentitas, no_ktp as noIdentitas, tanggal_expired_identitas as tglExpiredId, "
                . "user.cif, user.nama_ibu as ibuKandung, user.tempat_lahir as tempatLahir, user.tgl_lahir as tglLahir, "
                . "user.jenis_kelamin as jenisKelamin, user.no_hp as noHp, user.kewarganegaraan, user.status_kawin as statusKawin,"
                . "user.alamat as jalan, user.id_kelurahan as idKelurahan";
        if ($kodeBooking[0] == "1") {
            $cek = $this->db->select($selectId)
                            ->select('"Perhiasan" as jaminan')
                            ->select('"KT" as rubrik')
                            ->select('"KTEP" as tipeJaminan')
                            ->select('jenis_perhiasan as jenisPerhiasan')
                            ->select('foto_json as fotoJaminan')
                            ->select('kadar_emas as kadarEmas')
                            ->select('berat_bersih as beratBersih')
                            ->select('berat_kotor as beratKotor')
                            ->select('kode_outlet as kodeOutlet')
                            ->select('FROM_UNIXTIME(jadwal_kedatangan) as  jadwalKedatangan')
                            ->select('kode_booking as kodeBooking')
                            ->select('tanggal_booking as tglBooking')
                            ->select('gadai_perhiasan.status')
                            ->select('no_kredit as noKredit')
                            ->select('tanggal_kredit as tglKredit')
                            ->select('taksiran')
                            ->select('taksiran_atas as taksiranAtas')
                            ->select('taksiran_bawah as taksiranBawah')
                            ->select('taksir_ulang_admin')
                            ->select('bunga')
                            ->select('sewa_modal as sewaModal')
                            ->select('sewa_modal_maksimal as sewaModalMaksimal')
                            ->select('jenisPromo')
                            ->select('serialNumber')
                            ->select('administrasi')
                            ->select('sewaModal')
                            ->select('tglJatuhTempo')
                            ->select('biayaProsesLelang')
                            ->select('biayaLelang')
                            ->select('biayaAdmBjpdl')
                            ->select('biayaBjdplMax')
                            ->select('namaNasabah')
                            ->select('kodeNamaCabang')
                            ->select('tglTransaksi')
                            ->select('tglLelang')
                            ->select('jumlahHariTarif')
                            ->select('tanggal_pembayaran')
                            ->select('kodeOtp')
                            ->select('gadai_perhiasan.up')
                            ->select('no_kredit')
                            ->select('nama_group')
                            ->select('nama_cabang')
                            ->select('namaPinca')
                            ->select('asuransi')
                            ->select('diterimaNasabah')
                            ->where('kode_booking', $kodeBooking)
                            ->join('user', 'gadai_perhiasan.user_AIID=user.user_AIID', 'LEFT')
                            ->join('payment_gadai', 'gadai_perhiasan.kode_booking=payment_gadai.bookingCode', 'LEFT')
                            ->join('ref_user_mikro', 'gadai_perhiasan.kode_outlet=ref_user_mikro.ref_kode_cabang', 'LEFT')
                            ->get('gadai_perhiasan');
                            // print $this->db->last_query();
                            // exit();
            if ($cek->num_rows() > 0) {
                $r = $cek->row();
                $fotoList = json_decode($r->fotoJaminan);
                $resFotoList = array();
                $baseUrl = $this->config->item('base_url');
                if (!empty($fotoList)) {
                    foreach ($fotoList as $f) {
                        $resFotoList[] = $baseUrl.'uploaded/user/gadai/'.$f->nama_foto;
                    }
                }
                $r->fotoJaminan = $resFotoList;
                
                if ($r->jenisPerhiasan=='Kalung') {
                    $r->jenisPerhiasan = 'KL';
                } elseif ($r->jenisPerhiasan == 'Gelang') {
                    $r->jenisPerhiasan = 'GL';
                } elseif ($r->jenisPerhiasan == 'Cincin') {
                    $r->jenisPerhiasan = 'CN';
                } elseif ($r->jenisPerhiasan == 'Giwang') {
                    $r->jenisPerhiasan = 'GW';
                } elseif ($r->jenisPerhiasan == 'Anting') {
                    $r->jenisPerhiasan = 'AN';
                } elseif ($r->jenisPerhiasan == 'Liontin') {
                    $r->jenisPerhiasan = 'LI';
                } else {
                    $r->jenisPerhiasan = 'LN';
                }
                
                return $r;
            }
            return false;
        } elseif ($kodeBooking[0] == "2") {
            $cek = $this->db->select($selectId)
                            ->select('"Logam Mulia" as jaminan')
                            ->select('"KT" as rubrik')
                            ->select('"KTML" as tipeJaminan')
                            ->select('keping_json as beratKeping')
                            ->select('foto_json as fotoJaminan')
                            ->select('kode_outlet as kodeOutlet')
                            ->select('FROM_UNIXTIME(jadwal_kedatangan) as  jadwalKedatangan')
                            ->select('kode_booking as kodeBooking')
                            ->select('tanggal_booking as tglBooking')
                            ->select('gadai_logam_mulia.status')
                            ->select('no_kredit as noKredit')
                            ->select('tanggal_kredit as tglKredit')
                            ->select('taksiran')
                            ->select('taksir_ulang_admin')
                            ->select('taksiran_atas as taksiranAtas')
                            ->select('taksiran_bawah as taksiranBawah')
                            ->select('bunga')
                            ->select('sewa_modal as sewaModal')
                            ->select('sewa_modal_maksimal as sewaModalMaksimal')
                            ->select('jenisPromo')
                            ->select('serialNumber')
                            ->select('administrasi')
                            ->select('sewaModal')
                            ->select('tglJatuhTempo')
                            ->select('biayaProsesLelang')
                            ->select('biayaLelang')
                            ->select('biayaAdmBjpdl')
                            ->select('biayaBjdplMax')
                            ->select('namaNasabah')
                            ->select('kodeNamaCabang')
                            ->select('tglTransaksi')
                            ->select('tglLelang')
                            ->select('tanggal_pembayaran')
                            ->select('jumlahHariTarif')
                            ->select('kodeOtp')
                            ->select('gadai_logam_mulia.up')
                            ->select('no_kredit')
                            ->select('nama_group')
                            ->select('nama_cabang')
                            ->select('namaPinca')
                            ->select('asuransi')
                            ->select('diterimaNasabah')
                            ->where('kode_booking', $kodeBooking)
                            ->join('user', 'gadai_logam_mulia.user_AIID=user.user_AIID', 'LEFT')
                            ->join('payment_gadai', 'gadai_logam_mulia.kode_booking=payment_gadai.bookingCode', 'LEFT')
                            ->join('ref_user_mikro', 'gadai_logam_mulia.kode_outlet=ref_user_mikro.ref_kode_cabang', 'LEFT')
                            ->get('gadai_logam_mulia');
                            // print $this->db->last_query();
                            // exit();
            if ($cek->num_rows() > 0) {
                $r = $cek->row();
                $fotoList = json_decode($r->fotoJaminan);
                $resFotoList = array();
                $baseUrl = $this->config->item('base_url');
                // if (!empty($fotoList)){
                //     foreach($fotoList as $f)
                //   {
                //     $resFotoList[] = $baseUrl.'uploaded/user/gadai/'.$f->nama_foto;
                //   }
                // }
                $r->fotoJaminan = $resFotoList;
                $resKepings = array();
                $kepings = json_decode($r->beratKeping);
                foreach ($kepings as $k) {
                    $resKepings[] = $k->berat_keping;
                }
                
                $r->beratKeping = $resKepings;
                // if($r->tipeLayanan == 0){
                //     $r->tipeLayanan = 'Booking Gadai';
                // } else {
                //     $r->tipeLayanan = 'Gadai Pickup Delivery';
                // }
                
                return $r;
            }
            return false;
        } elseif ($kodeBooking[0] == "3") {
            $cek = $this->db->select($selectId)
                            ->select('"Handphone" as jaminan')
                            ->select('"EL" as rubrik')
                            ->select('"ELHP" as tipeJaminan')
                            ->select('merek, tipe, kondisi_smartphone as kondisi')
                            ->select('tahun_beli as tahunBeli')
                            ->select('harga_pasaran as hargaPasaran')
                            ->select('foto_json as fotoJaminan')
                            ->select('kode_outlet as kodeOutlet')
                            ->select('FROM_UNIXTIME(jadwal_kedatangan) as  jadwalKedatangan')
                            ->select('kode_booking as kodeBooking')
                            ->select('tanggal_booking as tglBooking')
                            ->select('gadai_handphone.status')
                            ->select('no_kredit as noKredit')
                            ->select('tanggal_kredit as tglKredit')
                            ->select('up')
                            ->select('taksiran')
                            ->select('taksiran_atas as taksiranAtas')
                            ->select('taksiran_bawah as taksiranBawah')
                            ->select('bunga')
                            ->select('sewa_modal as sewaModal')
                            ->select('sewa_modal_maksimal as sewaModalMaksimal')
                            ->select('jenisPromo')
                            ->where('kode_booking', $kodeBooking)
                            ->join('ref_smartphone', 'gadai_handphone.id_merek_smartphone=ref_smartphone.id')
                            ->join('user', 'gadai_handphone.user_AIID=user.user_AIID')
                            ->get('gadai_handphone');
            if ($cek->num_rows() > 0) {
                $r = $cek->row();
                $fotoList = json_decode($r->fotoJaminan);
                $resFotoList = array();
                $baseUrl = $this->config->item('base_url');
                foreach ($fotoList as $f) {
                    $resFotoList[] = $baseUrl.'uploaded/user/gadai/'.$f->nama_foto;
                }
                $r->fotoJaminan = $resFotoList;
                
                return $cek->row();
            }
            return false;
        } elseif ($kodeBooking[0] == "4") {
            $cek = $this->db->select($selectId)
                            ->select('"Elektronik" as jaminan')
                            ->select('"EL" as rubrik')
                            ->select('tipe_jaminan as tipeJaminan')
                            ->select('merek, gadai_elektronik.tipe, gadai_elektronik.jenis, kondisi_barang as kondisi')
                            ->select('harga_jual as hargaJual')
                            ->select('foto_json as fotoJaminan')
                            ->select('kode_outlet as kodeOutlet')
                            ->select('FROM_UNIXTIME(jadwal_kedatangan) as  jadwalKedatangan')
                            ->select('kode_booking as kodeBooking')
                            ->select('tanggal_booking as tglBooking')
                            ->select('gadai_elektronik.status')
                            ->select('no_kredit as noKredit')
                            ->select('tanggal_kredit as tglKredit')
                            ->select('up')
                            ->select('taksiran')
                            ->select('taksiran_atas as taksiranAtas')
                            ->select('taksiran_bawah as taksiranBawah')
                            ->select('bunga')
                            ->select('sewa_modal as sewaModal')
                            ->select('sewa_modal_maksimal as sewaModalMaksimal')
                            ->select('jenisPromo')
                            ->where('kode_booking', $kodeBooking)
                            ->join('ref_elektronik', 'ref_elektronik.id=gadai_elektronik.id_merek_elektronik')
                            ->join('user', 'gadai_elektronik.user_AIID=user.user_AIID')
                            ->get('gadai_elektronik');


            if ($cek->num_rows() > 0) {
                $r = $cek->row();
                $fotoList = json_decode($r->fotoJaminan);
                $resFotoList = array();
                $baseUrl = $this->config->item('base_url');
                foreach ($fotoList as $f) {
                    $resFotoList[] = $baseUrl.'uploaded/user/gadai/'.$f->nama_foto;
                }
                $r->fotoJaminan = $resFotoList;
                
                return $cek->row();
            }
            return false;
        } elseif ($kodeBooking[0] == "5") {
            $cek = $this->db->select($selectId)
                            ->select('"Kendaraan" as jaminan')
                            ->select('"KN" as rubrik')
                            ->select('"tipeJaminan " as tipeJaminan')
                            ->select('jenis_kendaraan as jenisKendaraan')
                            ->select('merek')
                            ->select('tipe')
                            ->select('deskripsi')
                            ->select('isi_silinder as isiSilinder')
                            ->select('tahun_pembuatan as tahunPembuatan')
                            ->select('harga_jual as hargaJual')
                            ->select('no_polisi as noPolisi')
                            ->select('no_bpkb as noBPKB')
                            ->select('no_stnk as noSTNK')
                            ->select('no_mesin as noMesin')
                            ->select('no_rangka as noRangka')
                            ->select('foto_json as fotoJaminan')
                            ->select('FROM_UNIXTIME(jadwal_kedatangan) as  jadwalKedatangan')
                            ->select('kode_booking as kodeBooking')
                            ->select('kode_outlet as kodeOutlet')
                            ->select('tanggal_booking as tglBooking')
                            ->select('gadai_kendaraan.status')
                            ->select('no_kredit as noKredit')
                            ->select('tanggal_kredit as tglKredit')
                            ->select('up')
                            ->select('taksiran')
                            ->select('taksiran_atas as taksiranAtas')
                            ->select('taksiran_bawah as taksiranBawah')
                            ->select('bunga')
                            ->select('sewa_modal as sewaModal')
                            ->select('sewa_modal_maksimal as sewaModalMaksimal')
                            ->select('jenisPromo')
                            ->where('kode_booking', $kodeBooking)
                            ->join('ref_kendaraan', 'ref_kendaraan.id=gadai_kendaraan.id_merek_kendaraan')
                            ->join('user', 'gadai_kendaraan.user_AIID=user.user_AIID')
                            ->get('gadai_kendaraan');
            if ($cek->num_rows() > 0) {
                $r = $cek->row();
                
                if ($r->jenisKendaraan == 'Motor') {
                    $r->tipeJaminan = 'KNSM';
                } elseif ($r->jenisKendaraan == 'Mobil') {
                    $r->tipeJaminan = 'KNML';
                }
                
                $fotoList = json_decode($r->fotoJaminan);
                $resFotoList = array();
                $baseUrl = $this->config->item('base_url');
                foreach ($fotoList as $f) {
                    $resFotoList[] = $baseUrl.'uploaded/user/gadai/'.$f->nama_foto;
                }
                $r->fotoJaminan = $resFotoList;
                return $r;
            }
            return false;
        } else {
            return false;
        }
    }

    
    function savePayment($data)
    {
        $this->db->insert('payment_gadai', $data);
    }
    
    function getPaymentByTrxId($trxId)
    {
        $cek = $this->db->select('payment_gadai.*, user.email, user.fcm_token, user.nama, user.no_hp')
                        ->join('user', 'user.user_AIID=payment_gadai.user_AIID')
                        ->where('reffSwitching', $trxId)->get('payment_gadai');
        if ($cek->num_rows() > 0) {
            return $cek->row();
        }
        
        return false;
    }
    
    function updatePayment($trxId, $data)
    {
        $this->db->where('reffSwitching', $trxId)->update('payment_gadai', $data);
    }
    
    function getUserPayment($userId)
    {
        return $this->db
                    ->where('user_AIID', $userId)
                    ->order_by('id', 'desc')
                    ->get('payment_gadai')->result();
    }

    function getProductName($jenisTransaksi, $productCode)
    {
        $cek = $this->db->where(array(
            'jenisTransaksi' => $jenisTransaksi,
            'productCode' => $productCode
        ))->get('ref_biaya_payment');

        if ($cek->num_rows() > 0) {
            return $cek->row()->productName;
        }

        return null;
    }

    function getUnCompleteGodLogamMulia()
    {
        $kode_booking = $this->db->select('kode_booking, no_order_gosend, user_AIID, status_fcm, status')
        ->where("no_order_gosend != '' AND jenis_layanan = 1 AND status NOT IN (0, 2, 9, 92, 7, 4, 94, 99)")
        ->get('gadai_logam_mulia');
        return $kode_booking->result();
    }

    function getUnCompleteGodPerhiasan()
    {
        $kode_booking = $this->db->select('kode_booking , no_order_gosend, user_AIID, status_fcm, status')
        ->where("no_order_gosend != '' AND jenis_layanan = 1 AND status NOT IN (0, 2, 9, 92, 7, 4, 94, 99)")
        ->get('gadai_perhiasan');
        return $kode_booking->result();
    }

    function getStatusDitolakPerhiasan()
    {
        $kode_booking = $this->db->select('kode_booking , no_order_gosend, user_AIID, status')
        ->where("((status = 0) OR (status = 94) OR (status = 92)) AND (jenis_layanan = 1) AND (no_order_gosend != '')")
        ->get('gadai_perhiasan');
        return $kode_booking->result();
    }
    function getStatusDitolakLogamMulia()
    {
        $kode_booking = $this->db->select('kode_booking , no_order_gosend, user_AIID, status')
        ->where("((status = 0) OR (status = 94) OR (status = 92)) AND (jenis_layanan = 1) AND (no_order_gosend != '')")
        ->get('gadai_logam_mulia');
        return $kode_booking->result();
    }
    
    function checkPromo($jenisPromo, $nilaiTransaksi, $tenor = 0)
    {
        $bannerPath = $this->config->item('super_admin_base_url') .'upload/banner/';
        
        $where = 'UPPER(kode_promo) = UPPER("'.$jenisPromo.'") AND aktif="1"';

        $cek =  $this->db->select('id, kode_product as productCode')
                        ->select('CONCAT("'.$bannerPath.'", banner ) as banner')
                        ->select('kode_promo as jenisPromo, unlimited, periode_awal as start')
                        ->select('periode_akhir as expired, tenor, minimum_transaksi as minimumTransaksi')
                        ->select('deskripsi, syarat_dan_ketentuan as syaratKetentuan')
                        ->select('date_update as last_update')
                        ->where($where)
                        ->get('ref_promo_gte');
        
        if ($cek->num_rows() == 0) {
            return [
                'status' => 'error',
                'message' => 'Kode promo tidak valid',
            ];
        } else {
            $data = $cek->row();
            $now = new DateTime();
            $begin = new DateTime($data->start);
            $end = new DateTime($data->expired);
            
            if (($now->getTimestamp() < $begin->getTimestamp()) ||
                    ($now->getTimestamp() > $end->getTimestamp())) {
                return [
                    'status' => 'error',
                    'message' => 'Promo hanya berlaku dari '.$begin->format('d/m/Y').' sampai '. $end->format('d/m/Y')
                ];
            } elseif ($nilaiTransaksi < $data->minimumTransaksi) {
                return [
                    'status' => 'error',
                    'message' => 'Minimum transaksi promo ini Rp '. number_format($data->minimumTransaksi, 0, ',', '.')
                ];
            } elseif ($tenor != 0 && $tenor > $data->tenor) {
                return [
                    'status' => 'error',
                    'message' => 'Promo ini hanya berlaku pada tenor maksimal '. $data->tenor.' hari'
                ];
            } else {
                return [
                    'status' => 'success',
                    'messaget' => '',
                    'data' => $data
                ];
            }
        }
    }

    function getPromo()
    {
        $bannerPath = $this->config->item('super_admin_base_url') .'upload/banner/';
        $where = 'periode_akhir >= NOW() AND aktif="1"';

        return $this->db->select('id, kode_product as productCode')
                            ->select('CONCAT("'.$bannerPath.'", banner ) as banner')
                            ->select('kode_promo as jenisPromo, unlimited, periode_awal as start')
                            ->select('periode_akhir as expired, minimum_transaksi as minimumTransaksi')
                            ->select('deskripsi, syarat_dan_ketentuan as syaratKetentuan')
                            ->select('date_update as last_update')
                            ->where($where)
                            ->get('ref_promo_gte')
                            ->result();
    }

    function simpanStatusGosend($user_AIID, $kodeBooking, $jenisGadai, $orderGosend, $statusFcm = '', $status = '')
    {
        $data = [
            'no_order_gosend' => $orderGosend,
            'status_fcm' => $statusFcm,
            'status' => $status
        ];

        return $this->db->where([
            'user_AIID' => $user_AIID,
            'kode_booking' => $kodeBooking
        ])->update('gadai_' . $jenisGadai, $data);
    }

    function simpanStatusSessionPay($kodeBooking, $jenisGadai)
    {
        return $this->db->where(
            'kode_booking',
            $kodeBooking
        )
              ->update('gadai_'.$jenisGadai, array(
              'status' => '91'));
    }

    function saveGadaiEfek($data)
    {
        $this->db->insert('gadai_efek', $data);
    }

    function updateGadaiEfek($kodeBooking, $data)
    {
        $this->db->where('bookingId', $kodeBooking)->update('gadai_efek', $data);
    }

    function getGadaiEfek($kodeBooking)
    {
        $cek = $this->db->select('user.user_AIID, user.nama, user.alamat, user.no_hp, email, fcm_token')
                    ->select('gadai_efek.*')
                    ->from('gadai_efek')
                    ->join('user', 'gadai_efek.user_AIID=user.user_AIID')
                    ->where('bookingId', $kodeBooking)
                    ->get();
        if ($cek->num_rows() > 0) {
            return $cek->row();
        }

        return false;
    }

    function getUserGadaiEfek($userId, $id = null)
    {
        $cek = $this->db
                    ->select('gadai_efek.*')
                    ->from('gadai_efek')
                    ->where('gadai_efek.user_AIID', $userId);
        if ($id != null) {
            $cek = $cek->where('gadai_efek.id', $id)->get();
            if ($cek->num_rows() > 0) {
                return $cek->row();
            } else {
                return [];
            }
        } else {
            return $cek->get()->result();
        }
    }

    function getEfekList($option)
    {
        return $this->db->where('tipeEfek', $option)->get('ref_efek')->result();
    }

    function simpanTaksirUlang($kodeBooking, $nominal_taksir_ulang, $kode_outlet, $user_penaksir, $jenisGadai, $cif, $user_AIID)
    {
      //Update to Gadai
        $saveTaksirUlang = array(
                'taksir_ulang_admin' => $nominal_taksir_ulang,
                'kode_outlet' => $kode_outlet,
                'user_penaksir' => $user_penaksir
                );
        $this->db->where('kode_booking', $kodeBooking)->update('gadai_'.$jenisGadai, $saveTaksirUlang);

      //Update to User
        $saveCIF = array('cif' => $cif);
        $this->db->where('user_AIID', $user_AIID)->update('user', $saveCIF);
    }

    function simpanNorekNasabah($kodeBooking, $norekBank, $kodeBank, $customerNameBank, $jenisGadai)
    {
        if ($norekBank) {
            return $this->db->where(
                'kode_booking',
                $kodeBooking
            )
              ->update('gadai_'.$jenisGadai, array(
              'norekBank' => $norekBank,
              'kodeBank' => $kodeBank,
              'customerNameBank' => $customerNameBank,
              'status' => '2'
              ));
        } else {
            return [
                    'status' => 'error',
                    'message' => 'field is required'
            ];
        }
    }

    function getDataRekeningLogamMuliaByKodeBooking($kode_booking)
    {
        $cek = $this->db->where('kode_booking', $kode_booking)
                      ->where('(gadai_logam_mulia.norekBank IS NOT NULL) AND (gadai_logam_mulia.kodeBank IS NOT NULL) AND (gadai_logam_mulia.customerNameBank IS NOT NULL)')
                      ->select('kodeBank')
                      ->select('norekBank')
                      ->select('customerNameBank as namaPemilik')
                      ->limit(1)
                      ->get('gadai_logam_mulia');
        return $cek;
    }

    function getDataRekeningPerhiasanByKodeBooking($kode_booking)
    {
        $cek = $this->db->where('kode_booking', $kode_booking)
                      ->where('(gadai_perhiasan.norekBank IS NOT NULL) AND (gadai_perhiasan.kodeBank IS NOT NULL) AND (gadai_perhiasan.customerNameBank IS NOT NULL)')
                      ->select('kodeBank')
                      ->select('norekBank')
                      ->select('customerNameBank as namaPemilik')
                      ->limit(1)
                      ->get('gadai_perhiasan');
        return $cek;
    }

    function simpanStatusOtp($user_AIID, $kodeBooking, $jenisGadai, $kodeOtp)
    {
        return $this->db->where(array(
              'user_AIID' => $user_AIID,
              'kode_booking' => $kodeBooking))
              ->update('gadai_'.$jenisGadai, array(
              'kodeOtp' => $kodeOtp,
              'otp' => '1'));
    }

    function simpanStatusGopayLogamMulia($kodeBooking, $amount, $jenisPickup)
    {
        $data = array(
        'status' => '8',
        'amount' => $amount,
        'jenis_pickup' => $jenisPickup
        );

        $this->db->where('kode_booking', $kodeBooking)->update('gadai_logam_mulia', $data);
    }

    function simpanLinkPaymentLogamMulia($kodeBooking, $payment_transaction_id, $payment_link)
    {
        $data = array(
        'payment_transaction_id' => $payment_transaction_id,
        'payment_link' => $payment_link
        );

        $this->db->where('kode_booking', $kodeBooking)->update('gadai_logam_mulia', $data);
    }

    function simpanLinkPaymentPerhiasan($kodeBooking, $payment_transaction_id, $payment_link)
    {
        $data = array(
        'payment_transaction_id' => $payment_transaction_id,
        'payment_link' => $payment_link
        );

        $this->db->where('kode_booking', $kodeBooking)->update('gadai_perhiasan', $data);
    }
    
    function check($kodeBooking, $tipe)
    {
        // $tipe
        //LM = Logam Mulia
        //PH = Perhiasan
        if ($tipe == 'LM') {
            $cek = $this->db->where('kode_booking', $kodeBooking)->get('gadai_logam_mulia');
        } elseif ($tipe == 'PH') {
            $cek = $this->db->where('kode_booking', $kodeBooking)->get('gadai_perhiasan');
        }
        if ($cek->row()) {
            return $cek->row();
        } else {
            return '';
        }
    }

    function simpanStatusGopayPerhiasan($kodeBooking, $amount, $jenisPickup)
    {
        $data = array(
        'status' => '8',
        'amount' => $amount,
        'jenis_pickup' => $jenisPickup
        );

        $this->db->where('kode_booking', $kodeBooking)->update('gadai_perhiasan', $data);
    }

    function getDetailGadaiByKodeBookingGod($kodeBooking)
    {
        if ($kodeBooking==null) {
            return false;
        }
        
        $selectId = "nama as namaNasabah, jenis_identitas as tipeIdentitas, no_ktp as noIdentitas, tanggal_expired_identitas as tglExpiredId, "
                . "user.cif, user.nama_ibu as ibuKandung, user.tempat_lahir as tempatLahir, user.tgl_lahir as tglLahir, "
                . "user.jenis_kelamin as jenisKelamin, user.no_hp as noHp, user.kewarganegaraan, user.status_kawin as statusKawin,"
                . "user.alamat as jalan, user.id_kelurahan as idKelurahan";
        
        if ($kodeBooking[0] == "1") {
            $cek = $this->db->select($selectId)
                            ->select('"Perhiasan" as jaminan')
                            ->select('"KT" as rubrik')
                            ->select('"KTEP" as tipeJaminan')
                            ->select('jenis_perhiasan as jenisPerhiasan')
                            ->select('foto_json as fotoJaminan')
                            ->select('kadar_emas as kadarEmas')
                            ->select('berat_bersih as beratBersih')
                            ->select('berat_kotor as beratKotor')
                            ->select('kode_outlet as kodeOutlet')
                            ->select('FROM_UNIXTIME(jadwal_kedatangan) as  jadwalKedatangan')
                            ->select('kode_booking as kodeBooking')
                            ->select('tanggal_booking as tglBooking')
                            ->select('gadai_perhiasan.status')
                            ->select('no_kredit as noKredit')
                            ->select('tanggal_kredit as tglKredit')
                            ->select('taksiran')
                            ->select('taksiran_atas as taksiranAtas')
                            ->select('taksiran_bawah as taksiranBawah')
                            ->select('bunga')
                            ->select('sewa_modal as sewaModal')
                            ->select('sewa_modal_maksimal as sewaModalMaksimal')
                            ->select('jenisPromo')
                            ->select('serialNumber')
                            ->select('administrasi')
                            ->select('sewaModal')
                            ->select('tglJatuhTempo')
                            ->select('biayaProsesLelang')
                            ->select('biayaLelang')
                            ->select('biayaAdmBjpdl')
                            ->select('namaNasabah')
                            ->select('kodeNamaCabang')
                            ->select('tglTransaksi')
                            ->select('tglLelang')
                            ->select('tanggal_pembayaran')
                            ->select('status_fcm as statusFcm')
                            ->select('no_order_gosend as noOrderGosend')
                            ->where('kode_booking', $kodeBooking)
                            ->join('user', 'gadai_perhiasan.user_AIID=user.user_AIID', 'LEFT')
                            ->join('payment_gadai', 'gadai_perhiasan.kode_booking=payment_gadai.bookingCode', 'LEFT')
                            ->get('gadai_perhiasan');
            if ($cek->num_rows() > 0) {
                $r = $cek->row();
                $fotoList = json_decode($r->fotoJaminan);
                $resFotoList = array();
                $baseUrl = $this->config->item('base_url');
                if (!empty($fotoList)) {
                    foreach ($fotoList as $f) {
                        $resFotoList[] = $baseUrl.'uploaded/user/gadai/'.$f->nama_foto;
                    }
                }
                $r->fotoJaminan = $resFotoList;
                
                if ($r->jenisPerhiasan=='Kalung') {
                    $r->jenisPerhiasan = 'KL';
                } elseif ($r->jenisPerhiasan == 'Gelang') {
                    $r->jenisPerhiasan = 'GL';
                } elseif ($r->jenisPerhiasan == 'Cincin') {
                    $r->jenisPerhiasan = 'CN';
                } elseif ($r->jenisPerhiasan == 'Giwang') {
                    $r->jenisPerhiasan = 'GW';
                } elseif ($r->jenisPerhiasan == 'Anting') {
                    $r->jenisPerhiasan = 'AN';
                } elseif ($r->jenisPerhiasan == 'Liontin') {
                    $r->jenisPerhiasan = 'LI';
                } else {
                    $r->jenisPerhiasan = 'LN';
                }
                
                return $r;
            }
            return false;
        } elseif ($kodeBooking[0] == "2") {
            $cek = $this->db->select($selectId)
                            ->select('"Logam Mulia" as jaminan')
                            ->select('"KT" as rubrik')
                            ->select('"KTML" as tipeJaminan')
                            ->select('keping_json as beratKeping')
                            ->select('foto_json as fotoJaminan')
                            ->select('kode_outlet as kodeOutlet')
                            ->select('FROM_UNIXTIME(jadwal_kedatangan) as  jadwalKedatangan')
                            ->select('kode_booking as kodeBooking')
                            ->select('tanggal_booking as tglBooking')
                            ->select('gadai_logam_mulia.status')
                            ->select('no_kredit as noKredit')
                            ->select('tanggal_kredit as tglKredit')
                            ->select('up')
                            ->select('taksiran')
                            ->select('taksiran_atas as taksiranAtas')
                            ->select('taksiran_bawah as taksiranBawah')
                            ->select('bunga')
                            ->select('sewa_modal as sewaModal')
                            ->select('sewa_modal_maksimal as sewaModalMaksimal')
                            ->select('jenisPromo')
                            ->select('status_fcm as statusFcm')
                            ->select('no_order_gosend as noOrderGosend')
                            ->where('kode_booking', $kodeBooking)
                            ->join('user', 'gadai_logam_mulia.user_AIID=user.user_AIID')
                            ->get('gadai_logam_mulia');
            if ($cek->num_rows() > 0) {
                $r = $cek->row();
                $fotoList = json_decode($r->fotoJaminan);
                $resFotoList = array();
                $baseUrl = $this->config->item('base_url');
                if (!empty($fotoList)) {
                    foreach ($fotoList as $f) {
                        $resFotoList[] = $baseUrl.'uploaded/user/gadai/'.$f->nama_foto;
                    }
                }
                $r->fotoJaminan = $resFotoList;
    
                $resKepings = array();
                $kepings = json_decode($r->beratKeping);
                foreach ($kepings as $k) {
                    $resKepings[] = $k->berat_keping;
                }
                
                $r->beratKeping = $resKepings;
                
                return $cek->row();
            }
            return false;
        } elseif ($kodeBooking[0] == "3") {
            $cek = $this->db->select($selectId)
                            ->select('"Handphone" as jaminan')
                            ->select('"EL" as rubrik')
                            ->select('"ELHP" as tipeJaminan')
                            ->select('merek, tipe, kondisi_smartphone as kondisi')
                            ->select('tahun_beli as tahunBeli')
                            ->select('harga_pasaran as hargaPasaran')
                            ->select('foto_json as fotoJaminan')
                            ->select('kode_outlet as kodeOutlet')
                            ->select('FROM_UNIXTIME(jadwal_kedatangan) as  jadwalKedatangan')
                            ->select('kode_booking as kodeBooking')
                            ->select('tanggal_booking as tglBooking')
                            ->select('gadai_handphone.status')
                            ->select('no_kredit as noKredit')
                            ->select('tanggal_kredit as tglKredit')
                            ->select('up')
                            ->select('taksiran')
                            ->select('taksiran_atas as taksiranAtas')
                            ->select('taksiran_bawah as taksiranBawah')
                            ->select('bunga')
                            ->select('sewa_modal as sewaModal')
                            ->select('sewa_modal_maksimal as sewaModalMaksimal')
                            ->select('jenisPromo')
                            ->select('no_order_gosend as noOrderGosend')
                            ->where('kode_booking', $kodeBooking)
                            ->join('ref_smartphone', 'gadai_handphone.id_merek_smartphone=ref_smartphone.id')
                            ->join('user', 'gadai_handphone.user_AIID=user.user_AIID')
                            ->get('gadai_handphone');
            if ($cek->num_rows() > 0) {
                $r = $cek->row();
                $fotoList = json_decode($r->fotoJaminan);
                $resFotoList = array();
                $baseUrl = $this->config->item('base_url');
                foreach ($fotoList as $f) {
                    $resFotoList[] = $baseUrl.'uploaded/user/gadai/'.$f->nama_foto;
                }
                $r->fotoJaminan = $resFotoList;
                
                return $cek->row();
            }
            return false;
        } elseif ($kodeBooking[0] == "4") {
            $cek = $this->db->select($selectId)
                            ->select('"Elektronik" as jaminan')
                            ->select('"EL" as rubrik')
                            ->select('tipe_jaminan as tipeJaminan')
                            ->select('merek, gadai_elektronik.tipe, gadai_elektronik.jenis, kondisi_barang as kondisi')
                            ->select('harga_jual as hargaJual')
                            ->select('foto_json as fotoJaminan')
                            ->select('kode_outlet as kodeOutlet')
                            ->select('FROM_UNIXTIME(jadwal_kedatangan) as  jadwalKedatangan')
                            ->select('kode_booking as kodeBooking')
                            ->select('tanggal_booking as tglBooking')
                            ->select('gadai_elektronik.status')
                            ->select('no_kredit as noKredit')
                            ->select('tanggal_kredit as tglKredit')
                            ->select('up')
                            ->select('taksiran')
                            ->select('taksiran_atas as taksiranAtas')
                            ->select('taksiran_bawah as taksiranBawah')
                            ->select('bunga')
                            ->select('sewa_modal as sewaModal')
                            ->select('sewa_modal_maksimal as sewaModalMaksimal')
                            ->select('jenisPromo')
                            ->select('no_order_gosend as noOrderGosend')
                            ->where('kode_booking', $kodeBooking)
                            ->join('ref_elektronik', 'ref_elektronik.id=gadai_elektronik.id_merek_elektronik')
                            ->join('user', 'gadai_elektronik.user_AIID=user.user_AIID')
                            ->get('gadai_elektronik');
            if ($cek->num_rows() > 0) {
                $r = $cek->row();
                $fotoList = json_decode($r->fotoJaminan);
                $resFotoList = array();
                $baseUrl = $this->config->item('base_url');
                foreach ($fotoList as $f) {
                    $resFotoList[] = $baseUrl.'uploaded/user/gadai/'.$f->nama_foto;
                }
                $r->fotoJaminan = $resFotoList;
                
                return $cek->row();
            }
            return false;
        } elseif ($kodeBooking[0] == "5") {
            $cek = $this->db->select($selectId)
                            ->select('"Kendaraan" as jaminan')
                            ->select('"KN" as rubrik')
                            ->select('"tipeJaminan " as tipeJaminan')
                            ->select('jenis_kendaraan as jenisKendaraan')
                            ->select('merek')
                            ->select('tipe')
                            ->select('deskripsi')
                            ->select('isi_silinder as isiSilinder')
                            ->select('tahun_pembuatan as tahunPembuatan')
                            ->select('harga_jual as hargaJual')
                            ->select('no_polisi as noPolisi')
                            ->select('no_bpkb as noBPKB')
                            ->select('no_stnk as noSTNK')
                            ->select('no_mesin as noMesin')
                            ->select('no_rangka as noRangka')
                            ->select('foto_json as fotoJaminan')
                            ->select('FROM_UNIXTIME(jadwal_kedatangan) as  jadwalKedatangan')
                            ->select('kode_booking as kodeBooking')
                            ->select('kode_outlet as kodeOutlet')
                            ->select('tanggal_booking as tglBooking')
                            ->select('gadai_kendaraan.status')
                            ->select('no_kredit as noKredit')
                            ->select('tanggal_kredit as tglKredit')
                            ->select('up')
                            ->select('taksiran')
                            ->select('taksiran_atas as taksiranAtas')
                            ->select('taksiran_bawah as taksiranBawah')
                            ->select('bunga')
                            ->select('sewa_modal as sewaModal')
                            ->select('sewa_modal_maksimal as sewaModalMaksimal')
                            ->select('jenisPromo')
                            ->select('no_order_gosend as noOrderGosend')
                            ->where('kode_booking', $kodeBooking)
                            ->join('ref_kendaraan', 'ref_kendaraan.id=gadai_kendaraan.id_merek_kendaraan')
                            ->join('user', 'gadai_kendaraan.user_AIID=user.user_AIID')
                            ->get('gadai_kendaraan');
            if ($cek->num_rows() > 0) {
                $r = $cek->row();
                
                if ($r->jenisKendaraan == 'Motor') {
                    $r->tipeJaminan = 'KNSM';
                } elseif ($r->jenisKendaraan == 'Mobil') {
                    $r->tipeJaminan = 'KNML';
                }
                
                $fotoList = json_decode($r->fotoJaminan);
                $resFotoList = array();
                $baseUrl = $this->config->item('base_url');
                foreach ($fotoList as $f) {
                    $resFotoList[] = $baseUrl.'uploaded/user/gadai/'.$f->nama_foto;
                }
                $r->fotoJaminan = $resFotoList;
                
                return $r;
            }
            return false;
        } elseif ($kodeBooking[0] == "6") {
                $cek = $this->db->select($selectId)
                            ->select('"Laptop" as jaminan')
                            ->select('"EL" as rubrik')
                            ->select('"ELKM" as tipeJaminan')
                            ->select('merek')
                            ->select('tipe')
                            ->select('deskripsi')
                            ->select('kapasitas_harddisk as kapasitasHDD')
                            ->select('tipe_processor as tipeProcessor')
                            ->select('kapasitas_ram as kapasitasRAM')
                            ->select('tahun_pembelian as tahunPembelian')
                            ->select('harga_jual as hargaJual')
                            ->select('foto_json as fotoJaminan')
                            ->select('FROM_UNIXTIME(jadwal_kedatangan) as  jadwalKedatangan')
                            ->select('kode_booking as kodeBooking')
                            ->select('kode_outlet as kodeOutlet')
                            ->select('tanggal_booking as tglBooking')
                            ->select('gadai_laptop.status')
                            ->select('no_kredit as noKredit')
                            ->select('tanggal_kredit as tglKredit')
                            ->select('up')
                            ->select('taksiran')
                            ->select('taksiran_atas as taksiranAtas')
                            ->select('taksiran_bawah as taksiranBawah')
                            ->select('bunga')
                            ->select('sewa_modal as sewaModal')
                            ->select('sewa_modal_maksimal as sewaModalMaksimal')
                            ->select('jenisPromo')
                            ->select('no_order_gosend as noOrderGosend')
                            ->where('kode_booking', $kodeBooking)
                            ->join('ref_laptop', 'ref_laptop.id=gadai_laptop.id_merek_laptop')
                            ->join('user', 'gadai_laptop.user_AIID=user.user_AIID')
                            ->get('gadai_laptop');
            if ($cek->num_rows() > 0) {
                $r = $cek->row();
                $fotoList = json_decode($r->fotoJaminan);
                $resFotoList = array();
                $baseUrl = $this->config->item('base_url');
                foreach ($fotoList as $f) {
                    $resFotoList[] = $baseUrl.'uploaded/user/gadai/'.$f->nama_foto;
                }
                $r->fotoJaminan = $resFotoList;
                
                return $cek->row();
            }
            return false;
        } else {
            return false;
        }
    }


    /*GOD
     * STATUS
    0 = 'Ditolak';
    1 = 'Tersedia';
    2 = 'Proses Selesai';
    3 = 'Assesmen';
    4 = 'Batal';
    5 = 'Sedang Dijemput Kurir';
    6 = 'Sedang Diantar Kurir';
    7 = 'Menunggu Konfirmasi Nasabah';
    8 = 'Sudah Dibayar';
    9 = 'Barang Sudah Sampai Outlet';
    91 = 'Cancel Timeout 2 menit';
    92 = Barang Dikembalikan;
    93 = Cancel Timeout 3 menit
     */

    function updateStatusGodCancel($data)
    {
        $dataUpdate = [
            'status' => 4,
        ];

        if ($data['jenisTransaksi'] == 'LM') {
            $this->db->where('kode_booking', $data['kodeBooking'])->update('gadai_logam_mulia', $dataUpdate);
        }
        if ($data['jenisTransaksi'] == 'PH') {
            $this->db->where('kode_booking', $data['kodeBooking'])->update('gadai_perhiasan', $dataUpdate);
        }
    }

    function updateOrderMidtrans(string $kodeBooking, string $tableName, array $data)
    {
        $this->db->where('kode_booking', $kodeBooking)->update($tableName, $data);
    }

    function getDataMidtrans(string $kodeBooking, string $tableName)
    {
        $select = $this->db
            ->select('id_order_midtrans')
            ->select('midtrans_response')
            ->where('kode_booking', $kodeBooking)->get($tableName);

        if ($select->num_rows() <= 0) {
            return false;
        }

        return $select->row();
    }
}
