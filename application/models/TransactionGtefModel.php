<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property BankModel bank_model
 */
class TransactionGtefModel extends CI_Model
{
    const TYPE = 'GTEF';
    const TRX_CODE = 43;
    const TRX_TYPE_OPEN = 'OP';

    protected static $table = 'transaction_gtef';
    protected $created_at = 'created_at';
    protected $updated_at = 'updated_at';

    public function __construct()
    {
        parent::__construct();

        $this->load->model('BankModel', 'bank_model');
        $this->load->model('GcashModel', 'gcash_model');
    }

    public function save(array $store_data)
    {
        $store_data = self::setDate($store_data, $this->created_at);
        $this->db->trans_start();
        $this->db->insert(self::$table, $store_data);
        $id = $this->db->insert_id();

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return null;
        }

        $this->db->trans_complete();

        if (empty($item = $this->findById($id))) {
            return null;
        }

        return $item;
    }

    public function findById($id)
    {
        return self::find(['id' => $id]);
    }

    public function find($query)
    {
        $item = $this->db->where($query)->get(self::$table);

        if (empty($item->num_rows())) {
            return null;
        }

        $item = $item->row();
        
        if (!empty($item->bank_id)) {
            $bank = $this->bank_model->findById($item->bank_id);
            $item->bank = $bank;
        }

        if (!empty($item->gcash_id)) {
            $bank = $this->gcash_model->getVaDetailsById($item->gcash_id);
            $item->bank = $bank;
        }

        return $item;
    }

    public function updateById($id, $update_data)
    {
        $update_data = self::setDate($update_data, $this->updated_at);
        $this->db->trans_start();
        $this->db->where('id', $id)->update(self::$table, $update_data);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return null;
        }

        $this->db->trans_complete();

        if (empty($item = $this->findById($id))) {
            return null;
        }

        return $item;
    }

    private function setDate($data, $key)
    {
        $data[$key] = date('Y-m-d H:i:s');

        return $data;
    }
}
