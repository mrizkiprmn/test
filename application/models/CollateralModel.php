<?php

defined('BASEPATH') or exit('No direct script access allowed');

class CollateralModel extends CI_Model
{
    /**
     * @param array $query
     * @return array
     */
    function get(array $query = [])
    {
        $table = $this->db;
        if (!empty($query)) {
            $table = $table->where($query);
        }

        $item = $table->get('collateral');

        if ($item && $item->num_rows() > 0) {
            $collateral = $item->result();
            foreach ($collateral as $key => $item) {
                $items = new stdClass();
                $items->id = $item->id;
                $items->financing_id = $item->financing_id;
                $items->type = $item->type;
                $items->detail = json_decode($item->detail);
                $items->created_at = $item->created_at;
                $collaterals[] = $items;
            }
        }

        return $collaterals ?? [];
    }

    function find(array $query = [])
    {
        $table = $this->db;

        if (!empty($query)) {
            $table = $table->where($query);
        }

        $item = $table->get('collateral');

        if ($item && $item->num_rows() > 0) {
            $collateral = $item->row();
            $collateral->detail = json_decode($collateral->detail);
        }

        return $collateral ?? null;
    }

    function insert(array $fields)
    {
        $fields = [
            'financing_id' => $fields['financing_id'],
            'type'         => $fields['type'],
            'detail'       => json_encode($fields['detail']),
            'created_at'   => date('Y-m-d H:i:s')
        ];

        $this->db->trans_begin();
        $this->db->insert('collateral', $fields);
        $this->db->trans_complete();

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();

            return false;
        }

        $this->db->trans_commit();

        return $this->db->insert_id();
    }

    function updateByID($id, array $fields)
    {
        $this->db->trans_begin();
        $this->db->where('id', $id)->update('collateral', $fields);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }

        $this->db->trans_commit();

        return $this->find(['id' => $id]);
    }

    function dropByFinancingID($id)
    {
         $this->db->where('financing_id', $id)->delete('collateral');

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();

            return false;
        }

        $this->db->trans_commit();
    }
}
