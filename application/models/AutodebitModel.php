<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AutodebitModel extends CI_Model
{
    protected $table = 'history_register_autodebit';

    const STATUS_REGIS = 1;
    const STATUS_UNREGIS = 0;
    const STATUS_PENDING = 'pending_otp';
    const STATUS_PENDING_REGIS = 'pending_register';
    const STATUS_SUCCESS = 'active';
    const STATUS_FAIL = 'not_active';

    public function add($data)
    {
        $save_data = [
            "user_id" => $data['user_id'],
            "user_name" => $data['user_name'],
            "no_hp" => $data['no_hp'],
            "type_transaksi" => $data['jenis_transaksi'],
            "transaksi_name" => $data['transaksi_name'],
            "no_kredit" => $data['no_kredit'],
            "kode_bank" => $data['kode_bank'],
            "bank_name" => $data['bank_name'],
            "norek_bank" => $data['norek_bank'],
            "total_debit" => $data['jumlah_debit'],
            "time_interval_debit" => $data['jangka_waktu_debit'],
            "date_debit" => $data['tanggal_debit'],
            "product_code" => $data['product_code'],
            "product_name" => $data['product_name'],
            "id_reg" => $data['id_reg'],
            "status_regis" => self::STATUS_REGIS,
            "start_regis" => $data['start_regis'],
            "exp_regis" => $data['exp_regis'],
            "start_date_debit" => $data['start_date_debit'],
            "exp_date_debit" => $data['exp_date_debit'],
        ];

        $this->db->trans_begin();
        $this->db->insert($this->table, $save_data);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }

        $this->db->trans_commit();
    }

    //ini berguna untuk list regis auto debit by user_id
    public function find(array $query = [])
    {
        $data = null;
        $table = $this->db;

        if (!empty($query)) {
            $table = $table->where($query);
        }

        $item = $table->get($this->table);

        if ($item && $item->num_rows() > 0) {
            $data = $item->result();
        }

        return $data;
    }

    //ini berguna untuk cek data by user_id dan no_kredit
    public function cekNoKreditByUserId($user_id, $no_kredit)
    {
        $where = "user_id = $user_id AND no_kredit = $no_kredit ";

        $item = $this->db->where($where)->get($this->table);

        if ($item->num_rows() > 0) {
            return $item->result();
        }

        return null;
    }

    //ini berguna untuk update data yang melakukan resend register by no_kredit
    public function updateRegis($no_kredit, $data)
    {
        $this->db->trans_begin();
        $this->db->where('no_kredit', $no_kredit)->update($this->table, $data);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }

        $this->db->trans_commit();
    }

    //ini berguna untuk cek data by id_reg
    public function cekIdReg($id_reg)
    {
        $item = $this->db->where('id_reg', $id_reg)->get($this->table);

        if (empty($item->num_rows())) {
            return null;
        }

        return $item->row();
    }

    public function updateByIdReg($data)
    {
        $update_success_otp = [
            "id_reg" => $data->id_reg,
            "flag_otp" => self::STATUS_SUCCESS,
            "status_debit" => self::STATUS_SUCCESS,
            "date_next_debit" => $data->date_next_debit,
            "date_active_debit" => date('Y-m-d H:i:s', strtotime($data->con_tgl))
        ];

        $this->db->trans_begin();
        $this->db->where('id_reg', $data->id_reg)->update($this->table, $update_success_otp);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }

        $this->db->trans_commit();
    }

    //jika waktu sekarang lebih besar dari date_next_debit maka akan update + 1 bulan
    public function updateNextDebit($data)
    {
        $update_next_debit = [
            "date_next_debit" => $data->update_next_debit
        ];

        $this->db->trans_begin();
        $this->db->where('id_reg', $data->id_reg)->update($this->table, $update_next_debit);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }

        $this->db->trans_commit();
    }

    //jika waktu exp_debit nya sudah terlewati maka update status regis = 0 && debit = tidak aktif
    public function updateStatusExpDebit($data)
    {
        $update_status_exp = [
            "status_regis" => self::STATUS_UNREGIS,
            "status_debit" => self::STATUS_FAIL,
            "date_next_debit" => null
        ];

        $this->db->trans_begin();
        $this->db->where('id_reg', $data->id_reg)->update($this->table, $update_status_exp);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }

        $this->db->trans_commit();
    }

    //ini berguna untuk cek data by user_id dan no_kredit
    public function cekFlagOtp($user_id, $flag_otp)
    {
        $where = "user_id = $user_id AND flag_otp = '$flag_otp'";

        $item = $this->db->where($where)->get($this->table);

        if (empty($item->num_rows())) {
            return null;
        }

        $data = $item->row();
        $auto_debit = new stdClass();
        $auto_debit->user_id = $data->user_id;
        $auto_debit->transaksi_name = $data->transaksi_name;
        $auto_debit->jenis_transaksi = $data->type_transaksi;
        $auto_debit->product_name = $data->product_name;
        $auto_debit->product_code = $data->product_code;
        $auto_debit->kode_bank = $data->kode_bank;
        $auto_debit->nama_pemilik_bank = $data->user_name;
        $auto_debit->norek_bank = $data->norek_bank;
        $auto_debit->bank_name = $data->bank_name;
        $auto_debit->no_kredit = $data->no_kredit;
        $auto_debit->no_hp = $data->no_hp;
        $auto_debit->jumlah_debit = $data->total_debit;
        $auto_debit->jangka_waktu_debit = $data->time_interval_debit;
        $auto_debit->tanggal_debit = $data->date_debit;
        $auto_debit->status_otp = $data->flag_otp;
        $auto_debit->id_reg = $data->id_reg;
        $auto_debit->screen_name = self::getScreenName($data->flag_otp);
        
        return $auto_debit;
    }

    function getScreenName($flag_otp)
    {
        if ($flag_otp == 'pending_otp') {
            return 'AutoDebitOtp_03';
        }
        if ($flag_otp == 'pending_register') {
            return 'AutoDebitRegistrasi_01';
        }
        return null;
    }

    //jika status otp = pending_otp dan besar dari 15 menit
    public function updateStatusOtp($data)
    {
        $update_next_debit = [
            "status_regis" => self::STATUS_UNREGIS,
            "flag_otp" => self::STATUS_PENDING_REGIS
        ];

        $this->db->trans_begin();
        $this->db->where('id_reg', $data->id_reg)->update($this->table, $update_next_debit);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return null;
        }
        
        $this->db->trans_commit();
    }
 
    //jika status otp = pending_register dan besar dari 24 jam
    public function deleteAutoDebit($data)
    {
        return $this->db->where('id', $data->id)->delete($this->table);
    }
}
