<?php

defined('BASEPATH') or exit('No direct script access allowed');

class MasterJewelryModel extends CI_Model
{
    protected $table = 'master_jewelry';

    function get(array $query = [])
    {
        $table = $this->db;
        if (!empty($query)) {
            $table = $table->where($query);
        }

        $item = $table->get($this->table);

        if ($item && $item->num_rows() > 0) {
            $rows = $item->result();
        }

        return $rows ?? [];
    }

    function find(array $query = [])
    {
        $table = $this->db;

        if (!empty($query)) {
            $table = $table->where($query);
        }

        $item = $table->get($this->table);

        if ($item && $item->num_rows() > 0) {
            $row = $item->row();
        }

        return $row ?? null;
    }
}
