<?php

defined('BASEPATH') or exit('No direct script access allowed');

class UserPinModel extends CI_Model
{
    protected static $table = 'user_pin';
    protected $created_at = 'created_at';
    protected $updated_at = 'updated_at';

    function insert(array $fields)
    {
        $fields = self::setDate($fields, $this->created_at);
        $fields = self::setDate($fields, 'last_access_time');
        $this->db->trans_start();
        $this->db->insert(self::$table, $fields);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return null;
        }

        $this->db->trans_complete();

        if (empty($item = $this->find(['user_id' => $fields['user_id']]))) {
            return null;
        }

        return $item;
    }

    function find($query)
    {
        $item = $this->db->where($query)->get(self::$table);

        if ($item && $item->num_rows() > 0) {
            $item = $item->row();
            return $this->setCounterDefault($item);
        }

        return null;
    }

    function updateByUserId($user_id, array $fields)
    {
        $fields = self::setDate($fields, $this->updated_at);
        $fields = self::setDate($fields, 'last_access_time');

        if (!empty($fields['is_blocked'])) {
            $fields = self::setDate($fields, 'blocked_date');
        }

        $this->db->trans_begin();
        $this->db->where('user_id', $user_id)->update(self::$table, $fields);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }

        $this->db->trans_commit();

        return $this->find(['user_id' => $user_id]);
    }

    function checkPin($user_id)
    {
        $user_pin = self::find(['user_id' => $user_id]);

        if (empty($user_pin)) {
            return false;
        }

        return $user_pin->is_blocked;
    }

    function unBlockedPinByUserId($user_id)
    {
        return self::updateByUserId($user_id, ['is_blocked' => false, 'blocked_date' => null, 'counter' => 0]);
    }

    private function setDate($data, $key)
    {
        $data[$key] = date('Y-m-d H:i:s');

        return $data;
    }

    private function setCounterDefault($item)
    {
        $last_access_time = new DateTime($item->last_access_time);
        $last_access_time = $last_access_time->format('Y-m-d');
        $time_now = date('Y-m-d');

        if ($last_access_time < $time_now) {
            return self::updateByUserId($item->user_id, ['counter' => 0]);
        }

        return $item;
    }
}
