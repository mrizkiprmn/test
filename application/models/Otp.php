<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Otp extends CI_Model
{
    const REGISTRASI = 'registrasi_user';
    const VERIFIKASI_NASABAH = 'verifikasi_nasabah';
    const GANTI_PIN = 'ganti_pin';

    public function insert($userId, $otp, $type, $data)
    {
        $this->db->insert('otp', array(
            'user_AIID' => $userId,
            'tipe_otp' => $type,
            'konten_otp' => $otp,
            'data_otp'=> $data
        ));
        
        return $this->db->insert_id();
    }

    public function get($userId, $otp, $type)
    {
        $where = array(
            'user_AIID' => $userId,
            'konten_otp' => $otp,
            'tipe_otp' => $type
        );
        return $this->db->where($where)->get('otp');
    }
    
    public function otpData($id)
    {
        return $this->db->where('otp_AIID', $id)->get('otp');
    }
}
