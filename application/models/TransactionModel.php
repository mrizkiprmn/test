<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * @property User user_model
 */
class TransactionModel extends CI_Model
{
    /**
     * @var
     */
    protected static $table = 'transactions';
    protected $created_at = 'created_at';
    protected $updated_at = 'updated_at';

    /**
     * TransactionModel constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('User', 'user_model');
    }

    public function save(array $store_data)
    {
        $store_data = self::setDate($store_data, $this->created_at);
        $store_data = self::setDate($store_data, 'date');
        $this->db->trans_start();
        $this->db->insert(self::$table, $store_data);
        $id = $this->db->insert_id();

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return null;
        }

        $this->db->trans_complete();

        if (empty($item = self::findById($id))) {
            return null;
        }

        return $item;
    }

    public function find($query)
    {
        $item = $this->db->where($query)->get(self::$table);

        if (empty($item->num_rows())) {
            return null;
        }

        return $item->row();
    }

    public function findById($id)
    {
        $item = $this->db->where('id', $id)->get(self::$table);

        if (empty($item->num_rows())) {
            return null;
        }

        return $item->row();
    }

    public function updateById($id, $update_data)
    {
        $update_data = self::setDate($update_data, $this->updated_at);
        $this->db->trans_start();
        $this->db->where('id', $id)->update(self::$table, $update_data);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return null;
        }

        $this->db->trans_complete();

        if (empty($item = $this->findById($id))) {
            return null;
        }

        return $item;
    }

    public function findByReffSwitchingGtef($reff_switching)
    {
        $where = array('reff_switching' => $reff_switching, 'relation_table' => 'transaction_gtef');
        $item = $this->db->select('*')
            ->join('transaction_gtef', 'transaction_gtef.id='.self::$table.'.relation_id')
            ->join('rekening_bank', 'rekening_bank.rekening_bank_AIID=transaction_gtef.bank_id')
            ->join('ref_bank', 'ref_bank.kode=rekening_bank.kode_bank')
            ->where($where)
            ->get(self::$table);

        if (empty($item->num_rows())) {
            return null;
        }

        return $item->row();
    }

    private function setDate($data, $key)
    {
        $data[$key] = date('Y-m-d H:i:s');

        return $data;
    }
}
