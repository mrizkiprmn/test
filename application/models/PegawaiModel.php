<?php

defined('BASEPATH') or exit('No direct script access allowed');

class PegawaiModel extends CI_Model
{
    function get(array $query = [])
    {
        $table = $this->db;

        if (!empty($query)) {
            $table = $table->where($query);
        }

        $cek = $table->get('pegawai');

        return $cek->num_rows() > 0 ? $cek->row() : false;
    }

    function insert($fields = [])
    {
        $this->db->trans_start();
        $this->db->insert('pegawai', $fields);
        $this->db->trans_complete();

        if ($this->db->trans_status() === false) {
            return 'gagal';
        }

        return 'sukses';
    }

    function deleteAll()
    {
        $this->db->trans_start();
        $this->db->empty_table('pegawai');
        $this->db->trans_complete();
    }

    function getIsEmployeeByUser($cif, $phone)
    {
        $where = [];

        if (!empty($cif)) {
            $where['cif'] = $cif;
        }

        if (!empty($phone)) {
            $where['hp_1'] = $phone;
            $where['hp_2'] = $phone;
        }

        if (!empty($where)) {
            $employee = $this->db->select('id')->or_where($where)->get('pegawai');
            $is_employee = $employee->num_rows() > 0;
        }

        return $is_employee ?? false;
    }

    function getEmployeeByUser($user)
    {
        if (!empty($user->cif)) {
            $where['cif'] = $user->cif;
        }

        if (!empty($user->no_hp)) {
            $where['hp_1'] = $user->no_hp;
            $where['hp_2'] = $user->no_hp;
        }

        if (!empty($where)) {
            $employee = $this->db->or_where($where)->get('pegawai');
            $is_employee = $employee->num_rows() > 0 ? $employee->row() : false;
        }

        return $is_employee ?? false;
    }
}
