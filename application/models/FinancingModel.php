<?php

defined('BASEPATH') or exit('No direct script access allowed');

class FinancingModel extends CI_Model
{
    public static $status_end = ['6.0', '7.0', '8.0', '9.0', '9.3'];
    public static $status_end_notification = ['3', '5', '6'];

    function __construct()
    {
        parent::__construct();

        $this->load->model('CollateralModel');
        $this->load->helper('pegadaian_helper');
    }

    function get(array $query = [], $selects = '')
    {
        $financings = [];
        $table = $this->db;

        if (!empty($selects)) {
            $table = $table->select($selects);
        }

        if (!empty($query)) {
            $table = $table->where($query);
        }

        $item = $table->get('financing');

        if ($item && $item->num_rows() > 0) {
            $financings = $item->result();
        }

        foreach ($financings as $key => $item) {
            $financing = $item;
            $financing->status_history = !empty($financing->status_history) ? json_decode($financing->status_history) : [];
            $financing->collaterals = $this->CollateralModel->get(['financing_id' => $financing->id]);
            $financings[] = $financing;
        }

        return $financings;
    }

    function find(array $query = [])
    {
        $financing = null;
        $table = $this->db;

        if (!empty($query)) {
            $table = $table->where($query);
        }

        $item = $table->get('financing');

        if ($item && $item->num_rows() > 0) {
            $financing = $item->row();
        }

        if (!empty($financing)) {
            $financing->status_history = !empty($financing->status_history) ? json_decode($financing->status_history) : [];
            $financing->collaterals = $this->CollateralModel->get(['financing_id' => $financing->id]);
        }

        return $financing;
    }

    function insert(array $fields)
    {
        $fields = [
            'id' => $id = strtotime(date('Y-m-d H:i:s')),
            'user_id' => $fields['user_id'],
            'code_booking' => $fields['code_booking'] ?? null,
            'code_outlet' => $fields['code_outlet'] ?? null,
            'loan_amount' => $fields['loan_amount'] ?? null,
            'tenor' => $fields['tenor'] ?? null,
            'status' => $fields['status'] ?? 0,
            'is_have_business' => (bool)$fields['is_have_business'],
            'date' => date('Y-m-d H:i:s'),
            'created_at' => date('Y-m-d H:i:s'),
            'update_at' => date('Y-m-d H:i:s')
        ];

        $this->db->trans_begin();
        $this->db->insert('financing', $fields);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }

        $this->db->trans_commit();

        return $this->find(['id' => $id]);
    }

    function updateByID($id, array $fields)
    {
        $this->db->trans_begin();
        $this->db->where('id', $id)->update('financing', $fields);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }

        $this->db->trans_commit();

        return $this->find(['id' => $id]);
    }

    function updateByCodeBooking($code_booking, array $fields)
    {
        $this->db->trans_begin();
        $this->db->where('code_booking', $code_booking)->update('financing', $fields);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }

        $this->db->trans_commit();

        return true;
    }

    function getFinancingIsSentActive(array $query = [])
    {
        $financings = [];
        $table = $this->db;
        $table = $table->select('id, user_id, code_product');
        $table = $table->select('code_booking, code_outlet, date, tenor, installment, status_desc, pic_current_status');
        $table = $table->select('loan_amount, tenor, status, status_history, response_los, request_los');
        $table = $table->select('is_have_business, created_at, update_at');

        if (!empty($query)) {
            $table = $table->where($query)->order_by('created_at', 'desc');
        }

        $item = $table->get('financing');
        ;

        if ($item && $item->num_rows() > 0) {
            $financings = $item->result();
        }

        foreach ($financings as $key => $item) {
            $financing = new stdClass();
            $financing->id = $item->id;
            $financing->type = 'financing';
            $financing->code_booking = $item->code_booking;
            $financing->status_current = $item->status;
            $financing->status_desc = $item->status_desc;
            $financing->pic_current_status = $item->pic_current_status ?? "";
            $financing->loan_amount = Pegadaian::currencyIdr($item->loan_amount);
            $financing->installment = Pegadaian::currencyIdr($item->installment);
            $financing->tenor = $item->tenor . ' Bulan';
            $financing->created_at = $item->created_at ?? "";
            $financing->update_date = $item->update_at ?? "";
            $financings[] = $financing;
            $data[] = $financing;
        }

        return $data ?? [];
    }

    function getFinancingByCodeBooking(array $query = [])
    {
        $model = $this->db->select('id, user_id, code_product')
            ->select('code_booking, code_outlet, date, tenor, installment')
            ->select('loan_amount, tenor, status, status_history, status_desc, response_los, request_los')
            ->select('is_have_business, created_at')
            ->where($query)
            ->get('financing');

        if ($model && $model->num_rows() > 0) {
            $financing = $model->row();
            $request_los =  $financing->request_los ?? null;
            $financing->collaterals = $this->CollateralModel->get(['financing_id' => $financing->id]);
            $financing->request_los = !empty($request_los) ? json_decode($request_los) : null;
        }

        return $financing ?? null;
    }

    function getFinancingActive(array $query)
    {
        $model = $this->db->select('id, user_id, code_product')
            ->select('code_booking, code_outlet, date, tenor, installment')
            ->select('loan_amount, tenor, status, status_history, status_desc, response_los, request_los')
            ->select('is_have_business, created_at')
            ->where($query)
            ->where_not_in('status', self::$status_end)
            ->get('financing');

        if ($model && $model->num_rows() > 0) {
            $financing = $model->row();
            $financing->collaterals = $this->CollateralModel->get(['financing_id' => $financing->id]);
        }

        return $financing ?? null;
    }
}
