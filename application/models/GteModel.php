<?php

defined('BASEPATH') or exit('No direct script access allowed');

class GteModel extends CI_Model
{
    const PRODUCT_CODE = '32';

    function checkPromo($jenisPromo)
    {
        $where = 'expired >= NOW() AND jenisPromo = "' . $jenisPromo . '"';

        return $this->db
            ->where($where)
            ->get('ref_gte_promo')
            ->result();
    }

    function getPaymentByReffSwitching($reff_switching)
    {
        $payment = $this->db
            ->select('user.user_AIID')
            ->select('user.email')
            ->select('user.no_hp')
            ->select('user.fcm_token')
            ->select('gte.jenisTransaksi')
            ->select('gte.productCode as product_code')
            ->select('gte.user_AIID')
            ->select('gte.tglTransaksi as payment_date')
            ->from('gte')
            ->join('user', 'gte.user_AIID = user.user_AIID')
            ->where('reffSwitching', $reff_switching)
            ->get();

        if ($payment && $payment->num_rows() > 0) {
            return $payment->row();
        }

        return null;
    }
}
