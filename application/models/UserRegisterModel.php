<?php

defined('BASEPATH') or exit('No direct script access allowed');

class UserRegisterModel extends CI_Model
{
    protected static $table = 'user_register';
    protected $created_at = 'created_at';

    function insert(array $fields)
    {
        $fields = self::setDate($fields, $this->created_at);
        $fields['id'] = $id = md5(Pegadaian::getMillSecID());

        $this->db->trans_start();
        $this->db->insert(self::$table, $fields);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return null;
        }

        $this->db->trans_complete();

        if (empty($item = $this->find(['id' => $id]))) {
            return null;
        }

        return $item;
    }

    function find($query)
    {
        $item = $this->db->where($query)->get(self::$table);

        if ($item && $item->num_rows() > 0) {
            return $item->row();
        }

        return null;
    }

    function drop($query)
    {
        $this->db->where($query)->delete(self::$table);
    }

    private function setDate($data, $key)
    {
        $data[$key] = date('Y-m-d H:i:s');

        return $data;
    }
}
