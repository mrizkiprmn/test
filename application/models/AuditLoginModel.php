<?php

defined('BASEPATH') or exit('No direct script access allowed');

class AuditLoginModel extends CI_Model
{
    protected static $table = 'audit_login';
    protected $created_at = 'created_at';

    function insert(array $fields)
    {
        $fields = self::setDate($fields, $this->created_at);

        $this->db->trans_start();
        $this->db->insert(self::$table, $fields);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }

        $this->db->trans_complete();

        return true;
    }

    function find(array $query = [], $limit = "")
    {
        $audit = $this->db->select('id');

        if (!empty($query)) {
            $audit = $audit->where($query);
        }

        if (!empty($limit)) {
            $audit = $audit->limit($limit);
        }

        $item = $audit->get(self::$table);

        if ($item && $item->num_rows() > 0) {
            return $item->row();
        }

        return null;
    }

    private function setDate($data, $key)
    {
        $data[$key] = date('Y-m-d H:i:s');

        return $data;
    }
}
