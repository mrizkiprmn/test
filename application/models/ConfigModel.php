<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ConfigModel extends CI_Model
{
    const KODE_BANK_PAYMENT_METHOD = [
        'BNI' => '009',
        'MANDIRI' => '008',
        'WALLET' => '',
        'VA_BCA' => '014',
        'VA_MANDIRI' => '008',
        'VA_PERMATA' => '013',
        'VA_BRI' =>  '002'
    ];
    protected static $table = 'config';

    public function getConfig()
    {
        return $this->db->get('config')->row();
    }

    public function getBiayaPayment($jenisTransaksi, $payment, $productCode, $kodeBank)
    {
        $where = array(
            'jenisTransaksi' => $jenisTransaksi,
            'payment' => $payment,
            'productCode' => $productCode,
            'kodeBank' => $kodeBank
        );

        $cek = $this->db->where($where)->get('ref_biaya_payment');

        if ($cek->num_rows() == 0) {
            return 0;
        }

        $jumlah = $cek->row()->jumlah;
        return $jumlah;
    }

    public function getRealBiayaPayment($jenisTransaksi, $payment, $productCode, $kodeBank)
    {
        $where = array(
            'jenisTransaksi' => $jenisTransaksi,
            'payment' => $payment,
            'productCode' => $productCode,
            'kodeBank' => $kodeBank
        );

        $cek = $this->db->where($where)->get('ref_biaya_payment');

        if ($cek->num_rows() > 0) {
            return $cek->row()->jumlahReal;
        }

        return 0;
    }

    /**
     * Method untuk mendapatkan minimal gram transfer emas
     */
    function getMinimumTransferEmas()
    {
        $cek = $this->db->select('cast(value as double) as minimum')
            ->where('variable', 'minimum_transfer_emas')
            ->get('config');

        if ($cek->num_rows() > 0) {
            $minimum = $cek->row()->minimum;
            return number_format($minimum, 4);
        } else {
            return 1.000;
        }
    }

    /**
     * Method untuk mendapatkan minimal gram transfer emas
     */
    function getMinimumCetakEmas()
    {
        $cek = $this->db->select('cast(value as double) as minimum')
            ->where('variable', 'minimum_cetak_emas')
            ->get('config');

        if ($cek->num_rows() > 0) {
            $minimum = $cek->row()->minimum;
            return number_format($minimum, 4);
        } else {
            return 1.000;
        }
    }

    /**
     * Method untuk mendapatkan minimal gram transfer emas
     */
    function getMinimumBuybackEmas()
    {
        $cek = $this->db->select('cast(value as double) as minimum')
            ->where('variable', 'minimum_cetak_emas')
            ->get('config');

        if ($cek->num_rows() > 0) {
            $minimum = $cek->row()->minimum;
            return number_format($minimum, 4);
        } else {
            return 1.000;
        }
    }

    function getNamaProduk($jenisTransaksi, $kodeProduct)
    {
        $cek = $this->db->where(array(
            'productCode' => $kodeProduct,
            'jenisTransaksi' => $jenisTransaksi
        ))->get('ref_biaya_payment');

        if ($cek->num_rows() > 0) {
            return $cek->row()->productName;
        }

        return "";
    }

    /**
     * Get redaksi tata cara pembayaran berdasarkan jenis pembayaran
     * @param  string $bank   kode bank
     * @param  string $va     nomor va
     * @param  integer $amount jumlah bayar
     * @return array         daftar tata cara bayar
     */
    function getRedaksiPayment($bank, $va = null, $amount = null)
    {
        $data = [];
        $cek = $this->db->where('kode_bank', $bank)->order_by('index', 'asc')
            ->get('ref_step_payment');
        foreach ($cek->result() as $s) {
            // Replace {{noVa}} with $va
            $desc = str_replace('{{noVa}}', $va, $s->deskripsi);
            // Replace {{nominal}}
            $desc = str_replace('{{nominal}}', $amount, $desc);
            // Replace paragraph tag
            $desc = str_replace('<p>', '', $desc);
            // Replace closing pragraph tag
            $desc = str_replace('</p>', '', $desc);

            $data[] = [
                'title' => $s->judul,
                'description' => $desc
            ];
        }

        return $data;
    }

    function getMsGcashRefreshToken()
    {
        $cek = $this->db->where('variable', 'msgcash_refresh_token')->get('config');
        if ($cek->num_rows() > 0) {
            return $cek->row()->value;
        } else {
            return '';
        }
    }

    function updateCoreAccessToken($token)
    {
        $key_config = getenv('CORE_KEY_CONFIG_TOKEN') ?: 'core_access_token';
        return $this->db
            ->where('variable', $key_config)
            ->update('config', ['value' => $token]);
    }

    function getCoreToken()
    {
        $key_config = getenv('CORE_KEY_CONFIG_TOKEN') ?: 'core_access_token';
        $token = $this->db->where('variable', $key_config)->get('config');

        if ($token->num_rows() > 0) {
            return $token->row()->value;
        }

        return null;
    }

    function getTelfonCustomerCare()
    {
        $telfonCustomerCare = $this->db->where('variable', 'telfon_customer_care')->get('config');

        if ($telfonCustomerCare->num_rows() > 0) {
            return $telfonCustomerCare->row()->value;
        }

        return null;
    }

    function getLosToken()
    {
        $token = $this->db->where('variable', 'los_access_token')->get('config');

        if ($token->num_rows() > 0) {
            return $token->row();
        }

        return null;
    }

    function updateLosAccessToken($token, $expire)
    {
        $date = new DateTime($expire);
        return $this->db->where('variable', 'los_access_token')
            ->update('config', [
                'value' => $token,
                'last_update' => $date->format('Y-m-d H:i:s')
            ]);
    }

    function getLosP4dToken()
    {
        $token = $this->db->where('variable', 'los_p4d_access_token')->get('config');

        if ($token->num_rows() > 0) {
            return $token->row();
        }

        return null;
    }

    function updateLosP4dAccessToken($token, $expire)
    {
        $date = new DateTime($expire);

        if (empty($this->getLosP4dToken())) {
            return $this->db->insert('config', [
                'value' => $token,
                'last_update' => $date->format('Y-m-d H:i:s'),
                'variable' => 'los_p4d_access_token',
                'description' => 'Access token untuk LOS P4D'
            ]);
        }

        return $this->db->where('variable', 'los_p4d_access_token')->update('config', [
            'value' => $token,
            'last_update' => $date->format('Y-m-d H:i:s')
        ]);
    }

    function getLimitSubmitEkycDukcapil()
    {
        $config = $this->db->where('variable', 'limit_submit_ekyc_dukcapil')->get('config');

        if ($config->num_rows() > 0) {
            $config = $config->row();
            $item = new stdClass();
            $item->limit = $config->value;
            $item->last_access = date('Y-m-d', strtotime($config->value_2));
        }

        return $item ?? null;
    }

    function updateLastAccessSubmitEkycDukcapil()
    {
        return $this->db->where('variable', 'limit_submit_ekyc_dukcapil')->update('config', [
            'value_2' => date('Y-m-d')
        ]);
    }

    function updateLimitSubmitEkycDukcapil($is_limit)
    {
        return $this->db->where('variable', 'limit_submit_ekyc_dukcapil')->update('config', [
            'value' => $is_limit,
            'value_2' => date('Y-m-d')
        ]);
    }

    function getClientIdEkyc()
    {
        $config = $this->db->where('variable', 'client_id_ekyc')->get('config');

        if ($config->num_rows() > 0) {
            $config = $config->row();
            $item = explode(',', $config->value);
        }

        return $item ?? null;
    }

    function whereByVariable($variable)
    {
        $item = $this->db->where(['variable' => $variable])->get(self::$table);

        if ($item && $item->num_rows() > 0) {
            return $item->row();
        }

        return null;
    }
}
