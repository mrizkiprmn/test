<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MainModel extends CI_Model
{
    function getFrontBanner()
    {
        return $this->db->where('use', 1)->order_by('id', 'desc')->get('ref_promo')->result();
    }

    function getFluktuasiEmas()
    {
        return $this->db->select('harga_emas_AIID as id, harga_jual as hargaJual, harga_beli as hargaBeli, UNIX_TIMESTAMP(waktu_update)*1000 as lastUpdate')
            ->order_by('waktu_update', 'desc')
            ->limit(7)
            ->get('harga_emas')
            ->result();
    }

    function getProductInfo()
    {
        return $this->db->get('produk')->row()->deskripsi;
    }


    function getLastHargaEmas()
    {
        $cek =  $this->db->limit(1)->order_by('harga_emas_AIID', 'desc')->get('harga_emas');
        if ($cek->num_rows() > 0) {
            $date = new DateTime($cek->row()->waktu_update);
            return array(
                'hargaJual' => $cek->row()->harga_jual,
                'hargaBeli' => $cek->row()->harga_beli,
                'tglBerlaku' => $date->format('d-m-Y')
            );
        }

        return array(
            'hargaJual' => 0,
            'hargaBeli' => 0,
            'tglBerlaku' => date('d-m-Y')
        );
    }

    function getShortCutBanner()
    {
        return $this->db->select('nama, gambar, screen_name')
            ->where('use', '1')
            ->order_by('index', 'asc')
            ->get('ref_banner_shortcut')
            ->result();
    }
}
