<?php

defined('BASEPATH') or exit('No direct script access allowed');

class AuditLogModel extends CI_Model
{
    protected $table = 'audit_log';

    // init const value
    const PLATFORM_PDS = 'pds';
    const PLATFORM_ADMIN = 'admin';
    const PLATFORM_PASSION = 'passion';

    const MODUL_RESET_PASSWORD = 'RESET PASSWORD';
    const MODUL_RESET_PIN = 'RESET PIN';
    const MODUL_LINK_CIF = 'LINK CIF';
    const MODUL_UNLINK_CIF = 'UNLINK CIF';
    const MODUL_AKTIFASI_FINANSIAL = 'AKTIFASI FINANSIAL';

    const ACTION_CHANGE = 'CHANGE';
    const ACTION_REQUEST = 'REQUEST';

    function add($data, array $other_property = [])
    {
        if (!empty($other_property)) {
            $data['other_property'] = json_encode($other_property);
        }

        $data['business_date'] = date('Y-m-d H:i:s');
        $data['change_date'] = date('Y-m-d H:i:s');

        $this->db->trans_begin();
        $this->db->insert($this->table, $data);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
        }

        $this->db->trans_commit();
    }
}
