<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * @property ArrumHajiAgunanModel arrum_haji_agunan_model
 * @property User user_model
 */
class ArrumHajiModel extends CI_Model
{
    protected $table = 'booking_arrum_haji';

    // init const value
    const LOGAM_MULIA = 'logam mulia';
    const PERHIASAN = 'perhiasan';
    const LOGAM_MULIA_KARAT = '24';
    const PRODUCT_CODE = "17";
    const TRX_TYPE_OPEN = "OP";
    const ARRUM_HAJI_TEXT = "ARRUM HAJI";

    public function __construct()
    {
        parent::__construct();

        $this->load->model('ArrumHajiAgunanModel', 'arrum_haji_agunan_model');
        $this->load->model('User', 'user_model');
    }

    function add($data, $token)
    {
        while ($this->isIdExist($data['booking_id']) == true) {
            $data['booking_id'] = $this->getNewId($data['id_outlet_pengajuan']);
        }

        $fields = [
            'booking_id' => $data['booking_id'],
            'user_id' => $token->id,
            'user_name' => $token->nama,
            'taksiran' => $data['total_taksiran'],
            'pengajuan_pinjaman' => $data['pengajuan_pinjaman'],
            'tenor' => $data['tenor'],
            'diskon_tabel_id' => $data['diskon_tabel_id'],
            'diskon_tabel' => $data['diskon_tabel'],
            'mu_nah' => $data['mu_nah'],
            'diskon' => $data['diskon'],
            'mu_nah_nett' => $data['mu_nah_nett'],
            'angsuran' => $data['angsuran'],
            'mu_nah_akad' => $data['mu_nah_akad'],
            'ijk' => $data['ijk'],
            'biaya_buka_rekening' => $data['biaya_buka_rekening'],
            'outlet_pengajuan ' => $data['outlet_pengajuan'],
            'code_outlet ' => $data['id_outlet_pengajuan'],
            'status ' => 'active',
            'applied_at ' =>  date('Y-m-d H:i:s'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ];

        $this->db->trans_begin();
        $this->db->insert($this->table, $fields);
        $this->db->insert_batch('booking_arrum_haji_agunan', $data['agunan']);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return null;
        }

        $this->db->trans_commit();

        return $this->getByCodeBooking($data['booking_id']);
    }

    function isIdExist($id)
    {
        $cek = $this->db->query('SELECT * FROM booking_arrum_haji where booking_id =' . $id);
        if ($cek->num_rows() > 0) {
            return true;
        }

        return false;
    }

    function getNewId($id_outlet_pengajuan)
    {
        //format id 5 digit kode cabang + 2 digit tahun + 4 digit sequence
        $result = $id_outlet_pengajuan . date('m') . date('y');
        
        $this->db->select('booking_id');
        $this->db->from('booking_arrum_haji');
        $this->db->like('booking_id', $result);
        $this->db->order_by('booking_id', 'desc');
        $this->db->limit(1);
        $cek = $this->db->get();
        
        if ($cek->num_rows() > 0) {
            $sequence = substr($cek->row()->booking_id, -4);
            $sequence += 1;
            $pad_length = 4;
            $pad_char = 0;

            // output 0123
            $sequence = str_pad($sequence, $pad_length, $pad_char, STR_PAD_LEFT);
            $result .= $sequence;
            return $result;
        }

        $result .= "0001";
        return $result;
    }

    function getByUserId($user_id)
    {
        $query = [
            'user_id' => $user_id,
        ];

        $items = [];
        $table = $this->db;

        if (!empty($query)) {
            $table = $table->where($query)->order_by('created_at', 'desc');
        }

        $item = $table->get('booking_arrum_haji');
        ;

        if ($item && $item->num_rows() > 0) {
            $items = $item->result();
        }

        foreach ($items as $key => $item) {
            $row = new stdClass();
            $row->type = 'arrum_haji';
            $row->code_booking = $item->booking_id;
            $row->status = $item->status;
            $row->status_desc = self::getLabelStatus($item->status);
            $row->estimated = Pegadaian::currencyIdr(floatval($item->pengajuan_pinjaman));
            $row->tenor = $item->tenor . ' Bulan';
            $row->created_at = $item->created_at ?? "";
            $row->update_date = $item->updated_at ?? "";
            $data[] = $row;
        }

        return $data ?? [];
    }

    function find(array $query = [])
    {
        $table = $this->db;

        if (!empty($query)) {
            $table = $table->where($query);
        }

        $item = $table->get($this->table);

        if ($item && $item->num_rows() > 0) {
            $row = $item->row();
            $row->produk = ArrumHajiModel::ARRUM_HAJI_TEXT;
            $row->nama_nasabah = $row->user_name;
            $row->cabang = $row->code_outlet . ' - ' . $row->outlet_pengajuan;
            $row->tenor_text = $row->tenor . ' Bulan';
            $row->biaya_pengajuan = $row->mu_nah_akad + $row->ijk + $row->biaya_buka_rekening;
            $row->agunan = $this->arrum_haji_agunan_model->getByCodeBooking($row->booking_id);
            $row->user = $this->user_model->getUser($row->user_id);
        }

        return $row ?? null;
    }

    function getByCodeBooking($code_booking)
    {
        return self::find(['booking_id' => $code_booking]);
    }

    function updateStatusByCodeBooking($code_booking, $status, $decline_reason)
    {
        $this->db->trans_begin();
        $this->db->where('booking_id', $code_booking)->update($this->table, [
            'status' => $status,
            'decline_reason' => $decline_reason
        ]);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }

        $this->db->trans_commit();

        return self::getByCodeBooking($code_booking);
    }

    function deleteByCodeBooking($code_booking)
    {
        $this->db->where('booking_id', $code_booking)->delete($this->table);

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();

            return false;
        }

        $this->db->trans_commit();
    }

    function getLabelStatus($status)
    {
        switch ($status) {
            case 'active':
                $status_label = 'Tersedia';
                break;
            case 'approve':
                $status_label = 'Diterima';
                break;
            case 'decline':
                $status_label = 'Ditolak';
                break;
            case 'expire':
                $status_label = 'Kadaluarsa';
                break;
        }

        return $status_label ?? '-';
    }
}
