<?php

defined('BASEPATH') or exit('No direct script access allowed');

class EkycSelfieModel extends CI_Model
{

    // INSERT
    function insert($fields = array())
    {
        $this->db->trans_start();
        $this->db->insert('ekyc_selfie', $fields);
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            return 'gagal';
        } else {
            return 'sukses';
        }
    }

    function delete($ekycId)
    {
        $this->db->trans_start();
        $this->db->delete('ekyc_selfie', array('id_ekyc' => $ekycId));
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            return 'gagal';
        } else {
            return 'sukses';
        }
    }
}
