<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Http_request_logger
{

    private $CI;
    
    public function __construct()
    {
        $this->CI =& get_instance();
    }
    
    public function request_logger()
    {
        $uri = $this->CI->uri->uri_string();
        
        $params = trim(print_r($this->CI->input->post(), true));
        
        log_message('info', '==============');
        log_message('info', 'URI: ' . $uri);
        log_message('info', '--------------');
        log_message('info', $params);
        log_message('info', '==============');
    }
}
