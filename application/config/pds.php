<?php
defined('BASEPATH') or exit('No direct script access allowed');

//Admin base URL untuk fetch API dari admin
$config['admin_base_url'] = getenv('ADMIN_BASE_URL');

//Direktori untuk gambar
$config['upload_dir'] = getenv('UPLOAD_DIR');
$config['bank_image_path'] = getenv('BANK_IMAGE_PATH');
$config['asset_url'] = getenv('ASSET_URL');

//Konfigurasi Email
$config['pds_email_host'] = getenv('PDS_EMAIL_HOST');
$config['pds_email_port'] = getenv('PDS_EMAIL_PORT');
$config['pds_email_username'] = getenv('PDS_EMAIL_USERNAME');
$config['pds_email_password'] = getenv('PDS_EMAIL_PASSWORD');
$config['pds_email_from'] = getenv('PDS_EMAIL_FROM');
$config['pds_email_fromname'] = getenv('PDS_EMAIL_FROMNAME');

//Core API Config
$config['core_password'] = getenv('CORE_PASSWORD');
$config['core_API_URL'] = getenv('CORE_API_URL');
$config['core_VA_API_URL'] = getenv('CORE_VA_API_URL');
$config['core_post_password'] = getenv('CORE_POST_PASSWORD');
$config['core_post_username'] = getenv('CORE_POST_USERNAME');
$config['core_syariah_post_username'] = getenv('CORE_SYARIAH_POST_USERNAME');
$config['core_auth_header'] = getenv('CORE_AUTH_HEADER');
$config['core_client_id'] = getenv('CORE_CLIENT_ID');
$config['core_gpoint_client_id'] = getenv('CORE_GPOINT_CLIENT_ID');

//Config Mulia
$config['mulia_image_path'] = getenv('KONTRAK_MULIA_URL');
$config['kontrak_mulia_url'] = getenv('EMAS_IMAGE_PATH');
$config['emas_image_path'] = getenv('MULIA_IMAGE_PATH');
$config['super_admin_base_url'] = getenv('SUPER_ADMIN_BASE_URL');
$config['ekyc_image_path'] = getenv('EKYC_IMAGE_PATH');

//Config API Gadai Efek
$config['gadaiefek_API_URL'] = getenv('GADAIEFEK_API_URL');
$config['gadaiefek_username'] = getenv('GADAIEFEK_USERNAME');
$config['gadaiefek_password'] = getenv('GADAIEFEK_PASSWORD');

// Config API LOS
$config['los_API_URL'] = getenv('LOS_API_URL');
$config['los_username'] = getenv('LOS_USERNAME');
$config['los_password'] = getenv('LOS_PASSWORD');

// Config API P4D LOS
$config['los_p4d_api_url'] = getenv('LOS_P4D_API_URL');
$config['los_p4d_username'] = getenv('LOS_P4D_USERNAME');
$config['los_p4d_password'] = getenv('LOS_P4D_PASSWORD');
$config['los_p4d_prefix_url'] = getenv('LOS_P4D_PREFIX_URL');

// Config API Gade Point
$config['gpoint_API_URL'] = getenv('GPOINT_API_URL');
$config['gpoint_auth_username'] = getenv('GPOINT_AUTH_USERNAME');
$config['gpoint_auth_password'] = getenv('GPOINT_AUTH_PASSWORD');
$config['gpoint_username'] = getenv('GPOINT_USERNAME');
$config['gpoint_password'] = getenv('GPOINT_PASSWORD');
$config['image_assets'] = getenv('IMAGE_ASSETS');

// Config API GCASH Microservice
$config['msgcash_API_URL'] = getenv('MSGCASH_API_URL');
$config['msgcash_client_id'] = getenv('MSGCASH_CLIENT_ID');
$config['msgcash_client_secret'] = getenv('MSGCASH_CLIENT_SECRET');
$config['msgcash_grant_type'] = getenv('MSGCASH_GRANT_TYPE');
$config['msgcash_refresh_token'] = getenv('MSGCASH_REFRESH_TOKEN');

// Config API GoldCard
$config['jobBidangUsaha'] = 99;
$config['jobSubBidangUsaha'] = 9990;
$config['home_status'] = 1;
$config['pendapatan_minimum'] = 3100000;
$config['pendapatan_acak_batas_atas'] = 3200000;

// Dukcapil Body
$config['use_fr_dukcapil'] = getenv('USE_FR_DUKCAPIL');
$config['position'] = getenv('POSITION');
$config['template'] = getenv('TEMPLATE');
$config['threshold'] = getenv('THRESHOLD');
$config['transaction_source'] = getenv('TRANSACTIONSOURCE');
$config['type'] = getenv('TYPE');

$config['base_url'] = getenv('BASE_URL');

//setting gosend url
$config['gosend_url'] = getenv('GOSEND_URL');

//setting gosend pass key
$config['gosend_clientid'] = getenv('GOSEND_CLIENTID');

//setting gosend pass key
$config['gosend_passkey'] = getenv('GOSEND_PASSKEY');

// setting midtrans url
$config['midtrans_url'] = getenv('MIDTRANS_URL');

// setting midtrans pass key
$config['midtrans_serverkey'] = getenv('MIDTRANS_SERVERKEY');

// setting endpoint service CoE
$config['coe_api_url']   = getenv('COE_API_URL');
$config['is_coe_bypass'] = getenv('IS_COE_BYPASS');

// setting client key for OCR
$config['CLIENT_KEY_OCR'] = getenv('CLIENT_KEY_OCR');

// config link aja
$config['biller_validation'] = [
    '070801' => ['max' => 2000000,'min' => 10000,'kelipatan' => 1000],
    '070601' => ['max' => 2000000,'min' => 50000,'kelipatan' => 1000]
];

// Version PDS
$config['versions'] = [
    'Android' => getenv('ANDROID'),
    'iOS' => getenv('IOS'),
    'API PDS' => getenv('API_PDS'),
    'DB' => getenv('DB')
];

// Session Custom
$config['session_expire'] = getenv('SESSION_EXPIRE');

// Milestone
$config['stage'] = getenv('STAGE');
$config['limit_reward_counter'] = getenv('LIMIT_REWARD_COUNTER');

// Web Pemasaran
$config['web_sahabat_pegadaian'] = getenv('WEB_SAHABAT_PEGADAIAN');
$config['web_ranking_sahabat_pegadaian'] = getenv('WEB_RANKING_SAHABAT_PEGADAIAN');

// Config Gpoint API V1
$config['point_api_url'] = getenv('POINT_API_URL');
$config['point_api_username'] = getenv('POINT_API_USERNAME');
$config['point_api_password'] = getenv('POINT_API_PASSWORD');
$config['point_api_prefix'] = getenv('POINT_API_PREFIX');
$config['point_url'] = getenv('POINT_URL');

// term & condition
$config['link_term_and_condition'] = getenv('LINK_TERM_AND_CONDITION');

// Coupon April Emas
$config['coupon_end_date'] = getenv('COUPON_END_DATE');
$config['coupon_draw_date'] = getenv('COUPON_DRAW_DATE');

// Zero Bounce
$config['zero_bounce_url'] = getenv('ZERO_BOUNCE_URL');
$config['zero_bounce_api_key'] = getenv('ZERO_BOUNCE_API_KEY');
$config['zero_bounce_is_dummy'] = getenv('ZERO_BOUNCE_IS_DUMMY');

// IsActive RestCore
$config['rest_core_is_active'] = getenv('REST_CORE_IS_ACTIVE');

// Config QR KTE API
$config['qr_api_url'] = getenv('QR_API_URL');
$config['qr_api_username'] = getenv('QR_API_USERNAME');
$config['qr_api_password'] = getenv('QR_API_PASSWORD');
$config['qr_api_prefix'] = getenv('QR_API_PREFIX');
$config['qr_basic_token'] = getenv('QR_BASIC_TOKEN');
$config['qr_client_key'] = getenv('QR_CLIENT_KEY');
$config['qr_is_dummy'] = getenv('QR_IS_DUMMY');

// Config Krasida Te
$config['merchant_code'] = getenv('MERCHANT_CODE');
$config['merchant_name'] = getenv('MERCHANT_NAME');
$config['merchant_image'] = getenv('MERCHANT_IMAGE');
