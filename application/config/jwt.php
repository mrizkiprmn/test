<?php
defined('BASEPATH') or exit('No direct script access allowed');

$config['jwt_key'] = 'ingDLMRuGe9UKHRNjs7cYckS2yul4lc3';

$config['jwt_algorithm'] = 'HS256';

$config['jwt_exp'] = !empty(getenv('JWT_EXP')) ? time() + getenv('JWT_EXP') : time() + 3600;
