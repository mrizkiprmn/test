<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/*
 * CodeIgniter Monolog integration
 *
 * (c) Steve Thomas <steve@thomasmultimedia.com.au>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/*
 * Log Threshold
 * "ERROR" => 1
 * "DEBUG" => 2 (default)
 * "INFO"  => 3
 * "ALL"   => 4
 */

/* GENERAL OPTIONS */
$config['app_name'] = getenv('LOG_APP_NAME') ?: 'PDS API';
$config['channel'] = ENVIRONMENT;
$config['handlers'] = explode(',', getenv('LOG_HANDLERS') ?: 'file');
$config['threshold'] = getenv('LOG_THRESHOLD') ?: '2';
$config['introspection_processor'] = false; // add some meta data such as controller and line number to log messages

/* FILE HANDLER OPTIONS
 * Log to default CI log directory (must be writable ie. chmod 757).
 * Filename will be encoded to current system date, ie. YYYY-MM-DD-ci.log
*/

// config logfile
$config['file_logfile'] = APPPATH . 'logs/log.php';

// config logstash
$config['logstash_host'] = getenv('LOGSTASH_HOST') ?: '127.0.0.1';
$config['logstash_port'] = getenv('LOGSTASH_PORT') ?: '5000';

// exclusion list for pesky messages which you may wish to temporarily suppress with strpos() match
$config['exclusion_list'] = [
    "loaded", "Enabled", "sanitized", "Session: Initialization under CLI aborted", "Token Session", "closing",
    "Total execution time"
];
