#!/bin/sh -eu

# run shell script scheduler
echo "running shell script scheduler"
sh scheduler.sh &

# run crontab
echo "running apps cronjob"
service cron start

# run nginx
echo "running nginx"
service nginx start

# run php-fpm
echo "running php-fpm"
php-fpm