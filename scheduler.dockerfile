FROM artifactory.pegadaian.co.id:8084/php:7.1.29-fpm

# init environment variables
ENV WORKDIR /var/www

# update linux installer
RUN apt-get update && apt-get -y install git && apt-get -y install zip

# install crontab
RUN apt-get -y install cron

# install libraries that needed for php extentions
RUN  apt-get install -y libmcrypt-dev \
        libmagickwand-dev --no-install-recommends \
        && apt-get install -y libxslt-dev \
        && apt-get install -y libssl-dev \
        && apt-get install -y gcc make autoconf libc-dev pkg-config \
        && apt-get install -y zlib1g-dev \
        && apt-get install -y libmemcached-dev \
        && apt-get install -y libfreetype6-dev \
        && apt-get install -y libxslt-dev

# install php extentions using docker-php-ext
RUN docker-php-ext-install pdo_mysql \
        && docker-php-ext-install mysqli \
        && docker-php-ext-install bz2 \
        && docker-php-ext-install mcrypt \
        && docker-php-ext-install calendar \
        && docker-php-ext-install exif \
        && docker-php-ext-install gd \
        && docker-php-ext-install gettext \
        && docker-php-ext-install sockets \
        && docker-php-ext-install wddx \
        && docker-php-ext-install xsl \
        && docker-php-ext-install bcmath

# install php extentions using pecl
RUN pecl install crypto-0.3.1 \
        && pecl install igbinary-2.0.8 \
        && pecl install memcached-3.0.4 \
        && pecl install msgpack-2.0.2 \
        && docker-php-ext-enable crypto \
        && docker-php-ext-enable igbinary \
        && docker-php-ext-enable memcached \
        && docker-php-ext-enable msgpack

# install composer
RUN php -r "readfile('http://getcomposer.org/installer');" | php -- --install-dir=/var --filename=composer

# create project directory
WORKDIR $WORKDIR

# copy all files
COPY . $WORKDIR

# move to project directory
RUN cd $WORKDIR

# create .env file
RUN echo > .env

# install all dependencies
RUN php /var/composer install --no-progress --prefer-dist --no-suggest

# cron setup
ADD cronfile /etc/cron.d/cronfile
RUN chmod 0644 /etc/cron.d/cronfile
RUN touch /var/log/cron.log
RUN crontab /etc/cron.d/cronfile

ENTRYPOINT ["sh", "./entrypoint.scheduler.sh"]