#!/bin/sh -eu

# run crontab
echo "running apps cronjob"
service cron start

# run shell script scheduler
echo "running shell script scheduler"
sh scheduler.sh >> /var/log/cron.log 2>&1 &

tail -f /var/log/cron.log
