<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/*
 * CodeIgniter Monolog integration
 *
 * Version 1.1.1
 * (c) Steve Thomas <steve@thomasmultimedia.com.au>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Monolog\Formatter\JsonFormatter;
use Monolog\Handler\SocketHandler;
use Monolog\Logger;
use Monolog\ErrorHandler;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Processor\IntrospectionProcessor;
use Monolog\Formatter\LogstashFormatter;
use Monolog\Processor\WebProcessor;

/**
 * replaces CI's Logger class, use Monolog instead
 *
 * see https://github.com/stevethomas/codeigniter-monolog & https://github.com/Seldaek/monolog
 *
 * @property Logger log
 */
class CI_Log
{
    // CI log levels
    protected $_levels = [
        'OFF'   => '0',
        'ERROR' => '1',
        'DEBUG' => '2',
        'INFO'  => '3',
        'ALL'   => '4'
    ];

    // config placeholder
    protected $config = [];
    protected $config_file = 'config/monolog.php';

    /**
     * prepare logging environment with configuration variables
     */
    public function __construct()
    {
        $file_path = APPPATH . $this->config_file;

        // Fetch the config file
        if (!file_exists($file_path) && empty($file_path)) {
            exit('monolog.php config does not exist');
        }

        require $file_path;

        // make $config from config/monolog.php accessible to $this->write_log()
        $this->config = $config;

        $this->log = new Logger($this->config['channel']);
        // detect and register all PHP errors in this log hence forth
        ErrorHandler::register($this->log);

        if ($this->config['introspection_processor']) {
            // add controller and line number info to each log message
            $this->log->pushProcessor(new IntrospectionProcessor());
        }

        // decide which handler(s) to use
        foreach ($this->config['handlers'] as $value) {
            switch ($value) {
                case 'file':
                    $handler = new RotatingFileHandler($this->config['file_logfile']);
                    $handler->pushProcessor(new WebProcessor());
                    $handler->pushProcessor($this->addLoggerFilteringFor());
                    $handler->pushProcessor($this->addRequestID());
                    $handler->setFormatter(new JsonFormatter());
                    break;
                case 'logstash':
                    $handler = new SocketHandler("tcp://{$config['logstash_host']}:{$config['logstash_port']}");
                    $handler->setFormatter(new LogstashFormatter($config['app_name']));
                    break;
                default:
                    exit('log handler not supported: ' . $this->config['handler']);
            }

            $this->log->pushHandler($handler);
        }
    }


    /**
     * Write to defined logger. Is called from CodeIgniters native log_message()
     *
     * @param string $level
     * @param $msg
     * @param array $data
     * @return bool
     */
    public function write_log($level, $msg, array $data)
    {
        $level = strtoupper($level);

        // verify error level
        if (!isset($this->_levels[$level])) {
            $this->log->addError('unknown error level: ' . $level, $data);
            $level = 'ALL';
        }

        // filter out anything in $this->config['exclusion_list']
        if (!empty($this->config['exclusion_list'])) {
            foreach ($this->config['exclusion_list'] as $find_me) {
                $pos = strpos($msg, $find_me);
                if ($pos !== false) {
                    // just exit now - we don't want to log this error
                    return true;
                }
            }
        }

        if ($this->_levels[$level] <= $this->config['threshold']) {
            switch ($level) {
                case 'ERROR':
                    $this->log->addError($msg, $data);
                    break;
                case 'DEBUG':
                    $this->log->addDebug($msg, $data);
                    break;
                case 'ALL':
                case 'INFO':
                    $this->log->addInfo($msg, $data);
                    break;
            }
        }

        return true;
    }

    private function addLoggerFilteringFor()
    {
        return function ($record) {
            foreach ($record['context'] as $key => $value) {
                $check = $this->checkMatchKeyFilter($key);
                if ($check) {
                    $record['context'][$key] = '**HIDDEN FROM LOG**';
                }
            }
            return $record;
        };
    }

    private function checkMatchKeyFilter($key)
    {
        $filters = ['password', 'pin', 'base64'];

        foreach ($filters as $filter) {
            $check = strpos(strtolower($key), $filter);
            if ($check !== false) {
                return true;
            }
        }

        return false;
    }

    private function addRequestID()
    {
        return function ($record) {
            $request_id = isset($_SERVER['HTTP_X_REQUEST_ID']) ? $_SERVER['HTTP_X_REQUEST_ID'] : null;
            $record['extra']['request_id'] = $request_id;

            return $record;
        };
    }
}
